<?php
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/seo-metas.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/tabbing.js"></script>
<script type="text/javascript" src="js/jquery.tool.min.js" language="javascript"></script>
<script type="text/javascript" src="js/popup1.js" language="javascript"></script>
<link rel="stylesheet" type="text/css" href="./css/global.css?v=<?php echo mktime(); ?>" />
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header-js.php");
	include_once("./includes/header.php"); 
	include_once("./cache-func.php");
    include_once('./classes/cor.mysql.class.php');
?>
<!--Banner Start Here-->

<!--Middle Start Here-->


<div id="middle" style="margin-top:10px;">
<h1 class="yellwborder" style="background-color:#DB4425;">
<div class="w1130" style="color:white;font-size:14px">OFFER : FIRST RIDE FREE</div>
</h1>
<div class="main">
<div class="aboutus">
		<h2 style="font-size:16px;font-family:calibri;"></h2>
		<p style="font-size:14px;font-family:calibri;">The offer can be redeemed as:</p>
		</br>
		<br />
		<span style="font-size:14px;font-family:calibri;"> 
		A complimentary/free ride for 2 hours on Amaze, Polo, i20 Diesel, Pulse, Etios GD,  Swift,  Beat, Grand i10, Figo, e2o, Nano Twist </br>in select cities/locations i.e. for a 2 hour reservation with  bill value less than/equal to Rs.200.</span><br />
		<br />
		<br />
		<!--<span style="font-size:14px;font-family:calibri;">
		   If you wish to go out for a longer drive, use the discount/coupon code to get a flat discount of Rs.200 applicable for all cars </br> & in all cities/locations. Choose from any of the hourly/daily/weekly/monthly booking reservation.
		</span>
		<br />
		<br />--->
		<br />
		<br />
		
		<h2 style="font-size:16px;font-family:calibri;">
			TERMS AND CONDITIONS</h2>
		
		<ol style="padding:20px;font-size:14px;font-family:calibri;" type="disc">
			<li>
				 Valid for first time users only.</li>
			<li>
				The discount amount is applicable on the base fare only.</li>
			<li>
				Valid only on the self-drive service: Myles</li>
			<li>
					Booking subject to car availability.</li>
			<li>
				As a mandate, a minimum of Rs.1 will have to be paid to complete the transaction. This can be paid through debit card/credit card/netbanking.</li>
				<li>
					Standing instructions/ pre-authorization will be made on the credit card based on the car type at the time of pick-up</li>
				<li>
					Offer not to be clubbed with any other promotion</li>
				<li>
					The discount can be redeemed in full & not in parts</li>
				<li>
					Valid for reservations made before 30th September 2015.</li>
				<li>
				Blackout dates apply</li>
				<li>
				Other standard T&C apply. </li>
				<br />
		</ol>
	  <h2 style="padding:0px;font-size:16px;font-family:calibri;">
			HOW TO AVAIL / BOOK</h2>
		
	     <ol style="padding:20px;font-size:14px;font-family:calibri;" type="disc">
			<li>
				Log on to www.carzonrent.com , our booking engine to book a Myles car. </li>
			<li>
				Select the preferred city, date/time, car & the location.</li>
			<li>
				Post filling in the details, enter the discount/coupon code in the “Discount/Promotion Code” box.  After entering the code, click on “Apply” button next to the box. </li>
			</ol>
			<p>Proceed with the booking & pay the balance amount through credit card/debit card/net banking. </p></br>

<p style="font-size:14px;font-family:calibri;">Know more about the service at www.mylescars.com or call on 08882222222.</p>
</br></br>

<h1 class="yellwborder" style="background-color:#F2C900;  width: 207px;background-color: #DB4425; float: left;font-size:14px;color:white;font-family:calibri;">
<div class="w1130" style="padding-left: 22px;" > Click <a href="javascript:void(0);" onclick="tnp_popup();" style="color:white;">here</a>  
to get the code.</div>
</h1>
                     


</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<div id="footer">
	<div class="main">
    	
        <div class="fbtm" style="width:740px;">
		<?php
			if((isset($_GET["src"]) && $_GET["src"] != "") || (isset($_COOKIE["corsrc"]) && isset($_COOKIE["corsrc"]) != ""))
			{}
			else
			{
		?>
        	<div class="cm" style="width:180px">
            	<h3>Company</h3>
                <ul>
                	<li><a href="<?php echo corWebRoot; ?>/aboutus.php">About</a></li>
			<li><a href="<?php echo corWebRoot; ?>/services.php">Services</a></li>
			<li><a href="http://careers.carzonrent.com/" target="_blank">Career</a></li>
			<li><a href="<?php echo str_ireplace("www", "blog", corWebRoot); ?>/" target="_blank">Blog</a></li>
			
                </ul>
            </div>
            <div class="cm" style="width:180px">
            	<h3>Book a Cab</h3>
                <ul>
                	<li><a href="<?php echo corWebRoot; ?>/#o">Outstation</a></li>
			<li><a href="<?php echo corWebRoot; ?>/#l">Local</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/">EasyCabs</a></li>
                </ul>
            </div>
            <div class="cm" style="width:180px">
            	<h3>EasyCabs</h3>
                <ul>
                	<li><a href="<?php echo ecWebRoot; ?>/advertise-with-us.php" target="_blank">Advertise with Us</a></li>
                        <li><a href="<?php echo ecWebRoot; ?>/fleet.php" target="_blank">Fleet</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/tariff.php" target="_blank">Tariff</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/partners.php" target="_blank">Partners</a></li>
                </ul>
            </div>
	   
            <div class="qu" style="width:180px">
            	<h3>Quick Links</h3>
                <ul>
			<li><a href="<?php echo corWebRoot; ?>/myaccount.php">My Account</a></li>
			<li><a href="<?php echo corWebRoot; ?>/print-invoice.php">Print Invoice</a></li>
			<li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
			<li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
                </ul>
            </div>
        </div>
	<?php
		}
	?>
<script type="text/javascript">
function troggleMail()
{
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var emailId= document.getElementById('chatemail').value;
if(emailId=="")
{
alert('Please Enter Email Id.');
$("#chatemail").attr('placeholder','Please Enter Email.').css('background','rgb(240, 223, 205)').focus();
return  false;
}
if (!filter.test(emailId)) {
	alert('Please fill a valid email id');
	$("#chatemail").attr('placeholder','Please fill a valid email id.').css('background','rgb(240, 223, 205)').focus();
	$("#chatemail").focus(); 
	return  false;

}
var dataString = 'emailId='+ emailId;
$.ajax({type:"POST",
url: "<?php echo corWebRoot; ?>/sendemailuser.php",
data:dataString,
beforeSend: function() {
$('#loading_image').show();
$('#btnfrst').attr('disabled','disabled');
},
success: function(response) { 
var txt=response;
if(txt==1)
{
$(".m2").hide();
alert('Message has been send.Please Check Email.');
$('#loading_image').hide();
}
}});

}
</script>
		<script type="text/javascript"> 
		function tnp_popup(){
		$(".visitorPopUp").delay(100).fadeIn();
		$("#additempopup1").show();
		$(".visitorPopUp").animate({top: '135px'});
		$(".contentSlider").show();
		$(".closetnp").show();
		}
		function tnp_popup_close(){
		$(".contentSlider").hide('slow');
		$("#additempopup1").hide('slow');
		$('.visitorPopUp').hide('slow');
		}
		
		</script>
		
		
		
		
		<div id='msgdata'></div>
		<div class="visitorPopUp">
		<a class="closetnp" onclick="tnp_popup_close();">X</a>
		<div class="row">
		<div class="row">
		<img src="<?php echo corWebRoot; ?>/images/32.gif" id="loading_image"/>
		</div>
		<div class="w_39 mr2 f_l">
		</div>
		<div class="w_59 f_l">
		<p class="m3"></p>
		<input placeholder="Please Enter Email." name="chatemail" id='chatemail' type="text" onkeyup="$('.m3').html('');$('#chatemail').css('background-color','');">
		<input type="button" class="signup" value="Submit" id="btnfrst" name='submit' onclick="troggleMail();">
		</div>
		</div>
		</div>
		
		
		
		
		
		
		
		
		
		<style type="text/css">
		img#loading_image{
		position: absolute;
        top: 167px;
		right: 19px;
		height: 36px;
		display:none;
		}
		.visitorPopUp{
		#background:#ffffff;
		background: url(../images/myles-popup.jpg);
		width:500px;
		padding:10px;
		position:fixed;
		height: 192px !IMPORTANT;
		left:30%;
		z-index:999999999999999999999999999999999999;
		color:#000;
		#border: solid rgb(219, 70, 38) 2px;
		}
		.visitorPopUp a{
		text-decoration:none;
		color:#fff;
		}
		/*****************/
		.visitorPopUp a.closetnp{
		position: absolute;
		top: -9px;
		right: -16px;
		color: white !important;
		text-decoration: none;
		font-weight: bold;
		font-size:18px !important;
		display:block;
		cursor:pointer;
		}
		.visitorPopUp{
		padding:30px;
		}
		.row ul li{
		font-size:14px;
		}
		.w_39{
		width:39%;
		}
		.w_59{
		width:59%;
		}
		.mr2{
		margin-right:2%;
		}
		.f_l{
		float:left;
		}
		.visitorPopUp h1{
		text-align:center;
		font-size:20px;
		line-height:20px;
		margin:0px;
		padding:0;
		color:#6B6B6B;
		}
		.visitorPopUp .row{
		margin:0px;
		padding: 5px 0;
		display: inline-block;
		width: 100%;
		min-height: 67px;
		}
		.visitorPopUp input{
		padding:8px 10px;
		line-height:18px;
		width:100%;
		margin-Left: 215PX;
		margin-top:47px;
		  width: 272px;
		}
		.visitorPopUp label,
		.visitorPopUp p{
		display:inline-block;
		font-size:14px;
		padding:2px 0;
		margin:0;
		}
		.visitorPopUp p{
		padding-bottom:5px;
		}
		.visitorPopUp strong{
		color:#DB4626;
		}
		.error{
		font-size:11px;
		padding:2px 0;
		}
		.visitorPopUp a{
		color:#DB4626;
		font-size:10px;
		padding-left:19px;
		}
		.visitorPopUp a.tc{
		display:block;
		padding-top:8px;
		}
		.visitorPopUp table input{
		width:auto;
		}
		.visitorPopUp input.signup{
		border:none;
        width: 100px;
		background:#DB4626;
		color:#fff;
		font-weight:bold;
		font-size:14px;
		line-height:18px;
		cursor:pointer;
		margin-top:8px;
	    margin-left: 411px;
		}
		.visitorPopUp ul{
		margin:5px 0 0 0;
		padding:0 0 0 20px;
		}


		</style>
		<div id="additempopup1"  style="width:100%; height:100%; left:0px; top:0px;z-index: 2147483646;  background:#000; opacity:.6; position:fixed; display:none;"></div>



	
	
<div style="clear: both;"></div>
<div class="citilist footer_list">
<a href="<?php echo corWebRoot; ?>/carrental/car-rental-delhi-to-agra/">Car Rental Delhi</a>
<a href="<?php echo corWebRoot; ?>/carrental/car-rental-bangalore-to-coorg/">Car Rental Bangalore</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Mumbai-4/">Car Rental Mumbai</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Hyderabad-6/">Car Rental Hyderabad</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Chennai-8/">Car Rental Chennai</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Pune-5/">Car Rental Pune</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Ahmedabad-1/">Car Rental Ahmedabad</a>
<a class="br_none" href="<?php echo corWebRoot; ?>/car-rental-Jaipur-10/">Car Rental Jaipur</a>
<span>
<a href="<?php echo corWebRoot; ?>/car-rental-Chandigarh-9/">Car Rental Chandigarh</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Visakhapatnam-49/">Car Rental Visakhapatnam</a>
<a href="<?php echo corWebRoot; ?>/car-rental-Noida-11/">Car Rental Noida</a>
<a class="br_none" href="<?php echo corWebRoot; ?>/car-rental-Gurgaon-3/">Car Rental Gurgaon</a>
</span>
</div>
	<div style="clear: both;"></div>
        <div class="copurit">Copyright @ 2014 <b>Carzonrent India Pvt Ltd</b>. All Rights Reserved. <a href="<?php echo corWebRoot; ?>/terms-of-use.php" class="copurita">Terms of Use</a> | <a href="<?php echo corWebRoot; ?>/privacy-policy.php" class="copurita">Privacy Policy</a> | <a href="<?php echo corWebRoot; ?>/faqs.php" class="copurita">FAQs</a> | <a href="http://ecompliance.carzonrent.com" class="copurita" target="_blank">e-Compliance</a>
		
    </div>
	<?php
		if(basename($_SERVER['PHP_SELF']) != 'myles-feedback.php')
		{
	?>
		<a id="feedbackpopupRED" class="feedbackred" href="javascript: void(0);" onclick="javascript:_disableThisPage();_setDivPos('registration');_checkIsSelfDrive();">
		<img src="<?php echo corWebRoot; ?>/images/feedb_red.png" width="30" height="100" border="0" /></a>
		
		<a id="feedbackpopupYELLOW" class="feedbackyellow" href="javascript: void(0);" onclick="javascript:_disableThisPage();_setDivPos('registration');_checkIsSelfDrive();">
		<img src="<?php echo corWebRoot; ?>/images/feedb_yellow.png" width="30" height="100" border="0" /></a>
		
		
		
	<?php
		}
	?>
	
	<script type="text/javascript"> 

	$('.top-mid li').click(function(e) 
	{
		href=$("a", $(this)).eq(0).text();
		if(href=='Local')
		{
		$("#querytype").prop('selectedIndex', 0);
		$("div#lyrics").hide();
		$(".close").hide();
		}
		
		if(href=='Outstation')
		{
		$("#querytype").prop('selectedIndex', 0);
		$("div#lyrics").hide();
		$(".close").hide();
		}
		if(href=='Selfdrive')
		{
		$("#querytype").prop('selectedIndex', 0);
		$("div#lyrics").hide();
		$(".close").hide();
		}
		
		
	});	
	
	
$(document).ready(function(){
    
	
	$("div#lyrics").hide();
	$("#songlist div").click(function(){
	$("#lyrics",this).show("slow");
	$(".close").show();
	});
	    $(".close").click(function(){
		$("#lyrics").hide();
		$(this).hide();
	});
	
    
});
function troggleopen()
{
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			regexpch = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
			var txtname=$("#txtname").val();
			var txtnameval =document.getElementById("txtname");
			var querytype = $( "#querytype option:selected" ).val();
			var email=$("#email").val();
			var monumber=$("#monumber").val();
			var query=$("#query").val();
			if($("#txtname").val().trim()=="")
			{
				alert("Please Enter Name");
				$("#txtname").focus();
				return  false;
			}
			if(!txtnameval.value.match(regexpch))
			{
			alert("Please Enter character only");
			$("#txtname").focus(); 
			return false

			}
			
			if(monumber=="")
			{
				alert("Please Enter Mobiile no");
				$("#monumber").focus();
				return  false;
			}
			
			
			if(monumber.substr(0,1) != "9" && monumber.substr(0,1) != "8" && monumber.substr(0,1) != "7")
			{
			alert("Invalid mobile number. Please enter correct mobile number.");
			$("#monumber").focus(); 
			return  false;
			}
			if(monumber)	
			{
			if(monumber.length != 10)
			{
			alert("Mobile number must be of 10 digit.");
			$("#monumber").focus(); 
			return  false;
			}
			}
			
			
			if(querytype=="0")
			{
				alert("Please Select Service");
				$("#querytype").focus();
				return  false;
			}
			
			
			$(function () {
				var dataString ='txtname='+txtname+'&email='+email+'&monumber='+monumber+'&query='+query+'&querytype='+querytype;
				$.ajax({type:"POST", url: "<?php echo corWebRoot; ?>/myles-enquiry_feedback.php",
				data:dataString,
				beforeSend: function() {
				$('#encmail').hide();	
				$("#enq_process").show();
                }, 
				success: function(response) {
				$("#txtname").val('');
				$("#email").val('');
				$("#monumber").val('');
				$( "#querytype option:selected" ).val('');
				$("#query").val('');
				$("div#lyrics").hide();
				
				if(response==1)
				{	
					alert("Thank you for your valued enquiry, will get back to you shortly");
				    $('#encmail').show();	
				    $("#enq_process").hide();
					$("#closebtn").hide();
					
				}
				}});
			}); 
		
		
}
</script>
	<div class="close" id="closebtn">X</div>
	<div id="songlist">
	<div class="enquery">
	
	<img class="enq enredbutton" id="enredbuttonID" src="<?php echo corWebRoot; ?>/images/enq_red.png" width="30" height="100" border="0"/>
	<img class="enq enyellowbutton" id="enyellowbuttonID" src="<?php echo corWebRoot; ?>/images/enq_yellow.png" width="30" height="100" border="0"/>
	<div id="lyrics" style="display:none;">
	<form name="from1" method="post">
	<label for="name" class="mr2">Name <span class="starR">*</span> :</label>
	<input type="text" name="txtname" id='txtname' value="">
	<div class="clr"> </div>
	<label for="email" class="mr2">Emal ID :</label>
	<input type="text" name="email" id='email' value="">
	<div class="clr"> </div>
	<label for="mobile" class="mr2">Mobile <span class="starR">*</span> :</label>
	<input type="text" name="monumber" id='monumber' maxlength="10" value="">
	<div class="clr"> </div>
	<label for="querrytype" class="mr2"> Service <span class="starR">*</span> :</label>
	<select  name="querytype" id='querytype'>
	<option value="0" selected='selected'>-Select Service-</option>
	<option value="Selfdrive">Selfdrive</option>
	<option value="Outstation">Outstation</option>
	<option value="Local">Local</option>
	</select>
	<div class="clr"> </div>
	<label for="query" class="mr2 query">Query :</label>
	<textarea name="query" id="query"></textarea>
	<label for="name" class="mr2 hidden">hidden</label>
	<img src="<?php echo corWebRoot; ?>/images/processing.jpg" id="enq_process"  style="height: 35px;display:none;"/>
	<img id='encmail' src="<?php echo corWebRoot; ?>/myles-campaign/images/enqButton.png" width="70" height="28" border="0" onclick="return troggleopen();"/>
	</form>
	</div>
	</div>
	</div>
	

</div>
<?php include_once("./includes/feedback-popup.php"); ?>
<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide:google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957885192;
var google_conversion_label = "FhZ6COil-gUQiNbgyAM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957885192/?value=0&amp;label=FhZ6COil-gUQiNbgyAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Google Code for Dynamic Remarketing Tag 10 june 2015 -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->

<script type="text/javascript">
var google_tag_params = {
dynx_itemid: 'Delhi1',
dynx_itemid2: '',
dynx_pagetype: 'H',
dynx_totalvalue: '',
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957885192;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957885192/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!----end dynamic code 10-06-2015----->

<div id='invtrflfloatbtn'></div>
	<script type="text/javascript">	
	//<![CDATA[
	var invite_referrals = window.invite_referrals || {}; (function() {
	invite_referrals.auth = { bid_e : '1B3596545F3AF427193DA8B36E523D4F', bid : '2926', t : '420', orderID : '', email : '' };
	var script = document.createElement('script');script.async = true;
	script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
	var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
	  //]]>
	</script>
	
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
//<![CDATA[

window.$zopim||(function(d,s)
{
try{
var z=$zopim=function(c){z._.push(c)},
$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];
z.set=function(o){
z.set._.push(o)
};
z._=[];
z.set._=[];
$.async=!0;
$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2dZneFTDBlOFVW3oEL4RWUbH0MOmmpHL';
z.t=+new Date;
$.type='text/javascript';
e.parentNode.insertBefore($,e)

}
catch(err)
{
}
})
(document,'script');
//]]>
</script>


<!-- Google Tag Manager -COR-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSZV9B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script type="text/javascript">
//<![CDATA[
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSZV9B');
//]]>
</script>
<!-- End Google Tag Manager -->

<!--End of Zopim Live Chat Script-->	
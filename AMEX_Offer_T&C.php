<?php 
error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	define('corWebRoot','http://10.90.90.37/carzonrent/images/mailer_banner.jpg');
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AMEX Offer T&C || carzonrent.com</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/cor.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<style type="text/css">
.myles_head {
margin: 20px auto 0;
width: 100%;
background-color: #DB4425;
height: 40px;
line-height: 40px;
color: #fff;
}
.magindiv {
width: 1130px;
margin: 0 auto !important;
}
.sale{
width:250px;
display:inline-block;
text-align:center;
margin-bottom:10px;
padding:0px;
height:auto;
background:#df4627;
}
.f_l{float:left;}
.clr{clear:both;}
.sale_content{
width:72%;
margin-left:20px;
margin-bottom:20px;
background-color:#FBFBFB;
padding:15px;
border:1px solid #E5E5E5;
}
.sale_content ul{
margin:5px 0 10px 19px;
padding:0px;
}
.sale_content p.know_more_TC{margin-bottom:0px;}
.sale_content ul li{
font-size:13px;
}
.hedding_tc{
color:#e1471b;
padding:5px 0 0;
font-size:13px;
}
.sale_content ul.off{
margin-bottom:2px;
}
</style>
<div id="middle" style="width:100%" >
<div class="myles_head">
<div class="magindiv">
</div>
</div>
<div class="mainW960" style="padding:20px 0;">
<div class="sale f_l">
<img src="http://www.carzonrent.com/images/tc.jpg" border="0">
</div>
<div class="f_l sale_content">
<br>
<strong>Terms and Conditions</strong>
<ul>
<li>An American Express Cardmember ("Cardmember") for the purpose of this program means a person holding an American Express Card, issued by American Express® Banking Corp in India Only. </li>
<li>This offer is valid on all American Express Cards. </li>
<li>Offer is valid only on the self drive service: Myles</li>
<li>Valid for first time Myles users & American Express cardmembers only.</li>
<li>The discount amount is applicable on the base fare. Base fare is excluding VAT & other tax.</li>
<li>Valid on all cars and city of operations.</li>
<li>Booking subject to car availability.</li>
<li>In case the total amount is less than or equal to Rs. 600, a minimum of Rs. 1 will have to be paid to complete the transaction.</li>
<li>Standing instructions/ pre- authorization will be made on the American Express credit card based on the car type at the time of pick up.</li>

 
 <li>Discount not applicable on hourly bookings.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>This offer is valid till 31st Jan’16. </li>
<li>This offer is being made purely on a “best effort” basis. Cardmembers are not bound in any manner to participate in this offer and any such participation is purely voluntary.</li>
<li>American Express is neither responsible for availability of services nor guarantees the quality of the goods/services and is not liable for any defect or deficiency of goods or services so obtained/availed of by the Cardmembers under this offer. Any disputes with regard to the quality of goods/services availed shall be taken up with the merchant/service establishment directly. American Express shall have no liability whatsoever with regard to the same.</li>
<li> American Express shall not be liable whatsoever for any loss/damage/claims that may arise out of use or non-use of any goods or services availed by Cardmember under this offer. American Express reserves its absolute right to withdraw and/or alter any of the terms and conditions of the offer at any time without prior notice</li>


<li>Nothing expressed or implied in the program shall in any way waive or amend any of the terms and conditions of the existing Cardmember agreement with the Card issuer</li>

<li> Any disputes arising out of and in connection with this program shall be subject to the exclusive jurisdiction of the courts in the state of Delhi only</li>

<li>For any queries related to the offer, please contact Myles customer service: 08882222222</li>

<li> Myles will determine the product/service prices, availability and taxes, all of which are subject to change at any time without notice.</li>
<li>Queries pertaining to discount not received would not be entertained after the 30th April, 2016.</li>

</ul>
</div>

</div>
</div>
<?php include_once("./includes/footer.php"); ?>
<script type="text/javascript" src="js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="css/tab.css">
</body>
</html>

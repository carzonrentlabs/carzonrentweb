<?php
header('Location:http://www.carzonrent.com');
exit;
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	
	$refCode = "";
	if(isset($_GET["ref"]) && trim($_GET["ref"]) != "")
	$refCode = $_GET["ref"];
	$refURL = $_SERVER['HTTP_REFERER'];
	
	if($refCode != "" || $refURL != ""){
		include_once('./classes/cor.mysql.class.php');
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$campaign_code = "MYLES";
		$trackData = array();
		$trackData["campaign_code"] = $campaign_code;
		$trackData["entry_date"] = date('Y-m-d H:i:s');
		$trackData["ref_code"] = $refCode;
		$trackData["ref_url"] = $refURL;
		$trackData["curr_url"] = $_SERVER['REQUEST_URI'];
		$trackData["ip"] = $_SERVER["REMOTE_ADDR"];
		$trackData["ua"] = $_SERVER["HTTP_USER_AGENT"];
		$r = $db->insert("cor_camaign_tracking", $trackData);
		
		unset($trackData);
		$db->close();
		
		if($refCode != "")
		setcookie("refCode", $refCode, time() + (24 * 60 * 60) , "/");
		if($refURL != "")
		setcookie("refURL", $refURL, time() + (24 * 60 * 60) , "/");
	}
	
	$ctid = "2";
	$ctnm = "Delhi";
	if(isset($_GET["ctid"]) && $_GET["ctid"] != ""){
	  $ctid = $_GET["ctid"];
	  $ctnm = ucwords($_GET["ctid"]);
	}
	$arrCityName = array("Delhi", "Mumbai", "Bangalore", "Hyderabad", "Goa", "Jaipur", "Chennai", "Pune", "Ahmedabad", "Chandigarh","Visakhapatnam","Amritsar", "Manesar", "Bhubaneswar", "Noida", "Gurgaon", "Mangalore", "Mysore", "Udaipur", "Ghaziabad");
	$arrCityID = array(2, 4, 7, 6, 69, 10, 8, 5, 1, 9, 49, 43, 39, 41, 11, 3, 80, 81, 44,82);
	$ctid = str_ireplace($arrCityName, $arrCityID, $ctid);
	$isHrAvail = true;
	$pkt = "Hourly";
	if(isset($_GET["pkt"]) && $_GET["pkt"] != "")
	$pkt = $_GET["pkt"];
	
	$isHrAvail = true;
	$isWeekAvail = true;
	$isMonthAvail = true;
	
	$arr = array();
	$arr["CityID"] = $ctid;
	$arr["PkgType"] = $pkt;
	$cor = new COR();
	$res = $cor->_CORGetPackageDetails($arr);	
	$myXML = new CORXMLList();
	$pkgs = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	unset($arr);
	
	$arr = array();
	$arr["CityID"] = $ctid;
	$arr["PkgType"] = "Daily";
	$res = $cor->_CORGetPackageDetails($arr);	
	$pkgsDaily = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	unset($arr);
	
	$arr = array();
	$arr["CityID"] = $cCityID;
	$arr["PkgType"] = "Weekly";
	$res = $cor->_CORGetPackageDetails($arr);	
	$pkgsWeekly = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	unset($arr);
	
	$arr = array();
	$arr["CityID"] = $cCityID;
	$arr["PkgType"] = "Monthly";
	$res = $cor->_CORGetPackageDetails($arr);	
	$pkgsMonthly = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	unset($arr);
	
	//echo "<pre>";
	//print_r($pkgs);
	//echo "</pre>";
	unset($myXML);
	unset($cor);
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/myles-seo-metas.php"); ?>
<meta property="og:title" content="Myles- Self Drive" />
<meta property="og:description" content="Hey guys! I'm about to hit the open road with a Myles cars! Check out the link below if you want to do the same!" />
<meta property="og:image" content="http://www.carzonrent.com/myles-campaign/shraredImage/new.png"/>
<meta property="og:image:type" content="image/png"/>
<meta property="og:image:width" content="600"/>
<meta property="og:image:height" content="315"/>



<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/enfeedback.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/myles.css" type="text/css" media="screen" />

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>
<script type="text/javascript">
//<![CDATA[
  $j = jQuery.noConflict();
  $j(document).ready(function(){  
    $j("div.item").tooltip();
    var url = document.URL;
    var hName = url.split("#");
    if (hName.length <= 1) {
      window.scrollTo(0, 170);
    }
  });
  
   function formatNumber (num) 
  {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }
  //]]>
</script>  
<script type="text/javascript">
//<![CDATA[
    var mode = "Hourly";
    var iCount = 0;
<?php
	if(count($pkgsDaily) > 0){
?>
    var iPkgName = "<?php echo trim(strtolower($pkgsDaily[0]["PkgName"])); ?>";
    var iPkgID = <?php echo $pkgsDaily[0]["PkgID"]; ?>;
<?php	
	}
?>
    var hPkgID = new Array();
    var dPkgID = new Array();
    var wPkgID = new Array();
    var mPkgID = new Array();
    
    var hCarImg = new Array();
    var dCarImg = new Array();
    var wCarImg = new Array();
    var mCarImg = new Array();
    
    var hCarName = new Array();
    var dCarName = new Array();
    var wCarName = new Array();
    var mCarName = new Array();
    
    var hCarDec = new Array();
    var dCarDec = new Array();
    var wCarDec = new Array();
    var mCarDec = new Array();
    
    var hPkgRate = new Array();
    var dPkgRate = new Array();
    var wPkgRate = new Array();
    var mPkgRate = new Array();
    
    var hSecDeposit = new Array();
    var dSecDeposit = new Array();
    var wSecDeposit = new Array();
    var mSecDeposit = new Array();
    
	_setCount = function(c){
		if (mode == "Hourly") {
			for(p = 0; p < hPkgID.length; p++){
				if (hPkgID[p] == c) {
					iCount = p;
					iPkgName = hCarName[p];
					break;
				}
			}
		} else if (mode == "Weekly") {
			for(p = 0; p < wPkgID.length; p++){
				if (wPkgID[p] == c) {
					iCount = p;
					iPkgName = wCarName[p];
					break;
				}
			}
		} else if (mode == "Month") {
			for(p = 0; p < mPkgID.length; p++){
				if (mPkgID[p] == c) {
					iCount = p;
					iPkgName = mCarName[p];
					break;
				}
			}
		} else {
			for(p = 0; p < dPkgID.length; p++){
				if (dPkgID[p] == c) {
					iCount = p;
					iPkgName = dCarName[p];
					break;
				}
			}
		}
	}
    _setMode = function(md){
		mode = md;
		iCount = 0;
		if (md == "Hourly") {
			iPkgID = hPkgID[0];
			for(p = 0; p < hPkgID.length; p++){
				if (hCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = hPkgID[p];
					break;
				}
			}
			
			if (document.getElementById('aHr'))
			document.getElementById('aHr').className = "hour bw";
			if (document.getElementById('aDl'))
			document.getElementById('aDl').className = "hour";
			if (document.getElementById('aWk'))
			document.getElementById('aWk').className = "hour";
			if (document.getElementById('aMt'))
			document.getElementById('aMt').className = "hour";
			if (document.getElementById('mycarousel-1'))
			document.getElementById('mycarousel-1').style.display = "none";
			if (document.getElementById('mycarousel'))
			document.getElementById('mycarousel').style.display = "block";
			if (document.getElementById('mycarousel-w'))
			document.getElementById('mycarousel-w').style.display = "none";
			if (document.getElementById('mycarousel-m'))
			document.getElementById('mycarousel-m').style.display = "none";
			if (document.getElementById('mycarousel')){
				jQuery('#mycarousel').jcarousel({
					start: parseInt(eval(iCount + 1))
				});
			}
		} else if (md == "Weekly") {
			iPkgID = wPkgID[0];
			for(p = 0; p < wPkgID.length; p++){
				if (wCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = wPkgID[p];
					break;
				}
			}
			
			if (document.getElementById('aHr'))
			document.getElementById('aHr').className = "hour";
			if (document.getElementById('aDl'))
			document.getElementById('aDl').className = "hour";
			if (document.getElementById('aWk'))
			document.getElementById('aWk').className = "hour bw";
			if (document.getElementById('aMt'))
			document.getElementById('aMt').className = "hour";
			if (document.getElementById('mycarousel-1'))
			document.getElementById('mycarousel-1').style.display = "none";
			if (document.getElementById('mycarousel'))
			document.getElementById('mycarousel').style.display = "none";
			if (document.getElementById('mycarousel-w'))
			document.getElementById('mycarousel-w').style.display = "block";
			if (document.getElementById('mycarousel-m'))
			document.getElementById('mycarousel-m').style.display = "none";
			if (document.getElementById('mycarousel-w')){
				jQuery('#mycarousel-w').jcarousel({
					start: parseInt(eval(iCount + 1))
				});
			}
		} else if (md == "Monthly") {
			iPkgID = mPkgID[0];
			for(p = 0; p < mPkgID.length; p++){
				if (mCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = mPkgID[p];
					break;
				}
			}
			
			if (document.getElementById('aHr'))
			document.getElementById('aHr').className = "hour";
			if (document.getElementById('aDl'))
			document.getElementById('aDl').className = "hour";
			if (document.getElementById('aWk'))
			document.getElementById('aWk').className = "hour";
			if (document.getElementById('aMt'))
			document.getElementById('aMt').className = "hour bw";
			if (document.getElementById('mycarousel-1'))
			document.getElementById('mycarousel-1').style.display = "none";
			if (document.getElementById('mycarousel'))
			document.getElementById('mycarousel').style.display = "none";
			if (document.getElementById('mycarousel-w'))
			document.getElementById('mycarousel-w').style.display = "none";
			if (document.getElementById('mycarousel-m'))
			document.getElementById('mycarousel-m').style.display = "block";
			if (document.getElementById('mycarousel-m')){
				jQuery('#mycarousel-m').jcarousel({
					start: parseInt(eval(iCount + 1))
				});
			}
		} else {
			iPkgID = dPkgID[0];
			for(p = 0; p < dPkgID.length; p++){
				if (dCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = dPkgID[p];
					break;
				}
			}
			if (document.getElementById('aHr'))
			document.getElementById('aHr').className = "hour";
			if (document.getElementById('aDl'))
			document.getElementById('aDl').className = "hour bw";
			if (document.getElementById('aWk'))
			document.getElementById('aWk').className = "hour";
			if (document.getElementById('aMt'))
			document.getElementById('aMt').className = "hour";
			if (document.getElementById('mycarousel-1'))
			document.getElementById('mycarousel-1').style.display = "block";
			if (document.getElementById('mycarousel'))
			document.getElementById('mycarousel').style.display = "none";
			if (document.getElementById('mycarousel-w'))
			document.getElementById('mycarousel-w').style.display = "none";
			if (document.getElementById('mycarousel-m'))
			document.getElementById('mycarousel-m').style.display = "none";
			if (document.getElementById('mycarousel-1')){
				jQuery('#mycarousel-1').jcarousel({
					start: parseInt(eval(iCount + 1))
				});
			}
		}
		_sePrice();
	}
	_sePrice = function(){
		if (mode == "Hourly") {
			<?php
				if(count($pkgs) > 0){
					for($pk = 0; $pk < count($pkgs); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgs[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
					}
				}
			?>
			//document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[iCount] + '1.png';
			document.getElementById('li-' + iPkgID).className = 'new-select';
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i>' + formatNumber(hPkgRate[iCount]) + '/hr. <span style="font-size:12px;font-weight:bold;">(Fuel Included)</span>';
			document.getElementById('carbrandline').innerHTML = hCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(hPkgRate[iCount]) + " / hr.<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + hPkgRate[iCount] + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((hPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(hPkgRate[iCount] + (hPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(hSecDeposit[iCount]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else if (mode == "Weekly") {
			<?php
				if(count($pkgsWeekly) > 0){
					for($pk = 0; $pk < count($pkgsWeekly); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
					}
				}
			?>
			//document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + wCarImg[iCount] + '1.png';
			document.getElementById('li-' + iPkgID).className = 'new-select';
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i>' + formatNumber(wPkgRate[iCount]) + '/ week';
			document.getElementById('carbrandline').innerHTML = wCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(wPkgRate[iCount]) + " / week<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(wPkgRate[iCount]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((wPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(wPkgRate[iCount] + (wPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(wSecDeposit[iCount]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else if (mode == "Monthly") {
			<?php
				if(count($pkgsMonthly) > 0){
					for($pk = 0; $pk < count($pkgsMonthly); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
					}
				}
			?>
			//document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + mCarImg[iCount] + '1.png';
			document.getElementById('li-' + iPkgID).className = 'new-select';
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(mPkgRate[iCount]) + '/ month';
			document.getElementById('carbrandline').innerHTML = mCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(mPkgRate[iCount]) + " / month<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(mPkgRate[iCount]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((mPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(mPkgRate[iCount] + (mPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(mSecDeposit[iCount]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else {
			<?php
				for($pk = 0; $pk < count($pkgsDaily); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
				}
			?>
			//document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[iCount] + '1.png';
			document.getElementById('li-' + iPkgID).className = 'new-select';
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(dPkgRate[iCount]) + '/day</span>';
			document.getElementById('carbrandline').innerHTML = dCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(dPkgRate[iCount]) + " / Day</span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(dPkgRate[iCount]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i>" + parseInt(eval((dPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(dPkgRate[iCount] + (dPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(hSecDeposit[iCount]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
	};
	<?php
		if(count($pkgs) > 0){
			for($pk = 0; $pk < count($pkgs); $pk++){
	?>
			hPkgID.push('<?php echo $pkgs[$pk]["PkgID"]; ?>');
			hCarImg.push('<?php echo trim(trim(strtolower($pkgs[$pk]["PkgName"]))); ?>');
			hCarName.push('<?php echo $pkgs[$pk]["PkgName"]; ?>');
			hCarDec.push('<?php echo $pkgs[$pk]["PkgDesc"]; ?>');
			hPkgRate.push(<?php echo intval($pkgs[$pk]["PkgRate"]); ?>);
			hSecDeposit.push('<?php echo intval($pkgs[$pk]["DepositAmt"]); ?>');
	<?php
			}
		} else {
			$isHrAvail = false;
	?>
		mode = "Daily";
	<?php
		}
		if(count($pkgsWeekly) > 0){
			for($pk = 0; $pk < count($pkgsWeekly); $pk++){
	?>
			wPkgID.push('<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>');
			wCarImg.push('<?php echo trim(strtolower($pkgsWeekly[$pk]["PkgName"])); ?>');
			wCarName.push('<?php echo $pkgsWeekly[$pk]["PkgName"]; ?>');
			wCarDec.push('<?php echo $pkgsWeekly[$pk]["PkgDesc"]; ?>');
			wPkgRate.push(<?php echo intval($pkgsWeekly[$pk]["PkgRate"]); ?>);
			wSecDeposit.push('<?php echo intval($pkgsWeekly[$pk]["DepositAmt"]); ?>');
	<?php
			}
		} else {
			$isWeekAvail = false;
		}
		if(count($pkgsMonthly) > 0){
			for($pk = 0; $pk < count($pkgsMonthly); $pk++){
	?>
			mPkgID.push('<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>');
			mCarImg.push('<?php echo trim(strtolower($pkgsMonthly[$pk]["PkgName"])); ?>');
			mCarName.push('<?php echo $pkgsMonthly[$pk]["PkgName"]; ?>');
			mCarDec.push('<?php echo $pkgsMonthly[$pk]["PkgDesc"]; ?>');
			mPkgRate.push(<?php echo intval($pkgsMonthly[$pk]["PkgRate"]); ?>);
			mSecDeposit.push('<?php echo intval($pkgsMonthly[$pk]["DepositAmt"]); ?>');
	<?php
			}
		} else {
			$isMonthAvail = false;
		}
		if(count($pkgsDaily) > 0) {
			for($pk = 0; $pk < count($pkgsDaily); $pk++){
	?>
			dPkgID.push('<?php echo $pkgsDaily[$pk]["PkgID"]; ?>');
			dCarImg.push('<?php echo trim(trim(strtolower($pkgsDaily[$pk]["PkgName"]))); ?>');
			dCarName.push('<?php echo $pkgsDaily[$pk]["PkgName"]; ?>');
			dCarDec.push('<?php echo $pkgsDaily[$pk]["PkgDesc"]; ?>');
			dPkgRate.push(<?php echo intval($pkgsDaily[$pk]["PkgRate"]); ?>);
			dSecDeposit.push('<?php echo intval($pkgsDaily[$pk]["DepositAmt"]); ?>');
	<?php
			}
		}
	?>
	_changeFrame = function(id)
	{
		var idx = 0;
		iPkgID = id;
		if (mode == "Hourly") {
			for(p = 0; p < hPkgID.length; p++){
				if (hPkgID[p] == id) {
					idx = p;
					iPkgName = hCarName[p];
					break;
				}
			}
		} else if (mode == "Weekly") {
			for(p = 0; p < wPkgID.length; p++){
				if (wPkgID[p] == id) {
					idx = p;
					iPkgName = wCarName[p];
					break;
				}
			}
		} else if (mode == "Monthly") {
			for(p = 0; p < mPkgID.length; p++){
				if (mPkgID[p] == id) {
					idx = p;
					iPkgName = mCarName[p];
					break;
				}
			}
		} else {
			for(p = 0; p < dPkgID.length; p++){
				if (dPkgID[p] == id) {
					idx = p;
					iPkgName = dCarName[p];
					break;
				}
			}
		}
		if (mode == "Hourly") {
			<?php
				for($pk = 0; $pk < count($pkgs); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgs[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[idx] + '.png';
			//document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[idx] + '1.png';
			document.getElementById('li-' + id).className = 'new-select';
			document.getElementById('carbrandname').innerHTML = hCarName[idx];
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(hPkgRate[idx]) + '/ hr. <span style="font-size:12px;font-weight:bold;">(Fuel Included)</span>';
			document.getElementById('carbrandline').innerHTML = hCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i>" + formatNumber(hPkgRate[idx]) + " / hr.<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(hPkgRate[idx]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((hPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(hPkgRate[idx] + (hPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + hSecDeposit[idx] + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else if (mode == "Weekly") {
			<?php
				for($pk = 0; $pk < count($pkgsWeekly); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + wCarImg[idx] + '.png';
			//document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + wCarImg[idx] + '1.png';
			document.getElementById('li-' + id).className = 'new-select';
			document.getElementById('carbrandname').innerHTML = wCarName[idx];
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(wPkgRate[idx]) + '/ week';
			document.getElementById('carbrandline').innerHTML = wCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(wPkgRate[idx]) + " / week<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(wPkgRate[idx]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((wPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(wPkgRate[idx] + (wPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(wSecDeposit[idx]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else if (mode == "Monthly") {
			<?php
				for($pk = 0; $pk < count($pkgsMonthly); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + mCarImg[idx] + '.png';
			//document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + mCarImg[idx] + '1.png';
			document.getElementById('li-' + id).className = 'new-select';
			document.getElementById('carbrandname').innerHTML = mCarName[idx];
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(mPkgRate[idx]) + '/ month';
			document.getElementById('carbrandline').innerHTML = mCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(mPkgRate[idx]) + " / month<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(mPkgRate[idx]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((mPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(mPkgRate[idx] + (mPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(mSecDeposit[idx]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else {
			<?php
				for($pk = 0; $pk < count($pkgsDaily); $pk++){
			?>
				document.getElementById('li-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>').className = 'new-un-select';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '.png';
			//document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '1.png';
			document.getElementById('li-' + id).className = 'new-select';
			document.getElementById('carbrandname').innerHTML = dCarName[idx];
			document.getElementById('carbrandcost').innerHTML = '<i class="fa fa-inr"></i> ' + formatNumber(dPkgRate[idx]) + '/day';
			document.getElementById('carbrandline').innerHTML = dCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class='pophold'><span class='headliners'>Rate (<?php echo ucwords($ctnm); ?>)</span><br /><span class='carrate'><i class='fa fa-inr'></i> " + formatNumber(dPkgRate[idx]) + " / hr.<span style='font-size:12px;font-weight:bold;'>(Fuel Included)</span></span><br /><span class='headliners'>Fare Details</span><br /><ul><li>Minimum Billing: <i class='fa fa-inr'></i> " + formatNumber(dPkgRate[idx]) + "</li><li>VAT (@12.5%): <i class='fa fa-inr'></i> " + parseInt(eval((dPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: <i class='fa fa-inr'></i> " + parseInt(eval(dPkgRate[idx] + (dPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): <i class='fa fa-inr'></i> " + formatNumber(dSecDeposit[idx]) + " (Mastercard and Visa only)</li></ul><span class='headliners'>Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		
	}
	//]]>
</script>

<script type="text/javascript">
//<![CDATA[
_frmClearPP = function(){

    document.getElementById("txtNamePP").value = "Name";
    document.getElementById("txtContactNoPP").value = "Mobile No";
    //document.getElementById("txtEmailPP").value = "Emal Id";
	document.getElementById("txtQuery").value = "Query";
};
_frmDisablePP = function(t){
    document.getElementById("txtNamePP").disabled = t;
    document.getElementById("txtContactNoPP").disabled = t;
    //document.getElementById("txtEmailPP").disabled = t;
	document.getElementById("txtQuery").value = t;
};
function _validateMyles()
{
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtNamePP"), "Name", "Please fill in your full name.","ppresponse");
    if(chk)
	chk = isFilledText(document.getElementById("txtContactNoPP"), "Mobile No", "Please fill in your mobile number.","ppresponse");
//    if(chk)
//	chk = isFilledText(document.getElementById("txtEmailPP"), "Email Id", "Please fill in your email id.","ppresponse");
//    if(chk)
//	chk = isEmailAddr(document.getElementById("txtEmailPP"), "Please fill in a valid email id.","ppresponse");
    if(chk)
      chk = isFilledText(document.getElementById("txtQuery"), "Query", "Please fill in your query.","ppresponse");
    if (chk) {
	var ajx;
	var strParam;
	if (window.XMLHttpRequest)
	    ajx = new XMLHttpRequest;
	else if (window.ActiveXObject)
	    ajx = new ActiveXObject("Microsoft.XMLHTTP");
	 strParam = "txtname=" + document.getElementById("txtNamePP").value + "&email=NA&monumber=" + document.getElementById("txtContactNoPP").value + "&query=" + document.getElementById("txtQuery").value + "&curr_url=" + escape(document.getElementById("hdCURL").value);
	 
	ajx.onreadystatechange = function()
	{
	    if (ajx.readyState < 4){
	    
		document.getElementById('ppresponse').innerHTML = "<b style='color:#DB4A28;'>Please Wait...</b>";
	    }
	    else if (ajx.readyState == 4) {
		var r = "";
		if(ajx.status == 200){
		    var url = webRoot + "/myles-thanks.php?op=" + document.getElementById("txtContactNoPP").value;
		    _frmDisablePP(false);
		    _frmClearPP();
		    window.location = url;
		    //document.getElementById("ppresponse").innerHTML = "<b style='color:#DB4A28;'>Thank you for your valued enquiry, will get back to you shortly</b>";
		}
	    }
	};
	ajx.open("POST", webRoot + "/myles-enquiry.php", true);
	ajx.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");        
	ajx.send(strParam);
    }   
};
wx = 170;
function _moveScreen() {
  if(wx <= 600){
    wx += 3;
    window.scrollTo(0, wx);
    setTimeout("_moveScreen()", wx * 0.00012);
  }
}
//]]>
</script>

	<style type="text/css">
		.white
		{background-color:#fff !important;}
		.nivo-controlNav
		{
			left:704px !important;
		}
		.car-heading {
		background-image: url("<?php echo corWebRoot; ?>/myles-campaign/images/heading-header-2-small.gif");
		cursor: pointer;
		float: left;
		height: 66px;
		margin: 0 0px;
		width: 904px;
	}
	.show-car {
    background-color: #e7e7e8;
    float: left;
    margin: 0 0 20px 0px;
    padding: 0;
    width: 904px;
}
.car-container {
    background-color: #fff;
    float: left;
    margin-left: 56px;
    padding: 0;
    width: 904px;
}
	  #registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.png") no-repeat scroll 0 0 transparent !important;}
	  #registration{border: 10px solid #db4626 !important;}
#mycarousel, #mycarousel-1, #mycarousel-w, #mycarousel-m
{
	overflow:hidden !important;
}
#mycarousel li, #mycarousel-1 li, #mycarousel-w li, #mycarousel-m li{
	list-style: none outside none;
	width:105px;
	margin-right:10px;
}
.jcarousel-skin-tango .jcarousel-container {
	float:left;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-direction-rtl {
	direction: rtl;
}

.jcarousel-skin-tango .jcarousel-container-horizontal {
    width:  828px;
    padding: 0px 20px;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-clip {
    overflow: hidden;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width:  800px;
	margin-left:17px;	
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-item {
    width: 105px;
}

.jcarousel-skin-tango .jcarousel-item-horizontal {
	margin-left: 0;
    /*margin-right: 10px;*/
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-item-horizontal {
	margin-left: 13px;
    margin-right: 0;
}

.jcarousel-skin-tango .jcarousel-item-placeholder {
    background: #fff;
    color: #000;
}

/**
 *  Horizontal Buttons
 */
.jcarousel-skin-tango .jcarousel-next-horizontal {
    position: absolute;
    top: 0px;
    right: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png) no-repeat;
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-next-horizontal {
    left: 8px;
    right: auto;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:focus {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:active {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:active {
    cursor: default;
    background-position: -40px 0;
	background-color:transparent;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal {
    position: absolute;
    top: 0px;
    left: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-left.png) no-repeat -40px 0;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-prev-horizontal {
    left: auto;
    right: 15px;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:focus {
    background-position: -0px 0;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:active {
    background-position: -40px 0;
}

.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:active {
    cursor: default;
    background-position: 0px 0;
	width:20px;
}
.AdminText{
/*padding:15px;
border:1px solid #E5E5E5;
margin-bottom:20px;
width:980px;*/
margin-bottom:20px;
width:890px;
margin-left:53px;
}
.new-select {
    background-color: #c7c7c7;
    border: 5px solid #c7c7c7;
    padding: 8px 5px;
    text-align: center;
}
.new-un-select {
    background-color: #fff;
    border: 5px solid #d4d4d4;
    padding: 8px 5px;
    text-align: center;
}
#mycarousel-m.jcarousel-list-horizontal{
left:0px;
}
	</style>
</head>

<body class="white">
<?php include_once("./includes/header-with-service.php"); ?>
<script  type="text/javascript" src="//dtxnk7lg3r1lt.cloudfront.net/tt-fbe10beedf9d29cf53137ba38859ffd1dbe7642cedb7ef0a102a3ab109b47842.js" async>
</script> 
<div id="main-container" class="white myles_bannerW">
	<div class="inner-container" style="float:left;margin-left:33px;">
    	<div class="myles-logo"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/myles.png" width="204" height="53" alt="Myles" title="Myles"/></div>
		<div style="float:right;margin:28px 15px 0px 0px;"><!--<span class="callnow"><span style="color:#333;font-size:12px;">Call 24x7</span> <strong style="font-family: 'HelveticaLTStd';">0888 222 2222</strong></span>--></div>
    </div>
    <div class="banner">
    	 <div id="slider" class="nivoSlider" style="float:left !important; margin:19px !important;height:420px;">
				
				
				<!--<a href="<?php //echo corWebRoot; ?>/gosf" target="_blank"><img src="<?php //echo corWebRoot; ?>/myles-campaign/images/mylesgosfpage.jpg" alt="" title="" /></a>-->
				<!--<img src="<?php echo corWebRoot; ?>/myles-campaign/images/141118_COR_Myles_bner1_daily2.jpg" alt="" title="" />-->
				
				
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/150123-myles-banner.jpg" width="950" height="420" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/141118_COR_Myles_bner1_daily.jpg" width="950" height="420" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/year2014-06-1.jpg" width="950" height="420" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/year2014-06-2.jpg" width="950" height="420" alt="" title="" />
				<!--<a href="<?php echo corWebRoot; ?>/ladakh/" target="_blank"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/140728_Myles_website_Ladakh_Banner.jpg" alt="" title="" /></a>-->
               
				
			
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/year2014-06-5.jpg" width="950" height="420" alt="" title="" />
            </div>
    </div>
    
	<div class="car-container" id="range-of-cars">
			<div class="car-heading" onclick="javascript: _moveScreen();">
            	<div class="text">Pick from a wide range of cars</div>
                <a class="know-more" href="javascript: void(0)" id="bkn">Available in 20 cities</a>
            </div>
            <div class="show-car">
            	<div class="car-img">
		<?php
			if($isHrAvail){
		    ?>
			<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[0]["PkgName"]); ?>.png" id="carbrandimage" width="" height="" alt="" title=""/>
		    <?php
			} else {
		    ?>
			<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[0]["PkgName"]); ?>.png" id="carbrandimage" width="" height="" alt="" title=""/>
		    <?php
			}
		    ?>	
		</div>
		<div class="holddata">
                <div class="holdall">
			<?php
			    if($isHrAvail){
			?>
				<span id="carbrandname"><?php echo $pkgs[0]["PkgName"]; ?></span>
				<span id="carbrandcost" style="width:400px !important;"><i class="fa fa-inr"></i> <?php echo number_format(intval($pkgs[0]["PkgRate"])); ?>/hr. <span style="font-size:12px;font-weight:bold;">(Fuel Included)</span></span>
				<span id="carbrandline"><?php echo $pkgs[0]["PkgDesc"]; ?></span>
				<span class="smdetails">
				<div id="item_1" class="item">
				Details +
				<div class="tooltip_description" style="display:none" id="popupFDetails">
					<div class="pophold">
						<span class="headliners">Rate (<?php echo $ctnm; ?>)</span><br />
						<span class="carrate"><i class="fa fa-inr"></i> <?php echo number_format($pkgs[0]["PkgRate"]); ?> / hr. <span style="font-size:12px;">(Fuel Included)</span></span><br />
						<span class="headliners">Fare Details</span><br />
						<ul>
							<li>Minimum Billing:<i class="fa fa-inr"></i><?php echo intval($pkgs[0]["PkgRate"]); ?></li>
							<li>VAT (@12.5%):<i class="fa fa-inr"></i> <?php echo intval(($pkgs[0]["PkgRate"] * 12.5) / 100); ?></li>
							<li>Total Fare: <i class="fa fa-inr"></i> <?php echo ($pkgs[0]["PkgRate"] + intval(($pkgs[0]["PkgRate"] * 12.5) / 100)); ?></li>
							<li>Refundable Security Deposit (Pre Auth from card):<i class="fa fa-inr"></i> <?php echo $pkgs[0]["DepositAmt"]; ?> (Mastercard and Visa only)</li>
						</ul>
						<span class="headliners">Mandatory Documents</span><br />
						<ul>
							<li>Passport/ Voter ID Card</li>
							<li>Driving License</li>
							<li>Credit Card</li>
						</ul>
					    </div>
				    </div>
				  </div>
				</span>
			<?php
			    } else {
			?>
				<span id="carbrandname"><?php echo $pkgsDaily[0]["PkgName"]; ?></span>
				<span id="carbrandcost" style="width:400px !important;"><i class="fa fa-inr"></i> <?php echo number_format(intval($pkgsDaily[0]["PkgRate"])); ?>/day</span><br />
				<span id="carbrandline"><?php echo $pkgsDaily[0]["PkgDesc"]; ?></span>
				<span class="smdetails">
				<div id="item_1" class="item">
				Details +
				<div class="tooltip_description" style="display:none" id="popupFDetails">
					<div class="pophold">
						<span class="headliners">Rate (<?php echo $ctnm; ?>)</span><br />
						<span class="carrate"><i class="fa fa-inr"></i><?php echo number_format($pkgsDaily[0]["PkgRate"]); ?> / day <span style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><br />
						<span class="headliners">Fare Details</span><br />
						<ul>
							<li>Minimum Billing: <i class="fa fa-inr"></i> <?php echo intval($pkgsDaily[0]["PkgRate"]); ?></li>
							<li>VAT (@12.5%): <i class="fa fa-inr"></i><?php echo intval(($pkgsDaily[0]["PkgRate"] * 12.5) / 100); ?></li>
							<li>Total Fare: <i class="fa fa-inr"></i><?php echo ($pkgsDaily[0]["PkgRate"] + intval(($pkgsDaily[0]["PkgRate"] * 12.5) / 100)); ?></li>
							<li>Refundable Security Deposit (Pre Auth from card): Rs <?php echo $pkgsDaily[0]["DepositAmt"]; ?> (Mastercard and Visa only)</li>
						</ul>
						<span class="headliners">Mandatory Documents</span><br />
						<ul>
							<li>Passport/ Voter ID Card</li>
							<li>Driving License</li>
							<li>Credit Card</li>
						</ul>
					    </div>
				    </div>
				  </div>
				</span>
			<?php
			    }
			?>	
		</div>
		
		
		<div style="float: left;width:90%;margin-top:5px;margin-left:3%;">
								<span class="fl-left" style="margin: 4px 5px 0 0;">Select Rental:</span>
								<div class="time-div mylesselected">
									<ul>
									
									<?php
										if($isHrAvail){
								?>
											<li><a href="javascript: void(0);" id="aHr" onclick="javascript: _setMode('Hourly');" class="hour bw">Hourly</a></li>
								<?php
										}
										if(count($pkgsDaily) > 0){
								?>
											<li><a href="javascript: void(0);" id="aDl" onclick="javascript: _setMode('Daily');" class="hour <?php if(!$isHrAvail){ echo "bw"; } ?>">Daily</a></li>
								<?php
										}
								?>
									</ul>
									<ul>
<?php
									if($isWeekAvail){
?>
										<li><a href="javascript: void(0);" id="aWk" onclick="javascript: _setMode('Weekly');" class="hour">Weekly</a></li>
<?php
									} 
									if($isMonthAvail){
?>
										<li><a href="javascript: void(0);" id="aMt" onclick="javascript: _setMode('Monthly');" class="hour">Monthly</a></li>
<?php
									} 
?>
									</ul>
								</div>
							</div>
               <div class="holdallbottom">
			<!--<span class="avin">Select Rental Type:-</span>-->
			<!--<ul>
		<?php
			if($isHrAvail){
		?>
				<li><a href="javascript: void(0);" id="aHr" onclick="javascript: _setMode('Hourly');" class="selected">Hourly</a></li>
		<?php
			}
			if(count($pkgsDaily) > 0){
		?>
				<li><a href="javascript: void(0);" id="aDl" onclick="javascript: _setMode('Daily');" class="<?php if(!$isHrAvail){ echo "selected"; } ?>">Daily</a></li>
		<?php
			}
			if($isWeekAvail){
		?>
				<li><a href="javascript: void(0);" id="aWk" onclick="javascript: _setMode('Weekly');">Weekly</a></li>
		<?php
			} 
			if($isMonthAvail){
		?>
				<li><a href="javascript: void(0);" id="aMt" onclick="javascript: _setMode('Monthly');">Monthly</a></li>
		<?php
			}
		?>
			</ul>-->
					<span class="avin">Select Your City:-</span>
					<div class="linkbar">
					<?php
						if($ctid == 2){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/delhi/#range-of-cars" class="first selected">DELHI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/delhi/#range-of-cars" class="first">DELHI</a><span class="spanner">|</span>
					<?php
						}
						if($ctid == 4){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mumbai/#range-of-cars" class="selected">MUMBAI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mumbai/#range-of-cars">MUMBAI</a><span class="spanner">|</span>
					<?php
						}
						if($ctid == 7){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/bangalore/#range-of-cars" class="selected">BANGALORE</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/bangalore/#range-of-cars">BANGALORE</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 6){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/hyderabad/#range-of-cars" class="selected">HYDERABAD</a>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/hyderabad/#range-of-cars">HYDERABAD</a>
					<?php
						}
					?>
					<?php
						if($ctid == 69){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/goa/#range-of-cars" class="first selected">GOA</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/goa/#range-of-cars" class="first">GOA</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 10){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/jaipur/#range-of-cars" class="selected">JAIPUR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/jaipur/#range-of-cars">JAIPUR</a><span class="spanner">|</span>
					<?php
						}
					?>	
					<!--<div style="clear: both"></div>-->
					
					<?php
						if($ctid == 8){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/chennai/#range-of-cars" class="selected">CHENNAI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/chennai/#range-of-cars">CHENNAI</a><span class="spanner">|</span>
					<?php
						}
					?>
					<!--<div style="clear: both"></div>-->
					<?php
						if($ctid == 5){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/pune/#range-of-cars" class="selected">PUNE</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/pune/#range-of-cars">PUNE</a><span class="spanner">|</span>
					<?php
						}
					?>	
					<?php
						if($ctid == 1){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/ahmedabad/#range-of-cars" class="selected">AHMEDABAD</a>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/ahmedabad/#range-of-cars">AHMEDABAD</a>
					<?php
						}
					?>	
					<?php
						if($ctid == 9){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/chandigarh/#range-of-cars" class="first selected">CHANDIGARH</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/chandigarh/#range-of-cars" class="first">CHANDIGARH</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 49){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/visakhapatnam/#range-of-cars" class="selected">VISAKHAPATNAM</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/visakhapatnam/#range-of-cars">VISAKHAPATNAM</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 43){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/amritsar/#range-of-cars" class="selected">AMRITSAR</a> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/amritsar/#range-of-cars">AMRITSAR</a> 
					<?php
						}
					?>
					<?php
						if($ctid == 39){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/manesar/#range-of-cars" class="first selected">MANESAR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/manesar/#range-of-cars" class="first">MANESAR</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 41){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/bhubaneswar/#range-of-cars" class="selected">BHUBANESWAR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/bhubaneswar/#range-of-cars">BHUBANESWAR</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 11){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/noida/#range-of-cars" class="selected">NOIDA</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/noida/#range-of-cars">NOIDA</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 3){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/gurgaon/#range-of-cars" class="selected">GURGAON</a> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/gurgaon/#range-of-cars">GURGAON</a>
					<?php
						}
					?>
					<?php
						if($ctid == 80){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mangalore/#range-of-cars" class="first selected">MANGALORE</a><span class="spanner">|</span> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mangalore/#range-of-cars" class="first">MANGALORE</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($ctid == 81){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mysore/#range-of-cars" class="selected">MYSORE</a><span class="spanner">|</span> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/mysore/#range-of-cars">MYSORE</a><span class="spanner">|</span> 
					<?php
						}
					?>
					<?php
						if($ctid == 44){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/udaipur/#range-of-cars" class="selected">UDAIPUR</a> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/udaipur/#range-of-cars">UDAIPUR</a><span class="spanner">|</span>
					<?php
						}
					?>
					
					<?php
						if($ctid == 82){
					?>
						<a href="<?php echo corWebRoot; ?>/myles/ghaziabad/#range-of-cars" class="selected">GHAZIABAD</a> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/myles/ghaziabad/#range-of-cars">GHAZIABAD</a> 
					<?php
						}
					?>
					
					
					</div>
					<div  class="clearfix"></div>
                    <a href="<?php echo corWebRoot; ?>/#selfdrive" class="bkn" target="_blank"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/booknow.png" alt="Book Now" title="Book Now"/></a>
               </div>
	    </div>
                 <div class="holdallform">
		    <p class="pp1">Assured 5 mins call back<br /><small>09:00AM - 09:00PM</small></p>
		    <p class="pp2" id="selectedpackage"></p>
		    <input type="hidden" id="hdPackagePP" name="hdPackagePP" value="" />
		    <input type="hidden" id="hdCURL" name="hdCURL" value="<?php //echo urlencode($_SERVER['REQUEST_URI']); ?>" />
		    <p class="prow"><input type="text" id="txtNamePP" name="txtNamePP" class="txtbox" value="Name" onblur="javascript: if(this.value==''){this.value='Name';}" onfocus="javascript: if(this.value=='Name'){this.value='';}"/></p>
		    <p class="prow"><input type="text" id="txtContactNoPP" name="txtContactNoPP" class="txtbox" value="Mobile Number" onblur="javascript: if(this.value==''){this.value='Mobile Number';}" onfocus="javascript: if(this.value=='Mobile Number'){this.value='';}" onkeypress="javascript: return _allowNumeric(event);" maxlength="10" /></p>
		    <p class="prow"><textarea type="textarea" id="txtQuery" name="txtQuery" class="tarea" cols="60" rows="5" value="Query" onblur="javascript: if(this.value==''){this.value='Query';}" onfocus="javascript: if(this.value=='Query'){this.value='';}">Query</textarea></p>
		    <p class="prow"><span class="mf"></span><input type="image" src="<?php //echo corWebRoot; ?>/myles-campaign/images/submit-2.jpg" id="btnSubmitPP" class="btnsumyles" name="btnSubmitPP" onclick="_validateMyles();"/></p>
		    <p class="prow mf" id="ppresponse"></p>
		 </div>
                <div  class="clearfix"></div>
				<!-- <img src="<?php //echo corWebRoot; ?>/myles-campaign/images/banner.png" /> -->
				<div style="float:left;width:950px;margin:25px 0px 20px 15px">
		<?php
			if(count($pkgs) > 0 && $isHrAvail){
		?>
				<ul id="mycarousel" class="jcarousel-skin-tango">
				<?php
					for($pk = 0; $pk < count($pkgs); $pk++){
						if($pk == 0){
				?>
						<li class="new-select" id="li-<?php echo $pkgs[$pk]["PkgID"]; ?>"><img alt="Myles Hourly Cars" title="Myles Hourly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower($pkgs[$pk]["PkgName"])); ?>.png" id="cars-<?php echo $pkgs[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgs[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgs[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						} else {
				?>
						<li class="new-un-select" id="li-<?php echo $pkgs[$pk]["PkgID"]; ?>"><img alt="Myles Hourly Cars" title="Myles Hourly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower($pkgs[$pk]["PkgName"])); ?>.png" id="cars-<?php echo $pkgs[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgs[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgs[$pk]["PkgID"]; ?>)" width="95" height="50" /></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
			if(count($pkgsDaily) > 0) {
				if($isHrAvail){
		?>
				<ul id="mycarousel-1" class="jcarousel-skin-tango" style="display: none;">
		<?php
			    } else {
		?>
				<ul id="mycarousel-1" class="jcarousel-skin-tango">
		<?php
			    }
					for($pk = 0; $pk < count($pkgsDaily); $pk++){
						if($pk == 0){
				?>
						<li class="new-select" id="li-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>"><img alt="Myles Daily Cars" title="Myles Daily Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower($pkgsDaily[$pk]["PkgName"])); ?>.png" id="cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						} else {
				?>
						<li class="new-un-select" id="li-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>"><img alt="Myles Daily Cars" title="Myles Daily Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower($pkgsDaily[$pk]["PkgName"])); ?>.png" id="cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
			
			if(count($pkgsWeekly) > 0) {
		?>
				<ul id="mycarousel-w" class="jcarousel-skin-tango" style="display: none;">
		<?php
		
					for($pk = 0; $pk < count($pkgsWeekly); $pk++){
						if($pk == 0){
				?>
						<li class="new-select" id="li-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>"><img alt="Myles Weekly Cars" title="Myles Weekly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower(trim($pkgsWeekly[$pk]["PkgName"]))); ?>.png" id="cars-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						} else {
				?>
						<li class="new-un-select" id="li-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>"><img alt="Myles Weekly Cars" title="Myles Weekly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower(trim($pkgsWeekly[$pk]["PkgName"]))); ?>.png" id="cars-<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsWeekly[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
			if(count($pkgsMonthly) > 0) {
		?>
				<ul id="mycarousel-m" class="jcarousel-skin-tango" style="display: none;">
		<?php
		
					for($pk = 0; $pk < count($pkgsMonthly); $pk++){
						if($pk == 0){
				?>
						<li class="new-select" id="li-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>"><img alt="Myles Monthly Cars" title="Myles Monthly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower(trim($pkgsMonthly[$pk]["PkgName"]))); ?>.png" id="cars-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						} else {
				?>
						<li class="new-un-select" id="li-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>"><img alt="Myles Monthly Cars" title="Myles Monthly Cars" src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo trim(strtolower(trim($pkgsMonthly[$pk]["PkgName"]))); ?>.png" id="cars-<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsMonthly[$pk]["PkgID"]; ?>)" width="95" height="50"/></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
		?>
				</div>
            </div>
           
            
	</div>
<div class="clearfix"></div>


 
	  <?php  
	  
	   if($page_content_data!="")
	   { 
		?>
		<div class="" id='contentd' style="padding:15px;border:1px solid #E5E5E5;margin-bottom:20px;width:944px;margin-left:21px;">
		<?php

		echo $page_content_data;
		?>
		</div>
		<?php
		}
		else
		{
		?>
		<div class="" id='contentd'>
		</div>
		<?php
		}
		?>
	  


</div>
    <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

	<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.jcarousel.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		<?php
		    if($isHrAvail){
		?>
			jQuery('#mycarousel').jcarousel({
			    start: 1
			});
		<?php
		    } else {
		?>
			jQuery('#mycarousel-1').jcarousel({
			    start: 1
			});
		<?php
		    }
		?>
		});
	</script> 

	  
	  
	  <?php include_once("./includes/footer-for-myles.php"); ?>
	  
	  <!---code  for feedback -->
	  
	   	<?php
	if(basename($_SERVER['PHP_SELF']) != 'myles-feedback.php')
	{
	
	?>
	<a id="feedbackpopupRED" href="javascript: void(0);" onclick="javascript:_disableThisPage();_setDivPos('registration');_checkIsSelfDrive();">
	<img src="<?php echo corWebRoot; ?>/images/feedb_red.png" width="30" height="100" alt='feedback' border="0" />	
		<!--<span class="enqButton feedB">Feedback</span>-->
		</a>
	<?php
	  
	}
	?>
<script type="text/javascript"> 
//<![CDATA[
$(document).ready(function(){
    $("div#lyrics").hide();
    
    $("#songlist div").click(function(){
        $("#lyrics",this).show("slow");
		$(".close").show();
		
    });
	$(".close").click(function(){
		$("#lyrics").hide();
		$(this).hide();
	});
	/*.mouseout(function(){
        $("#lyrics",this).hide();
    }); */
    
});
function troggleopenmyles()
{
	var regexpchmyles = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
	var txtname=$("#txtname1").val();
	var txtnameval =document.getElementById("txtname1");
	var querytype = $( "#querytype option:selected" ).val();
	var email=$("#email1").val();
	var monumber=$("#monumber1").val();
	var query=$("#query1").val();
	if($("#txtname1").val().trim()=="")
	{
	alert("Please Enter Name");
	$("#txtname1").focus();
	return  false;
	}
	
	if(!txtnameval.value.match(regexpchmyles))
		{
		alert("Please Enter character only");
		$("#txtname1").focus(); 
		return false
		
		}
	
	if(monumber=="")
	{
	alert("Please Enter Mobile No");
	$("#monumber1").focus();
	return  false;
	}
	
	if(monumber.substr(0,1) != "9" && monumber.substr(0,1) != "8" && monumber.substr(0,1) != "7")
		{
			alert("Invalid mobile number. Please enter correct mobile number.");
			$("#monumber1").focus(); 
			return  false;
	    }
	if(monumber)	
	{
		if(monumber.length != 10)
		{
			alert("Mobile number must be of 10 digit.");
			$("#monumber1").focus(); 
			return  false;
		}
	}
	if(querytype=="0")
	{
		alert("Please Select Service");
		$("#querytype1").focus();
		return  false;
	}
	
	var dataString ='txtname='+txtname+'&email='+email+'&monumber='+monumber+'&query='+query+'&querytype='+querytype;;
	$.ajax({type:"POST", url: "<?php echo corWebRoot; ?>/myles-enquiry_feedback.php",
	data:dataString, success: function(response) {
	var txt=response;
	$('#loader4').hide();
	
	var result = txt;
	$("div#lyrics").hide();
	var data =1;
	if(result==1)
	{	
		$(".close").hide();
		alert("Thank you for your valued enquiry, will get back to you shortly");
	}
	
	}});
			 		
}
 //]]>
</script>
	 
	 
	 <!-- enquery form-->
	<div class="close">X</div>	
	<div id="songlist" class="mylesEnq">
	<div class="enquery">
	<img alt="Enquiry" title="Enquiry" class="enq enredbutton" src="<?php echo corWebRoot; ?>/images/enq_red.png" width="30" height="100" border="0"/>
	<!--<img class="enq" src="<?php echo corWebRoot; ?>/images/enq.jpg" border="0"/>-->
	<div id="lyrics" style="display:none;">
	<form name="from1" method="post" action="">
	<label for="name" class="mr2">Name <span class="starR">*</span> :</label>
	<input type="text" name="txtname" id='txtname1' value=""/>
	<label for="email" class="mr2">Emal ID :</label>
	<input type="text" name="email" id='email1' value=""/>
	<label for="mobile" class="mr2">Mobile <span class="starR">*</span> :</label>
	<input type="text" name="monumber" id='monumber1' maxlength="10" value=""/>
	
	<label for="querrytype" class="mr2"> Service <span class="starR">*</span> :</label>
	<select  name="querytype"  class="servicequery" id='querytype1'>
	<option value="0" selected='selected'>-Select Service-</option>
	<option value="Selfdrive">Selfdrive</option>
	<option value="Outstation">Outstation</option>
	<option value="Local">Local</option>
	</select>
	
	<div class="clr"> </div>
	<label for="query" class="mr2 query">Query :</label>
	<textarea name="query" rows="" cols="" id="query"></textarea>
	<label for="name" class="mr2 hidden">hidden</label>
	<img alt="Enquiry" title="Enquiry" src="<?php echo corWebRoot; ?>/myles-campaign/images/enqButton.png" border="0" onclick="return troggleopenmyles();"/>
	</form>
	</div>
	</div>
	</div>
	 <?php include_once("./includes/feedback-ladakh-popup.php");  ?>
	 
<div id="fb-root"></div>
<script type="text/javascript">
//<![CDATA[
(function(d, s, id) {
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) return;
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1439754936255803&version=v2.0";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
  //]]>
</script>


<!-- Google Tag Manager -COR-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSZV9B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script type="text/javascript">
//<![CDATA[
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSZV9B');
  //]]>
</script>
<!-- End Google Tag Manager -->

<!--<div class="fb-share-button" data-href="http://www.carzonrent.com/myles-campaign.php"></div>-->
	 
</body>


</html>

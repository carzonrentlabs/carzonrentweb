<html>
<head>
 <link rel="stylesheet" href="http://www.carzonrent.com/beta/applicationform/css/style.css">
 <script type="text/javascript" src="http://www.carzonrent.com/beta/applicationform/js/jquery.js"></script>
</head>



<body>
<!-- header   -->
<div id="header">
 
<div class="logo"><a href="http://www.carzonrent.com/" id="brand" class="corbrand"></a></div>
<div style='color: green;float: right;margin-top: 60px;'> </div>
</div>
<div class="clearfix"></div>
<!-- main ---->
<div class="container" >
    <!--<fieldset>
	<legend>Contact Form</legend> -->
	<div id="emailform" style="width:850px;margin:0px auto;">
		<form>
		 <div>
			<label class="label">Title</label>
			<select  name="title"><option>select type</option><option>Mr.</option><option>Mrs.</option><option>Ms.</option></select>
		 </div>
		 <div>
			<div class="name_set1"><label class="label">First Name</label><input type="text" name="fname" id="fname" placeholder="First Name" style="float:left;"></div>
			<!--<div class="name_set2"><label class="label" style="text-align:right">Middle name</label><input type="text" name="mname" id="mname" placeholder="Middle Name" style="float:left;" class="mleft"></div>-->
			<div class="name_set3"><label class="label" style="text-align:right">Last name</label><input type="text" name="lname" id="lname" placeholder="Last Name" style="float:left" class="mleft"></div>
			<div class="clearfix"></div>
		</div>
         <div>
			<label class="label">Age</label><select  name="age" id="age" placeholder="Age"><option>select Age</option><option>18-25</option><option>25-35</option><option>35-45</option> <option>50 above</option></select>
		 </div>
		 <fieldset id="addr_detail">
			<legend>Address</legend>
			<div id="address">
				<div style="width:390;float:left;"><label class="label">Address Line1</label><textarea   name="addr1" id="addr1" placeholder="address line 1" style="float:left" rows="3" cols="25"></textarea></div>
				<div style="width:390;float:left;"><label class="label">Address Line2</label><textarea   name="addr2" id="addr2" placeholder="address line 2"rows="3" cols="25" ></textarea></div>
			</div>
			<div class="clearfix"></div>
			<div id="city_detail">
				<div style="width:390;float:left;"><label class="label">City</label><input type="text" name="city" id="city" placeholder="city" style="float:left"> </div>
				<div style="width:390;float:left;"><label class="label" >Landmark</label><input type="text" name="landmark" id="landmark" placeholder="landmark"></div> 
			</div>
			<div class="clearfix"></div>
			<div id="city_detail1">
				<div style="width:390;float:left;"><label class="label">State</label><input type="text" name="state" id="state" placeholder="state" style="float:left"></div>
				<div style="width:390;float:left;"><label class="label"  >Pincode</label><input type="text" name="pin" id="pin" placeholder="pin"></div>
			</div>
			<div class="clearfix"></div>
			
			<div id="cell_info">
				<div style="width:390;float:left;"><label class="label">Landline</label><input type="text" name="landline" id="landline" placeholder="landline" style="float:left"></div>
				<div style="width:390;float:left;"><label class="label"  >Mobile</label><input type="text" name="mobile" id="moble" placeholder="mobile" style="float:left"></div>
			</div>
			<div class="clearfix"></div>
			<div><label class="label">Email</label><input type="text" name="email" id="email" placeholder="email" style="width: 230px;" ></div>
			
		 </fieldset>
		 <div>
			<div style="width:200;float:left;"><label class="label">Salaried/Selfemployed</label><input type="radio" name="check_b" id="salaried" value="0" style="float:left" class="mleft"></div>
			<div style="width:390;float:left;"><label class="label" style="width:65px;">Business</label><input type="radio" name="check_b" id="business" value="1" style="float:left" ></div>
			<div class="clearfix"></div>
			
		 </div>
		 <div id="b_detail" class="active">
		    <label class="title">Business Details</label>
			<div><label class="label">Name of business</label><input type="text" name="n_o_b" id="nob" placeholder="name of business"></div>
			<div ><label id="tow">Type of Ownership</label></div>
			<div style="width:200;float:left;">
				<label class="label">Partnership</label><input type="radio" name="b_ownership" id="partnership" value="0" style="float:left">
			</div>
			<div style="width:200;float:left;">
				<label class="label">Proprietorship</label><input type="radio" name="b_ownership" id="prop" value="1" style="float:left" >
			</div>
			<div class="clearfix"></div>
			<div style="float:left;padding:0px;" class="active" id="partnership_detail">
				<label class="label"  style="width:180px!important;">No. of Business Partners</label><input type="text" name="b_partner" id="b_partners" placeholder="business partners" style="float:left">
			</div>
			<div class="clearfix"></div>
			<div style="width:300px;float:left">
				<label class="label">Industry Type</label>
				<select name="indutry_type" id="i_type">
					<option value="1">select type</option>
					<option value="2">IT-Infrastructure</option>
					<option value="3">IT-Services</option>
					<option value="4">Media/Advertise</option>
					<option value="5">Automotive</option>
					<option value="6">Logistics</option>
					<option value="7">Manufacturing</option>
					<option value="7">Retail</option>
					<option value="8">Car Rental</option>
					<option value="9">Others</option>
				</select>
			</div>
			<div style="width:300px;float:left" class="active" id="o_type_name">
				<label class="label"  style="width:50px">Name</label><input type="text" name="o_type" id="o_type" placeholder="provide type" style="float:left">
			</div>
			<div class="clearfix"></div>
			<div id="address_office" ><div class="label">Addresss</div><textarea rows="2" cols="40" placeholder="Address"></textarea></div>
			
		 </div>
		
		 <div class="clearfix"></div>
		 
		 <!-- business details-->
		 <fieldset id="c_detail" class="active" >
			<label class="title">Car Details</label>
			<!--<div><label class='label1'>Are you currently in car rental Business</label><input type="checkbox" name="car_rental_b" id="car_rental_b" ></div>-->
			<div id="car_rental_business">
				<!--<div class="olc"><label class='label1' style="width:200px;">Is your office Owned/Leased</label><input type="checkbox" name="own_lease" id="own_lease">yes</div>-->
				
				<!--<div id="leased_detail" class="active olc"><label class='label1' style="width:110px;">No. of Location</label><select  name="no_of_location" id="no_of_location" ><option>select</option><option>1</option><option>2</option><option>3</option></select>
				</div>-->
				<!--<div class="clearfix"></div>-->
				
				
					<div>
						<label>Total no of Car</label>
						<select  name="no_of_car" id="no_of_car"><option value="0" class="mleft">select</option><?php for($i=1;$i<=4;$i++){$j=5*($i-1); $k=5*($i);echo "<option value='$j-$k'>$j-$k</option>";} ?><option value="25 above">25 above</option></select> 
					</div>
					<div id="type_of_car" >
							<div class="label2">Share details of your current fleets</div><textarea rows="2" cols="40" placeholder="Details"></textarea>
					</div>
					<!--<div><label id="add_more" class="active"><button type="button" >+ add more</button></label></div>-->
					<div id="rate">
						<div style="font-weight:bold">Current Rate Charged</div>
						 <textarea id="rate_desc" name="rate_desc" rows="2" cols="40" placeholder="Rate Charged"></textarea>	
					</div>
				
			</div>
			</fieldset>
			<div>
				<label class="label" style="width:400px!important">How soon can you start the business with Carzonrent?</label>
				<input type="radio" name="start_business">0-3 months <input type="radio" name="start_business" >3-6 months <input type="radio" name="start_business">6-12 months
			</div>
			<div class="clearfix"></div>
			<div>
				<label class="label" style="width:400px!important;margin-bottom: 5px;">Any other information you would like to share?</label>
				<textarea  name="other_nfo" id="other_info" placeholder="other information" style="float:left" rows="5" cols="90"></textarea>
			</div>
			<div class="clearfix"></div>
				
		 
		
	
	
			<div style="text-align:center"><input type="submit" value="submit"></div>
		</form>
	</div>
</div>

</body>
<script>
$(document).ready(function(){
	//partnership event
	$('input[type=radio]').click(function(e){
	
	   if(e.target.id=="partnership"){
	$('#partnership_detail').slideDown();
	}
	else
	{
		$('#partnership_detail').slideUp();
	}
	});
	
	//business details event
	$('#car_rental_b').click(function(){
	
	$('#car_rental_business').slideToggle(1000);
	});
	//no of car event
	/*$('#no_of_car').change(function(){
	 
	 if($(this).val()!=0)
	 {
	 $('#type_of_car').removeClass('active');
	 $('#add_more').show();
	 }
	 else
	 {
	 $('#type_of_car').addClass('active');
	 $('#add_more').hide();
	 }
	
	
	});*/
	//industry type
	$('#i_type').change(function(){
	 if($(this).val()==8){ $('#c_detail').slideDown(); $('#o_type_name').hide(); }
	 if($(this).val()==9){$('#o_type_name').show();$('#c_detail').slideUp(1000);}
	 else 
	 {	//$('#c_detail').slideUp();
		$('#o_type_name').hide();
	 }
	
	});
	//business
	 $('#business').click(function(){
		$('#b_detail').slideDown(1000);
	 
	 });
	 $('#salaried').click(function(){
		$('#b_detail').slideUp(1000);
	 
	 });
	//add more row
	/*$('#add_more').click(function()
	{
	   var i = $('#type_of_car .add_row').length+1 ;
	$('<div id="count_'+i+'" class="add_row"><div style="float:left;margin-right: 55px;"><label>Type of Car</label><input type="text" name="type_car[]" id="type_car_'+i+'" placeholder="type of car" class="mleft"></div><div style="float:left;"><label>No of Car</label><input type="text" name="no_of_car[]" id="no_of_car_'+i+'" placeholder="no_of_car" class="mleft"></div><a href="javascript:void(0)" id="rm_row'+i+'" onclick="remove_row(this.id)">remove</a><div class="clearfix"></div></div><').appendTo('#type_of_car');
		
	});*/
	//lease click
	$('#own_lease').click(function()
	{
	 $('#leased_detail').slideToggle();
	 $('#address_office').slideToggle();
	});
	
});

function remove_row(id)
	{
	var num=id.replace(/^\D+/g,'');
	$('#count_'+num).remove();
	}
</script>




</html>
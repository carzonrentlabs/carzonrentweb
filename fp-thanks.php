<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php //include_once("./includes/seo-metas.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" ></script>
<link rel="stylesheet" type="text/css" href="./css/global.css?v=<?php echo mktime(); ?>" />
</head>
<body>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:2000, autoplay_slideshow:false});
});
</script> 
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 


<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">

<div class="lfcnt" style="width:90%">
<div id="country1" class="tabcontent">
  <h1>Forgot Password</h1>
  <h2>Thank You</h2>
<p class="txt14px">&nbsp;</p>
<p>Please check your email account to setup your password.<br />
<div class="apply"><div class="submit"><a href="javascript:void(0)"></a></div></div>
<p class="txt14px">&nbsp;</p>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>-->
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
    /**** Payback case for selfdrive and outstation and local ***/
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
    include_once('./classes/cor.mysql.class.php');
	$fullname = explode(" ", $_POST["name"]);
	if(count($fullname) > 1)
	{
	    $fname = $fullname[0];
	    $lname = $fullname[1];
	}
	else
	{
	    $fname = $fullname[0];
	    $lname = "";
	}
	$cor = new COR();
	
	
	
	
	/******************** Outstation and local case added by vinod ***************/
		$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
		$myXML = new CORXMLList();
		$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
		if(key_exists("Column1", $arrUserId[0]) && intval($arrUserId[0]["Column1"]) == 0){
			if(isset($_POST["corpassword"]))
			$pword = $_POST["corpassword"];
			else
			$pword = "retail@2012";
			$res = $cor->_CORRegister(array("fname"=>$fname, "lname"=>$lname, "PhoneNumber"=>$_POST["monumber"], "emailId"=>$_POST["email"], "password"=>$pword));
			$arrUserId = $myXML->xml2ary($res->{'RegistrationResult'}->{'any'});
			if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0)
			$tCCIID = trim($arrUserId[0]["Column1"]);
		}
		else {
			if(intval($arrUserId[0]["ClientCoIndivID"]) > 0)
			$tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
		}
	/******************** Outstation and local case added by vinod ***************/
	
              
	$mtime = round(microtime(true) * 1000);
	$dataToSave = array();
	
	if($_POST["refclient"]!='')
	{
	$dataToSave["source"] = $cor->decrypt($_POST["refclient"]);
	}
	else
	{
	$dataToSave["source"] = "COR";
	}     
	$dataToSave["tour_type"] 		 = $_POST["hdTourtype"];
	$dataToSave["package_id"] 		 = $_POST["hdPkgID"];
	$dataToSave["car_cat"] 			 = $_POST["hdCat"];
	if(isset($_POST["hdCarCatID"]) && $_POST["hdCarCatID"] != "")
	$dataToSave["car_cat_id"] 		 = $_POST["hdCarCatID"];
	$dataToSave["origin_name"]  	 = $_POST["hdOriginName"];
	$dataToSave["origin_id"] 		 = $_POST["hdOriginID"];
	$dataToSave["destinations_name"] = $_POST["hdDestinationName"];
	$dataToSave["destinations_id"] = $_POST["hdDestinationID"];
	$picDate = date_create($_POST["hdPickdate"]);
	$dataToSave["pickup_date"] = $picDate->format('Y-m-d');
	if(isset($_POST["tHour"]) && $_POST["tHour"] != "" && isset($_POST["tMin"]) && $_POST["tMin"] != "")
	$dataToSave["pickup_time"] = $_POST["tHour"] . $_POST["tMin"];
	elseif(isset($_POST["tHourP"]) && isset($_POST["tMinP"]))
	$dataToSave["pickup_time"] = $_POST["tHourP"] . $_POST["tMinP"];
	$drpDate = date_create($_POST["hdDropdate"]);
	$dataToSave["drop_date"] = $drpDate->format('Y-m-d');
	if($_POST["hdTourtype"] == "Selfdrive")
	$dataToSave["drop_time"] = $_POST["tHourD"] . $_POST["tMinD"];
	$dataToSave["duration"] = $_POST["duration"];
        $dataToSave["nightduration"] =$_POST["nightduration"];
	if(isset($_POST["hdDistance"]) && $_POST["hdDistance"] != "")
	$dataToSave["distance"] = $cor->decrypt($_POST["hdDistance"]);
	else
	$dataToSave["distance"] = "0";
	$dataToSave["airportCharges"] = $_POST["airportCharges"];
        
	/************ new change airport************/
	if($_POST["hdTourtype"]=='airport')
	{
	$billableamount=$cor->decrypt($_POST["totFare"])-$_POST["txtamounttoredeem"];
	$dataToSave["tot_fare"] = $billableamount;
	}
	else
	{
	  $dataToSave["tot_fare"] = $cor->decrypt($_POST["totFare"]);
	}
	
	/************ new change************/
        
	$dataToSave["address"] = $_POST["address"];
	$dataToSave["remarks"] = $_POST["remarks"];
	$dataToSave["full_name"] = $_POST["name"];
	$dataToSave["email_id"] = $_POST["email"];
	$dataToSave["mobile"] = $_POST["monumber"];
	$dataToSave["promotion_code"] = "PayBack";
	$dataToSave["discount_coupon"] = $_POST["txtcardnopb"];
	$dataToSave["additional_srv"] = $_POST["addServ"];
	$dataToSave["additional_srv_amt"] = $_POST["addServAmt"];
	$dataToSave["chauffeur_charges"] =$cor->decrypt($_POST["ChauffeurCharges"]);
        $dataToSave["nightstayCharge"] =$cor->decrypt($_POST["NightStayAllowance"]);
       
	$dataToSave["cciid"] = $tCCIID;
	$dataToSave["coric"] = "CORIC-" . $mtime;
	$dataToSave["ip"] = $_SERVER["REMOTE_ADDR"];
	$dataToSave["ua"] = $_SERVER["HTTP_USER_AGENT"];
	$dataToSave["entry_date"] = date('Y-m-d H:i:s');
	if(isset($_POST["totAmount"]) && $_POST["totAmount"] != "")
	$dataToSave["base_fare"] = $cor->decrypt($_POST["totAmount"]);
	else
	$dataToSave["base_fare"] = $cor->decrypt($_POST["BasicAmt"]);
	if(isset($_POST["vat"]) && $_POST["vat"] != "")
	$dataToSave["vat"] = $cor->decrypt($_POST["vat"]);
	else
	$dataToSave["vat"] = 0.00;
	if(isset($_POST["secDeposit"]) && $_POST["secDeposit"] != "")
	$dataToSave["sec_deposit"] = $cor->decrypt($_POST["secDeposit"]);
	else
	$dataToSave["sec_deposit"] = 0;
	if(isset($_POST["kmRate"]) && $_POST["kmRate"] != "")
	$dataToSave["km_rate"] = $cor->decrypt($_POST["kmRate"]);
	else
	$dataToSave["km_rate"] = 0;
	$isPB = '0';
	if(isset($_POST["paybackearn"]) && $_POST["paybackearn"] != "")
	$isPB = $_POST["paybackearn"];
	$dataToSave["isPayBack"] = $isPB;
	$dataToSave["disc_value"] = $_POST["txtpbpoints"];
	if(isset($_POST["ddlSubLoc"]))
	$dataToSave["subLoc"] = $_POST["ddlSubLoc"];
	if(isset($_COOKIE["kword"]) && $_COOKIE["kword"] != "")
	$dataToSave["kword"] = $_COOKIE["kword"];
	if(isset($_COOKIE["gclid"]) && $_COOKIE["gclid"] != "")
	$dataToSave["gclid"] = $_COOKIE["gclid"];
	$dataToSave["pkg_type"] = $_POST["chkPkgType"];
	$dataToSave["add_service_cost_all"] = $_POST["addServiceCostAll"];
	$dataToSave["model_name"] = str_ireplace(array("<br>","<br />"), "", nl2br($_POST["hdCarModel"]));
	if(isset($_POST["hdCarModelID"]) && $_POST["hdCarModelID"] != "")
	$dataToSave["model_id"] = $_POST["hdCarModelID"];
	if(isset($_POST["subLocName"]) && $_POST["subLocName"] != "")
	$dataToSave["subLocName"] = $_POST["subLocName"];
	if(isset($_POST["subLocCost"]) && $_POST["subLocCost"] != "")
	$dataToSave["subLocCost"] = $_POST["subLocCost"];
	if(isset($_POST["OriginalAmt"]) && $_POST["OriginalAmt"] != "")
	$dataToSave["OriginalAmt"] = $cor->decrypt($_POST["OriginalAmt"]);
	if(isset($_POST["BasicAmt"]) && $_POST["BasicAmt"] != "")
	$dataToSave["BasicAmt"] = $cor->decrypt($_POST["BasicAmt"]);
	if(isset($_POST["KMIncluded"]) && $_POST["KMIncluded"] != "")
	$dataToSave["KMIncluded"] = $_POST["KMIncluded"];
	if(isset($_POST["ExtraKMRate"]) && $_POST["ExtraKMRate"] != "")
	$dataToSave["ExtraKMRate"] = $cor->decrypt($_POST["ExtraKMRate"]);
	if(isset($_POST["WeekDayDuration"]) && $_POST["WeekDayDuration"] != "")
	$dataToSave["WeekDayDuration"] = $_POST["WeekDayDuration"];
	if(isset($_POST["WeekEndDuration"]) && $_POST["WeekEndDuration"] != "")
	$dataToSave["WeekEndDuration"] = $_POST["WeekEndDuration"];
	if(isset($_POST["FreeDuration"]) && $_POST["FreeDuration"] != "")
	$dataToSave["FreeDuration"] = $_POST["FreeDuration"];
	
	$dataToSave["tot_gross_amount"]=$cor->decrypt($_POST["totFare"]);
	
	 if (isset($_REQUEST["pickdrop"]) && $_REQUEST["pickdrop"] != "") {
        $dataToSave["pickupdrop_address"] = $_REQUEST["pickdrop"]; //pick drop address
        }
	
	
	if(isset($_POST["txtamounttoredeem"]) && $_POST["txtamounttoredeem"] != "")
	{
		$billableamount=intval($cor->decrypt($_POST["totFare"]))-$_POST["txtamounttoredeem"];
		$dataToSave["User_billable_amount"] = $billableamount;
	}
	if(isset($_POST["triptype"]) && $_POST["triptype"] != "")
	$dataToSave["trip_type"] = $_POST["triptype"];
	if(isset($_REQUEST["FlexiBooking"]) && $_REQUEST["FlexiBooking"]!="")
	{
	 $dataToSave["flexi_remark"] = $_REQUEST["FlexiBooking"]; //flexi search
	}
	$dataToSave["pb_status"] = 1;
        
	$dataToSave["cab_purpose"] = $_POST["cabid"];
	list($terminalid,$cityid) = explode("|",$_POST["terminalid"]);
	$dataToSave["terminalid"] = $terminalid;
	$dataToSave["terminalname"] = $_POST["terminalname"];
	
	$dataToSave["CGSTPercent"] = $cor->decrypt($_POST['CGSTPercent']);
		$dataToSave["SGSTPercent"] = $cor->decrypt($_POST['SGSTPercent']);
		$dataToSave["IGSTPercent"] = $cor->decrypt($_POST['IGSTPercent']);
		$dataToSave["GSTEnabledYN"] = $cor->decrypt($_POST['GSTEnabledYN']);
		$dataToSave["ClientGSTId"] = $cor->decrypt($_POST['ClientGSTId']);
	
        
	$db = new MySqlConnection(CONNSTRING);
	$db->open();
	$r = $db->insert("cor_booking_new", $dataToSave);
	unset($dataToSave);
	$db->close();
	$backUrl = corWebRoot."/backurl.php";
	$redirectUrl = corWebRoot."/redircturl.php";
	$url = 'https://npg.payback.in/nextgen-pgateway/NewPointGateway';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
        $post_items[] = 'lmid='.'90014740';
        $post_items[] = 'digest='.'16439EDEFD8660B487C3D38D75B73283';

	$post_items[] = 'loyaltyNo='.$_POST["txtcardnopb"];
	$post_items[] = 'currency='.'PNT';
	$post_items[] = 'points='. $_POST["txtpbpoints"];
	$post_items[] = 'trnsid='. "CORIC" . $mtime;
	$post_items[] = 'backUrl='.$backUrl;
	$post_items[] = 'redirectUrl='.$redirectUrl;
	//format the $post_items into a string
	$post_string = implode ('&', $post_items);
	//send the $_POST data to the new page
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	$response = curl_exec($ch);
	curl_close($ch);
	$rURL = str_replace("10.0.0.7:8447","npg.payback.in",$response);
        $rURL = str_replace("localhost:8447","npg.payback.in",$response);
	//echo $rURL;
	header('Location: ' . $rURL);
?>

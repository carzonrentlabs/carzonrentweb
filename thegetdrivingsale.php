<?php 
error_reporting(0);
header("Location: http://www.carzonrent.com");
exit(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	//define('corWebRoot','http://localhost/cor_css2');
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Offer: The Get Driving Sale</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/cor.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<style type="text/css">
.myles_head {
margin: 20px auto 0;
width: 100%;
background-color: #DB4425;
height: 40px;
line-height: 40px;
color: #fff;
}
.magindiv {
width: 1130px;
margin: 0 auto !important;
}
.sale{
width:250px;
display:inline-block;
text-align:center;
margin-bottom:10px;
padding:166px 0;
height:auto;
background:#df4627;
}
.f_l{float:left;}
.clr{clear:both;}
.sale_content{
width:72%;
margin-left:20px;
margin-bottom:20px;
background-color:#FBFBFB;
padding:15px;
border:1px solid #E5E5E5;
}
.sale_content ul{
margin:5px 0 10px 19px;
padding:0px;
}
.sale_content p.know_more_TC{margin-bottom:0px;}
.sale_content ul li{
font-size:13px;
}
.hedding_tc{
color:#e1471b;
padding:5px 0 0;
font-size:13px;
}
.sale_content ul.off{
margin-bottom:2px;
}
</style>
<div id="middle" style="width:100%" >
<div class="myles_head">
<div class="magindiv">
<h2 class="heading2">The Get Driving Sale</h2>
</div>
</div>
<div class="mainW960" style="padding:20px 0;">
<div class="sale f_l">
<img src="http://www.carzonrent.com/beta/mylesimages/sale.png" border="0">
</div>
<div class="f_l sale_content">
<p style="padding-bottom:10px;">
The biggest & first time sale on self-drive is here. Fetch the best deals for your wheels. 
</p>
<p style="padding-bottom:10px;">
Be it the awesome Myles Nano@ Rs.400/day or the swanky Myles Merc E class@ Rs.6000/day, drive yourself where ever, whenever at your freewill. Whenever you need a car, we have the keys.
</p>
<strong>Offer Details:</strong>
<div class="clr"> </div>
<div class="hedding_tc">Delhi NCR (includes Gurgaon, Ghaziabad & Noida)</div>
<ul class="off">
<li>Weekday: Flat 50% off</li>
<li>Weekend: Flat 50% off</li>
</ul>
<div class="hedding_tc">Other Cities</div>
<ul>
<li>Weekday Flat 50% off</li>
<li>Weekend Flat 20% off</li>
</ul>
<strong>Terms and Conditions:</strong>
<ul style="margin:5px 0 10px 20px;">
<li>Offer is valid only on the self-drive service: Myles</li>
<li>Offer valid on daily, weekly & monthly reservations</li>
<li>The discount is on the base fare</li>
<li>Booking subject to car availability.</li>
<li>Standing instructions/ pre-authorization will be made on the credit card based on the car type at the time of pick-up</li>
<li>Offer not to be clubbed with any other promotion/coupon code</li>
<li>The discount can be redeemed in full & not in parts</li>
<li>Valid on bookings made/created till the midnight of 10th September, 2015 & with pick-up date on or before 30th September 2015.</li>
<li>Blackout dates apply</li>
<li>Other standard T&C apply</li>
</ul>
<strong>How to avail / book</strong>
<ul style="margin:5px 0 10px 20px;">
<li>Log on to <a href="http://www.mylescars.com/" target="_block">www.mylescars.com</a> to book a Myles car.</li>
<li>Select the preferred city, date/time, car & the location.</li>
<li>No coupon/voucher code is required to avail the offer. The discount will be automatically calculated for the mentioned period.</li>
<li>Proceed with the booking & pay the balance amount through credit card/debit card/net banking.</li>
</ul>
<p class="know_more_TC">Know more about the service: <strong>call 08882222222</strong></p>
</div>
</div>
</div>
<?php include_once("./includes/footer.php"); ?>
<script type="text/javascript" src="js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="css/tab.css">
</body>
</html>

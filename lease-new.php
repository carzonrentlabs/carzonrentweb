<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COR</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="css/global.css">
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 
<script type="text/javascript" src="js/slides.min.jquery.js"></script>
<script type="text/javascript" src="js/customSelect.jquery.js"></script>
	<script>
		$(function(){
			$('select.styled').customSelect();
			$('#slides').slides({
				preload: false,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(current){
					$('.caption').animate({
						bottom:-35
					},100);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationStart on slide: ', current);
					};
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationComplete on slide: ', current);
					};
				},
				slidesLoaded: function() {
					$('.caption').animate({
						bottom:0
					},200);
				}
			});
			
			
		});
	</script>
	<script>
		_setActive =  function(tab)
	    {
		    document.getElementById('outstation').className = '';
		    document.getElementById('local').className = '';
		    document.getElementById('easycabs').className = '';
		    
		    document.getElementById('outstationDiv').style.display = 'none';
		    document.getElementById('localDiv').style.display = 'none';
		    document.getElementById('easycabsDiv').style.display = 'none';
		    
		    var tabName = tab;
		    if(tabName == 'outstation')
		    {
		        document.getElementById('outstation').className = 'activeTab';
		        document.getElementById('outstationDiv').style.display = 'block';
		    }
		    else if(tabName == 'local')
		    {
		        document.getElementById('local').className = 'activeTab';
		        document.getElementById('localDiv').style.display = 'block';
		    }
		    else if(tabName == 'easycabs')
		    {
		        document.getElementById('easycabs').className = 'activeTab';
		        document.getElementById('easycabsDiv').style.display = 'block';
		    }
	    }
	</script>
</head>
<body>
<!--Header Start Here-->
<div id="header">
	<div class="main">
    	<div class="logo"><a href="./" id="brand" class="easybrand"></a></div>
        <div class="logo-right">
        	<div class="logo-righttop">
            	<ul class="lf">
                	<li><a href="#">COR-Car rentals</a></li>
                    <li><a href="#" class="active">COR-Easycabs</a></li>
                    <li><a href="#">COR-leasing</a></li>
                </ul>
                <ul class="rt">
                	<li><a href="#">Corporate User</a></li>
                    <li><a href="#">Login</a></li>
                    <li class="no"><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="menu">
            	<ul>
                	<li><a href="#" class="active">Delhi-NCR </a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Bangalore</a></li>
                    <li><a href="#">Hyderabad</a></li>
                    <li class="more"><a href="#">More</a>
                    	<ul class="submenu">
                        	<li><a href="#">Mumbai</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                        </ul>
                    </li>
                    <li class="tfn">Call 24 hours (prefix city code)<strong>0888 222 2222</strong> </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_right"></div>
</div>
<!--Banner Start Here-->
<div class="banner">
	<div class="main">
		<div class="lfts-lease">
			<div class="topcrb">
				<span class="crp-lease">CORPORATE LEASING</span>
			</div>
			<div class="running">
				<div class="tberlease">
					<div class="bdrbtmlease">
						<p>
						    <input type="text" class="bigtxtbox"  onfocus="if(value=='Organization') value = ''" onblur="if(value=='') value = 'Organization'" value="Organization" name="txtorg"  />
						</p>
					</div>
					<div class="bdrbtmlease">
						<p>
						    <input type="text" class="bigtxtbox" name="txtname" onfocus="if(value=='Name') value = ''" onblur="if(value=='') value = 'Name'" value="Name"  />
						</p>
					</div>
					<div class="bdrbtmlease">
						<p>
						    <input type="text" class="bigtxtbox" name="txtemailid"  onfocus="if(value=='Email') value = ''" onblur="if(value=='') value = 'Email'" value="Email" />
						</p>
					</div>
					<div class="bdrbtmlease">
						<p>
						    <input type="text" class="bigtxtbox" name="monumber" onfocus="if(value=='Mobile') value = ''" onblur="if(value=='') value = 'Mobile'" value="Mobile"  />
						</p>
					</div>						
						<div class="bdrbtmlease">
							<p>
								<span class="location1">
									<select class="styled" name="">
									<option>Number of employees</option>
									<option>1-5</option>
									<option>5-10</option>
									</select>
								</span>
							</p>
					      </div>
					<div class="bdrbtmlease">
						 <p><a href=""><img src="images/leasing.jpg" alt=""  border="0"/></a></p>
					</div>
				</div>
			</div>
			<div class="bcrb"></div>
		</div>
			<div class="social-network">
				<img src="images/socal.png" alt="" />
			</div>	
		<div class="rgts-lease" style=>
			<div class="topcurb"></div>
			<div class="repet">
		
				<div class="right-banner">
					<div class="bannertexbox">
						<div class="bdrbtmlease">
							<p>
							    <input type="text" class="bigtxtbox"  onfocus="if(value=='Choose sector') value = ''" onblur="if(value=='') value = 'Choose sector'" value="Choose sector" name="txtsector"  />
						  
							      <input type="text" class="bigtxtbox"  onfocus="if(value=='Enter company name') value = ''" onblur="if(value=='') value = 'Enter company name'" value="Enter company name" name="txtcompany" />
							      <input type="text" class="bigtxtbox"  onfocus="if(value=='Enter employee strength') value = ''" onblur="if(value=='') value = 'Enter employee strength'" value="Enter employee strength" name="txtstrength"  />
							      <a href=""><img src="images/begin.jpg" alt=""  border="0"/></a>
							</p>
						</div>
						
					</div>
				
				</div>
			</div>
			<div class="botcurb"></div>
		</div>
	</div>
</div>
	
<!--Middle Start Here-->
<div class="bannerbtm">
	<div class="main">
        <span>Point to point radio taxi service: </span>Pay as per the kilometers you travel
    </div>
</div>
<div class="cabs">
	<div class="main">
    	<div class="ftop">
        	<div class="wdcar">
            <img src="images/car.png" alt="" /> <label><span>6,500+</span><br />Cabs</label>
            </div>
            <div class="wdtrips">
            <img src="images/trips.png" alt="" /> <label><span>15,000+</span><br />Trips Daily</label>
            </div>
            <div class="wdten">
            <img src="images/lst.png" alt="" /> <label><span>6,000,000+</span><br />Customers Annually</label>
            </div>
            <div class="wdten">
            <img src="images/ten.png" alt="" /> <label><span>100,000+</span><br />Kilometers Daily</label>
            </div>
        </div>
    </div>
</div>
<div id="middle">
	<div class="main">    
    	<div class="homepage">
        	<div class="repeatdiv">
            	<img src="images/icon3.gif" alt="" class="icon1" />
            	<h2 class="icon1">Quality</h2>
                <ul>
                	<li>Well trained chauffeurs</li>
                    <li>Young, well-maintained car fleet</li>
                    <li>Amenities for comfort</li>
                </ul>
            </div>
            <div class="repeatdiv md">
            	<img src="images/icon2.gif" alt="" class="icon2" />
            	<h2 class="icon2">Reliable</h2>
                <ul>
                	<li>India's largest car hire/rental company</li>
                    <li>In car GPS devices for extra safety</li>
                    <li>Transparent pricing structure</li>
                </ul>
            </div>
            <div class="repeatdiv no">
            	<img src="images/icon1.gif" alt="" class="icon3" />
            	<h2 class="icon3">Convenient</h2>
                <ul>
                	<li>Available across major cities</li>
                    <li>Book through web or phone</li>
                    <li>Pay by cash or card</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--footer Start Here-->
<div id="footer">
	<div class="main">
    	
        <div class="fbtm">
        	<div class="cm">
            	<h3>Company</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/aboutus.php" target="_blank">About</a></li>
			<li><a href="http://www.carzonrent.com/services.php" target="_blank">Services</a></li>
			<li><a href="http://www.carzonrent.com/careers.php" target="_blank">Career</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Book a Cab</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/business-enquiries.php" target="_blank">Outstation</a></li>
			<li><a href="http://www.carzonrent.com/corporate-rental.php" target="_blank">Local</a></li>
			<li><a href="http://www.easycabs.com/" target="_blank">Easycabs</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Easy Cabs</h3>
                <ul>
                	<li><a href="http://www.easycabs.com/advertisewithus.php" target="_blank">Advertise with Us</a></li>
                        <li><a href="http://www.easycabs.com/fleet.php" target="_blank">Fleet</a></li>
			<li><a href="http://www.easycabs.com/tariff.php" target="_blank">Tariff</a></li>
                </ul>
            </div>
	    <div class="qu">
            	<h3>Corporate Leasing</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/corlease/comparative-analysis.php" target="_blank">Why Leasing</a></li>
			<li><a href="http://www.carzonrent.com/corlease/lease-benefits.php" target="_blank">Benefits</a></li>
			<li><a href="http://www.carzonrent.com/corlease/product-and-services.php" target="_blank">Products & Services</a></li>
                </ul>
            </div>
            <div class="qu">
            	<h3>Quick Links</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/my-account.php" target="_blank">My Account</a></li>
			<li><a href="http://www.carzonrent.com/print-booking.php" target="_blank">Print Booking</a></li>
			<li><a href="http://www.carzonrent.com/vehicle-guide.php">Vehicle Guide</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
	    
            <div class="socil">
            	<h3>Social</h3>
                <div class="soccd">
			<table cellpadding="5" cellspacing="0" border="0">
                        <tr>
                            <td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1" type="text/javascript"></script><fb:like href="http://www.facebook.com/carzonrent/" send="false" layout="button_count" width="90" show_faces="false"></fb:like></td>
                            <td><a href="https://twitter.com/carzonrentIN" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @carzonrentIN</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
                            <td style="padding-left:15px;"><!-- Place this tag where you want the +1 button to render -->
<g:plusone size="medium" href="http://www.carzonrent.com"></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></td>
                        </tr>
                    </table>
		</div>
                <!--<h3>Download Mobile App</h3>
                <p><img src="images/apps.png" alt="" /></p>-->
            </div>
        </div>
        <div class="copurit">Copyright &copy; Carzonrent. All Rights Reserved. Terms of Use.</div>
    </div>
</div>
</body>
</html>

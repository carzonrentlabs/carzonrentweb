<?php
    class GharPay{
        function __construct(){
            $this->soapClient = new SoapClient("http://webservices.gharpay.in/GharpayService?wsdl", array('trace' => 1));
            $this->headerUser = new SoapHeader("http://webinterface.webservices.gharpay.com","username", "carzonrent_api");
            $this->headerPass = new SoapHeader("http://webinterface.webservices.gharpay.com","password", "q9#&8a9i");
        }
        function _CORGPCreateOrder($arg){
            $this->soapClient->__setSoapHeaders(array($this->headerUser,$this->headerPass));  
            try {
                 $r = $this->soapClient->createOrder($arg);
                 return $r;
            } catch(Exception $e) {
                 //print $e->getMessage();
            }
        }
        function _CORGPCheckPin($arg){            
            $this->soapClient->__setSoapHeaders(array($this->headerUser,$this->headerPass));  
            try {
                 $r = $this->soapClient->isPincodePresent($arg);
                 return $r;
            } catch(Exception $e) {
                 //print $e->getMessage();
            }
        }
    };
?>
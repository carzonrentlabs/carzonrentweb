/**
 *
 * ัontent slider with jCarousel
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Script Tutorials
 * http://www.script-tutorials.com/
 */

// Set active slide function
function setActiveSlide(i) {

    // Update current position counter
    $('#count').html( (i + 1) + '/' + $('.sl-slide').length );

    // Remove 'selected' class attribute from all jcarousel-items
    $('ul#sl-thumbs li').removeClass('selected');

    // Set 'selected' class attribute to a selected jcarousel-item
    var li = $('ul#sl-thumbs li').eq(i);
    li.addClass('selected');

    // Hide all slides (remove 'visible' class attribute)
    $('.sl-slide').removeClass('visible');

    // Show selected slide (set 'visible' class attribute)
    $('.sl-slide').eq(i).addClass('visible');

    // Update browser's URL with a title of selected slide (optional):
    //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');
}


// Once DOM (document) is finished loading
$(document).ready(function(){

    // Initialize jCarousel
    $('#sl-thumbs').jcarousel();

    // set first active slide
    setActiveSlide(0);

    // jcarousel-item onclick handler
    $('#sl-thumbs li').click(function() {
        setActiveSlide($(this).index());
    });

    // Slide's image onclick handler
    $('.sl-slide img').click(function(){

        if (! bViewAllMode) {
            // Find current index and next position
            var iCur = $('ul li.selected').index();
            var iMax = $('ul li').length - 1;
            var iNext = (iCur + 1 >  iMax) ? 0 : (iCur + 1);

            // Update position and set to next slide
            setActiveSlide(iNext);
        }

        return false;
    });

    // Next button onclick handler
    $('.next').click(function(){

        // Find current index and next position
        var iCur = $('ul#sl-thumbs li.selected').index();
        var iMax = $('ul#sl-thumbs li').length - 1;
        var iNext = (iCur + 1) >  iMax ? 0 : (iCur + 1);

        // Update position and set to next slide
        setActiveSlide(iNext);

        return false;
    });

    // Prev button onclick handler
    $('.prev').click(function(){

        // Find current index and previous position
        var iCur = $('ul#sl-thumbs li.selected').index();
        var iMax = $('ul#sl-thumbs li').length - 1;
        var iPrev = (iCur - 1 >  iMax) ? 0 : (iCur - 1);
        iPrev = (iPrev == -1) ? iMax : iPrev;

        // Update position and set to previous slide
        setActiveSlide(iPrev);

        // Update browser's URL with a title of selected slide (optional):
        //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');

        return false;
    });
});

//tab2 start
// Set active slide function
function setActiveSlide2(i) {

    // Update current position counter
    $('#count').html( (i + 1) + '/' + $('.sl-slide2').length );

    // Remove 'selected' class attribute from all jcarousel-items
    $('ul#sl-thumbs2 li').removeClass('selected');

    // Set 'selected' class attribute to a selected jcarousel-item
    var li = $('ul#sl-thumbs2 li').eq(i);
    li.addClass('selected');

    // Hide all slides (remove 'visible' class attribute)
    $('.sl-slide2').removeClass('visible');

    // Show selected slide (set 'visible' class attribute)
    $('.sl-slide2').eq(i).addClass('visible');

    // Update browser's URL with a title of selected slide (optional):
    //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');
}


// Once DOM (document) is finished loading
$(document).ready(function(){
	
    // Initialize jCarousel
    $('#sl-thumbs2').jcarousel();

    // set first active slide
    setActiveSlide2(0);

    // jcarousel-item onclick handler
    $('#sl-thumbs2 li').click(function() {
        setActiveSlide2($(this).index());
    });

    // Slide's image onclick handler
    $('.sl-slide2 img').click(function(){

        if (! bViewAllMode) {
            // Find current index and next position
            var iCur = $('ul li.selected').index();
            var iMax = $('ul li').length - 1;
            var iNext = (iCur + 1 >  iMax) ? 0 : (iCur + 1);

            // Update position and set to next slide
            setActiveSlide2(iNext);
        }

        return false;
    });

    // Next button onclick handler
    $('.next').click(function(){
        // Find current index and next position
        var iCur = $('ul#sl-thumbs2 li.selected').index();
        var iMax = $('ul#sl-thumbs2 li').length - 1;
        var iNext = (iCur + 1) >  iMax ? 0 : (iCur + 1);

        // Update position and set to next slide
        setActiveSlide2(iNext);

        return false;
    });
	
	// Prev button onclick handler
    $('.prev').click(function(){

        // Find current index and previous position
        var iCur = $('ul#sl-thumbs li.selected').index();
        var iMax = $('ul#sl-thumbs li').length - 1;
        var iPrev = (iCur - 1 >  iMax) ? 0 : (iCur - 1);
        iPrev = (iPrev == -1) ? iMax : iPrev;

        // Update position and set to previous slide
        setActiveSlide2(iPrev);

        // Update browser's URL with a title of selected slide (optional):
        //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');

        return false;
    });
});

//tab3 start
// Set active slide function
function setActiveSlide3(i) {

    // Update current position counter
    $('#count').html( (i + 1) + '/' + $('.sl-slide2').length );

    // Remove 'selected' class attribute from all jcarousel-items
    $('ul#sl-thumbs3 li').removeClass('selected');

    // Set 'selected' class attribute to a selected jcarousel-item
    var li = $('ul#sl-thumbs3 li').eq(i);
    li.addClass('selected');

    // Hide all slides (remove 'visible' class attribute)
    $('.sl-slide3').removeClass('visible');

    // Show selected slide (set 'visible' class attribute)
    $('.sl-slide3').eq(i).addClass('visible');

    // Update browser's URL with a title of selected slide (optional):
    //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');
}


// Once DOM (document) is finished loading
$(document).ready(function(){
	
    // Initialize jCarousel
    $('#sl-thumbs3').jcarousel();

    // set first active slide
    setActiveSlide3(0);

    // jcarousel-item onclick handler
    $('#sl-thumbs3 li').click(function() {
        setActiveSlide3($(this).index());
    });

    // Slide's image onclick handler
    $('.sl-slide3 img').click(function(){

        if (! bViewAllMode) {
            // Find current index and next position
            var iCur = $('ul li.selected').index();
            var iMax = $('ul li').length - 1;
            var iNext = (iCur + 1 >  iMax) ? 0 : (iCur + 1);

            // Update position and set to next slide
            setActiveSlide3(iNext);
        }

        return false;
    });

    // Next button onclick handler
    $('.next').click(function(){
        // Find current index and next position
        var iCur = $('ul#sl-thumbs3 li.selected').index();
        var iMax = $('ul#sl-thumbs3 li').length - 1;
        var iNext = (iCur + 1) >  iMax ? 0 : (iCur + 1);

        // Update position and set to next slide
        setActiveSlide3(iNext);

        return false;
    });
	
	// Prev button onclick handler
    $('.prev').click(function(){

        // Find current index and previous position
        var iCur = $('ul#sl-thumbs li.selected').index();
        var iMax = $('ul#sl-thumbs li').length - 1;
        var iPrev = (iCur - 1 >  iMax) ? 0 : (iCur - 1);
        iPrev = (iPrev == -1) ? iMax : iPrev;

        // Update position and set to previous slide
        setActiveSlide3(iPrev);

        // Update browser's URL with a title of selected slide (optional):
        //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');

        return false;
    });
});

//tab4 start
// Set active slide function
function setActiveSlide4(i) {

    // Update current position counter
    $('#count').html( (i + 1) + '/' + $('.sl-slide2').length );

    // Remove 'selected' class attribute from all jcarousel-items
    $('ul#sl-thumbs4 li').removeClass('selected');

    // Set 'selected' class attribute to a selected jcarousel-item
    var li = $('ul#sl-thumbs4 li').eq(i);
    li.addClass('selected');

    // Hide all slides (remove 'visible' class attribute)
    $('.sl-slide4').removeClass('visible');

    // Show selected slide (set 'visible' class attribute)
    $('.sl-slide4').eq(i).addClass('visible');

    // Update browser's URL with a title of selected slide (optional):
    //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');
}


// Once DOM (document) is finished loading
$(document).ready(function(){
	
    // Initialize jCarousel
    $('#sl-thumbs4').jcarousel();

    // set first active slide
    setActiveSlide4(0);

    // jcarousel-item onclick handler
    $('#sl-thumbs4 li').click(function() {
        setActiveSlide4($(this).index());
    });

    // Slide's image onclick handler
    $('.sl-slide4 img').click(function(){

        if (! bViewAllMode) {
            // Find current index and next position
            var iCur = $('ul li.selected').index();
            var iMax = $('ul li').length - 1;
            var iNext = (iCur + 1 >  iMax) ? 0 : (iCur + 1);

            // Update position and set to next slide
            setActiveSlide4(iNext);
        }

        return false;
    });

    // Next button onclick handler
    $('.next').click(function(){
        // Find current index and next position
        var iCur = $('ul#sl-thumbs4 li.selected').index();
        var iMax = $('ul#sl-thumbs4 li').length - 1;
        var iNext = (iCur + 1) >  iMax ? 0 : (iCur + 1);

        // Update position and set to next slide
        setActiveSlide4(iNext);

        return false;
    });
	
	// Prev button onclick handler
    $('.prev').click(function(){

        // Find current index and previous position
        var iCur = $('ul#sl-thumbs li.selected').index();
        var iMax = $('ul#sl-thumbs li').length - 1;
        var iPrev = (iCur - 1 >  iMax) ? 0 : (iCur - 1);
        iPrev = (iPrev == -1) ? iMax : iPrev;

        // Update position and set to previous slide
        setActiveSlide4(iPrev);

        // Update browser's URL with a title of selected slide (optional):
        //window.location.hash = $('.visible .sl-title').text().replace(/ /g, '-');

        return false;
    });
});
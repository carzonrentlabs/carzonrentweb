<?php
    include_once('./classes/cor.ws.class.php');
    include_once('./classes/cor.xmlparser.class.php');
    if(isset($_GET["pd"])){
        $pd = $_GET["pd"];
        $pickDate = date_create($pd);
    }
    $corArrCA = array();
    $corArrCA["cityid"] = $_GET["ci"];
    $corArrCA["CarCatId"] = $_GET["cci"];
    $corArrCA["ServiceDate"] = $_GET["pd"];
    $cor = new COR();
    $res = $cor->_CORCheckSoldOut($corArrCA);
    $myXML = new CORXMLList();
    $isAvail = "0";
    if($res->{'CheckSoldOutResult'}->{'any'} != ""){
        $packages = $myXML->xml2ary($res->{'CheckSoldOutResult'}->{'any'});
        if($packages[0]["CarCatName"] != "")
            $isAvail = "1";
    }
    unset($cor);
    print_r($isAvail);
?>
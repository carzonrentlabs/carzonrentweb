<?php
	session_start();
	include_once('./classes/easycabs.ws.class.php');
	include_once('./classes/easycabs.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$ec = new EasyCabs();

	$res = $ec->_ECGetBranches();
	
	$myXML = new ECXMLList();
	$branch = $myXML->xml2ary($res);
	//print_r($branch);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EasyCabs</title>
<link rel="stylesheet" type="text/css" href="<?php echo ecWebRoot; ?>/css/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ecWebRoot; ?>/css/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ecWebRoot; ?>/css/global.css" />
<script type="text/javascript" src="<?php echo ecWebRoot; ?>/js/tabcontent.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo ecWebRoot; ?>/js/slides.min.jquery.js"></script>
<script type="text/javascript" src="<?php echo ecWebRoot; ?>/js/_bb_general_v3.js"></script>
<script type="text/javascript" src="<?php echo ecWebRoot; ?>/js/validation.js"></script>
<script type="text/javascript" src="<?php echo ecWebRoot; ?>/js/ajax.js"></script>

<script>
	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}


function _makeSelection(a, e){
	var kc = getKeyCode(e);
	
	if(kc == 40){
		alert(document.getElementById(a).innerHTML);
		//document.getElementById('sp').innerHTML = document.getElementById('autosuggest').innerHTML;
		//document.getElementById('a'+ca).style.backgroundColor = '#f2f2f2 !important';
		ca++;
		if(ca >= 10)
			ca = 1;
	}
}
</script>
</head>
<body>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<div class="tbbingouter yellowstrip">
  <div class="main">
    <ul class="myaccount_tab">
    	<li>You booking could not confirmed</li>
    </ul>
  </div>
</div>
<div class="clr"></div>
<?php
	if(isset($_GET["bookid"]))
	{
		$bookID = $_GET["bookid"];	
	}			
?>
<!--Middle Start Here-->
<div class="main">

<div class="myprofile">
	<div class="heading">Error details <span id="msg"><!-- <a href="javascript:void(0)" onclick="javascript: _cancelBooking('<?php //echo $bookID; ?>', '<?php //echo $cciid; ?>');">Cancel Booking</a> --></span></div>
	<div class="tripdetails">  
<?php 
	$city = $_SESSION["PickupSubUrb"];
	$displayCity = "";
	if($city == 1)
		$displayCity = "Delhi";
	if($city == 2)
		$displayCity = "Mumbai";
	if($city == 3)
		$displayCity = "Bangalore";
	if($city == 4)
		$displayCity = "Hyderabad";
?>
	<p><strong>ERROR:</strong> <?php echo $bookID; ?><br /><br /><a href="<?php echo ecWebRoot; ?>">Click here to try again.</a></p>
</div>
<input type="hidden" name="hdBookingID" id="hdBookingID" value="<?php echo $bookID; ?>" />
<input type="hidden" name="hdCCIID" id="hdCCIID" value="<?php echo $cciid; ?>" />
<div class="tripsmap">
	<!-- <iframe width="439" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=<?php echo urlencode($orgName); ?>&amp;daddr=<?php echo urlencode($dwstate); ?>&amp;hl=en&amp;aq=1&amp;oq=<?php echo urlencode($orgName); ?>&amp;sll=<?php echo $ll; ?>&amp;mra=ls&amp;ie=UTF8&amp;ll=<?php echo $ll; ?>&amp;t=m&amp;z=4&amp;iwloc=addr&amp;output=embed"></iframe> -->
</div>
<div class="clr"></div>
<!-- <div class="heading">Share Feedback/Suggestion</div>
<div class="trip_suggestion">
<p><label>Comment</label><textarea></textarea></p>
<p><label>&nbsp;</label><input type="submit" value="" /></p>
</div> -->
<div class="clr"></div>
</div>

</div>

<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

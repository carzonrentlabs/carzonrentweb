<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Car Rental Business Enquiry</title>
<meta name="description" content="To contact the Carzonrent about our car rental business, business car rental and car rental services. Just fills out the form for all your business rental needs."/>
<meta name="keywords" content="car rental business, business car rental, rental car business, business car rentals, small business car rental, business rental car, car rental for business, car rental enquiry" />

<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder">
		<div class="magindiv" style="padding:7px 0px 7px 0px;">
			<h2>Business Enquiries </h2>
		</div>
	</div>	
	<div style="margin:0 auto;width:968px;">
			<div class="divtabtxt">
				<span class="txtcar" style="margin:30px 0px 15px 40px">If you have a business need, please get in touch filling the form given below <br/>or simply mail us at<a class="txtmail" href="mailto:humanresources@carzonrent.com"> humanresources@carzonrent.com</a> </span>
				<div class="businessEnquiry"  style="margin-left:40px">
				<form action="send-business-enquiries.php" method="post" name="frmBusinessEnquiries" id="frmBusinessEnquiries">
					<table width="600px" cellpadding="0" cellspacing="10" border="0" align="left">
						<tr>
							<td align="right" valign="middle"><label>Name</label></td>
							<td align="left" valign="top" ><input type="text" name="txtName" id="txtName"  /></td>	 
						</tr>
						<tr>
							<td align="right" valign="middle"><label>Company Name</label></td>
							<td align="left" valign="top" ><input type="text" name="txtCompany" id="txtCompany" /></td>
						</tr>
						<tr>
							<td align="right" valign="middle"><label>Contact Number</label></td>
							<td align="left" valign="top" ><input type="text" name="txtContactNum" id="txtContactNum" /></td>
						</tr>
						<tr>
							<td align="right" valign="middle"><label>Designation</label></td>
							<td align="left" valign="top" ><input type="text" name="txtDesignation" id="txtDesignation" /></td>
						</tr>
						<tr>
							<td align="right" valign="middle"><label>Email Id</label></td>
							<td align="left" valign="top" ><input type="text" name="txtEmailID" id="txtEmailID" /></td> 
						</tr>									
						<tr>
							<td align="right" valign="top"><label>Your Requirement </label>
							<td align="left" valign="top" ><textarea cols="30" rows="4" name="txtRequirement" id="txtRequirement"></textarea></td> 
						</tr>	
						<tr>
							<td align="right" valign="middle"></td>
							<td align="left" valign="top" > <div class="submit"><a href="javascript:void(0)" onclick="javascript: _sendBusinessEnquiries();"></a></div></td>
						</tr>
					</table>
				</form>
				</div>
				</div>
				<div class="imagediv imghand">
					<img src="./images/keyboard.png" border="0" width="229px" height="297px"/>
				</div>
	</div>	
</div>
<script type="text/javascript">
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/cache-func.php");
	include_once('./classes/cor.mysql.class.php');
	$rurl = corWebRoot;
	if(isset($_GET["rurl"]))
		$rurl = $_GET["rurl"];
	else {
		$rurl = $_SERVER['HTTP_REFERER'];
		if(stripos($rurl, "easycabs.com") !== false){
			$rurl = ecWebRoot . "/ccurl.php";
			if(isset($_COOKIE["cciid"]) && isset($_COOKIE["emailid"]) && isset($_COOKIE["phone1"]) && isset($_COOKIE["gname"])){
				$rurl .= "?cc=" . $_COOKIE["cciid"] . "&em=" . $_COOKIE["emailid"] . "&ph=" . $_COOKIE["phone1"] . "&gn=" .$_COOKIE["gname"];
				header("Location: " . $rurl);
			}
		}
		elseif(stripos($rurl, "carzonrent.com") === false)
			$rurl = corWebRoot;
		elseif(stripos($rurl, "new-password.php") !== false)
			$rurl = corWebRoot;
	}
	
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers book a cab or rent a car for airport transfer car rentals, online economy car rentals/car hiring and hertz car rental solutions." />
<meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental india, car rental company india, budget/executive car rentals, economy car rental services, car hire services india, airport transfer car rental services, luxury cars on rent" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<script language="javascript" type="text/javascript">
var id;
$(document).ready(function(eOuter) {
    $('input').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });
	$('#txtPassword').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
			
			_doLogin();
		}
	});
	
});
</script>
</head>
<body> 

<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle" >	
	<div class="yellwborder" style="margin-top: 20px;">
		<div class="yellodivtxt">
			<h2>Login</h2>
		</div>
	</div>
<div class="main">

<br /><br />
<div class="businessEnquiry">
  
 
<form action="./do-login.php" method="post" id="frmLogin" name="frmLogin">
	<input type="hidden" id="hdRURL" name="hdRURL" value="<?php echo $rurl; ?>" />
<table width="75%" cellpadding="0" cellspacing="10" border="0" align="left" class="border-radius"> 
<tr>
<td colspan="2">
  <?php if(isset($_GET["resp"])) {
	if($_GET["resp"] == "e"){
?>
<p style="color:red;">Invalid mobile number/password.</p>
<?php
	}
  }
  ?>
</td>
</tr> 
<tr>
	<td align="left" valign="middle" colspan="2">	
			<span class="regi_l"><a href="<?php echo $rurl ?>"><img src="<?php echo corWebRoot; ?>/images/512_512_logo.png" width="50" height="50" alt="We take You Places" title="We take You Places"/></a>
			</span>
			<span class="regi_r">
			<span class="heading">Enter details below to access your account.</span>
			</span>
    </td>
</tr>               
                    
    <!--<tr>
		<td align="right" valign="middle"><label>Tour Type</label></td>
                <td align="left" valign="top" >   
                    <select name="tourtype" class="service_type">
                        <option value="1">Selfdrive</option>
                        <option value="2">Outstation/Local</option>
                    </select>
                </td>	     
	</tr>-->
    <tr>
                    	<td align="right" valign="middle"><label>Mobile Number</label></td>
                        <td align="left" valign="top" ><input type="text" name="txtMobile" tabindex="1" id="txtMobile" maxlength="10" onKeyPress="javascript: return _allowNumeric(event);"  /></td>
                                     
                    </tr>
			<tr>
                    	<td align="right" valign="middle"><label>Password</label></td>
                        <td align="left" valign="top" ><input type="password" name="txtPassword" tabindex="2" id="txtPassword" /></td>

					
					<tr>
                    	<td align="right" valign="middle"></td>
                        <td align="left" valign="top" ><div class="forgotPassword"><a href="./forgot-password.php">Forgot password?</a>&nbsp;<a href="./register.php">New User?</a></div></td>
						
                                     
                    </tr>
			<tr>
                    	<td align="right" valign="middle"></td>
                        <td align="left" valign="top" > <div class="submit"><a href="javascript:void(0)" onclick="javascript: _doLogin();" tabindex="3"></a></div></td>
						
                                     
                    </tr>   

			
					
                    </table>

</form></div>

  <div class="clr"></div>

  <p>&nbsp;</p>

</div>
</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>


</div>
</div>

<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

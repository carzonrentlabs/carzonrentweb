<?php    
	/* 
		API Class
 	*/	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "carzonrent-db.cwhd81c19z15.ap-southeast-1.rds.amazonaws.com";
		const DB_USER = "root";
		const DB_PASSWORD = "N2o13delhimY";
		const DB = "cor_by_bytesbrick";
		const SITEURL = "http://www.carzonrent.com/rest/";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();	
				// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['request'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/*
		* getPrivateKey method
		* Description: get private key 
		*/
		
		    private function getCustomerSearch(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
			$obj = new BaseResponse();
			$obj->status = 0;
			$obj->msg = "Unauthorized request";
			$obj->errNo = 0;
			$this->response(json_encode($obj), 200);
			} 
			//for date wise filtartion
			//where entry_date >= '2015/02/25' and entry_date <= '2015/06/27'
			$startdate=$this->_request['startdate'];
			$enddate = $this->_request['enddate'];
			$PageNo = $this->_request['PageNo'];
			$PageSize = $this->_request['PageSize'];
			
			    //for pagination
				$Page = ($PageNo != "" || $PageNo != 0)?$PageNo:1;
				if($Page == 1) {
					$limit_str = " limit " . ($Page - 1) . "," . $PageSize;
				}else {
				    $limit_str = " limit " . ($Page * $PageSize) . "," . $PageSize;
				}

		    $where='where 1=1';
			if(!empty($startdate) && !empty($enddate))
			{
			$where ='where cs.entry_date >= "'.$startdate.'" and cs.entry_date <= "'.$enddate.'" and cs.website="%myles%"';
			}
		    $sql="SELECT cs.refsite,cs.adunit,cs.channel,cs.campaign,(count(cs.refsite)) AS hits,(count(bn.booking_id)) AS bookingcount FROM customer_search AS cs LEFT JOIN cor_booking_new as bn on cs.coric=bn.coric ".$where.' group by cs.refsite order by  cs.unique_id DESC '.$limit_str;
			//echo $sql;exit;
			$prj = array();
			$res = mysql_query($sql, $this->db);
			if(mysql_num_rows($res) > 0){								
				while($arr = mysql_fetch_array($res,MYSQL_ASSOC)){
				  $prj[] = $arr;
				}								
			}				
			$sql1 = "SELECT FOUND_ROWS() AS `found_rows`";
			$result = mysql_query($sql1, $this->db);
			if(mysql_num_rows($result) > 0){
				$ct = mysql_fetch_array($result,MYSQL_ASSOC);			
				$count = $ct['found_rows'];
			}
			
			$prjData = new CommentList();
			$prjData->TotalRecords = $count;
			$prjData->SearchData = array_values($prj);
			
			$obj = new ResponseData();
			$obj->status = 1;
			$obj->msg = "Customer Search";
			$obj->errNo = 0;
			$obj->data = json_encode($prjData);
			$this->response(json_encode($obj), 200);
		}	
				
		
		
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	class BaseResponse{
		public $status;
		public $errNo;
		public $msg;
	}
	
	class ResponseData extends BaseResponse {	
		public $data;
	}
	
	class ResponseItem extends BaseResponse {	
		public $item;
	}
	class commentList{
		public $TotalRecords;
		public $SearchData;
	}
	// Initiate Library
	
	$api = new API;
	$api->processApi();
?>
<?php
    error_reporting(0);
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    include_once('./classes/cor.ws.class.php');
    include_once('./classes/cor.xmlparser.class.php');
    include_once("./cache-func.php");
    include_once('./classes/cor.mysql.class.php');
    setcookie("tcciid", "", time()-60);
    $cor = new COR();
    $res = $cor->_CORGetCities();	
    $myXML = new CORXMLList();
    $org = $myXML->xml2ary($res);
    $orgH = $org;
    $orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    $orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    $orgH[] = array("CityID" => 39, "CityName" => "Manesar");
    $orgH[] = array("CityID" => 41, "CityName" => "Bhubaneswar");
    $res = $cor->_CORGetDestinations();
    $des = $myXML->xml2ary($res);
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js-no-ga.php"); ?>
<script type="text/javascript">
	$(function(){
		$('select.styled').customSelect();
		$('#slidesOutstation').slides({
			preload: false,
			preloadImage: '/images/loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
			
				$('.caption').animate({
					bottom:-35
				},100);
			},
			animationComplete: function(current){
			
				$('.caption').animate({
					bottom:0
				},200);
			},
			slidesLoaded: function() {
			
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
	});

	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
	arrDestination = new Array();
<?php
	for($i = 0; $i < count($des); $i++){
?>
		arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
</script>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js"></script>
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>
<div class="banner">
    	<div class="lfts">
        <div class="book">Book a Cab for</div>
        <div id="TripleTabs">
		<div id="outstationFrm" class="tber">
		<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
                <form id="form1" name="form1" action="<?php echo corWebRoot; ?>/search-result.php" method="post" target="_top">
		<input type="hidden" name="hdOriginName" id="hdOriginName" />
		<input type="hidden" name="hdOriginID" id="hdOriginID" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationName" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationID" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Outstation" />
		<input type="hidden" name="chkPkgType" id="chkPkgType" value="Daily" />
		<input type="hidden" name="refclient" id="refclient" value="<?php echo $_SESSION['refclient']; ?>" />
                    <div class="bdrbtm">
			<?php include_once("./includes/m-city-popup.php"); ?> 
                        <p>
                            <label>Starting From</label>
	                        <span class="location">
                                <select name="ddlOrigin" id="ddlOrigin" class="styled" onchange="javascript: _setOrigin(this.id, 'o');">
                                    <option value="">Select City</option>
                                    <?php
                                    $selCityId = 2;
                                    $selCityName = "Delhi";
                                    for($i = 0; $i < count($org); $i++)
                                    {
                                        if($org[$i]["CityName"] == $cCity){
                                            $selCityId = $org[$i]["CityID"];
                                            $selCityName = $org[$i]["CityName"];
                                            ?>
                                            <option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
                                            <script type="text/javascript" language="javascript">
                                            document.getElementById('hdOriginID').value = '<?php echo $org[$i]["CityID"]; ?>';
                                            document.getElementById('hdOriginName').value = '<?php echo $org[$i]["CityName"]; ?>';
                                            </script>
                                            <?php
                                        }
                                        else {
                                            ?>
                                            <option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </span>
                        </p>
                        <p>
				<label>Travelling to</label>
				<span class="cities">
					<input type="text" name="txtDestination" id="txtDestination" value="" autocomplete="off" onkeyup="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest', arrDestination, 0, 0, 0);" />
					<div id="autosuggest" class="autosuggest floatingDiv"></div>
				</span>
                        </p>
                    </div>
                    <p>
                        <label>Date of Travel</label>
                        <span class="datepick">
				<input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField" class="datepicker" onchange="javascript: _setDropDate('inputField', 'inputField2');" />
                        </span>	
                    </p>
                    <p>
                        <label>Date of Return</label>
	                    <span class="datepick">
	                        <input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo date('d M, Y'); ?>" id="inputField2" class="datepicker" />
	                    </span>
                    </p>
			<p>
                        <label>&nbsp;</label>
                        <span class="mkebtn" style="float:left;">
                            <!--<a href="javascript: void(0);">Make Your Booking Now</a>-->
			    <input type="button"  name="btnMakePayment" value="Make Your Booking Now" onclick="javascript: return _validate();" /><br />
			    
                        </span>
                    </p>
			<p>
				<label>&nbsp;</label>
				<a href="javascript:void(0)" class="multi">Book A Car For A Multi City Trip</a>
			</p>
                </form>
            </div>
        </div>
        <!--TripleTabs Ends Here-->
        </div>
    <div class="rgts">
		<!--div class="righttb">
	  <a href="http://www.mylescars.com/landing/First_time/tnc?utm_source=Carzonrent&utm_medium=Banner&utm_campaign=carzonrent-banner-700off" target="_blank"><img src="<?php echo corWebRoot; ?>/images/Flat-700off-Self-Drive-Car-Cor.jpg" alt="<?php echo $imageval['alt_desc'];?>" title="<?php echo $imageval['img_title'];?>" width="346px" height="80px" /></a>		
	</div-->
        <div class="righttb">
	    <div id="slidesOutstation">
		<div class="slides_container" style="height: 390px;">
		 <?php
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			$sql_slider = "select * from cor_local_banner where banner_type='outstation' and status='1' ORDER BY `orderimage` ASC";
		    $slider = $db->query("query", $sql_slider);
			$db->close();
			?>
			    <?php
                foreach($slider as $imageval)		
				{			
				?>			
				<div class="slide">
				<a href="<?php echo $imageval['link_url'];?>" target="_blank"><img src="<?php echo corWebRoot; ?>/images/<?php echo $imageval['image'];?>" alt="<?php echo $imageval['alt_desc'];?>" title="<?php echo $imageval['img_title'];?>" width="346px" height="392px" /></a>
				</div>
				<?php  } ?>
				<!---<div class="slide"><a href="<?php echo corWebRoot; ?>/EasyCabsMobileApp/" target="_blank"><img src="<?php echo corWebRoot; ?>/images/cor_banner1.jpg" alt="Get the Carzonrent App and avail discounts on Outstation, Radio Taxi and Self-Drive booking." title="Get the Carzonrent App and avail discounts on Outstation, Radio Taxi and Self-Drive booking." width="346px" height="392px" /></a></div>
				<div class="slide"><a href="<?php echo corWebRoot; ?>/mylescity-drive/" target="_blank"><img src="<?php echo corWebRoot; ?>/images/141009_COR_Myles_nano_banner.png" alt="Book Online and Win Exciting Prizes Every Week - EasyCabs.com" title="Book Online and Win Exciting Prizes Every Week - EasyCabs.com" width="344px" height="390px" /></a></div>
				<div class="slide"><a href="javascript:void(0);"><img src="<?php echo corWebRoot; ?>/images/year14-0.jpg" alt="India's Leading Self-Drive Service - Carzonrent.com" title="India's Leading Self-Drive Service - Carzonrent.com" width="344px" height="390px" /></a></div>
				<div class="slide"><a href="javascript:void(0);"><img src="<?php echo corWebRoot; ?>/images/year14-1.jpg" alt="India's Leading Self-Drive Service - Carzonrent.com" title="India's Leading Self-Drive Service - Carzonrent.com" width="344px" height="390px" /></a></div>
				<div class="slide"><a href="<?php echo corWebRoot; ?>/cor-payback.php" target="_blank"><img src="<?php echo corWebRoot; ?>/images/131111_COR_Payback-web-banner-01.jpg" alt="It Pays to go places with us Earn PAYBACK Points with every Local, Outstation and Self-Drive Rental or EasyCabs booking." title="It Pays to go places with us Earn PAYBACK Points with every Local, Outstation and Self-Drive Rental or EasyCabs booking." width="344px" height="390px" /></a></div>
				<div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/leave-town-for-less.jpg" alt="Leave Town for Less - Budget cars now available for only Rs 10/km - Carzonrent.com" title="Leave Town for Less - Budget cars now available for only Rs 10/km - Carzonrent.com" width="344px" height="390px" /></a></div>
				<div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/130606_COR_WebBanner3.jpg" alt="Planning a business trip. We have prices you can not refuse - Carzonrent.com" title="Planning a business trip. We have prices you can not refuse - Carzonrent.com" width="344px" height="390px" /></a></div>
				<div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/130607_COR_WebBanner1.jpg" alt="Take advantage of our transparent pricing. Our family cars now available at Rs 13/KM only - Carzonrent.com" title="Take advantage of our transparent pricing. Our family cars now available at Rs 13/KM only - Carzonrent.com" width="344px" height="390px" /></a></div>--->
		</div>
	    </div>
        </div>
    </div>
</div>
<!--Middle Start Here-->

</body>
</html>

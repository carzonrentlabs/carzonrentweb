﻿<?php
  error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php //include_once("./includes/seo-metas.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />

</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header-js.php");
	include_once("./includes/header.php"); 
	include_once('./classes/cor.mysql.class.php');
	include_once("./includes/dbconnect.php");
?>
<!--Banner Start Here-->

<!--Middle Start Here-->


<div id="middle" style="padding-top:10px;">
<h1 class="yellwborder" style="background-color:#F2C900;">
<div class="w1130">Terms of Use</div>
</h1>
<div class="main">

<div class="aboutus">



<div id="country1" class="tabcontent">
  
  <div style="padding-top:5px; margin-left:5px; marhin-right:5px; border:none;">
  <?php 
  $db = new MySqlConnection(CONNSTRING);
  $db->open();
  
  $sql = "select * from cms where pagename='term of use'";
  $record = $db->query("query", $sql);
  
 // $seletQuery="select * from cms where pagename='term of use'";
 // $querydata=mysql_query($seletQuery);
  //$record=mysql_fetch_array($querydata);
  echo $record[0]['page_desc'];
  $db->close();
  ?>
  </div>
  <div class="clr"></div>


<div class="term_head">Following conditions will be considered as Myles car misuse.  Customers will be fined if found involved in any of the following:</div>
<ol>
<li><strong>Over speeding:</strong> Users are allowed to drive the Vehicle up to a maximum speed of 100 km/hr (63 miles/hr), beyond which the Vehicle will be considered as over speeding. A penalty of Rs. 200 will be charged on the first instance, followed by a further penalty of Rs. 500 and Rs. 1000 for 2nd instance and 3rd instance respectively making it a total of Rs. 1,700/- at third instance. After the third instance, Booking will stand cancelled and the Company shall get the right to take back the Vehicle and terminate this Agreement.</li>
<li><strong>Traffic violation:</strong> Customers are liable to pay for any traffic violation tickets received during their journey. Carzonrent can charge traffic violation from the customer’s credit/debit card on actuals if the violations tickets are sent directly to the company by the traffic department.</li>
<li><strong>Car spare part changed:</strong> Customers should not change or remove any car spare parts. In case of emergency, the customer should inform Carzonrent and act as per advice. Customers will be charged a penalty of Rs. 5000 over and above the cost of spare part.</li>
<li><strong>Tyre misuse:</strong> In case of any tyre damages resulting from driving in bad terrain and continued driving in case of tyre puncture, customers will be charged for the cost of tyre on actuals.</li>
<li><strong>Running vehicle in damaged conditions:</strong> Customers are not advised to drive the car if it gets damaged in an accident. Customers are advised to inform Carzonrent immediately. In such case, customers will be charged for the cost of spare parts on actuals.</li>
<li><strong>Unauthorized activity in the car:</strong> Customers are not allowed to carry arms, ammunitions and banned drugs. In addition, use of car for commercial activity such as product sell and promotion, and carry goods is strictly prohibited. In such cases, customers will be charged a penalty of Rs. 5000.</li>
<li><strong>External branding:</strong> Any form of external branding on the car is prohibited. Customers are not allowed to paste or paint any external brand promotion on the car. Customers will have to pay a penalty of Rs. 5000.</li>
<li><strong>Tampering with devices:</strong> Customers are not allowed to tamper with the odometer, GPS device and in-car technology devices. Customers will have to pay a penalty of Rs. 1000 and over in addition to the actual cost of the device.</li>
<li><strong>Deliberately driving the car in water:</strong> Customers will be charged for the actual cost of repair and spare parts.</li>
<li><strong>Producing fake and tampered personal documents:</strong> Customers will be charged a penalty of Rs. 1000 if found guilty.</li>
<li><strong>Diversion:</strong> Customers are not allowed to drive the car into unauthorized or government banned areas. Customers are advised to inform Carzonrent in case they change the course of their trip. All Myles cars are geo-fenced and customers have to pay a penalty Rs. 10,000 if the car trespasses into banned areas, naxal hit areas, international border of republic of India and extreme end of ladakh.</li>
</ol>

</div>


</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>

<?php include_once("./includes/footer.php"); ?>
</body>
</html>

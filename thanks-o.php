<?php
    //error_reporting(E_ALL);
    //ini_set("display_errors", 1);
    session_start();
    include_once("./includes/cache-func.php");
    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
    $xmlToSave .= "<ccvtrans>";    
    $query_string = "";
    if ($_POST) {
      $kv = array();
      foreach ($_POST as $key => $value) {
	    if($key != "corpassword"){
	    $kv[] = "$key=$value";
	      $xmlToSave .= "<$key>$value</$key>";
	    }
      }
      $query_string = join("&", $kv);
    }
    else {
      $query_string = $_SERVER['QUERY_STRING'];
    }
    $xmlToSave .= "</ccvtrans>";
    if(!is_dir("./xml/trans/" . date('Y')))
      mkdir("./xml/trans/" . date('Y'), 0775);
    if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
      mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
    if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
      mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
    if(isset($_REQUEST['Order_Id']))
      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . ".xml");
    else
      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . uniqid("F-") . ".xml");
    
    require_once("./includes/libfuncs.php");
        $WorkingKey = "72dxgbita13wdybvv9ar8du7bzizgqhq" ; //put in the 32 bit working key in the quotes provided here
	$Merchant_Id= $_REQUEST['Merchant_Id'];
	$Amount= $_REQUEST['Amount'];
	$Order_Id= $_REQUEST['Order_Id'];
	$Merchant_Param= $_REQUEST['Merchant_Param'];
	$Checksum= $_REQUEST['Checksum'];
	$AuthDesc=$_REQUEST['AuthDesc'];
        $ChecksumTF = verifyChecksum($Merchant_Id, $Order_Id , $Amount,$AuthDesc,$Checksum,$WorkingKey);
    
    if($ChecksumTF == true && isset($_REQUEST["AuthDesc"])){
        include_once('./classes/cor.ws.class.php');
        include_once('./classes/cor.xmlparser.class.php');
        include_once('./classes/cor.gp.class.php');
        
        if($_REQUEST["AuthDesc"] == "Y" || $_REQUEST["AuthDesc"] == "B"){
            //$mParams = explode("|", $Merchant_Param);
            //$query_string = "hdOriginName=&hdDestinationName=&hdCat=&hdCarModel=&hdTourtype=&hdDestinationName=&hdPickdate=&picktime=&hdDropdate=&address=&totFare=&rdoPayment=&dayRate=&kmRate=&duration=";
            $url = "";
            if(isset($_SESSION["rurl"])){
                $url = $_SESSION["rurl"];
                unset($_SESSION["rurl"]);
            }
            $fullname = explode(" ", $_SESSION["name"]);
            if(count($fullname) > 1)
            {
                $firstName = $fullname[0];
                $lastName = $fullname[1];
            }
            else
            {
                $firstName = $fullname[0];
                $lastName = "";
            }
            $cor = new COR();
            //$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_SESSION["monumber"],"clientid"=>1684));
	    
            $myXML = new CORXMLList();
            //$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
            //if(intval($arrUserId[0]["ClientCoIndivID"]) > 0){
                $mtime = round(microtime(true) * 1000);
		if($_SESSION["hdTourtype"] == "Selfdrive"){
		  $pkgId = $_SESSION["pkgId"];
		  $destination = $_SESSION["hdDestinationName"];
		  $origin = $_SESSION["hdOriginName"];
		  $TimeOfPickup = $_SESSION["picktime"];
		  $address = $_SESSION["address"];
		  $phone = $_SESSION["monumber"];
		  $emailId = $_SESSION["email"];
		  $userId = $_SESSION["cciid"]; //$arrUserId[0]["ClientCoIndivID"];
		  $paymentAmount = $_REQUEST["Amount"];
		  //$paymentType = $_POST["rdoPayment"];
		  $paymentType = "1";
		  $paymentStatus = "1";
		  $trackId = $_REQUEST["Order_Id"];
		  $transactionId = $_REQUEST["nb_order_no"];
		  $discountPc = $_SESSION["dispc"];
		  $discountAmt = $_SESSION["discountAmt"];
		  $empcode = $_SESSION["empcode"];
		  if(trim($empcode) == "Promotion code")
		    $empcode = "";
		  $disccode = $_SESSION["disccode"];
		  if(trim($disccode) == "Discount coupon number")
		    $disccode = "";
		  $remarks = $_SESSION["remarks"];
		  $visitedCities = $_SESSION["hdDestinationName"];
		  $outStationYN = "true";
		  if(isset($_COOKIE["corsrc"]))
		    $srcCookie = $_COOKIE["corsrc"];//Aamir 1-Aug-2012
		  if(isset($_COOKIE["gclid"]))
		    $srcCookie = $_COOKIE["gclid"];//Iqbal 10-Oct-2012
		  $tourType = $_SESSION["hdTourtype"];
		  if($_SESSION["hdTourtype"] != "Local")
		      $outStationYN = "true";
		  else
		      $outStationYN = "false";
		  
		  if(isset($_SESSION["hdPickdate"]))
		  {
		      $pDate =  $_SESSION["hdPickdate"];
		      $pickDate = date_create($pDate);
		  }
		  if(isset($_SESSION["hdDropdate"]))
		  {
		      $dDate =  $_SESSION["hdDropdate"];
		      $dropDate = date_create($dDate);
		  }
		  $dateOut = $pickDate->format('Y-m-d');
		  $dateIn = $dropDate->format('Y-m-d');
		  
		  if(isset($_SESSION["droptime"]))
		      $dTime =  $_SESSION["droptime"];
	  
		  $corArrSTD = array();
		  $corArrSTD["pkgId"] = $pkgId;
		  $corArrSTD["PickUpdate"] = $dateOut;
		  $corArrSTD["PickUptime"] = $TimeOfPickup;
		  $corArrSTD["DropOffDate"] = $dateIn;
		  $corArrSTD["DropOfftime"] = $dTime;
		  $corArrSTD["FirstName"] = $firstName;
		  $corArrSTD["LastName"] = $lastName;
		  $corArrSTD["phone"] = $phone;
		  $corArrSTD["paymentMode"] = "1";
		  $corArrSTD["emailId"] = $emailId;
		  $corArrSTD["userId"] = $userId;
		  $corArrSTD["paymentAmount"] = $paymentAmount;
		  $corArrSTD["paymentType"] = $paymentType;
		  $corArrSTD["paymentStatus"] = $paymentStatus;
		  $corArrSTD["visitedCities"] = $visitedCities;
		  $corArrSTD["AdditionalService"] = $totalKM;
		  $corArrSTD["trackId"] = $trackId;
		  $corArrSTD["transactionId"] = $transactionId;
		  $corArrSTD["remarks"] = $remarks;
		  $corArrSTD["chkSum"] = $Checksum;
		  $corArrSTD["OriginCode"] = $srcCookie;
		  $corArrSTD["discountPc"] = $discountPc;
		  $corArrSTD["discountAmount"] = $discountAmt;
		  $corArrSTD["DiscountCode"] = $disccode;
		  $corArrSTD["PromotionCode"] = $empcode;
		  //print_r($corArrSTD);
		  $res = $cor->_CORSelfDriveCreateBooking($corArrSTD);
		  //print_r($res);
		  $myXML = new CORXMLList();
		  if($res->{'SelfDrive_CreateBookingResult'}->{'any'} != ""){
                    $arrUserId = $myXML->xml2ary($res->{'SelfDrive_CreateBookingResult'}->{'any'});
                    if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){
                        $url = corWebRoot . "/self-drive/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			$xmlToSave .= "<cortrans>";
			$xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
			$xmlToSave .= "</cortrans>";
			
			$dataToSave = array();
			$dataToSave["BookingID"] = intval($arrUserId[0]["Column1"]);
			$dataToSave["PaymentStatus"] = 1;
			$dataToSave["PaymentAmount"] = intval($paymentAmount);
			$dataToSave["TransactionId"] = $_REQUEST["nb_order_no"];
			$dataToSave["ckhSum"] = $Checksum;
			$dataToSave["Trackid"] = $trackId;
			$res = $cor->_CORSelfDriveUpdatePayment($dataToSave);
			unset($dataToSave);
                    }
                    else{
                        $url = str_replace("search-result.php", "booking-fail.php", $url);
			$xmlToSave .= "<cortrans>";
			$xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			$xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			$xmlToSave .= "</cortrans>";
			
			$dataToMail= array();
			$dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			$dataToMail["Subject"] = "Booking Failure: " . $trackId;
			$dataToMail["MailBody"] = $xmlToSave;
		      
			$res = $cor->_CORSendMail($dataToMail);
			unset($dataToMail);
                    }
		  }
		}
		else {
                $pkgId = $_SESSION["pkgId"];
                $destination = $_SESSION["hdDestinationName"];
		$origin = $_SESSION["hdOriginName"];
                $TimeOfPickup = $_SESSION["picktime"];
                $address = $_SESSION["address"];
                $phone = $_SESSION["monumber"];
                $emailId = $_SESSION["email"];
                $userId = $_SESSION["cciid"]; //$arrUserId[0]["ClientCoIndivID"];//$_SESSION["cciid"];
                $paymentAmount = $_REQUEST["Amount"];
                $paymentType = "1";
                $paymentStatus = "1";
                $trackId = $_REQUEST["Order_Id"];
                $transactionId = $_REQUEST["nb_order_no"];
                $discountPc = $_SESSION["dispc"];
		$discountAmt = $_SESSION["discountAmt"];
		$empcode = $_SESSION["empcode"];
		if(trim($empcode) == "Promotion code")
		  $empcode = "";
		$disccode = $_SESSION["disccode"];
		if(trim($disccode) == "Discount coupon number")
		  $disccode = "";
                $remarks = $_SESSION["remarks"];
                $totalKM = $_SESSION["hdDistance"];
                $visitedCities = $_SESSION["hdDestinationName"];
                $outStationYN = "true";
		$srcCookie = "";
		if(isset($_COOKIE["corsrc"]))
		  $srcCookie = $_COOKIE["corsrc"];//Aamir 1-Aug-2012
		if(isset($_COOKIE["gclid"]))
		  $srcCookie = $_COOKIE["gclid"];//Iqbal 10-Oct-2012					
		$tourType = $_SESSION["hdTourtype"];
                if($_SESSION["hdTourtype"] != "Local")
                    $outStationYN = "true";
                else
                    $outStationYN = "false";
                if(isset($_SESSION["hdPickdate"]))
                {
                    $pDate =  $_SESSION["hdPickdate"];
                    $pickDate = date_create($pDate);
                }
                if(isset($_SESSION["hdDropdate"]))
                {
                    $dDate =  $_SESSION["hdDropdate"];
                    $dropDate = date_create($dDate);
                }
                $dateOut = $pickDate->format('m/d/Y');
                $dateIn = $dropDate->format('m/d/Y');
                
                $corArrSTD = array();
                $corArrSTD["pkgId"] = $pkgId;
                $corArrSTD["destination"] = $destination;
                $corArrSTD["dateOut"] = $dateOut;
                $corArrSTD["dateIn"] = $dateIn;
                $corArrSTD["TimeOfPickup"] = $TimeOfPickup;
                $corArrSTD["address"] = nl2br($address);
                $corArrSTD["firstName"] = $firstName;
                $corArrSTD["lastName"] = $lastName;
                $corArrSTD["phone"] = $phone;
                $corArrSTD["emailId"] = $emailId;
                $corArrSTD["userId"] = $userId;
                $corArrSTD["paymentAmount"] = $paymentAmount;
                $corArrSTD["paymentType"] = $paymentType;
                $corArrSTD["paymentStatus"] = $paymentStatus;
                $corArrSTD["trackId"] = $trackId;
                $corArrSTD["transactionId"] = $transactionId;
                $corArrSTD["discountPc"] = $discountPc;
		$corArrSTD["discountAmount"] = $discountAmt;
                $corArrSTD["remarks"] = $remarks;
                $corArrSTD["totalKM"] = $totalKM;
                $corArrSTD["visitedCities"] = $visitedCities;
                $corArrSTD["outStationYN"] = $outStationYN;
		$corArrSTD["OriginCode"] = $srcCookie; //Aamir 1-Aug-2012
		$corArrSTD["chkSum"] = $Checksum;
		$corArrSTD["DisCountCode"] = $disccode;
		$corArrSTD["PromotionCode"] = $empcode;
		
                $res = $cor->_CORMakeBooking($corArrSTD);
		
                $myXML = new CORXMLList();
                if($res->{'SetTravelDetailsResult'}->{'any'} != ""){
                    $arrUserId = $myXML->xml2ary($res->{'SetTravelDetailsResult'}->{'any'});
                    if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){
                        //$url = str_replace("search-result.php", "my-account-trip-details.php", $url);
                        //$url .= "?resp=booksucc&bookid=" . $arrUserId[0]["Column1"];
			if($tourType != "Local")
				$url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "-to-" . str_ireplace(" ", "-", strtolower($destination)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			else
				$url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			//$url = corWebRoot . "/" . strtolower($tourType) . "/booking-confirm/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			$xmlToSave .= "<cortrans>";
			$xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
			$xmlToSave .= "</cortrans>";
                    }
                    else{
                        $url = str_replace("search-result.php", "booking-fail.php", $url);
			$xmlToSave .= "<cortrans>";
			$xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			$xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			$xmlToSave .= "</cortrans>";
			
			$dataToMail= array();
			$dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			$dataToMail["Subject"] = "Booking Failure: " . $trackId;
			$dataToMail["MailBody"] = $xmlToSave;
		      
			$res = $cor->_CORSendMail($dataToMail);
			unset($dataToMail);
                    }
                }
                else{
                    $url = str_replace("search-result.php", "booking-fail.php", $url);
		    $xmlToSave .= "<cortrans>";
		    $xmlToSave .= "<error2>" . serialize($res) . "</error2>";
		    $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";
		    $xmlToSave .= "</cortrans>";
		    
		    $dataToMail= array();
		    $dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
		    $dataToMail["Subject"] = "Booking Failure: " . $trackId;
		    $dataToMail["MailBody"] = $xmlToSave;
		  
		    $res = $cor->_CORSendMail($dataToMail);
		    unset($dataToMail);
                }
	      }
            
	    createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $trackId . ".xml");
            //createcache($xmlToSave, "./xml/trans/" . $trackId . ".xml");
            unset($cor);
            //echo "<br />" . $url;
            header('Location: ' . urldecode($url));
        }
    }
    else {
      $url = "booking-fail.php";
      $url .= "?resp=bookfail&bookid=" . $_SESSION["hdBookingID"];
      //echo "<br />" . $url;
      header('Location: ' . urldecode($url));
    }
?>
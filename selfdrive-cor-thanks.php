<?php
     //error_reporting(E_ALL);
     //ini_set("display_errors", 1);
	 
	 
     session_start();
     include_once("./includes/cache-func.php");
     include_once('./classes/cor.mysql.class.php');
     $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
     $xmlToSave .= "<ccvtrans>";    
     $query_string = "";
     if ($_POST) {
	  $kv = array();
	  
	  foreach ($_POST as $key => $value) {
	       if($key != "corpassword"){
		    $kv[] = "$key=$value";
		    $xmlToSave1 .= "<$key>$value</$key>";
	       }
	  }
	  $query_string = join("&", $kv);
     }
     else {
       $query_string = $_SERVER['QUERY_STRING'];
     }
     $xmlToSave .= "</ccvtrans>";
     if(!is_dir("./xml/trans/" . date('Y')))
     mkdir("./xml/trans/" . date('Y'), 0775);
     if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
     mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
     if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
     mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
     if(isset($_REQUEST['Order_Id']))
     createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . ".xml");
     else
     createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . uniqid("F-") . ".xml");
     
    require_once("./includes/libfuncs.php");
        $WorkingKey = "72dxgbita13wdybvv9ar8du7bzizgqhq" ; //put in the 32 bit working key in the quotes provided here
	$Merchant_Id= $_REQUEST['Merchant_Id'];
	$Amount= $_REQUEST['Amount'];
	$Order_Id= $_REQUEST['Order_Id'];
	$Merchant_Param= $_REQUEST['Merchant_Param'];
	$Checksum= $_REQUEST['Checksum'];
	$AuthDesc=$_REQUEST['AuthDesc'];
        $ChecksumTF = verifyChecksum($Merchant_Id, $Order_Id , $Amount,$AuthDesc,$Checksum,$WorkingKey);

    if($ChecksumTF == true && isset($_REQUEST["AuthDesc"])){
	  include_once('./classes/cor.ws.class.php');
	  include_once('./classes/cor.xmlparser.class.php');
	  include_once('./classes/cor.gp.class.php');
	  
	  if($_REQUEST["AuthDesc"] == "Y" || $_REQUEST["AuthDesc"] == "B"){
	    
	       if(isset($_REQUEST['Order_Id'])){
		    $db = new MySqlConnection(CONNSTRING);
		    $db->open();
		    $dataToSave = array();
		    $dataToSave["payment_mode"] = "1";
		    $dataToSave["payment_status"] = "1";
		    
		    $dataToSave["billing_cust_name"] = $_POST["billing_cust_name"];
		    $dataToSave["billing_cust_address"] = $_POST["billing_cust_address"];
		    $dataToSave["billing_cust_state"] = $_POST["billing_cust_state"];
		    $dataToSave["billing_zip_code"] = $_POST["billing_zip_code"];
		    $dataToSave["billing_cust_city"] = $_POST["billing_cust_city"];
		    $dataToSave["billing_cust_country"] = $_POST["billing_cust_country"];
		    $dataToSave["billing_cust_tel"] = $_POST["billing_cust_tel"];
		    $dataToSave["billing_cust_email"] = $_POST["billing_cust_email"];
		    $dataToSave["AuthDesc"] = $_POST["AuthDesc"];
		    $dataToSave["Checksum"] = $_POST["Checksum"];
		    $dataToSave["Merchant_Param"] = $_POST["Merchant_Param"];
		    $dataToSave["nb_bid"] = $_POST["nb_bid"];
		    $dataToSave["nb_order_no"] = $_POST["nb_order_no"];
		    $dataToSave["card_category"] = $_POST["card_category"];
		    $dataToSave["bank_name"] = $_POST["bank_name"];
		    $dataToSave["payment_update_date"] = "date('Y-m-d H:i:s')";
		    
		    $whereData = array();
		    $whereData["coric"] = $_REQUEST['Order_Id'];		
		    $r = $db->update("cor_booking_new", $dataToSave, $whereData);
		    unset($whereData);
		    unset($dataToSave);
		    $db->close();
	       }
	  
	       $url = "my-account-trip-detailscall.php";
	       $fullname = explode(" ", $_SESSION["name"]);
	       if(count($fullname) > 1){
		   $firstName = $fullname[0];
		   $lastName = $fullname[1];
	       } else {
		   $firstName = $fullname[0];
		   $lastName = "";
	       }
	       if(isset($_SESSION["DiscTxnID"]) && $_SESSION["DiscTxnID"] != "")
	       $DiscTxnID = $_SESSION["DiscTxnID"];
	       else
	       $DiscTxnID = "0";
	       if(isset($_SESSION["empcode"]) && $_SESSION["empcode"] != "")
	       $empcode = $_SESSION["empcode"];
	       else
	       $empcode = "0";
	       if(isset($_SESSION["discAmt"]) && $_SESSION["discAmt"] != "")
	       $discAmt = $_SESSION["discAmt"];
	       else
	       $discAmt = "0";
	       
	       $cor = new COR();
	       $res = $cor->_CORGetBookingsSelfdriveDetail(array("BookingID"=>$Merchant_Param));
	       $bookingDetails = array();
	       if($res->{'GetBookingDetailResult'}->{'any'} != ""){
		   $myXML = new CORXMLList();
		   $bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});
	       }
	       if($bookingDetails[0]["TourType"] == "Self Drive" || $bookingDetails[0]["TourType"] == "SelfDrive"){
	       //if(strtolower($bookingDetails[0]["TourType"]) == "self drive"){
		    $dataToSave = array();
		    $dataToSave["BookingID"] = intval($Merchant_Param);
		    $dataToSave["PaymentStatus"] = 1;
		    $dataToSave["PaymentAmount"] = $Amount;
		    $dataToSave["TransactionId"] = $_REQUEST["nb_order_no"];
		    $dataToSave["ckhSum"] = $Checksum;
		    $dataToSave["Trackid"] = $Order_Id;
		    $dataToSave["discountPc"] = "0";
		    $dataToSave["discountAmount"] = $discAmt;
		    $dataToSave["DisCountCode"] = $empcode;
		    $dataToSave["DiscountTransactionId"] = $DiscTxnID;
		    $res = $cor->_CORSelfDriveUpdatePayment($dataToSave);
		    unset($dataToSave);
		    if(intval($res->{'SelfDrive_UpdatePaymentResult'}) > 0){
			 $url .= "?resp=booksucc&bookid=" . $Merchant_Param;
			 $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			 $xmlToSave .= "<cortrans>";
			 $xmlToSave .= "<bookid>" . $Merchant_Param . "</bookid>";
			 $xmlToSave .= "</cortrans>";
			 if(!is_dir("./xml/trans/" . date('Y')))
			 mkdir("./xml/trans/" . date('Y'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			 if(isset($_REQUEST['Order_Id']))
			 createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-B.xml");
			 
			 if(isset($_REQUEST['Order_Id'])){
			      $db = new MySqlConnection(CONNSTRING);
			      $db->open();
			      $dataToSave = array();
			      $dataToSave["booking_id"] = $Merchant_Param;
			      $whereData = array();
			      $whereData["coric"] = $_REQUEST['Order_Id'];		
			      $r = $db->update("cor_booking_new", $dataToSave, $whereData);
			      unset($whereData);
			      unset($dataToSave);
			      $db->close();
			 }
		    } else {
			 $url = "booking-fail.php";
			 $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			 $xmlToSave .= "<cortrans>";
			 $xmlToSave .= "<error3>" . serialize($res) . "</error3>";
			 $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";
			 $xmlToSave .= "</cortrans>";
			 if(!is_dir("./xml/trans/" . date('Y')))
			 mkdir("./xml/trans/" . date('Y'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			 if(isset($_REQUEST['Order_Id']))
			 createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
	
	//for paid & fail
                        $html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
                        $html .= "<tr>";
                        $html .= "<td>Coric</td>";
                        $html .= "<td>" . $_REQUEST['Order_Id'] . "</td>";
                        $html .= "</tr>";
                        $html .= "<tr>";
                        $html .= "<td>Amount</td>";
                        $html .= "<td>" . $Amount . "</td>";
                        $html .= "</tr>";
                        $html .= "</table>";
                        
                        
                        $dataToMail= array();
                        $dataToMail["To"] = "binary.priyanka@carzonrent.com;balwinder@mylescars.com";
                        $dataToMail["Subject"] = "Booking Fail Alert - Carzonrent";
                        $dataToMail["MailBody"] = $html;
                        $res = $cor->_CORSendMail($dataToMail);
                        unset($dataToMail);
                    
                    // end paid n fail 


		 }
		    createcache($xmlToSave, "./xml/trans/" . $trackId . ".xml");
	       } 
	       unset($cor);
	       header('Location: ' . urldecode($url));
	  }
     }
     else {
       $url = "booking-fail.php";
       $url .= "?resp=bookfail&bookid=" . $_SESSION["hdBookingID"];
       header('Location: ' . urldecode($url));
     }
?>
<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
session_start();
set_include_path('./lib'.PATH_SEPARATOR.get_include_path());
require_once 'Zend/Crypt/Hmac.php';

include_once("./includes/cache-func.php");
include_once('./classes/cor.mysql.class.php');
function generateHmacKey($data, $apiKey=null){
	$hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
	return $hmackey;
}

$txnid = "";
$txnrefno = "";
$txnstatus = "";
$txnmsg = "";
$firstName = "";
$lastName = "";
$email = "";
$street1 = "";
$city = "";
$state = "";
$country = "";
$pincode = "";
$mobileNo = "";
$signature = "";
$reqsignature = "";
$data = "";
$txnGateway = "";
$paymentMode = "";
$maskedCardNumber = "";
$cardType = "";
$flag = "dataValid";

if(isset($_POST['TxId']))
{
	$txnid = $_POST['TxId'];
	$data .= $txnid;
}
if(isset($_POST['TxStatus']))
{
	$txnstatus = $_POST['TxStatus'];
	$data .= $txnstatus;
}
if(isset($_POST['amount']))
{
	$amount = $_POST['amount'];
	$data .= $amount;
}
if(isset($_POST['pgTxnNo']))
{
	$pgtxnno = $_POST['pgTxnNo'];
	$data .= $pgtxnno;
}
if(isset($_POST['issuerRefNo']))
{
	$issuerrefno = $_POST['issuerRefNo'];
	$data .= $issuerrefno;
}
if(isset($_POST['authIdCode']))
{
	$authidcode = $_POST['authIdCode'];
	$data .= $authidcode;
}
if(isset($_POST['firstName']))
{
	$firstName = $_POST['firstName'];
	$data .= $firstName;
}
if(isset($_POST['lastName']))
{
	$lastName = $_POST['lastName'];
	$data .= $lastName;
}
if(isset($_POST['pgRespCode']))
{
	$pgrespcode = $_POST['pgRespCode'];
	$data .= $pgrespcode;
}
if(isset($_POST['addressZip']))
{
	$pincode = $_POST['addressZip'];
	$data .= $pincode;
}
if(isset($_POST['signature']))
{
	$signature = $_POST['signature'];
}
/*signature data end*/

if(isset($_POST['TxRefNo']))
{
	$txnrefno = $_POST['TxRefNo'];
}
if(isset($_POST['TxMsg']))
{
	$txnmsg = $_POST['TxMsg'];
}
if(isset($_POST['email']))
{
	$email = $_POST['email'];
}
if(isset($_POST['addressStreet1']))
{
	$street1 = $_POST['addressStreet1'];
}
if(isset($_POST['addressStreet2']))
{
	$street2 = $_POST['addressStreet2'];
}
if(isset($_POST['addressCity']))
{
	$city = $_POST['addressCity'];
}
if(isset($_POST['addressState']))
{
	$state = $_POST['addressState'];
}
if(isset($_POST['addressCountry']))
{
	$country = $_POST['addressCountry'];
}

if(isset($_POST['mandatoryErrorMsg']))
{
	$mandatoryerrmsg = $_POST['mandatoryErrorMsg'];
}
if(isset($_POST['successTxn']))
{
	$successtxn = $_POST['successTxn'];
}
if(isset($_POST['mobileNo']))
{
	$mobileNo = $_POST['mobileNo'];
}
if(isset($_POST['txnGateway']))
{
	$txnGateway = $_POST['txnGateway'];
}
if(isset($_POST['paymentMode']))
{
	$paymentMode = $_POST['paymentMode'];
}
if(isset($_POST['maskedCardNumber']))
{
	$maskedCardNumber = $_POST['maskedCardNumber'];
}
if(isset($_POST['cardType']))
{
	$cardType = $_POST['cardType'];
}

$respSignature = generateHmacKey($data,"01ead54113da1cb978b39c1af588cf83e16c519d");

if($signature != "" && strcmp($signature, $respSignature) != 0)
{
	$flag = "dataTampered";
	if(isset($_POST["TxId"])){
		include_once("./includes/cache-func.php");
		include_once('./classes/cor.mysql.class.php');
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
	
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "5";//4 tempered transaction at PG		    
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];		
		$r = $db->update("cor_booking_new", $dataToSave, $whereData);
		unset($whereData);
		unset($dataToSave);
		$db->close();
	}
?>
	<form id="formBooking" name="formBooking" method="POST" action="http://www.carzonrent.com/booking-fail.php">
	<input type="hidden" name="TxId" id="TxId" value="<?php echo $_POST["TxId"]; ?>" />
	</form>
	<script>
		document.getElementById("formBooking").submit();
	</script>
<?php
} else {
	if($_POST["TxStatus"] == "SUCCESS"){
		include_once("./includes/cache-func.php");
		include_once('./classes/cor.mysql.class.php');
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		
		if(isset($_POST["couponCode"]) && $_POST["couponCode"]!="" && isset($_POST['adjustedAmount']) && $_POST['adjustedAmount']!='')
		{
		
		/****************For CITY BANK Implementation  bin range **********/
		
		 if($_POST["couponCode"]=='Carzonrent citibank offer')
		 {
		   $dataToSave["promotion_code"] = 'CITYBANKBIN';
		 }
		/****************For CITY BANK Implementation  bin range **********/
		
		/****************For ICICI Implementation  bin range **********/
		$dataToSave["tot_fare"] = $_POST["amount"];
		$dataToSave["discount_amt"] = $_POST["adjustedAmount"];
		$dataToSave["User_billable_amount"] = $_POST["transactionAmount"];
		/****************For ICICI Implementation bin range **********/
		}
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "3";//3 successful transaction at PG		    
		$dataToSave["billing_cust_name"] = $_POST["firstName"] . " " . $_POST["lastName"];
		$dataToSave["billing_cust_address"] = $_POST["addressStreet1"] . " " . $_POST["addressStreet2"];
		$dataToSave["billing_cust_state"] = $_POST["addressState"];
		$dataToSave["billing_zip_code"] = $_POST["addressZip"];
		$dataToSave["billing_cust_city"] = $_POST["addressCity"];
		$dataToSave["billing_cust_country"] = $_POST["addressCountry"];
		$dataToSave["billing_cust_tel"] = $_POST["mobileNo"];
		$dataToSave["billing_cust_email"] = $_POST["email"];
		$dataToSave["AuthDesc"] = "Y";
		$dataToSave["Checksum"] = $_POST["authIdCode"];
		$dataToSave["Merchant_Param"] = $_POST["TxRefNo"];
		$dataToSave["nb_bid"] = $_POST["pgTxnNo"];
		$dataToSave["nb_order_no"] = $_POST['transactionId'];
		$dataToSave["card_category"] = $_POST["paymentMode"];
		$dataToSave["bank_name"] = "NA";
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		$dataToSave["signature"] = $_POST["signature"];
		
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];		
		$r = $db->update("cor_booking_new", $dataToSave, $whereData);
		//print_r($r);
		unset($whereData);
		unset($dataToSave);
		$db->close();
?>
		<form id="formTravelDetail" name="formTravelDetail" method="POST" action="http://www.carzonrent.com/payment-confirmation.php">
		<input type="hidden" name="TxId" id="TxId" value="<?php echo $_POST["TxId"]; ?>" />
		</form>
		<script>
			document.getElementById("formTravelDetail").submit();
		</script>
<?php
	} else {
		include_once("./includes/cache-func.php");
		include_once('./classes/cor.mysql.class.php');
		
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
	
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "4";//4 cancelled transaction at PG		    
		$dataToSave["billing_cust_name"] = $_POST["firstName"] . " " . $_POST["lastName"];
		$dataToSave["billing_cust_address"] = $_POST["addressStreet1"] . " " . $_POST["addressStreet2"];
		$dataToSave["billing_cust_state"] = $_POST["addressState"];
		$dataToSave["billing_zip_code"] = $_POST["addressZip"];
		$dataToSave["billing_cust_city"] = $_POST["addressCity"];
		$dataToSave["billing_cust_country"] = $_POST["addressCountry"];
		$dataToSave["billing_cust_tel"] = $_POST["mobileNo"];
		$dataToSave["billing_cust_email"] = $_POST["email"];
		$dataToSave["AuthDesc"] = "N";
		$dataToSave["Checksum"] = $_POST["authIdCode"];
		$dataToSave["Merchant_Param"] = $_POST["TxRefNo"];
		$dataToSave["nb_bid"] = $_POST["pgTxnNo"];
		$dataToSave["nb_order_no"] = $_POST['transactionId'];
		$dataToSave["card_category"] = $_POST["paymentMode"];
		$dataToSave["bank_name"] = "NA";
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		$dataToSave["signature"] = $_POST["signature"];
		
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];		
		$r = $db->update("cor_booking_new", $dataToSave, $whereData);
		//print_r($r);
		unset($whereData);
		unset($dataToSave);
		$db->close();
		//header("Location: http://www.carzonrent.com/booking-fail.php");
?>
		<form id="formBooking" name="formBooking" method="POST" action="http://www.carzonrent.com/booking-fail.php">
		<input type="hidden" name="TxId" id="TxId" value="<?php echo $_POST["TxId"]; ?>" />
		</form>
		<script>
			document.getElementById("formBooking").submit();
		</script>
<?php
	}
}
?>
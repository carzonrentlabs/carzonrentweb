<?php
	error_reporting(E_ALL);
	session_start();
	include_once('classes/cor.ws.class.php');
	include_once('classes/cor.xmlparser.class.php');
	include_once("includes/cache-func.php");
	include_once('classes/cor.mysql.class.php');
	include_once('uploadclass/S3_class.php');
	include_once('uploadclass/S3_config.php');
    if (!empty($_FILES)) {		
		$name = $_FILES['FileName']['name'];
		$size = $_FILES['FileName']['size'];
		$tmp = $_FILES['FileName']['tmp_name'];
		$ext = getExtension($name);
		if($_POST["docType"] == '0'){
			$val = 'P';
		}elseif($_POST["docType"] == '4'){
			$val = 'V';
		}elseif($_POST["docType"] == '5'){
			$val = 'A';
		}else{
			$val = 'D';
		}
		
		$rand_number = mt_rand(10000, 99999);
	
		if(strlen($name) > 0)
		{
			if(in_array($ext,$valid_formats))
			{ 
				if($size<(1000000))
				{				
					//Rename image name. 
						$actual_image_name = $_POST["GuestID"]."_".$rand_number."_".$val.".".$ext;
						$bucket='uploadmyles';
						$targetPath = $_POST["GuestID"].'/'.$actual_image_name;
						$dataToSave= array();
						$resval = 0;
						$resval =  $s3->putObjectFile($tmp, $bucket , $targetPath , S3::ACL_PUBLIC_READ);
						
						if($resval == 1){	
							$dataToSave["GuestID"] = $_POST["GuestID"];
							$dataToSave["UserID"] = $_POST["UserID"];
							$dataToSave["FileName"] = $actual_image_name;
							$dataToSave["status"] = $_POST["docType"];
							$dataToSave["Path"] = $targetPath;
							$dataToSave["BookingID"] = 0;
							$dataToSave["IDNo"] = $_POST["IDNo"];
							$dataToSave["Expiry"] = $_POST["Expiry"];
							$dataToSave["IssueCity"] = $_POST["IssuCity"];
							//echo '<pre>'; print_r($dataToSave); 
							//die;
						}
						$_SESSION['type'] = $_POST["docType"];
						$cor = new COR();
						$res = $cor->_CORSaveUploadDetails($dataToSave);
						//echo '<pre>'; print_r($res); die; 
						$arrResult = $res->SaveUploadDetailsResult;
						
						if($arrResult == '1'){
							$msg = "successfully uploaded";
							$_SESSION['file_msg'] = $msg ;
							$_SESSION['class'] = 'active' ;
							$url = "./myaccount.php#tab3";
							header('Location: ' . $url);
						}else {
							$msg = "not uploaded";
							$_SESSION['file_msg'] = $msg ;
							$_SESSION['class'] = 'active' ;
							$url = "./myaccount.php#tab3";
							header('Location: ' . $url);
						}
						
				}else{
					$msg = "Please couldn't upload more than 1MB file size";
					$_SESSION['file_msg'] = $msg ;
					$_SESSION['class'] = 'active' ;
					$url = "./myaccount.php#tab3";
					header('Location: ' . $url);
				}
		
			}else{
				$msg = "Please upload only(jpeg ,jpg or png) file";
				$_SESSION['file_msg'] = $msg ;
				$_SESSION['class'] = 'active' ;
				$url = "./myaccount.php#tab3";
				header('Location: ' . $url);
			}     
		}else{
			$msg = "Invalid file, please upload image file.";
				$_SESSION['file_msg'] = $msg ;
				$_SESSION['class'] = 'active' ;
				$url = "./myaccount.php#tab3";
				header('Location: ' . $url);
		}    
    }  
		
?>
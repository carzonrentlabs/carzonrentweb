<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="googlebot" content="noindex, nofollow">
<meta name="googlebot" content="noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
<meta name="title" content="Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com" />
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers book a cab or rent a car for airport transfer car rentals, online economy car rentals/car hiring and hertz car rental solutions." />
<meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental india, car rental company india, budget/executive car rentals, economy car rental services, car hire services india, airport transfer car rental services, luxury cars on rent" />
<!-- <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-v2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/tabcontent-v2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/global-v2.css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/tabcontent.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/jquery.prettyPhoto.js" ></script> -->

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

</head>
<body>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:2000, autoplay_slideshow:false});
});
</script> 
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<!--<div class="tbul">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">PAYBACK</a></li>	
  </ul>
</div>-->
<div class="lfcnt" style="width:90%">
<div id="country1" class="tabcontent">
  <h1>PAYBACK Failure</h1>
  <h2>Sorry, booking not completed.</h2>
<p class="txt14px">&nbsp;</p>
<p>Booking could not be completed. Please <a href="/">try again</a>.<br />
<div class="apply"><div class="submit"><a href="javascript:void(0)"></a></div></div>
<p class="txt14px">&nbsp;</p>
<div class="clearfix"></div>
</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

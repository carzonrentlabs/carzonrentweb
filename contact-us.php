<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />

</head>
<body>
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder" style="margin-top: 20px;">
		<div class="magindiv" style="padding:7px 0px 7px 0px;width:1130px;">
			<h2>Contact Us</h2>
		</div>
	</div>	
	<div style="margin:0 auto;width:1130px;">
			<div class="divcenter" style="width:100%">
			<div class="contactleftdiv">
				<div class="contactlistdiv" style="height:90px;">
					<h4 style="margin-top:5px;">Feedback / Reservation</h4>
					<span>011-41841212 (24X7)</span><br />
					 Email: for local and outstation <a class="txtmail" href="mailto:crm@carzonrent.com">crm@carzonrent.com</a> <br>
					 <!--Email: for Myles self drive <a class="txtmail" href="mailto:help@mylescars.com">help@mylescars.com</a>-->
				</div>
				<div class="contactlistdiv" style="height:90px;">
					<h4 style="margin-top:5px;">Corporate Reservation</h4>
					<p>Car Rental : +91-11- 41841212<br />
					 Email: <a class="txtmail" href="mailto:reserve@carzonrent.com">reserve@carzonrent.com</a>
					<!-- Limo Service: 18605001212 --></p>
				</div>
				<div class="contactlistdiv" style="height:110px;">
					<h4 style="margin-top:5px;">Corporate</h4>
					<p>9th  Floor, Videocon Towers,<br />
					Jhandewalan Extension, New Delhi - 110055.<br />
					Landline: +91 - 11 - 43083000<br />
					Email: <a class="txtmail" href="mailto:corporate@carzonrent.com">corporate@carzonrent.com</a><br />
					</p>
				</div>
				<div class="contactlistdiv" style="height:110px;">
					<h4 style="margin-top:5px;">Sales</h4>
					<p>Landline: +91 - 11 - 43083000<br />
					Email: <a class="txtmail" href="mailto:sales@carzonrent.com">sales@carzonrent.com</a><br /></p>
				</div>
				<div class="contactlistdiv">
					<h4 style="margin-top:5px;">Media Center</h4>
					<p><strong>Ajay/Vaibhav:</strong> +91 - 11 - 43083000<br />
					Email: <a class="txtmail" href="mailto:marketing@carzonrent.com">marketing@carzonrent.com</a><br /></p>
				</div>
				<div class="contactlistdiv">
					<h4 style="margin-top:5px;">Marketing</h4>
					<p>Landline: +91 - 11 - 43083000<br />
					Email: <a class="txtmail" href="mailto:marketing@carzonrent.com">marketing@carzonrent.com</a><br /></p>
				</div>
				<div class="contactlistdiv" style="height:86px;">
					<h4 style="margin-top:5px;">Franchise</h4>					
					
					 Landline: +91 - 11 - 43083000 Ext:170<br />
					Email: <a class="txtmail" href="mailto:franchise@carzonrent.com">franchise@carzonrent.com</a><br /></p>
				</div>
				<div class="contactlistdiv" style="height:86px;">
					<h4 style="margin-top:5px;">EasyCabs Corporate Tie Up Enquiry</h4>					
					
					 <p><strong>Sharad : </strong>9971399300<br />
					Email: <a class="txtmail" href="mailto:franchise@carzonrent.com">corporate@carzonrent.com</a><br /></p>
				</div>
				
			</div>
			<div class="imagediv">
			<img src="./images/contactusimg.png" alt="Contact Us" title="Contact Us" border="0" width="304" height="345"/>
		</div>
		</div>
	</div>	
</div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once('./classes/cor.gp.class.php');

	if(isset($_POST["hdOriginName"]))
	{
		$orgName =  $_POST["hdOriginName"];
		$orgNames = explode(",", $orgName);
	}
	if(isset($_POST["hdOriginID"]))
	{
		$orgID =  $_POST["hdOriginID"];
		$orgIDs = explode(",", $orgID);
	}
	if(isset($_POST["hdDestinationName"]))
	{
		$destName =  $_POST["hdDestinationName"];
		$destNames = explode(",", $destName);
	}
	if(isset($_POST["hdDestinationID"]))
	{
		$destID =  $_POST["hdDestinationID"];
		$destIDs = explode(",", $destID);
	}
	if(isset($_POST["pickdate"]))
	{
		$pDate =  $_POST["pickdate"];
		$pickDate = date_create($pDate);
	}
	if(isset($_POST["dropdate"]))
	{
		$dDate =  $_POST["dropdate"];
		$dropDate = date_create($dDate);
	}
	$tab = 1;
	if(isset($_POST["tab"]))
	{
		$tab =  $_POST["tab"];
		
	}
	if($tab == 4)
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
<meta name="title" content="Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com" />
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers book a cab or rent a car for airport transfer car rentals, online economy car rentals/car hiring and hertz car rental solutions." />
<meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental india, car rental company india, budget/executive car rentals, economy car rental services, car hire services india, airport transfer car rental services, luxury cars on rent" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="js/customSelect.jquery.js"></script>
<script type="text/javascript" src="js/_bb_general_v3.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/funtion_init.js"></script>
<!-- <script type="text/javascript" src="js/map.js"></script> -->
<script>
$(function(){
	$('select.time').customSelect();
	$('select.area').customSelect();
})

function resgiter_cont(){
	document.getElementById("register").style.display = 'none';  
	document.getElementById("travel_details").style.display = 'block';  
}
</script>


</head>
<body>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<div class="tbbingouter">
  <div class="main">
<?php if($tab == 1) { ?>
	<ul id="countrytabs" class="shadetabst" style="background-position:0 -33px;">
<?php } else if($tab == 2 || $tab == 3) { ?>
	<ul id="countrytabs" class="shadetabst" style="background-position:0 -66px;">
<?php } else if($tab == 4) { ?>
	<ul id="countrytabs" class="shadetabst" style="background-position:0 -99px;">
<?php } else { ?>
	<ul id="countrytabs" class="shadetabst">
<?php } ?>
      <li class="step1"><a href="javascript:void(0);" id="menuTab1" rel="country1" style="cursor:default;" class="selected">&nbsp;</a></li>
      <li class="step2"><a href="javascript:void(0);" id="menuTab2" rel="country2" style="cursor:default;">&nbsp;</a></li>
      <li class="step3"><a href="javascript:void(0);" id="menuTab3" rel="country3" style="cursor:default;">&nbsp;</a></li>
    </ul>
  </div>
</div>
<!--Middle Start Here-->
<!--Change by Iqbal on 09-May-2012 STARTS-->
<?php
        if(isset($_POST["hdDays"])){
            $days = $_POST["hdDays"];
        }
        if(isset($_POST["pickdate"]))
        {
            $pDate =  $_POST["pickdate"];
            $pickDate = date_create($pDate);
            $dropDate = date('Y-m-d', strtotime($pDate) + (60 * 60 * 24 * $days));
        }
?>
<!--Change by Iqbal on 09-May-2012 ENDS-->
<div class="tbbinginr">
  <div id="country1" class="tabcontent">
    <div class="selectcartp">
        <div class="main">
            <h2>You Searched For</h2>
            <div class="wd136">
            <!--Change by Iqbal on 09-May-2012 STARTS-->
                <h3>From</h3>
                <p><?php echo $orgName; ?></p>
            </div>
            <div class="wd336">
                <h3>Destination</h3>
                <p><?php echo $destName; ?></p>
            </div>
            <div class="wd136">
                <h3>Date</h3>
                <p><?php echo $pickDate->format('d-m-Y'); ?></p>
            </div>
            <div class="wd136">
                <h3>Days</h3>
                <p><?php echo $days; ?></p>
            </div>
        </div>
        </div>
    </div>
    <div class="main">
    <div class="selectcarmid">
        <div class="trtp">
		<label class="frst">Type of Car</label>
		<label class="snd">Capacity</label>
		<label class="lst">Price</label>
        </div>
		<!-- all categories -->
		<?php
                        
                        $corPack = array();
                        $corPack["cityId"] = $_POST["hdOriginID"];
			$cor = new COR();
                        $res = $cor->_CORGetPackages($corPack);
                        
                        //print_r($res);
                        $myXML = new CORXMLList();                        
                        $packages = $myXML->xml2ary($res);
                        
                        $packPrice = array();
                        foreach($packages as $k => $v)
                        {
                                $packPrice[$k] = $v['Price'];
                        }
                        array_multisort($packPrice, SORT_ASC, $packages);
                        
                        for($i = 0; $i < count($packages); $i++){                            
                            if(trim($packages[$i]["PackageName"]) == trim($_POST["hdDestinationName"])){
                                $corArrCA = array();
                                $corArrCA["cityid"] = $_POST["hdOriginID"];
                                $corArrCA["CarCatId"] = $packages[$i]["CarCatID"];
                                $corArrCA["ServiceDate"] = $_POST["pickdate"];
                                
                                $res = $cor->_CORCheckSoldOut($corArrCA);
                                $isAvail = "";
                                if($res->{'CheckSoldOutResult'}->{'any'} != ""){
                                    $isAvail = $myXML->xml2ary($res->{'CheckSoldOutResult'}->{'any'});
                                    if(count($isAvail) <= 0)
                                    {   
                ?>
                                <form id="formBook" name="formBook" method="POST" action="./search-result.php">
				<input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $packages[$i]["CarCatName"]; ?>" />
				<input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"]?>" />
				<input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"]?>"/>
				<input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]?>"/>
				<input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"]?>"/>
				<input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $packages[$i]["PkgId"]; ?>">				
				<input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
				<input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo intval($arr[$i]["KMRate"]); ?>" />
				<input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />
				<input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo intval($packages[$i]["Price"]); ?>" />
                                <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
				<input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $_POST["pickdate"]?>" />
				<input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate;?>" />
				<input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"]?>" />
				<input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo intval($totalKM/1000);?>" />
				<div class="repeatdiv">
                                    <div class="typefcr">
                                        <div class="imgdv"><a href="javascript: void(0);">
                                                <img src="images/<?php echo $packages[$i]["CarCatName"]; ?>.jpg" alt="" /></a>
                                        </div>
                                        <div class="infdv">
                                            <span><?php echo $packages[$i]["CarCatName"]; ?></span><br />
                                            <?php
                                                $carModel = "";
                                                $passCount = 4;
                                                if($packages[$i]["CarCatName"] == 'Economy')	
                                                        $carModel = "Indica, Wagon R<br />Ecco, Santro" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Compact')	
                                                        $carModel = "Indigo, Esteem<br />Swift" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Intermediate')	
                                                        $carModel = "Dzire, Fusion<br />Ikon,  Accent" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Van'){
                                                        $carModel =  "Innova, Xylo" . "<br />";
                                                        $passCount = 6;
                                                }
                                                else if($packages[$i]["CarCatName"] == 'Standard')	
                                                        $carModel =  "City, Fiesta<br />SX4, Verna" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Superior')	
                                                        $carModel =  "Corolla, Elantra<br />Cruze" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Premium')	
                                                        $carModel =  "Accord, Camry<br />Mercedes-C" . "<br />";
                                                else if($packages[$i]["CarCatName"] == 'Luxury')	
                                                        $carModel =  "Mercedes-E<br />BMW 5 Series" . "<br />";
                                                else if($packages[$i]["CarCatName"] == '4 Wheel Drive' || $packages[$i]["CarCatName"] == 'Full Size')	{
                                                        $carModel =  "Endeavour, Fortuner" . "<br />";
                                                        $passCount = 4;
                                                }
                                                echo $carModel;
                                            ?>
                                            <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
                                        </div>
                                    </div>
                                    <div class="capcity">
                                            <label class="noof"><?php echo $passCount; ?></label>
                                            <label class="tx">+ driver</label>
                                    </div>
                                    <div class="price">
                                        <span id="ffare<?php echo $i; ?>"><br />Rs <?php echo intval($packages[$i]["Price"]); ?>/-</span><br />
                                    </div>
                                    <div class="boknw">
                                            <input type="button" id="btnBookNow<?php echo $i; ?>" name="btnBookNow" value="Book Now" onclick="javascript: this.form.submit();" />
                                    </div>
				</div><!-- repeatdiv -->
				</form>
                <?php
                                    }
                                }
                            }
                        }
                ?>
      </div><!-- selectcarmid -->
    </div><!-- main -->
  </div><!-- country1 -->
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
<?php
if(isset($_POST["resp"]))
	{
		if($_POST["resp"] == 'country2')
		{
?>
	<script language="javascript">
	<!--
		document.getElementById('country1').style.display = 'none';
		document.getElementById('country2').style.display = 'block';
		document.getElementById('menuTab1').style.className = '';
		document.getElementById('menuTab2').style.className = 'selected';
	//-->
	</script>
<?php
		}
	}
?>
</body>
</html>
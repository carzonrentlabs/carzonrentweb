<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Invoice - Carzonrent (India) Pvt. Ltd.</title>
<meta name="description" content="Find this page to print invoice of your booking." />
<meta name="keywords" content="print booking, print invoice" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/_bb_general_v3.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" ></script>
</head>
<body>
<script type="text/javascript" charset="utf-8">
//$(document).ready(function(){
//	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:2000, autoplay_slideshow:false});
//});
</script> 
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<div class="tbul">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">Print Booking </a></li>
    
  </ul>
</div>
<div class="lfcnt">
<div id="country1" class="tabcontent">
  <h1>Print Booking</h1>
  <p>&nbsp;</p>
<div class="printBooking">
<?php
	$mobile = "";
	if(isset($_GET["m"]))
		$mobile = $_GET["m"];
	if(!isset($_GET["m"]) && isset($_SESSION["MobileNumber"]))
		$mobile = $_SESSION["MobileNumber"];
	$bookID = "";
	if(isset($_GET["b"]))
		$bookID = $_GET["b"];
	if(!isset($_GET["b"]) && isset($_SESSION["CORID"]))
		$bookID = $_SESSION["CORID"];
?>
<form action="#" method="post" name="frmPrintInvoice" id="frmPrintInvoice">
<table width="75%" cellpadding="0" cellspacing="10" border="0" align="left">
	<tr>
		<td colspan="2" valign="middle" align="center"><span id="msg"></span></td>
	</tr>
	<tr>
	    <td align="right" valign="middle"><label>Mobile No</label></td>
	    <td align="left" valign="top" ><input type="text" name="txtMobile" maxlength="10" id="txtMobile" onKeyPress="javascript: return _allowNumeric(event);" value="<?php echo $mobile; ?>" /></td>
	</tr>
	<tr>
	    <td align="right" valign="middle"><label>COR ID</label></td>
	    <td align="left" valign="top" ><input type="text" name="txtBookingID" maxlength="11" id="txtBookingID" value="<?php echo $bookID; ?>" /></td>
	</tr> 
	<tr>
	    <td align="right" valign="middle"></td>
	    <td align="left" valign="top" ><div class="booknow"><a href="javascript:void(0)" onclick="javascript: _checkInvoice();"></a></div></td>
	</tr>                                    
</table>

</form></div>

  <div class="clr"></div>
</div>
  <p>&nbsp;</p>


</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<?php 
	include_once("./includes/footer.php"); 
?>
</html>

<?php
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    
     $html = "";     
    if(isset($_POST["seq_no"]))
    {
        $sno = $_POST["seq_no"];	
        
        $db = new MySqlConnection(CONNSTRING);
        $db->open();
		$dispLocation = "";
        $r = $db->query("stored procedure","cor_loadmore_new_bookings_ec('".$_POST["seq_no"]."','".$_POST["startdate"]."','".$_POST["enddate"]."')");
        //print_r($r);
        for($i = 0;$i < count($r); $i++)
        {
            $style = "";            
            if($i%2 == 0)
            {
            $style = "background-color:#f1f1f1;";
            }
            else
            {
            $style = "background-color:#dddada;";
            }
            //if($avgDist >=200 && $avgDist <= 400)
            //$style .= "color:#0f0;font-weight:bold;";
				switch ($r[$i]["pickup_location"])
				{
					case 1:
						$dispLocation = "Delhi";
						break;
					case 2:
						$dispLocation = "Mumbai";
						break;
					case 3:
						$dispLocation = "Bangalore";
						break;
					case 4:
						$dispLocation = "Hyderabad";
						break;
				}
				$html .= "<tr style=\"$style \">
                        <td style=\"padding:5px;word-wrap:break-word;\" name = \"seqno\">".($i+1)."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".date('F j, Y', strtotime($r[$i]["entry_datetime"]))."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["booking_id"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["keyword"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["cust_mobile"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["guest_name"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["pickup_address"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".date('F j, Y', strtotime($r[$i]["pickup_datetime"]))."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$dispLocation ."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["pickup_landmark"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["drop_landmark"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["dest_address"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["ip"] . "<br /><br />" . $r[$i]["ua"] ."</td>
			<td style=\"padding:5px !important;word-wrap:break-word;\"><br />" . $r[$i]["userid"] . "<br />" . $r[$i]["username"] . "<br />" . $r[$i]["useremail"] . "<br />" . $r[$i]["userphone"]  . "</td>
                    </tr>";
                $sno++;
        }
        unset($r);
        
        $count = $db->query("stored procedure","cor_countInCor_Booking_ec('".$_POST["startdate"]."','".$_POST["enddate"]."')");

        $hdCount = $count[0]["cnt"];
        
        unset($count);
        $db->close();
    }
    
    $c = 0;
    if($hdCount <= $sno)
    {
	$c =1;
    }
    
    echo $sno."#!#".$html."#!#".$c."#!#".$_POST["startdate"]."#!#".$_POST["enddate"];
?>
<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("../includes/check-user.php");
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    include_once("../classes/cor.ws.class.php");
    include_once("../classes/cor.xmlparser.class.php");
    
    $coric = "";
    if(isset($_GET["coric"]))
        $coric = $_GET["coric"];
        
    $html = "";
    
        $WebRequestData1 = array();
        $WebRequestData1["CorIC"] = $coric;
        
        $soapClient = new COR();
        $myXML = new CORXMLList();
        $RecordResult = $soapClient->_CORDashBoardDetails($WebRequestData1);
        //echo "<pre>";print_r($RecordResult);die;



        if ($RecordResult->{'DashBoardDetailsResult'}) {
            $result = $RecordResult->{'DashBoardDetailsResult'}->{'any'};

            $resultany = $myXML->xml2ary($result);
            //echo "<pre>";print_r($resultany);die;
            $countArray = count($resultany);
            
    
    
   
    if (is_array($resultany)) {
        $i=1;
        //echo "<pre>";print_r($resultany[0]);
       
        
        $html = "<table class='dtl' cellspacing='1' border='0' width='100%'>";
            $html .= "<tr>";
                $html .= "<th colspan='6' align='left'><b>Booking Details</b></th>";
            $html .= "</tr>";
            if($resultany[0]["BookingID"] != ""){
                $html .= "<tr>";
                    $html .= "<td style='background-color: #fef3bc;font-size:14px;' align='left'>Booking ID</td>";
                    $html .= "<td style='background-color: #fef6cf;font-size:14px;color:#f00;' align='left' colspan='5'><b>" . $resultany[0]['BookingID'] . "</b></td>";
                $html .= "</tr>";
            }
                $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc;width:25% !important;' align='left'>Tour Type</td>";
                $html .= "<td style='background-color: #fef6cf;width:25% !important;' align='left'><input type='text' id='txtTourType' name='txtTourType' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc;width:25% !important;' align='left'>Destination</td>";
//                $html .= "<td style='background-color: #fef6cf;width:25% !important;' align='left'><input type='text' id='txtDestination' name='txtDestination' style='width:80% !important;' class='txtBox' value='" . $resultany[0]["destinations_name"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Car Model</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPackageID' name='txtPackageID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['CarModelName'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Distance</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDistance' name='txtDistance' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['Distance'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Date</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPickupDate' name='txtPickupDate' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['PickUpDate'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Drop Date</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropDate' name='txtDropDate' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['DropOffDate'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Time</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPickupTime' name='txtPickupTime' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['PickUpTime'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Drop Time</td>";
                if($resultany[0]["DropOffTime"] != "")
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropTime' name='txtDropTime' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['DropOffTime'] . "' /></td>";
                else
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropTime' name='txtDropTime' style='width:80% !important;' class='txtBox' value='' /></td>";
                $html .= "</tr>";
                $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Discount PC</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountPC' name='txtDiscountPC' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Discount Amount</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountAmount' name='txtDiscountAmount' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Full Name</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['Name'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Email ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['EmailID'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>User ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtUserID' name='txtUserID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['UserId'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Mobile</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtMobile' name='txtMobile' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['Phone1'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Track ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtTrackID' name='txtTrackID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['Trackid'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Transaction ID</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtTransactionID' name='txtTransactionID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['Transactionid'] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left' colspan='2'>Address</td>";
                $html .= "<td style='background-color: #fef3bc' align='left' colspan='2'>Remarks</td>";
            $html .= "</tr>";
            $html .= "<tr>";  
                $html .= "<td style='background-color: #fef6cf' align='left' colspan='2'><textarea type='text' id='txtAddress' name='txtAddress' rows='15' style='width:90% !important;' class='txtAr'>" . str_ireplace("<br />", "\n", $resultany[0]['PickUpAdd']) . "</textarea></td>";                
                $html .= "<td style='background-color: #fef6cf' align='left' colspan='2'><textarea type='text' id='txtRemarks' name='txtRemarks' rows='15' style='width:90% !important;' class='txtAr'>" . str_ireplace("<br />", "\n", $resultany[0]['Remarks']) . "</textarea></td>";
            $html .= "</tr>";
            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Additional Services</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAddServices' name='txtAddServices' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['AdditionalService'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Additional Charges</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAddCharges' name='txtAddCharges' style='width:35% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' />&nbsp;<input type='text' id='add_service_cost_all' name='add_service_cost_all' style='width:35% !important;' class='txtBox' value='" . $rows[0]["add_service_cost_all"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Fare Amount</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFareAmount' name='txtFareAmount' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['IndicatedPrice'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Payment Type</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPaymentType' name='txtPaymentType' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
            $html .= "</tr>";
//            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Payment Status</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPaymentStatus' name='txtPaymentStatus' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Auth Desc</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAuthDesc' name='txtAuthDesc' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//            $html .= "</tr>";
            //$html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Destination ID</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtVisitedCities' name='txtVisitedCities' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
               // $html .= "<td style='background-color: #fef3bc' align='left'>Origin Code</td>";
               // $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtOriginCode' name='txtOriginCode' style='width:80% !important;' class='txtBox' value='Rental Cars' /></td>";
            //$html .= "</tr>";
//            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>CheckSum</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtCheckSum' name='txtCheckSum' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Discount Code</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountCode' name='txtDiscountCode' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//            $html .= "</tr>";
            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Promotion Code</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPromotionCode' name='txtPromotionCode' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Sub Location Name</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtSubLocationID' name='txtSubLocationID' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['SubLocationName'] . "' /></td>";
            $html .= "</tr>";
//            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>VAT</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='vat' name='vat' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Duration</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDuration' name='txtDuration' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//            $html .= "</tr>";
//            $html .= "<tr>";
//                $html .= "<td style='background-color: #fef3bc' align='left'>Convenience Charges</td>";
//                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='airportCharges' name='airportCharges' style='width:80% !important;' class='txtBox' value='" . $resultany[0]['TourType'] . "' /></td>";
//            if($rows[0]["payment_mode"] == 1 && is_null($rows[0]["booking_id"]) && $rows[0]["AuthDesc"] == 'Y'){
//                    $html .= "<td colspan='2' align='left'><input type=\"image\" src=\"../images/submit-a.png\" onclick=\"javascript: _createBooking();\" /><span id='msg' style='color:#f00;'></span><input type='hidden' id='txtDestinationID' name='txtDestinationID' value='" . $resultany[0]['TourType'] . "' /></td>";
//            }
//            $html .= "</tr>";
        $html .= "</table>";
    } else {
        $html = "No data found for " + $coric;
        }}
    
    echo $html;
?>

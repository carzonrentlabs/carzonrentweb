var arrData = new Array();
var isWriteMode = false;
_startEdit = function(tb, arrCell, arrCtrl, md, rid, btn){
    if (!isWriteMode) {
        var tab = document.getElementById(tb);            
        var newRow;
        if (md == 'add') {
            tab.insertRow(1);
            newRow = tab.rows[1];
            newRow.id = "row-1";
            for (e = 0; e < arrCell.length; e++)
                newRow.insertCell(arrCell[e]);  
        } else if (md == 'edit'){
            newRow = tab.rows[rid];
        }
        
        var newCell;
        var strToEval = "";
        var ctrlType = "";
        var lastCell = -1;
        var currCell;
        for (e = 0; e < arrCtrl.length; e++) {
            strToEval = "";
            currCell = 0;
            for (a = 0; a < arrCtrl[e].length; a++) {
                switch (arrCtrl[e][a]) {
                    case "cell":
                        a++;
                        strToEval += "newCell = newRow.cells[" + arrCtrl[e][a] + "];";
                        if (md == 'edit') {
                            strToEval += "arrData.push(newCell.innerHTML.trim());";
                        }
                        currCell = arrCtrl[e][a];
                    break;
                    case "element":
                        a++;
                        strToEval += "var elm = document.createElement('" + arrCtrl[e][a] + "');";
                    break;
                    case "type":
                        strToEval += "elm." + arrCtrl[e][a] + "  = ";
                        a++;
                        strToEval += "'" + arrCtrl[e][a] + "';"; 
                        ctrlType = arrCtrl[e][a];
                    break;
                    case "event":
                        a++;
                        strToEval += "elm.setAttribute(";
                        var evt = arrCtrl[e][a].split("|#|");
                        strToEval += "\"" + evt[0] + "\", \"" + evt[1] + "\"";
                        strToEval += ");";
                    break;
                    default:
                        if (arrCtrl[e][a] == 'value' && md == 'edit' && ctrlType == "text") {
                            strToEval += "elm." + arrCtrl[e][a] + "  = ";
                            a++;
                            strToEval += "newCell.innerHTML.trim();";
                        } else if (arrCtrl[e][a] == 'value' && md == 'edit' && ctrlType == "textarea") {
                            strToEval += "elm.innerHTML  = ";
                            a++;
                            strToEval += "newCell.innerHTML.trim();";
						} else {
                            strToEval += "elm." + arrCtrl[e][a] + "  = ";
                            a++;
                            strToEval += "'" + arrCtrl[e][a] + "';"; 
                        }
                    break;
                }
            }
            if (lastCell != currCell)
                strToEval += "newCell.innerHTML = '';";
            strToEval += "newCell.appendChild(elm);";
            lastCell = currCell;
            eval(strToEval);
        }

        arrData.push(rid);
        document.getElementById(btn).style.visibility = 'hidden';
        isWriteMode = true;
    } else {
        alert('You are already adding/editing something.\nPlease finish that before making any further change.');
    }
};

_cancelEdit = function(tb, btn, md, idx, arrCtrl){
    var tab = document.getElementById(tb);
    if (md == 'add') {
        tab.deleteRow(1); 
    } else if (md == 'edit') {
        var cRow = tab.rows[idx];
        for (c = 1; c < 6; c++)
            cRow.cells[c].innerHTML = arrData[c - 1];
        cCell = cRow.cells[c];
        var strToEval = "";
        var lastCell = -1;
        var currCell;
        for (e = 0; e < arrCtrl.length; e++) {
            strToEval = "";
            currCell = 0;
            for (a = 0; a < arrCtrl[e].length; a++) {
                switch (arrCtrl[e][a]) {
                    case "cell":
                        a++;
                        strToEval += "cCell = cRow.cells[" + arrCtrl[e][a] + "];";
                        currCell = arrCtrl[e][a];
                    break;
                    case "element":
                        a++;
                        strToEval += "var elm = document.createElement('" + arrCtrl[e][a] + "');";
                    break;
                    case "event":
                        a++;
                        strToEval += "elm.setAttribute(";
                        var evt = arrCtrl[e][a].split("|#|");
                        strToEval += "\"" + evt[0] + "\", \"" + evt[1] + "\"";
                        strToEval += ");";
                    break;
                    default:
                        if (arrCtrl[e][a] == 'value' && md == 'edit') {
                            strToEval += "elm." + arrCtrl[e][a] + "  = ";
                            a++;
                            strToEval += "'" + cCell.innerHTML + "';";
                        }else {
                            strToEval += "elm." + arrCtrl[e][a] + "  = ";
                            a++;
                            strToEval += "'" + arrCtrl[e][a] + "';";    
                        }
                    break;
                }
            }
            if (lastCell != currCell)
                strToEval += "cCell.innerHTML = '';";
            strToEval += "cCell.appendChild(elm);";
            lastCell = currCell;
            eval(strToEval);
        }
    }   
    document.getElementById(btn).style.visibility = 'visible';
    isWriteMode = false; 
};
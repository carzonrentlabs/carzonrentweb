<div class="bannerbtm">
	<div class="main" id="mtext">
    <span>Pay-per-use:</span> Fixed daily charge for retaining the car | Pay as per the kilometers you travel | All taxes included
    </div>
</div>
<div class="cabs">
	<div class="main">
    	<div class="ftop">
        	<div class="wdcar">
            <img src="<?php echo corWebRoot; ?>/images/car.png" alt="Cabs" title="Cabs" width="47" height="39" /> <label><span>6,500+</span><br />Cabs</label>
            </div>
            <div class="wdtrips">
            <img src="<?php echo corWebRoot; ?>/images/trips.png" alt="Trips Daily" title="Trips Daily" width="42" height="42" /> <label><span>15,000+</span><br />Trips Daily</label>
            </div>
            <div class="wdten">
            <img src="<?php echo corWebRoot; ?>/images/lst.png" alt="Customers Annually" title="Customers Annually" width="44" height="40" /> <label><span>6,000,000+</span><br />Customers Annually</label>
            </div>
            <div class="wdten">
            <img src="<?php echo corWebRoot; ?>/images/ten.png" alt="Kilometers Daily" title="Kilometers Daily" width="40" height="47" /> <label><span>100,000+</span><br />Kilometers Daily</label>
            </div>
        </div>
    </div>
</div>
<div id="middle">
	<div class="main">
    	<div class="homepage">
        	<div class="repeatdiv">
            	<img src="<?php echo corWebRoot; ?>/images/icon3.gif" alt="Quality" width="66px" height="63px" title="Quality" class="icon1" />
            	<h2 class="icon1">Quality</h2>
                <ul>
                	<li>Well trained chauffeurs</li>
			<li>Young, well-maintained car fleet</li>
			<li>Amenities for comfort  </li>
  

                </ul>
            </div>
            <div class="repeatdiv md">
            	<img src="<?php echo corWebRoot; ?>/images/icon2.gif" alt="Reliability" width="34px" height="51px" title="Reliability" class="icon2" />
            	<h2 class="icon2">Reliability</h2>
                <ul>
                    <li>India's largest car hire/rental company</li>
                    <li>In car GPS devices for extra safety</li>
                    <li>Transparent pricing structure</li>
                </ul>
            </div>
            <div class="repeatdiv right">
            	<img src="<?php echo corWebRoot; ?>/images/icon1.gif" alt="Convenience" title="Convenience" width="47px" height="50px" class="icon3" />
            	<h2 class="icon3">Convenience</h2>
                <ul>
                	<li>Available across major cities</li>
                    <li>Book through web or phone</li>
                    <li>Pay by cash or card</li>
                </ul>
            </div>
        </div>
    </div>
</div>
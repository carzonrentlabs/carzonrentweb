<?php
    class CORXMLList {      
        function xml2ary($string) {
            $xml = new SimpleXmlElement($string);
            $table = $xml->{'NewDataSet'}->{'Table'};
            $arr = array();
            for($i = 0; $i < count($table); $i++)
            {
                $rows = $table[$i];
                $json  = json_encode($rows);
                $a = json_decode($json, true);
                $arr[] = $a;
            }
            return $arr;
        }
    };
?>
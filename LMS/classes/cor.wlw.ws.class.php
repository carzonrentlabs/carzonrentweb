<?php
    class COR{
        private $soapClient;
        function __construct(){
            $this->soapClient = new SoapClient("http://insta.carzonrent.com/WsTripsAlliance_New/service.asmx?wsdl", array('trace' => 1));
	    $this->soapClientCOR = new SoapClient("http://insta.carzonrent.com/WsIntercityRetail_New/service.asmx?wsdl", array('trace' => 1));
        }
        
        function _CORCheckPhone($arg){
            try{
                $r = $this->soapClient->VerifyPhone($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCheckLogin($arg){
            try{
                $r = $this->soapClient->verifyLogin($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORRegister($arg){
            try{
                $r = $this->soapClient->Registration($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORMakeBooking($arg){
            try{
                $r = $this->soapClient->SetTravelDetails($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCancelBooking($arg){
            try{
                $r = $this->soapClient->CancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveUpdatePayment($arg){
            try{
                $r = $this->soapClientCOR->SelfDrive_UpdatePayment($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveCreateBooking($arg){
            try{
                $r = $this->soapClientCOR->SelfDrive_CreateBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveCancelBooking($arg){
            try{
                $r = $this->soapClientCOR->SelfDrive_cancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
    };
?>
<?php
    //error_reporting(E_ALL);
    //ini_set("display_errors", 1);
    if(isset($_POST["monumber"])){        
        include_once('./classes/cor.ws.class.php');
        include_once('./classes/cor.xmlparser.class.php');
        $query_string = "";
        if ($_POST) {
          $kv = array();
          foreach ($_POST as $key => $value) {
                if($key != "corpassword")
                $kv[] = "$key=$value";
          }
          $query_string = join("&", $kv);
        }
        else {
            $query_string = $_SERVER['QUERY_STRING'];
        } 
        if(isset($_COOKIE["cciid"]) && isset($_COOKIE["emailid"])){
            if($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != ""){
                $query_string = str_replace("&tab=2", "&tab=3", $query_string);
                $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
            }
        }
        else {
            $cor = new COR();
            if($_POST["corpassword"] == ""){
                $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>1684));
                $myXML = new CORXMLList();
                $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
                if(key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0){
                    $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                }
                else {
                    echo $arrUserId[0]["ClientCoIndivID"];
                    setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                    $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                }
                unset($myXML);            
            }
            else {
                if(isset($_POST["isExistUser"])){
                    if($_POST["isExistUser"] == "1"){
                        $res = $cor->_CORCheckLogin(array("PhoneNo"=>$_POST["monumber"],"password"=>$_POST["corpassword"],"clientid"=>1684));
                        $myXML = new CORXMLList();
                        if($res->{'verifyLoginResult'}->{'any'} != "")
                        {
                            $arrUserId = $myXML->xml2ary($res->{'verifyLoginResult'}->{'any'});
                            if($arrUserId[0]["ClientCoIndivID"] > 0){
                                setcookie("cciid", $arrUserId[0]["ClientCoIndivID"]);
                                setcookie("emailid", $arrUserId[0]["EmailID"]);
                                setcookie("phone1", $arrUserId[0]["Phone1"]);
                                $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                            }
                        }
                        else {
                            $query_string = str_replace("&tab=3", "&tab=2", $query_string);
                            $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string) . "&err=invlg";
                        }
                        unset($myXML);
                    }
                }
                else {
                    $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>1684));
                    $myXML = new CORXMLList();
                    $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
                    if(key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0){
                        $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                    }
                    else {
                        setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                        $url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                    }
                    unset($myXML);
                }
                
            }
            unset($cor);
        }
        header('Location: ' . $url);
    }
?> 
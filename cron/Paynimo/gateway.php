<?php

ob_start();

require_once('TransactionRequestBean.php');
require_once('TransactionResponseBean.php');


function paynimo_request($val = array(),$key='',$iv=''){
       
    $transactionRequestBean = new TransactionRequestBean();
        
    //Setting all values here
    $transactionRequestBean->setMerchantCode(trim($val['mrctCode']));
    $transactionRequestBean->setAccountNo(trim($val['tpvAccntNo']));
    $transactionRequestBean->setITC(trim($val['itc']));
    $transactionRequestBean->setMobileNumber(trim($val['mobNo']));
    $transactionRequestBean->setCustomerName(trim($val['custname']));
    $transactionRequestBean->setRequestType(trim($val['reqType']));
    $transactionRequestBean->setMerchantTxnRefNumber(trim($val['mrctTxtID']));
    $transactionRequestBean->setAmount(trim($val['amount']));
    $transactionRequestBean->setCurrencyCode(trim($val['currencyType']));
    $transactionRequestBean->setReturnURL(trim($val['returnURL']));
    $transactionRequestBean->setS2SReturnURL(trim($val['s2SReturnURL']));
    $transactionRequestBean->setShoppingCartDetails(trim($val['reqDetail']));
    $transactionRequestBean->setTxnDate(trim($val['txnDate']));
    $transactionRequestBean->setBankCode(trim($val['bankCode']));
    $transactionRequestBean->setTPSLTxnID(trim($val['tpsl_txn_id']));
    $transactionRequestBean->setCustId(trim($val['custID']));
    $transactionRequestBean->setCardId($val['cardID']);
    $transactionRequestBean->setKey(trim($val['key']));
    $transactionRequestBean->setIv(trim($val['iv']));
    $transactionRequestBean->setWebServiceLocator(trim($val['locatorURL']));
    $transactionRequestBean->setMMID(trim($val['mmid']));
    $transactionRequestBean->setOTP(trim($val['otp']));
    $transactionRequestBean->setCardName(trim($val['cardName']));
    $transactionRequestBean->setCardNo(trim($val['cardNo']));
    $transactionRequestBean->setCardCVV(trim($val['cardCVV']));
    $transactionRequestBean->setCardExpMM(trim($val['cardExpMM']));
    $transactionRequestBean->setCardExpYY(trim($val['cardExpYY']));
    $transactionRequestBean->setTimeOut(trim($val['timeOut']));

    $responseDetails = $transactionRequestBean->getTransactionToken();
    $responseDetails = (array)$responseDetails;
    $response = $responseDetails[0];

    if(is_string($response) && preg_match('/^msg=/',$response)){

        $outputStr = str_replace('msg=', '', $response);
        $outputArr = explode('&', $outputStr);
        $str = $outputArr[0];


        $transactionResponseBean = new TransactionResponseBean();
        $transactionResponseBean->setResponsePayload($str);
        $transactionResponseBean->setKey($key);
        $transactionResponseBean->setIv($iv);

        $response = $transactionResponseBean->getResponsePayload();

    }elseif(is_string($response) && preg_match('/^txn_status=/',$response)){

    }
   
    
    $response_arr = explode("|", $response);
        foreach ( $response_arr as $key => $val) {
              
            $param = explode("=", $val);
            $arr[$param[0]] = $param[1];
                
        }
    return $arr;
}
    



?>


<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/excellib/Classes/PHPExcel.php';
define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
//include_once('Paynimo/gateway.php');


$Myle = new Myles();
$arr = array();
$date_raw = date("Y-m-d H:i:s");
$first_date = strtotime($date_raw);
$second_date = date("Y-m-d", strtotime('-1 day', $first_date));
$db = new MySqlConnection(CONNSTRING);
$db->open();

////////////////////////////////
///////////////////////////////////




$query = "SELECT * FROM myles_franchises WHERE created BETWEEN '2017-02-26 00:00:00' AND '2017-02-26 23:59:59'";

$franchisedata = $db->query("query", $query);
//"name", "email", "mobile", "city", "query", "created", "income", "occupation", "state"
if (!array_key_exists("response", $franchisedata)) {
    $j = 2;

//////////////////////

    $objPHPExcel = new PHPExcel();

// Set document properties

    $objPHPExcel->getProperties()->setCreator("Mylescars")
            ->setLastModifiedBy("Mylescars");
//        ->setTitle("Office 2007 XLSX Test Document")
//        ->setSubject("Office 2007 XLSX Test Document")
//        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
//        ->setKeywords("office 2007 openxml php")
//        ->setCategory("Test result file");
// Create a first sheet
//echo date('H:i:s'), " Add data", EOL;
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', "Name");
    $objPHPExcel->getActiveSheet()->setCellValue('B1', "Email");
    $objPHPExcel->getActiveSheet()->setCellValue('C1', "Mobile");
    $objPHPExcel->getActiveSheet()->setCellValue('D1', "City");
    $objPHPExcel->getActiveSheet()->setCellValue('E1', "Created");
    $objPHPExcel->getActiveSheet()->setCellValue('F1', "Income");
    $objPHPExcel->getActiveSheet()->setCellValue('G1', "Occupation");
    $objPHPExcel->getActiveSheet()->setCellValue('H1', "State");

    $objPHPExcel->getSecurity()->setLockWindows(true);
    $objPHPExcel->getSecurity()->setLockStructure(true);
    $objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

    $objPHPExcel->getActiveSheet()->getProtection()->setPassword('PHPExcel');
    $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true); // This should be enabled in order to enable any of the following!
    $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);


/////////////////////

    for ($i = 0; $i < count($franchisedata); $i++) {
        $name = $franchisedata[$i]["name"];
        $email = $franchisedata[$i]["email"];
        $mobile = $franchisedata[$i]["mobile"];
        $city = $franchisedata[$i]["city"];
        $created = date("d-m-Y H:i:s", strtotime($franchisedata[$i]["created"]));
        $income = $franchisedata[$i]["income"];
        $occupation = $franchisedata[$i]["occupation"];
        $state = $franchisedata[$i]["state"];

        $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $name)
                ->setCellValue('B' . $j, $email)
                ->setCellValue('C' . $j, $mobile)
                ->setCellValue('D' . $j, $city)
                ->setCellValue('E' . $j, $created)
                ->setCellValue('F' . $j, $income)
                ->setCellValue('G' . $j, $occupation)
                ->setCellValue('H' . $j, $state);
        $j++;
    }
    $callStartTime = microtime(true);
    $filename = $callStartTime . '.xls';
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save($filename);

    
    $my_path = dirname(__FILE__);
    $mailto='ruby.singh@mylescars.com';
    $cc = 'gurkeerat.sekhon@mylescars.com,product@mylescars.com,abhishek.singh@mylescars.com,vinod.maurya@carzonrent.com,dayalu.shanker@mylescars.com';
	//$mailto='abhishek.singh@mylescars.com';
    //$cc = 'vinod.maurya@carzonrent.com,dayalu.shanker@mylescars.com';
    $from_name = "Mylescars";
    $from_mail = "angelsleads@mylescars.com";
    //$my_replyto = "my_reply_to@mail.net";
    $my_subject = "Angels leads - 26/02/2017 (26 FEB'2017)";
    $my_message = "Dear all,<br/>*Please find attached, the compilation of Myles Angels leads generated on 26/02/2017*
<br/>
Thanks<br/>
Technology team";
    mail_attachment($filename, $my_path, $mailto, $from_mail, $from_name, $my_subject, $my_message,$cc);
}
$db->close();

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name,  $subject, $message,$cc) {
    $file = $path.'/' . $filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    
	
	/////////////////////////////////////////
	
	//$emessage = '';
	$headers = "From: ".$from_mail."\r\n";
$headers.= "Cc: ".$cc."\n";
$headers .= 'Reply-To: ' . $from_mail . "\r\n";
$headers .= "Return-Path: ". $from_mail . "\r\n";
$headers.= "MIME-Version: 1.0\r\n";
$headers.= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
$emessage = "--".$uid."\n";
$emessage .= "Content-type:text/html; charset=iso-8859-1\n";
$emessage .= "Content-Transfer-Encoding: 7bit\n\n";
$emessage .= $message."\n\n";
$emessage .= "--".$uid."\n";
$emessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n";
$emessage .= "Content-Transfer-Encoding: base64\n";
$emessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
$emessage .= $content."\n\n";
$emessage .= "--".$uid."--"; 

if(mail($mailto,$subject,$emessage,$headers)){
    echo "Mailsent successfully";
}
else
{
    echo "Mail couldn't be sent"; 
}
	
	
	
	
	
}

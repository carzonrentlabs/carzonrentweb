<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('max_input_vars', 10000);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/excellib/Classes/PHPExcel.php';
define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

$arr = array();
$date_raw = date("Y-m-d H:i:s");
$first_date = strtotime($date_raw);
$second_date = date("Y-m-d", strtotime('-1 day', $first_date));
$db = new MySqlConnection(CONNSTRING);
$db->open();
$j = 2;
	$objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Mylescars")
            ->setLastModifiedBy("Mylescars");
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue('A1', "Unique ID");

    $objPHPExcel->getActiveSheet()->setCellValue('B1', "Search start date");
    $objPHPExcel->getActiveSheet()->setCellValue('C1', "Search start time");
    $objPHPExcel->getActiveSheet()->setCellValue('D1', "Pickup date");
    $objPHPExcel->getActiveSheet()->setCellValue('E1', "Pickup time");
    $objPHPExcel->getActiveSheet()->setCellValue('F1', "Dropoff date");
    $objPHPExcel->getActiveSheet()->setCellValue('G1', "Dropoff time");
    $objPHPExcel->getActiveSheet()->setCellValue('H1', "City of Search");
    $objPHPExcel->getActiveSheet()->setCellValue('I1', "No. of cars models visible");
    $objPHPExcel->getActiveSheet()->setCellValue('J1', "Car Model");
    $objPHPExcel->getActiveSheet()->setCellValue('K1', "Price for each car model");
    $objPHPExcel->getActiveSheet()->setCellValue('L1', "Locations where car model is available");
    $objPHPExcel->getActiveSheet()->setCellValue('M1', "Time spent on search page (in minutes)");
    $objPHPExcel->getActiveSheet()->setCellValue('N1', "Customer name (incase s/he is logged in)");
    $objPHPExcel->getActiveSheet()->setCellValue('O1', "Customer email ID");
    

    $objPHPExcel->getSecurity()->setLockWindows(true);
    $objPHPExcel->getSecurity()->setLockStructure(true);
    $objPHPExcel->getSecurity()->setWorkbookPassword("PHPExcel");

    $objPHPExcel->getActiveSheet()->getProtection()->setPassword('PHPExcel');
    $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true); // This should be enabled in order to enable any of the following!
    $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
    $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
echo $query = "SELECT count(*) as total FROM myles_searches WHERE entry_date = '" .  date('Y-m-d', strtotime(' -1 day')) . "' order by id desc";

$franchisedata1 = $db->query("query", $query);


if (!array_key_exists("response", $franchisedata1)) {
	 for ($limit = 0; $limit < $franchisedata1[0]['total']; $limit = $limit+1000) 
	 {
		 
		 
	 

echo $query = "SELECT * FROM myles_searches WHERE entry_date = '" .  date('Y-m-d', strtotime(' -1 day')) . "' limit $limit,1000";

$franchisedata = $db->query("query", $query);
echo '<pre>';
print_r( $franchisedata);
if (!array_key_exists("response", $franchisedata)) {
    
   /* for ($i = 0; $i < count($franchisedata); $i++) {
        $customer_id1 = $franchisedata[$i]["customer_id"];
        $entry_date = date("d-M-Y",strtotime($franchisedata[$i]["entry_date"]));
        $entry_time = $franchisedata[$i]["entry_time"];
        $pickup_date = date("d-M-Y",strtotime($franchisedata[$i]["pickup_date"]));
        $pickup_time = $franchisedata[$i]["pickup_time"];
        $drop_date = date("d-M-Y",strtotime($franchisedata[$i]["drop_date"]));
        $drop_time = $franchisedata[$i]["drop_time"];

        $destination_name = $franchisedata[$i]["destination_name"];
        $guestname = $franchisedata[$i]["guestname"];
        $emailid = $franchisedata[$i]["emailid"];
        $timespentinminute = '';
		$remainingtimespentinseconds = '';
		$finaltime= '';
        $timespent = $franchisedata[$i]["enttime"] - $franchisedata[$i]["starttime"]; 
		if($timespent > 0 ){
        $timespentinminute = intval($timespent / 60);
        $remainingtimespentinseconds = intval($timespent % 60);
		if($remainingtimespentinseconds < 10)
		{
			$remainingtimespentinseconds = '0'.$remainingtimespentinseconds;
		}
		$finaltime = $timespentinminute.':'.$remainingtimespentinseconds;
        }


       /* $objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $customer_id1)
                ->setCellValue('B' . $j, $entry_date)
                ->setCellValue('C' . $j, $entry_time)
                ->setCellValue('D' . $j, $pickup_date)
                ->setCellValue('E' . $j, $pickup_time)
                ->setCellValue('F' . $j, $drop_date)
                ->setCellValue('G' . $j, $drop_time)
                ->setCellValue('H' . $j, $destination_name)
                ->setCellValue('M' . $j, $finaltime)
                ->setCellValue('N' . $j, $guestname)
                ->setCellValue('O' . $j, $emailid); 
        
        
        $modelquery = "SELECT * FROM myles_searches_model WHERE searchid = " . $franchisedata[$i]["id"];

        $modeldata = $db->query("query", $modelquery);
        
        if (!array_key_exists("response", $modeldata)) {
         
            //$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, count($modeldata));

       
            for ($k = 0; $k < count($modeldata); $k++) {
               /* $objPHPExcel->getActiveSheet()->setCellValue('J' . $j, $modeldata[$k]['Model']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $j, $modeldata[$k]['IndicatedPrice']);
                                 ///
                if($modeldata[$k]["id"] != ''){
                $locationquery = "SELECT * FROM myles_searches_sublocations WHERE searchesmodelid = " . $modeldata[$k]["id"];

                $locationdata = $db->query("query", $locationquery);

                if (!array_key_exists("response", $locationdata)) {
                    
                    for ($l = 0; $l < count($locationdata); $l++) {
                   //     $objPHPExcel->getActiveSheet()->setCellValue('L' . $j, $locationdata[$l]['sublocation']);
                        $j++;
                    }
                }
				}
                $j++;
              
            }
        }

       
        $j++;
		} */
	}
	 
	 }
	 
//$callStartTime = microtime(true);
   // $filename = 'searchdata-'.$callStartTime . '.xls';
   // $objPHPExcel->setActiveSheetIndex(0);
   // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
   // $objWriter->save($filename);
	
	//$my_path='/root';
    //$my_path = dirname(__FILE__);
    //$mailto = 'ashish.sharma@mylescars.com';
    //$cc = 'gurkeerat.sekhon@mylescars.com,product@mylescars.com,web-dev@mylescars.com';
    //$mailto='abhishek.singh@mylescars.com';
    //$cc = 'saurabh.avasthi@mylescars.com,hemant.dixit@mylescars.com,sakshi@mylescars.com,product@mylescars.com,abhishek.singh@mylescars.com,dayalu.shanker@mylescars.com';
    //$from_name = "Mylescars";
    //$from_mail = "searchdata@mylescars.com";
    //$my_replyto = "my_reply_to@mail.net";
    //$my_subject = "Search Data - ". date('d/m/Y', strtotime(' -1 day')).' ('.date("d M'y", strtotime(' -1 day')).')';
    //$my_message = "Dear all,<br/>*Please find attached, the compilation of Myles Search Data generated on ".date('d/m/y')."*
//<br/>
//Thanks<br/>
//Technology team";
	
 //   mail_attachment($filename, $my_path, $mailto, $from_mail, $from_name, $my_subject, $my_message, $cc); 
}
$db->close();

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $subject, $message, $cc) {
	$file = $path . '/' . $filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));


    /////////////////////////////////////////
    //$emessage = '';
    $headers = "From: " . $from_mail . "\r\n";
    $headers .= "Cc: " . $cc . "\n";
    $headers .= 'Reply-To: ' . $from_mail . "\r\n";
    $headers .= "Return-Path: " . $from_mail . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\n\n";
    $emessage = "--" . $uid . "\n";
    $emessage .= "Content-type:text/html; charset=iso-8859-1\n";
    $emessage .= "Content-Transfer-Encoding: 7bit\n\n";
    $emessage .= $message . "\n\n";
    $emessage .= "--" . $uid . "\n";
    $emessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\n";
    $emessage .= "Content-Transfer-Encoding: base64\n";
    $emessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\n\n";
    $emessage .= $content . "\n\n";
    $emessage .= "--" . $uid . "--";

    if (mail($mailto, $subject, $emessage, $headers)) {
        echo "Mailsent successfully";
    } else {
        echo "Mail couldn't be sent";
    }
}


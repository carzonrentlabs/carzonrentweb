<?php
error_reporting(0);
ini_set("display_errors", 1);    
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php'); 
include("mpdf.php");
$db = new MySqlConnection(CONNSTRING);
$db->open();
$r = $db->query("stored procedure", "mylespdf()"); 
if($r['response']!="ERROR")
{
foreach($r as $res) 
{
$html ='<div class="container-fluid ltlGreyBg">
<div class="container">
<p>Car Rental Agreement</p>
</div>	
</div>
<div class="clr"> </div>
<div class="tc_myles dt">
<div class="mainwrapper">
<div class="agreement_tc">
<div class="tabcontent agreement">
<div class="term_head">NOW, THEREFORE, THIS AGREEMENT WITNESSETH AND IT IS HEREBY AGREED BY AND BETWEEN THE PARTIES AS FOLLOWS:</div>
<div class="clr"> </div>
<p>This CAR RENTAL AGREEMENT ("Agreement") is made and entered on this ' . date("l jS \of F") . ',' . date('Y') . ' ("Effective Date") at ' . $_REQUEST['location'].';</p>
<p class="term_head">By and between</p>
<p><strong>CARZONRENT (INDIA) PRIVATE LIMITED,</strong> a private limited company incorporated under the provisions of the Companies Act 1956, having its Registered Office at 9th Floor Videocon Tower, E-1 Block, Jhandewalan Extension, New Delhi 110055 represented through Ashish Sharma, duly authorised in this behalf by resolution dated 21st March 2016 of the board of directors (hereinafter referred as the <strong>&#34;Company&#34;</strong> and which term shall unless excluded by or repugnant to the context shall mean and include its successors, administrators and assignees etc. ) of the FIRST PART;</p>
<p class="term_head"><strong>AND</strong></p>
<p>'.$res['name'].', Phone no:'.$res['mobile'].', Email Address :'.$res['email'].', (hereinafter referred as the &#34;User&#34; and which term shall unless excluded by or repugnant to the context shall mean and include his/her legal heirs and representatives etc. ) of the SECOND PART;</p>
<p>The Company and the User may be collectively referred to as the <strong>&#34;Parties&#34;</strong> and individually as <strong>&#34;Party&#34;.</strong></p>
<p class="term_head"><strong>WHEREAS</strong></p>
<p>
<li>The Company is, inter alia, engaged in the business of providing passenger transportation in Vehicles, including providing self driven cars, on rental basis.</li>
<li>The User wishes to hire the self driven cars of the Company (&#34;<strong>Vehicle(s)</strong>&#34;) on the rental basis and has therefore approached the Company for the purposes of availing the services of the Company of renting out self driven cars, from time to time, as described in the invoice, for a particular duration and for plying the Vehicle within India as per details mentioned herein. The Vehicles that may be rented to the User are available for renting on the web portal of the Company.</li>
<li>The Company is in the possession of the Vehicle and the Vehicle is registered as a commercial vehicle in the name of the Owner, for the purposes of compliance with Motor Vehicle Act, 1988 (the &#34;<strong>Act</strong>&#34;) and the provisions thereof. </li>
<li>The Company has agreed to lease and the User has agreed to take on hire the Vehicle, as may be chosen from the available Vehicles on the web portal of the Company, on and subject to the mutually agreed terms and conditions appearing hereinafter.</li>

<strong>NOW, THEREFORE, THIS AGREEMENT WITNESSETH AND IT IS HEREBY AGREED BY AND BETWEEN THE PARTIES AS FOLLOWS:</strong>

<li>
<u><strong>Renting of Vehicles</strong></u>
</li>
<p>
<li><strong>1.1</strong> The Company hereby agrees to rent to the User and the User hereby agrees to take on rent the Vehicle, upon placement of the Rental Order (<i>as defined hereinafter</i>) for a Vehicle, subject to all the terms and conditions contained herein.</li>
<li><strong>1.2</strong> The User shall as and when required by him/her, during the term of this Agreement, place the order for Vehicle (&#34;<strong>Rental Order</strong>&#34;) specifying the type of the Vehicle time period for which the Vehicle is to be used by the User and the Company shall provide the Vehicle to the User on rent in accordance with the terms and conditions as specified in this Agreement. </li>
<li><strong>1.3</strong> The User shall simultaneously with the placement of the Rental Order, submit with the Company a copy of valid driving license, passport /Aadhar Card/ Voter ID of the User and any other document that may be required by the Company. The User hereby represents and warrants that the User shall submit genuine, correct and accurate documents, set out in this Clause, to the Company.</li>
<li><strong>1.4</strong> The Company upon the receipt of the Rental Order and upon receipt of the rental amount as specified in the website shall confirm the booking and the Company shall send confirmation e-mail to the User. The User shall thereafter reach the designated location, as informed by the Company and the possession of the Vehicle along with the documents in relation to the Vehicle as appended in <strong>Annexure B</strong> to this Agreement (<strong>&#34;Vehicle Documents&#34;</strong>) shall be handed over by the officials of the Company to the User for rental purposes (<strong>&#34;Rental Period&#34;</strong>). If the User does not show up for picking up the Vehicle within 2 hours of scheduled pick up time, the booking for the said Vehicle shall be considered as no show and be cancelled and complete booking amount will be forfeited. The hourly and daily rental terms and conditions with respect to the renting of the Vehicles under this Agreement are appended as <strong>Annexure A.</strong></li>
<li><strong>1.5</strong> The User hereby authorizes the Company to use the debit/credit card/wallet of the User to the debit payments due to the Company in terms of this Agreement through irrevocable standing instructions issued by the User in the format appended as <strong>Annexure C</strong> to this Agreement (<strong>&#34;Standing Instructions&#34;</strong>). The Standing instructions shall remain valid for a period of 15 days from the date of handing over of possession of the Vehicle by the User to the Company.</li>
<li><strong>1.6</strong> In the event of the driving license being changed, altered, expired or renewed during the term of this Agreement, a copy of the same shall be uploaded by the User on the web portal <a href="http://www.mylescars.com/" target="_blank">www.mylescars.com</a> [OR] a copy of the same shall be handed over the Company within 24 hours of such renewal of the driving license. However, in the event the driving license of the User has expired or has become invalid for any reason whatsoever, during the period where the Vehicle is in possession of the User for its use, the User shall within 24 hours of the driving license of the User having expired or having become invalid, shall hand over the possession of the Vehicle to the Company along with the Vehicle Documents.</li>
<li><strong>1.7</strong> In the event where User anticipates delay in car return, he has two options to intimate the delay. User shall contact the Customer Care Department of the Company and in case where Customer Care is not reachable/out of reach, User shall inform the delay to the Company via e-mail however, extension is subject to the availability of the car. Reporting time in such cases shall be calculated from the time of dispatching of the e-mail by the User.</li>
<li><strong>1.8</strong> In the event where User has a booking against a car, he is not allowed to make fresh booking for himself and request an extension of previous booking made by him. In this case, the User has to return the previous booking car on the scheduled drop time or else face extension/late return charges and pick up car against new booking on scheduled time or face no-show case. Extension of booking shall be made subject to approval of the Company and all other situations shall be treated as violation of terms and conditions of the Agreement.</li>
<li><strong>1.9</strong> In the event the User desires to extend the period of usage of Vehicle beyond the Rental	 Period, it shall intimate to the Company 24 hours prior to expiry of the Rental Period. The Company may extend the period of lease of the Vehicle beyond the period of Rental Period (<strong>&#34;Additional Period&#34;</strong>) depending upon the availability of the Vehicle, to the satisfaction of the Company. In the event the Company intimates to the User that the Vehicle cannot be rented to the User for the Additional Period, User shall not be entitled to use the Vehicle for a period beyond the Rental Period and the User shall hand over the possession of the Vehicle along with all the Vehicle Documents before the expiry of the Rental Period. </li>
<li><strong>1.10</strong> In the event the User desires to extend the period of usage of Vehicle beyond the Rental Period and informs about the same before 2 hours prior to scheduled drop time in case of hourly bookings/24 hours prior to scheduled drop time in case of daily bookings, and the Vehicle is available to be rented to the User for the extended period, the Company will grant the approval of the Vehicle to be rented for extended period whereby booking extension shall attract a surcharge of 30% in addition to actual rental amount applicable for the duration for all types of bookings. The User shall not be entitled to use the Vehicle beyond the extended period, and the User shall hand over the possession of the Vehicle along with the Vehicle Documents prior to the expiry of such extended period. </li>
<li><strong>1.11</strong> In the event the User desires to extend the period of usage of Vehicle beyond the Rental Period and informs about the same within 2 hours of scheduled drop time or after that, in case of hourly bookings/24 hours of scheduled drop time or after that, in case of daily bookings, and the Vehicle is available to be rented to the User for the extended period, the Company will grant the approval of the Vehicle to be rented for extended period, the same shall be treated as late return, thereby attracts a surcharge of 50% in addition to the actual rental amount applicable for the duration of all types of bookings. </li>
<li><strong>1.12</strong> The User shall be charged 100% of the actual rental amount, mentioned in the invoice, in addition to such rental amount for such extra period of unauthorized use or possession of Vehicle, as may be the case, by the User in the event: 
<ul>
<li>User uses the Vehicle beyond the Rental Period without seeking an approval from the Company in the manner provided in this Agreement; or User uses the Vehicle beyond the Rental Period, where the approval has been sought by the User but the Company has not approved the usage of Vehicle beyond the Rental Period, orUser sought the approval of the Company for the Additional Period but does not make the payment of amounts due for the Additional Period; or </li>
<li>User uses the Vehicle beyond the Additional Period; or</li>
<li>Where the User does not hand over the possession of the Vehicle along with the Vehicle Documents upon the expiry or termination, whichever is earlier, of the Rental Period, where the Additional Period has not been approved/granted by the Company, or the Additional Period.</li>
</ul>
<p>
The Company, in the cases of occurrence of the events set out above in this Clause, shall send a payment link for settling of respective charges to the User and it shall be the responsibility of the User to settle the relevant amount claimed by the Company within 4 hours.
</p>
<p>
In cases where the User fails to settle the respective charges within the time stipulated, the Company shall have a right to debit/deduct such amount from the Designated Account of the User through the Standing Instructions as well as through Security Deposit. 
</p>
</li>
<li><strong>1.13</strong> Notwithstanding the debit of the amount as specified in Clause <strong>1.12</strong> above of this Agreement, the Company shall have a right to re-possess the Vehicle upon the termination of the Rental Period/Additional Period.</li>
<li><strong>1.14</strong> Payment of any nature including charge/fees payable under this Agreement shall not be allowed to be made in cash. The User hereby authorizes the Company to charge its debit card/credit card/wallet for which he shall submit the necessary details at the time of taking placing the Rental order towards the amounts due under this Agreement including applicable Rental charges, penalties and damages etc.</li>
</li>
</ul>
<ul>
<li>
<u><strong>Obligations of the User</strong></u>
</li>
</ul>
<ul>
<li><strong>2.1</strong> The User represents and warrants the Company that the Vehicle rented to it by the Company shall be self driven by the User only and the User shall not assign/sub-rent/license the rights in the Vehicle to any other person whatsoever. In the event of breach of this Clause, all liabilities in relation to the Vehicle will be passed on to User apart from forfeiting Security Deposit and booking amount.</li>
<li><strong>2.2</strong> In case the Vehicle is supposed to be driven by any other person apart from the User, the User shall ensure that such driver is holding a valid driving license and the User shall obtain prior written consent of the Company for such person at the time of placing the Rental Order, such that the additional driver’s name, driving license or any other document as may be demanded by the Company must be submitted along with the Rental Order and the same is approved by the Company. In the event the User allows or permits use of the Vehicle by any person other than the User without obtaining a prior written approval from the Company, the User shall be liable to pay damages to the tune of Rs. 5000 to the Company for each such unauthorized use of the Vehicle by the Company.</li>
<li><strong>2.3</strong> The User agrees that he  will return the Vehicle, together with all its tyres, tools, Vehicle documents, accessories and equipment, to the agreed return location specified on the date specified in the invoice, unless the User requests and Company agrees to an extension in writing or sooner upon demand being made by the Company. In the event where the User desires to retun the Vehicle before scheduled drop time, he shall not be entitled to get/claim any refund/waiver for the balanced period for which the Vehicle was booked, under this Agreement. The User shall ensure that the Vehicle, along with accessories and requisite documents, are in proper operating condition, clean and fit for the purpose for which it is required, to the satisfaction of the Company. The User shall ensure that five tyres are in good condition without puncture or any other defect that renders the tyres non workable.</li>
<li><strong>2.4</strong> In event of damage caused to any part of the Vehicle or any accessory thereof, other than normal wear and tear thereof, the User agrees to repair and/or replace the same forthwith at his/her own expense with a part/accessory/tyre of the same dimension/brand and in equivalent state of wear at its own cost. The odometer and accessories of the Vehicle shall not be damaged//tampered and in case it is done, the User is liable for the replacement of the same at its own cost. No parts of the Vehicle shall be replaced without obtaining a prior written consent of the Company</li>
<li><strong>2.5</strong> The User shall return the possession of the Vehicle to the Company in the condition, subject to normal wear and tear in which the delivery of the same was taken. In the event, upon the handing over of the Vehicle by the User, the Company finds any defect in the Vehicle or any part or accessory of the Vehicle is damaged or defective, the Company shall deduct such amount as required for repairing such defect in Vehicle from the Security Deposit/through Standing Instructions. In cases of latent or hidden defect which the Company discovers within a period of 15 days from the date of handing over the possession of the Vehicle by the User, the Company shall deduct/debit such amount equivalent to the cost of repairing such defect in Vehicle from the Security Deposit/through Standing Instructions. The User shall be refunded with the unadjusted amount of Security Deposit after expiry of aforesaid period of 15 days.</li>
<li><strong>2.6</strong> The User shall pay all the toll taxes and other taxes linked with usage of the vehicles in as per the applicable laws. The User shall pay the state entry tax as per the applicable laws irrespective of the period of lease of the Vehicle.</li>
<li><strong>2.7</strong> The User shall hand over the possession of the Vehicle to the Company upon the expiry or termination, as may be the case, of the Rental Period or Additional Period, as may be the case, of the Vehicle. The Vehicle Documents shall be handed over to the Company by the User simultaneously with handing over of the possession of the Vehicle to the Company with any demur of delay. The User hereby acknowledges and agrees that the Security Deposit amount shall be returned by the Company only after 15 days of handing over possession of the Vehicles along with Vehicle Documents and it shall not raise any objection to the same in any manner whatsoever. The User hereby acknowledges and agrees that the said period of 15 days is taken by the Company to found any latent or hidden defect in the Vehicles caused by the User, which is sufficient and shall not be objected by the User in future in any manner whatsoever.</li>
<li><strong>2.8</strong> It shall be the responsibility of the User to take care of all its belongings in the Vehicle and check the same at the time of handing over the possession of the Vehicle or at the time of repossession of the Vehicles by the Company in terms of this Agreement. The Company shall not be liable for loss or damage to any belongings of the User and the User shall not claim from the Company for the loss or damage of any of its belongings.</li>
<li><strong>2.9</strong> The User shall neither use nor allow the Vehicle to be used for any illegal or unlawful activity or for any other purposes contrary to the provisions of the applicable laws.</li>
<li><strong>2.10</strong> The User shall neither consume nor allow the consumption of alcohol, cigarettes or any contraband or psychotropic substance in the Vehicle.</li>
<li><strong>2.11</strong> The User shall neither carry or allow or cause any person to carry any arms, ammunition or inflammable or explosive substance inside the Vehicle.</li>
<li><strong>2.12</strong> The User shall be solely liable for cost of all fuel consumed, unless the same is covered in the package while booking the Vehicle,  during the Rental Period and/or Additional Period (as may be the case) and the restoration.</li>
<li><strong>2.13</strong> The User hereby acknowledges and agrees to the following:
<ul>
<li>Handing over the Vehicle in the same good order and condition to the satisfaction of the Company;</li>
<li>The User shall maintain the Vehicle as a prudent man.</li>
<li>The Vehicle will be provided with a full tank of fuel. The Vehicle will be restored with the Company containing the same amount of fuel in it as was provided at the time of handing it over to the User. In the event User does not provide the fuel as has been mentioned in the aforesaid clause, the User will be charged for short fuel along with 10% surcharge on fuel price.</li>
</ul>
</li>
<li><strong>2.14</strong> That the User agrees that after being handed over with the possession of the Vehicle, the Vehicle shall not be operated:
<ul>
<li>To transport goods in violation of Customs regulations or in any other illegal manner.</li>
<li>To carry passengers or property for a consideration express or implied;</li>
<li>To propel or tow any Vehicle or trailer without obtaining a prior written consent of the Company;</li>
<li>In motor sport events (including racing pacemaking, reliability trials, driving lessons speed testing and any other events of similar nature):</li>
<li>By any person driving when unfit through drink or drugs or with blood alcohol concentration above the limit prescribed by the applicable law;</li>
<li>By any person other than the User or any person(s) nominated or employed by the User who is approved by the Company; who is at least 23 years of age or older as stated on the Company’s tariff, is duty qualified and holds and has held a current valid full driving licence for at least one year, or in the case of breakdown or accident, a motor Vehicle repairer provided that he is duly qualified and licensed.</li>
<li>Outside geographical boundaries of the Republic of India.</li>
<li>User will not take a Vehicle to high terrain areas, terrorism/naxal affected areas.</li>
</ul>
</li>
<li><strong>2.15</strong> The User is personally liable to pay the Company on demand:
<ul>
<li>A mileage charge computed at the rate specified in enclosed invoice for the mileage covered by the Vehicle until the Vehicle is returned (the number of miles over which the Vehicle is operated shall be determined by reading the odometer installed by the manufacturer, if odometer fails the mileage charge shall be calculated from the road map distance of the journey traveled):</li>
<li>Time, damage waiver (if any), personal accident insurance (if any), pick up service, drop service. Fuel and miscellaneous charges at the rates specified in invoice.</li>
<li>All fines and court costs for parking, traffic or other legal violations assessed against the Vehicle, the User, other driver or the Company until the Vehicle is returned, except where caused through fault of the Company.</li>
<li>The Company&#39;s cost, including reasonable legal fees where permitted by law, incurred collecting payments due from the User hereunder, and</li>
<li>The Company&#39;s cost for repairing collision or upset damages to the Vehicle, provided, however, if the Vehicle is operated in accordance with all the terms hereof, the User’s liability for such damage shall be waived if the User has purchased the damage waiver in advance subject to damage waiver policy.</li>
<li>Value Added Tax and all other applicable taxes shall be payable extra and in addition to the rental amount.</li>
</ul>
<p>
All the above expenses shall be adjusted from the Security Deposit (as defined below). In the above expenses exceed the amount of Security Deposit, the User shall pay such extra amount to the Company.
</p>
</li>
<li><strong>2.16</strong> That the User undertakes to take all necessary steps to protect the interest of the Company and the Company&#39;s insurance company and shall ensure that the Vehicle is not used for any purpose not permitted by the terms and conditions of the relevant policy of insurance.</li>
<li><strong>2.17</strong> The User shall not do or allow to be done any act or thing where by any such policy of insurance may be avoided nor taken outside any territorial limit stipulated in such policy of insurance. The User and any authorized User shall further participate as an insured under an automobile insurance policy. The User is bound by and agrees to the terms and conditions thereof. The User agrees further to protect the interests of the Company and its insurance company in case of accident or theft by doing the following, as applicable:
<ul>
<li>By co-operating with the Company to comply with necessary documentation and other formalities required for claiming insurance.</li>
<li>Obtaining names and addresses of the parties involved, and of witnesses;</li>
<li>Not admitting liability or guilt or giving money to any persons involved;</li>
<li>Not abandoning the Vehicle without adequate provision for safeguarding and securing same.</li>
<li>Calling the Company within 24 hours by telephone and forward a copy of FIR (if applicable) even in case of slight damage; further completing the Company&#39;s accident report including diagram as required on return of Vehicle; and </li>
<li>Notifying the police immediately if another party&#39;s guilt has to be ascertained, or if any person is injured.</li>
</ul>
</li>
<li><strong>2.18</strong> The Company hereby declines that it has no responsibility for subjects left in the Vehicle during the Rental Period of the Vehicle. The Company further declares that it has no responsibility for injury to third parties or damages to the Vehicle, which the User may cause during the Rental Period or Additional Period.</li>
<li><strong>2.19</strong> the Company whilst taking all precautions and using its best efforts to prevent such happening shall not be liable for any loss or damage arising from any fault or defect in or from mechanical failure of the Vehicle of any consequential loss or damage.</li>
<li><strong>2.20</strong> That User shall during the continuance of the Rental Period:
<ul>
<li>Not drive the Vehicle outside the territorial limits of India. In the event of any loss or damage suffered by the Company as a result of driving the Vehicle outside the territorial limits, the User shall indemnify the Company for such losses or damages;</li>
<li>Always lock the Vehicle when not in use and ensure it is adequately protected against damage due to adverse weather conditions.</li>
<li>Not allow any person without the prior authorization of the Company to carry any work which otherwise interferes with the Vehicle or any part thereof except if the prior authorization cannot be obtained and the repair is minor to the tune of Rs. 500/- (Rupees Five Hundred Only).</li>
<li>Not act as or purports to act as the agent of the Company for any purpose whatsoever.</li>
<li>Be fully responsible for any loss or damage caused to the Vehicle howsoever occasioned other than normal wear and tear. The User shall give immediate notice to the Company and subsequently confirm in writing by sending through speed post/courier within 24 hours of any loss or damage caused to the Vehicle or any breakdown, malfunction or other failure thereof. The User shall not continue to use the Vehicle in the event of damage to or a breakdown of the Vehicle if to do so would or might cause further damage to the Vehicle. The User shall take the Vehicle to the nearest authorized service centre for repair and pay such cost as estimated by the authorized service centre and shall ensure delivery of the Vehicle of the Company. The User shall be responsible to bear all cost for handover of the Vehicle.</li>
<li>Without prejudice to provision for Company&#39;s right to indemnification under this Agreement, the User agrees to pay to the Company for each and every breach of terms and condition of this Agreement as detailed in this agreement.</li>
<li>The User agrees that subsequent to taking over of the possession of the Vehicle by the Company from the User after the expiry of the Rental Period if the Company detects any damage in the Vehicle, the Company shall intimate details of such damage and the cost payable for such damage to the User. If the User fails to pay such cost even after intimation from the Company for three times, it shall be deemed acceptance by the User of his/her liability to pay for such damage and on such happening, the User authorizes the Company to deduct cost of such damage as mentioned below from the Security Deposit paid to the Company at the time of booking.</li>
<li>Not sell, assign, pledge, let or hire or otherwise dispose of the Vehicle and/or its equipment or attempt to do any of these things. Not allow any person to use or drive the Vehicle without prior written consent of the company save and except as provided in this agreement.</li>
<li>Neither remove nor change any name or other mark identifying the ownership of the Vehicle; </li>
<li>Neither use nor allow anyone to carry passengers more than permitted by the registration paper.</li>
<li>Neither use nor drive the Vehicle under the influence of alcohol or narcotic drugs</li>
<li>Neither drive the Vehicle beyond permissible speed limit nor contravene with the provisions, of any statue, statutory, instrument, regulation relating either to the Vehicle to the Vehicle of its use and procure that driver of the Vehicle shall observe and perform the terms and conditions of all policies or contracts of insurance relating to the Vehicle for its use.</li>
<li>Acknowledge that the Vehicle is and shall be throughout the period of its hiring be the sole property of the Company and/or its affiliates and all rights thereto shall vest in the company and/or its affiliates.</li>
<li>Not acknowledge or compound any claim either partial or in full in respect of any accident involving the Vehicle.</li>
<li>The User agrees to comply with the &#34;Car Usage Conditions&#34; appended as Annexure D to this Agreement.</li>
<li>The cost/penalty payable by the User for repair/replacement of any part of the Vehicle is listed in detail in <strong>Annexure E</strong> of this Agreement (hereinafter referred to as <strong>&#34;Damages/Penalty&#34;</strong>).</li>
<li>The User must swiftly report any incident involving loss or damage to the Vehicle, and no later than 4 (four) hours of the occurrence of such loss or damage, while rented under this Agreement or to any property or person to the Company location from where the Vehicle was hired and will deliver to the Company immediately, every summons, complaint or paper in relation to such loss.</li>
<li>The User shall release and hold harmless the Company (and its directors, officers and employees) from all claims for loss or damage to their personal property. Or that of any other person’s property left in the Vehicle, or which is received, handled or stored by the Company at any time before, during or after this Rental Period, whether due to the Company&#39;s negligence of otherwise.</li>
</ul>
</li>
<li><strong>2.21</strong> Notwithstanding anything contained in this Agreement, the User shall be liable to pay for damages not covered under the Company&#39;s insurance policy for the Vehicles.</li>
<li><strong>2.22</strong> The User hereby further represents and warrants that the information and documents of the User supplied by him/her to the company are true, correct and accurate.</li>
<li><strong>2.23</strong> The User hereby acknowledges and agrees that the User shall be liable in respect of the Vehicle including but not limited to any challans, penalties, third party liability, accidents and the User shall indemnify the Company against any losses, damage, costs and liabilities etc., in this regard.</li>
<li><strong>2.24</strong> In the event of any breach by the User of any of the terms and conditions hereof, the Company may with prior notice of 24 hours repossess the Vehicle and for such purpose may enter upon premises where the Vehicle may be and remove the same and the User shall be responsible for and indemnify the company against all actions, claims, costs and damage consequent upon or arising from such repossession and removal.</li>
<li><strong>2.25</strong> The User hereby understands and agrees that any fine arising from parking, traffic or driving violation will be charged along with 100% surcharge on such amount, VAT/Service Tax at applicable rates will be charged extra. Any increase in tax or the incidence of new taxes will be charged directly from the User in additional to the rental charges.</li>
<li><strong>2.26</strong> The User hereby understands and agrees that the Security Deposit shall be refunded to the User, after deduction of damages and any other amounts payable under this Agreement.</li>
<li><strong>2.27</strong> During the usage of the Vehicle, if the Vehicle is damaged, user will intimate the same to Company within 1 (one) hour of such damage and any repair or emergency assistance and all other matters in relation to the Company shall be dealt as per advice provided by the Company.</li>
<li><strong>2.28</strong> In this Agreement, the word “Vehicle” shall in addition to the meaning ascribed hereto shall also include any replacement thereof and shall include all equipment, accessories, tools and spare tyre relating to the same and the singular shall where as appropriate including the plural and vice-versa. Any reference to any statutes shall be deemed to refer to any statutory modification or re-enactment, or any rules, regulations, notifications, circulars, etc. made or issued thereunder for the time being in force.</li>
<li><strong>2.29</strong> The User agrees that the data provided in this Agreement and at the time of placing the Rental Order may be stored, processed and transmitted manually/electronically by the Company. The User also agrees to provide accurate information Rental Order and shall be liable for any damages and disputes arising due to the inaccuracy of the information. In addition to obligation of the User to indemnify the Company for losses as a result of providing incorrect data, the Company shall have the right to forfeit the Security Deposit and terminate the Agreement with immediate effect.</li>
<li><strong>2.30</strong> In an event wherein cancellation of the Rental Order is done by the Company for whatsoever reason, Company shall refund booking amount and shall pay an amount equivalent to the rental amount. In case of cancellation of booking by company due to car unavailability, full booking amount will be refunded and User will also get 20% discount on next booking. The compensation, if any, payable to the User shall be decided by the Management of the Company and such decision shall be final and the User shall have no objection to the same.  Cases related to cancellations due to force majeure, fog, earthquake, floods, fire, riots, strike or circumstances beyond Company control will not be covered under this clause and in such cases only booking amount will be refunded. Parties hereby agree that under no circumstances the liability of the Company shall exceed the rental charges of the Vehicle.</li>
<li><strong>2.31</strong> Notwithstanding anything in this Agreement, the Company has the sole right to refuse delivery of the Vehicle to the User upon placement of the Rental Order without any reason subject to refund of amounts paid by the User to the Company.</li>
<li><strong>2.32</strong> Notwithstanding anything contained in this Agreement, the Company reserves the right to take back the Vehicle at any time during the continuance of Rental Period or thereafter, if in the opinion of the Company, the User or any third party may damage the Vehicle or may use the Vehicle for any unauthorized purpose.</li>
</ul>
<ul>
<li>
<u><strong>Termination</strong></u>
</li>
</ul>
<ul>
<li><strong>3.1</strong> The Company shall have the right to terminate this Agreement right to terminate this Agreement in the following cases:
<ul>
<li>In the event of breach of any term or condition of this Agreement, by giving a notice in writing; and</li>
<li>Without any cause by giving 24 (twenty four) hours written notice.</li>
</ul>
</li>
</ul>
<ul>
<li>
<u><strong>Indemnity</strong></u>
</li>
</ul>
<ul>
<li><strong>4.1</strong> The User agrees to indemnify and hold the Company including its directors, shareholders and employees harmless for any loss suffered by the Company including reasonable legal costs and third parties claims arising out of or in relation to the breach of its obligations, representations, warranties and covenants by the User of this Agreement and against all claims, penalties, proceedings, civil or criminal, initiated against the Company as a result of such breach.</li>
<li><strong>4.2</strong> The User agrees to indemnify and hold the Company its directors, shareholders and employees harmless for any loss suffered by the Company including reasonable legal costs, liabilities, costs actions, claims, any fines or penalties imposed in respect of or arising out of use of the Vehicle, including losses on account of any third party liability and third parties claims, as a result of unlawful or illegal use or misuse of the Vehicle by the User or as a result of use of the Vehicle for the purposes contrary to those set out in this Agreement.</li>
<li><strong>4.3</strong> That User hereby releases and agrees to indemnify the Company its directors, shareholders and employees and hold harmless from and against any liability for loss or for damage to any property (including costs relating thereto) left, stored or transported by the User or any other person in or through the Vehicle during or after handing over of the possession or at the time of repossession of the Vehicle to the Company.</li>
<li><strong>4.4</strong> The User shall be solely responsible for and hold the Company fully indemnified against any loss, theft, damage, costs, accident or injury (including death) to persons or property, including loss, theft, damage, accident of the Vehicle, accruing in connection with the Vehicle or as a result of the negligent use thereof and against any breach of obligation by the User of this Agreement. This Clause 3 relating to obligation of the User to indemnify shall survive termination of this Agreement.</li>
</ul>
<ul>
<li>
<u><strong>Miscellaneous</strong></u>
</li>
</ul>
<ul>
<li><strong>5.1</strong> <u>Amendment:</u> Any addition, amendment or alteration to the terms and conditions of this Agreement shall be null and void unless agreed upon in writing by the Parties.</li>
<li><strong>5.2</strong> <u>Assignment:</u> The User shall not transfer or assign any of its rights or obligations under this Agreement without the prior written approval of the Company.</li>
<li><strong>5.3</strong> <u>Notice:</u> Unless otherwise stated, all notices, approvals, instructions and other communications for the purposes of this Agreement shall be given in writing and may be given by facsimile, by e-mails, by personal delivery or by sending the same by registered acknowledgement due or courier addressed to the party concerned at the address first stated herein above or any other address subsequently notified to the other party. Such notice shall be deemed to be delivered on receipt thereof.</li>
<li><strong>5.4</strong> <u>Dispute Resolution:</u> In the event of any dispute or difference arising out of or in connection with this Agreement, the Parties shall endeavor to resolve the same mutually through amicable discussions. In the event the Parties fall to arrive at an amicable settlement within 15 days from the date of reference thereof for amicable discussions, such dispute/difference shall be settled through arbitration and shall be referred to the sole arbitrator to be appointed by the Company. The arbitration proceedings shall be conducted in accordance with the provisions of the Arbitration and Conciliation Act, 1996 and/or any amendments and modifications made thereto. The arbitration proceedings shall be conduct in the English language and the place of arbitration shall be Delhi. It is hereby further agreed that the award of the arbitrator shall be final and binding on the parties. This clause shall survive expiry or termination of this Agreement.</li>
<li><strong>5.5</strong> <u>Governing Law:</u> This Agreement shall be governed by and construed in accordance with the laws of India and subject to dispute resolution mechanism hereby agreed, the Parties have agreed to submit themselves to the jurisdiction of the courts of the place where the Vehicle is delivered by the Company to the User.</li>
<li><strong>5.6</strong> This Agreement constitutes the entire agreement between the Parties with respect to the subject matter hereof and supersedes all prior communications negotiations and representations either oral or written, between the Parties, in relation hereto. The terms and conditions of the rental of the Vehicles to the User shall be governed by this Agreement.
<p>
IN WITNESS WHEREOF, the Parties have executed this Agreement through their duly authorized representatives at the place and on the date, month and year first above written.
</p>
</li>
</ul>

<table width="100%" border="1" cellpadding="5">
<p class="term_head">
<strong>ANNEXURE A</strong> <br>
<strong>TERMS AND CONDITIONS <br>
Daily rental Terms and Conditions</strong>
</p>
<tr>
<th width="30%" align="left" valign="top">Daily Rental- Terms and Conditions</th>
<th align="left" valign="top">Description</th>
</tr>
<tr>
<td align="left" valign="top">Eligibility Age</td>
<td align="left" valign="top">
For Tata Nano &#45; 18 years or above <br>
For other Myles Cars &#45; 23 years above
</td>
</tr>
<tr>
<td align="left" valign="top">Documents</td>
<td align="left" valign="top">Passport/Voter ID/Aadhar Card, Driving license, Credit Card with desired limit of Security Deposit of Rs. 5,000 (Rupees Five Thousand Only) for non-luxury Cars and Rs. 50,000/- to Rs. 1,00,000/- for Luxury Cars depending on the model as specified on Website. If the User does not present the above documents, in original at the time of obtaining delivery of the Vehicle, the Company shall be entitled to refuse the delivery of the Vehicle and cancel this Agreement in such an event 50% of the amount paid by the User shall be forfeited by the Company subject to minimum of one day rental.</td>
</tr>
<tr>
<td align="left" valign="top">Rental Period</td>
<td align="left" valign="top">Rental period will be of '.$res['total_hours'].' hours from the user booking time. The said period shall be calculated from the time the User collects the Vehicle and up to the time the User returns the rental vehicle.</td>
</tr>
<tr>
<td align="left" valign="top">Other terms</td>
<td align="left" valign="top">As per terms given below</td>
</tr>
<tr>
<td align="left" valign="top">Pre authorization from credit card</td>
<td align="left" valign="top">Will be done at the time of Vehicle delivery to the User at a rate provided in the Rental Agreement attached herewith. This will be treated as Security Deposit. In case of any damage in the vehicle, cost of damage will be deducted from the pre authorized amount subject to terms of the Agreement.</td>
</tr>
<tr>
<td align="left" valign="top">Delivery /Collection</td>
<td align="left" valign="top">Vehicle must be returned at the location from which it was picked up/hired.</td>
</tr>
<tr>
<td align="left" valign="top">Included KMs</td>
<td align="left" valign="top">Unlimited</td>
</tr>
<tr>
<td align="left" valign="top">Extra KM Rate</td>
<td align="left" valign="top">N/A</td>
</tr>
<tr>
<td align="left" valign="top">Fuel</td>
<td align="left" valign="top">The Vehicle will be provided with a full tank of fuel. In case the tank is not full at the time of return of Vehicle, the User will be charged for short fuel along with 10% surcharge on fuel price.</td>
</tr>
<tr>
<td align="left" valign="top">GPS Navigation system/Child Safety Seat</td>
<td align="left" valign="top">GPS Navigation / Child safety seat shall be available on request with additional charges (Subject to availability)</td>
</tr>
<tr>
<td align="left" valign="top">Threshold time</td>
<td align="left" valign="top">In case of late return, the User will be allowed a leverage of 1 hour  for returning the Vehicle after that late return charges shall be applicable as below.</td>
</tr>
<tr>
<td align="left" valign="top">Returning vehicle late</td>
<td align="left" valign="top">In the event of late return, there  will be 50% surcharge in addition to the actual rental amount for the additional period. </td>
</tr>
<tr>
<td align="left" valign="top">Cancellation</td>
<td align="left" valign="top">
If booking is cancelled 24 hours prior to the scheduled pick up time, no cancellation charges will be applied. If the booking is cancelled within 24 hours of the scheduled pick up time, 1 day rental will be charged and if booking is cancelled on or after pickup time, it will be treated as no show and no amount will be refunded. 
<p>In case of cancellations from Company the customer will be intimated 12 hours before his scheduled pick up time.</p>
</td>
</tr>
<tr>
<td align="left" valign="top">No show</td>
<td align="left" valign="top">In case of No-Show no amount will be refunded to the User</td>
</tr>
<tr>
<td align="left" valign="top">Vehicle Damage</td>
<td align="left" valign="top">Damage charges to be paid by User in terms of the Rental Agreement. In case damage cost goes beyond Security Deposit Amount insurance claim may be filed and Repair/depreciation amount (In case of insurance claim) & Parts which are not covered under insurance will be recovered from the User. The assessment of damage made by Company will be final. Insurance claim is subject to Company&#34;s discretion.</td>
</tr>
<tr>
<td align="left" valign="top">Outstation</td>
<td align="left" valign="top">When traveling out of state, it is always important to step at RTO check post to pay the government applicable fax. Cost/penalties pertaining to such interstate tax, toll or any other govt. levied tax will be borne by the User directly.</td>
</tr>
<tr>
<td align="left" valign="top">Traffic Rule Violation</td>
<td align="left" valign="top">Cost for any traffic rule violation during the Rental Period will be borne by the User directly. In cases, where challans is received by the Company via post upon completion of the Agreement, the User authorizes the Company to charge the same on the credit card number of the User as mentioned in this Agreement offline. In the event where the Company has not charged on the credit card number of the User, it shall be entitled to recover the amount from the User in his next booking. The User shall follow all the rules and regulation under applicable laws while driving the Vehicle and otherwise including applicable speed limits.</td>
</tr>
<tr>
<td align="left" valign="top">Cleanliness</td>
<td align="left" valign="top">Company does not allow the Vehicle to be used to carry pets, goods or any other object which can annihilate the upholstery/any of the vehicles. In such event, the User will be penalized with the 20% surcharge on the actual cost towards repairing/cleaning of interior / exterior of vehicle.</td>
</tr>
<tr>
<td align="left" valign="top">Out of Reach</td>
<td align="left" valign="top">During the continuance of Rental Period or any time after it, if User remains non-contactable for a continuous period of 24 Hours, then the Company shall be entitled to immediately take back the Vehicle and terminate this Agreement.  </td>
</tr>
<tr>
<td align="left" valign="top">Special Pricing</td>
<td align="left" valign="top">During festive season, special pricing shall be applicable and the same will be reflected in inventory at the time of booking. (Special Pricing shall only be applicable at the discretion of the Company under this Agreement.).</td>
</tr>
</table>
<br>
<br>
<table width="100%" border="1" cellpadding="5">
<p class="term_head">
<strong><u>Hourly rental terms and conditions</u></strong>
</p>
<tr>
<th width="30%" align="left" valign="top">Hourly Rental- Terms and Conditions</th>
<th align="left" valign="top">Description</th>
</tr>
<tr>
<td align="left" valign="top">Eligibility Age</td>
<td align="left" valign="top">
For Tata Nano &#45; 18 years or above <br>
For other Myles Cars &#45; 23 years above
</td>
</tr>
<tr>
<td align="left" valign="top">Documents</td>
<td align="left" valign="top">Passport/Voter ID/Aadhar Card, Driving license, Credit Card with desired limit of Security Deposit of Rs. 5,000 (Rupees Five Thousand Only) for non-luxury Cars and Rs. 50,000/- to Rs. 1,00,000/- for Luxury Cars depending on the model as specified on Website. If the User does not present the above documents, in original at the time of obtaining delivery of the Vehicle, the Company shall be entitled to refuse the delivery of the Vehicle and cancel this Agreement in such an event 50% of the amount paid by the User shall be forfeited by the Company subject to minimum of one day rental.</td>
</tr>
<tr>
<td align="left" valign="top">Rental Period</td>
<td align="left" valign="top">Rental Period will be from 4  hour to up to 23 hours from pick up time.</td>
</tr>
<tr>
<td align="left" valign="top">Other terms</td>
<td align="left" valign="top">As per terms given below</td>
</tr>
<tr>
<td align="left" valign="top">VAT</td>
<td align="left" valign="top">As per State rules</td>
</tr>
<tr>
<td align="left" valign="top">Pre authorization from credit card</td>
<td align="left" valign="top">Will be done at the time of Vehicle delivery to the User at a rate provided in the Rental Agreement attached herewith. This will be treated as Security Deposit. In case of any damage in the Vehicle, cost of damage will be deducted from the pre authorized amount subject to terms of the Agreement.</td>
</tr>
<tr>
<td align="left" valign="top">Delivery /Collection</td>
<td align="left" valign="top">Vehicle must be returned at the location from which it was picked up/hired.</td>
</tr>
<tr>
<td align="left" valign="top">Included KMs</td>
<td align="left" valign="top">For all Myles Cars – Max. up to 10 Kms. Per hour, beyond which extra Km, rate shall apply. However, during the day, the maximum limit is 230 Kms. Beyond which extra Km, rate shall apply.</td>
</tr>
<tr>
<td align="left" valign="top">Extra KM Rate</td>
<td align="left" valign="top">As mentioned in the invoice. Amount equivalent to extra Kms. charges shall be debited from the security deposit.</td>
</tr>
<tr>
<td align="left" valign="top">GPS Navigation system/Child Safety Seat</td>
<td align="left" valign="top">GPS Navigation / Child safety seat shall be available on request with additional charges (Subject to availability)</td>
</tr>
<tr>
<td align="left" valign="top">GPS Navigation system/Child Safety Seat</td>
<td align="left" valign="top">GPS Navigation / Child safety seat shall be available on request with additional charges (Subject to availability)</td>
</tr>
<tr>
<td align="left" valign="top">Threshold time</td>
<td align="left" valign="top">In case of late return, the User will be allowed a leverage of 30 minutes for returning the Vehicle after that late return charges shall be applicable as below.</td>
</tr>
<tr>
<td align="left" valign="top">Returning vehicle late</td>
<td align="left" valign="top">In the event of late return, there will be 50% surcharge in addition to the actual rental amount for the additional period.  </td>
</tr>
<tr>
<td align="left" valign="top">Cancellation</td>
<td align="left" valign="top">
No refund on cancellation.
<p>In case of cancellations from company the customer will be intimated 4 hours before his scheduled pick up time.</p>
</td>
</tr>
<tr>
<td align="left" valign="top">No show</td>
<td align="left" valign="top">In case of No-Show no amount will be refunded to the User</td>
</tr>
<tr>
<td align="left" valign="top">Vehicle Damage</td>
<td align="left" valign="top">Damage charges to be paid by User in terms of the Rental Agreement. In case damage cost goes beyond Security Deposit Amount insurance claim may be filed and Repair/depreciation amount (In case of insurance claim) & Parts which are not covered under insurance will be recovered from the User. The assessment of damage made by Company will be final. Insurance claim is subject to Company’s discretion.</td>
</tr>
<tr>
<td align="left" valign="top">Replacement Car</td>
<td align="left" valign="top">Replacement Vehicle shall be provided within a city limits only (Subject to availability of similar segment in the city) in all such cases where in a Vehicle  develops mechanical error and restrain the User from its use. In a case, where in Vehicle develops such error outside the city limit, replacement Vehicle will not be provided and amount equivalent to agreed rental will be charged. Company decision related to replacement vehicle will be final.</td>
</tr>
<tr>
<td align="left" valign="top">Toll and Taxes</td>
<td align="left" valign="top">When travelling out of state, it is always important to stop at RTO check post to pay the government applicable tax. Cost/penalties pertaining to such interstate tax, toll or any other govt. levied tax will be borne by the User directly.</td>
</tr>
<tr>
<td align="left" valign="top">Traffic Rule Violation</td>
<td align="left" valign="top">Cost for any traffic rule violation during the Rental Period will be borne by the User directly. In cases, where challans is received by the Company via post upon completion of the Agreement, the User authorizes the Company to charge the same on the credit card number of the User as mentioned in this Agreement offline. In the event where the Company has not charged on the credit card number of the User, it shall be entitled to recover the amount from the User in his next booking. The User shall follow all the rules and regulations under applicable laws while driving the Vehicle and otherwise including applicable speed limits.</td>
</tr>
<tr>
<td align="left" valign="top">Cleanliness</td>
<td align="left" valign="top">Company does not allow the Vehicle to be used to carry pets, goods or any other object which can annihilate the upholstery/any of the Vehicles. In such event, the User will be penalized with the 20% surcharge on the actual cost towards repairing/cleaning of interior / exterior of Vehicle.</td>
</tr>
<tr>
<td align="left" valign="top">Out of Reach</td>
<td align="left" valign="top">During the continuance of Rental Period or any time after it, if User remains non-contactable for a continuous period of 10 Hours, then the Company shall be entitled to immediately take back the Vehicle and terminate this Agreement</td>
</tr>
<tr>
<td align="left" valign="top">Special Pricing</td>
<td align="left" valign="top">During festive season, special pricing shall be applicable and the same will be reflected in inventory at the time of booking. (Special Pricing shall only be applicable at the discretion of the Company, under this Agreement). </td>
</tr>
</table>
<br>
<br>
<table width="100%" cellpadding="5">
<p class="term_head">
<strong>ANNEXURE B</strong> <br/>
<strong>LIST OF VEHICLE DOCUMENTS</strong>
</p>
<tr>
<td align="left" valign="top">
<ul>
<li>Registration Certificate</li>
<li>Permit Document</li>
<li>Fitness Document</li>
<li>Insurance</li>
</ul>
</td>
</tr>
</table>
<table width="100%" cellpadding="5">
<p class="term_head">
<strong>ANNEXURE C</strong> <br/>
<strong>STANDING INSTRUCTIONS</strong>
</p>
<tr>
<td align="left" valign="top">
I, Mr./Ms.'.$res['name'].', Phone no '.$res['mobile'].', Email Address '.$res['email'].' hereby authorize Carzonrent India Private Limited, a private limited company incorporated under the provisions of the Companies Act 1956, having its Registered Office at 9th Floor Videocon Tower, E-1 Block, Jhandewalan Extension, New Delhi 110055 to use my debit card or credit card or wallet, details whereof is given below) to charge the same towards amounts due to it under the Agreement dated _____ for rental of Vehicles executed between me and the Company.
</td>
</tr>
</table>
<br>
<br>
<table width="100%" cellpadding="5">
<p class="term_head">
<strong>ANNEXURE D</strong> <br/>
<strong>CAR USAGE CONDITIONS</strong>
</p>
<tr>
<td align="left" valign="top">
<p>Following conditions will be considered as Myles car usage. Customers will be fined if found involved in any of the following:</p>
<ul>
<li><strong>Over speeding:</strong> Customers are allowed to drive the Vehicle up to a maximum speed of 100 km/hr (63 miles/hr), beyond which the Vehicle will be considered as over speeding. A penalty of Rs. 200 will be charged on the first instance, followed by a further penalty of Rs. 500 and Rs. 1000 for 2nd instance and 3rd instance respectively making it a total of Rs. 1,700/- after third instance. After the third instance, Booking will stand cancelled and the Company shall get the right to take back the Vehicle and terminate this Agreement.</li>
<li><strong>Traffic violation:</strong> Users are liable to pay for any traffic violation tickets received during the Rental Period. Company can charge traffic violation from the User&#39;s credit/debit card on actual if the violations tickets are sent directly to the Company by the traffic department.</li>
<li><strong>Car spare part changed:</strong> Users should not charge or remove any car spare parts. In case of emergency, the User should inform Company and act as per advice. Users will be charged a penalty of Rs. 5000 over and above the cost of spare part.</li>
<li><strong>Tyre misuse:</strong> In case of any tyre damages resulting from driving in bad terrain and continued driving in case of tyre puncture, User will be charged for the cost of tyre on actuals.</li>
<li><strong>Running Vehicle in damaged conditions:</strong> Users are not advised to drive the Vehicle if it gets damaged in an accident. Users are advised to inform the Company immediately. In such case, Users will be charged for the cost of spare parts on actual as per this Agreement.</li>
<li><strong>Unauthorized activity in the Vehicle:</strong> Users are not allowed to carry arms ammunitions and banned drugs. In addition, use of Vehicle for commercial activity such as product sell and promotion, and carry goods is strictly prohibited. In such cases, Users will be charged a penalty of Rs. 5000.</li>
<li><strong>External branding:</strong> Any form of external branding on the Vehicle is prohibited. Users are not allowed to paste or paint any external brand promotion on the Vehicle. Users will have to pay a penalty of Rs. 5000.</li>
<li><strong>Tampering with devices:</strong> Users are not allowed to tamper with the odometer, GPS device and in car technology devices. Users will have to pay a penalty of Rs. 1000 and over in additional to the actual cost of the device.</li>
<li><strong>Deliberately driving the car in water:</strong> Users will be charged for the actual cost of repair and spare parts in terms of this Agreement.</li>
<li><strong>Producing fake and tampered personal documents:</strong> Users will be charged a penalty of Rs. 1000 if found guilty.</li>
<li><strong>Diversion:</strong> Users are not allowed to drive the car into unauthorized or government banned areas. Users are advised to inform the Company in case they change the course of their trip. All Myles Vehicles are geo-fenced and customers have to pay a penalty Rs. 10,000 if the Vehicle trespasses into banned areas, naxal hit areas, international border of republic of India and extreme end of ladakh.</li>
<li><strong>Tobacco:</strong> Users are not allowed to consume, chew or use any type of Tobacco, Cigarettes, Drugs or any type of Narcotics and Psychotropic Substances violating which User shall be liable to penalty of Rs. 1000/- (One Thousand Only)and further it shall entitle the Company to take back the Vehicle and terminate this Agreement.</li>
</ul>
</td>
</tr>
</table>

<table width="100%" border="1" cellpadding="5">
<p class="term_head">
<strong>ANNEXURE E</strong><br/>
<strong>LIST OF PENALTY/ DAMAGES
</p>
<tr>
<th width="10%" align="left" valign="top">SEGMENT</th>
<th align="left" valign="top">VARIANT Details</th>
</tr>
<tr>
<td align="center" valign="top">A</td>
<td align="center" valign="top">
MARUTI ALTO, MAHINDRA E2O,TATA NANO,FORD FIGO,HYUNDAI I10, HYUNDAI I 10 GRAND,MARUTI SWIFT,MARUTI RITZ
</td>
</tr>
<tr>
<td align="center" valign="top">B</td>
<td align="center" valign="top">
VOLKSWAGON VENTO,MARUTI ERTIGA,NISSAN SUNNY,HONDA CITY,HONDA AMAZE,VOLKSWAGON POLO, HYUNDAI I20,TOYOTA ETIOS,MARUTI CIAZ
</td>
</tr>
<tr>
<td align="center" valign="top">C</td>
<td align="center" valign="top">
FORD ECOSPORTS, RENAULT DUSTER, MAHINDRA SCORPIO,TOYOTA INNOVA,TATA SAFARI, HONDA ACCORD,COROLLA ALTIS
</td>
</tr>
<tr>
<td align="center" valign="top">D</td>
<td align="center" valign="top">
MAHINDRA XUV, FORD ENDEVOR, TOYOTA FORTUNER,TOYOTA CAMARY
</td>
</tr>
<tr>
<td align="center" valign="top">E</td>
<td align="center" valign="top">
MERCEDES BENZ C, E, S CLASS
</td>
</tr>
<tr>
<td align="center" valign="top"></td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<th align="center" width="30%" valign="middle">PANEL NAME</th>
<th align="center" valign="middle">A</th>
<th align="center" valign="middle">B</th>
<th align="center" valign="middle">C</th>
<th align="center" valign="middle">D</th>
<th align="center" valign="middle">E</th>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">1</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">FRONT BUMPER</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">4500</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">2</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">HOOD</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">3</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">FENDER</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">4</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">DOOR</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">5</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">QUARTER PANEL</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">6</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">BOOT LID</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">7</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">REAR BUMPER</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">2200</td>
<td align="center" valign="middle">2500</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">8</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">ROOF</td>
<td align="center" valign="middle">2000</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">3000</td>
<td align="center" valign="middle">3500</td>
<td align="center" valign="middle">5000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">9</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">REAR VIEW MIRROR</td>
<td align="center" valign="middle">500</td>
<td align="center" valign="middle">600</td>
<td align="center" valign="middle">1000</td>
<td align="center" valign="middle">1000</td>
<td align="center" valign="middle">1500</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">10</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">RUNNING BOARD</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">2000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">11</td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">A PILLAR</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">2000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top" colspan="2" height="20"></td>
</tr>
<tr>
<td align="center" valign="top" colspan="2">REMOVE AND FIT SCHEDULE</td>
</tr>
<tr>
<td align="center" valign="top"></td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">REAR VIEW MIRROR</td>
<td align="center" valign="middle">150</td>
<td align="center" valign="middle">200</td>
<td align="center" valign="middle">250</td>
<td align="center" valign="middle">300</td>
<td align="center" valign="middle">400</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top"></td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">FRONT WINDSHIELD</td>
<td align="center" valign="middle">1000</td>
<td align="center" valign="middle">1200</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1800</td>
<td align="center" valign="middle">2000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top"></td>
<td align="center" valign="top">
<table width="100%" border="1">
<tr>
<td align="center" width="30%" valign="middle">REAR WINDSHIELD</td>
<td align="center" valign="middle">1000</td>
<td align="center" valign="middle">1200</td>
<td align="center" valign="middle">1500</td>
<td align="center" valign="middle">1800</td>
<td align="center" valign="middle">2000</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top" colspan="2" height="20"></td>
</tr>
</tr>
</table>

</td>
</tr>
</table></div>
</div>
</div>
</div>
<div class="clr"> </div>';
//==============================================================
//==============================================================
//==============================================================


$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);
//echo $_SERVER["DOCUMENT_ROOT"];exit;
//$mpdf->Output($_SERVER["DOCUMENT_ROOT"].'/myles/webroot/Agreements/'.$res['coric'].'.pdf','F');
$mpdf->Output('/var/www/html/mylescars/webroot/Agreements1/'.$res['coric'].'.pdf','F');
}
}	
?>
<?php

//error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');
//include_once('Paynimo/gateway.php');

$Myle = new Myles();
$arr = array();


$db = new MySqlConnection(CONNSTRING);
$db->open();

//$book = $db->query("stored procedure", "myles_paid_and_failed()");
//payment_status = 4 => payment sucessful but booking id not found

$book_query = "select * from `myles_payment_logs` where final_status_update_time <= DATE_ADD(Now(), INTERVAL '325' MINUTE) 
AND final_status_update_time >= DATE_ADD(Now(), INTERVAL '-1108' MINUTE) AND final_status in('AUTHORIZATION_FAILED','PENDING_VBV') 
AND pg_source='JusPay' AND payment_source_id='1'";


$book = $db->query("query", $book_query);

if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        
        $juspaydata = array();
       
        $juspay = array();
       
		
		$juspay_url = 'https://api.juspay.in/';
        $juspay_key = 'DBB94372C82C46F789CF20C3EF050C6A';
		
		$payment_log = array();
        $juspay_query = "SELECT * FROM myles_payment_logs logs WHERE logs.coric= '" . $coric . "' AND logs.payment_source_id = '1' AND final_status in('AUTHORIZATION_FAILED','PENDING_VBV')";

        $rjuspay = $db->query("query", $juspay_query);
        
        if (array_key_exists(0, $rjuspay)) {
            $juspay = $rjuspay[0];
			if (count($juspay) > 0) {
			
				$curl = curl_init($juspay_url . 'order_status');
				curl_setopt($curl, CURLOPT_POSTFIELDS, array('orderId' => $juspay['order_id'], 'merchantId' => 'mylescars'));
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_USERPWD, $juspay_key);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
				$jsonRespon = json_decode(curl_exec($curl));
				if ($jsonRespon->{'status'} == 'CHARGED') {
					$where = array();
		            $where["id"] = $juspay['id'];
                    $payment_log['amount'] = $jsonRespon->{'amount'};
			        $payment_log['trans_id'] = $jsonRespon->{'txnId'};					
					$payment_log['status'] =  $jsonRespon->{'status'};
					$payment_log['final_status'] =  $jsonRespon->{'status'};
			        $payment_log['final_status_update_time'] =  date("Y-m-d H:i:s");
				    $up = $db->update("myles_payment_logs", $payment_log, $where);
					unset($payment_log);
					unset($where);	
				}
			}
        } 
        
    }
}
$db->close();

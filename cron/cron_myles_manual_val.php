<?php
   
    //error_reporting(E_ALL);
	
    ini_set("display_errors", 1);    
    date_default_timezone_set('Asia/Calcutta');

    include_once('classes/mylesdb.php');   
    include_once('classes/rest.php');
    //include_once('Paynimo/gateway.php');
    
    $Myle = new Myles();
    
    $arr = array();
  
    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    
    //$book = $db->query("stored procedure", "myles_bookings_failed()");
	$book_query = "SELECT * FROM `myles_bookings` WHERE coric='CORIC1486705888116'";
    $book = $db->query("query", $book_query);
  
 
	 
	if(!array_key_exists("response",$book))
    {
	           
        for($i = 0; $i < count($book); $i++){
			
			$coric = $book[$i]["coric"];
			
			
			$ModeDetails = $Myle->_MYLESGetMultipleModePaymentsStatus('GetMultipleModePaymentsStatus',  $coric);
			
			if ((strtolower($ModeDetails['Message']) == 'success') && (strtolower($ModeDetails['Rental']) == 'paid')) {
				
				$dataToSave = array();						
				$where = array();
				$where["coric"] = $coric;					
			  
				$dataToSave["payment_mode"] = "1";
				$dataToSave["payment_status"] = "1"; 		    
										
				$up = $db->update("myles_bookings", $dataToSave, $where);
						
				unset($dataToSave);
				unset($where);
					
				
			}
						
		
		}
    }
	   
    $db->close();
?>

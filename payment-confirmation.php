<?php
//	error_reporting(E_ALL);
//	ini_set("display_errors", 1);
	session_start();
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/cache-func.php");
	include_once('./classes/cor.mysql.class.php');
?>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="googlebot" content="noindex, nofollow">
<meta name="googlebot" content="noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payment Confirmation - Carzonrent Pvt. Ltd</title>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
</head>
<body>
<?php
	if($_SESSION["hdTourtype"] == "Outstation" || $_SESSION["hdTourtype"] == "Local"){
?>
	<style>
		.showtime, .showhour {
			background: none repeat scroll 0 0 #FAEAA3 !important;
			border: 1px solid #EFC14D !important;
			border-radius: 4px 4px 4px 4px;
			color: #D68300 !important;
		}
		#registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.jpg") no-repeat scroll 0 0 transparent !important;}
	</style>
<?php
	} elseif(trim($_SESSION["hdTourtype"]) == "Selfdrive"){
?>
	<style>
		.tripdetails h3 {color: #db4626 !important;}
		.tripdetails ul li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll left 8px rgba(0, 0, 0, 0) !important;}
		#registration{border: 10px solid #db4626 !important;}
	</style>
<?php
	}
?>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<div class="tbbingouter yellowstrip">
  <div class="main">
    <ul class="myaccount_tab">
    	<li>You payment has been received.</li>
    </ul>
  </div>
</div>
<div class="clr"></div>
<!--Middle Start Here-->
<div class="main">

<div class="myprofile">
	<div class="heading">Payment Confirmation</div>
	<div class="tripdetails" style="width: 100% !important; padding: 30px 0px;text-align: center;">  
		Your payment has been successfully received by us.
		<br /><img src='<?php echo corWebRoot; ?>/images/new-loader.gif' width="179px" height="95px" border='0' style="padding: 10px 0px;" />
		<br />Now we are trying to book your car. Please do not CLOSE or REFRESH this window.
	</div>
	<input type="hidden" name="hdCORIC" id="hdCORIC" value="<?php echo $_POST["TxId"]; ?>" />
<div class="clr"></div>
</div>
</div>
<!--footer Start Here-->
<script type="text/javascript">
	_createBooking();
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

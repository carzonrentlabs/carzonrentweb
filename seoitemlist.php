<?php
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');

include_once('./classes/cor.mysql.class.php');

$cor = new COR();
$res = $cor->_CORGetCities();
$myXML = new CORXMLList();
$org = $myXML->xml2ary($res);

////////////////// get model of particular city //////////////////////

if (isset($_POST['sub']) && !empty($_POST['sub']) && $_POST['sub'] != '') {
    $arg['CityId'] = $_POST['city'];
    $arg['ClientCoID'] = 2205;
    $res = $cor->_CORGetModels($arg);

    $myXML = new CORXMLList();
    $models = $myXML->xml2ary($res);
    $option = '<option value="0">Select Model</option>';
    if (count($models) > 0) {

        foreach ($models as $key => $val) {
            $option .= '<option value="' . $val['CarModelID'] . '">' . $val['CarModel'] . '</option>';
        }
    }
    echo $option;
    die;
}


if (isset($_POST['search']) && !empty($_POST['search'])) {
    $db = new MySqlConnection(CONNSTRING);
    $db->open();


    $sql = "select * from cor_seoitem where city = " . $_POST['city'] . " and model = " . $_POST['model'];
    $data = '';
    $r = $db->query("query", $sql);
    if ((count($r) > 0) && (!array_key_exists("response",$r)) ){
        $data = '<table class="seoitem" border="0" cellpadding="0" cellspacing="0"><tr>'
                . '<td>itemid</td>'
                . '<td>itemid2</td>'
                . '<td>pagetype</td>'
                . '<td>totalvalue</td>'
                . '<td>&nbsp;</td>'
                . '</tr>';
        foreach ($r as $key => $val) {

            $data .= "<tr id='" . $val['id'] . "'>"
                    . "<td> <p class='dblclick' id='itemid_" . $val['id'] . "'>" . $val['itemid'] . "</p></td>"
                    . "<td> <p  class='dblclick' id='itemid2_" . $val['id'] . "'>" . $val['itemid2'] . "</p></td>"
                    . "<td> <p class='dblclick' id='pagetype_" . $val['id'] . "'>" . $val['pagetype'] . "</p></td>"
                    . "<td> <p class='dblclick' id='totalvalue_" . $val['id'] . "'>" . $val['totalvalue'] . "</p></td>"
                    . "<td> <a href='javascript:void(0);' onclick='deleteModelData(" . $val['id'] . ")'>Delete</a></td>"
                    . "</tr>";
        }
         $data .="</table>";
    }
    else
    {
        $data .= "Data not available";
    }
   
    echo $data;
    $db->close();
    die;
}


if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $dataToSave = array();
    $idarray = explode("_", $_REQUEST['id']);
    $dataToSave[$idarray[0]] = $_REQUEST["value"];
    $whereData = array();
    $whereData['id'] = $idarray[1];
    $r = $db->update("cor_seoitem", $dataToSave, $whereData);

    $db->close();
    echo $_REQUEST["value"];
    die;
}
if(isset($_REQUEST['del']) && ($_REQUEST['del'] != '') )
{
     $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $ar['id'] = $_REQUEST['delid'];
    $r = $db->delete("cor_seoitem",$ar);
    echo '<pre>';
    print_r($r);
}
?>

<html>

    <head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/seoitem.css" type="text/css" />
		<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/jquery.jeditable.js"></script>

        <script type="text/javascript">
            function getModel() {
                values = "city=" + $("#city").val() + "&sub=true";
                $.ajax({
                    url: "seoitemlist.php",
                    type: "post",
                    data: values,
                    success: function (data) {
                        $("#model").html(data);

                    },
                    error: function () {
                        alert("failure");

                    }
                });


                return false;
            }
            function deleteModelData(delid)
            {
                if (confirm("Are you sure you want to delete this?")) {
                    values = "delid=" + delid + "&del=true";
                    $.ajax({
                        url: "seoitemlist.php",
                        type: "post",
                        data: values,
                        success: function (data) {
                            

                        },
                        error: function () {
                            alert("failure");

                        }
                    });

                }
            }
            function searchData()
            {
                if ($("#city").val() == '0')
                {
                    alert("Please select city");
                }
                else if ($("#model").val() == '0')
                {
                    alert("Please select car");
                }

                else
                {
                    var values = "city=" + $("#city").val() + "&model=" + $('#model').val() + "&search=true";

                    $.ajax({
                        url: "seoitemlist.php",
                        type: "post",
                        data: values,
                        success: function (data) {
                            $("#searchdata").html(data);
                            $(".dblclick").editable("<?php echo corWebRoot; ?>/seoitemlist.php", {
                                indicator: "<img src='img/indicator.gif'>",
                                type: "textarea",
                                submit: "OK",
                                cancel: "Cancel"

                            });
                        },
                        error: function () {
                            alert("failure");

                        }
                    });
					
					$("#searchdata").show();



                }
                return false;
            }





        </script>

        <style type="text/css">
            #sidebar {
                width: 0px;
            }

            #content {
                width: 770px;
            }

            .editable input[type=submit] {
                color: #F00;
                font-weight: bold;
            }
            .editable input[type=button] {
                color: #0F0;
                font-weight: bold;
            }
            textarea{
                width:150px !important;
                min-height:100px !important;
                resize:none;
            }
        </style>
    </head>
    <body>

        <table class="seoitem" class="seoitem" width="500" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>City</td>
                <td>
                    <select name="city" id="city" onchange="getModel()">
                        <option value="0">Select City</option>
                        <?php
                        if (count($org) > 0) {

                            foreach ($org as $key => $val) {
                                ?>
                                <option value="<?php echo $val['CityID']; ?>"><?php echo $val['CityName']; ?></option>
                                <?php
                            }
                        }
                        ?>

                    </select>
                </td>
            </tr>
            <tr>
                <td>Model</td>
                <td>
                    <select name="model" id="model">
                        <option value="0">Select Model</option>
                    </select>

                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td><a href="javascript:void(0)" class="seo" onclick="searchData();">Search</a></td>
            </tr>
        </table>
        <div id="searchdata"></div>

    </body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COR</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="js/customSelect.jquery.js"></script>
 <script src="js/jquery.min.js" type="text/javascript" id="jquery"></script>
   <script language="javascript" type="text/javascript" src="js/jquery.js"></script>
   <script type="text/javascript">
	$(document).ready(function(){
	$("#box1").hide();
	$("#icici").click(function(){
		$('#box1').slideUp(1000);
		$(this).next().slideDown(1000);
		$("#icici").hide();
		$('#IndusInd').show();
		$('#Indcard').show();
		$('#card').show();
		$('#box2').hide();
		$('#box3').hide();
		$('#box4').hide()
    });
	$("#box1").click(function(){
	 if($(this).next().is(':visible')) {
	  $('#box1').slideUp(1000);
	  $("#icici").show();
	  $('#box2').hide();
	   $('#box3').hide();
	    $('#box4').hide()
	 	}
    });
	$("#box2").hide();
	$("#card").click(function(){
		$('#box2').slideUp(1000);
		$(this).next().slideDown(1000);
		$("#card").hide();
		$('#icici').show();
		$('#IndusInd').show();
		$('#Indcard').show();
		$('#box1').hide();
		$('#box3').hide();
		$('#box4').hide()
    });
	$("#box2").click(function(){
	 if($(this).next().is(':visible')) {
	  $('#box2').slideUp(1000);
	  $("#card").show();
	  $('#box1').hide();
	  $('#box3').hide();
	  $('#box4').hide()
	 	}
    });
	$("#box3").hide();
	$("#IndusInd").click(function(){
		$('#box3').slideUp(1000);
		$(this).next().slideDown(1000);
		$("#IndusInd").hide();
		$('#Indcard').show();
		$('#card').show();
		$('#icici').show();
		$('#box2').hide();
		$('#box1').hide();
		$('#box4').hide()
    });
	$("#box3").click(function(){
	 if($(this).next().is(':visible')) {
	  $('#box3').slideUp(1000);
	  $("#IndusInd").show();
	  $('#box2').hide();
	  $('#box1').hide();
	  $('#box4').hide()
	 	}
    });
	$("#box4").hide();
	$("#Indcard").click(function(){
		$('#box4').slideUp(1000);
		$(this).next().slideDown(1000);
		$("#Indcard").hide();
		$('#card').show();
		$('#icici').show();
		$('#IndusInd').show();
		$('#box2').hide();
		$('#box3').hide();
		$('#box1').hide()
    });
	$("#box4").click(function(){
	 if($(this).next().is(':visible')) {
	  $('#box4').slideUp(1000);
	  $("#Indcard").show();
	  $('#box2').hide();
	  $('#box3').hide();
	  $('#box1').hide()
	 	}
    });
});
</script>
<script>
$(function(){
	$('select.time').customSelect();
	$('select.area').customSelect();
	$('select.styled').customSelect();
})
function resgiter_cont(){
	document.getElementById("register").style.display = 'none';  
	document.getElementById("travel_details").style.display = 'block';  
}
</script>
</head>
<body>
<!--Header Start Here-->
<div id="header">
	<div class="main">
    	<div class="logo"><a href="./" id="brand" class="easybrand"></a></div>
        <div class="logo-right">
        	<div class="logo-righttop">
            	<ul class="lf">
                	<li><a href="#">COR-Car rentals</a></li>
                    <li><a href="#" class="active">COR-Easycabs</a></li>
                    <li><a href="#">COR-leasing</a></li>
                </ul>
                <ul class="rt">
                	<li><a href="#">Corporate User</a></li>
                    <li><a href="#">Login</a></li>
                    <li class="no"><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="menu">
            	<ul>
                	<li><a href="#" class="active">Delhi-NCR </a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Bangalore</a></li>
                    <li><a href="#">Hyderabad</a></li>
                    <li class="more"><a href="#">More</a>
                    	<ul class="submenu">
                        	<li><a href="#">Mumbai</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                        </ul>
                    </li>
                    <li class="tfn">Call 24 hours (prefix city code)<strong>0888 222 2222</strong> </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_right"></div>
</div>
<!--Banner Start Here-->
<div class="tbbingouter">
  <div class="main">
    <ul id="countrytabs" class="shadetabst">
      <li class="step1"><a href="#" rel="country1">Step 1</a></li>
      <li class="step2"><a href="#" rel="country2">Step 2</a></li>
      <li class="step3"><a href="#" rel="country3" class="selected">Step 3</a></li>
    </ul>
  </div>
</div>
<!--Middle Start Here-->

<div class="tbbinginr">
  <div id="country2" class="tabcontent">
   <form action="" method="">
    <div class="main">
      <div class="innerpages">
        <div class="leftside">
        <h1>Payment</h1>
        <form method="post" action="">
          <fieldset class="payment">
            <div class="leftfieldset">
              <input type="radio" />
              <label>Pay Online</label>
              <ul class="listarrow">
                <li>Using visa, AMEX credit and debit cards</li>
                <li>Using net banking facility</li>
                <li>Using cash cards</li>
              </ul>
            </div>
            <div class="offers">
                <ul>
					<span class="exciting">Exciting Offers</span>
                    <li id="icici"> <span><b>ICICI Bank Buy 1 Get 1 Offer</b> - On Select Credit Cards</span></li>
                    <div class="offerBox" id="box1">
                            <p class="offerBoxtxt"><img src="images/minus.gif" alt="" style="margin-right:5px;"/><b>ICICI Bank Buy 1 Get 1 Offer</b> -  On Select Credit Cards</p>
                            <div class="search_box">
                              <input type="text" onfocus="if(this.value=='enter your coupon code to redeem'){this.value='';}" onblur="if(this.value==''){this.value='enter your coupon code to redeem';}" class="input" value="enter your coupon code to redeem" name="Search">                      <input type="submit" class="search_btn" value="">
                          </div>
                            <div class="get">Get 1 <br />
                            <span class="txtorange">FREE</span> 
                            </div>
                       </div>   
                        
                    <li id="card"> <span><b>ICICI Bank Rs. 10 Off Offer</b> - On Select Credit Cards</span></li>
                     <div class="offerBox" id="box2">
                            <p class="offerBoxtxt"><img src="images/minus.gif" alt="" style="margin-right:5px;"/><b>ICICI Bank Rs. 10 Off Offer</b> -  On Select Credit Cards</p>
                            <div class="search_box"><input type="text" onfocus="if(this.value=='enter your coupon code to redeem'){this.value='';}" onblur="if(this.value==''){this.value='enter your coupon code to redeem';}" class="input" value="enter your coupon code to redeem" name="Search">        <input type="submit" class="search_btn" value="">                          </div>
                            <div class="get">Get 1 <br />
                            <span class="txtorange">FREE</span> 
                            </div>
                       </div>   
                    <li id="IndusInd"> <span><b>IndusInd Bank Credit Card Offer</b> - Buy 1 Get 1 Free</span></li>
                    <div class="offerBox" id="box3">
                            <p class="offerBoxtxt"><img src="images/minus.gif" alt="" style="margin-right:5px;"/><b>IndusInd Bank Credit Card Offer</b> - Buy 1 Get 1 Free</p>
                            <div class="search_box">
                              <input type="text" onfocus="if(this.value=='enter your coupon code to redeem'){this.value='';}" onblur="if(this.value==''){this.value='enter your coupon code to redeem';}" class="input" value="enter your coupon code to redeem" name="Search">                      <input type="submit" class="search_btn" value="">
                          </div>
                            <div class="get">Get 1 <br />
                            <span class="txtorange">FREE</span> 
                            </div>
                        </div>   
                    <li id="Indcard" > <span><b>IndusInd Bank Credit Card Offer</b> - Buy 1 Get 1 Free</span></li>
					<div class="offerBox" id="box4">
                            <p class="offerBoxtxt"><img src="images/minus.gif" alt="" style="margin-right:5px;"/><b>IndusInd Bank Credit Card Offer</b> - Buy 1 Get 1 Free</p>
                            <div class="search_box">
                              <input type="text" onfocus="if(this.value=='enter your coupon code to redeem'){this.value='';}" onblur="if(this.value==''){this.value='enter your coupon code to redeem';}" class="input" value="enter your coupon code to redeem" name="Search">     <input type="submit" class="search_btn" value="">
                          </div>
                            <div class="get">Get 1 <br />
                            <span class="txtorange">FREE</span> 
                            </div>
                        </div>   
                </ul>
				 <div class="confirmbooking ml_cnf"><a href="#"></a></div>
            </div>
          </fieldset>
          <fieldset class="payment">
            <div class="leftfieldset">
              <input type="radio" />
              <label>Pay advance online</label>
              <ul class="listarrow">
                <li>Pending amount needs to be paid in cash to the driver at the end of journey</li>
              </ul>
              <div class="confirmbooking ml_cnf"><a href="#"></a></div>
            </div>
          </fieldset>
          <fieldset class="payment" style="border:none">
            <div class="leftfieldset">
              <input type="radio" />
              <label>Pay cash to the driver at the start of journey</label>
              <ul class="listarrow">
                <li>You shall receive a confirmation call 2-4 hours prior to start of journey</li>
                <li>Cab will be dispatched to pick up location after receiving confirmation on call</li>
              </ul>
              <div class="confirmbooking ml_cnf"><a href="#"></a></div>
              
              <div class="middiv" style="padding-left:0px;"> <strong>NOTE</strong>
              <ul>
                <li>Final amount will be as per actual</li>
                <li>Pending amount after advance adjustment is to be paid to driver as cash at the end of journey</li>
                <li>0% cancellation charges on amount paid as advance</li>
              </ul>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    <div class="rightside">
      <h1>Booking Summary</h1>
      <div class="tpdiv"> <img src="images/car1.jpg" alt="" />
        <div class="clr"></div>
        <p><span>Car Type:</span> Economy - Indica/WagonR</p>
        <p><span>From:</span> Mumbai</p>
        <p><span>Service:</span> Outstation </p>
        <p><span>Destination:</span> Agra, Delhi, Pune</p>
        <p><span>Pickup Date:</span> 17th Apr '12</p>
        <p><span>Pickup Time:</span> Not Set</p>
        <h3>Payable Amount<br />
          <span><img src="images/re1.jpg" alt="" align="absmiddle" /> 3999/-</span></h3>
      </div>
      <div class="tpdiv">
        <p><span>Fare Details</span></p>
        <ul>
          <li>Rate: <img src="images/re2.jpg" alt="" align="absmiddle" /> 1000 / day + <img src="images/re2.jpg" alt="" align="absmiddle" /> 6 / km</li>
          <li>Daily rental charge = <img src="images/re2.jpg" alt="" align="absmiddle" /> 1000x2 </li>
          <li>Approx round trip distance charge = <img src="images/re2.jpg" alt="" align="absmiddle" /> 6 x450</li>
          <li>Toll and parking extra</li>
          <li>Chauffeur charges, state permit and
            service tax included</li>
        </ul>
      </div>
      <div class="tpdiv">
        <p>Final fare shall depend on actual kms traveled. </p>
        <div class="map"><img src="images/map1.jpg" alt="" /></div>
      </div>
    </div>
  </div>
    </div>
    </form>
</div>
</div>
<!--footer Start Here-->
<div id="footer">
	<div class="main">
    	
        <div class="fbtm">
        	<div class="cm">
            	<h3>Company</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/aboutus.php" target="_blank">About</a></li>
			<li><a href="http://www.carzonrent.com/services.php" target="_blank">Services</a></li>
			<li><a href="http://www.carzonrent.com/careers.php" target="_blank">Career</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Book a Cab</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/business-enquiries.php" target="_blank">Outstation</a></li>
			<li><a href="http://www.carzonrent.com/corporate-rental.php" target="_blank">Local</a></li>
			<li><a href="http://www.easycabs.com/" target="_blank">Easycabs</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Easy Cabs</h3>
                <ul>
                	<li><a href="http://www.easycabs.com/advertisewithus.php" target="_blank">Advertise with Us</a></li>
                        <li><a href="http://www.easycabs.com/fleet.php" target="_blank">Fleet</a></li>
			<li><a href="http://www.easycabs.com/tariff.php" target="_blank">Tariff</a></li>
                </ul>
            </div>
	    <div class="qu">
            	<h3>Corporate Leasing</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/corlease/comparative-analysis.php" target="_blank">Why Leasing</a></li>
			<li><a href="http://www.carzonrent.com/corlease/lease-benefits.php" target="_blank">Benefits</a></li>
			<li><a href="http://www.carzonrent.com/corlease/product-and-services.php" target="_blank">Products & Services</a></li>
                </ul>
            </div>
            <div class="qu">
            	<h3>Quick Links</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/my-account.php" target="_blank">My Account</a></li>
			<li><a href="http://www.carzonrent.com/print-booking.php" target="_blank">Print Booking</a></li>
			<li><a href="http://www.carzonrent.com/vehicle-guide.php">Vehicle Guide</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
	    
            <div class="socil">
            	<h3>Social</h3>
                <div class="soccd">
			<table cellpadding="5" cellspacing="0" border="0">
                        <tr>
                            <td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1" type="text/javascript"></script><fb:like href="http://www.facebook.com/carzonrent/" send="false" layout="button_count" width="90" show_faces="false"></fb:like></td>
                            <td><a href="https://twitter.com/carzonrentIN" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @carzonrentIN</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
                            <td style="padding-left:15px;"><!-- Place this tag where you want the +1 button to render -->
<g:plusone size="medium" href="http://www.carzonrent.com"></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></td>
                        </tr>
                    </table>
		</div>
                <!--<h3>Download Mobile App</h3>
                <p><img src="images/apps.png" alt="" /></p>-->
            </div>
        </div>
        <div class="copurit">Copyright &copy; Carzonrent. All Rights Reserved. Terms of Use.</div>
    </div>
</div>
</body>
</html>

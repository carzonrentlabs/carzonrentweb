<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome to COR next</title>

    <!-- Bootstrap -->
    <link href="css/cornext.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript">
		function addQuote()
		{
			$("#message").removeClass('error');
			$("#message").removeClass('success');
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			
			var name = $.trim($("#name").val());
			var org = $.trim($("#org").val());
			var email = $.trim($("#email").val());
			var mobile = $.trim($("#mobile").val());
			
			if(name == '')
			{
				$('#message').html('Please Enter Name');
				$("#message").addClass('error');
				$("#name").focus();
			}
			else if(org == '')
			{
				$('#message').html('Please Enter Organization');
				$("#message").addClass('error');
				$("#org").focus();
			}
			else if(email == '')
			{
				$('#message').html('Please Enter Email');
				$("#message").addClass('error');
				$("#email").focus();
			}
			else if (!filter.test(email))
			{
				$('#message').html('Please Enter Valid Email');
				$("#message").addClass('error');
				$("#email").focus();
			}	
			else if(mobile == '')
			{
				$('#message').html('Please Enter Mobile No');
				$("#message").addClass('error');
				$("#mobile").focus();
			}
			else
			{
				var param = "name="+name+"&org="+org+"&email="+email+'&mobile='+mobile;

				$.ajax({
					url: "cornextajax.php",
					type: "POST",
					data: param,
					beforeSend: function () {
						//$("#loaderImg").show();
					},
					success: function (data) {
						
						//$("#loaderImg").hide();
						$("#message").html('Thank you! We will be in touch.');
						$("#message").addClass('success');
						clearemployeeform();
					},
					error: function () {
						//alert("failure");
					}
				});
			}
			
		}
		function clearemployeeform()
		{
			$("#name").val('');
			$("#org").val('');
			$("#email").val('');
			$("#mobile").val('');
			
			
		}
		function cleanmessage()
		{
			$("#message").html('');
		}
		
	</script>
  </head>
  <body>
    <div class="cor_wrapper">
      <div class="cor_header navbar-fixed-top">
        <div class="container">
          <div class="cor-4">
            <a href="#" class="cor_logo"><img src="img/logo.png" alt="logo"></a>
          </div><!--cor-4-->
          <div class="cor-8">
            <a href="javascript:void(0);" class="mobile_click" onclick="mobile_click();">
              <span></span>
              <span></span>
              <span></span>
            </a>
            <ul class="cor_menu">
              <li><a class="active" href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#features">Features</a></li>
              <li><a class="btnn" href="#requestdemo">Request Demo</a></li>
              <li><img class="mobiicon" src="img/mobileicon.png" alt=""> 011-41841212</li>
            </ul>
          </div><!--cor-8-->
          <div class="clr"></div><!--clr-->
        </div><!--container-->
      </div><!--cor_header-->
      <div class="cor_banner" id="requestdemo">
        <div class="container"> 
        <div class="cor-8">
          <div class="banner_content"> 
            <span class="presents">Carzonrent presents</span>
            <h1>COR next <br>The best gets better!</h1>
            <p class="smallText">A superior travel experience enabled by technology with premium cars, quality chauffeurs and the finest service</p>
            <a href="#introducingcor" class="knowmorebtn">Know More</a>
          </div><!--banner_content-->
        </div><!--cor-7-->
        <div class="cor-4">
          <div class="cor_form">
            <span class="formHdn">Request a demo</span>
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name" id="name" onclick="cleanmessage()"/>
              </div>
              <div class="form-group">
                <label>Organization</label>
                <input class="form-control" type="text" name="org" id="org" onclick="cleanmessage()" />
              </div>
              <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="email" id="email" onclick="cleanmessage()" />
              </div>
              <div class="form-group">
                <label>Contact Number</label>
                <input class="form-control" type="text" name="mobile" id="mobile" onclick="cleanmessage()" />
              </div>
			  <div class="error" id="message"></div>
              <div class="btnBox">
                <button type="button" class="corBtn btn" onclick="addQuote()">Request Demo</button>
              </div>
          </div><!--cor_form-->
        </div><!--cor-4-->
        <div class="clr"></div><!--clr-->
        </div><!--container-->
      </div><!--cor_banner-->
		<div id="about"></div>
        <div class="cor_box introducingCor" id="introducingcor">
          <div class="container">
            <span class="iconbox"><img src="img/carIcon.png" alt=""></span>
            <h2>Intoducing <span class="corb"> Cor<span class="corbb"> next</span></span></h2>
            <p>A seamless blend of technology and service, COR next
is re-defining corporate travel. Designed for the discerning traveler,
COR next promises a superior car rental service using
technology to deliver impeccable service </p> 
          </div><!--container-->
        </div><!--cor_box introducingCor-->
        <div class="cor_box features" id="features">
          <div class="container">
            <span class="iconbox"><img src="img/featureIcon.png" alt=""></span>
            <h2>OUR  <span class="corb"> FEATURES</span></h2>
            <div class="featuresIn">
              <div class="cor-3">
                <div class="box">
                  <span class="iconbox"><img src="img/ficon01.png" alt=""></span>
                  <span class="textBold">Intuitive reservation<span class="br"> process</span></span>
                  <ul>
                    <li>Multi-channel booking</li>
                    <li>Mobile app & web booking</li>
                    <li>Administrative dashboards</li>
                    <li>Live tracking</li>
                    <li>Transparent invoicing</li>
                  </ul>
                </div><!--box-->
              </div><!--cor-3-->
              <div class="cor-3">
                <div class="box">
                  <span class="iconbox"><img src="img/ficon02.png" alt=""></span>
                  <span class="textBold">Premium <span class="br">cars</span></span>
                  <ul>
                    <li>The widest range of premium
  and luxury vehicles for a
  comfortable ride</li>
                    <li>GPS enabled cars</li>
                    <li>The youngest fleet in town</li>
                  </ul>
                </div><!--box-->
              </div><!--cor-3-->
              <div class="cor-3">
                <div class="box">
                  <span class="iconbox"><img src="img/ficon03.png" alt=""></span>
                  <span class="textBold">Experienced <span class="br">chauffeurs</span></span>
                  <ul>
                    <li>Verified drivers with more
  than 5 years of experience</li>
                    <li>Qualified in Road Safety
  Management from Hubert
  Ebner (India) Pvt. Ltd.</li>
                    <li>Excellent communication &
  inter-personal skills</li>
                  </ul>
                </div><!--box-->
              </div><!--cor-3-->
              <div class="cor-3">
                <div class="box">
                  <span class="iconbox"><img src="img/ficon04.png" alt=""></span>
                  <span class="textBold">Seamless service &<span class="br"> amenities</span></span>
                  <ul>
                    <li>Wi-Fi enabled cars</li>
                    <li>Refreshment kits</li>
                    <li>Emergency assistance</li>
                    <li>24/7 helpline</li>
                    <li>Roadside assistance</li>
                  </ul>
                </div><!--box-->
              </div><!--cor-3-->


              <div class="clr"></div><!--clr-->
            </div><!--featuresIn-->
          </div><!--container-->
        </div><!--cor_box introducingCor-->
        <div class="cor_box more_details">
          <div class="container">
            <span class="for_more">For more details, please get in touch with us</span>
            <a href="#requestdemo" class="btn corBtn2">Request Demo</a>
          </div><!--container-->
        </div><!--cor_box introducingCor-->
        <div class="cor_footer">
          <ul class="sociallist">
            <li class="gmail"><a href="mailto:cornext@carzonrent.com"><span>Gmail</span></a></li>
            <li class="facebook"><a href="https://www.facebook.com/carzonrent/" target="_blank"><span>Facebook</span></a></li>
            <li class="linkedin"><a href="https://in.linkedin.com/company/carzonrent-india-pvt-ltd" target="_blank"><span>Linkedin</span></a></li>
          </ul>
          <span class="copyright">Copyright ©2018 Carzonrent India Pvt Ltd. All Rights Reserved</span>
        </div><!--cor_footer-->
    </div><!--cor_wrapper-->



     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript" src="js/cornextcustom.js"></script>
	<script type="text/javascript">
	 $(document).ready(function(){
       $('.cor_header .cor_menu li').click(function(){
         $('.cor_header .cor_menu li').removeClass("active");
         $(this).addClass("active");
     });
     });
		const element = document.getElementById('mobile');
const keys = ' 0123456789';
element.addEventListener('keypress', (e) => {
	
	if(e.key == 'Backspace' || e.key == 'Delete' || e.key == 'Home' || e.key == 'End' || e.key == 'Tab')
	{
	}
	
  else if (keys.includes(e.key) === false) {
    e.stopPropagation();
    e.preventDefault();
  }
}, false);

    
    
   </script>
   
   <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQCJKN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-WQCJKN');</script>

  </body>
</html>
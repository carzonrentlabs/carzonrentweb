<?php
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');

include_once('./classes/cor.mysql.class.php');

$cor = new COR();
$res = $cor->_CORGetCities();
$myXML = new CORXMLList();
$org = $myXML->xml2ary($res);

////////////////// get model of particular city //////////////////////

if (isset($_POST['sub']) && !empty($_POST['sub']) && $_POST['sub'] != '') {
    $arg['CityId'] = $_POST['city'];
    $arg['ClientCoID'] = 2205;
    $res = $cor->_CORGetModels($arg);

    $myXML = new CORXMLList();
    $models = $myXML->xml2ary($res);
    if (count($models) > 0) {
        $option = '<option value="0">Select Model</option>';
        foreach ($models as $key => $val) {
            $option .= '<option value="' . $val['CarModelID'] . '">' . $val['CarModel'] . '</option>';
        }
    }
 else {
     $option = '<option value="0">Select Model</option>';    
    }
    echo $option;
    die;
}

if (isset($_POST['save']) && !empty($_POST['save']) && $_POST['save'] != '') {
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $dataToSave = array();
    $dataToSave["city"] = $_POST['city'];
    $dataToSave["model"] = $_POST['model'];
    $dataToSave["itemid"] = trim($_POST['itemid1']);
    $dataToSave["itemid2"] = trim($_POST['itemid2']);
    $dataToSave["pagetype"] = trim($_POST['pagetype']);
    $dataToSave["totalvalue"] = trim($_POST['totalvalue']);
    $dataToSave['curdate'] = strtotime(date('Y-m-d'));
    
    $r = $db->insert("cor_seoitem", $dataToSave);
    echo $r['response'];
    die;
}
////////////////////
?>

<html>

    <head>
        <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
		<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/seoitem.css" type="text/css" />
        <script type="text/javascript">
            function getModel() {
                values = "city=" + $("#city").val() + "&sub=true";
                $.ajax({
                    url: "seoitemadd.php",
                    type: "post",
                    data: values,
                    success: function (data) {
                        $("#model").html(data);

                    },
                    error: function () {
                        alert("failure");

                    }
                });


                return false;
            }
            function saveData()
            {
                if ($("#city").val() == '0')
                {
                    alert("Please select city");
                }
                else if ($("#model").val() == '0')
                {
                    alert("Please select car");
                }
                else if ($("#itemid1").val() == '')
                {
                    alert("Please enter itemid");
                }
                else if ($("#itemid2").val() == '')
                {
                    alert("Please enter itemid2");
                }
                else if ($("#pagetype").val() == '')
                {
                       alert("Please enter page type");
                }
                else if ($("#totalvalue").val() == '')
                {
                    alert("Please enter total value");
                }
                else
                {
                    var values = "city=" + $("#city").val() +"&model=" + $('#model').val() +"&itemid1=" + $("#itemid1").val() +
                            "&itemid2=" + $("#itemid2").val() +"&pagetype=" + $("#pagetype").val() +"&totalvalue=" + $("#totalvalue").val()+"&save=true";
                   
                    $.ajax({
                        url: "seoitemadd.php",
                        type: "post",
                        data: values,
                        success: function (data) {
                            if(data == 'SUCCESS')
                            {
                                alert("Data saved successfully.");
                                $("#itemid1").val('');
                                $("#itemid2").val('');
                                $("#pagetype").val('');
                                $("#totalvalue").val('');
                            }
                           
                           else
                           {
                                alert("Error.");
                           }

                        },
                        error: function () {
                            alert("failure");

                        }
                    });


                   
                }
                 return false;
            }
        </script>
    </head>
    <body>
        <form method="">
            <table class="seoitem" width="500" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>City</td>
                    <td>
                        <select name="city" id="city" onchange="getModel()">
                            <option value="0">Select City</option>
<?php
if (count($org) > 0) {
    foreach ($org as $key => $val) {
        ?>
                                    <option value="<?php echo $val['CityID']; ?>"><?php echo $val['CityName']; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Model</td>
                    <td>
                        <select name="model" id="model">
                            <option value="0">Select Model</option>
                        </select>

                    </td>
                </tr>
                <tr>
                    <td>dynx_itemid</td>
                    <td><input type="text" name="itemid1" id="itemid1"/></td>
                </tr>
                <tr>
                    <td>dynx_itemid2</td>
                    <td><input type="text" name="itemid2" id="itemid2"/></td>
                </tr>
                <tr>
                    <td>Page Type</td>
                    <td><input type="text" name="pagetype" id="pagetype"/></td>
                </tr>
                <tr>
                    <td>Total Value</td>
                    <td><input type="text" name="totalvalue" id="totalvalue"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><a href="javascript:void(0)" class="seo" onclick="saveData();">Save</a></td>
                </tr>
            </table>
        </form>
    </body>
</html>


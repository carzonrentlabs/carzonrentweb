<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('../classes/cor.ws.class.php');
	include_once('../classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrent | Myles</title>

<?php include_once("../includes/header-css.php"); ?>
<?php include_once("../includes/header-js.php"); ?>
<link href="mystyle/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
    <script type="text/javascript" src="javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="javascripts/jquery.tooltip.js"></script>
    <script type="text/javascript">
      $j = jQuery.noConflict();
      $j(document).ready(function(){
        $j("div.item").tooltip();
      });
    </script>
   
</head>

<body>
<?php include_once("./includes/header.php"); ?>
<div id="main-container">
	<div class="inner-container">
    	<div class="myles-logo"><img src="images/myles.png" /></div>
    </div>
    <div class="banner">
    	 <div id="slider" class="nivoSlider" style=" float:left !important; margin:19px !important;">
                <img src="./images/10-to-go.jpg" alt="" title="" />
                <img src="./images/10-to-go.jpg" alt="" title="" />
				<img src="./images/10-to-go.jpg" alt="" title="" />
				<img src="./images/10-to-go.jpg" alt="" title="" />
            </div>
    </div>
    
	<div class="car-container">
			<div class="car-heading">
            	<div class="text">MYLES CARS<br /><span>Browse through our range of cars.</span></div>
                <div class="know-more">KNOW MORE</div>
            </div>
            <div class="show-car">
            	<div class="car-img"><img src="images/car-img.png" /></div>
                <div style="float:left; margin:53px 0 0 65px;">
                	<span style="color:#df4627; font-size:25px; font-weight:normal;">TOYOTA ETIOS</span><span style="font-size:21px; color:#414042;"> `1499/DAY <span style="font-size:14px; font-weight:bold; margin:8px 0 0 13px; float:right;"><div id="item_1" class="item">
        Details +
        <div class="tooltip_description" style="display:none" title="Item 1 Description">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </div>
      </div></span></span><br /><span style="font-size:10px; color:#414042;">A luxuriously comfortable ride where ever the road takes you.</span>
               </div>
               <div style="float:left; width:380px; margin: 9px 0 0 66px;">
               		<div style="font-weight:bold; font-size:14px; color:#414042; width:80px; float:left;">Available in</div>
                    <div style="background-color:#FFF; font-size:11px; color:#414042; float:left; margin:-1px 0 0 8px; padding:4px;">DELHI  |  MUMBAI  |  BANGALORE  | HYDERABAD</div>
                    
               </div>
                 
                <div  class="clearfix"></div>
                
            <div style="margin:22px 0 10px 29px; padding:0;"><img src="images/banner.png" /></div>    
            </div>
           
            
	</div>
<div class="clearfix"></div>

</div>
<?php include_once("./includes/footer.php"); ?>



    <script type="text/javascript" src="scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
</body>
</html>

<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrent | Myles</title>

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>
<script type="text/javascript">
  $j = jQuery.noConflict();
  $j(document).ready(function(){
	$j("div.item").tooltip();
	window.scrollTo(0, 90);
  });
</script>  
<script>
	_changeFrame = function(id)
	{
		document.getElementById('carswift').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift0.png';
		document.getElementById('caretios').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios0.png';
		document.getElementById('carpolo').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo0.png';
		document.getElementById('carbeat').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat0.png';
		document.getElementById('carsantro').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro0.png';
		document.getElementById('carertiga').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga0.png';
		document.getElementById('carvento').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento0.png';
		document.getElementById('carsunny').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny0.png';
		document.getElementById('carinnova').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova0.png';
		document.getElementById('carxylo').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo0.png';
		document.getElementById('carthar').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar0.png';
		document.getElementById('carxuv').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-5000.png';
		document.getElementById('carhondacity').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity0.png';
		document.getElementById('carendeavour').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour0.png';
		document.getElementById('carfortuner').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner0.png';
		document.getElementById('care2o').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/E2o0.png';
		if(id == 'carswift')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift.png';
			document.getElementById('carswift').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift1.png';
			document.getElementById('carbrandname').innerHTML = 'Maruti Swift';
			document.getElementById('carbrandline').innerHTML = 'This is a zippy companion for ur weekend trip';
			document.getElementById('carbrandcost').innerHTML = '1299/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1299 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1299</li><li>VAT (@12.5%): Rs 163</li><li>Total Fare: Rs 1462</li><li>Refundable Security Deposit (Pre Auth from card): Rs 30000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'caretios')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios.png';
			document.getElementById('caretios').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios1.png';
			document.getElementById('carbrandname').innerHTML = 'Toyota Etios';
			document.getElementById('carbrandline').innerHTML = 'Toyota Etios content to be provided';
			document.getElementById('carbrandcost').innerHTML = '1499/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1499 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1499</li><li>VAT (@12.5%): Rs 188</li><li>Total Fare: Rs 1687</li><li>Refundable Security Deposit (Pre Auth from card): Rs 30000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carpolo')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo.png';
			document.getElementById('carpolo').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/polo1.png';
			document.getElementById('carbrandname').innerHTML = 'Polo';
			document.getElementById('carbrandline').innerHTML = 'The all time international favourite, now available for your driving pleasure.';
			document.getElementById('carbrandcost').innerHTML = '1449/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1449 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1449</li><li>VAT (@12.5%): Rs 182</li><li>Total Fare: Rs 1631</li><li>Refundable Security Deposit (Pre Auth from card): Rs 30000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carbeat')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat.png';
			document.getElementById('carbeat').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat1.png';
			document.getElementById('carbrandname').innerHTML = 'Chevrolet Beat';
			document.getElementById('carbrandline').innerHTML = 'Take a turn for the dynamic, bold, and stylish';
			document.getElementById('carbrandcost').innerHTML = '2300/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Goa)</span><br /><span class=\"carrate\">Rs 2300 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 2300</li><li>VAT (@5%): Rs 115</li><li>Total Fare: Rs 2415</li><li>The billing cycle starts from 8am each day</li><li>Refundable Security Deposit: Rs 5000.00 (To be paid in cash before the start of the journey)</li><li>Additional charges will be applicable for delivery of the vehicle at a special location.</li><li>The vehicle is to be driven within the permissible limits of Goa.</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport</li><li>Driving License</li><li>Any of the following has to be submitted in original as an identity proof.<ul><li>Adhaar Card</li><li>Pan Card</li><li>Voter ID Card</li></ul></li><li>Please note that the car in Goa will be provided through a partner.</li></ul></div>";
		}
		if(id == 'carsantro')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro.png';
			document.getElementById('carsantro').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro1.png';
			document.getElementById('carbrandname').innerHTML = 'Hyundai Santro';
			document.getElementById('carbrandline').innerHTML = 'Keep it simple with santro all time favourite';
			document.getElementById('carbrandcost').innerHTML = '2500/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Goa)</span><br /><span class=\"carrate\">Rs 2500 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 2500</li><li>VAT (@5%): Rs 125</li><li>Total Fare: Rs 2625</li><li>The billing cycle starts from 8am each day</li><li>Refundable Security Deposit: Rs 5000.00 (To be paid in cash before the start of the journey)</li><li>Additional charges will be applicable for delivery of the vehicle at a special location.</li><li>The vehicle is to be driven within the permissible limits of Goa.</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport</li><li>Driving License</li><li>Any of the following has to be submitted in original as an identity proof.<ul><li>Adhaar Card</li><li>Pan Card</li><li>Voter ID Card</li></ul></li><li>Please note that the car in Goa will be provided through a partner.</li></ul></div>";
		}
		if(id == 'carertiga')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga.png';
			document.getElementById('carertiga').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga1.png';
			document.getElementById('carbrandname').innerHTML = 'Ertiga';
			document.getElementById('carbrandline').innerHTML = 'In LUV everything you love to do.';
			document.getElementById('carbrandcost').innerHTML = '1799/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1799 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1799</li><li>VAT (@12.5%): Rs 225</li><li>Total Fare: Rs 2024</li><li>Refundable Security Deposit (Pre Auth from card): Rs 50000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carvento')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento.png';
			document.getElementById('carvento').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento1.png';
			document.getElementById('carbrandname').innerHTML = 'Vento';
			document.getElementById('carbrandline').innerHTML = 'The promise of German engineering at its stylish best.';
			document.getElementById('carbrandcost').innerHTML = '1799/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1799 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1799</li><li>VAT (@12.5%): Rs 225</li><li>Total Fare: Rs 2024</li><li>Refundable Security Deposit (Pre Auth from card): Rs 50000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carsunny')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny.png';
			document.getElementById('carsunny').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny1.png';
			document.getElementById('carbrandname').innerHTML = 'Sunny';
			document.getElementById('carbrandline').innerHTML = 'A comfortable, powerful, companion on the road.';
			document.getElementById('carbrandcost').innerHTML = '1899/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 1899 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 1899</li><li>VAT (@12.5%): Rs 238</li><li>Total Fare: Rs 2137</li><li>Refundable Security Deposit (Pre Auth from card): Rs 50000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carinnova')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova.png';
			document.getElementById('carinnova').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova1.png';
			document.getElementById('carbrandname').innerHTML = 'Toyota Innova';
			document.getElementById('carbrandline').innerHTML = 'Enough room for entire family for entire weekend.';
			document.getElementById('carbrandcost').innerHTML = '2199/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 2199 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 2199</li><li>VAT (@12.5%): Rs 275</li><li>Total Fare: Rs 2474</li><li>Refundable Security Deposit (Pre Auth from card): Rs 50000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carxylo')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo.png';
			document.getElementById('carxylo').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo1.png';
			document.getElementById('carbrandname').innerHTML = 'Mahindra Xylo';
			document.getElementById('carbrandline').innerHTML = 'Your weekend getaway accomplice.';
			document.getElementById('carbrandcost').innerHTML = '3500/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Goa)</span><br /><span class=\"carrate\">Rs 3500 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 3500</li><li>VAT (@5%): Rs 175</li><li>Total Fare: Rs 3675</li><li>The billing cycle starts from 8am each day</li><li>Refundable Security Deposit: Rs 70000.00 (To be paid in cash before the start of the journey)</li><li>Additional charges will be applicable for delivery of the vehicle at a special location.</li><li>The vehicle is to be driven within the permissible limits of Goa.</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport</li><li>Driving License</li><li>Any of the following has to be submitted in original as an identity proof.<ul><li>Adhaar Card</li><li>Pan Card</li><li>Voter ID Card</li></ul></li><li>Please note that the car in Goa will be provided through a partner.</li></ul></div>";
		}
		if(id == 'carthar')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar.png';
			document.getElementById('carthar').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar1.png';
			document.getElementById('carbrandname').innerHTML = 'Mahindra Thar';
			document.getElementById('carbrandline').innerHTML = 'Command respect with this 4x4.';
			document.getElementById('carbrandcost').innerHTML = '3500/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Goa)</span><br /><span class=\"carrate\">Rs 3500 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 3500</li><li>VAT (@5%): Rs 175</li><li>Total Fare: Rs 3675</li><li>The billing cycle starts from 8am each day</li><li>Refundable Security Deposit: Rs 10000.00 (To be paid in cash before the start of the journey)</li><li>Additional charges will be applicable for delivery of the vehicle at a special location.</li><li>The vehicle is to be driven within the permissible limits of Goa.</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport</li><li>Driving License</li><li>Any of the following has to be submitted in original as an identity proof.<ul><li>Adhaar Card</li><li>Pan Card</li><li>Voter ID Card</li></ul></li><li>Please note that the car in Goa will be provided through a partner.</li></ul></div>";
		}
		if(id == 'carxuv')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-500.png';
			document.getElementById('carxuv').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-5001.png';
			document.getElementById('carbrandname').innerHTML = 'Mahindra XUV 500';
			document.getElementById('carbrandline').innerHTML = 'The big daddy of the road.';
			document.getElementById('carbrandcost').innerHTML = '2699/Day';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 2699 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 2699</li><li>VAT (@12.5%): Rs 338</li><li>Total Fare: Rs 3037</li><li>Refundable Security Deposit (Pre Auth from card): Rs 10000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carhondacity')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity.png';
			document.getElementById('carhondacity').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity1.png';
			document.getElementById('carbrandname').innerHTML = 'Honda City';
			document.getElementById('carbrandline').innerHTML = 'Explore the city in a city of your own.';
			document.getElementById('carbrandcost').innerHTML = '3300/Day';
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Goa)</span><br /><span class=\"carrate\">Rs 3300 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 3300</li><li>VAT (@12.5%): Rs 165</li><li>Total Fare: Rs 3465</li><li>The billing cycle starts from 8am each day</li><li>Refundable Security Deposit: Rs 10000.00 (To be paid in cash before the start of the journey)</li><li>Additional charges will be applicable for delivery of the vehicle at a special location.</li><li>The vehicle is to be driven within the permissible limits of Goa.</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport</li><li>Driving License</li><li>Any of the following has to be submitted in original as an identity proof.<ul><li>Adhaar Card</li><li>Pan Card</li><li>Voter ID Card</li></ul></li><li>Please note that the car in Goa will be provided through a partner.</li></ul></div>";
		}
		if(id == 'carendeavour')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour.png';
			document.getElementById('carendeavour').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour1.png';
			document.getElementById('carbrandname').innerHTML = 'Ford Endeavour';
			document.getElementById('carbrandline').innerHTML = 'On road or off road tame it all';
			document.getElementById('carbrandcost').innerHTML = '4499/Day';
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 4499 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 4499</li><li>VAT (@12.5%): Rs 563</li><li>Total Fare: Rs 5062</li><li>Refundable Security Deposit (Pre Auth from card): Rs 70000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'carfortuner')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner.png';
			document.getElementById('carfortuner').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner1.png';
			document.getElementById('carbrandname').innerHTML = 'Toyota Fortuner';
			document.getElementById('carbrandline').innerHTML = 'A 4x4 exhibition on the art of power.';
			document.getElementById('carbrandcost').innerHTML = '4999/Day';
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 4999 / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 4999</li><li>VAT (@12.5%): Rs 625</li><li>Total Fare: Rs 5624</li><li>Refundable Security Deposit (Pre Auth from card): Rs 70000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		if(id == 'care2o')
		{
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/E2o.png';
			document.getElementById('care2o').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/E2o1.png';
			document.getElementById('carbrandname').innerHTML = 'Mahindra e2o';
			document.getElementById('carbrandline').innerHTML = 'A next generation electric vehicle and the greenest way to get around town.';
			document.getElementById('carbrandcost').innerHTML = '100/Hour';

			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs 100 / Hour</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs 800</li><li>VAT (@12.5%): Rs 100</li><li>Total Fare: Rs 900</li><li>Discount Amount: Rs 1600</li><li>Refundable Security Deposit (Pre Auth from card): Rs 5000.00 (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
	}
</script>

<script type="text/javascript">
_frmClearPP = function(){

    document.getElementById("txtNamePP").value = "Name";
    document.getElementById("txtContactNoPP").value = "Mobile No";
    //document.getElementById("txtEmailPP").value = "Emal Id";
	document.getElementById("txtQuery").value = "Query";
};
_frmDisablePP = function(t){
    document.getElementById("txtNamePP").disabled = t;
    document.getElementById("txtContactNoPP").disabled = t;
    //document.getElementById("txtEmailPP").disabled = t;
	document.getElementById("txtQuery").value = t;
};
function _validateMyles()
{
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtNamePP"), "Name", "Please fill in your full name.","ppresponse");
    if(chk)
	chk = isFilledText(document.getElementById("txtContactNoPP"), "Mobile No", "Please fill in your mobile number.","ppresponse");
//    if(chk)
//	chk = isFilledText(document.getElementById("txtEmailPP"), "Email Id", "Please fill in your email id.","ppresponse");
//    if(chk)
//	chk = isEmailAddr(document.getElementById("txtEmailPP"), "Please fill in a valid email id.","ppresponse");
    if(chk)
      chk = isFilledText(document.getElementById("txtQuery"), "Query", "Please fill in your query.","ppresponse");
    if (chk) {
	var ajx;
	var strParam;
	if (window.XMLHttpRequest)
	    ajx = new XMLHttpRequest;
	else if (window.ActiveXObject)
	    ajx = new ActiveXObject("Microsoft.XMLHTTP");
	 strParam = "txtname=" + document.getElementById("txtNamePP").value + "&email=NA&monumber=" + document.getElementById("txtContactNoPP").value + "&query=" + document.getElementById("txtQuery").value;
	 
	ajx.onreadystatechange = function()
	{
	    if (ajx.readyState < 4){
	    
		document.getElementById('ppresponse').innerHTML = "<b style='color:#DB4A28;'>Please Wait...</b>";
	    }
	    else if (ajx.readyState == 4) {
		var r = "";
		if(ajx.status == 200){
		    var url = webRoot + "/myles-thanks.php?op=" + document.getElementById("txtContactNoPP").value;
		    _frmDisablePP(false);
		    _frmClearPP();
		    window.location = url;
		    //document.getElementById("ppresponse").innerHTML = "<b style='color:#DB4A28;'>Thank you for your valued enquiry, will get back to you shortly</b>";
		}
	    }
	};
	ajx.open("POST", webRoot + "/myles-enquiry.php", true);
	ajx.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");        
	ajx.send(strParam);
    }   
};
wx = 90;
function _moveScreen() {
  if(wx <= 500){
    wx += 3;
    window.scrollTo(0, wx);
    setTimeout("_moveScreen()", wx * 0.00012);
  }
}
</script>

	<style>
	  #registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.png") no-repeat scroll 0 0 transparent !important;}
	  #registration{border: 10px solid #db4626 !important;}
#mycarousel
{
	overflow:hidden !important;
}
#mycarousel li{
	list-style: none outside none;
	width:105px;
	margin-right:10px;
}
.jcarousel-skin-tango .jcarousel-container {
	float:left;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-direction-rtl {
	direction: rtl;
}

.jcarousel-skin-tango .jcarousel-container-horizontal {
    width:  828px;
    padding: 0px 20px;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-clip {
    overflow: hidden;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width:  800px;
	margin-left:17px;	
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-item {
    width: 105px;
}

.jcarousel-skin-tango .jcarousel-item-horizontal {
	margin-left: 0;
    /*margin-right: 10px;*/
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-item-horizontal {
	margin-left: 13px;
    margin-right: 0;
}

.jcarousel-skin-tango .jcarousel-item-placeholder {
    background: #fff;
    color: #000;
}

/**
 *  Horizontal Buttons
 */
.jcarousel-skin-tango .jcarousel-next-horizontal {
    position: absolute;
    top: 0px;
    right: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png) no-repeat;
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-next-horizontal {
    left: 8px;
    right: auto;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:focus {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:active {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:active {
    cursor: default;
    background-position: -40px 0;
	background-color:transparent;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal {
    position: absolute;
    top: 0px;
    left: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-left.png) no-repeat -40px 0;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-prev-horizontal {
    left: auto;
    right: 15px;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:focus {
    background-position: -0px 0;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:active {
    background-position: -40px 0;
}

.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:active {
    cursor: default;
    background-position: 0px 0;
	width:20px;
}
	</style>
</head>

<body>
<?php include_once("./includes/header-for-myles.php"); ?>
<div id="main-container">
	<div class="inner-container" style="float:left;margin-left:33px;">
    	<div class="myles-logo"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/myles.png" /></div>
		<div style="float:right;margin:28px 15px 0px 0px;"><!--<span class="callnow"><span style="color:#333;font-size:12px;">Call 24x7</span> <strong style="font-family: 'HelveticaLTStd';">0888 222 2222</strong></span>--></div>
    </div>
    <div class="banner">
    	 <div id="slider" class="nivoSlider" style="float:left !important; margin:19px !important;">
                <img src="<?php echo corWebRoot; ?>/myles-campaign/images/myles_week2_1.jpg" alt="" title="" />
                <img src="<?php echo corWebRoot; ?>/myles-campaign/images/myles_week2_2.jpg" alt="" title="" />
		<img src="<?php echo corWebRoot; ?>/myles-campaign/images/myles_week2_3.jpg" alt="" title="" />
            </div>
    </div>
    
	<div class="car-container">
			<div class="car-heading" onclick="javascript: _moveScreen();">
            	<div class="text">Pick from a wide range of cars</div>
                <a class="know-more" href="javascript: void(0)" id="bkn">Available in 10 cities</a>
            </div>
            <div class="show-car">
            	<div class="car-img">
					<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner.png" id="carbrandimage" alt="" title=""/>
				</div>
		<div class="holddata">
                <div class="holdall">
                	<span id="carbrandname">Toyota Fortuner</span>
					<span id="carbrandcost">4999/DAY</span>
					<span class="smdetails">
					<div id="item_1" class="item">
					Details +
					<div class="tooltip_description" style="display:none" id="popupFDetails">
						<div class="pophold">
							<span class="headliners">Rate (Delhi)</span><br />
							<span class="carrate">Rs 4999 / Day</span><br />
							<span class="headliners">Fare Details</span><br />
							<ul>
								<li>Minimum Billing: Rs 4999</li>
								<li>VAT (@12.5%): Rs 625</li>
								<li>Total Fare: Rs 5624</li>
								<li>Refundable Security Deposit (Pre Auth from card): Rs 70000.00 (Mastercard and Visa only)</li>
							</ul>
							<span class="headliners">Mandatory Documents</span><br />
							<ul>
								<li>Passport/ Voter ID Card</li>
								<li>Driving License</li>
								<li>Credit Card</li>
							</ul>
						</div>
					</div>
				  </div>
				  </span>
					<span id="carbrandline">A 4x4 exhibition on the art of power.</span>
               </div>
               <div class="holdallbottom">
		<br />
					<span class="avin">Cars available in</span>
					<div class="linkbar">
						<a href="javascript: void(0)" class="first">DELHI</a><span class="spanner">|</span>
						<a href="javascript: void(0)">MUMBAI</a><span class="spanner">|</span>
						<a href="javascript: void(0)">BANGALORE</a><span class="spanner">|</span>
						<a href="javascript: void(0)">HYDERABAD</a><span class="spanner">|</span>
						<a href="javascript: void(0)">GOA</a><br />
						<a href="javascript: void(0)" class="first">JAIPUR</a><span class="spanner">|</span>
						<a href="javascript: void(0)">CHENNAI</a><span class="spanner">|</span>
						<a href="javascript: void(0)">PUNE</a><span class="spanner">|</span>
						<a href="javascript: void(0)">AHMEDABAD</a><span class="spanner">|</span>
						<a href="javascript: void(0)">CHANDIGARH</a> 
					</div>
					<div  class="clearfix"></div>
                    <a href="<?php echo corWebRoot; ?>/#selfdrive" class="bkn" target="_blank"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/booknow.png" alt="Book Now" title="Book Now"/></a>
               </div>
	    </div>
                 <div class="holdallform">
		    <p class="pp1">Assured 5 mins call back</p>
		    <p class="pp2" id="selectedpackage"></p>
		    <input type="hidden" id="hdPackagePP" name="hdPackagePP" value="" />
		    <p class="prow"><input type="text" id="txtNamePP" name="txtNamePP" class="txtbox" value="Name" onblur="javascript: if(this.value==''){this.value='Name';}" onfocus="javascript: if(this.value=='Name'){this.value='';}"/></p>
		    <p class="prow"><input type="text" id="txtContactNoPP" name="txtContactNoPP" class="txtbox" value="Mobile Number" onblur="javascript: if(this.value==''){this.value='Mobile Number';}" onfocus="javascript: if(this.value=='Mobile Number'){this.value='';}" onkeypress="javascript: return _allowNumeric(event);" maxlength="10" /></p>
		    <p class="prow"><textarea type="text" id="txtQuery" name="txtQuery" class="tarea" cols="60" rows="5" value="Query" onblur="javascript: if(this.value==''){this.value='Query';}" onfocus="javascript: if(this.value=='Query'){this.value='';}">Query</textarea></p>
		    <p class="prow"><span class="mf"></span><input type="image" src="<?php echo corWebRoot; ?>/myles-campaign/images/submit-2.jpg" id="btnSubmitPP" class="btnsumyles" name="btnSubmitPP" onclick="_validateMyles();"/></p>
		    <p class="prow mf" id="ppresponse"></p>
		 </div>
                <div  class="clearfix"></div>
                
				<!-- <img src="<?php //echo corWebRoot; ?>/myles-campaign/images/banner.png" /> -->
				<div style="float:left;width:950px;margin:25px 0px 20px 15px">
				<ul id="mycarousel" class="jcarousel-skin-tango">
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner1.png" id="carfortuner" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour0.png" id="carendeavour" onclick="javascript: _changeFrame(this.id)" />
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity0.png" id="carhondacity" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-5000.png" id="carxuv" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar0.png" id="carthar" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo0.png" id="carxylo" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova0.png" id="carinnova" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny0.png" id="carsunny" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento0.png" id="carvento" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga0.png" id="carertiga" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro0.png" id="carsantro" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat0.png" id="carbeat" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo0.png" id="carpolo" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios0.png" id="caretios" onclick="javascript: _changeFrame(this.id)"/>
					</li>
					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift0.png" id="carswift" onclick="javascript: _changeFrame(this.id)"/>
					</li>

					<li>
						<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/E2o0.png" id="care2o" onclick="javascript: _changeFrame(this.id)"/>
					</li>
				</ul>
				</div>
            </div>
           
            
	</div>
<div class="clearfix"></div>

</div>
    <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

	<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.jcarousel.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#mycarousel').jcarousel({
				start: 1
			});
		});
	</script> 

	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Swift0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Etios0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Polo0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Beat0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Santro0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Ertiga0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Vento0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Sunny1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Innova0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Thar0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-500.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-5001.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/XUV-5000.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Xylo0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/HondaCity0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Endeavour0.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner1.png" width="1px" class="abs" />
	<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/Fortuner0.png" width="1px" class="abs" />
</body>
</html>

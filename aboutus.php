<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" style="width:100%" >
	<div class="yellwborder" style="margin: 20px 0 0 0 !important;">
		<div class="magindiv">
			<h2 class="heading2">About Us</h2>
		</div>
	</div>
	<div class="lightborder about">
		<div class="magindiv">
			<ul>
				<li><a href="javascript:void(0);" id="corp" class="active" onclick="javascript: _setActive(this.id);">Corporate</a></li>
				<li><a href="javascript:void(0);" id="miss"  onclick="javascript: _setActive(this.id);">Mission</a></li>
				<li><a href="javascript:void(0);" id="mana" onclick="javascript: _setActive(this.id);">Management Team</a></li>
			</ul>
		</div>
		<div class="magindiv">
			<img id="arrowmoment" width="18" height="8" src="./images/selecttbimg.png" border="0"/>
		</div>
	</div>
	<div class="mainW960">
	<div id="divcorp" class="tabbbox" style="display:block">
		<div class="divtabtxt">
			<p>India's # 1 personal ground Transportation Service Provider</p>
			<span class="txtcar">A pioneer in shaping the personal ground transportation industry in India, Carzonrent (India) Pvt. Ltd. (CIPL) is <b>India's # 1 personal ground transportation service provider</b> today offering a complete bouquet of end-to-end long and short term car rental solutions through its fleet of 6500 cars across the country.</span>
			<span class="txtcar">A brainchild of Rajiv K. Vij, CEO, Carzonrent (India) Pvt. Ltd., was launched in the year 2000 with a short-term objective of offering a safe and reliable medium of travel to customers and a long-term vision of giving form and structure to the unorganized Indian personal ground transportation industry and helping the industry get its due recognition.</span>
			<span class="txtcar">"COR" the branding of Carzonrent signifies the fact that Personal Ground Transportation is the "Core" business of Carzonrent. Carzonrent is perhaps the first and the only player to offer complete 360 degree solutions to completely transform the Indian personal ground transportation industry into an organized business.</span>
			<span class="txtcar">Ensuring passenger comfort to the minutest detail, Carzonrent has pulled out all stops, from bringing in the latest technology to setting up <b>24x7 customer support centers;</b> to bringing uniformity in operations; to introducing customized solutions for all customer segments, the company has focused all efforts towards organizing the largely unorganized Indian personal ground transportation industry.</span>
		</div>
		<div class="divrht">
			<h1 style="float:left;width:100%;margin:15px 0px 0px 10px;">Vision</h1>			
			<span style="float:left;padding-left:10px;font-weight:bold;line-height:20px;"><img width="73" height="75" src="./images/bulbicon.png" border="0" style="float:right;margin-top:-25px;"/>To build a technology driven, nationally networked personal ground transportation services company with diversified revenue streams to provide business stability, sustainable revenue and profitable growth.</span>
		</div>
	</div>
	<div id="divmiss" class="tabbbox">
		<div style="float:left;width:855px;">
			<p>To build an institution to achieve customer loyalty</p>
			<span class="txtcar">Carzonrent is the result of a Dream - to build an Institution, which will achieve customer loyalty through understanding ever-changing needs, <b>deliver service quality</b> and follow value for money approach.</span>
			<span class="txtcar">Carzonrent through focus on <b>Human Resource Development, Transparency and Involvement of Team</b> Members in decision - making process will work towards building a team of highly creative, knowledgeable members with impeccable integrity.</span>
			<span class="txtcar">The Core Management Team of Carzonrent has taken upon itself the responsibility to realize the Dream of building a business, creating an organization and developing it into an "Institution".</span>
			<span class="txtcar">As a team, we have the knowledge and understanding of business and its future potential, complimentary management skills and a <b>commitment to achieve the leadership</b> in the car rental industry. The task is tough, we have to cover many grounds, we have to fight many battles, and we have to achieve what most people with our means do not even dream.</span>	
			<span class="txtcar">Carzonrent intends to be the preferred option for all its stakeholders through <b>consistently building, preserving and delivering high value</b> to them.</span>
		</div>
		<div class="divrht">
			<h1 style="float:left;width:100%;margin:15px 0px 0px 10px;">Vision</h1>			
			<span style="float:left;padding-left:10px;font-weight:bold;line-height:20px;"><img width="73" height="75" src="./images/bulbicon.png" border="0" style="float:right;margin-top:-25px;"/>To build a technology driven,nationally networked personal ground transportaion servicres company with diversified revenue streams to provide business stabilty, sustaniable revenue and profitable growth.</span>
		</div>
	</div>
	<div id="divmana" class="tabbbox">
		<div style="float:left;width:100%;">
			<p style="float:left;width:100%;margin-bottom:15px;">Our Team</p>
			<div class="teamdiv">
				<div class="panal">
					<img style="float:left;margin:8px;"src="./images/rajiv.jpg" border="1px solid #fff;" width="117" height="152"/>
					<h3 style="margin-top:5px;">Rajiv K Vij</h3>
					<h4 style="margin-top:5px;">Managing Director  & CEO</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Having done his graduation from Hindu College, he followed it up with Post Graduation in Management from the Faculty of Management Studies , Delhi University . A major part of his initial working years were within the Automobile Industry.</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">
					<a id="rajive_<?php echo $i; ?>" href="javascript:void(0);"class="clickme" style="color:#ff6600;text-decoration:none;"onclick="javascript:_lockPage();_setDivPos('popuprajive');">Read More [+]</a></h4>
				</div>
				
				
				<div class="panal">
					<img style="float:left;margin:8px;"src="./images/rajesh.jpg" border="1px solid #fff;" width="117" height="152"/>
					<h3 style="margin-top:5px;">Rajesh Munjal</h3>
					<h4 style="margin-top:5px;">Director Operations</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Rajesh heads
the operations function for all businesses of the company and is responsible for managing existing business
operations in Delhi/Mumbai/Bangalore and Hyderabad and ensuring implementation
of operating strategy in all businesses</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px"><a id="rajeshop_<?php echo $i; ?>" href="javascript:void(0);" style="color:#ff6600;text-decoration:none;"onclick="javascript:_lockPage();_setDivPos('popuprajeshop');">Read More [+]</a></h4>
				</div>
			
				
				
				
				<!--<div class="panal">
					<img style="float:left;margin:8px;"src="./images/rajesh.jpg" border="1px solid #fff;" width="117px" height="152px"/>
					<h3 style="margin-top:5px;">Rajesh Munjal</h3>
					<h4 style="margin-top:5px;"> Director Operations</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Rajesh heads the operations function for Radio Taxi business and is responsible for managing existing business operations in Delhi/Mumbai/Bangalore and Hyderabad and ensuring implementation of operating strategy for taxi business.</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px"><a id="rajesh_<?php echo $i; ?>" href="javascript:void(0);" style="color:#ff6600;text-decoration:none;"onclick="javascript:javascript:_lockPage();_setDivPos('popupRajesh');">Read More [+]</a></h4>
			</div>-->
			</div>	
			<div class="teamdiv">
				
				<div class="panal">
					<img style="float:left;margin:8px;"src="./images/shakshi-vij.jpg" border="1px solid #fff;" width="117" height="152"/>
					<h3 style="margin-top:5px;">Sakshi Vij</h3>
					<h4 style="margin-top:5px;">Executive Director Corporate Strategy and Marketing</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Sakshi handles the marketing and strategy
function of the company, her core responsibility is to discuss,
define and monitor implementation of marketing and communication Strategies for
all functions in the organization
advisors. ...</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px"><a id="sakshi_<?php echo $i; ?>" href="javascript:void(0);" style="color:#ff6600;text-decoration:none;"onclick="javascript:_lockPage();_setDivPos('popupsakshi');">Read More [+]</a></h4>
				</div>
			</div>	
				
			<div class="teamdiv">
				<div class="panal">
					<img style="float:left;margin:8px;"src="./images/rakesh.jpg" border="1px solid #fff;" width="117" height="152"/>
					<h3 style="margin-top:5px;">Rakesh Bhatia</h3>
					<h4 style="margin-top:5px;">Financial Controller</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Rakesh is responsible for corporate finance/accounts, taxation and treasury functions at Carzonrent. Being a qualified chartered accountant, he has worked for over 11 years in Transportation industry in the country. Rakesh has been with Carzonrent for almost 5 years...</h4>
					<h4 style="margin-top:5px;font-weight:normal;line-height:19px"><a id="rekesh_<?php echo $i; ?>" href="javascript:void(0);" style="color:#ff6600;text-decoration:none;"onclick="javascript:_lockPage();_setDivPos('popuprakesh');"">Read More [+]</a></h4>
				</div>				
			</div>
		</div>
	</div>
	</div>
</div>
<div id="popuprajive" class="popupteam">
	<a id="popuprajiveclose" class="popupclose" href="javascript:void(0);" onclick="_hideFloatingObjectWithID('popuprajive');_unlockPage();">Close</a>
	<div class="panal" style="border:0px;margin:0px;">
		<img style="float:left;margin:8px;"src="./images/rajivebig.png" border="1px solid #fff;" width="158" height="205"/>
		<h3 style="margin-top:5px;">Rajiv K Vij</h3>
		<h4 style="margin-top:5px;">Managing Director  & CEO</h4>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Having done his graduation from Hindu College, he followed it up with Post Graduation in Management from the Faculty of Management Studies , Delhi University . A major part of his initial working years were within the Automobile Industry . He held various positions at senior levels in Sales and Marketing with industry majors like Hindustan Motors before moving to Travel Industry . In the travel industry he can be credited with being instrumental in the growth and expanded reach of global car rental brands in India.</h4><br/>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">As Head of the Car Rental Business with International Travel House , an ITC group company he was responsible for expanding and turning around the Europcar Brand in India. Having successfully established this segment , he went on to Head - Sales for all travel segments at Travel House reporting directly to the Board of Directors.</h4>
		<br/>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">The entrepreneur in him saw an opportunity in working with the world's largest car rental brand, Hertz and in 2001 he set up Carzonrent (India) Private Limited with a team of like minded professionals. The company was appointed Master Licensee for Hertz International in this region. A hands on professional in this segment he spearheaded the expansion of Hertz in India and in a short period of 4 years has taken the organization to the # 1 position in Car Rentals in India.</h4><br/>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Having established a strong core team of professional managers, he now spends more time in taking the organization into its next level of expansion. Primary focus is on building Carzonrent into an integrated passenger ground transportation company through expansion of the services portfolio into Self Drive, Fleet Management and Low Cost Taxi Services.</h4>
	</div>
</div>

<div id="popupsakshi" class="popupteam">
	<a id="popupsakshiclose" class="popupclose" href="javascript:void(0);" onclick="_hideFloatingObjectWithID('popupsakshi');_unlockPage();">Close</a>
	<div class="panal" style="border:0px;margin:0px;">
		<img style="float:left;margin:8px;"src="./images/shakshi-vij.jpg" width="117" height="152"/>
		<h3 style="margin-top:5px;">Sakshi Vij</h3>
		<h4 style="margin-top:5px;"> Executive Director Corporate Strategy and Marketing</h4>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Sakshi handles the marketing and strategy
function of the company, her core responsibility is to discuss,
define and monitor implementation of marketing and communication Strategies for
all functions in the organization. Having graduated with an MBA in Services Marketing and
Management from the SP Jain Centre of Management-Dubai-Singapore, she has
valuable experience of working in two distinctly progressive cities with
organizations like YCH Logistics. She has also worked on projects with Finoble
advisors.</h4><br/>
		<!--<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Being responsible for the marketing function of the leader in Car rentals in a highly competitive industry, she takes the charge with innovation and creativity as the most important tools.</h4>-->
	</div>
</div>
<div id="popuprajeshop" class="popupteam">
	<a id="popupreshavclose" href="javascript:void(0);" onclick="_hideFloatingObjectWithID('popuprajeshop');_unlockPage();" class="popupclose">Close</a>
	<div class="panal" style="border:0px;margin:0px;">
		<img style="float:left;margin:8px;"src="./images/rajesh.jpg" width="117" height="152"/>
		<h3 style="margin-top:5px;">Rajesh Munjal</h3>
		<h4 style="margin-top:5px;">Director Operations</h4>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Rajesh heads
the operations function for all businesses of the company and is responsible for managing existing business
operations in Delhi/Mumbai/Bangalore and Hyderabad and ensuring implementation
of operating strategy in all businesses.</h4><br/>
		
	</div>
</div>
<div id="popuprakesh" class="popupteam">
	<a id="popuprakeshclose" href="javascript:void(0);" onclick="_hideFloatingObjectWithID('popuprakesh');_unlockPage();" class="popupclose">Close</a>
	<div class="panal" style="border:0px;margin:0px;">
		<img style="float:left;margin:8px;"src="./images/rakesh.jpg" width="117" height="152"/>
		<h3 style="margin-top:5px;">Rakesh Bhatia</h3>
		<h4 style="margin-top:5px;">Financial Controller</h4>
		<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Rakesh is responsible for corporate finance/accounts, taxation and treasury functions at Carzonrent. Being a qualified chartered accountant, he has worked for over 11 years in Transportation industry in the country. Rakesh has been with Carzonrent for almost 5 years and during this period, he has handled finance and accounts of all businesses.</h4><br/>
	</div>
</div>
<!--<div id="backgroundPopup"></div>-->
<div id="overlay123"></div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

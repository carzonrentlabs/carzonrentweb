<!DOCTYPE html>
<html>
<head>
<meta property="og:title" content="Myles- Self Drive" />
<meta property="og:description" content="Hey guys! I'm about to hit the open road with a Myles cars! Check out the link below if you want to do the same!" />
<meta property="og:image" content="http://www.carzonrent.com/myles-campaign/shraredImage/new.png?v=2">
<meta http-equiv="Cache-control" content="no-cache">

</head>
<link rel="stylesheet" type="text/css" href="trip.css">
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></head>
<body>
<div id="trip">
<div class="trip_top">
<div class="f_l"><strong>Trip details</strong></div>
<div class="f_r"><a class="Cbooking" href="#">Cancel Booking</a></div>
</div>
<div class="w96 ptb_lr_1_2">
<div class="w68 mr2 f_l">
<span class="nameI">Hey Rohan,</span>
<p>Your MARUTI SWIFT is booked and ready to hit the road. Here&#8217;s your booking confirmation. Feel free to get in touch with us at anytime, day or night.</p>
<div class="bh4"> </div>
<p>
Booking ID: 5600729 <br>
Car Type: Maruti Swift <br>
From: Delhi <br>
Service: Self Drive <br>
Destination: Delhi <br>
Pickup Date: 2014-09-09 <br>
Pickup Time: 1900 HRS <br>
Drop Date: 2014-09-10 <br>
Address: Address not required for the Self Drive
</p>
<span class="redT">Round Trip Fare</span> <br>
<span class="fs20">Rs.1574/-</span> (Including VAT) <br>
Payment Mode: Pay Online <br>
<strong>Fee Details</strong>
<ul class="trip_arrow">
<li>Minimum Billing: Rs. 1399/-</li>
<li>Additional Serive Cost: Rs. 0/-</li>
<li>Sub Total: Rs. 1399/-</li>
</ul>
VAT(@12.500%): Rs. 175/- <br>
Total Fare: Rs. 1574/-
</div>
<div class="w30 f_l">
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1439754936255803&version=v2.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<img class="mb25" border="0" src="http://www.carzonrent.com/mylesimages/add1.jpg"/>
<a href="http://www.carzonrent.com/ReferralProgram/" target="_blank"><img class="mb25" border="0" src="http://www.carzonrent.com/mylesimages/add3.jpg"/></a>
<div class="add2 mb25 relative">
<div class="tripF"><div class="fb-share-button" data-href="http://www.carzonrent.com/myles-campaign.php?v=2"></div></div>
<div class="tripT"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.carzonrent.com/myles" data-text="Hey guys, Just booked a car @Myles_IN ! You can do the same too at" data-count="none">Tweet</a>
</div>
</div>
</div>
</div>
<div class="clr"> </div>
</div>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Find Job Opening | Job Opportunities in Carzonrent (India) Pvt. Ltd.</title>
<meta name="description" content="Carzonrent - If you are interested in a career with us for the position in operation, fleet management & operating lease, quality assurance, sales, finance and accounts please email your resume to humanresources@carzonrent.com." />
<meta name="keywords" content="job opportunities, job opportunities for, job opportunity, job opening, find job openings, career with Carzonrent." />

<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/tabbing.js"></script>
<script type="text/javascript" src="js/jquery.tool.min.js" language="javascript"></script>
<script type="text/javascript" src="js/popup1.js" language="javascript"></script>
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder">
		<div class="yellodivtxt" style="padding:7px 0px 7px 0px;">
			<h2>Career</h2>
		</div>
	</div>
	<div class="lightborder">
		<div class="magindiv">
			<ul>
				<li><a href="javascript:void(0);" id="opera" class="active" onclick="javascript: _setActivecareers(this.id);">Operations</a></li>
				<li><a href="javascript:void(0);" id="fleet"  onclick="javascript: _setActivecareers(this.id);">Fleet Management & Operating Lease</a></li>
				<li><a href="javascript:void(0);" id="qual" onclick="javascript: _setActivecareers(this.id);">Quality Assurance</a></li>
				<li><a href="javascript:void(0);" id="sales" onclick="javascript: _setActivecareers(this.id);">Sales</a></li>
				<li><a href="javascript:void(0);" id="finan" onclick="javascript: _setActivecareers(this.id);">Finance & Accounts</a></li>
			</ul>
		</div>
		<div class="yellodivtxt">
			<img id="arrowmoment" src="./images/selecttbimg.png" border="0"/>
		</div>
	</div>
	<div class="divcareercenter">
	<div id="divopera" class="tabbbox" style="display:block">
		<div class="divtabtxt">
			<p style="margin-bottom:20px;">Operations</p>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Business Managers/Profit Center Head</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:40px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>We are looking for MBA's with 10-15 years of experience in the Services Industry having successfully managed the Business/ Operations with extremely strong commitment to Financial Controls and Operating Processes supported by track record of having achieved business objectives.</td>
					</tr>
				</table>
				<span style="margin:0px 0px 5px 17px;"><b>Location:</b> Delhi</span>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Operations Managers-Customer Service/Airport Operations/ Vendor Development and Management/Driver recruitment</h4>
				<table style="margin:15px 0px 0px 15px;"cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:40px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>MBA/Graduate with 4-6 years of experience in Service/Automobile/Aviation/Travel/Hospitality industry having extremely strong commitment to Operating Processes, ability to handle conflicts/pressures and in managing large manpower including drivers.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Strong communication and inter personal skills are critical for this role.</td>
					</tr>
				</table>
				<span style="margin:0px 0px 5px 17px;"><b>Location:</b> Delhi, Bangalore, Hyderabad, Mumbai</span>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Operations Executives-Customer Service/Airport Operations /Vendor Management/Driver Recruitment</h4>
				<table style="margin:15px 0px 0px 15px;" cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;line-height:25px;" border="0"/></span>Graduate with 2-4 years of experience in Services/ Automobile/Aviation/Travel/Hospitality Industry having extremely strong commitment to Operating Processes.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Strong communication and inter personal skills are critical for this role.</td>
					</tr>
				</table>
				<h4 style="margin:5px 0px 5px 17px;">Location: Delhi, Bangalore, Hyderabad, Mumbai</h4>
			</div>
			<span class="txtcar">Should you be interested in a career with us please email your resume to <a class="txtmail" href="mailto:humanresources@carzonrent.com">humanresources@carzonrent.com</a> <br/>indicating the "<strong>Post Applied for</strong>" as the subject.</span>
		</div>
		<div class="imagediv imghand">
			<img src="./images/handshake.png" border="0" width="228px" height="297px"/>
		</div>
	</div><!-- end of divairp-->
	<div id="divfleet" class="tabbbox">
		<div style="float:left;width:650px;">
			<p style="margin-bottom:20px;">Fleet Management & Operating Lease</p>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Operations Managers- Fleet Management</h4>
				<table style="margin:15px 0px 0px 15px;" cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:40px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Degree/Diploma in Automobile/Mechanical Engineering with 4-6 years of experience in Service/Automobile industry having extremely strong commitment to Operating Processes and in managing large manpower besides ability to handle conflicts and pressures.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Strong communication and inter personal skills are critical for this role.</td>
					</tr>
				</table>
				<h4 style="margin:0px 0px 5px 17px;">Location: Delhi, Bangalore, Hyderabad, Mumbai</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Operations Executives- Fleet Management</h4>
				<table style="margin:15px 0px 0px 15px;" cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:40px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Degree/Diploma in Automobile/Mechanical Engineering with 2-4 years of experience in Services/ Automobile Industry having extremely strong commitment to Operating Processes and in managing large manpower besides ability to handle conflicts and pressures.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Strong communication and inter personal skills are critical for this role.</td>
					</tr>
				</table>
				<h4 style="margin:0px 0px 5px 17px;">Location: Delhi, Bangalore, Hyderabad, Mumbai</h4>
			</div>
		</div>
		<div class="imagediv imghand">
			<img src="./images/handshake.png" border="0" width="228px" height="297px"/>
		</div>
	</div><!-- end of divchau-->
	<div id="divqual" class="tabbbox">
		<div style="float:left;width:650px;">
			<p style="margin-bottom:20px;">Quality Assurance</p>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Operations Executives- Quality Control</h4>
				<table style="margin:15px 0px 0px 15px;" cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;line-height:25px;" border="0"/></span>Graduate with 2-4 years of experience in Services/ Automobile Industry having extremely strong commitment to Operating Processes and in managing large manpower besides ability to handle conflicts and pressures.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;" border="0"/></span>Strong communication and inter personal skills are critical for this role.</td>
					</tr>
				</table>
				<h4 style="margin:0px 0px 5px 17px;">Location: Delhi, Bangalore, Hyderabad, Mumbai</h4>
			</div>
			<span class="txtcar">Should you be interested in a career with us please email your resume to <a class="txtmail" href="mailto:humanresources@carzonrent.com">humanresources@carzonrent.com</a> <br/>indicating the "<strong>Post Applied for</strong>" as the subject.</span>			
		</div>
		<div class="imagediv imghand">
			<img src="./images/handshake.png" border="0" width="228px" height="297px"/>
		</div>
	 </div><!-- end of divself-->
	 <div id="divsales" class="tabbbox">
		<div class="divtabtxt">
			<p style="margin-bottom:20px;">Sales</p>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Business Development Executives</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;line-height:25px;" border="0"/></span>MBA/ Graduates with 2-6 years of experience in corporate/ institutional selling of Automobiles /Services with successful target oriented track record.</td>
					</tr>
				</table>
				<h4 style="margin:0px 0px 5px 17px;">Location: Mumbai</h4>
			</div>
			<span class="txtcar">Should you be interested in a career with us please email your resume to <a class="txtmail" href="mailto:humanresources@carzonrent.com">humanresources@carzonrent.com</a> <br/>indicating the "<strong>Post Applied for</strong>" as the subject.</span>				
		</div>
		<div class="imagediv imghand">
			<img src="./images/handshake.png" border="0" width="228px" height="297px"/>
		</div>
	</div><!-- end of limo-->
	<div id="divfinan" class="tabbbox">
		<div class="divtabtxt">
			<p style="margin-bottom:20px;">Finance and Accounts</p>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Executive - Finance and Accounts</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" style="margin-right:2px;line-height:25px;" border="0"/></span>CA(Inter)/B.Com. with 3-8 years of experience in managing the financial functions- statutory accounting, budgeting, MIS, etc.</td>
					</tr>
				</table>
				<h4 style="margin:0px 0px 5px 17px;">Location: Delhi</h4>
			</div>
			<span class="txtcar">Should you be interested in a career with us please email your resume to <a class="txtmail" href="mailto:humanresources@carzonrent.com">humanresources@carzonrent.com</a> <br/>indicating the "<strong>Post Applied for</strong>" as the subject.</span>				
		</div>
		<div class="imagediv imghand">
			<img src="./images/handshake.png" border="0" width="228px" height="297px"/>
		</div>
	</div><!-- end of oper-->	
	</div><!-- end of divcenter -->
</div>
<div id="backgroundPopup"></div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

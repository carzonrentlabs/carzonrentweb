<?php

//error_reporting(E_ALL);
//sini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');
include_once('classes/rest.php');

$Myle = new Myles();

$db = new MySqlConnection(CONNSTRING);
$db->open();

echo $book_query = "SELECT * from myles_extratax where mail_status=0";

$book = $db->query("query", $book_query);

//echo '<pre>';
//print_r($book);
//die;
if (!array_key_exists("response", $book)) 
{

    for ($i = 0; $i < count($book); $i++) 
	{
		$id = $book[$i]['id'];
        $bookingid = $book[$i]["booking_id"];
        $email_id = $book[$i]['email_id'];
        $amount = $book[$i]['extra_amount'];
		$totalamount = $book[$i]['total_amount'];
		$url = 'https://www.mylescars.com/bookings/taxpayment?bookingid='.base64_encode($bookingid);
		
		
		$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
<title>Myles</title>
<style type="text/css">
body {
	margin: 0;
	padding: 0;
}
.mainTable {
	height: 100% !important;
	width: 100% !important;
	margin: 0;
	padding: 0;
}
img, a img {
	border: 0;
	outline: none;
	text-decoration: none;
}

.imageFix {
	display: block;
}
table, td {
	border-collapse: collapse;
	mso-table-lspace: 0pt;
	mso-table-rspace: 0pt;
}
p {
	margin: 0;
	padding: 0;
	margin-bottom: 0;
}
img {
	-ms-interpolation-mode: bicubic;
}
img:focus, a:focus { outline: 0; border: 0; }
body, table, td, p, a, li, blockquote {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
table {
	border-collapse: collapse;
}
</style>
<style>
@media only screen and (max-width: 600px) {
body[yahoo] .rimg {
	max-width: 100%;
	height: auto;
}
body[yahoo] .rtable {
	width: 100% !important;
	table-layout: fixed;
}
body[yahoo] .rtable tr {
	height: auto !important;
}
body[yahoo] .rblock {
	display:block;}
}
</style>
<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>

<body yahoo=fix scroll="auto" style="padding:0; margin:0; font-size: 12px; font-family: Arial, Helvetica, sans-serif; cursor:auto;color:#000000; background: #ddd">
<table class="rtable" style="width:600px;margin:auto; background:#ebebeb;" width="600" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
 <td align="left" valign="top">
 <table class="rtable" style="border-collapse:collapse: border="0" cellspacing="0" cellpadding="0" width="100%">


 <tr>
   <td>
     <table style="background: #ebebeb;" width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td><img src="https://myles-marketing.s3.amazonaws.com/gstimgTop.jpg" style="outline: 0; border: 0;" alt=""></td>
       </tr>
     </table>
   </td>
 </tr>
 <tr>
   <td>
     <table style="background: #ebebeb;" width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td style="padding: 20px; font-size: 14px; line-height: 24px; margin: 0px; color: #262626;">
           <p style=" margin: 0px; padding-bottom: 20px;"><strong>Dear Myler,<br/></strong></br>
As you are aware that your booking id '.$bookingid.' is ending after 30th June, it would come under the purview of GST. Hence, new GST rate will be applicable on your booking and your revised booking amount will be &#8377; '.number_format($totalamount,2).'.</p>
<p style=" margin: 0px; padding-bottom: 20px;">We would request you to pay the tax differential amount of &#8377; '.number_format($amount,2).'. We have created a new payment link and you can pay via the same.</p>
<p style=" margin: 0px; padding-bottom: 20px;">Would appreciate your support on the same.<br/><br/>
In case you have received a similar mail before, kindly ignore the same as the previous payment link has been expired.</br>
</p>
<p>Sincerely,</br>
Myles</p>
         </td>
       </tr>
     </table>
   </td>
 </tr>
 <tr>
   <td>
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td><a href="'.$url.'" target="_blank"><img src="https://myles-marketing.s3.amazonaws.com/gstimgBott.jpg" style="outline: 0; border: 0;" alt=""></a></td>
       </tr>
     </table>
   </td>
 </tr>
   
  
   <tr>
    <td align="center" valign="top" style="background-color:#272727;color:#ffffff;padding:22px 15px 30px;">
<table class="rtable" style="width:100% !important;" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
    <table width="140" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><a style="text-decoration:none; color:#000000;" href="https://twitter.com/MylesCars" target="_blank"><img style="border:0;display:block;background-color:transparent;margin:0;padding:0;" class="rimg" src="http://52.74.77.52/html-alliance-offer/images/twitter.png" alt="Twitter"></a></td>
    <td align="center" valign="middle"><a style="text-decoration:none; color:#000000;" href="https://www.facebook.com/Mylescars" target="_blank"><img style="border:0;display:block;background-color:transparent;margin:0;padding:0;" class="rimg" src="http://52.74.77.52/html-alliance-offer/images/facebook.png" alt="Facebook"></a></td>
    <td align="center" valign="middle"><a style="text-decoration:none; color:#000000;" href="https://www.instagram.com/mylescars" target="_blank"><img style="border:0;display:block;background-color:transparent;margin:0;padding:0;" class="rimg" src="http://52.74.77.52/html-alliance-offer/images/instagram.png" alt="Instagram"></a></td>
    <td align="center" valign="middle"><a style="text-decoration:none; color:#000000;" href="https://www.linkedin.com/company-beta/9426659/" target="_blank"><img style="border:0;display:block;background-color:transparent;margin:0;padding:0;" class="rimg" src="http://52.74.77.52/html-alliance-offer/images/linkedin.png" alt="linkedin"></a></td>
   
  </tr>
</table>

    </td>
  </tr>
  <tr>
    <td style="padding-top:15px;padding-bottom:12px;"><hr style="width:100%;border:none;height:1px;background-color:#6c6c6c;margin-top:0;margin-bottom:0;" /></td>
  </tr>
  <tr>
    <td>
      <p style="padding-bottom: 10px; font-size: 12px; line-height: normal; color: #fff;">Age Limit: 21 & Above | No Security Deposits | Speed Limit: 120 Km/Hr</p>
    </td>
  </tr>

  <tr>
    <td>
    <table class="rtable" style="width:100% !important;" width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
     <tr>
    <td align="left" style="color:#ffffff !important;padding:5px 0px;">Booking Starts @ &#x20B9; <b>60/hr*</b></td>
    <td align="right" style="color:#ffffff !important;padding:5px 0px;">Unlimited Kilometers </td>
  </tr>
  <tr>
    <td align="left"><a href="https://www.mylescars.com" target="_blank" style="color:#fff;font-size:13px;text-decoration:none;">www.mylescars.com</a></td>
    <td align="right"><a href="tel:0888 222 2222" style="color:#fff;font-size:13px;text-decoration:none;" >0888 222 2222</a></td>
  </tr>
</table>

    </td>
  </tr>
  <tr>
    <td style="padding-top:12px;"><hr style="width:100%;border:none;height:1px;background-color:#6c6c6c;margin-top:0;margin-bottom:0;" /></td>
  </tr>
</table>
   </td>
  </tr>
</table>
</td>
</tr>
</table>

</body>
</html>
';
        $dataToMail = array();
        $dataToMail["To"] = $email_id;//'milan.singh@mylescars.com';//$email_id;
		//$dataToMail["cc"] = 'abhishek.singh@mylescars.com';
		//echo '<br/>';
		
        $dataToMail["Subject"] = "Important Notice: Goods and Services Tax (GST) applicable on your Myles booking: ".$bookingid;
        $dataToMail["MailBody"] = $html;
		
		
		

        $Myle->_MYLESSendMail('SendMail', $dataToMail);
        unset($dataToMail);
		
		//exit;
		//ss$db->query("query", $book_update);
		$updatearray = array();
		$whereupdatearray = array();
		$whereupdatearray['id']=$id;
		$updatearray['mail_status']=1;
		$updatearray['bookingurl']= $url;
		echo '<pre>';
		print_r($whereupdatearray);
		print_r($updatearray);
		$up = $db->update("myles_extratax", $updatearray, $whereupdatearray);
		
		
		if($i == 5)
			exit;
	}
}
$db->close();
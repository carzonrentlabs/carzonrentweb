<?php
    if(isset($_POST["order_id"])){
	include_once('./classes/cor.mysql.class.php');

	$dataToSave = array();
	$db = new MySqlConnection(CONNSTRING);
	$db->open();

	$dataToSave["payment_status"] = "2"; //2 means the user has gone to the payment gateway
	$dataToSave["payment_mode"] = "1";
	$dataToSave["payment_start_dt"] = date('Y-m-d H:i:s');

	$whereData = array();
	$whereData["coric"] = $_POST["order_id"];		
	$r = $db->update("cor_booking_new", $dataToSave, $whereData);
	unset($whereData);
	unset($dataToSave);
	$db->close();
    }
?>
<!doctype html>
<!--
Added: Vinod K Maurya
Dated : 03/02/2015 
Description: carzonrent direction map on email confirmation
-->
<script>
/*function checkData()
{
if(document.getElementById("routeStart1").value=="")
{
var msgdat='Please Enter Your Starting point';
document.getElementById("routeStart1").focus();
document.getElementById("routeStart1").value=msgdat;
return false;
}
else
{
return true;
}

} */

<?php
$endcity=$_REQUEST['endcity'];
$useragent=$_SERVER['HTTP_USER_AGENT'];  
if(preg_match('/android|avantgo|samsung|HTC|nokia|iphone|Opera Mini|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))  
header('Location: http://m.carzonrent.com/directionmap.php?endcity='.$endcity);
?>

</script>
    <html>
    <head>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
		<link rel="stylesheet" type="text/css" href="css/routeForm.css" />
     </head>
    <body>
	<div>
        <div class="routeForm_map">
		<div class="logo_cor">
		<a href="http://carzonrent.com" target="_blank"><img src="images/cor_logo_map.png" width="3.5%"></a>
		</div>
		<form id="routeForm" name="routeForm" method="post" action="">
            <label for="routeStart">Starting point:</label><input type="text" id="routeStart1" name="routeStart1" value=""  >
			
            <label for="routeEnd">Choose destination:</label><input type="text" id="routeEnd1" name="routeEnd1" value="<?php  if(isset($_REQUEST['endcity'])!=""){ echo $_REQUEST['endcity'];} ?>" >
            <input type="submit"  name="submit" value="Get Direction">
        <form>
		</div>
        <div class="clear"></div>
        <div id="map_canvas"></div>
        <div id="directionsPanel" style="width:334px;"></div>

		
		<div class="clear"></div>
		</div>
		
		

		
		
<script>


google.maps.event.addDomListener(window, 'load', initialize);
//window.onLoad = initialize();
var map, directionsService, directionsDisplay, geocoder, startLatlng, endLatlng, routeStart, routeEnd, markers = [];

function initialize() {
    var latlng = new google.maps.LatLng(20.9835876,82.7526275);
    routeStart = document.getElementById('routeStart1');
    routeEnd = document.getElementById('routeEnd1');
    geocoder = new google.maps.Geocoder();
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer({
        animation: google.maps.Animation.DROP,
        draggable: true
    });
    var myOptions = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
   directionsDisplay.setPanel(document.getElementById("directionsPanel"));
   
	
    var form = document.getElementById("routeForm");
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        var start = this.elements["routeStart1"].value;
		
        var end = this.elements["routeEnd1"].value;
		
		
		if (start.length && end.length) {
            calcRoute(start, end);
        }
    });

    google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
        var directions = this.getDirections();
        var overview_path = directions.routes[0].overview_path;
        var startingPoint = overview_path[0];
        var destination = overview_path[overview_path.length - 1];
        if (typeof startLatlng === 'undefined' || !startingPoint.equals(startLatlng)) {
            startLatlng = startingPoint;
            getLocationName(startingPoint, function(name) {
                routeStart.value = name;
            });
        }
        if (typeof endLatlng === 'undefined' || !destination.equals(endLatlng)) {
            endLatlng = destination;
            getLocationName(destination, function(name) {
                routeEnd.value = name;
            });
        }
    });
}

function getLocationName(latlng, callback) {
    geocoder.geocode({
        location: latlng
    }, function(result, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var i = -1;
            console.log(result);
            // find the array index of the last object with the locality type
            for (var c = 0; c < result.length; c++) {
                for (var t = 0; t < result[c].types.length; t++) {
                    if (result[c].types[t].search('locality') > -1) {
                        i = c;
                    }
                }
            }
            var locationName = result[i].address_components[0].long_name;
			//alert(locationName);
			
            //callback(locationName);
        }
    });
}

function calcRoute(start, end) {
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}



</script>
<style>
.clear {
                clear: both;
            }
            #map_canvas {
                float: left;
                height: auto !important;
				min-height:700px;
                width: 100%;
            }
            #directionsPanel {
                float: left;
                width: 300px;
				position: absolute;
				top: 47px;
				right:0px;
				z-index:1;
				background:#fff;
            }
			.adp, .adp table{
			margin:0px;
			}
</style>
    </body>
</html>
<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");
	if(isset($_GET["on"]) && isset($_GET["oi"]) && isset($_GET["dn"]) && isset($_GET["di"])){
		$orgName = $_GET["on"];
		$orgID = $_GET["oi"];
		$destName = $_GET["dn"];
		$destID = $_GET["di"];
	}
	else if(isset($_COOKIE["orgName"]) && isset($_COOKIE["orgID"]) && isset($_COOKIE["destName"]) && isset($_COOKIE["destID"])){
		$orgName = $_COOKIE["orgName"];
		$orgID = $_COOKIE["orgID"];
		$destName = $_COOKIE["destName"];
		$destID = $_COOKIE["destID"];
	}
	else {
		header('Location: /');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if(trim($cCity) != "") { ?>
<title>Car Rental/Hire Company - Book a Cab or Rent a Car for <?php echo $cCity; ?></title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers cab or car rental services for <?php echo $cCity; ?>." />
<meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental <?php echo $cCity; ?>, car rental company <?php echo $cCity; ?>, economy car rental services, car hire services <?php echo $cCity; ?>" />
<?php } else { ?>
<title>Car Rental/Hire Company - Book a Cab or Rent a Car for Mumbai, Delhi, Bangalore</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers cab or car rental services for Delhi, Mumbai, Bangalore, Hyderabad, Gurgaon, Noida and  Pune locations." />
<meta name="keywords" content="car rental,book a cab,rent a car, car hire, car rentals, car rental india, car rental company india, economy car rental services, car hire services india" />
<?php } ?>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>


<script>
	$(function(){
		$('select.styled').customSelect();
		$('#slides').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesLocal').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesInt').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
	});

	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
	arrDestination = new Array();
<?php
	for($i = 0; $i < count($des); $i++){
?>
		arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
</script>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css">

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
<script type="text/javascript">
function _setActive(tab)
{
    document.getElementById('brand').className = 'corbrand';
    document.getElementById('TripleTabs').className = '';

    document.getElementById('outstation').className = '';
    document.getElementById('local').className = '';
    document.getElementById('easycabs').className = '';
    document.getElementById('international').className = '';
    document.getElementById('selfdrive').className = '';
    
    document.getElementById('outstationDiv').style.display = 'none';
    document.getElementById('localDiv').style.display = 'none';
    document.getElementById('internationalDiv').style.display = 'none';
    document.getElementById('selfdriveDiv').style.display = 'none';
    
    var tabName = tab;
    document.getElementById(tab).className = 'activeTab';
    document.getElementById(tab + 'Div').style.display = 'block';
    if(tab == "outstation"){
	if(document.getElementById('mtext'))
	    document.getElementById('mtext').innerHTML = "<span>Pay-per-use:</span> Fixed daily charge for retaining the car | Pay as per the kilometers you travel | All taxes included";
	if(document.getElementById('slides'))
	    document.getElementById('slides').style.display = "block";
	if(document.getElementById('slidesLocal'))
	    document.getElementById('slidesLocal').style.display = "none";
	if(document.getElementById('slidesInt'))
	    document.getElementById('slidesInt').style.display = "none";
    }
    else if(tab == "local"){
	if(document.getElementById('mtext'))
	    document.getElementById('mtext').innerHTML = "<span>Pay-per-use:</span> Pay as per the hours and kilometers you travel";
	if(document.getElementById('slides'))
	    document.getElementById('slides').style.display = "none";
	if(document.getElementById('slidesLocal'))
	    document.getElementById('slidesLocal').style.display = "block";
	if(document.getElementById('slidesInt'))
	    document.getElementById('slidesInt').style.display = "none";
    }
    else if(tab == "international"){
	if(document.getElementById('mtext'))
	    document.getElementById('mtext').innerHTML = "<span>Pay-per-use:</span> Pay as per the days you travel";
	if(document.getElementById('slides'))
	    document.getElementById('slides').style.display = "none";
	if(document.getElementById('slidesLocal'))
	    document.getElementById('slidesLocal').style.display = "none";
	if(document.getElementById('slidesInt'))
	    document.getElementById('slidesInt').style.display = "block";
    }
    else if(tab == "selfdrive"){
	if(document.getElementById('mtext'))
	    document.getElementById('mtext').innerHTML = "<span>Pay-per-use:</span> Pay as per the days you travel";
    }
};
</script>
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js" async="true"></script>
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>	
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<div class="banner">
	<div class="main">
    	<div class="lfts">
        <div class="book">Book a Cab for</div>
        
        <!--TripleTabs Start Here-->
        <ul id="typeTab">
            <li>
                <a id="outstation" style="border-right: 1px solid #C5C5C5; border-radius: 4px 4px 0 0 !important;" class="activeTab" onmouseover="Tip('Book a cab for travel to other cities anywhere within India')" onmouseout="UnTip()" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">OUTSTATION</a>
            </li>
            <!--<li>
                <a id="local" href="javascript:void(0)" onmouseover="Tip('Book a cab for holding at your disposal as per your needs')" onmouseout="UnTip()" onclick="javascript: _setActive(this.id)">LOCAL</a>
            </li>
            <li>
                <a id="easycabs" onmouseover="Tip('Book a metered taxi for a local trip on point to point basis')" onmouseout="UnTip()" href="<?php echo ecWebRoot; ?>">EASYCABS</a>
            </li>
	    <li>
                <a id="international" onmouseover="Tip('Book a cab for self-drive at an international destination')" onmouseout="UnTip()" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">INTERNATIONAL</a>
            </li>
	    <li>
                <a id="selfdrive" onmouseover="Tip('Book a cab for self-drive')" onmouseout="UnTip()" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">SELF DRIVE</a>
            </li>-->
            <!--<li class="shade"></li>-->
        </ul>
        <div id="TripleTabs">
		<div id="outstationDiv" class="tber" style="display:block;">
		<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
                <form id="form1" name="form1" action="<?php echo corWebRoot; ?>/search-result.php" method="POST">
		<input type="hidden" name="hdOriginName" id="hdOriginName" value="<?php echo $orgName; ?>" />
		<input type="hidden" name="hdOriginID" id="hdOriginID" value="<?php echo $orgID; ?>" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationName" value="<?php echo $destName; ?>" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationID" value="<?php echo $destID; ?>" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Outstation" />
                    <div class="bdrbtm">
                        <p>
                            <label>I want a cab from</label>
	                        <span style="background: none repeat scroll 0 0 #FFFFFF;border: medium none;color: #9F9F9F;float: left;font-size: 14px;font-weight: bold;height: 28px;line-height: 28px;margin: 3px 0 0; width: 245px;"><?php echo ucwords($orgName); ?></span>
                        </p>
                        <p>
				<label>I want to go to</label>
				<span style="background: none repeat scroll 0 0 #FFFFFF;border: medium none;color: #9F9F9F;float: left;font-size: 14px;font-weight: bold;height: 28px;line-height: 28px;margin: 3px 0 0; width: 245px;"><?php echo ucwords($destName); ?></span>
                        </p>
                    </div>
                    <p>
                        <label>Pickup Date</label>
                        <span class="datepick">
				<input type="text" type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField" class="datepicker" onchange="javascript: _setDropDatePromo('inputField', 'inputField2', 1);" />
                        </span>	
                    </p>
                    <p>
                        <label>Drop Date </label>
	                    <span class="datepick">
	                        <input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo date('d M, Y', strtotime("+1 days")); ?>" id="inputField2" class="datepicker" onchange="javascript: _setCheckPromo('inputField', 'inputField2', 1);" />
	                    </span>
                    </p>
					<p>
                        <label>&nbsp;</label>
                        <span class="mkebtn" style="float:left;">
                            <!--<a href="javascript: void(0);">Make Your Booking Now</a>-->
			    <input type="button" id="btnMakePayment" name="btnMakePayment" value="Make Your Booking Now" onclick="javascript: return _validate();" />
                        </span>
                    </p>
                </form>
            </div>
		<div id="localDiv" class="tber">
                <form id="form2" name="form2" action="<?php echo corWebRoot; ?>/search-result.php" method="POST" onsubmit="javascript: return _validateLocal();">
		<input type="hidden" name="hdOriginName" id="hdOriginNameL" />
		<input type="hidden" name="hdOriginID" id="hdOriginIDL" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationNameL" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationIDL" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Local" />
                    <div class="bdrbtmL">
                        <p>
                            <label>I want a cab from</label>
	                        <span class="location">
	                            <select name="ddlOrigin" id="ddlOriginL" class="styled" onchange="javascript: _setOrigin(this.id, 'l');">
    	                            <option>Select Origin</option>
<?php
		$selCityId = 2;
		$selCityName = "Delhi";
		for($i = 0; $i < count($org); $i++)
		{
			if($org[$i]["CityName"] == $cCity){
				$selCityId = $org[$i]["CityID"];
				$selCityName = $org[$i]["CityName"];
?>
			<option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
			<script type="text/javascript" language="javascript">
				document.getElementById('hdOriginIDL').value = '<?php echo $org[$i]["CityID"]; ?>';
				document.getElementById('hdOriginNameL').value = '<?php echo $org[$i]["CityName"]; ?>';
			</script>
<?php
			}
			else {
?>
			<option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
<?php
			}
		}
?>
                                </select>
                            </span>
                        </p>
		    <p>
                        <label>I want a cab for</label>
                        <span class="cabhours"><select class="styled wdth145" style="width:200px;" name="cabHour" id="cabHour" onchange="javascript: _setHour(this.value);">
    	                <?php
				for($i = 4; $i <= 20; $i++){
			?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?> hours</option>
			<?php
				}
			?>
                        </select></span>
			<span id="selCH" style="margin-left:6px;float:left;" class="showhour"><span id="selCabHour">40 kms included</span></span>
                    </p>
		    </div>
                    <p>
                        <label>Pickup Date</label>
                        <span class="datepick">
                            <input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField5" class="datepicker" />
                        </span>
                    </p>
                    <p>
                        <label>Pickup Time </label>
	                    <span class="pickuphour">
	                        <select class="styled wdth60" name="tHour" id="tHour" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
    	                        <!--<option>Hours</option> -->
					<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 10){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
						<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
					<?php } ?>
                            </select>
                        </span>
                        <span class="pickupmin ml5">
	                        <select class="styled wdth60" name="tMin" id="tMin" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
    	                        <!--<option>Min</option>  -->
								<option value="00">00</option> 
								<option value="15">15</option> 
								<option value="30">30</option>
								<option value="45">45</option>
                            </select>
                        </span>
			<span id="selT" style="margin-left:6px" class="showtime"><span id="seltime"></span></span>
			<script type="text/javascript">
				_setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');
			</script>
                    </p>
                    <p>
                        <label>&nbsp;</label>
                        <span class="mkebtn" style="float:left;">
                            <!--<a href="javascript: void(0);">Make Your Booking Now</a>-->
			    <input type="submit" id="btnMakePayment" name="btnMakePayment" value="Make Your Booking Now" />
                        </span>
                    </p>
                </form>  
            </div>
		<div id="internationalDiv" class="tber">
			<iframe src="http://international.carzonrent.com/?module=2808&language=en&countrycode=IN&currency=INR" width="580px" height="350px" frameborder="0"></iframe>
		</div>
		<div id="selfdriveDiv" class="tber">
			<form id="form3" name="form3" action="<?php echo corWebRoot; ?>/search-result.php" method="POST">
			<input type="hidden" name="hdOriginName" id="hdOriginNameSF" />
			<input type="hidden" name="hdOriginID" id="hdOriginIDSF" />
			<input type="hidden" name="hdDestinationName" id="hdDestinationNameSF" />
			<input type="hidden" name="hdDestinationID" id="hdDestinationIDSF" />
			<input type="hidden" name="hdTourtype" id="hdTourtype" value="Selfdrive" />
			    <div class="bdrbtmL" style="padding-bottom:20px;">
				<p>
				    <label>I want a car in</label>
					<span class="location">
					    <select name="ddlOrigin" id="ddlOriginSF" class="styled" class="styled" onchange="javascript: _setOrigin(this.id, 's');">
					    <option>Select Origin</option>
	<?php
			$selCityId = 2;
			$selCityName = "Delhi";
			$sdOrigin = array(2, 7);
			
			for($i = 0; $i < count($org); $i++)
			{
				if(in_array($org[$i]["CityID"], $sdOrigin)){
					if($org[$i]["CityName"] == $cCity){
						$selCityId = $org[$i]["CityID"];
						$selCityName = $org[$i]["CityName"];
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
					<script type="text/javascript" language="javascript">
						document.getElementById('hdOriginIDSF').value = '<?php echo $org[$i]["CityID"]; ?>';
						document.getElementById('hdOriginNameSF').value = '<?php echo $org[$i]["CityName"]; ?>';
					</script>
	<?php
					}
					else {
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
	<?php
					}
				}
			}
	?>
					</select>
				    </span>
				</p>
				<!--<p>
					<label>I want to go to</label>
					<span class="cities">
						<input type="text" name="txtDestination" id="txtDestinationSF" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationIDSF', 'hdDestinationNameSF', 'autosuggestSF', arrDestination, 0, 0, 0);" />
						<div id="autosuggestSF" class="autosuggest floatingDiv"></div>
					</span>
				</p>-->
			    </div>
			    <p>
				<label>Pickup Date & Time</label>
				<span class="datepick">
				    <input type="text" type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputFieldSF1" class="datepicker" onchange="javascript: _setDropDate('inputFieldSF1', 'inputFieldSF2');" readonly="readonly" />
				</span>
				<span class="pickuphour ml5">
					<select class="styled wdth60" name="tHourP" id="tHourP" onchange="javascript: _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTime');">
						<!--<option>Hours</option> -->
						<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 10){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
							<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
						<?php } ?>
					</select>
				</span>
				<span class="pickupmin ml5">
					<select class="styled wdth60" name="tMinP" id="tMinP" onchange="javascript: _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTime');">
						<!--<option>Min</option>  -->
						<option value="00">00</option> 
						<option value="15">15</option> 
						<option value="30">30</option>
						<option value="45">45</option>
					</select>
				</span>
			    </p>
				<span id="selTP" style="margin:0px 45px 20px 0px; float:right;" class="showtime"><span id="seltimeP"></span></span>
				<script type="text/javascript">
					 _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTime');
				</script>
			    <p>
					<label>Drop Date & Time</label>
					<span class="datepick">
					<input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo date('d M, Y'); ?>" id="inputFieldSF2" class="datepicker" readonly="readonly" />
					</span>
					<span class="pickuphour ml5">
						<select class="styled wdth60" name="tHourD" id="tHourD" onchange="javascript: _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTime');">
							<!--<option>Hours</option> -->
							<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 10){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
								<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
							<?php } ?>
						</select>
					</span>
					<span class="pickupmin ml5">
						<select class="styled wdth60" name="tMinD" id="tMinD" onchange="javascript: _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTime');">
							<option value="00">00</option> 
							<option value="15">15</option> 
							<option value="30">30</option>
							<option value="45">45</option>
						</select>
					</span>
			    </p>
				<span id="selTD" style="margin:0px 45px 20px 0px; float:right;" class="showtime"><span id="seltimeD"></span></span>
				<script type="text/javascript">
					 _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTime');
				</script>
				<p>
				<label>&nbsp;</label>
				<span class="mkebtn" style="float:left;">
				    <!--<a href="javascript: void(0);">Make Your Booking Now</a>-->
				    <input type="button" id="btnMakePaymentSF" name="btnMakePayment" value="Make Your Booking Now" onclick="javascript: return _validateSF();" />
				</span>
			    </p>
			</form>
		</div>
        </div>
	
        <!--TripleTabs Ends Here-->
        <script type="text/javascript">
		var dURL = document.URL;
		if(dURL.indexOf('#') > -1){
			var dURLPart = 	dURL.split('#');
			var tab = "outstation";
			switch(dURLPart[1]){
				case "o":
					tab = "outstation";
				break;
				case "i":
					tab = "international";
				break;
				case "l":
					tab = "local";
				break;
				case "s":
					tab = "selfdrive";
				break;
				default:
					tab = "outstation";
				break;
			}
			_setActive(tab);
		}
	</script>
        </div>
    <div class="rgts">
        <div class="social">
		<table cellpadding="5" cellspacing="0" border="0">
                        <tr>
                            <td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1" type="text/javascript"></script><fb:like href="http://www.facebook.com/carzonrent/" send="false" layout="button_count" width="90" show_faces="false"></fb:like></td>
                            <td style="padding-left:15px;"><a href="https://twitter.com/carzonrentIN" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @carzonrentIN</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
                            <td style="padding-left:15px;"><!-- Place this tag where you want the +1 button to render -->
<g:plusone size="medium" href="<?php echo corWebRoot; ?>"></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></td>
                        </tr>
                </table>
        </div>
        <div class="righttb">
            <div id="slides">
                <div class="slides_container">
                    <!--<div class="slide"><a href="javascript: void(0);"><img src="<?php //echo corWebRoot; ?>/images/carzonrent-1.jpg" alt="One day trip to Agra in sedan at just Rs 4000 - Book now - Carzonrent.com" title="One day trip to Agra in sedan at just Rs 4000 - Book now- Carzonrent.com" width="344px" height="390px" /></a></div>-->
                    <div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/carzonrent-2.jpg" alt="All inclusive pricing - Interstate taxes, service tax, chauffeur allowance and night charges - Carzonrent.com" title="All inclusive pricing - Interstate taxes, service tax, chauffeur allowance and night charges - Carzonrent.com" width="344px" height="390px" /></a></div>
                    <!--<div class="slide"><a href="javascript: void(0);"><img src="<?php //echo corWebRoot; ?>/images/carzonrent-3.jpg" alt="Don't pay for untravelled kms - Use our pay per use pricing - Carzonrent.com" title="Don't pay for untravelled kms - Use our pay per use pricing - Carzonrent.com" width="344px" height="390px" /></a></div>-->
		    <div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/carzonrent-4.jpg" alt="Launch offer - 20% discount on all outstation travel - Book now - Carzonrent.com" title="Launch offer - 20% discount on all outstation travel - Book now - Carzonrent.com" width="344px" height="390px" /></a></div>
                </div>
            </div>
	    <div id="slidesLocal" style="display:none">
                <div class="slides_container">
                    <div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/carzonrent-local-1.jpg" alt="One day trip to Agra in sedan at just Rs 4000 - Book now - Carzonrent.com" title="One day trip to Agra in sedan at just Rs 4000 - Book now- Carzonrent.com" width="344px" height="390px" /></a></div>
                    <div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/carzonrent-local-2.jpg" alt="All inclusive pricing - Interstate taxes, service tax, chauffeur allowance and night charges - Carzonrent.com" title="All inclusive pricing - Interstate taxes, service tax, chauffeur allowance and night charges - Carzonrent.com" width="344px" height="390px" /></a></div>
                </div>
            </div>
	    <div id="slidesInt" style="display:none">
                <div class="slides_container">
                    <div class="slide"><a href="javascript: void(0);"><img src="<?php echo corWebRoot; ?>/images/carzonrent-int-1.jpg" alt="One day trip to Agra in sedan at just Rs 4000 - Book now - Carzonrent.com" title="One day trip to Agra in sedan at just Rs 4000 - Book now- Carzonrent.com" width="344px" height="390px" /></a></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!--Middle Start Here-->
<?php include_once("./includes/middle.php"); ?>
<?php
if(trim($cCity) != ""){
	$xmlDoc = new DOMDocument();
	$xmlDoc->load("./xml/city-desc/" . strtolower($cCity) . ".xml");
	$itemArray = $xmlDoc->getElementsByTagName("city");
	$optText = "";
	foreach($itemArray as $item){
		$txt = $item->getElementsByTagName("citydesc");
		$optText = $txt->item(0)->nodeValue;
	}
	$xmlDoc = null;
?>
	<div class="about-city" style="float:left;">
		<div class="main">
			<div class="city-desc">			
				<h2>About <?php echo $cCity; ?></h2><br />
			<?php if($optText != "") { ?>
				<?php echo $optText ?>
			<?php } ?>
			</div>
		</div>
	</div>
<?php
}
?>
<!--footer Starts Here-->
<?php include_once("./includes/footer.php"); ?>
<script type="text/javascript">
	 var odd = document.getElementById('inputField2').value;
</script>
<!--footer Ends Here-->
</body>
</html>

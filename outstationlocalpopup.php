<?php
session_start();
include_once("./includes/cache-func.php");
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.mysql.class.php');
$db = new MySqlConnection(CONNSTRING);
$db->open();
 $cor = new COR();
 $returnval = 0;
if (isset($_POST["email"])) {
    $dataToSave = array();

    $dataToSave["name"] = $_REQUEST['name'];
    $dataToSave["mobile"] = $_REQUEST['mobile'];
    $dataToSave["email_id"] = $_REQUEST['email'];
    $dataToSave["query"] = $_REQUEST['query'];
    $dataToSave['service_type'] = $_REQUEST['service_type'];
    $dataToSave['entry_date'] = date("Y-m-d H:i:s");
    
    $r = $db->insert("myles_enquiry", $dataToSave);
    $_SESSION['lastinsertId'] = $r['lastinsertId'];
    
    ///////////////////// mail /////////////////////
    
    
     $html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
	    $html .= "<tr>";
	    $html .= "<td>Mobile</td>";
	    $html .= "<td>" . $dataToSave["mobile"] . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Name</td>";
	    $html .= "<td>" . $dataToSave["name"] . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Email</td>";
	    $html .= "<td>" . $dataToSave["email_id"] . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Query</td>";
	    $html .= "<td>" . $dataToSave["query"] . "</td>";
	    $html .= "</tr>";
        $html .= "</table>";
        
        $dataToMail= array();

        $dataToMail["To"] = "shiv.kumar@carzonrent.com,trips@carzonrent.com";
	//$dataToMail["To"] = "binary.abhishek@carzonrent.com";
        $dataToMail["Subject"] = "COR Retail Enquiry";
        $dataToMail["MailBody"] = $html;
        
        $res = $cor->_CORSendMail($dataToMail);
    
    
    ////////////////////////////
    
    $returnval = 1;
    unset($dataToSave);
}
$db->close();
echo  $returnval;
die;
?>
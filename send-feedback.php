<?php
    if(isset($_POST["monumber"])){
	include_once("./includes/cache-func.php");
        include_once('./classes/cor.ws.class.php');
        $cor = new COR();
	
        $html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
        $html .= "<tr>";
        $html .= "<td>Booking ID</td>";
        if(isset($_POST["bookingid"]))
        $html .= "<td>" . $_POST["bookingid"] . "</td>";
        else
        $html .= "<td>NO</td>";
        $html .= "</tr>";
        if($_POST["enc"] == "1"){
	    $html .= "<tr>";
	    $html .= "<td>Mobile</td>";
	    $html .= "<td>" . $cor->_decrypt($_POST["monumber"]) . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Name</td>";
	    $html .= "<td>" . $cor->_decrypt($_POST["txtname"]) . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Email</td>";
	    $html .= "<td>" . $cor->_decrypt($_POST["email"]) . "</td>";
	    $html .= "</tr>";
	} else {
	    $html .= "<tr>";
	    $html .= "<td>Mobile</td>";
	    $html .= "<td>" . $_POST["monumber"] . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Name</td>";
	    $html .= "<td>" . $_POST["txtname"] . "</td>";
	    $html .= "</tr>";
	    $html .= "<tr>";
	    $html .= "<td>Email</td>";
	    $html .= "<td>" . $_POST["email"] . "</td>";
	    $html .= "</tr>";
	}
        $html .= "<tr>";
        $html .= "<td>Was the booking procedure convenient?</td>";
        if($_POST["rdo1"] == "1")
        $html .= "<td>Yes</td>";
        elseif($_POST["rdo1"] == "2")
        $html .= "<td>No</td>";
        $html .= "</tr>";
	
        if($_POST["isd"] == "0"){
            $html .= "<tr>";
            $html .= "<td>Did the cab report on time?</td>";
            if($_POST["rdo2"] == "1")
            $html .= "<td>Yes</td>";
            elseif($_POST["rdo2"] == "2")
            $html .= "<td>No</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Were you happy with your experience with the chauffeur?</td>";
            if($_POST["rdo3"] == "1")
            $html .= "<td>Yes</td>";
            elseif($_POST["rdo3"] == "2")
            $html .= "<td>No</td>";
            $html .= "</tr>";
        }
	
        $html .= "<tr>";
        $html .= "<td>Did the car meet your expectations?</td>";
        if($_POST["rdo4"] == "1")
        $html .= "<td>Yes</td>";
        elseif($_POST["rdo4"] == "2")
        $html .= "<td>No</td>";
        $html .= "</tr>";
        $html .= "<tr>";
	if($_POST["isd"] == "0")
	$html .= "<td>Were you satisfied with your overall experience with us?</td>";
	else
        $html .= "<td>Hope you had a wonderful experience with Myles?</td>";
        if($_POST["rdo5"] == "1")
        $html .= "<td>Yes</td>";
        elseif($_POST["rdo5"] == "2")
        $html .= "<td>No</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td colspan=\"2\">Details experience with COR</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td colspan=\"2\">" . $_POST["experience"] . "</td>";
        $html .= "</tr>";
        $html .= "</table>";
        
        $dataToMail= array();
        
        $dataToMail["To"] = "crm@carzonrent.com;ajay.anand@carzonrent.com;abhishek.bhaskar@carzonrent.com";
	
        $dataToMail["Subject"] = "Customer Feedback - Carzonrent";
        $dataToMail["MailBody"] = $html;
        
        $res = $cor->_CORSendMail($dataToMail);
	unset($dataToMail);
        unset($cor);
        echo "1";
        //header("Location: ./feedback.php?res=s");
    }
?>
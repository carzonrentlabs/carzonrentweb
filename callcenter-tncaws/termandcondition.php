<script type="text/javascript" src="<?php echo corWebRoot; ?>/callcenter-tncaws/ajax.js"></script>
<div class="aboutus" style="display:none;">
<a class="closetnp" onclick="close_popup();" style="float: right;cursor:pointer">X</a>
<img src="<?php echo corWebRoot; ?>/images/32.gif" id="loading_image" style="display:none;float: right;"/>
<input class="tc_input" placeholder="Please Enter Name." name="chatemail" id='chatemail' type="text" onkeyup="$('.m3').html('');$('#chatemail').css('background-color','');" 
value='<?PHP if(isset($bookingDetails[0]['Fname'])) { echo ucfirst($bookingDetails[0]['Fname']).' '.ucfirst($bookingDetails[0]["Lname"]);}?>' disabled>

<input class="tc_input" placeholder="" name="entrydate" id='entrydate' type="text" value="<?PHP echo date("Y-m-d h:i:sa");?>" disabled>
<input class="tc_input" placeholder="" name="coricid" id='coricid' type="hidden" value="<?PHP echo $bookingDetails[0]["trackID"];?>">

<input class="tc_input" placeholder="" name="hideemail" id='hideemail' type="hidden" value="<?PHP echo $bookingDetails[0]["EmailID"];?>">
<input class="tc_input" placeholder="" name="hidefanme" id='hidefanme' type="hidden" value="<?PHP if(isset($bookingDetails[0]['Fname'])) { echo ucfirst($bookingDetails[0]['Fname']);}?>">
<input class="tc_input" placeholder="" name="hidelname" id='hidelname' type="hidden" value="<?PHP if(isset($bookingDetails[0]['Lname'])) { echo ucfirst($bookingDetails[0]['Lname']);}?>">
<input class="tc_input" placeholder="" name="hidemobile" id='hidemobile' type="hidden" value="<?PHP echo $bookingDetails[0]["mobile"];?>"><input class="tc_input" placeholder="" name="hidedestination" id='hidedestination' type="hidden" value="<?PHP echo $bookingDetails[0]["destination"];?>">
<input class="tc_input" placeholder="" name="checkbtn" id='checkbtn' type="hidden" value="0">
<input class="tc_input" placeholder="" name="hidebookingid" id='hidebookingid' type="hidden" value="<?PHP echo $_GET['bookingid'];?>">
<div class="tabcontent agreement">
<p>This CAR RENTAL AGREEMENT (“Agreement”) is made and entered on this <?PHP echo date("l"); ?> of <?PHP echo date('d-M') ?>, <?PHP echo date('Y');?> (“Effective Date”) at <?PHP echo $bookingDetails[0]["destination"];?>.</p>
<div class="term_head">By and between</div>
<p><strong>CARZONRENT (INDIA) PRIVATE LIMITED,</strong> a private limited company incorporated under the provisions of the Companies Act 1956, having its Registered Office at 9th Floor Videocon Tower, E-1 Block, Jhandewalan Extension, New Delhi 110055, (hereinafter referred as the <strong>“Company”</strong> and which term shall unless excluded by or repugnant to the context shall mean and include its successors, administrators and assignees etc. ) of the FIRST PART;  </p>
<div class="term_head">AND</div>
<p><span style="font-weight:bold;"><?PHP echo ucfirst($bookingDetails[0]["Fname"]).' '.ucfirst($bookingDetails[0]["Lname"]);?></span> Phone no : <span style="font-weight:bold;"><?PHP echo $bookingDetails[0]["mobile"];?></span> ,Email Address: <span style="font-weight:bold;"><?PHP echo $bookingDetails[0]["EmailID"];?></span> (hereinafter referred as the “User” and which term shall unless excluded by or repugnant to the context shall mean and include his/her legal heirs and representatives etc. ) of the SECOND PART;
</p>
<p>The Company and the User may be collectively referred to as the <strong>‘Parties’</strong> and individually as <strong>‘Party’.</strong></p>
<div class="term_head">WHEREAS</div>
<ol style="margin-bottom:0px;">
<li>The Company is, inter alia, engaged in the business of providing passenger transportation in Vehicles, including providing self driven cars, on rental basis.</li>
<li>The User wishes to hire the self driven cars of the Company <strong>(“Vehicles”)</strong> on the rental basis and has therefore approached the Company for the purposes of availing the services of the Company of renting out self driven cars, from time to time, as described in the invoice, for a particular duration and for plying the Vehicle within the territory as per details mentioned herein. The Vehicles that may be rented to the User are available for renting on the web portal of the company.</li>
<li>The Company has agreed to let and the User has agreed to take on hire the Vehicle, as may be chosen from the available Vehicles on the web portal of the company, on and subject to the mutually agreed terms and conditions appearing hereinafter.</li>
</ol>
<a href="http://www.carzonrent.com/agreement.php" class="agree_tc" target="_blank">More...</a>
</div>
</div>
<?php
session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once("../includes/cache-func.php");
include_once('../classes/cor.mysql.class.php');
require('../callcenter-tncaws/pdf/fpdf.php');
require '../callcenter-tncaws/test/vendor/autoload.php';

if (isset($_POST["emailId"])) {

    $dataToSave = array();
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $dataToSave["name"] = $_REQUEST['emailId'];
    $dataToSave["entrydate"] = $_REQUEST['entrydate'];
    $dataToSave["source"] = 'Call Center';
    $dataToSave["coric"] = $_REQUEST['coric'];
    $r = $db->insert("cor_accept_tnc", $dataToSave);
    $_SESSION['lastinsertId'] = $r['lastinsertId'];
    unset($dataToSave);
    $db->close();

    class PDF extends FPDF {

// Page header
        function Header() {
            // Logo
            $this->Image('../callcenter-tncaws/pdf/COR-logo.gif', 10, 6, 30);
            // Arial bold 15
            $this->SetFont('Arial', 'B', 15);
            // Move to the right
            $this->Cell(80);
            // Title
            $this->Cell(60, 10, 'User Agreement', 1, 0, 'C');
            // Line break
            $this->Ln(20);
        }

// Page footer
        function Footer() {
            // Position at 1.5 cm from bottom
            $this->SetY(-15);
            // Arial italic 8
            $this->SetFont('Arial', 'I', 8);
            // Page number
            $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
        }

    }

// Instanciation of inherited class
    $pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetFont('Times', '', 8);
    $text1 = 'This CAR RENTAL AGREEMENT ("Agreement") is made and entered on this ' . date('l') . ' of ' . date('M') . ',' . date('Y') . ' ("Effective Date") at ' . $_POST['destination'] . '';
    $text2 = 'By and between';
    $text3 = 'CARZONRENT (INDIA) PRIVATE LIMITED, a private limited company incorporated under the provisions of the Companies Act 1956, having its Registered Office at 9th Floor Videocon Tower, E-1 Block, Jhandewalan Extension, New Delhi 110055,(hereinafter referred as the "Company" and which term shall unless excluded by or repugnant to the context shall mean and include its successors, administrators and assignees etc. ) of the FIRST PART;';
    $text4 = 'AND';
    $text5 = '' . ucfirst($_POST['fname'] . ' ' . $_POST['lname']) . ' Phone no : ' . $_POST['mobile'] . ' ,Email Address: ' . $_POST['email'] . ' (hereinafter referred as the "User" and which term shall unless excluded by or repugnant to the context shall mean and include his/her legal heirs and representatives etc. ) of the SECOND PART;
The Company and the User may be collectively referred to as the "Parties" and individually as "Party".';
    $pdf->Write(5, $text1, 0, 2);
    $pdf->ln();
    $pdf->Write(5, $text2, 0, 2);
    $pdf->ln();
    $pdf->Write(5, $text3, 0, 2);
    $pdf->ln();
    $pdf->Write(5, $text4, 0, 2);
    $pdf->ln();
    $pdf->Write(5, $text5, 0, 2);
    $pdf->ln();
    $pdf->Cell(0, 2, ucfirst($_REQUEST['emailId']), 0, 1);
    $pdf->ln();
    $pdf->Cell(0, 2, 'Booking Id-' . $_POST['bookingid'], 0, 1);
    $pdf->ln();
    $pdf->Cell(0, 2, 'Accept Date And time-' . $_REQUEST['entrydate'], 0, 1);

    $file1 = $_REQUEST['coric'] . '-' . date("d-m-Y-h-m-s") . '.pdf';
    $filepathwithname = $_SERVER['DOCUMENT_ROOT'] . '/callcenter-tncaws/agreementdoc/' . $file1;


    $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/callcenter-tncaws/agreementdoc/' . $file1, 'F');
//$pdf->Output('../callcenter-tncaws/agreementdoc/'.$_REQUEST['emailId'].'-'.date("d-m-Y-h-m-s").'-'.$_POST['bookingid'].'.pdf','F');
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $dataToSave = array();
    $dataToSave["tncpdf"] = $_REQUEST['coric'] . '-' . date("d-m-Y-h-m-s") . '.pdf';  // Added by Iqbal on 28Aug2013
    $whereData = array();
    $whereData["id"] = $_SESSION['lastinsertId'];
    $r = $db->update("cor_accept_tnc", $dataToSave, $whereData);
    $db->close();
    unset($whereData);
    unset($dataToSave);
    unset($_SESSION['lastinsertId']);
// $bucket = 'uploadmyles';
//    $keyname = $file1;
//					
//    $filepath = $filepathwithname;
//
//
 $bucket = 'uploadmyles';
    $keyname = $file1;
					
    $filepath = $filepathwithname;


    $s3 = new Aws\S3\S3Client(array(
        'credentials' => array(
            'key' => 'AKIAI5XMVA6ULCZIUR5A',
            'secret' => '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',
        ),
        'region' => 'ap-southeast-1',
        'version' => 'latest' , 'debug' => true
    ));
//
//
////echo '<pre>';
////print_r($s3);
//// Upload a file.
//
    try {
        $result = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => 'test/' . basename($filepath),
            'SourceFile' => $filepath,
            'ContentType' => 'application/pdf',
            'Content-Length'=>filesize($filepath),
            'ACL' => 'public-read',
            'headers' => array(// raw headers
                'Cache-Control' => 'max-age',
                //'Content-Encoding' => 'gzip',
                'Content-Language' => 'en-US'
            //'Expires'       => 'Thu, 01 Nov 2012 16:00:00 GMT'
            ),
            'Metadata' => array(
                'profile' => $filepath,
            ),
        ));
        print_r($result);
    } catch (S3Exception $e) {
        echo $e->getMessage() . "\n";
    }
} 
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Offer on Myles</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/cor.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/tab.css">
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" style="width:100%" >
<div class="main">
<img class="myles_img" width="15%" src="images/myles.png" border="0"/>
</div>
	<div class="myles_head">
		<div class="magindiv">
			<h2 class="heading2">Myles Offers </h2>
		</div>
	</div>
<div class="mainW960">
<!-- Term & Condition start -->
<br>
<br>
<div class="w_25 f_l mr2">
<ul class="nav nav-tabs myles_nav">
<li class="active">
<a href="#tab1">June3for2 Offer</a>
</li>
<li>
<a href="#tab2">Father’s Day Offer</a>
</li>
<li>
<a href="#tab3">Myles Tech Hub Offer</a>
</li>
</ul>
</div>
<div class="w_73 f_l">
<div class="cor_nav">
<div id="tab1" class="tab-content active">
<div class="myles_list">
<strong>How to avail / book</strong>
<ol>
<li>Log on to <a href="http://www.carzonrent.com/" target="_blank">www.carzonrent.com</a>. Select the preferred city, date/time, car & the location.</li>
<li>Post filling in the details, enter the discount code shared with you in the “Discount/Promotion Code”. After entering the code, click on Apply next to the Discount/Promotion Code box.</li>
<li>Proceed with the booking & pay the balance amount through credit card/debit card/net banking.</li>
</ol>
<br>
<p>Know more about the service at <a href="http://www.mylescar.com/" target="_blank">www.mylescar.com</a></p>
<br>
<strong>Terms and conditions:</strong>
<br>
<ol>
<li>Offer: Book for 3 days and pay for only 2 days.</li>
<li>Applicable on bookings made through Myles App/Website/Wap site.</li>
<li>Apply the coupon code before making the reservation.</li>
<li>Applicable on all car categories and in all cities of operations.</li>
<li>Only one offer is applicable on a booking at a time.</li>
<li>Cars will be provided subject to availability.</li>
<li>The company reserves the right to change, suspend, or discontinue the offer at any time for any reason, without any prior notice.</li>
<li>Coupon code applicable only once.</li>
<li>Validity: Pick up date should be on or before 30<sup>th</sup> June 2015.</li>
</ol>
</div>
</div>
<div id="tab2" class="tab-content hide">
<div class="myles_list">
<strong>How to avail / book</strong>
<ol>
<li>Log on to <a href="http://www.carzonrent.com/" target="_blank">www.carzonrent.com</a>. Select the preferred city, date/time, car & the location.</li>
<li>Post filling in the details, enter the discount code shared with you in the “Discount/Promotion Code”. After entering the code, click on Apply next to the Discount/Promotion Code box.</li>
<li>Proceed with the booking & pay the balance amount through credit card/debit card/net banking.</li>
</ol>
<br>
<p>Know more about the service at <a href="http://www.mylescar.com/" target="_blank">www.mylescar.com</a></p>
<br>
<strong>Winner Entitlements: Mercedes for a Day.</strong>
<br>
<br>
<strong>Terms and conditions:</strong>
<br>
<ol>
<li>Code is applicable for winner of the Father’s Day contest only.</li>
<li>Valid only on the self-drive service: Myles.</li>
<li>Valid in all available cities where car is available.</li>
<li>Booking subject to car availability. Prior reservation is advised (72 hrs in advance).</li>
<li>Standing instructions/ Pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Valid for booking until 21<sup>st</sup> July’ 15 only.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ol>
</div>
</div>
<div id="tab3" class="tab-content hide">
<div class="myles_list">
<strong>How to avail / book</strong>
<ol>
<li>Log on to <a href="http://www.carzonrent.com/" target="_blank">www.carzonrent.com.</a> Select the preferred city, date/time, car & the location.</li>
<li>Post filling in the details, enter the discount code shared with you in the “Discount/Promotion Code”. After entering the code, click on Apply next to the Discount/Promotion Code box.</li>
<li>Proceed with the booking & pay the balance amount through credit card/debit card/net banking.</li>
</ol>
<br>
<p>Know more about the service at <a href="http://www.mylescar.com/" target="_blank">www.mylescar.com</a></p>
<br>
<strong>Winner Entitlements</strong>
<br>
<br>
<strong>Terms and conditions:</strong>
<br>
<ol>
<li>Offer valid for Tech Hub Employees only.</li>
<li>Code is applicable on a minimum booking of Rs 2000.</li>
<li>Valid for all Myles Self Drive Cars and Cities.</li>
<li>Booking subject to availability.</li>
<li>Valid for reservation made till 31st December 2015.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>Standing instruction / Pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Valid for booking until 21st July’ 15 only.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ol>
</div>
</div>
<br>
<br>
</div>
</div>
<!-- Term & Condition start -->	
</div>
</div>


<?php include_once("./includes/footer.php"); ?>
</body>
</html>

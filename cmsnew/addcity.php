<?php 
if(!ob_start("ob_gzhandler")) ob_start();  session_start(); 
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
$db->sess_expire();
if(!isset($_SESSION['userid'])){header("Location:$base_url/cms_admin.php");}
// $ebits = ini_get('error_reporting');
// error_reporting($ebits ^ ~E_NOTICE);
// error_reporting(0);
 ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<?php //include 'cms_includes/header.php';  ?>
<?php
$mode=$_GET['mode'];
switch ($mode){
	case 'addcity':
	{
	      add_city();	  
			break;
	}		
	case 'editcity':
	{
		 edit_city();
		break;
	}
	default:
	echo 'something went wrong';


}


?>
<style>.c_type{width: 125px;border: 1px solid rgb(214, 207, 207);box-shadow: 3px 4px 5px 2px whitesmoke;}</style>
<script src="<?php echo $base_url; ?>/ckeditor/ckeditor.js"></script>
<body>
<?php function add_city(){
   global $db;
   global $base_url;
   global $base_url1;
?>

<table width="100%" align="center" cellspacing="0px" cellpadding="0px" style="background-color:#FFF;" >
		<tr>
		<td colspan="2" cellspacing="0px">
		<?php  include("cms_includes/header.php"); ?>
		</td>
		</tr>
		<tr>
		<td class="widthtbs">
		
		<?php include("cms_includes/leftpanel.php"); ?>
		
		
		</td>
		<td style="margin-right:40px">
			<section class="container">
    <div class='login mtop' style='width:636px'>
      <h1>Carzonrent</h1>
      <form name='example_form' method="post" action="" id='addcity'>
        <p style='text-align:center'><input type="text" name="c_name" id='c_name' value="" placeholder="type city name here" style='width:152px'><img src='<?php echo $base_url; ?>/cms_images/ajax-loader.gif'/ style='display:none'>
		<div id='city_error' style='text-align: center;color: red;display:none;'>city already exist</div>
		</p>
        <p style='text-align:center'><select name='c_type' class='c_type' id='c_type' onchange='city_check(this.id)'><option value='-1'>Select Type</option><option value='0'>From</option><option value='1'>To</option></select></p>
        <p style='text-align:center'><div>Description</div><textarea id='desc' name='c_desc' placeholder='type city description' rows='7' cols='35' class='ckeditor'></textarea></p>
		
        <p  style='text-align:center' class="submit"><input type="submit" name="c_add" value="Add"></p>
      </form> 
       <div id='c_fill'style='text-align:center;color:red;display:none'>please fill above field</div>
    </div>
	<div style='float:right;margin-top:50px;' ><a href='<?php echo $base_url1; ?>/showcity.php'><div class="back">Back</div></a><div>
	<div style='float:right;display:none' id='show_city' ><a href='<?php echo $base_url1; ?>/showcity.php/'>show City</a><div>
  
</section>
		</td>
		</tr>


		<tr>
		<td colspan="2" style="margin-top:0px;">
		<?php  include("cms_includes/footer.php"); ?>
		</td>
		</tr>

		</td>
		</tr>

		</table>
	


<?php  }?>

<?php 
 function edit_city() 
{ global $db;
  global $base_url;
  global $base_url1;
 
$id=$_GET['id'];
$city_id=$_GET['cityid'];
// for city
$query=$db->query("select * from city where id=$city_id");
$city=$db->loadrows();
// for description
$desc=$db->query("select * from city_description where id='".$id."' and city_id='".$city_id."'");
$citydesc=$db->loadrows();

?>


<table width="100%" align="center" cellspacing="0px" cellpadding="0px" style="background-color:#FFF;" >
		<tr>
		<td colspan="2" cellspacing="0px">
		<?php  include("cms_includes/header.php"); ?>
		</td>
		</tr>
		<tr>
		<td class="widthtbs">
		
		<?php include("cms_includes/leftpanel.php"); ?>
		
		
		</td>
		<td style="margin-right:40px">
			<section class="container">
    <div class='login mtop' style='width:636px'>
      <h1>Carzonrent</h1>
      <form name='' method="post" action="" id='editcity'>
	  <input type='hidden' value='<?php echo $id;?>' name='c_id' id='c_id'/>
	  <input type='hidden' value='<?php echo $city_id;?>' name='city_id' id='city_id'/>
        <p style='text-align:center'><input type="text" name="c_name" id='c_name' value="<?php echo $city[0]['name'];?>" placeholder="type city name here" style='width:152px;background-color:ghostwhite;' ><img src='<?php echo $base_url; ?>/cms_images/ajax-loader.gif'/ style='display:none'>
		<div id='city_error' style='text-align: center;color: red;display:none;'>city already exist with the type you specified</div>
		</p>
        <p style='text-align:center'><select name='c_type' class='c_type' id='c_type'  onchange='city_edit_check(this.id)' ><option value='-1'>Select Type</option><option value='0' <?php  echo $select=($city[0]['type']==0)?'selected':''; ?> >From</option><option value='1' <?php  echo $select=($city[0]['type']==1)?'selected':''; ?> >To</option></select></p>
        <p style='text-align:center'><div>Description1:</div><textarea id='desc'  name='c_desc' placeholder='type city description' rows='7' cols='35'  ><?php echo $citydesc[0]['city_description'];?></textarea></p>
		
		
        <p  style='text-align:center' class="submit"><input type="submit" name="c_edit" value="Update"></p>
      </form> 
       <div id='c_fill'style='text-align:center;color:red;display:none'>please fill above field</div>
    </div>
	<div style='float:right;margin-top:50px;'  ><a href='<?php echo $base_url1; ?>/citydescription.php'><div class="back">Back</div></a></div>
	<div style='float:right;display:none' id='show_city' ><a href='<?php echo $base_url; ?>/showcity.php'>show City</a></div>
  
</section>
		</td>
		</tr>


		<tr>
		<td colspan="2" style="margin-top:0px;">
		<?php  include("cms_includes/footer.php"); ?>
		</td>
		</tr>

		</td>
		</tr>

		</table>
	


<?php
} 
 
?>

 <script>
// var validator = new FormValidator('example_form', [{
    // name: 'c_name',
    // display: 'required',    
    // rules: 'required'
// }, {
    // name: 'c_type',
    // rules: 'alpha_numeric'
// }, {
    // name: 'c_desc',
    // rules: 'required'
// }], function(errors, event) {
    // if (errors.length > 0) {
        //Show the errors
    // }
// });
// 
</script>
</body>
<script type="text/javascript">

	CKEDITOR.replace('c_desc');

</script>
<script>
global=0;
//already exist city  name check
function city_check(element)
{  var name=$('#c_name').val();
   var type=$('#'+element).val();
   //$('#c_name').next().show();
   $.post('<?php echo $base_url; ?>/ajax.php?mode=city_check',{city_name:name,city_type:type},function(data){
   if(data==1)
   { 
   //$('#c_name').next().hide();
   //$('#city_error').fadeIn(200,function(){$('#city_error').fadeOut(3000);});
   //$('#c_type').prop('selectedIndex',0);
   //global=1;
   }
   else
   {
  // $('#c_name').next().hide();
   //$('#city_error').fadeOut(500);
  
   global=0;
   }
  });

}

//check city and type when edit
function city_edit_check(element)
{  var name=$('#c_name').val();
   var type=$('#'+element).val();
   var city_id=$('#c_id').val();
   $('#c_name').next().show();
  $.post('<?php echo $base_url; ?>/ajax.php?mode=city_edit_check',{city_name:name,city_type:type,id:city_id},function(data){
   if(data==1)
   { $('#c_name').next().hide();
  $('#city_error').fadeIn(200,function(){$('#city_error').fadeOut(3000);});
   $('#c_type').prop('selectedIndex',0);
  global=1;
   }
   else
   {$('#c_name').next().hide();
   $('#city_error').fadeOut(500);
  
   global=0;
   }
  });

}

$(document).ready(function(){
//city add using ajax

$('#addcity').submit(function(){
$('textarea#desc').val(CKEDITOR.instances.desc.getData()); 

if(global==0){
   regex=/^[a-zA-Z -]+$/;
	
	if(regex.test($('#c_name').val())==false)
  {
   alert('only character allowed in city name place');
   return false;
  }
  //|| $('#desc').val()==''
  if($('#c_name').val()=='' || $('#c_type').val()=='-1' )
 {
 alert('please fill all the field first')
 return false;
 }
  
 else{
 
$.post('<?php echo $base_url;?>/ajax.php?mode=addcity',$(this).serialize(),function(data){

 
 if(data==1)
 {
  
 alert('city is added');
 $('#show_city').show();
 $('#addcity').get(0).reset();
 window.location="<?php echo $base_url; ?>/contentmenu.php";
 
 }
 else
 {
  alert('somthing went wrong');
 }

}); 
return false;
}
}
else
{
//$('#c_fill').fadeIn(4000,function(){$(this).fadeOut(500);});
//return false;
}
});
//city edited using ajax
$('#editcity').submit(function(){
 $('textarea#desc').val(CKEDITOR.instances.desc.getData());
if(global==0){
	 regex=/^[a-zA-Z -]+$/;
	
  if(regex.test($('#c_name').val())==false)
  {
	alert('only character allowed in city name place');
    return false;
  }
  //|| $('#desc').val()==''
 if($('#c_name').val()=='' || $('#c_type').val()=='-1'   )
 {
 alert('please fill all the field first')
 return false
 }
 else{
   
$.post('<?php echo $base_url;?>/ajax.php?mode=editcity',$(this).serialize(),function(data){

 if(data==1)
 { 
 alert('city is updated');
 $('#show_city').show();
 window.location='<?php echo $base_url1; ?>/citydescription.php';
 
 }
 else
 {
  alert('somthing went wrong');
 }

}); 
return false;
}
}
else
{
$('#c_fill').fadeIn(4000,function(){$(this).fadeOut(500);});
return false;
}
});



});
</script>
</html>
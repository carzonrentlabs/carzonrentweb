<?php
global $base_url;
global $base_url1;
global $db;
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
if(!ob_start("ob_gzhandler")) ob_start();  session_start(); 
$db->sess_expire();
if(!isset($_SESSION['userid'])){header("Location:$base_url1/cms_admin.php");}
?>	
<style>.c_type{width: 125px;border: 1px solid rgb(214, 207, 207);box-shadow: 3px 4px 5px 2px whitesmoke;}</style>
<script src="<?php echo $base_url; ?>/ckeditor/ckeditor.js"></script>
	<script language="javascript">
function validateVal(theForm) 
{
	var reason = "";
	reason += validateEmpty(theForm.cityname);
	//var descval= CKEDITOR.instances.citydescVal.getData() == '';
	var descval = CKEDITOR.instances.citydescVal.document.getBody().getChild(0).getText() ;
	
	if(descval=='')
	{
	reason +="The required field has not been filled in";
	}

	if (reason != "") {
	alert("Some fields need correction:\n" + reason);
	return false;
	}

  return true;
}


function validateEmpty(fld) {
    var error = "";
 
    if (fld.value.length == 0) {
        fld.style.background = 'red'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;  
}


</script>

<?php
if(isset($_POST['submit']))
{
$filename=trim($_REQUEST['cityname']);
$cityDesc=$_REQUEST['citydesc'];
$exisstfilename='../xml/city-desc/city_sub_desc/'.$filename.".xml";
$dir    = '../xml/city-desc/city_sub_desc';
	if (is_dir($dir)) 
	{
		if ($dh = opendir($dir)) 
		{
			while (($file = readdir($dh)) !== false) 
			{
				if($file != "." && $file != "..") 
				{
				$val=explode(".",$file);
				$dirfile=$mainfile=$val[0]."\n";
				
				  if (file_exists($exisstfilename)) 
				  {
						
						$msg="City already exist Please enter other city";
						header("Location:addcitydescription.php?msg=".$msg);
						exit;
				  }
				  else
				  {
				  
						    $dir    = $root_url.'/xml/city-desc/city_sub_desc/'.$filename;
							$completeurl = '../xml/city-desc/city_sub_desc/'.$filename.".xml";
							
							$xmlDoc = new DOMDocument('1.0', 'iso-8859-1');
							$xmlDoc->formatOutput = true;
							$root = $xmlDoc->appendChild($xmlDoc->createElement( "city" )); 
							$sxe = simplexml_import_dom( $xmlDoc ); 
							$sxe->addAttribute("name", $filename); 
							$sxe->addChild("citydesc",$cityDesc); 
							$sxe->asXML($completeurl); 
							
							$oldfile    = '../xml/city-desc/city_sub_desc/'.$filename.".xml";
							$newfile    = '../xml/city-desc/'.$filename.".xml";
							if (!@copy($oldfile, $newfile)) 
							{
								$msg= "not copy Properly";
							}
							else
							{
							$msg="City description created successfully";
							}
							
							
							
							header("Location:addcitydescription.php?msg=".$msg);
							exit;
				  }
				  
				  
				}
			}
			closedir($dh);
			
			
		}
	} 
}


?>


<link rel="stylesheet" href="<?php echo $base_url ?>/ckeditor/_sample/sample.css">
	<table width="100%" align="center" cellspacing="0px" cellpadding="0px" style="background-color:#FFF;" >
		<tr>
		<td colspan="2" cellspacing="0px">
		<?php  include("cms_includes/header.php"); ?>
		</td>
		</tr>
		<tr>
		<td class="widthtbs">
		
		
		<?php include("cms_includes/leftpanel.php"); ?>
	
		</td>
		<td class="vt">
			
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr><td class="headingData">Add City desc</td></tr>
			<?php
			if(isset($_REQUEST['msg']))
			{
			?>
		    <tr><td style="vertical-align:text-top; text-align:center; font-size:10px; border:none; color:red;"><?php echo $_REQUEST['msg']; ?></td></tr>
			<?php
			}
			?>
			</table>
		  <form name="form1" action="" method="post" onsubmit="return validateVal(this);">
			<table width="100%" cellspacing="0" cellpadding="0" >
			 <tr>
			 <td>
			  City Name 
			 </td>
			 <td>
				 <input type="text" name="cityname" id="cityname" value="">
				 
			 </td>
			 </tr>
			 
			 <tr>
			 <td>
				Description
			 </td>
			 <td>
				 <textarea name="citydesc" id="citydescVal" class='ckeditor' rows='7' cols='35'></textarea>
				 
			 </td>
			 </tr>
			<tr>
			<td>
				
			</td>
			<td>
			<input type="submit" name="submit" value="Submit">
			</td>
			</tr>
			</table>
		  
		  
		  
		  </form>
		</td>
		</tr>


		<tr>
		<td colspan="2" style="margin-top:0px;">
		<?php  include("cms_includes/footer.php"); ?>
		</td>
		</tr>

		</td>
		</tr>

		</table>
		
		
		<script type="text/javascript">


			CKEDITOR.replace( 'citydescVal', {
			fullPage: true,
			allowedContent: true
			});
			</script>
		
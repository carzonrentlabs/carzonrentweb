﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// config.height = 200; 
	// config.width = 250; 
	 config.linkShowAdvancedTab = false; 
	  config.allowedContent = true;
	
	
};
CKEDITOR.replace( 'textarea_id', {
	toolbarGroups: [
		{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
 		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
 		'/',																// Line break - next group will be placed in new line.
 		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
 		{ name: 'links' }
	]

	// NOTE: Remember to leave 'toolbar' property with the default value (null).
});
CKEDITOR.on( 'dialogDefinition', function( ev )
   {
      // Take the dialog name and its definition from the event data.
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
     alert(dialogDefinition)
      // Check if the definition is from the dialog we're
      // interested in (the 'link' dialog).
      if ( dialogName == 'link' )
      {
         // Remove the 'Target' and 'Advanced' tabs from the 'Link' dialog.
         dialogDefinition.removeContents( 'target' );
         dialogDefinition.removeContents( 'advanced' );
 
         // Get a reference to the 'Link Info' tab.
         var infoTab = dialogDefinition.getContents( 'info' );
 
         // Remove unnecessary widgets from the 'Link Info' tab.         
         infoTab.remove( 'linkType');
         infoTab.remove( 'protocol');
      }
   });
<?php
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
//check login credential 
if(!ob_start("ob_gzhandler")) ob_start();  session_start(); 
if(!isset($_SESSION['userid'])){header('Location:http://localhost/corzoncms/admincms/');}
//get all city
$db->query('select * from city  where remove=0 order by id desc; ');


?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Cor Cms</title>
  <link rel="stylesheet" href="<?=$base_url?>/cms_css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?=$base_url?>/cms_js/validation.js"></script>
</head>

<body>
	
<?php include 'cms_includes/header.php';  ?>
	<div style="margin-top:20px;text-align:center">
		<input type="radio" id="c_view" name="click1" checked>city View</input><input type="radio" id="c_deleted" name="click1">deleted cities</input>
	</div>
  <section class="container" id="city_view" style='width:940px;margin:0px auto;padding:25px 0px;'>	
  
    <div class="login" style='width:886px;'>
      <h1> City View</h1>
      
	<?if($db->countrows()>0) {$city=$db->loadrows();?>
	 <table border='1' cellpadding='0' cellspacing='0' width='886' class="display" id="c_paginate">
	    <thead><tr><th class="th2">Name</th><th class="th2">Type</th><th class="th2">Description</th><th class="th2">status</th><th class="th2">Operations</th></tr></thead>
		<tbody>
	 <? $flag=0; 
	   foreach($city as $val): 
	   
	   ?>
	    
		<tr >
			<td width="60"><?=$val['name']?></td>
			<td width="60"><?=($val['type']==0)?'from City':'To City'?></td>
			<td><?=substr(strip_tags($val['city_description']),0,150)?>...</td>
			<td id="city_<?=$val['id']?>"><?=($val['active']==1)?'Active':'Inactive'?></td>
			<td width='150'><a style="padding:0px 45px 0px 11px" href="<?=$base_url?>/addcity.php?mode=editcity&id=<?=$val['id']?>" ><img src='<?=$base_url?>/cms_images/edit.png' class='edit' /></a><?=($val['active']==1)?"<a id='inactive1' href='javascript:void(0)' onclick=change_status($val[id],'inactive') style='padding-right:46px'><img src='$base_url/cms_images/active.png' title='active' class='active1'/></a>":"<a href='javascript:void(0)' id='active1' onclick=change_status($val[id],'active') style='padding-right:46px'><img src='$base_url/cms_images/inactive.png' title='Inactive' class='inactive1'/> </a>" ?><a href='javascript:void(0)' style='padding-right:46px' onclick="city_delete(<?=$val['id']?>,this)"><img class='delete' title='Delete 'src='<?=$base_url?>/cms_images/delete.png'/></a></td>
		</tr>
	 
	 <?  endforeach;	?>
	 </tbody>
	 </table>
	 <? } else {?>
	 
	  <p class="no_r_found" >no record found	</p>
	 <? }?>
	 
	 </div>	 
    
    
	<div style='float:right;' ><a href='<?=$base_url?>/mainmenu/'><div class="back"></div></a><div>
    
  </section>
   <section class="container" id="deleted_cities" style='width:940px;margin:0px auto;padding:25px 0px;display:none'>	
  
    <div class="login" style='width:886px;'>
      <h1> Deleted Cities</h1>
      <? $db->query('select * from city where remove=1'); ?>
	<?if($db->countrows()>0) {$city=$db->loadrows(); ?>
	 <table border='1' cellpadding='0' cellspacing='0' width='886' id="d_paginate" class="display">
	    <thead><tr><th class="th2">Name</th><th class="th2">Type</th><th class="th2">Description</th><th class="th2">status</th><th class="th2">Operations</th></tr>
		</thead>
		<tbody>
	 <?  
	   foreach($city as $val): 
	  
	   ?>
	    
		<tr style='background:gainsboro'>
			<td width="60"><?=$val['name']?></td>
			<td width="60"><?=($val['type']==0)?'from City':'To City'?></td>
			<td><?=substr(strip_tags($val['city_description']),0,150)?>...</td>
			<td id="city_<?=$val['id']?>"><?=($val['active']==1)?'Active':'Inactive'?></td>
			<td width='150' style="text-align:center!important"><a href='javascript:void(0)' style='padding-right:20px' onclick="restore_city(<?=$val['id']?>,this)"><button type="button" class="restore" style="">Restore</button></a></td>
		</tr>
	 
	 <? endforeach;	?>
	 </tbody>
	 </table>
	 <? } else {?>
	 
	  <p class="no_r_found" >no record found	</p>
	 <? }?>
	 
	 </div>	 
    
    
	<div style='float:right;' ><a href='<?=$base_url?>/mainmenu/'><div class="back"></div></a><div>
    
  </section>
 
 
 

<?php include 'cms_includes/footer.php';  ?>
</body>
<script type="text/javascript" charset="utf-8">

$(document).ready(function(){
    $('#c_paginate').dataTable();
	$('#d_paginate').dataTable();
	$('#c_paginate_length,#d_paginate_length').hide();
});


  
</script>
<script>
$('#c_view').click(function(){
 $('#city_view').show(800);
 $('#deleted_cities').hide(1000);
 
});
$('#c_deleted').click(function(){
 $('#city_view').hide(1000);
 $('#deleted_cities').show(800);
})
</script>
<script>
function change_status(id,status)
{
$.post('<?=$base_url ?>/ajax.php?mode=city_status&id='+id+'&status='+status,function(data){

if(data==1)
{location.reload();
}
else if(data==0)
{//alert(id+'in')
 // $('#city_'+id).text('Inactive');
 location.reload();
}
else
{ alert('something went wrong')
}

});


}
function city_delete(id,element)
{ var flag=confirm('Are you sure ?');
// alert(id);
if(flag){
$.post('<?=$base_url ?>/ajax.php?mode=city_delete&id='+id,function(data){

if(data==1)
{$(element).closest('tr').remove();
 location.reload();
}
else
{ alert('something went wrong')
}

});
}
}

// restore city after deleting
function restore_city(id,element)
{
var flag=confirm('Are you sure ?');
// alert(id);
if(flag){
$.post('<?=$base_url ?>/ajax.php?mode=restore_city&id='+id,function(data){

if(data==1)
{$(element).closest('tr').remove();
 location.reload();
}
else
{ alert('something went wrong')
}

});
}


}


</script>
</html>

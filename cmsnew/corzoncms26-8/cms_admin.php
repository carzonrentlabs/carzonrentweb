<?php
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
//check login credential 
session_start();
if(isset($_SESSION['userid'])){header('Location:http://localhost/corzoncms/mainmenu/');}
$msg='';
if(isset($_POST['login']))
{ 
 $login_check=$db->check_login($_POST);
 if($login_check==-1){$msg="username or password empty";}
 if($login_check==0){$msg="username or password does not exist";}
 if($login_check==1){header('Location:'.$base_url.'/mainmenu/');}  
}


 

?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Cor Cms</title>
  <link rel="stylesheet" href="<?=$base_url?>/cms_css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?=$base_url?>/cms_js/validation_admin.js"></script>
</head>

<body>
<div id="header">
<div class="logo "><a href="http://www.carzonrent.com/" id="brand" class="corbrand"></a></div>

</div>
<div style='clear:both'></div>
  <div class="container">
    <div class="login mtop">
      <h1>Login to carzonrent</h1>
      <form method="post" action="" onsubmit='return validation()' id='adminform'>
        <p><input type="text" name="uname" id='uname' value="" placeholder="Username or Email"></p>
        <p><input type="password" name="password" id='pass' value="" placeholder="Password"></p>
        <!--<p class="remember_me">
          <label>
            <input type="checkbox" name="remember_me" id="remember_me">
            Remember me on this computer
          </label>
        </p>-->
        <p class="submit"><input type="submit" name="login" value="Login"></p>
      </form>
    </div>
	<?php if(!empty($msg))
	{ echo "<div class='a_error1'>$msg</div>"; }?>
	
	<div class='a_error' style='display:none'>
	 Please fill requred filled
	</div>

    <!--<div class="login-help">
      <p>Forgot your password? <a href="index.html">Click here to reset it</a>.</p>
    </div>
	-->
  </div>

  
</body>
</html>

<?php
error_reporting(1);
ini_set("display_error",1);
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
//check login credential 
session_start();
ob_end_clean();

$msg = '';
if (isset($_POST['login'])) {
    $login_check = $db->check_login($_POST);
    if ($login_check == -1) {
        $msg = "username or password empty";
    }
    if ($login_check == 0) {
        $msg = "username or password does not exist";
    }
    if ($login_check == 1) {
	
        header('Location:' . $base_url1 . '/contentmenu.php');
    }
}
?>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cor Cms</title>
        <link rel="stylesheet" href="<?php echo $base_url; ?>/cms_css/style.css">
        <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="<?php echo $base_url; ?>/cms_js/validation_admin.js"></script>
        <link rel="stylesheet" href="<?php echo $base_url; ?>/cms_css/custom.css">
    </head>

    <body>
        <div  class="topdata">

            <a href="<?php echo $base_url; ?>/contentmenu.php"><img border="0" src="cms_images/logo.png"/>
            </a>
        </div>
        <div style='clear:both'></div>

        <div class="container">
<?php if (!empty($msg)) {
    echo "<div class='a_error1'>$msg</div>";
}
?>

            <div class='a_error' style='display:none'>
                Please fill all required fields
            </div>
            <div class="login mtop">
                <h1>Login to Carzonrent Admin</h1>
                <form method="post" action="" onsubmit='return validation()' id='adminform'>
                    <p><input type="text" name="uname" id='uname' value="" placeholder="Username or Email"></p>
                    <p><input type="password" name="password" id='pass' value="" placeholder="Password"></p>
                    <p class="submit"><input type="submit" name="login" value="Login"></p>
                </form>
            </div>

        </div>

<?php include 'cms_includes/footer.php'; ?>
    </body>
</html>

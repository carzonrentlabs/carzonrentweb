<?php
include 'baseurl.php';
include 'cms_classes/db_connect.php';
include 'cms_classes/functions.class.php';
//check login credential 
if(!ob_start("ob_gzhandler")) ob_start();  session_start(); 

 $db->sess_expire();
if(!isset($_SESSION['userid'])){header("Location:$base_url1/cms_admin.php");}
//get all Months
$db->query('select * from cor_month_search where is_deleted="0" order by id asc; ');
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Cor Cms</title>
  <link rel="stylesheet" href="<?php echo $base_url; ?>/cms_css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo $base_url; ?>/cms_js/validation.js"></script>
</head>

<body>
	<table width="100%" align="center" cellspacing="0px" cellpadding="0px" style="background-color:#FFF;" >
		<tr>
		<td colspan="2" cellspacing="0px">
		<?php  include("cms_includes/header.php"); ?>
		</td>
		</tr>
		<tr>
		<td class="widthtbs" style="vertical-align:text-top; margin-top:10px;">
		<?php include("cms_includes/leftpanel.php"); ?>
		</td>
		<td style="margin-right:40px">
    <div class="login" style='width:886px;vertical-align:top;'>
      <h1> Manage Month</h1>
      <span style='float: right;'><a href="addMonth.php">Add Month</a></span>
	<?php if($db->countrows()>0) {$month=$db->loadrows();?>
	 <table border='1' cellpadding='0' cellspacing='0' width='886' class="display" id="c_paginate">
	    <thead><tr>
                    <th class="">Name</th>
                    <th class="">Status</th>
                    <th class="">Edit</th>
                    <th class="">Delete</th>
                </tr></thead>
		<tbody>
                    
	 <?php  $flag=0; 
	   foreach($month as $val): 
	   ?>
	    	<tr >
			<td><?php echo $val['name'];?></td>
			<td id='status_<?php echo $val['id'];?>'><?php echo $status=($val['status']==1)?"<a href='javascript:void(0)' onclick=change_status($val[id],'inactive') style='padding-right:46px'><img src='$base_url/cms_images/activeeye.jpg' title='active' class='active1'/></a>":"<a href='javascript:void(0)' onclick=change_status($val[id],'active') style='padding-right:46px'><img src='$base_url/cms_images/inactiveeye.jpg' title='Inactive' class='inactive1'/> </a>"; ?></td>
			<td><a style="padding:0px 45px 0px 11px" href="<?php echo $base_url1; ?>/editMonth.php?id=<?php echo $val['id'];?>" ><img src='<?php echo $base_url; ?>/cms_images/icon_Edit.png' /></a></td>
			<td>
                            <a href='javascript:void(0)' style='padding-right:46px' onclick="month_delete(<?php echo $val['id'];?>,this)"><img title='Delete 'src='<?php echo $base_url; ?>/cms_images/remove.png'/></a></td>

		</tr>
	 
	 <?php  endforeach;	?>
	 </tbody>
	 </table>
	 <?php } else {?>
	 
	  <p class="no_r_found" >no record found</p>
	 <?php }?>
	 
	 </div>	 
		</td>
		</tr>
		<tr>
		<td colspan="2" style="margin-top:0px;">
		<?php  include("cms_includes/footer.php"); ?>
		</td>
		</tr>
		</td>
		</tr>
		</table>
</body>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
    $('#c_paginate').dataTable();
	$('#d_paginate').dataTable();
	$('#c_paginate_length,#d_paginate_length').hide();
});
</script>

<script>
function change_status(id,status)
{
$.post('<?php echo $base_url; ?>/ajax.php?mode=month_status&id='+id+'&status='+status,function(data){
if(data==1)
{   
    $('#status_'+id).html('<a style="padding-right:46px" onclick="change_status('+id+',\'inactive\')" href="javascript:void(0)"><img class="active1" title="active" src="http://localhost/carzonrent/cmsnew/cms_images/activeeye.jpg"></a>');
    
}
else if(data==0)
{
    $('#status_'+id).html('<a style="padding-right:46px" onclick="change_status('+id+',\'active\')" href="javascript:void(0)"><img class="inactive1" title="inactive" src="http://localhost/carzonrent/cmsnew/cms_images/inactiveeye.jpg"></a>');
 
}
else
{ alert('something went wrong')
}
});
}
function month_delete(id,element)
{ var flag=confirm('Are you sure delete record?');
// alert(id);
if(flag){
$.post('<?php echo $base_url; ?>/ajax.php?mode=month_delete&id='+id,function(data){
if(data==1)
{$(element).closest('tr').remove();
 //location.reload();
}
else
{ alert('something went wrong')
}
});
}
}
// restore city after deleting
function restore_city(id,element)
{
var flag=confirm('Are you sure restore record ?');
// alert(id);
if(flag){
$.post('<?php echo $base_url; ?>/ajax.php?mode=restore_city&id='+id,function(data){
if(data==1)
{$(element).closest('tr').remove();
 location.reload();
}
else
{ alert('something went wrong')
}
});
}
}
</script>
</html>
<?php 
error_reporting(0);
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
define('corWebRoot','http://10.90.90.37/carzonrent/images/mailer_banner.jpg');	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Great Indian BreakUp || carzonrent.com</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="css/cor.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<style type="text/css">
.myles_head {
margin: 20px auto 0;
width: 100%;
background-color: #DB4425;
height: 40px;
line-height: 40px;
color: #fff;
}
.magindiv {
width: 1130px;
margin: 0 auto !important;
}
.sale{
width:250px;
display:inline-block;
text-align:center;
margin-bottom:10px;
padding:0px;
height:auto;
background:#df4627;
}
.f_l{float:left;}
.clr{clear:both;}
.sale_content{
width:72%;
margin-left:20px;
margin-bottom:20px;
background-color:#FBFBFB;
padding:15px;
border:1px solid #E5E5E5;
}
.sale_content ul{
margin:5px 0 10px 19px;
padding:0px;
}
.sale_content p.know_more_TC{margin-bottom:0px;}
.sale_content ul li{
font-size:13px;
}
.hedding_tc{
color:#e1471b;
padding:5px 0 0;
font-size:13px;
}
.sale_content ul.off{
margin-bottom:2px;
}
</style>
<div id="middle" style="width:100%" >
<div class="myles_head">
<div class="magindiv">
<h2>The Great Indian BreakUp</h1>
</div>
</div>
<div class="mainW960" style="padding:20px 0;">
<div class="sale f_l" style="width:276px;">
<img src="http://www.mylescars.com/img/500off.jpg" border="0">
</div>
<div class="f_l sale_content" style="width:70%;padding-bottom:54px;">
<h2 class="heading2">Offer:-</h2>
<div class="hedding_tc">Drive away your first Myles car at a Flat Rs. 500/- discount. </div>
<br>
<strong>TERMS AND CONDITIONS</strong>
<ul style="margin:5px 0 10px 20px;font-family: calibri;">
<li>Valid for first time users only.</li>
<li>  The discount amount is applicable on the base fare only.</li>
<li> Valid only on the self-drive service: Myles</li>
<li>   Valid on all cars and all city of operations.</li>
<li>  Booking subject to car availability.</li>
<li>   As a mandate, a minimum of Rs.1 will have to be paid to complete the transaction in case the amount is less or equal to the discount amount. This can be paid through debit card/credit card/netbanking.</li>
<li>  Standing instructions/ pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>  Offer not to be clubbed with any other promotion.</li>
<li>  The discount can be redeemed in full & not in parts.</li>
<li>Blackout dates apply</li>
<li>Other standard T&C apply.</li>
</ul>
<br>
<strong>How to avail / book</strong>
<ul style="margin:5px 0 10px 20px;font-family: calibri;">
<li>Log on to <a href="http://www.mylescars.com/" target="_block">www.mylescars.com</a> to book a Myles car.</li>
<li>Select the preferred city, date/time, car &amp; the location.</li>
<li>Fill in the details and proceed with the booking. Discount amount will be automatically deducted from the final booking amount.  </li>
<li>Proceed with the booking &amp; pay the balance amount through credit card/debit card/net banking.</li>
</ul>
<p class="know_more_TC">Know more about the service: <strong>call 08882222222</strong></p>
</div>
</div>
</div>
<?php include_once("./includes/footer.php"); ?>
<script type="text/javascript" src="js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="css/tab.css">
</body>
</html>

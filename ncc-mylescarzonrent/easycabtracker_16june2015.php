<?php
	ob_start();
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
		<head>
		<title>Booking Status | Carzonrent Admin</title>
		<?php
		include_once("./includes/cache-func.php");
		include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
		include_once("./includes/config.php");
		include_once("./classes/bb-mysql.class.php");
		include_once("./classes/cor.ws.class.php");
		include_once("./classes/cor.xmlparser.class.php");
       ?>
	   <style>
	   .h1 {
         font-weight: bold;
       }
	   </style>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Easycab Payback Status</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <form id="" name="" action="./easycabtracker.php" method="GET" onsubmit="return validate();">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td colspan="2" style="padding-left: 12px;">Enter Easycab Coupan Code</td>
					    <td style="padding-left: 10px;">
						<input type="text" id="q" name="q" class="txtSBox" size="22" placeholder="Enter here" <?php if(isset($_GET["q"])) { echo "value=" . $_GET["q"]; } ?> /></td>
					    <td style="padding-left: 15px;"><input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" /></td>
					</tr>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
	<?php 
	 if(isset($_REQUEST['q']) && $_REQUEST['q']!="")
	 {
	 $weburl= webserviceurl1;
	 $WebServiceRequest= new SoapClient($weburl, array('trace' => 1));
	 $WebRequestData1 = array();
	 $WebRequestData1["CouponCode"] = $_REQUEST['q'];
	 $RecordResult = $WebServiceRequest->PaybackTracker($WebRequestData1);
	 if($RecordResult->{'PaybackTrackerResult'})
	 {
		$resultany=$RecordResult->{'PaybackTrackerResult'}->{'PayBackClass'};
	    $countArray=count($resultany);
		if(is_array($resultany))
		{ 
		 foreach($resultany as $valarray)
		 {
		 ?>
			<table cellpadding="5" cellspacing="1" class="adm" border="0" style="margin: 10px 1.5%; width: 97%;float: left;">
				   
				   <tr class="od">
					<td class="h1">Job No</td>
					<td><?php echo $valarray->JobNo; ?></td>
					<td class="h1">Caller ID</td>
					<td><?php echo $valarray->CallerID; ?></td>
				    </tr>
				    <tr class="ev">
					<td class="h1">Pax Name</td>
					<td><?php echo $valarray->PaxName; ?></td>
					<td class="h1">Pickup Address</td>
					<td><?php echo $valarray->PickupAddress; ?></td>
				    </tr>
				    <tr class="od">
					<td class="h1">Destination </td>
					<td><?php echo $valarray->Destination; ?></td>
					<td class="h1">EMail ID</td>
					<td><?php echo $valarray->eMailID; ?></td>
				    </tr>
				    <tr class="ev">
					<td class="h1">Discount</td>
					<td><?php echo $valarray->Discount; ?></td>
					<td class="h1">Discount Rcvd</td>
					<td><?php echo $valarray->Discount_Rcvd; ?></td>
				    </tr>
				    <tr class="od">
					<td class="h1">Coupon Code</td>
					<td><?php echo $valarray->Coupon_Code; ?></td>
					<td class="h1">Payment Amount</td>
					<td><?php echo $valarray->JobNo; ?></td>
				    </tr>
				</table>
			<?php
			
			}
		}
		else
		{
			?>
			<table cellpadding="5" cellspacing="1" class="adm" border="0" style="margin: 10px 1.5%; width: 97%;float: left;">
			 <tr class="od">
	
					<td style="text-align: center !IMPORTANT;"><span style="color: red;"><?php echo $resultanyData=$resultany->{'DoReturn'};  ?></span></td>
				    </tr>
				</table>
		  <?PHP 
		}
	 }
	 }
	?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p <?php if(!isset($_GET["q"])) { echo "style=\"height:300px;\""; } ?>>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	unset($bookingDetails);
	?>
    </body>
</html>
<script>
function validate()
{
 var code = $('#q').val();
 if(code=="")
 {
	 alert('Please Enter Coupan code');
	 $('#q').css('background-color','pink').attr('placeholder','Please Enter Coupan code').focus();
	 return false;
 }
}
</script>
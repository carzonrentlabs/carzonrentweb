_searchDefiner = function()
{
     
}

_getDateWiseDetails = function(id,startdate,enddate,origin,destination,tour_type)
{
    var p = new Array();
    p[0] = new Array("id",id);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);
    p[3] = new Array("origin",origin);
    p[4] = new Array("destination",destination);
    p[5] = new Array("tour_type",tour_type);
    var aj = new _ajax("./ajax/get-date-wise-details.php", "post",p,function(){_waitDateWiseDetails()}, function(r){_responseDateWiseDetails(r)});
    aj._query();
}

_waitDateWiseDetails = function()
{
    
}

_responseDateWiseDetails = function(resp)
{
    var disresp = resp.split("#!#");    
    var tab = document.getElementById("tbdy");   
    tab.innerHTML = disresp[1];    
}

_getOriginDateWiseDetails = function(id,startdate,enddate,origin,tour_type)
{
    var p = new Array();
    p[0] = new Array("id",id);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);
    p[3] = new Array("origin",origin);
    p[4] = new Array("tour_type",tour_type);
    var aj = new _ajax("./ajax/get-origin-date-wise-details.php", "post",p,function(){_waitOriginDateWiseDetails()}, function(r){_responseOriginDateWiseDetails(r)});
    aj._query();
}

_waitOriginDateWiseDetails = function()
{
    
}

_responseOriginDateWiseDetails = function(resp)
{
    var disresp = resp.split("#!#");    
    var tab = document.getElementById("tbdy");   
    tab.innerHTML = disresp[1];    
}

_getDateWiseDetailsTourType = function(startdate,enddate,tour_type)
{
    var p = new Array();   
    p[0] = new Array("startdate",startdate);   
    p[1] = new Array("enddate",enddate);
    p[2] = new Array("tour_type",tour_type);
    var aj = new _ajax("./ajax/get-date-wise-details-tour-type.php", "post",p,function(){_waitDateWiseDetailsTourType()}, function(r){_responseDateWiseDetailsTourType(r)});
    aj._query();
}

_waitDateWiseDetailsTourType = function()
{
    
}

_responseDateWiseDetailsTourType = function(resp)
{    
    var tab = document.getElementById("tbdy");   
    tab.innerHTML = resp;
}

_loadMoreSearch = function(sdate,edate,seq_no)
{
    var p = new Array();
    p[0] = new Array("sdate",sdate);
    p[1] = new Array("edate",edate);
    p[2] = new Array("seq_no",seq_no);      
    var aj = new _ajax("./ajax/load-more-search.php", "post",p,function(){_waitloadMoreSearch()}, function(r){_responseloadMoreSearch(r)});
    aj._query();
}
_waitloadMoreSearch = function()
{
    
}
_responseloadMoreSearch = function(resp)
{
    var disresp = resp.split("#!#");
    var sdate = disresp["0"];
    var edate = disresp["1"];
    var sno = disresp["2"]
    var html = disresp["3"]
    var c = disresp["4"]
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreSearch("+"'"+ sdate +"'"+","+ "'"+ edate+"'" +"," + sno +")\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
    
}

_loadMoreNewBooking = function(seq_no,startdate,enddate)
{    
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);    
    var aj = new _ajax("./ajax/loadmore-new-bookings.php", "post",p,function(){_waitloadMoreNewBooking()}, function(r){_responseloadMoreNewBooking(r)});
    aj._query();
}

_waitloadMoreNewBooking = function()
{
    
}

_responseloadMoreNewBooking = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"]
    var html = disresp["1"]
    var c = disresp["2"];
    var stime = disresp["3"];
    var etime = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreNewBooking("+ sno +",'"+stime+"','"+etime+"')\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
    
}


_loadMoreFailedBooking = function(seq_no,stdate,edate)
{    
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("stdate",stdate);
    p[2] = new Array("edate",edate);
    var aj = new _ajax("./ajax/loadmore-Failed-bookings.php", "post",p,function(){_waitloadMoreFailedBooking()}, function(r){_responseloadMoreFailedBooking(r)});
    aj._query();
}

_waitloadMoreFailedBooking = function()
{
    
}

_responseloadMoreFailedBooking = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"]
    var html = disresp["1"]
    var c = disresp["2"];
    var sdate = disresp["3"];
    var edate = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreFailedBooking("+ sno +",'"+ sdate +"'"+","+ "'"+ edate+"'" +")\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
    
}

_loadMoreAllFromCOrBooking = function(seq_no,stdate,edate)
{
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("stdate",stdate);
    p[2] = new Array("edate",edate);
    var aj = new _ajax("./ajax/loadmore-get-all-from-cor-booking.php", "post",p,function(){_waitloadMoreAllFromCOrBooking()}, function(r){_responseloadMoreAllFromCOrBooking(r)});
    aj._query();
}
_waitloadMoreAllFromCOrBooking = function()
{
    
}

_responseloadMoreAllFromCOrBooking = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"];
    var html = disresp["1"];
    var c = disresp["2"];
    var sdate = disresp["3"];
    var edate = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreAllFromCOrBooking("+ sno +",'"+sdate+"'"+",'"+edate+"')\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
    
}


/*Aamir*/
_loadMoreNewBookingEC = function(seq_no,startdate,enddate)
{    
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);    
    var aj = new _ajax("./ajax/loadmore-new-bookings-ec.php", "post",p,function(){_waitloadMoreNewBookingEC()}, function(r){_responseloadMoreNewBookingEC(r)});
    aj._query();
}

_waitloadMoreNewBookingEC = function()
{
    
}

_responseloadMoreNewBookingEC = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"]
    var html = disresp["1"]
    var c = disresp["2"];
    var stime = disresp["3"];
    var etime = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreNewBookingEC("+ sno +",'"+stime+"','"+etime+"')\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
}


_getCityDateWiseDetailsEC = function(id,startdate,enddate,origin)
{
    var p = new Array();
    p[0] = new Array("id",id);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);
    p[3] = new Array("origin",origin);
    var aj = new _ajax("./ajax/get-city-date-wise-details-ec.php", "post",p,function(){_waitCityDateWiseDetailsEC()}, function(r){_responseCityDateWiseDetailsEC(r)});
    aj._query();
}

_waitCityDateWiseDetailsEC = function()
{}

_responseCityDateWiseDetailsEC = function(resp)
{
    var disresp = resp.split("#!#");    
    var tab = document.getElementById("tbdy");   
    tab.innerHTML = disresp[1];    
}

_loadMoreNewBookingECFail = function(seq_no,startdate,enddate)
{    
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);    
    var aj = new _ajax("./ajax/loadmore-new-bookings-ec-failure.php", "post",p,function(){_waitloadMoreNewBookingECFail()}, function(r){_responseloadMoreNewBookingECFail(r)});
    aj._query();
}

_waitloadMoreNewBookingECFail = function()
{
    
}

_responseloadMoreNewBookingECFail = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"]
    var html = disresp["1"]
    var c = disresp["2"];
    var stime = disresp["3"];
    var etime = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreNewBookingECFail("+ sno +",'"+stime+"','"+etime+"')\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
}



_getErrorReportEC = function(id,startdate,enddate,err)
{
    var p = new Array();
    p[0] = new Array("id",id);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);
	p[3] = new Array("err",err);
    var aj = new _ajax("./ajax/get-error-report-date-wise-details-ec.php", "post",p,function(){_waitErrorReportEC()}, function(r){_responseErrorReportEC(r)});
    aj._query();
}

_waitErrorReportEC = function()
{}

_responseErrorReportEC = function(resp)
{
    var disresp = resp.split("#!#");    
    var tab = document.getElementById("tbdy");   
    tab.innerHTML = disresp[1];    
}


_loadMoreLoginEC = function(seq_no,startdate,enddate)
{    
    var p = new Array();   
    p[0] = new Array("seq_no",seq_no);
    p[1] = new Array("startdate",startdate);
    p[2] = new Array("enddate",enddate);    
    var aj = new _ajax("./ajax/loadmore-login-ec.php", "post",p,function(){_waitloadMoreLoginEC()}, function(r){_responseloadMoreLoginEC(r)});
    aj._query();
}

_waitloadMoreLoginEC = function()
{
    
}

_responseloadMoreLoginEC = function(resp)
{
    var disresp = resp.split("#!#");   
    var sno = disresp["0"]
    var html = disresp["1"]
    var c = disresp["2"];
    var stime = disresp["3"];
    var etime = disresp["4"];
    
    document.getElementById("searchData").innerHTML = document.getElementById("searchData").innerHTML + html;
    var seq_no = document.getElementsByName("seqno");
    for(var i = 0;i<seq_no.length;i++)
    {
        seq_no[i].innerHTML = i+1;
    }
    document.getElementById('loadmore').innerHTML = "<tr><td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"javascript:_loadMoreLoginEC("+ sno +",'"+stime+"','"+etime+"')\">LOAD MORE</a></td></tr>";
    
    if(c == 1)
    {
            document.getElementById("loadmore").style.display = "none";
    }
}
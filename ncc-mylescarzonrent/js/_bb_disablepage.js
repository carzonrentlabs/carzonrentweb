function _disableThisPage()
{
	var tranDiv;
	var isFound = false;
	for(i = 0; i < document.body.getElementsByTagName("IFRAME").length; i++)
	{
		if(document.body.getElementsByTagName("IFRAME")[i].id == "disableDiv")
			isFound = true;
	}
	if(isFound == false)
	{
		try{
			tranDiv = document.createElement("<IFRAME id=\"disableDiv\" style=\"background-color:#111111;z-index:99;top:0;left:0;position:absolute;width:100%\">");
			hDiv = document.createElement("<div id=\"lastItem\"></div>");
		}
		catch(e)
		{
			tranDiv = document.createElement("IFRAME");
			tranDiv.id = "disableDiv";
			tranDiv.style.backgroundColor = "#111111";
			tranDiv.style.zIndex = "99";
			tranDiv.style.top = 0;
			tranDiv.style.left = 0;
			tranDiv.style.position = "absolute";

			hDiv = document.createElement("div");
			hDiv.id = "lastItem";
		}
	}
	else
		tranDiv = document.getElementById("disableDiv");
	
	if(isFound == false)
		document.body.appendChild(hDiv);

	var brsName = navigator.appName;
	var scrWidth = screen.availWidth;
	var scrHeight = document.getElementById("lastItem").offsetTop;
	if(scrHeight <  screen.availHeight)
		scrHeight = screen.availHeight;
    if(brsName == "Microsoft Internet Explorer")
		tranDiv.style.width = (scrWidth - 20) + "px";
	else
		tranDiv.style.width = (scrWidth - 20)  + "px";

	tranDiv.style.opacity = (70 / 100); 
    tranDiv.style.MozOpacity = (70 / 100); 
    tranDiv.style.KhtmlOpacity = (70 / 100);
    tranDiv.style.filter = "alpha(opacity=" + 70 + ")";
	tranDiv.style.display = "block";
	tranDiv.style.height = (scrHeight * 3) + "px";
	
	if(isFound == false)
		document.body.appendChild(tranDiv);
};

function _enableThisPage()
{
	try{
		var tranDiv = document.getElementById("disableDiv");
		tranDiv.style.display = "none";
	}
	catch(e){}
};
<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("./includes/check-user.php");
    include_once("./includes/cache-func.php");
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");
    
    $report = "";
    if(isset($_GET["rpt"]) && $_GET["rpt"] != "")
        $report = $_GET["rpt"];
    $type = "";
    if(isset($_GET["tp"]) && $_GET["tp"] != "")
        $type = $_GET["tp"];
    $sDate = date_create(date("Y-m-d"));
    $eDate = date_create(date("Y-m-d"));    
    if(isset($_GET["sd"]) && $_GET["sd"] != "")
        $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
        
    if(isset($_GET["ed"]) && $_GET["ed"] != "")
        $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
        
    if(isset($_GET["sd"]) && $_GET["sd"] != "" && isset($_GET["ed"]) && $_GET["ed"] != "")                   
        $filename = "report_" . $sDate->format('d-m-Y') . "_" . $eDate->format('d-m-Y');
    else
        $filename = "report_overall";    
    $filename .= ".csv";
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    if($type = "booking"){
    // SOURCE report code STARTS
            if($report == "source")
            fputcsv($output, array("Source Report"));
            elseif($report == "tourtype")
            fputcsv($output, array("Tour Type Report"));
            elseif($report == "origin")
            fputcsv($output, array("Origin Report"));
            
            fputcsv($output, array("Report date", $sDate->format("d M, Y") . " - " . $eDate->format("d M, Y")));
            $arrData = array();
                if($report == "source")
                $arrData[] = "Source";
                elseif($report == "tourtype")
                $arrData[] = "Tour Type";
                elseif($report == "origin")
                $arrData[] = "Origin";
                $arrData[] = "Searches";
                $arrData[] = "Booking attempts";
                $arrData[] = "";
                $arrData[] = "Confirmed booking";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "Payment Mode";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "Incomplete booking";
                $arrData[] = "";
                $arrData[] = "Payment Status";
                $arrData[] = "";
                $arrData[] = "";
            fputcsv($output, $arrData);
            unset($arrData);
            $arrData = array();
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "#";
                $arrData[] = "%";
                $arrData[] = "#";
                $arrData[] = "%";
                $arrData[] = "Amount (Rs)";
                $arrData[] = "Online";
                $arrData[] = "";
                $arrData[] = "Pay to driver";
                $arrData[] = "";
                $arrData[] = "#";
                $arrData[] = "%";
                $arrData[] = "Gateway";
                $arrData[] = "Cancelled";
                $arrData[] = "Unauthorized";
                fputcsv($output, $arrData);
            unset($arrData);
            $arrData = array();
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "#";
                $arrData[] = "Amount (Rs)";
                $arrData[] = "#";
                $arrData[] = "Amount (Rs)";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
                $arrData[] = "";
            fputcsv($output, $arrData);
            unset($arrData);
        if($report == "source")
            $sql = "SELECT website as source, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY website ORDER BY COUNT(unique_id) DESC";
        elseif($report == "tourtype")
            $sql = "SELECT tour_type, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY tour_type ORDER BY COUNT(unique_id) DESC";
        elseif($report == "origin")
            $sql = "SELECT origin_name, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
        //elseif($report == "payment-mode")
            //$sql = "SELECT payment_mode, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY payment_mode ORDER BY COUNT(unique_id) DESC";
        $r = $db->query("query", $sql);
        $totS = 0;
        $totA = 0;
        $totC = 0;
        $totI = 0;
        $totO = 0;
        $totP = 0;
        $totCC = 0;
        $totOC = 0;
        $totPC = 0;
        $totPG = 0;
        $totCN = 0;
        $totFL = 0;
        if(!array_key_exists("response", $r)){
            for($i = 0; $i < count($r); $i++){
                $tS = 0;
                $tA = 0;
                $tC = 0;
                $tI = 0;
                $tO = 0;
                $tP = 0;
                $tCC = 0;
                $tOC = 0;
                $tPC = 0;
                $tPG = 0;
                $tCN = 0;
                $tFL = 0;
                $tS = $r[$i]["TotalSearch"];
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "'";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "'";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "'";
                $data = $db->query("query", $sql);
                if(!array_key_exists("response", $data))
                $tA = $data[0]["TotalAttempts"];
                unset($data);
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL";
                $data = $db->query("query", $sql);
                $tC = $data[0]["TotalBooking"];
                if(isset($data[0]["TotalCost"]))
                $tCC = $data[0]["TotalCost"];
                else
                $tCC = 0;
                unset($data);
                
                $tI = $tA - $tC;


            $arrData = array();
                if($report == "source")
                $arrData[] = $r[$i]["source"];
                elseif($report == "tourtype")
                $arrData[] = $r[$i]["tour_type"];
                elseif($report == "origin")
                $arrData[] = $r[$i]["origin_name"];
                $arrData[] = $tS;
                $arrData[] = $tA;
                if($tS > 0)
                $arrData[] = number_format(($tA * 100) / $tS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                
                $arrData[] = $tC;
                if($tS > 0)
                $arrData[] = number_format(($tC * 100) / $tS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                $arrData[] = $tCC;
                
                if($report == "source")
                $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                elseif($report == "tourtype")
                $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                elseif($report == "origin")
                $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                $data = $db->query("query", $sql);
                if(isset($data[0]["TotalBooking"]))
                $tO = $data[0]["TotalBooking"];
                else
                $tO = 0;
                if(isset($data[0]["TotalCost"]))
                $tOC = $data[0]["TotalCost"];
                else
                $tOC = 0;
                unset($data);
        
                $arrData[] = $tO;
                $arrData[] = $tOC;
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                $data = $db->query("query", $sql);
                if(isset($data[0]["TotalBooking"]))
                $tP = $data[0]["TotalBooking"];
                else
                $tP = 0;
                if(isset($data[0]["TotalCost"]))
                $tPC = $data[0]["TotalCost"];
                else
                $tPC = 0;
                $arrData[] = $tP;
                $arrData[] = $tPC;
                $arrData[] = $tI;
                if($tS > 0)
                $arrData[] = number_format(($tI * 100) / $tS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 2";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 2";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 2";
                $data = $db->query("query", $sql);
                if(isset($data[0]["TotalBooking"]))
                $tPG = $data[0]["TotalBooking"];
                else
                $tPG = 0;
                $arrData[] = $tPG;

                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 4";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 4";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 4";
                $data = $db->query("query", $sql);
                if(isset($data[0]["TotalBooking"]))
                $tCN = $data[0]["TotalBooking"];
                else
                $tCN = 0;
                $arrData[] = $tCN;
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 5";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 5";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 5";
                $data = $db->query("query", $sql);
                if(isset($data[0]["TotalBooking"]))
                $tFL = $data[0]["TotalBooking"];
                else
                $tFL = 0;
                $arrData[] = $tFL;
            fputcsv($output, $arrData);
            unset($arrData);
                $totS += $tS;
                $totA += $tA;
                $totC += $tC;
                $totI += $tA - $tC;
                $totO += $tO;
                $totOC += $tOC;
                $totP += $tP;
                $totCC += $tCC;
                $totPC += $tPC;
                $totPG += $tPG;
                $totCN += $tCN;
                $totFL += $tFL;
            }
            $arrData = array();
                $arrData[] = "TOTAL";
                $arrData[] = $totS;
                $arrData[] = $totA;
                if($totS > 0)
                $arrData[] = number_format(($totA * 100) / $totS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                $arrData[] = $totC;
                if($totS > 0)
                $arrData[] = number_format(($totC * 100) / $totS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                $arrData[] = $totCC;
                $arrData[] = $totO;
                $arrData[] = $totOC;
                $arrData[] = $totP;
                $arrData[] = $totPC;
                $arrData[] = $totI;
                if($totS > 0)
                $arrData[] = number_format(($totI * 100) / $totS, 2, ".", "") . "%";
                else
                $arrData[] = "0.00%";
                $arrData[] = $totPG;
                $arrData[] = $totCN;
                $arrData[] = $totFL;
            fputcsv($output, $arrData);
            unset($arrData);
        }
        unset($r);
        // SOURCE report code ENDS
    }
    $db->close();    
?>
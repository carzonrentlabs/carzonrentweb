<?php
    include_once("./includes/check-user.php");
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Myles Visits | Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Enquiry</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./myles-visits.php";
			$dlndURL = "./myles-visits-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "conversions";
			$sortMode = "desc";
                        $sortModeC = SORT_DESC;
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("ref_code" => "Source");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			if(isset($_GET["f"]) && $_GET["f"] != ""){
			    $fld = $_GET["f"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["q"]) && $_GET["q"] != ""){
			    $fVal = $_GET["q"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["inc"]) && $_GET["inc"] != ""){
			    $isDtInc = true;
			    $isFiltered = true;
			}
			
			if(isset($_GET["sf"]) && $_GET["sf"] != "")
			$sortFld = $_GET["sf"];
			
			if(isset($_GET["sm"]) && $_GET["sm"] != ""){
                            $sortMode = $_GET["sm"];
                            if(strtolower($_GET["sm"]) == "asc")
                            $sortModeC = SORT_ASC;
                        }
                        
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT UPPER(ref_code) as ref_code, COUNT(uid) as 'visits' FROM cor_camaign_tracking WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ";
			    }
			    $sql .= " GROUP BY ref_code";
			} else {
			    $sql = "SELECT UPPER(ref_code) as ref_code, COUNT(uid) as 'visits' FROM cor_camaign_tracking WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' GROUP BY ref_code";
			}
                        $totVisits = 0;
                        $totEnquiries = 0;
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
                            for($i = 0; $i < count($r); $i++){
                                $totVisits += $r[$i]["visits"];
                                if($r[$i]["ref_code"] != "")
                                $sql = "SELECT COUNT(unique_id) as 'conversions' FROM myles_enquiry WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND ref_code = '" . $r[$i]["ref_code"] . "'";
                                else
                                $sql = "SELECT COUNT(unique_id) as 'conversions' FROM myles_enquiry WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND ref_code IS NULL";
                                $conv = $db->query("query", $sql);
                                if(!array_key_exists("response", $conv)){
                                    $r[$i]["conversions"] = $conv[0]["conversions"];
                                    $totEnquiries += $conv[0]["conversions"];
                                } else {
                                    $r[$i]["conversions"] = "0";
                                }
                                unset($conv);
                            }
                        }
                        $fSort = array();
                        foreach($r as $k => $v)
                            $fSort[$k] = $v[$sortFld];
                        array_multisort($fSort, $sortModeC, $r);
                        
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
			$db->close();
			$pagingHTML = "";
		?>
	    <?php
		if($_SERVER["QUERY_STRING"] != ""){
	    ?>
		<script type="text/javascript">
	    <?php
		    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
		    for($q = 0; $q < count($qsParams); $q++){
			if(trim($qsParams[$q]) != ""){
			    $qsParam = explode("=", $qsParams[$q]);
	    ?>
			    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
	    <?php
			}
		    }
	    ?>
		</script>
	    <?php
		} else {
	    ?>
		<script type="text/javascript">
		    qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
		    qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
		</script>
	    <?php
		}
	    ?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				    <?php
				    if(isset($_GET["sd"])){
					if($_GET["sd"] == ""){
				?>
				    <input type="hidden" id="sds" name="sd" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
				    }
				?>
				<?php
				    if(isset($_GET["ed"])){
					if($_GET["ed"] == ""){
				?>
				    <input type="hidden" id="eds" name="ed" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
				    }
				?> 
				    <table cellspacing="1" cellpadding="5" border="0">
					<tbody>
					    <tr>
						<td style="padding-right: 10px;"><label>Search</label></td>
					    </tr>
					    <tr>
						<td><input type="text" size="30" autocomplete="off" name="q" id="q" class="txtSBox" placeholder="Enter search query here" value="<?php if(isset($_GET["q"]) && $_GET["q"] != ""){echo $_GET["q"];} ?>" /></td>
						<td>
						    <select name="f" id="f" class="ddlS" style="margin-left: 10px;">
							<option>Select field&nbsp;</option>
						<?php
							foreach($arrFields as $key => $val){
							    $sel = "";
							    if($key == $fld)
							    $sel = "selected='selected'";
						?>
							    <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $val; ?></option>
						<?php
							}
						?>
							
						    </select>
						</td>
						<td style="padding-left: 10px;">
						    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
						</td>
					    </tr>
					    <tr>
						<td>
						    <table border="0">
							<tr>
							    <td>
								<input type="checkbox" id="inc" name="inc" value="1" <?php if(!isset($_GET["sd"])){echo "checked=\"checked\"";}elseif(isset($_GET["sd"]) && $_GET["sd"] != ""){ echo "checked=\"checked\"";} ?> onclick="if(this.checked){document.getElementById('sds').value = document.getElementById('sd').value;document.getElementById('eds').value = document.getElementById('ed').value;}else{document.getElementById('sds').value = '';document.getElementById('eds').value ='';}" <?php if($isDtInc){ echo "checked='checked'"; } ?> />
							    </td>
							    <td>Include date filter</td>
							</tr>
						    </table>
						</td> 
						<td colspan="2" valign="middle" align="right">
						    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
							<tr>
							    <td><a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>" style="color:#128a30;">Download</a></td>
						    <?php
							if($numOfPage > 1){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?>?<?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
						    <?php
							}
							if($isDtInc || $isFiltered){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
						    <?php
							} else {
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
						    <?php
							}
						    ?>
							</tr>
						    </table>
						    
						    
						</td>
					</tbody>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td><span class="dataLabel">Total Visits: <b style='color:#00f;'><?php echo $totVisits; ?></b></span></td>
                                            <td><span class="dataLabel">Total Enquiries: <b style='color:#00f;'><?php echo $totEnquiries; ?></b></span></td>
					</tr>
				    </table>
				</td>
				<td align="right" style="width:50%;"><?php echo $pagingHTML; ?></td>
				
			    </tr>
			</table>
			<table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">#</th>
				<?php
				    $sm = $sortFld == "ref_code" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=ref_code" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=ref_code&sm=" . $sm ?>">Source <?php if ($sortFld == "ref_code") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "visits" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=visits" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=visits&sm=" . $sm ?>&dt=unsigned integer">Visits <?php if ($sortFld == "visits") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "conversions" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=conversions" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=conversions&sm=" . $sm ?>">Leads <?php if ($sortFld == "conversions") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "bookings" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=bookings" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=bookings&sm=" . $sm ?>">Bookings <?php if ($sortFld == "bookings") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    
				    if($i % 2 == 0)
					$cls = "ev";
		    ?>
				    <tr class="<?php echo $cls; ?>">
					<td align="left"><?php echo $srno; ?></td>
					<td align="left"><?php echo $r[$i]["ref_code"]; ?></td>
					<td align="left"><?php echo $r[$i]["visits"]; ?></td>
					<td align="left"><?php echo $r[$i]["conversions"]; ?></td>
					<td align="left">0</td>
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
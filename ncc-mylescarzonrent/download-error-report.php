<?php
	include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('Error', 'start date','end date', 'count'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");
    //mysql_connect('localhost', 'username', 'password');
    //mysql_select_db('database');
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_ErrorReport_ec('".$_GET["sdate"]."','".$_GET["edate"]."')");
    // loop over the rows, outputting them
    for($i = 0;$i< count($rows);$i++)
    {
         fputcsv($output, array($rows[$i]["err"],$_GET["sdate"],$_GET["edate"],$rows[$i]["total"]));        
    }    
?>
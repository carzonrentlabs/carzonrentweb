<?php
    class COR{
        private $soapClient;
        function __construct(){
            $this->soapClient = new SoapClient("http://insta.carzonrent.com/WsIntercityRetail_New/service.asmx?wsdl", array('trace' => 1));
	    $this->soapClientSF = new SoapClient("http://insta.carzonrent.com/SelfDrive_Service/service.asmx?wsdl", array('trace' => 1));
        }
        
        function _CORCheckPhone($arg){
            try{
                $r = $this->soapClient->VerifyPhone($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCheckLogin($arg){
            try{
                $r = $this->soapClient->verifyLogin($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORRegister($arg){
            try{
                $r = $this->soapClient->Registration($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORMakeBooking($arg){
            try{
                $r = $this->soapClient->SetTravelDetails($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCancelBooking($arg){
            try{
                $r = $this->soapClient->CancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORUpdatePayment($arg){
            try{
                $r = $this->soapClient->UpdatePayment($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveUpdatePayment($arg){
            try{
                $r = $this->soapClientSF->SelfDrive_UpdatePayment($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
		
	function _CORSelfDriveCreateBooking_WeekEnd($arg){
		try{
			$r = $this->soapClientSF->SelfDrive_CreateBooking_WeekEnd($arg);
			return $r;
		}
		catch(Exception $e){
			print $e->getMessage();
		}
	}



	function _CORSendMail($arg){
            try{
                $r = $this->soapClient->SenMail($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
		
	
	function _CORSelfDriveCreateBooking($arg){
            try{
                $r = $this->soapClientSF->SelfDrive_CreateBooking_New($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveCancelBooking($arg){
            try{
                $r = $this->soapClientSF->SelfDrive_cancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORGetBookingsDetail($arg){
            try{
                $r = $this->soapClientSF->GetBookingDetail($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
    };
?>
<?php
    //include_once("./includes/check-user.php");
	include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");

	include_once("./includes/encrypt.php");
	session_start();
	error_reporting(0);
	if (!isset($_SESSION['email']) && $_SESSION['email']=="" && !isset($_SESSION['password']) && $_SESSION['password']=="" && $_SESSION['activeuser']!="activeuser")  
	{
		
		$fURL1 = "./";
		header('Location: ' . $fURL1);	
	} 
	
    $fld = "";
    $val = "";
    $isDtInc = false;
    $isFiltered = false;
    $sortFld = "id";
    $sortMode = "desc";
    $sDate = date_create(date("Y-m-d"));
    $eDate = date_create(date("Y-m-d"));
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
    $cPage = $_GET["pg"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "")
		$sDate=date('Y-m-d',strtotime(urldecode($_GET["sd"])));
     
 
      
    if(isset($_GET["ed"]) && $_GET["ed"] != "")
		
	$eDate =date('Y-m-d',strtotime(urldecode($_GET["ed"])));
      
    
    
    
   
    
    if(isset($_GET["inc"]) && $_GET["inc"] != ""){
        $isDtInc = true;
        $isFiltered = true;
    }
    if(isset($_GET["sf"]) && $_GET["sf"] != "")
    $sortFld = $_GET["sf"];
    
    if(isset($_GET["sm"]) && $_GET["sm"] != "")
    $sortMode = $_GET["sm"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "" && isset($_GET["ed"]) && $_GET["ed"] != "")                   
        $filename = "CORLead_overall_" . $sDate . "_" . $eDate;
    else
        $filename = "CORLead_overall_" . $sDate->format('d-m-Y');
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
        $filename .= "_p" . $cPage;
    
    $filename .= ".csv";
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // fetch the data
    //include_once("./includes/config.php");
    //include_once("./classes/bb-mysql.class.php");

    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    if($fld != "" && $fVal != ""){
        $sql = "SELECT * FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
        if($isDtInc){
            $sql .= " AND entry_date BETWEEN '" . $sDate . " 00:00:00' AND '" . $eDate . " 23:59:59' ";
        }
        $sql .= " ORDER BY " . $sortFld . " " . $sortMode;
    } else {
        $sql = "SELECT * FROM cor_leading WHERE entry_date BETWEEN '" . $sDate . " 00:00:00' AND '" . $eDate . " 23:59:59' ORDER BY " . $sortFld . " " . $sortMode;
		
    }
    if(isset($_GET["pg"]) && $_GET["pg"] != ""){
        $recPerPage = 100;
        $lStart = ($cPage - 1) * $recPerPage;
        $sql .= " LIMIT " . $lStart . ", " . $recPerPage;
    }
    $rows = $db->query("query", $sql);    
    // loop over the rows, outputting them
    if(!array_key_exists("response", $rows)){
        // output the column headings
        $fieldNames = array();
        $fieldNames = array_keys($rows[0]);
        fputcsv($output, $fieldNames);
        unset($fieldNames);
        for($i = 0;$i< count($rows);$i++){        
            $ua = browser_info($rows[$i]["ua"]);
            $b = array_keys($ua);
            $v = array_values($ua);
            $rows[$i]["ua"] = ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]);
            $fieldValues = array();
            $fieldValues = $rows[$i];
            fputcsv($output, $fieldValues);
            unset($fieldValues);
        }
    }
?>
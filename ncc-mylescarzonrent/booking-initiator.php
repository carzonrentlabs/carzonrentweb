<?php
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/ajax.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/admin.js" type="text/javascript"></script>	    
	    <script>
	    $(function() {
		    var dateToday = new Date();
		    dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    var dateNextDay = new Date();
		    dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    $( ".datepicker" ).datepicker({minDate: dateToday, maxDate: dateNextDay});
	    });
	    </script>
	    <style type="text/css">
		.wordwrap {
			    white-space: pre-wrap;
			    word-wrap: break-word;
			    }		    
	    </style>
    </head>
    <body>
    <?php     include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> Booking Initiated</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<br><br>
		<div style="margin:0 auto;width:99%;">		
			<form id="frmSearch" name="frmSearch" action="booking-initiator.php" method="get">
			<table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
			    <tbody>
                                <tr>
				    <?php
					$db = new MySqlConnection(CONNSTRING);
					$db->open();
					
					$sDate = "";
					if(isset($_GET["startdate"]) && $_GET["startdate"] != ""){
					    $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
					}
					else {
					    $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
					    $sDate = date_create($maxDate[0]["max_date"]);
					}
					
					$eDate = "";
					if(isset($_GET["enddate"]) && $_GET["enddate"] != ""){
					    $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
					}
					else {
					    $eDate = $sDate;
					}										
				    ?>
				    <td><label>Start Date</label></td>
				    <td>
					<span class="datepick">
					    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
					</span>
				    </td>
				    <td><label>End Date </label>
				    </td>
				    <td><span class="datepick">
						    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
						</span>
				    </td>
				    <td>
					<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
				    </td>
				    <td valign="middle">					
					<a href="download-booking-initiator.php?sdate=<?php echo $sDate->format('Y-m-d 00:00:00'); ?>&edate=<?php echo $eDate->format('Y-m-d 11:59:59'); ?>">Download</a>
				    </td>
                                </tr>
				</tbody>
			</table>
			</form>
			<table width="99%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
			    <tbody id="searchData">			
                                <tr>
				    <td align="center" width="5%" class="search_table">SrNo.</td>
				    <td align="center" width="10%" class="search_table">Tour Type</td>
				    <td align="center" width="10%" class="search_table">Origin</td>
				    <td align="center" width="10%" class="search_table">Destination</td>				    
                                    <td align="center" width="10%" class="search_table">Book Date</td>				                                                                        
                                    <td align="center" width="10%" class="search_table">Travel Date</td>
				    <td align="center" width="10%" class="search_table">Name</td>
                                    <td align="center" width="10%" class="search_table">Email/Mobile</td>
                                    <td align="center" width="10%" class="search_table">CORIC</td>
				    <td align="center" width="10%" class="search_table">Booking ID</td>                                    
                                    <!--<td align="center" width="10%" class="search_table">BROWSER</td>-->
                                </tr>			    
                        <?php					     								
				$r = $db->query("stored procedure", "cor_all_from_cor_bookings('".$sDate->format('Y-m-d 00:00:00')."','".$eDate->format('Y-m-d 23:59:59')."')");				
				if(!array_key_exists("response", $r)){
				    for($i=0 ; $i < count($r); $i++)
				    {				   
					$style = "";					
					if($i%2 == 0)
					    $style = "background-color:#f1f1f1;";
					else
					    $style = "background-color:#dddada;";					
                        ?>
				    <tr style="<?php echo $style ?>">
					<td style="padding:5px !important;word-wrap:break-word;" name = "seqno"><?php echo ($i+1) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["hdTourtype"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["hdOriginName"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["hdDestinationName"] ?></td>					
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('F j, Y', strtotime($r[$i]["entry_date"])) ?></td>															
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('F j, Y', strtotime($r[$i]["hdPickdate"]))?> - <?php echo date('F j, Y', strtotime($r[$i]["hdDropdate"])) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["name"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["email"] ?> / <?php echo $r[$i]["monumber"] ?></td>										
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["coric"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["bookingid"] ?></td>
					<!--<td><?php //echo $r[$i]["user_agent"] ?></td>-->
				    </tr>
                        <?php
				    }
				    unset($r);
				}
				else {
			?>
				    <tr style="background-color:#f1f1f1;">
					<td align="center" colspan="10">No data found.</td>
				    </tr>
			<?php
				}
                        ?>
			    </tbody>
			</table>
			<div style="clear:both"></div>
			<?php
			 if($i >= 50)
                        {
                            ?>
			<table width="100%" cellspacing="1" cellpadding="5" border="0"  style="float: left;margin:15px 0 0 38px;">
			    <tbody id = "loadmore">
				<tr>
				    <td align="right">
					<a href="javascript:void(0);" onclick="javascript: _loadMoreAllFromCOrBooking(<?php echo $i ?>)">LOAD MORE</a>
				    </td>
				</tr>
			    </tbody>
			</table>
			<?php
                        }
                        ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
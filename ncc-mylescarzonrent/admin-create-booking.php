<?php
//    error_reporting(E_ALL);
    //ini_set("display_errors", 1);
    //echo "<pre>";
    include_once("./includes/check-user.php");
    include_once("../includes/cache-func.php");
    include_once('../classes/cor.ws.class.php');
    include_once('../classes/cor.xmlparser.class.php');
    include_once('../classes/cor.mysql.class.php');

    $cor = new COR();
    $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
    $myXML = new CORXMLList();
    $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
    $fullname = explode(" ", $_POST["name"]);
    if(count($fullname) > 1){
         $firstName = $fullname[0];
         $lastName = $fullname[1];
    } else {
         $firstName = $fullname[0];
         $lastName = "";
    }
    $pickDate = date_create($_POST["hdPickdate"]);
    $dropDate = date_create($_POST["hdDropdate"]);
    
    $url = "./booking.php?dir=" . $_POST["hdRURL"];
    $xmlURL = str_ireplace("/pre-trans/", "/trans/", $_POST["hdRURL"]);
    if($_POST["hdTourtype"] == "Selfdrive"){
        $corArrSTD = array();
        $corArrSTD["pkgId"] = $_POST["hdPkgID"];
        $corArrSTD["PickUpdate"] = $pickDate->format('Y-m-d');
        $corArrSTD["PickUptime"] = $_POST["tHourP"] . $_POST["tMinP"];
        $corArrSTD["DropOffDate"] = $dropDate->format('Y-m-d');
        $corArrSTD["DropOfftime"] = $_POST["tHourD"] . $_POST["tMinD"];
        $corArrSTD["FirstName"] = $firstName;
        $corArrSTD["LastName"] = $lastName;
        $corArrSTD["phone"] = $_POST["monumber"];
        $corArrSTD["paymentMode"] = "1";
        $corArrSTD["emailId"] = $_POST["email"];
        $corArrSTD["userId"] = $arrUserId[0]["ClientCoIndivID"];
        $corArrSTD["paymentAmount"] = $_POST["totFare"];
        $corArrSTD["paymentType"] = "3";
        $corArrSTD["paymentStatus"] = "1";
        $corArrSTD["visitedCities"] = $_POST["hdDestinationName"];
        $corArrSTD["AdditionalService"] = "";
        $corArrSTD["trackId"] = $_POST["corid"];
        $corArrSTD["transactionId"] = $_POST["nb_order_no"];
        $corArrSTD["remarks"] = $_POST["remarks"];
        if(isset($_POST["Checksum"]))
        $corArrSTD["chkSum"] = $_POST["Checksum"];
        else
        $corArrSTD["chkSum"] = "0";
        $corArrSTD["OriginCode"] = "ADMIN";
        $corArrSTD["discountPc"] = $_POST["discount"];
        $corArrSTD["discountAmount"] = $_POST["discountAmt"];
        if(trim($_POST["disccode"]) == "Discount coupon number")
        $corArrSTD["DiscountCode"] = "";
        else
        $corArrSTD["DiscountCode"] = $_POST["disccode"];
        if(trim($_POST["empcode"]) == "Promotion code")
        $corArrSTD["PromotionCode"] = "";
        else
        $corArrSTD["PromotionCode"] = $_POST["empcode"];
        
        $res = $cor->_CORSelfDriveCreateBooking($corArrSTD);
        if($res->{'SelfDrive_CreateBookingResult'}->{'any'} != ""){
            $arrUserId = $myXML->xml2ary($res->{'SelfDrive_CreateBookingResult'}->{'any'});
            if($arrUserId[0]["Column1"] > 0){
                $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                $xmlToSave .= "<cortrans>";
                $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                $xmlToSave .= "</cortrans>";
                if(!is_dir("../" . $xmlURL))
                mkdir("../" . $xmlURL);
                if(isset($_POST["corid"]))
                createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-B.xml");
                if(file_exists("../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml"))
                    unlink("../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
                    
               $db = new MySqlConnection(CONNSTRING);
               $db->open();
               $dataToSave = array();
               $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
               $whereData = array();
               $whereData["coric"] = $_POST["corid"];		
               $r = $db->update("cor_booking_new", $dataToSave, $whereData);
               unset($whereData);
               unset($dataToSave);
               $db->close();
               
            } else {
                $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                $xmlToSave .= "<cortrans>";
                $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
                $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
                $xmlToSave .= "</cortrans>";
                if(!is_dir("../" . $xmlURL))
                mkdir("../" . $xmlURL);
                if(isset($_POST["corid"]))
                createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
                
                $dataToMail= array();
                $dataToMail["To"] = "reshav.singla@carzonrent.com;abhishek.bhaskar@carzonrent.com";
                $dataToMail["Subject"] = "Booking Failure: " . $trackId;
                $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
                
                $res = $cor->_CORSendMail($dataToMail);
                unset($dataToMail);
                $url .= "&r=err";
            }
        } else {
            $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
            $xmlToSave .= "<cortrans>";
            $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
            $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
            $xmlToSave .= "</cortrans>";
            if(!is_dir("../" . $xmlURL))
            mkdir("../" . $xmlURL);
            if(isset($_POST["corid"]))
            createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
            
            $dataToMail= array();
            $dataToMail["To"] = "reshav.singla@carzonrent.com;abhishek.bhaskar@carzonrent.com";
            $dataToMail["Subject"] = "Booking Failure: " . $trackId;
            $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
            
            $res = $cor->_CORSendMail($dataToMail);
            unset($dataToMail);
            $url .= "&r=err";
        }
    } else {
        if($_POST["hdTourtype"] != "Local")
            $outStationYN = "true";
        else
            $outStationYN = "false";
        $corArrSTD = array();
        $corArrSTD["pkgId"] = $_POST["hdPkgID"];
        $corArrSTD["destination"] = $_POST["hdDestinationName"];
        $corArrSTD["dateOut"] = $pickDate->format('Y-m-d');
        $corArrSTD["dateIn"] = $dropDate->format('Y-m-d');
        $corArrSTD["TimeOfPickup"] = $_POST["tHour"] . $_POST["tMin"];
        $corArrSTD["address"] = $_POST["address"];
        $corArrSTD["firstName"] = $firstName;
        $corArrSTD["lastName"] = $lastName;
        $corArrSTD["phone"] = $_POST["monumber"];
        $corArrSTD["emailId"] = $_POST["email"];
        $corArrSTD["userId"] = $arrUserId[0]["ClientCoIndivID"];
        $corArrSTD["paymentAmount"] = $_POST["totFare"];
        $corArrSTD["paymentType"] = "3";
        $corArrSTD["paymentStatus"] = "1";
        $corArrSTD["trackId"] = $_POST["corid"];
        $corArrSTD["transactionId"] = $_POST["nb_order_no"];
        $corArrSTD["discountPc"] = $_POST["discount"];
        $corArrSTD["discountAmount"] = $_POST["discountAmt"];
        $corArrSTD["remarks"] = $_POST["remarks"];
        if(isset($_POST["hdDistance"]) && $_POST["hdDistance"] != "")
        $corArrSTD["totalKM"] = $_POST["hdDistance"];
        else
        $corArrSTD["totalKM"] = "0";
        $corArrSTD["visitedCities"] = $_POST["hdDestinationName"];
        $corArrSTD["outStationYN"] = $outStationYN;
        $corArrSTD["OriginCode"] = "ADMIN"; //Aamir 1-Aug-2012
        if(isset($_POST["Checksum"]))
        $corArrSTD["chkSum"] = $_POST["Checksum"];
        else
        $corArrSTD["chkSum"] = "0";
        if(trim($_POST["disccode"]) == "Discount coupon number")
        $corArrSTD["DiscountCode"] = "";
        else
        $corArrSTD["DiscountCode"] = $_POST["disccode"];
        if(trim($_POST["empcode"]) == "Promotion code")
        $corArrSTD["PromotionCode"] = "";
        else
        $corArrSTD["PromotionCode"] = $_POST["empcode"];
        
        $res = $cor->_CORMakeBooking($corArrSTD);
        //print_r($res);
        //echo "<br /><pre>";
        //print_r($corArrSTD);
        if($res->{'SetTravelDetailsResult'}->{'any'} != ""){
            $arrUserId = $myXML->xml2ary($res->{'SetTravelDetailsResult'}->{'any'});
            if($arrUserId[0]["Column1"] > 0){
                $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                $xmlToSave .= "<cortrans>";
                $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                $xmlToSave .= "</cortrans>";
                if(!is_dir("../" . $xmlURL))
                mkdir("../" . $xmlURL);
                if(isset($_POST["corid"]))
                createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-B.xml");
                if(file_exists("../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml"))
                    unlink("../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
               
               $dataToSave = array();
               $dataToSave["BookingID"] = intval($arrUserId[0]["Column1"]);
               $dataToSave["PaymentStatus"] = 1;
               $dataToSave["PaymentAmount"] = intval($_POST["totFare"]);
               $dataToSave["TransactionId"] = $_POST["nb_order_no"];
               $dataToSave["ckhSum"] = $_POST["Checksum"];
               $dataToSave["Trackid"] = $_POST["corid"];
               $res = $cor->_CORSelfDriveUpdatePayment($dataToSave);
               unset($dataToSave);
                            
               $db = new MySqlConnection(CONNSTRING);
               $db->open();
               $dataToSave = array();
               $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
               $whereData = array();
               $whereData["coric"] = $_POST["corid"];		
               $r = $db->update("cor_booking_new", $dataToSave, $whereData);
               unset($whereData);
               unset($dataToSave);
               $db->close();
               
            } else {
                $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                $xmlToSave .= "<cortrans>";
                $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
                $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
                $xmlToSave .= "</cortrans>";
                if(!is_dir("../" . $xmlURL))
                mkdir("../" . $xmlURL);
                if(isset($_POST["corid"]))
                createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
                
                $dataToMail= array();
                $dataToMail["To"] = "reshav.singla@carzonrent.com;abhishek.bhaskar@carzonrent.com";
                $dataToMail["Subject"] = "Booking Failure: " . $trackId;
                $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
                
                $res = $cor->_CORSendMail($dataToMail);
                unset($dataToMail);
                $url .= "&r=err";
            }
        } else {
            $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
            $xmlToSave .= "<cortrans>";
            $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
            $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
            $xmlToSave .= "</cortrans>";
            if(!is_dir("../" . $xmlURL))
            mkdir("../" . $xmlURL);
            if(isset($_POST["corid"]))
            createcache($xmlToSave, "../" . $xmlURL . "/" . $_POST["corid"] . "-E.xml");
            
            $dataToMail= array();
            $dataToMail["To"] = "reshav.singla@carzonrent.com;abhishek.bhaskar@carzonrent.com";
            $dataToMail["Subject"] = "Booking Failure: " . $trackId;
            $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
            
            $res = $cor->_CORSendMail($dataToMail);
            unset($dataToMail);
            $url .= "&r=err";
        }        
    }
    header("Location: " . $url);
?>
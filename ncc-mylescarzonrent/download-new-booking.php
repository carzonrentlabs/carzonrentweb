<?php
	include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('Tour Type', 'Origin', 'Destination','Book Date','Travel Date','Name','Email/Mobile','CORIC','Booking ID'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_downloadCorBookingData('".$_GET["sdate"]."','".$_GET["edate"]."')");  
    // loop over the rows, outputting them
    for($i = 0;$i< count($rows);$i++)
    {
         fputcsv($output, array($rows[$i]["hdTourtype"],$rows[$i]["hdOriginName"],$rows[$i]["hdDestinationName"],date('F j, Y', strtotime($rows[$i]["entry_date"])),date('F j, Y', strtotime($rows[$i]["hdPickdate"]))." - ".date('F j, Y', strtotime($rows[$i]["hdDropdate"])),$rows[$i]["name"],$rows[$i]["email"]." / ".$rows[$i]["monumber"],$rows[$i]["coric"],$rows[$i]["bookingid"]));        
    }    
?>
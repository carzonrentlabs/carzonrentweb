<?php
session_start();
 if($_REQUEST['location']){
        $loc = $_REQUEST['location'];
    }else{
        $loc = '';
    }
if(!isset($_SESSION["email"]) && $_SESSION["email"]=="")
{

	$fURL1 = "./";
	header('Location: ' . $fURL1);	
} 
else
{

   // include_once("./includes/check-user.php");
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Easycabs Feedback | Carzonrent ncc-mylescarzonrent</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		         include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Easycabs FeedBack</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./easycabs-feedback.php";
			$dlndURL = "./myles-feedback-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "unique_id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("booking_id" => "Booking id");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			if(isset($_GET["f"]) && $_GET["f"] != ""){
			    $fld = $_GET["f"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["q"]) && $_GET["q"] != ""){
			    $fVal = $_GET["q"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["inc"]) && $_GET["inc"] != ""){
			    $isDtInc = true;
			    $isFiltered = true;
			}
			
			if(isset($_GET["sf"]) && $_GET["sf"] != "")
			$sortFld = $_GET["sf"];
			
			if(isset($_GET["sm"]) && $_GET["sm"] != "")
			$sortMode = $_GET["sm"];
			
			if(isset($_GET["dt"]) && $_GET["dt"] != "")
			    $sortDType = "CONVERT(easycabs_booking." . $sortFld . ", " . $_GET["dt"] . ")";
			else {
			    if(isset($_GET["sf"]) && $_GET["sf"] != "")
			    $sortDType = $sortFld;
			    else
			    $sortDType = "CONVERT(easycabs_booking." . $sortFld . ", unsigned integer)";		    
			}
			//$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    
			
			$lStart = ($cPage - 1) * $recPerPage;			
			$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(easycabs_feedback.unique_id) as TotalRecord FROM easycabs_feedback LEFT JOIN easycabs_booking ON easycabs_feedback.booking_id=easycabs_booking.booking_id WHERE easycabs_feedback" . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date_feedback BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
                            if($_REQUEST['location']!=''){
                                $sql.=" and easycabs_booking.pickup_location='$loc'";
                            }
                            
			} else {
			    $sql = "SELECT COUNT(easycabs_feedback.unique_id) as TotalRecord FROM easycabs_feedback LEFT JOIN easycabs_booking ON easycabs_feedback.booking_id=easycabs_booking.booking_id WHERE entry_date_feedback BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
                            if($_REQUEST['location']!=''){
                                $sql.=" and easycabs_booking.pickup_location='$loc'";
                            }
			}
                        //echo $sql;
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			//$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT easycabs_feedback.*,easycabs_booking.pickup_location FROM easycabs_feedback LEFT JOIN easycabs_booking ON easycabs_feedback.booking_id=easycabs_booking.booking_id WHERE easycabs_feedback." . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date_feedback BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ";
			    }
                            if($_REQUEST['location']!=''){
                                $sql.=" and easycabs_booking.pickup_location='$loc'";
                            }
			    $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			} else {
                            //SELECT easycabs_feedback.*,easycabs_booking.pickup_location FROM easycabs_feedback LEFT JOIN easycabs_booking ON easycabs_feedback.booking_id=easycabs_booking.booking_id WHERE entry_date_feedback BETWEEN '2015-10-01 00:00:00' AND '2015-10-10 23:59:59' AND easycabs_booking.pickup_location = '3' ORDER BY CONVERT(easycabs_feedback.unique_id, unsigned integer) desc LIMIT 0, 100 
			    $sql = "SELECT easycabs_feedback.*,easycabs_booking.pickup_location FROM easycabs_feedback LEFT JOIN easycabs_booking ON easycabs_feedback.booking_id=easycabs_booking.booking_id WHERE entry_date_feedback BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
                            if($_REQUEST['location']!=''){
                                $sql.=" and easycabs_booking.pickup_location='$loc'";
                            }
                            $sql .="ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
                            //$sql = "SELECT * FROM easycabs_feedback WHERE entry_date_feedback BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			}
			//echo $sql;
                        //for ttl avg
                        $row = $db->query("query", $sql);
                        $ttlavg = '0';	
                        if(!array_key_exists("response", $row)){
			
                        for($i = 0; $i < count($row); $i++){
                            $avg = ($row[$i]["booking_expirience"]+$row[$i]["reporting_time"]+$row[$i]["customer_satisfaction"]+$row[$i]["driver_behaviour"])/4;
                            $ttlavg += $avg;
                        }}
                        
                        //end code
                        
			$r = $db->query("query", $sql);
			
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
			$db->close();
			//print_r($r);
			
			$pagingHTML = "";
			if($numOfPage > 1){
			    $nURL = "";
			    $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));			    
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    $pagingHTML .= "<td>";
				    if($cPage % $maxPageNum != 0)
					$pgStart = $cPage - ($cPage % $maxPageNum);
				    else
					$pgStart = $cPage - $maxPageNum;
				    $pgStart++;
				    if($cPage > $maxPageNum){
					$pgURL = $nURL;
					$pgPrev = $pgStart -  1;
					$pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
				    }
				    for($p = $pgStart; $p <= $numOfPage; $p++){
					$pgURL = $nURL;
					if($p != $cPage){
					    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
					}
					else
					    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
					if($p % $maxPageNum == 0)
					break;
				    }
				    if($numOfPage > $p){
					$p++;
					$pgURL = $nURL;
					$pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
				    }
				    $pgURL = $nURL;
				    $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
			    $pagingHTML .= "Go to page number <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
				    for($p = 1; $p <= $numOfPage; $p++){
					if($p != $cPage){
					    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
					} else {
					    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
					}
				    }
					$pagingHTML .= "</select>";
				    $pagingHTML .= "</td>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			} else {
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			}
		?>
	    <?php
		if($_SERVER["QUERY_STRING"] != ""){
	    ?>
		<script type="text/javascript">
	    <?php
		    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
		    for($q = 0; $q < count($qsParams); $q++){
			if(trim($qsParams[$q]) != ""){
			    $qsParam = explode("=", $qsParams[$q]);
	    ?>
			    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
	    <?php
			}
		    }
	    ?>
		</script>
	    <?php
		} else {
	    ?>
		<script type="text/javascript">
		    qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
		    qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
		</script>
	    <?php
		}
	    ?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
                                            
                                            <td style="padding-left: 10px;">
                                            <select id="location" class="ddlS" style="margin-left: 10px;" name="location">
                                                    <option selected="selected" value="">-Select-</option>
                                                    <option value="3" <?php if($loc=='3'){?>selected="selected"<?php }?>>Bangalore</option>
                                                    <option value="1" <?php if($loc=='1'){?>selected="selected"<?php }?>>Delhi</option>
                                                    <option value="4" <?php if($loc=='4'){?>selected="selected"<?php }?>>Hyderabad</option>
                                                    <option value="2" <?php if($loc=='5'){?>selected="selected"<?php }?>>Mumbai</option>
                                            </select></td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				    <?php
				    if(isset($_GET["sd"])){
					if($_GET["sd"] == ""){
				?>
				    <input type="hidden" id="sds" name="sd" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
				    }
				?>
				<?php
				    if(isset($_GET["ed"])){
					if($_GET["ed"] == ""){
				?>
				    <input type="hidden" id="eds" name="ed" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
				    }
				?> 
				    <table cellspacing="1" cellpadding="5" border="0">
					<tbody>
					    <tr>
						<td style="padding-right: 10px;"><label>Search</label></td>
					    </tr>
					    <tr>
						<td><input type="text" size="30" autocomplete="off" name="q" id="q" class="txtSBox" placeholder="Enter Booking Id" value="<?php if(isset($_GET["q"]) && $_GET["q"] != ""){echo $_GET["q"];} ?>" /></td>
						<td>
						    <select name="f" id="f" class="ddlS" style="margin-left: 10px;">
							
						<?php
							foreach($arrFields as $key => $val){
							    $sel = "";
							    if($key == $fld)
							    $sel = "selected='selected'";
						?>
							    <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $val; ?></option>
						<?php
							}
						?>
							
						    </select>
						</td>
						<td style="padding-left: 10px;">
						    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
						</td>
					    </tr>
					    <tr>
						<td>
						    <table border="0">
							
						    </table>
						</td> 
						
					</tbody>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td><span class="dataLabel">Total Feedback: <b style='color:#00f;'><?php echo $totRec; ?></b></span></td>
					
					    <td><span class="dataLabel">Total Average: <b style='color:#00f;'><?php echo $ttlavg;?></b></span></td>
					</tr>
                                            
				    </table>
				</td>
				<td align="right" style="width:50%;"><?php echo $pagingHTML; ?></td>
				
			    </tr>
			</table>
			<div style="float:left;width:99%;overflow-x: scroll">
			<div style="float:left;width:100%;overflow-x: hidden">
			<table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">#</th>
				<?php
				    $sm = $sortFld == "booking_id" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=booking_id" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=booking_id&sm=" . $sm ?>">Booking Id <?php if ($sortFld == "booking_id") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "booking_expirience" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=booking_expirience" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=booking_expirience&sm=" . $sm ?>&dt=unsigned integer">Booking Experience <?php if ($sortFld == "booking_expirience") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reporting_time" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reporting_time" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reporting_time&sm=" . $sm ?>">Reporting Time <?php if ($sortFld == "reporting_time") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "customer_satisfaction" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=customer_satisfaction" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=customer_satisfaction&sm=" . $sm ?>&dt=datetime">Customer Satifaction<?php if ($sortFld == "customer_satisfaction" || $sortFld == "unique_id") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "driver_behaviour" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=driver_behaviour" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=driver_behaviour&sm=" . $sm ?>&dt=datetime">Driver Behaviours <?php if ($sortFld == "driver_behaviour" || $sortFld == "unique_id") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                    <th align="left">Average </th>
				<?php
				    $sm = $sortFld == "user_comment" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=user_comment" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=user_comment&sm=" . $sm ?>">User Comment<?php if ($sortFld == "user_comment") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "entry_date_feedback" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=entry_date_feedback" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date_feedback&sm=" . $sm ?>">Entry Date <?php if ($sortFld == "entry_date_feedback") { ?><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
					
					 <th align="left">View Details</a></th>
					
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
					
				    if($i % 2 == 0)
					$cls = "ev";
				    if($r[$i]["unique_id"] == 1 && ($r[$i]["unique_id"] == 1 || $r[$i]["unique_id"] == 3) && is_null($r[$i]["booking_id"]) && $r[$i]["AuthDesc"] == 'Y'){
					$cls = "er";
					$vTxt .= "&nbsp;<a href='javascript: void(0)' title='Failed booking. Click on View Details to create booking of " . $r[$i]["unique_id"] . "'>?</a>";
				    }
				    $payMode = "-";
				    switch($r[$i]["unique_id"]){
					case 1:
					    $payMode = "Online";
					break;
					case 2:
					    $payMode = "Pay to driver";
					break;
				    }
				    $payStatus = "-";
				    if(intval($r[$i]["unique_id"]) == 1){
					switch($r[$i]["unique_id"]){
					    case 1:
						$payStatus = "Success";
						break;
					    case 2:
						$payStatus = "Gateway";
						break;
					    case 3:
						$payStatus = "Payment";
						break;
					    case 4:
						$payStatus = "Cancelled";
						break;
					    case 5:
						$payStatus = "Unauthorised";
					}
				    } elseif(intval($r[$i]["unique_id"]) == 2){
					switch($r[$i]["unique_id"]){
					    case 1:
						$payStatus = "Success";
						break;
					}
				    }
				    //elseif($r[$i]["AuthDesc"] == 'N')
				    //$payStatus = "Unauthorised";
				    $ua = browser_info($r[$i]["unique_id"]);
				    //echo $r[$i]["ua"] . "<pre>";
				    $b = array_keys($ua);
				    $v = array_values($ua);
				    $bookDate = date_create($r[$i]["entry_date_feedback"]);
				    //print_r($r);
                                    $avg = ($r[$i]["booking_expirience"]+$r[$i]["reporting_time"]+$r[$i]["customer_satisfaction"]+$r[$i]["driver_behaviour"])/4;
                                   
		    ?>            
                                
				    <tr class="<?php echo $cls; ?>">
				      <td align="left"><?php echo $srno; ?></td>
				        <td align="left"><?php echo $r[$i]["booking_id"]; ?></td>
				        <td align="left"><?php echo $r[$i]["booking_expirience"]; ?></td>
						 <td align="left"><?php echo $r[$i]["reporting_time"]; ?></td>
				        <td align="left"><?php echo $r[$i]["customer_satisfaction"]; ?></td>						
						<td align="left"><?php echo $r[$i]["driver_behaviour"]; ?></td>
                                                <td align="left"><?php echo $avg; ?></td>
				        <td align="left"><?php echo strip_tags($r[$i]["user_comment"]); ?></td>					
				        <td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookDate->format('H:i A'); ?></small></td>
						<td align="left"><?PHP echo  $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showFeedbackDetails('".$r[$i]["booking_id"] . "')\" title=\"Click on view details of " . $r[$i]["booking_id"] . "\">Details</a>";?></td>
				       
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
		</div>
			</div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<?PHP
}
?>
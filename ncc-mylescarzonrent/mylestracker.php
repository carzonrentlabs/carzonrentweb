<?php
    ob_start();
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
		<head>
		<title>Booking Status | Carzonrent Admin</title>
		<?php
		include_once("./includes/cache-func.php");
		include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
		include_once("./includes/config.php");
		include_once("./classes/bb-mysql.class.php");
		include_once("./classes/cor.ws.class.php");
		include_once("./classes/cor.xmlparser.class.php");
		$coric=0;
		$db = new MySqlConnection(CONNSTRING);
	    $db->open();
		if(isset($_REQUEST['q']))
		{
			$coric=$_REQUEST['q'];
			$selectData="select  * from cor_booking_new where coric='".$coric."'";
			$r = $db->query("query", $selectData);
	}
		
		
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Payback Status</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <form id="" name="" action="./mylestracker.php" method="GET" onsubmit="return validate();">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td colspan="2" style="padding-left: 12px;">Enter CORIC ID</td>
					    <td style="padding-left: 10px;"><input type="text" id="q" name="q" class="txtSBox" size="22" placeholder="Enter here" <?php if(isset($_GET["q"])) { echo "value=" . $_GET["q"]; } ?> /></td>
					    <td style="padding-left: 15px;"><input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" /></td>
					</tr>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			  <?php 
			  if($coric!="")
			  {
			  
			  if(!array_key_exists("response", $r))
			  {
				$source 			= $r[0]["source"];
				$bookingid 			= $r[0]["booking_id"];
				$tourtype 			= $r[0]["tour_type"];
				$carcat 			= $r[0]["car_cat"];
				$origin_name 		= $r[0]["origin_name"];
				$destinations_name  = $r[0]["destinations_name"];
				$full_name 			= $r[0]["full_name"];
				$email				= $r[0]["email_id"];
				$pickup_date 		= $r[0]["pickup_date"];
				$promotion_code 	= $r[0]["promotion_code"];
				$discount_coupon 	= $r[0]["discount_coupon"];
				$additional_srv 	= $r[0]["additional_srv"];
				$additional_srv_amt = $r[0]["additional_srv_amt"];
				$chauffeur_charges  = $r[0]["chauffeur_charges"];
				$pkg_type 			= $r[0]["pkg_type"];
				$pb_status 			= $r[0]["pb_status"];
				$isPayBack 			= $r[0]["isPayBack"];
				$payment_status 	= $r[0]["payment_status"];
				$WeekDayDuration 	= $r[0]["WeekDayDuration"];
				$WeekEndDuration 	= $r[0]["WeekEndDuration"];
				$FreeDuration 		= $r[0]["FreeDuration"];
				$trip_type 			= $r[0]["trip_type"];
				$User_billable_amount = $r[0]["User_billable_amount"];
				$payment_mode = $r[0]["payment_mode"];
				$discount_amt = $r[0]["discount_amt"];
				
			  ?>
				<table cellpadding="5" cellspacing="1" class="adm" border="0" style="margin: 10px 1.5%; width: 97%;float: left;">
				   
				   <tr class="od">
					<td>Source</td>
					<td><?php echo $source; ?></td>
					<td>Booking ID</td>
					<td><?php echo $bookingid; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Tour Type</td>
					<td><?php echo $tourtype; ?></td>
					<td>Car Cat</td>
					<td><?php echo $carcat?></td>
				    </tr>
				    <tr class="od">
					<td>Orgin Name </td>
					<td><?php echo $origin_name ?></td>
					<td>Destination name</td>
					<td><?php echo $destinations_name ?></td>
				    </tr>
				    <tr class="ev">
					<td>Email</td>
					<td><?php echo $email; ?></td>
					<td>Full Name</td>
					<td><?php echo $full_name ?></td>
				    </tr>
				    <tr class="od">
					<td>Payment Status</td>
					<td><?php echo $payment_status ?></td>
					<td>Payment Amount</td>
					<td><?php echo $User_billable_amount; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Tour Type</td>
					<td><?php echo $tourtype; ?></td>
					<td>Payment Status</td>
					<td><?php echo $payment_mode; ?></td>
				    </tr>
				    <tr class="od">
					<td>Discount Amount</td>
					<td><?php echo $discount_amt; ?></td>
					<td>payback status</td>
					<td><?php echo $pb_status; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Trip Type</td>
					<td><?php echo $trip_type; ?></td>
					<td>Ragular Day </td>
					<td><?php echo $WeekDayDuration; ?></td>
				    </tr>
					
					<tr class="od">
					<td>Premium Day</td>
					<td><?php echo $WeekEndDuration; ?></td>
					<td>User Billable Amount</td>
					<td><?php echo $User_billable_amount; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Discount Coupon</td>
					<td><?php echo $discount_coupon; ?></td>
					<td>Promotion Code </td>
					<td><?php echo $promotion_code; ?></td>
				    </tr>
					</table>
					
					
				
			<?php
			
			}
			else
			{
			?>
				<table width="100%">
				<tr><td class="od" align="center" colspan='4' style="color:red;">No Record found</td></tr>
				</table>
			<?php
			}
			?>
			
			<?php
			}
			
			?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p <?php if(!isset($_GET["q"])) { echo "style=\"height:300px;\""; } ?>>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	unset($bookingDetails);
	?>
    </body>
</html>
<script>
function validate()
{
 var code = $('#q').val();
 if(code=="")
 {
	 alert('Please Enter CorIC ID');
	 $('#q').css('background-color','pink').attr('placeholder','Please Enter CorIC ID').focus();
	 return false;
 }
}
</script>
<?php
	include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('Book Date','Booking ID', 'Keyword', 'Mobile','Name','Pickup Address','Pickup Datetime','Location','Landmark','Drop Location', 'Drop Landmark','IP','Browser', 'User ID', 'User Name', 'User Email', 'User Phone'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_downloadCorBookingData_ec('".$_GET["sdate"]."','".$_GET["edate"]."')");
    // loop over the rows, outputting them
    $dispLocation = "";
    if(!array_key_exists("response", $rows)){
        for($i = 0;$i< count($rows);$i++)
        {
            switch ($rows[$i]["pickup_location"]) {
                case 1:
                    $dispLocation = "Delhi";
                    break;
                case 2:
                    $dispLocation = "Mumbai";
                    break;
                case 3:
                    $dispLocation = "Bangalore";
                    break;
                case 4:
                    $dispLocation = "Hyderabad";
                    break;
            }
    
             fputcsv($output, array(date('F j, Y', strtotime($rows[$i]["entry_datetime"])), $rows[$i]["booking_id"],$rows[$i]["keyword"],$rows[$i]["cust_mobile"],$rows[$i]["guest_name"],$rows[$i]["pickup_address"],date('F j, Y', strtotime($rows[$i]["pickup_datetime"])),$dispLocation,$rows[$i]["pickup_landmark"],$rows[$i]["drop_landmark"],$rows[$i]["dest_address"],$rows[$i]["ip"] ,$rows[$i]["ua"], $rows[$i]["userid"], $rows[$i]["username"], $rows[$i]["useremail"], $rows[$i]["userphone"]));        
        }
    }
?>
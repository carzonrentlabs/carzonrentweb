<?php
    include_once("./includes/check-user.php");
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Myles Feedback | Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles FeedBack</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./myles-feedback.php";
			$dlndURL = "./myles-feedback-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "uid";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("booking_id" => "Booking id", "purpose_of_trip" => "Purpose Of Trip", "other_purpose" => "Other", "reason_1" => "Reason 1", "reason_2" => "Reason 2", "reason_3" => "Reason 3", "reason_4" => "Reason 4", "reason_5" => "Reason 5", "reason_6" => "Reason 6", "reason_7" => "Reason 7", "other_reason" => "Other Reason", "experience" => "Experience", "experience_1" => "Experience 1", "experience_2" => "Experience 2", "experience_3" => "Experience 3", "experience_4" => "Experience 4", "experience_5" => "Experience 5", "experience_6" => "Experience 6", "experience_7" => "Experience 7", "other_experience" => "Other Experience","feedback" => "Feedback", "entry_date" => "Entry date", "ip" => "IP", "ua" => "Browser", "CarCatName" => "CarCatName", "destination" => "Destination", "PickUpTime" => "PickUpTime", "PickUpAdd" => "PickUpAdd", "Fname" => "Fname", "Lname" => "Lname", "mobile" => "Mobile", "EmailID" => "Email ID", "BookingSource" => "BookingSource", "userid" => "userid", "PaymentStatus" => "PaymentStatus", "PaymentAmount" => "PaymentAmount", "trackID" => "trackID", "VisitedCities" => "VisitedCities", "OutstationYN" => "OutstationYN", "Remarks" => "Remarks", "IndicatedDiscPC" => "IndicatedDiscPC", "PickUpDate" => "PickUpDate", "DropOffDate" => "DropOffDate", "OriginName" => "OriginName", "originId" => "originId", "Status" => "Status", "TourType" => "TourType", "IndicatedPkgID" => "IndicatedPkgID", "DiscountAmount" => "DiscountAmount", "ChauffeurCharges" => "ChauffeurCharges", "DropOffTime" => "DropOffTime", "serviceAmount" => "serviceAmount", "PayBackMember" => "PayBackMember", "subLocationID" => "subLocationID", "convenient_booking_process" => "convenient_booking_process", "cab_report_time" => "cab_report_time", "chauffer_experience" => "chauffer_experience", "expectation_met" => "expectation_met", "overall_experience" => "overall_experience");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			if(isset($_GET["f"]) && $_GET["f"] != ""){
			    $fld = $_GET["f"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["q"]) && $_GET["q"] != ""){
			    $fVal = $_GET["q"];
			    $isFiltered = true;
			    if($fld != ""){
				if($fld == "payment_mode"){
				    if(trim(strtolower($fVal)) == "online")
					$fVal = 1;
				    elseif(trim(strtolower($fVal)) == "pay to driver")
					$fVal = 2;
				} elseif($fld == "payment_status"){
				    if(trim(strtolower($fVal)) == "success")
					$fVal = 1;
				    elseif(trim(strtolower($fVal)) == "gateway")
					$fVal = 2;
				    elseif(trim(strtolower($fVal)) == "payment")
					$fVal = 3;
				    elseif(trim(strtolower($fVal)) == "failure" || trim(strtolower($fVal)) == "cancelled")
					$fVal = 4;
				    elseif(trim(strtolower($fVal)) == "unauthorised")
					$fVal = 5;
				}				
			    }
			}
			
			if(isset($_GET["inc"]) && $_GET["inc"] != ""){
			    $isDtInc = true;
			    $isFiltered = true;
			}
			
			if(isset($_GET["sf"]) && $_GET["sf"] != "")
			$sortFld = $_GET["sf"];
			
			if(isset($_GET["sm"]) && $_GET["sm"] != "")
			$sortMode = $_GET["sm"];
			
			if(isset($_GET["dt"]) && $_GET["dt"] != "")
			    $sortDType = "CONVERT(" . $sortFld . ", " . $_GET["dt"] . ")";
			else {
			    if(isset($_GET["sf"]) && $_GET["sf"] != "")
			    $sortDType = $sortFld;
			    else
			    $sortDType = "CONVERT(" . $sortFld . ", unsigned integer)";		    
			}
			//$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    
			
			$lStart = ($cPage - 1) * $recPerPage;			
			$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM myles_feedback WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM myles_feedback WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			//$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT * FROM myles_feedback WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ";
			    }
			    $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			} else {
			    $sql = "SELECT * FROM myles_feedback WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			}
			$r = $db->query("query", $sql);
			
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
			$db->close();
			//print_r($r);
			
			$pagingHTML = "";
			if($numOfPage > 1){
			    $nURL = "";
			    $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));			    
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    $pagingHTML .= "<td>";
				    if($cPage % $maxPageNum != 0)
					$pgStart = $cPage - ($cPage % $maxPageNum);
				    else
					$pgStart = $cPage - $maxPageNum;
				    $pgStart++;
				    if($cPage > $maxPageNum){
					$pgURL = $nURL;
					$pgPrev = $pgStart -  1;
					$pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
				    }
				    for($p = $pgStart; $p <= $numOfPage; $p++){
					$pgURL = $nURL;
					if($p != $cPage){
					    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
					}
					else
					    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
					if($p % $maxPageNum == 0)
					break;
				    }
				    if($numOfPage > $p){
					$p++;
					$pgURL = $nURL;
					$pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
				    }
				    $pgURL = $nURL;
				    $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
			    $pagingHTML .= "Go to page number <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
				    for($p = 1; $p <= $numOfPage; $p++){
					if($p != $cPage){
					    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
					} else {
					    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
					}
				    }
					$pagingHTML .= "</select>";
				    $pagingHTML .= "</td>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			} else {
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			}
		?>
	    <?php
		if($_SERVER["QUERY_STRING"] != ""){
	    ?>
		<script type="text/javascript">
	    <?php
		    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
		    for($q = 0; $q < count($qsParams); $q++){
			if(trim($qsParams[$q]) != ""){
			    $qsParam = explode("=", $qsParams[$q]);
	    ?>
			    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
	    <?php
			}
		    }
	    ?>
		</script>
	    <?php
		} else {
	    ?>
		<script type="text/javascript">
		    qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
		    qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
		</script>
	    <?php
		}
	    ?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				    <?php
				    if(isset($_GET["sd"])){
					if($_GET["sd"] == ""){
				?>
				    <input type="hidden" id="sds" name="sd" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
				    }
				?>
				<?php
				    if(isset($_GET["ed"])){
					if($_GET["ed"] == ""){
				?>
				    <input type="hidden" id="eds" name="ed" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
				    }
				?> 
				    <table cellspacing="1" cellpadding="5" border="0">
					<tbody>
					    <tr>
						<td style="padding-right: 10px;"><label>Search</label></td>
					    </tr>
					    <tr>
						<td><input type="text" size="30" autocomplete="off" name="q" id="q" class="txtSBox" placeholder="Enter search query here" value="<?php if(isset($_GET["q"]) && $_GET["q"] != ""){echo $_GET["q"];} ?>" /></td>
						<td>
						    <select name="f" id="f" class="ddlS" style="margin-left: 10px;">
							<option>Select field&nbsp;</option>
						<?php
							foreach($arrFields as $key => $val){
							    $sel = "";
							    if($key == $fld)
							    $sel = "selected='selected'";
						?>
							    <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $val; ?></option>
						<?php
							}
						?>
							
						    </select>
						</td>
						<td style="padding-left: 10px;">
						    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
						</td>
					    </tr>
					    <tr>
						<td>
						    <table border="0">
							<tr>
							    <td>
								<input type="checkbox" id="inc" name="inc" value="1" <?php if(!isset($_GET["sd"])){echo "checked=\"checked\"";}elseif(isset($_GET["sd"]) && $_GET["sd"] != ""){ echo "checked=\"checked\"";} ?> onclick="if(this.checked){document.getElementById('sds').value = document.getElementById('sd').value;document.getElementById('eds').value = document.getElementById('ed').value;}else{document.getElementById('sds').value = '';document.getElementById('eds').value ='';}" <?php if($isDtInc){ echo "checked='checked'"; } ?> />
							    </td>
							    <td>Include date filter</td>
							</tr>
						    </table>
						</td> 
						<td colspan="2" valign="middle" align="right">
						    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
							<tr>
							    <td><a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>" style="color:#128a30;">Download</a></td>
						    <?php
							if($numOfPage > 1){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?>?<?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
						    <?php
							}
							if($isDtInc || $isFiltered){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
						    <?php
							} else {
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
						    <?php
							}
						    ?>
							</tr>
						    </table>
						</td>
					</tbody>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td><span class="dataLabel">Total Feedback: <b style='color:#00f;'><?php echo $totRec; ?></b></span></td>
					</tr>
				    </table>
				</td>
				<td align="right" style="width:50%;"><?php echo $pagingHTML; ?></td>
				
			    </tr>
			</table>
			<div style="float:left;width:99%;overflow-x: scroll">
			<div style="float:left;width:700%;overflow-x: hidden">
			<table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">#</th>
				<?php
				    $sm = $sortFld == "booking_id" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=booking_id" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=booking_id&sm=" . $sm ?>">Booking Id <?php if ($sortFld == "booking_id") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "purpose_of_trip" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=purpose_of_trip" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=purpose_of_trip&sm=" . $sm ?>&dt=unsigned integer">Purpose of Trip <?php if ($sortFld == "purpose_of_trip") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "other_purpose" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=other_purpose" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=other_purpose&sm=" . $sm ?>">Other purpose <?php if ($sortFld == "other_purpose") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_1" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_1" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_1&sm=" . $sm ?>&dt=datetime">Reason 1 <?php if ($sortFld == "reason_1" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_2" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_2" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_2&sm=" . $sm ?>&dt=datetime">Reason 2 <?php if ($sortFld == "reason_2" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_3" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_3" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_3&sm=" . $sm ?>">Reason 3 <?php if ($sortFld == "reason_3") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_4" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_4" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_4&sm=" . $sm ?>">Reason 4 <?php if ($sortFld == "reason_4") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_5" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_5" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_5&sm=" . $sm ?>">Reason 5 <?php if ($sortFld == "reason_5") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_6" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_6" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_6&sm=" . $sm ?>">Reason 6 <?php if ($sortFld == "reason_6") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "reason_7" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=reason_7" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=reason_7&sm=" . $sm ?>">Reason 7 <?php if ($sortFld == "reason_7") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				    <th align="left">Other Reason</th>
				<?php
				    $sm = $sortFld == "experience" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience&sm=" . $sm ?>">Experience<?php if ($sortFld == "experience") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				    
				<?php
				    $sm = $sortFld == "experience_1" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_1" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_1&sm=" . $sm ?>&dt=datetime">Experience 1 <?php if ($sortFld == "experience_1" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_2" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_2" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_2&sm=" . $sm ?>&dt=datetime">Experience 2 <?php if ($sortFld == "experience_2" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_3" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_3" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_3&sm=" . $sm ?>">Experience 3 <?php if ($sortFld == "experience_3") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_4" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_4" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_4&sm=" . $sm ?>">Experience 4 <?php if ($sortFld == "experience_4") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_5" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_5" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_5&sm=" . $sm ?>">Experience 5 <?php if ($sortFld == "experience_5") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_6" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_6" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_6&sm=" . $sm ?>">Experience 6 <?php if ($sortFld == "experience_6") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "experience_7" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=experience_7" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=experience_7&sm=" . $sm ?>">Experience 7 <?php if ($sortFld == "experience_7") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				    <th align="left">Other Experience</th>
				    <th align="left">Feedback</th>
				<?php
				    $sm = $sortFld == "entry_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date&sm=" . $sm ?>&dt=datetime">Enquired On <?php if ($sortFld == "entry_date" || $sortFld == "unique_id") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "ip" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=ip&sm=" . $sm ?>">IP <?php if ($sortFld == "ip") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a><br ><small>Browser</small></th>
				    
				    <th align="left">CarCatName</th>
				    <th align="left">Destination</th>
				    <th align="left">PickUpTime</th>
				    <th align="left">PickUpAdd</th>
				    <th align="left">Fname</th>
				    <th align="left">Lname</th>
				    <th align="left">Mobile</th>
				    <th align="left">Email Id</th>
				    <th align="left">BookingSource</th>
				    <th align="left">Userid</th>
				    <th align="left">PaymentStatus</th>
				    <th align="left">PaymentAmount</th>
				    <th align="left">TrackID</th>
				    <th align="left">VisitedCities</th>
				    <th align="left">OutstationYN</th>
				    <th align="left">Remarks</th>
				    <th align="left">IndicatedDiscPC</th>
				    <th align="left">PickUpDate</th>
				    <th align="left">DropOffDate</th>
				    <th align="left">OriginName</th>
				    <th align="left">OriginId</th>
				    <th align="left">Status</th>
				    <th align="left">TourType</th>
				    <th align="left">IndicatedPkgID</th>
				    <th align="left">DiscountAmount</th>
				    <th align="left">ChauffeurCharges</th>
				    <th align="left">DropOffTime</th>
				    <th align="left">serviceAmount</th>
				    <th align="left">PayBackMember</th>
				    <th align="left">subLocationID</th>
				    <th align="left">convenient_booking_process</th>
				    <th align="left">cab_report_time</th>
				    <th align="left">chauffer_experience</th>
				    <th align="left">expectation_met</th>
				    <th align="left">overall_experience</th>
				</tr>
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showDetails(" . $r[$i]["uid"] . ", '" . $r[$i]["coric"] . "')\" title=\"Click on view details of " . $r[$i]["coric"] . "\">Details</a>";
				    if($i % 2 == 0)
					$cls = "ev";
				    if($r[$i]["payment_mode"] == 1 && ($r[$i]["payment_status"] == 1 || $r[$i]["payment_status"] == 3) && is_null($r[$i]["booking_id"]) && $r[$i]["AuthDesc"] == 'Y'){
					$cls = "er";
					$vTxt .= "&nbsp;<a href='javascript: void(0)' title='Failed booking. Click on View Details to create booking of " . $r[$i]["coric"] . "'>?</a>";
				    }
				    $payMode = "-";
				    switch($r[$i]["payment_mode"]){
					case 1:
					    $payMode = "Online";
					break;
					case 2:
					    $payMode = "Pay to driver";
					break;
				    }
				    $payStatus = "-";
				    if(intval($r[$i]["payment_mode"]) == 1){
					switch($r[$i]["payment_status"]){
					    case 1:
						$payStatus = "Success";
						break;
					    case 2:
						$payStatus = "Gateway";
						break;
					    case 3:
						$payStatus = "Payment";
						break;
					    case 4:
						$payStatus = "Cancelled";
						break;
					    case 5:
						$payStatus = "Unauthorised";
					}
				    } elseif(intval($r[$i]["payment_mode"]) == 2){
					switch($r[$i]["payment_status"]){
					    case 1:
						$payStatus = "Success";
						break;
					}
				    }
				    //elseif($r[$i]["AuthDesc"] == 'N')
				    //$payStatus = "Unauthorised";
				    $ua = browser_info($r[$i]["ua"]);
				    //echo $r[$i]["ua"] . "<pre>";
				    $b = array_keys($ua);
				    $v = array_values($ua);
				    $bookDate = date_create($r[$i]["entry_date"]);
				    //print_r($r);
		    ?>
				    <tr class="<?php echo $cls; ?>">
				        <td align="left"><?php echo $srno; ?></td>
				        <td align="left"><?php echo $r[$i]["booking_id"]; ?></td>
				        <td align="left"><?php echo $r[$i]["purpose_of_trip"]; ?></td>
				        <td align="left"><?php echo $r[$i]["other_purpose"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_1"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_2"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_3"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_4"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_5"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_6"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["reason_7"]; ?></td>
				        <td align="left"><?php echo $r[$i]["other_reason"]; ?></td>
				        <td align="left"><?php echo $r[$i]["experience"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_1"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_2"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_3"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_4"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_5"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_6"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["experience_7"]; ?></td>
				        <td align="left"><?php echo $r[$i]["other_experience"]; ?></td>
				        <td align="left"><?php echo $r[$i]["feedback"]; ?></td>
				        <td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookDate->format('H:i A'); ?></small></td>
				        <td align="left"><?php echo $r[$i]["ip"]; ?><br /><small><?php echo ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]); ?> <a href="javascript: void(0);" title="<?php echo $r[$i]["ua"]; ?>">?</a></small></td>
				        
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["CarCatName"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["destination"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PickUpTime"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PickUpAdd"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["Fname"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["Lname"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["mobile"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["EmailID"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["BookingSource"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["userid"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PaymentStatus"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PaymentAmount"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["trackID"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["VisitedCities"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["OutstationYN"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["Remarks"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["IndicatedDiscPC"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PickUpDate"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["DropOffDate"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["OriginName"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["originId"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["Status"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["TourType"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["IndicatedPkgID"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["DiscountAmount"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["ChauffeurCharges"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["DropOffTime"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["serviceAmount"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["PayBackMember"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["subLocationID"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["convenient_booking_process"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["cab_report_time"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["chauffer_experience"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["expectation_met"]; ?></td>
				        <td align="left" style="white-space: pre-wrap;word-wrap: break-word;"><?php echo $r[$i]["overall_experience"]; ?></td>
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
		</div>
			</div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
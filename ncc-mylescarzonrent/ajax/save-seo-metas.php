<?php
	include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
	$resp = 0;
	$metaTitle = "";
	$metaDescription = "";
	$metaKeyword = "";
	$uid = -1;
	if(isset($_POST["uid"]) && $_POST["uid"] != "")
	$uid = $_POST["uid"];

	$db = new MySqlConnection(CONNSTRING);
	$db->open();
	$sql = "SELECT * FROM cor_seo_mata_tags WHERE page_url = '" . trim($_POST["pgurl"]) . "'";
	$r = $db->query("query", $sql);
	if(!array_key_exists("response", $r))
	$uid = $r[0]["uid"];
	unset($r);

	
	$arrDataToSave = array();
	$arrDataToSave["page_url"] = $_POST["pgurl"];
	$arrDataToSave["metaTitle"] = $_POST["title"];
	$arrDataToSave["metaDescription"] = $_POST["description"];
	$arrDataToSave["metaKeyword"] = $_POST["keywords"];
	$arrDataToSave["entry_date"] = date('Y-m-d H:i:s');
	if($uid > -1){
		$arrWhere = array();
		$arrWhere["uid"] = $uid;
		$r = $db->update("cor_seo_mata_tags", $arrDataToSave, $arrWhere);
		unset($arrWhere);
	}
	else
		$r = $db->insert("cor_seo_mata_tags", $arrDataToSave);
	if($r["response"] == "SUCCESS")
	$resp = 1;

	$totRec = 0;
	$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_seo_mata_tags";
	$r = $db->query("query", $sql);
	if(!array_key_exists("response", $r)){
		$totRec = $r[0]["TotalRecord"];
	}
	unset($r);

	unset($arrDataToSave);
	$db->close();
	echo $resp . "|#|" . $totRec;
?>
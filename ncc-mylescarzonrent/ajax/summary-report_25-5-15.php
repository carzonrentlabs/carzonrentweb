<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("../includes/check-user.php");
    include_once("../includes/cache-func.php");
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    
    $report = "";
    if(isset($_POST["rpt"]) && $_POST["rpt"] != "")
        $report = $_POST["rpt"];
    $type = "";
    if(isset($_POST["tp"]) && $_POST["tp"] != "")
        $type = $_POST["tp"];
    $sDate = date_create(date("Y-m-d"));
    $eDate = date_create(date("Y-m-d"));    
    if(isset($_POST["sd"]) && $_POST["sd"] != "")
        $sDate = date_create(str_ireplace(",", "", urldecode($_POST["sd"])));
        
    if(isset($_POST["ed"]) && $_POST["ed"] != "")
        $eDate = date_create(str_ireplace(",", "", urldecode($_POST["ed"])));
        
    $query_string = "";
    if ($_POST) {
      $kv = array();
      foreach ($_POST as $key => $value) {
	$kv[] = "$key=$value";
      }
      $query_string = join("&", $kv);
    }
    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $html = "<div style='width:100%;height:100%;float:left;overflow:scroll;'>";
    if($type = "booking"){
    // SOURCE report code STARTS
        $html .= "<table class='dtl' cellspacing='1' border='0' width='100%'>";
            $html .= "<tr>";
                if($report == "source")
                $html .= "<th colspan='17' align='left' style='font-size:14px;font-weight:bold;'><b>Source Report</b></th>";
                elseif($report == "tourtype")
                $html .= "<th colspan='17' align='left' style='font-size:14px;font-weight:bold;'><b>Tour Type Report</b></th>";
                elseif($report == "origin")
                $html .= "<th colspan='17' align='left' style='font-size:14px;font-weight:bold;'><b>Origin Report</b></th>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc'><b>Report date</b></td>";
                $html .= "<td style='background-color: #fef6cf' colspan=\"12\">" . $sDate->format("d M, Y") . " - " . $eDate->format("d M, Y") . "</td>";
                $html .= "<td style='background-color: #fef6cf' colspan=\"4\"><table cellspacing='0' cellpadding='0' border='0'><tr><td><a href=\"" . corWebRoot . "/admin/report-download.php?" . $query_string . "\" style='float:right;color:#128a30;'><img src=\"" . corWebRoot . "/admin/img/download.png\" alt=\"Download\" title=\"Download\" width=\"16px\" border=\"0\" /></a></td>";
                $html .= "<td style='background-color: #fef6cf'><a href=\"" . corWebRoot . "/admin/report-download.php?" . $query_string . "\" style='float:right;color:#128a30;'>Download</a></td><td>&nbsp;&nbsp;&nbsp;<a href=\"javascript: void(0);\" onclick=\"javascript: _closePopup();_showReport('" . $report . "','" . $type . "');\" style='float:right;color:#128a30;'><img src=\"" . corWebRoot . "/admin/img/refresh.png\" alt=\"Refresh\" title=\"Refresh\" width=\"16px\" border=\"0\" /></a></td>";
                $html .= "<td style='background-color: #fef6cf'><a href=\"javascript: void(0);\" onclick=\"javascript: _closePopup();_showReport('" . $report . "','" . $type . "');\" style='float:right;color:#2026a4;'>Refresh</a></td></tr></table></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                if($report == "source")
                $html .= "<td width=\"10%\" style='background-color: #fef3bc' rowspan=\"3\"><b>Source</b></td>";
                elseif($report == "tourtype")
                $html .= "<td width=\"10%\" style='background-color: #fef3bc' rowspan=\"3\"><b>Tour Type</b></td>";
                elseif($report == "origin")
                $html .= "<td width=\"10%\" style='background-color: #fef3bc' rowspan=\"3\"><b>Origin</b></td>";
                
                $html .= "<td width=\"8%\" style='background-color: #fef3bc' rowspan=\"3\"><b>Searches</b></td>";
                $html .= "<td width=\"12%\" style='background-color: #fef3bc' colspan=\"2\"><b>Booking attempts</b></td>";
                $html .= "<td width=\"16%\" style='background-color: #fef3bc' colspan=\"3\"><b>Confirmed booking</b></td>";
                $html .= "<td width=\"26%\" style='background-color: #fef3bc' colspan=\"4\"><b>Payment Mode</b></td>";
                $html .= "<td width=\"12%\" style='background-color: #fef3bc' colspan=\"2\"><b>Incomplete booking</b></td>";
                $html .= "<td width=\"15%\" style='background-color: #fef3bc' colspan=\"3\"><b>Payment Status</b></td>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc' rowspan=\"3\"><b>&nbsp;</b></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' rowspan=\"2\"><b><i>#</i></b></td>";
                $html .= "<td style='background-color: #fef3bc' rowspan=\"2\"><b><i>%</i></b></td>";
                $html .= "<td width=\"4%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>#</i></b></td>";
                $html .= "<td width=\"4%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>%</i></b></td>";
                $html .= "<td width=\"8%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>Amount (Rs)</i></b></td>";
                $html .= "<td width=\"13%\" style='background-color: #fef3bc' colspan='2'><b><i>Online</i></b></td>";
                $html .= "<td width=\"13%\" style='background-color: #fef3bc' colspan='2'><b><i>Pay to driver</i></b></td>";
                $html .= "<td style='background-color: #fef3bc' rowspan=\"2\"><b><i>#</i></b></td>";
                $html .= "<td style='background-color: #fef3bc' rowspan=\"2\"><b><i>%</i></b></td>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>Gateway</i></b></td>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>Cancelled</i></b></td>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc' rowspan=\"2\"><b><i>Unauthorized</i></b></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc'><b><i>#</i></b></td>";
                $html .= "<td width=\"8%\" style='background-color: #fef3bc'><b><i>Amount (Rs)</i></b></td>";
                $html .= "<td width=\"5%\" style='background-color: #fef3bc'><b><i>#</i></b></td>";
                $html .= "<td width=\"8%\" style='background-color: #fef3bc'><b><i>Amount (Rs)</i></b></td>";
            $html .= "</tr>";
    
        if($report == "source")
            $sql = "SELECT website as source, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY website ORDER BY COUNT(unique_id) DESC";
        elseif($report == "tourtype")
            $sql = "SELECT tour_type, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY tour_type ORDER BY COUNT(unique_id) DESC";
        elseif($report == "origin")
            $sql = "SELECT origin_name, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
        //elseif($report == "payment-mode")
            //$sql = "SELECT payment_mode, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY payment_mode ORDER BY COUNT(unique_id) DESC";
        $r = $db->query("query", $sql);
        $totS = 0;
        $totA = 0;
        $totC = 0;
        $totI = 0;
        $totO = 0;
        $totP = 0;
        $totCC = 0;
        $totOC = 0;
        $totPC = 0;
        $totPG = 0;
        $totCN = 0;
        $totFL = 0;
        if(!array_key_exists("response", $r)){
            for($i = 0; $i < count($r); $i++){
                $tS = 0;
                $tA = 0;
                $tC = 0;
                $tI = 0;
                $tO = 0;
                $tP = 0;
                $tCC = 0;
                $tOC = 0;
                $tPC = 0;
                $tPG = 0;
                $tCN = 0;
                $tFL = 0;
                $tS = $r[$i]["TotalSearch"];
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "'";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "'";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalAttempts FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "'";
                $data = $db->query("query", $sql);
                if(!array_key_exists("response", $data))
                $tA = $data[0]["TotalAttempts"];
                unset($data);
                
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL";
                $data = $db->query("query", $sql);
                $tC = $data[0]["TotalBooking"];
                if(isset($data[0]["TotalCost"]))
                $tCC = $data[0]["TotalCost"];
                else
                $tCC = 0;
                unset($data);
                
                $tI = $tA - $tC;
                $html .= "<tr>";
                    if($report == "source")
                    $html .= "<td style='background-color: #fef6cf'>" . $r[$i]["source"] . "</td>";
                    elseif($report == "tourtype")
                    $html .= "<td style='background-color: #fef6cf'>" . $r[$i]["tour_type"] . "</td>";
                    elseif($report == "origin")
                    $html .= "<td style='background-color: #fef6cf'>" . $r[$i]["origin_name"] . "</td>";
                    //<img src=\"" . corWebRoot . "/admin/img/ddl-a.gif\" border=\"0\" alt=\"Click view drill down report\" title=\"Click view drill down report\" style=\"float:right;cursor:pointer;\" />
                    $html .= "<td style='background-color: #fef6cf'>" . $tS . "</td>";
                    
                    $html .= "<td style='background-color: #fef6cf'>" . $tA . "</td>";
                    if($tS > 0)
                    $html .= "<td style='background-color: #fef6cf'>" . number_format(($tA * 100) / $tS, 2, ".", "") . "%</td>";
                    else
                    $html .= "<td style='background-color: #fef6cf'>0.00%</td>";
                    
                    $html .= "<td style='background-color: #fef6cf'>" . $tC . "</td>";
                    if($tS > 0)
                    $html .= "<td style='background-color: #fef6cf'>" . number_format(($tC * 100) / $tS, 2, ".", "") . "%</td>";
                    else
                    $html .= "<td style='background-color: #fef6cf'>0.00%</td>";
                
                    $html .= "<td width=\"5%\" style='background-color: #fef6cf'>" . number_format($tCC, 0, "", ",") . "</td>";
                    
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL AND payment_mode = 1";
                    $data = $db->query("query", $sql);
                    if(isset($data[0]["TotalBooking"]))
                    $tO = $data[0]["TotalBooking"];
                    else
                    $tO = 0;
                    if(isset($data[0]["TotalCost"]))
                    $tOC = $data[0]["TotalCost"];
                    else
                    $tOC = 0;
                    unset($data);
                    $html .= "<td width=\"5%\" style='background-color: #fef6cf'>" . $tO . "</td>";
                    $html .= "<td width=\"8%\" style='background-color: #fef6cf'>" . number_format($tOC, 0, "", ",") . "</td>";
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND booking_id IS NOT NULL AND payment_mode = 2";
                    $data = $db->query("query", $sql);
                    if(isset($data[0]["TotalBooking"]))
                    $tP = $data[0]["TotalBooking"];
                    else
                    $tP = 0;
                    if(isset($data[0]["TotalCost"]))
                    $tPC = $data[0]["TotalCost"];
                    else
                    $tPC = 0;
                    $html .= "<td width=\"5%\" style='background-color: #fef6cf'>" . $tP . "</td>";
                    $html .= "<td width=\"8%\" style='background-color: #fef6cf'>" . number_format($tPC, 0, "", ",") . "</td>";
                    
                    $html .= "<td style='background-color: #fef6cf'>" . $tI . "</td>";
                    if($tS > 0)
                    $html .= "<td style='background-color: #fef6cf'>" . number_format(($tI * 100) / $tS, 2, ".", "") . "%</td>";
                    else
                    $html .= "<td style='background-color: #fef6cf'>0.00%</td>";
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 2";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 2";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 2";
                    $data = $db->query("query", $sql);
                    if(isset($data[0]["TotalBooking"]))
                    $tPG = $data[0]["TotalBooking"];
                    else
                    $tPG = 0;
                    $html .= "<td style='background-color: #fef6cf'>" . $tPG . "</td>";
                    
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 4";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 4";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 4";
                    $data = $db->query("query", $sql);
                    if(isset($data[0]["TotalBooking"]))
                    $tCN = $data[0]["TotalBooking"];
                    else
                    $tCN = 0;
                    $html .= "<td style='background-color: #fef6cf'>" . $tCN . "</td>";
                    
                if($report == "source")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source = '" . $r[$i]["source"] . "' AND  payment_status = 5";
                elseif($report == "tourtype")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND tour_type = '" . $r[$i]["tour_type"] . "' AND  payment_status = 5";
                elseif($report == "origin")
                    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND origin_name = '" . $r[$i]["origin_name"] . "' AND  payment_status = 5";
                    $data = $db->query("query", $sql);
                    if(isset($data[0]["TotalBooking"]))
                    $tFL = $data[0]["TotalBooking"];
                    else
                    $tFL = 0;
                    $html .= "<td style='background-color: #fef6cf'>" . $tFL . "</td>";
                    $html .= "<td style='background-color: #fef6cf'>&nbsp;</td>";
                $html .= "</tr>";
                $totS += $tS;
                $totA += $tA;
                $totC += $tC;
                $totI += $tA - $tC;
                $totCC += $tCC;
                $totO += $tO;
                $totOC += $tOC;
                $totP += $tP;                
                $totPC += $tPC;
                $totPG += $tPG;
                $totCN += $tCN;
                $totFL += $tFL;
            }
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc'><b>TOTAL</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totS . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totA . "</b></td>";
                if($totS > 0)
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format(($totA * 100) / $totS, 2, ".", "") . "%</b></td>";
                else
                $html .= "<td style='background-color: #fef3bc'><b>0.00%</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totC . "</b></td>";
                if($totS > 0)
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format(($totC * 100) / $totS, 2, ".", "") . "%</b></td>";
                else
                $html .= "<td style='background-color: #fef3bc'><b>0.00%</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format($totCC, 0, "", ",") . "</b></td>";
                
                $html .= "<td style='background-color: #fef3bc'><b>" . $totO . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format($totOC, 0, "", ",") . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totP . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format($totPC, 0, "", ",") . "</b></td>";
                    
                $html .= "<td style='background-color: #fef3bc'><b>" . $totI . "</b></td>";
                if($totS > 0)
                $html .= "<td style='background-color: #fef3bc'><b>" . number_format(($totI * 100) / $totS, 2, ".", "") . "%</b></td>";
                else
                $html .= "<td style='background-color: #fef3bc'><b>0.00%</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totPG . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totCN . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>" . $totFL . "</b></td>";
                $html .= "<td style='background-color: #fef3bc'><b>&nbsp;</b></td>";
            $html .= "</tr>";
        } else {
            $html .= "<tr>";
                $html .= "<td colspan='5' style='background-color: #fef3bc'><b>NO DATA</b></td>";
            $html .= "</tr>";
        }
        $html .= "</table>";
        unset($r);
        // SOURCE report code ENDS
    }
    $db->close();    
    $html .= "</div>";
    echo $html;
?>
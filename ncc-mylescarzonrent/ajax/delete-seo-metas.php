<?php
	include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
	$resp = 0;
	$uid = -1;
	if(isset($_POST["uid"]) && $_POST["uid"] != "")
	$uid = $_POST["uid"];

	$db = new MySqlConnection(CONNSTRING);
	$db->open();
	if($uid > -1){
		$arrWhere = array();
		$arrWhere["uid"] = $uid;
		$r = $db->delete("cor_seo_mata_tags", $arrWhere);
		unset($arrWhere);
	}

	$totRec = 0;
	$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_seo_mata_tags";
	$r = $db->query("query", $sql);
	if(!array_key_exists("response", $r)){
		$totRec = $r[0]["TotalRecord"];
	}
	unset($r);

	unset($arrDataToSave);
	$db->close();
	echo $totRec;
?>
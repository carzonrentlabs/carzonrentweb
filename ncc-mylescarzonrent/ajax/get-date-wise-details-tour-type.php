<?php
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    
    
    $startdate = $_POST["startdate"];
    $enddate = $_POST["enddate"];   
    $tour_type = $_POST["tour_type"];
    $html = "";
    
    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    
    $sql = $db->query("stored procedure","cor_getDateWiseTourTypeReport('".$startdate."','".$enddate."','".$tour_type."')");
    //print_r($sql);
    $k = 1;
    for($i=0;$i < count($sql);$i++)
    {
        if($k%2 == 0)
        {
            $style = "background-color:#f1f1f1;";
        }
        else
        {
            $style = "background-color:#dddada;";
        }
        $html .= "<tr style=\"$style\">
                    <td width=\"10%\" style=\"padding:5px\">".($i+1)."</td>
                    <td width=\"30%\" style=\"padding:5px\">".$sql[$i]["origin_name"]."--".$sql[$i]["destination_name"]."</td>
                    <td width=\"30%\" style=\"padding:5px\">".$sql[$i]["entry_date"]."-".$sql[$i]["entry_date"]."</td>
                    <td width=\"30%\" style=\"padding:5px\">".$sql[$i]["cnt"]."</td>
                </tr>";
        $k++;
    }
    
    $html .="<tr>
            <td colspan=\"4\" align=\"right\">
                  <a href=\"download-tour-type-datewise-report.php?sdate=".$startdate."&edate=". $enddate."&tt=".$tour_type."\">Download</a>
            </td></tr>";
    unset($sql);
    $db->close();
    echo $html;
?>
<?php

    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
   // $bookingid = 0;
    if(isset($_GET["bookingid"]))
    $bookingid = $_GET["bookingid"];       
    $html = "";
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
	$sql = "SELECT eb.booking_id,eb.cust_email,eb.cust_mobile,eb.guest_name, eb.pickup_address, eb.drop_landmark, eb.pickup_datetime, eb.dest_address, eb.ip, eb.entry_datetime, ef.booking_expirience, ef.reporting_time,ef.customer_satisfaction,ef.driver_behaviour,ef.user_comment  from easycabs_booking as eb inner join easycabs_feedback as ef on eb.booking_id=ef.booking_id where ef.booking_id='".$bookingid."' order by booking_id desc ";
    $rows = $db->query("query", $sql);
    if(!array_key_exists("response", $rows)){
        $html = "<table class='dtl' cellspacing='1' border='0' width='100%'>";
            $html .= "<tr>";
                $html .= "<th colspan='6' align='left'><b>Booking Details</b></th>";
            $html .= "</tr>";
            if($rows[0]["booking_id"] != ""){
                $html .= "<tr>";
                    $html .= "<td style='background-color: #fef3bc;font-size:14px;' align='left'>Booking ID</td>";
                    $html .= "<td style='background-color: #fef6cf;font-size:14px;color:#f00;' align='left' colspan='5'><b>" . $rows[0]["booking_id"] . "</b></td>";
                $html .= "</tr>";
            }
               
                $html .= "<tr>";
				if(isset($rows[0]["cust_mobile"]) && $rows[0]["cust_mobile"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Customer Mobile</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountPC' name='txtDiscountPC' style='width:80% !important;' class='txtBox' value='" . $rows[0]["cust_mobile"] . "' /></td>";
				}
				if(isset($rows[0]["cust_email"]) && $rows[0]["cust_email"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Customer Email</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountAmount' name='txtDiscountAmount' style='width:80% !important;' class='txtBox' value='" . $rows[0]["cust_email"] . "' /></td>";
				}
                $html .= "</tr>";
                $html .= "<tr>";
				if(isset($rows[0]["guest_name"]) && $rows[0]["guest_name"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Name</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["guest_name"] . "' /></td>";
				}
				if(isset($rows[0]["pickup_address"]) && $rows[0]["pickup_address"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Location</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["pickup_address"] . "' /></td>";
				}
                $html .= "</tr>";
				if(isset($rows[0]["pickup_landmark"]) && $rows[0]["pickup_landmark"] != ""){
                 $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Landmark</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtUserID' name='txtUserID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["pickup_landmark"] . "' /></td>";
				}
				if(isset($rows[0]["drop_landmark"]) && $rows[0]["drop_landmark"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Drop Landmark</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtMobile' name='txtMobile' style='width:80% !important;' class='txtBox' value='" . $rows[0]["drop_landmark"] . "' /></td>";
				}
                $html .= "</tr>";
			
			    if(isset($rows[0]["pickup_datetime"]) && $rows[0]["pickup_datetime"] != ""){
			    $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Date Time</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["pickup_datetime"] . "' /></td>";
				}
				if(isset($rows[0]["dest_address"]) && $rows[0]["dest_address"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Destination Address</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["dest_address"] . "' /></td>";
				}
                $html .= "</tr>";
			
			
			    $html .= "<tr>";
				if($rows[0]["ip"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Ip</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["ip"] . "' /></td>";
				}
				if($rows[0]["entry_datetime"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Entry Date time</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["entry_datetime"] . "' /></td>";
				}
                $html .= "</tr>";
			
			
			
			    $html .= "<tr>";
				if($rows[0]["booking_expirience"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Booking Expirience</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["booking_expirience"] . "' /></td>";
				}
				if($rows[0]["reporting_time"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Reporting Time</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["reporting_time"] . "' /></td>";
				}
                $html .= "</tr>";
			
			
			
			    $html .= "<tr>";
				if($rows[0]["customer_satisfaction"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Customer Satisfaction</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["customer_satisfaction"] . "' /></td>";
				}
				if($rows[0]["driver_behaviour"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left'>Driver Behaviour</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["driver_behaviour"] . "' /></td>";
				}
                $html .= "</tr>";
			
			
			
			    $html .= "<tr>";
				if($rows[0]["user_comment"] != ""){
                $html .= "<td style='background-color: #fef3bc' align='left' valign='top'>User Comment</td>";
                $html .= "<td colspan='3' style='background-color: #fef6cf' align='left'><textarea id='txtFullName' name='txtFullName' style='width:96% !important;height:100px;margin-bottom:5px;' class='txtBox'>". $rows[0]["user_comment"] . "</textarea></td>";
				}
     
            $html .= "</tr>";
			
        $html .= "</table>";
    } else {
        $html = "No data found for " + $bookingid;
    }
    $db->close();
    
    echo $html;
?>
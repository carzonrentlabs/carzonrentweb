<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    
    $uid = 0;
    $coric = "";
    if(isset($_GET["uid"]))
        $uid = $_GET["uid"];
    if(isset($_GET["coric"]))
        $coric = $_GET["coric"];
        
    $html = "";
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $sql = "SELECT * FROM cor_booking_new WHERE uid = " . $uid . " AND coric = '" . $coric . "' ORDER BY uid DESC";
    $rows = $db->query("query", $sql);
    if(!array_key_exists("response", $rows)){
        $pickTime = date_create($rows[0]["pickup_time"]);
        if($rows[0]["drop_time"] != "")
        $dropTime = date_create($rows[0]["drop_time"]);
        $html = "<table class='dtl' cellspacing='1' border='0' width='100%'>";
            $html .= "<tr>";
                $html .= "<th colspan='6' align='left'><b>Booking Details</b></th>";
            $html .= "</tr>";
            if($rows[0]["booking_id"] != ""){
                $html .= "<tr>";
                    $html .= "<td style='background-color: #fef3bc;font-size:14px;' align='left'>Booking ID</td>";
                    $html .= "<td style='background-color: #fef6cf;font-size:14px;color:#f00;' align='left' colspan='5'><b>" . $rows[0]["booking_id"] . "</b></td>";
                $html .= "</tr>";
            }
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc;width:25% !important;' align='left'>Tour Type</td>";
                $html .= "<td style='background-color: #fef6cf;width:25% !important;' align='left'><input type='text' id='txtTourType' name='txtTourType' style='width:80% !important;' class='txtBox' value='" . $rows[0]["tour_type"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc;width:25% !important;' align='left'>Destination</td>";
                $html .= "<td style='background-color: #fef6cf;width:25% !important;' align='left'><input type='text' id='txtDestination' name='txtDestination' style='width:80% !important;' class='txtBox' value='" . $rows[0]["destinations_name"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Car Model</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPackageID' name='txtPackageID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["model_name"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Distance</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDistance' name='txtDistance' style='width:80% !important;' class='txtBox' value='" . $rows[0]["distance"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Date</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPickupDate' name='txtPickupDate' style='width:80% !important;' class='txtBox' value='" . $rows[0]["pickup_date"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Drop Date</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropDate' name='txtDropDate' style='width:80% !important;' class='txtBox' value='" . $rows[0]["drop_date"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Pickup Time</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPickupTime' name='txtPickupTime' style='width:80% !important;' class='txtBox' value='" . $pickTime->format('is') . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Drop Time</td>";
                if($rows[0]["drop_time"] != "")
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropTime' name='txtDropTime' style='width:80% !important;' class='txtBox' value='" . $dropTime->format('is') . "' /></td>";
                else
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDropTime' name='txtDropTime' style='width:80% !important;' class='txtBox' value='' /></td>";
                $html .= "</tr>";
                $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Discount PC</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountPC' name='txtDiscountPC' style='width:80% !important;' class='txtBox' value='" . $rows[0]["discount"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Discount Amount</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountAmount' name='txtDiscountAmount' style='width:80% !important;' class='txtBox' value='" . $rows[0]["discount_amt"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Full Name</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFullName' name='txtFullName' style='width:80% !important;' class='txtBox' value='" . $rows[0]["full_name"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Email ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtEmailID' name='txtEmailID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["email_id"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>User ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtUserID' name='txtUserID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["cciid"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Mobile</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtMobile' name='txtMobile' style='width:80% !important;' class='txtBox' value='" . $rows[0]["mobile"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Track ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtTrackID' name='txtTrackID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["coric"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Transaction ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtTransactionID' name='txtTransactionID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["nb_order_no"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Address</td>";
				$html .= "<td style='background-color: #fef6cf' align='left'><textarea type='text' id='txtAddress' name='txtAddress' rows='1' style='width:90% !important;' class='txtAr'>" . str_ireplace("<br />", "\n", $rows[0]["address"]) . "</textarea></td>";                                
                $html .= "<td style='background-color: #fef3bc' align='left'>Remarks</td>";
				$html .= "<td style='background-color: #fef6cf' align='left'><textarea type='text' id='txtRemarks' name='txtRemarks' rows='1' style='width:90% !important;' class='txtAr'>" . str_ireplace("<br />", "\n", $rows[0]["remarks"]) . "</textarea></td>";
            
            $html .= "</tr>";  
                
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Additional Services</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAddServices' name='txtAddServices' style='width:80% !important;' class='txtBox' value='" . $rows[0]["additional_srv"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Additional Charges</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAddCharges' name='txtAddCharges' style='width:35% !important;' class='txtBox' value='" . $rows[0]["additional_srv_amt"] . "' />&nbsp;<input type='text' id='add_service_cost_all' name='add_service_cost_all' style='width:35% !important;' class='txtBox' value='" . $rows[0]["add_service_cost_all"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Fare Amount</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtFareAmount' name='txtFareAmount' style='width:80% !important;' class='txtBox' value='" . $rows[0]["tot_fare"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Payment Type</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPaymentType' name='txtPaymentType' style='width:80% !important;' class='txtBox' value='" . $rows[0]["payment_mode"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Payment Status</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPaymentStatus' name='txtPaymentStatus' style='width:80% !important;' class='txtBox' value='" . $rows[0]["payment_status"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Auth Desc</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtAuthDesc' name='txtAuthDesc' style='width:80% !important;' class='txtBox' value='" . $rows[0]["AuthDesc"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Destination ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtVisitedCities' name='txtVisitedCities' style='width:80% !important;' class='txtBox' value='" . $rows[0]["destinations_name"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Origin Code</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtOriginCode' name='txtOriginCode' style='width:80% !important;' class='txtBox' value='" . $rows[0]["source"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>CheckSum</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtCheckSum' name='txtCheckSum' style='width:80% !important;' class='txtBox' value='" . $rows[0]["Checksum"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Discount Code</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDiscountCode' name='txtDiscountCode' style='width:80% !important;' class='txtBox' value='" . $rows[0]["promotion_code"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Promotion Code</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtPromotionCode' name='txtPromotionCode' style='width:80% !important;' class='txtBox' value='" . $rows[0]["discount_coupon"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Sub Location ID</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtSubLocationID' name='txtSubLocationID' style='width:80% !important;' class='txtBox' value='" . $rows[0]["subLoc"] . "' /></td>";
            $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>VAT</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='vat' name='vat' style='width:80% !important;' class='txtBox' value='" . $rows[0]["vat"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Duration</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='txtDuration' name='txtDuration' style='width:80% !important;' class='txtBox' value='" . $rows[0]["duration"] . "' /></td>";
            $html .= "</tr>";
			$html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Regular Day</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='WeekDayDuration' name='WeekDayDuration' style='width:80% !important;' class='txtBox' value='" . $rows[0]["WeekDayDuration"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Premium Day</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='WeekEndDuration' name='WeekEndDuration' style='width:80% !important;' class='txtBox' value='" . $rows[0]["WeekEndDuration"] . "' /></td>";
            $html .= "</tr>";
			$html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Free Duration</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='FreeDuration' name='FreeDuration' style='width:80% !important;' class='txtBox' value='" . $rows[0]["FreeDuration"] . "' /></td>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Trip Type</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='trip_type' name='trip_type' style='width:80% !important;' class='txtBox' value='" . $rows[0]["trip_type"] . "' /></td>";
            $html .= "</tr>";
			$html .= "<tr>";
                $html .= "<td  style='background-color: #fef3bc' align='left'>User Billable Amount</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='User_billable_amount' name='User_billable_amount' style='width:80% !important;' class='txtBox' value='" . $rows[0]["User_billable_amount"] . "' /></td>";
				$html .= "<td colspan='2' style='background-color: #fef3bc' align='left'>&nbsp</td>";
		   $html .= "</tr>";
            $html .= "<tr>";
                $html .= "<td style='background-color: #fef3bc' align='left'>Convenience Charges</td>";
                $html .= "<td style='background-color: #fef6cf' align='left'><input type='text' id='airportCharges' name='airportCharges' style='width:80% !important;' class='txtBox' value='" . $rows[0]["airportCharges"] . "' /></td>";
            if($rows[0]["payment_mode"] == 1 && is_null($rows[0]["booking_id"]) && $rows[0]["AuthDesc"] == 'Y'){
                    $html .= "<td colspan='2' align='left'><input type=\"image\" src=\"../images/submit-a.png\" onclick=\"javascript: _createBooking();\" /><span id='msg' style='color:#f00;'></span><input type='hidden' id='txtDestinationID' name='txtDestinationID' value='" . $rows[0]["destinations_id"] . "' /></td>";
            }
            $html .= "</tr>";
        $html .= "</table>";
    } else {
        $html = "No data found for " + $coric;
    }
    $db->close();
    
    echo $html;
?>
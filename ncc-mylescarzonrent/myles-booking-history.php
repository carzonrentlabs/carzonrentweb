<?php
session_start();
error_reporting(0);
if (!isset($_SESSION['email']) && $_SESSION['email']=="" && !isset($_SESSION['password']) && $_SESSION['password']=="" && $_SESSION['activeuser']!="activeuser")  
{
	
	$fURL1 = "./";
	header('Location: ' . $fURL1);	
} 
else
{
function browser_info($agent = null) {
    // Declare known browsers to look for
    $known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
        'konqueror', 'gecko');

    // Clean up agent and build regex that matches phrases for known browsers
    // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
    // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
    $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

    // Find all phrases (or return empty array if none found)
    if (!preg_match_all($pattern, $agent, $matches))
        return array();

    // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
    // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
    // in the UA).  That's usually the most correct.
    $i = count($matches['browser']) - 1;
    return array($matches['browser'][$i] => $matches['version'][$i]);
}

function filter_querystring($query_string, $arrFields) {
    if ($query_string != "") {
        $qString = "";
        if (count($arrFields) > 0) {
            $qsParams = explode("&", $query_string);
            for ($q = 0; $q < count($qsParams); $q++) {
                $qsParam = explode("=", $qsParams[$q]);
                $iF = -1;
                for ($f = 0; $f < count($arrFields); $f++) {
                    if ($qsParam[0] == $arrFields[$f]) {
                        $iF = $f;
                        break;
                    }
                }
                if ($iF == -1)
                    $qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
            }
        } else {
            $qString = $query_string;
        }
    } else
        $qString = "";
    return $qString;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
	<style>
	.lead td, .lead th {border: 1px solid #ddd; padding: 10px 5px;}
	.lead thead { background:#f2f2f2; color:#333;}
	.lead thead th { padding:10px; vertical-align:top; }
	.lead thead th a { color:#333; text-decoration:none; }
	
	</style>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/leanp_panel/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a>Carzonrent Lead</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php 
			$url = "./myles-booking-history.php";
			$dlndURL = "./booking-offline-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 5;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("Pickup city" => "origin_name", "" => "origin_id");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			
			
			$lStart = ($cPage - 1) * $recPerPage;
			$totATRec = 0;
			$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_leading";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totATRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND payment_mode='1' AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM cor_leading WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			$totATBooking = 0;
			$sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totATBooking = $r[0]["TotalBooking"];
			unset($r);
			
			
			
			$totRevenue = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT SUM(tot_fare) as TotalFare FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT SUM(tot_fare) as TotalFare FROM cor_leading WHERE booking_id IS NOT NULL and `source` like 'COR2' AND payment_mode='1' AND payment_status = 1 AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totRevenue = $r[0]["TotalFare"];
			unset($r);
			

	 
			
			$totPTDBooking = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE booking_id IS NOT NULL and `source` like 'COR2' AND payment_mode='1' AND payment_mode = 2 AND payment_status = 1 AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totPTDBooking = $r[0]["TotalBooking"];
			unset($r);
			
			$totPPCBooking = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE booking_id IS NOT NULL and `source` like 'COR2' AND NOT gclid IS NULL AND payment_mode='1' AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totPPCBooking = $r[0]["TotalBooking"];
			unset($r);
			
			$totPBBooking = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE booking_id IS NOT NULL AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totPBBooking = $r[0]["TotalBooking"];
			unset($r);
			
			$totBooking = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalBooking FROM cor_leading WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totBooking = $r[0]["TotalBooking"];
			unset($r);
			
			$totFailBooking = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalFailedBooking FROM cor_leading WHERE " . $fld . " = '" . $fVal . "')";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			    }
			} else {
			    $sql = "SELECT COUNT(uid) as TotalFailedBooking FROM cor_leading WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59')";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totFailBooking = $r[0]["TotalFailedBooking"];
			unset($r);
			
			//$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT * FROM cor_leading WHERE " . $fld . " = '" . $fVal . "'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND payment_mode='1' and `source` like 'COR2'";
			    }
			    $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			} else {
			    $sql = "SELECT * FROM cor_leading WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'  ORDER BY id " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			}
			//echo $sql;
			$r = $db->query("query", $sql);
			
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
			if($dtDiff < 0){
			    $sql = "SELECT entry_date as entry_date, COUNT(unique_id) as TotalSearch FROM cor_leading WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'";
			    if($fld == "" && $fVal != ""){
				$sql.= " AND " . $fld . " = '" . $fVal . "'";
			    }
			    $sql .= " GROUP BY entry_date ORDER BY entry_date ASC";
			}
			else {
			    $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
			    $sql = "SELECT entry_date as entry_date, COUNT(unique_id) as TotalSearch FROM cor_leading WHERE entry_date BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "'";
			    $sql .= " GROUP BY entry_date ORDER BY entry_date ASC";
			}
			$chartSearch = $db->query("query", $sql);

			if($dtDiff < 0)
			$sql = "SELECT DATE(entry_date) as entry_date, COUNT(uid) as TotalBooking FROM cor_leading WHERE DATE(entry_date) BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' ORDER BY entry_date ASC";
			else {
			    $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
			    $sql = "SELECT DATE(entry_date) as entry_date, COUNT(uid) as TotalBooking FROM cor_leading WHERE DATE(entry_date) BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "' GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
			}
			$chartBooking = $db->query("query", $sql);
			
			if($dtDiff < 0)
			$sql = "SELECT DATE(entry_date) as entry_date, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_leading WHERE DATE(entry_date) BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' and `source` like 'COR2' AND payment_mode='1' AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
			else {
			    $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
			    $sql = "SELECT DATE(entry_date) as entry_date, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_leading WHERE DATE(entry_date) BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "' and `source` like 'COR2' AND payment_mode='1' AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
			}
			$chartFare = $db->query("query", $sql);

			$db->close();
			//print_r($r);
			
			$pagingHTML = "";
			if($numOfPage > 1){
			    $nURL = "";
			    $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));			    
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    $pagingHTML .= "<td>";
				    if($cPage % $maxPageNum != 0)
					$pgStart = $cPage - ($cPage % $maxPageNum);
				    else
					$pgStart = $cPage - $maxPageNum;
				    $pgStart++;
				    if($cPage > $maxPageNum){
					$pgURL = $nURL;
					$pgPrev = $pgStart -  1;
					$pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
				    }
				    for($p = $pgStart; $p <= $numOfPage; $p++){
					$pgURL = $nURL;
					if($p != $cPage){
					    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
					}
					else
					    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
					if($p % $maxPageNum == 0)
					break;
				    }
				    if($numOfPage > $p){
					$p++;
					$pgURL = $nURL;
					$pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
				    }
				    $pgURL = $nURL;
				    $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
			    $pagingHTML .= "Go to page <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" style=\"float:none !important;width:auto !important;\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
				    for($p = 1; $p <= $numOfPage; $p++){
					if($p != $cPage){
					    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
					} else {
					    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
					}
				    }
					$pagingHTML .= "</select>";
				    $pagingHTML .= "</td>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;" . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;" . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			} else {
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			}
		?>
	    <?php
		if($_SERVER["QUERY_STRING"] != ""){
	    ?>
		<script type="text/javascript">
	    <?php
		    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
		    for($q = 0; $q < count($qsParams); $q++){
			if(trim($qsParams[$q]) != ""){
			    $qsParam = explode("=", $qsParams[$q]);
	    ?>
			    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
	    <?php
			}
		    }
	    ?>
		</script>
	    <?php
		} else {
	    ?>
		<script type="text/javascript">
		    qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
		    qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
		</script>
	    <?php
		}
	    ?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				<?php
				    if(isset($_GET["sd"])){
					if($_GET["sd"] == ""){
				?>
				    <input type="hidden" id="sds" name="sd" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				<?php
				    }
				?>
				<?php
				    if(isset($_GET["ed"])){
					if($_GET["ed"] == ""){
				?>
				    <input type="hidden" id="eds" name="ed" value="" />
				<?php	    
					} else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
					}
				    } else {
				?>
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				<?php
				    }
				?> 
				    <table cellspacing="1" cellpadding="5" border="0">
					<tbody>
					    
					    <tr>
						
						
						<td style="padding-left: 10px;">
						    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
						</td>
					    </tr>
					    <tr>
						
						<td colspan="2" valign="middle" align="right">
						    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
							<tr>
							    <td><a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>"><img src="<?php echo corWebRoot; ?>/ncc-mylescarzonrent/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>" style="color:#128a30;">Download</a></td>
						    
							</tr>
						    </table>
						    
						    
						</td>
					</tbody>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			
			
			<?php //echo "hiiii";die;
			    if(!array_key_exists("response", $chartSearch)){
				$labels = "";
				$dataSearch = "";
				$dataBook = "";
				$dataFare = "";
				for($c = 0; $c< count($chartSearch); $c++){
				    $cDate = date_create($chartSearch[$c]["entry_date"]);
				    $labels .= $labels == "" ? $cDate->format("d-M") : ", " . $cDate->format("d-M");
				    $dataSearch .= $dataSearch == "" ? $chartSearch[$c]["TotalSearch"] : ", " . $chartSearch[$c]["TotalSearch"];
				    $dataBook .= $dataBook == "" ? $chartBooking[$c]["TotalBooking"] : ", " . $chartBooking[$c]["TotalBooking"];
				    $dataFare .= $dataFare == "" ? $chartFare[$c]["TotalCost"] : ", " . $chartFare[$c]["TotalCost"];
				}
			?>
				<div id="chartC" style="text-align: left;width:98%;padding-right: 2%;"><canvas id="search-booking-graph" height="150" width="998" style="margin:10px 0 0 0;"></canvas></div>
				
				<script>
				    function _showChart(arrLabel, arrData) {
					
					var lineChartData = {
					    labels : arrLabel,
					    datasets : [
						{
						    fillColor : "rgba(254,246,207,0.5)",
						    strokeColor : "rgba(250,196,130,1)",
						    pointColor : "rgba(255,161,47,1)",
						    pointStrokeColor : "#fff",
						    data : arrData
						}
					    ]
					}
					var myLine = new Chart(document.getElementById("search-booking-graph").getContext("2d")).Line(lineChartData);
					//alert("'" + arrLabel.join("', '") + "'");
				    }
				    var aL = new Array(<?php echo "'" . str_ireplace(", ", "', '", $labels) . "'"; ?>);
				    var aDB = new Array(<?php echo $dataBook; ?>);
				    var aDS = new Array(<?php echo $dataSearch; ?>);
				    var aDF = new Array(<?php echo $dataFare; ?>);
				    _showChart(aL, aDB);
				</script>
				<div id="chartOptions" style="text-align: right;width:98%;padding-right: 2%;">Choose data point for chart <select id="ddlCharts" style="float:none !important;width:auto !important;" class="ddl" onchange="javascript: _changeChart(this.value);"><option value="bookings">Bookings</option><option value="searches">Searches</option><option value="fare">Total Fare</option></select> <a style="color:#128a30;" id="btnChClose" href="javascript: void(0);" onclick="javascript: _closeChart(this);">Hide Chart</a></div>
				<div id="chartOptions2" style="display:none;text-align: right;width:98%;padding-right: 2%;"><a style="color:#128a30;" id="btnChClose" href="javascript: void(0);" onclick="javascript: _openChart(this);_showChart(aL, aDB);">Show Chart</a></div>
			<?php
			    }
			    unset($chart);
			?>
			
			<table width="99%" class="adm lead" cellspacing="0" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=Origin_name&sm=" . $sm ?>">Pickup City <?php if ($sortFld == "origin_name") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "origin_name" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=booking_id&sm=" . $sm ?>&dt=unsigned integer">Destination City <?php if ($sortFld == "booking_id") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "tour_type" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=tour_type&sm=" . $sm ?>">Tour Type <?php if ($sortFld == "tour_type") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "package_type" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=package_type&sm=" . $sm ?>&dt=unsigned integer">Package Type <?php if ($sortFld == "package_type") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "payment_status" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=payment_status&sm=" . $sm ?>&dt=unsigned integer">Pickup date <?php if ($sortFld == "payment_status") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "payment_mode" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?> <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=payment_mode&sm=" . $sm ?>&dt=unsigned integer">Drop date<?php if ($sortFld == "payment_mode") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "pickup_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=pickup_date&sm=" . $sm ?>&dt=datetime">Email <?php if ($sortFld == "email") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "email" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date&sm=" . $sm ?>&dt=datetime">Mobile No <?php if ($sortFld == "mobile" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
				<?php
				    $sm = $sortFld == "ip" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><small><a href="#">Comment</a></small></th>
				    <th align="left"><small><a href="#">Entry Date</a></small></th>
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showDetails(" . $r[$i]["uid"] . ", '" . $r[$i]["coric"] . "')\" title=\"Click on view details of " . $r[$i]["coric"] . "\">Details</a>";
				    if($i % 2 == 0)
					$cls = "ev";
				    if($r[$i]["payment_mode"] == 1 && ($r[$i]["payment_status"] == 1 || $r[$i]["payment_status"] == 3) && is_null($r[$i]["booking_id"]) && $r[$i]["AuthDesc"] == 'Y'){
					$cls = "er";
					$vTxt .= "&nbsp;<a href='javascript: void(0)' title='Failed booking. Click on View Details to create booking of " . $r[$i]["coric"] . "'>?</a>";
				    }
				    $payMode = "-";
				    switch($r[$i]["payment_mode"]){
					case 1:
					    $payMode = "Online";
					break;
					case 2:
					    $payMode = "Pay to driver";
					break;
				    }
				    $payStatus = "-";
				    if(intval($r[$i]["payment_mode"]) == 1){
					switch($r[$i]["payment_status"]){
					    case 1:
						$payStatus = "Success";
						break;
					    case 2:
						$payStatus = "Gateway";
						break;
					    case 3:
						$payStatus = "Payment";
						break;
					    case 4:
						$payStatus = "Cancelled";
						break;
					    case 5:
						$payStatus = "Unauthorised";
					}
				    } elseif(intval($r[$i]["payment_mode"]) == 2){
					switch($r[$i]["payment_status"]){
					    case 1:
						$payStatus = "Success";
						break;
					}
				    }
				    $bookStyle = "";
				    if($payMode == "Online" && $payStatus == "Success"){
					if($r[$i]["promotion_code"] == "PayBack")
					$bookStyle = "border-left:solid 5px #e2a115;";
					else
					$bookStyle = "border-left:solid 5px #1b9d06;";
				    }
				    elseif($payMode == "Pay to driver" && $payStatus == "Success")
				    $bookStyle = "border-left:solid 5px #060a9d;";
				    
				    $ua = browser_info($r[$i]["ua"]);
				    
				    $b = array_keys($ua);
				    $v = array_values($ua);
				    $pickDate = date_create($r[$i]["pickup_date"]);
				    $pickTime = date_create($r[$i]["pickup_time"]);
				    $dropDate = date_create($r[$i]["drop_date"]);
				    if(!is_null($r[$i]["drop_time"]))
				    $dropTime = date_create($r[$i]["drop_time"]);
				    $bookDate = date_create($r[$i]["entry_date"]);
		    ?>
				    <tr class="<?php echo $cls; ?>">
					<td align="left" style="<?php echo $bookStyle; ?>"><?php echo $r[$i]["origin_name"]; ?></td>
					<td align="left"><?php echo $r[$i]["destination_name"]; ?><?php if($r[$i]["gclid"] != "") { echo "&nbsp;|&nbsp;<span style='font-weight:bold;color:#a5109c;'>PPC</span>"; } ?></td>
					<td align="left"><?php echo $r[$i]["tour_type"] == "" ? "-" : $r[$i]["tour_type"]; ?></td>
					<td align="left"><?php echo $r[$i]["package_type"] == "" ? "-" : $r[$i]["package_type"]; ?></td>
					<td align="left"><?php echo $r[$i]["pickup_date"] == "" ? "-" : $r[$i]["pickup_date"]; ?></td>
					<td align="left"><?php echo $r[$i]["drop_date"]; ?></td>
					<td align="left"><?php echo $r[$i]["email"] ?></td>
					<td align="left"><?php echo $r[$i]["mobile"] ?></td>
					<td align="left"><?php echo substr($r[$i]["comment"],0,50); ?>...</td>
					<td align="left"><?php echo $r[$i]["entry_date"] ?></td>
					
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<?PHP
}	
?>
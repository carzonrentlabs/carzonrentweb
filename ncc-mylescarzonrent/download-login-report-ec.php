<?php
	include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('Booking ID','User ID', 'User Name', 'User Email', 'User Phone','Date'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");    
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_new_easycabs_loginReport('".$_GET["sdate"]."','".$_GET["edate"]."')");
    $dispLocation = "";
    if(!array_key_exists("response", $rows)){
        for($i = 0;$i< count($rows);$i++)
        {   
             fputcsv($output, array($rows[$i]["booking_id"],$rows[$i]["userid"], $rows[$i]["username"], $rows[$i]["useremail"], $rows[$i]["userphone"],date('F j, Y', strtotime($rows[$i]["entry_datetime"]))));        
        }
    }
?>
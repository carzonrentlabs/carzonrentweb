<?php
session_start();
if(!isset($_SESSION["email"]) && $_SESSION["email"]=="")
{

	$fURL1 = "./";
	header('Location: ' . $fURL1);	
} 
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin Console By Bytesbrick</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header.php");
                include_once("./includes/header-css.php");
            ?>

    </head>
    <body>
	<div id="middle">	
		<div class="yellwborder">
		    <div style="padding:7px 0px 7px 0px;" class="magindiv">
			<h2 class="heading2">NCC Mylescarzonrent Signup <span style="float:right;"> <a href="javascript: history.go(-1)">Back</a></span></h2>
		    </div>
		</div>
	    <div class="main">
	    <br><br>
		<div class="businessEnquiry">
		<form name="frmLogin" id="frmLogin" method="post" action="./do-signup.php">
		    <!--<input type="hidden" value="http://www.carzonrent.com" name="hdRURL" id="hdRURL">-->
		    <table width="75%" cellspacing="10" cellpadding="0" border="0" align="left" class="border-radius">                
			<tbody>
				<tr>
				<td></td>		     
				<td id="errmsg">
				<?PHP 
				$msg='';
				$css='';
				if(isset($_GET['msg']) && $_GET['msg']=="done") 
				{
				  $msg = "Registration Successfully!";
				  $css= "color:green;";
			    }
				else if(isset($_GET['msg']) && $_GET['msg']=="Failed") 
				{
					$msg = "Already Registered User!";
					$css= "color:red;";
				}
				else if(isset($_GET['msg']) && $_GET['msg']=="allfield") 
				{
					$msg = "Fill All The Mandatory Field.";
					$css= "color:red;";
					
				}
                ?>	
                <span style="<?PHP echo $css;?>"><?PHP echo $msg;?></span>				
				</td>		     
			    </tr>
				<tr>
				<td valign="middle" align="right"><label>Name</label></td>
				<td valign="top" align="left"><input type="text" id="username" name="username" placeholder ="Enter User Name" onkeyup="$('#errmsg').html('');$('#username').css('background','');"></td>			     
			    </tr>
			    <tr>
				<td valign="middle" align="right"><label>Email Id</label></td>
				<td valign="top" align="left"><input type="text" id="txtuserid" name="txtuserid" placeholder ="Enter Email Id" onkeyup="$('#errmsg').html('');$('#txtuserid').css('background','');"></td>			     
			    </tr>
				<!---<tr>
				<td valign="middle" align="right"><label>User Id</label></td>
				<td valign="top" align="left"><input type="text" id="userid" name="userid" placeholder ="Enter User Id" onkeyup="$('#errmsg').html('');$('#userid').css('background','');"></td>			     
			    </tr>-->
			    <tr>
				<td valign="middle" align="right"><label>Password</label></td>
				<td valign="top" align="left"><input type="password" id="txtpassword" placeholder ="Enter Password" name="txtpassword" onkeyup="$('#errmsg').html('');$('#txtpassword').css('background','');"></td>					
			    </tr>
				<tr>
				<td valign="middle" align="right"><label>User Type</label></td>
				<td valign="top" align="left">
				<select name="usertype" id="usertype" style="height: 34px;width: 202px;" onchange="$('#errmsg').html('');$('#usertype').css('background','');">
				<option name="" value="" selected>Select User type</option>
				<option value="Myles">Myles</option>
				<option value="Easycabs">Easycabs</option>
				</select>
				</td>					
			    </tr>
			    <tr>
				<td valign="middle" align="right"></td>                                     
			    </tr>
			    <tr>
				<td valign="middle" align="right"></td>
				<td valign="top" align="left"> <div class="submit"><a onclick="validate();" href="javascript:void(0)"></a></div></td>                                   
			    </tr>    
			</tbody>
		    </table>		    
		</form>
		</div>
    
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>
	</div>
        <?php
            include_once("./includes/footer.php");
        ?>
    </body>
</html>
<script>
   $(document).ready(function() {
	<?PHP
	if(isset($_GET['resp']) && $_GET['resp']=='inv')
	{?>
	$('#errmsg').css('color','red').html('Either Email Id Or Password Is Wrong!.');
	return false;
	<?PHP } ?>
    });
	function validate()
	{
		var id =$('#txtuserid').val();
		var pwd =$('#txtpassword').val();
		var username =$('#username').val();
		var usertype =$('#usertype').val();
		var userid =$('#userid').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(username=="")
		{
			$('#errmsg').css('color','red').html('Please Enter User Name.');
			$('#username').css('background-color','pink').attr('Placeholder','Please Enter User Name.').focus();
			return false;
		}
		else if(id=="")
		{
			$('#errmsg').css('color','red').html('Please Enter Email id.');
			$('#txtuserid').css('background-color','pink').attr('Placeholder','Please Enter Email id.').focus();
			return false;
		}
		if (!filter.test(id)) {$('#errmsg').css('color','red').html('Please Enter Email id in correct format.');
			$('#txtuserid').css('background-color','pink').attr('Placeholder','Please Enter Email id in correct format.').focus();
			return false;
		}
		else if(userid=="")
		{
			$('#errmsg').css('color','red').html('Please Enter User Id.');
			$('#userid').css('background-color','pink').attr('Placeholder','Please Enter User Id.').focus();
			return false;
		}
		else if(pwd=="")
		{
			$('#errmsg').css('color','red').html('Please Enter Password.');
			$('#txtpassword').css('background-color','pink').attr('Placeholder','Please Enter Password.').focus();
			return false;
		}
		
		else if(usertype=="")
		{
			$('#errmsg').css('color','red').html('Please Select UserType.');
			$('#usertype').css('background-color','pink').attr('Placeholder','Please Enter UserType.').focus();
			return false;
		}
		else 
		{
			document.forms['frmLogin'].submit();
		}	
		
	}
	</script>
	
<?PHP
}
?>
<?php
    include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
				include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/ajax.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/admin.js" type="text/javascript"></script>	 
		<script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js" type="text/javascript"></script>	  		
	    <script>
	    $(function() {
		    var dateToday = new Date();
		    dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    var dateNextDay = new Date();
		    dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    $( ".datepicker" ).datepicker({minDate: dateToday, maxDate: dateNextDay});
	    });
	    </script>     
    </head>
    <body>
    <?php     include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px; width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Search Report</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<br><br>
		<div style="margin:0 auto;width:99%;">
			<?php include_once("./includes/search-menu.php"); ?>
			<form id="frmSearch" name="frmSearch" action="search-report.php" method="get">
			<table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
			    <tbody>
				<tr>
				    <?php
					$db = new MySqlConnection(CONNSTRING);
					$db->open();
					
					$sDate = "";
					if(isset($_GET["startdate"]) && $_GET["startdate"] != ""){
					    $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
					}
					else {
					    $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
					    $sDate = date_create($maxDate[0]["max_date"]);
					}
					
					$eDate = "";
					if(isset($_GET["enddate"]) && $_GET["enddate"] != ""){
					    $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
					}
					else {
					    $eDate = $sDate;
					}
				    ?>
                                    <td><label>Start Date</label></td>
				    <td>
					<span class="datepick">
					    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
					</span>
				    </td>
				    <td><label>End Date </label>
				    </td>
				    <td><span class="datepick">
						    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
						</span>
				    </td>
				    <td>
					<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
				    </td>
				    <td valign="middle">					
					<a href="download-search-report.php?sdate=<?php echo $sDate->format('Y-m-d'); ?>&edate=<?php echo $eDate->format('Y-m-d'); ?>">Download</a>
				    </td>
                                </tr>
				</tbody>
			</table>
			</form>
			<table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
			    <tbody id="searchData">			
                                <tr>
				    <td align="center" width="5%" class="search_table">SrNo.</td>
                                    <td align="center" width="10%" class="search_table">Search Date</td>
				    <td align="center" width="10%" class="search_table">Type</td>
                                    <td align="center" width="10%" class="search_table">Origin</td>
                                    <td align="center" width="10%" class="search_table">Destination</td>
                                    <td align="center" width="10%" class="search_table">Travel Date</td>
                                    <td align="center" width="10%" class="search_table">Duration</td>
									<td align="center" width="10%" class="search_table">Duration Type</td>
                                    <!--<td align="center" width="10%" class="search_table">Details</td>-->
                                    <td align="center" width="10%" class="search_table">Distance</td>
				    <td align="center" width="10%" class="search_table">Avg Distance</td>
                                    <td align="center" width="5%" class="search_table">IP</td>
                                    <!--<td align="center" width="10%" class="search_table">BROWSER</td>-->
                                </tr>			    
                        <?php		

				
				 /*echo $data="select entry_date,tour_type,origin_name, destination_name, 
     pickup_date, duration, distance, ip, user_agent from customer_search 
     where (entry_date BETWEEN '", $sDate->format('Y-m-d'),"' AND '", $eDate->format('Y-m-d'),"') 
     ORDER by unique_id DESC LIMIT ",0,",",50; */
	 
	 
				 $r = $db->query("stored procedure", "cor_search_report('". $sDate->format('Y-m-d') ."','".$eDate->format('Y-m-d')."')");				
				if(!array_key_exists("response", $r)){
				    for($i=0 ; $i < count($r); $i++)
				    {				   
					$style = "";
					if($r[$i]["duration"] != 0)
					    $avgDist = ceil($r[$i]["distance"]/$r[$i]["duration"]);
					if($i%2 == 0)
					    $style = "background-color:#f1f1f1;";
					else
					    $style = "background-color:#dddada;";
					//if($avgDist >=150 && $avgDist <= 500)
					//    $style .= "color:#0f0;font-weight:bold;";
                        ?>
				    <tr style="<?php echo $style ?>">
					<td style="padding:5px !important;" name = "seqno"><?php echo ($i+1) ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["entry_date"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["tour_type"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["origin_name"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["destination_name"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["pickup_date"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["duration"] ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["rental_type"]?></td>
                    <!--<td style="padding:5px !important;"><a href='javascript: void(0)' onclick='javascript: _searchDetails(<?php echo $r[$i]["unique_id"];?>)' >Details</a></td>-->
					<td style="padding:5px !important;"><?php echo $r[$i]["distance"] ?></td>
					<td style="padding:5px !important;"><?php echo $avgDist; ?></td>
					<td style="padding:5px !important;"><?php echo $r[$i]["ip"] ?></td>
					<!--<td><?php //echo $r[$i]["user_agent"] ?></td>-->
				    </tr>
                        <?php
				    }
				    unset($r);
				}
				else {
			?>
				    <tr style="background-color:#f1f1f1;">
					<td align="center" colspan="10">No data found.</td>
				    </tr>
			<?php
				}
                        ?>
			    </tbody>
			</table>
			<div style="clear:both"></div>
			<table width="100%" cellspacing="1" cellpadding="5" border="0"  style="float: left;margin:15px 0 0 0px;">
			    <tbody id = "loadmore">
				<tr>
				    <td align="right">
					<a href="javascript:void(0);" onclick="javascript: _loadMoreSearch('<?php echo $sDate->format('Y-m-d') ?>','<?php echo $eDate->format('Y-m-d') ?>',<?php echo $i ?>)">LOAD MORE</a>
				    </td>
				</tr>
			    </tbody>
			</table>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
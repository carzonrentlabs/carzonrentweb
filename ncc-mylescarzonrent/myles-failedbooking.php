<?php
error_reporting(0);
include_once("./includes/check-user.php");
include_once("./includes/encrypt.php");
$lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
if ($lgUserName != "admin@carzonrent.com") {
    $fURL1 = "./logout.php";
    header('Location: ' . $fURL1);
}

function browser_info($agent = null) {
    // Declare known browsers to look for
    $known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
        'konqueror', 'gecko');

    // Clean up agent and build regex that matches phrases for known browsers
    // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
    // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
    $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

    // Find all phrases (or return empty array if none found)
    if (!preg_match_all($pattern, $agent, $matches))
        return array();

    // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
    // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
    // in the UA).  That's usually the most correct.
    $i = count($matches['browser']) - 1;
    return array($matches['browser'][$i] => $matches['version'][$i]);
}

function filter_querystring($query_string, $arrFields) {
    if ($query_string != "") {
        $qString = "";
        if (count($arrFields) > 0) {
            $qsParams = explode("&", $query_string);
            for ($q = 0; $q < count($qsParams); $q++) {
                $qsParam = explode("=", $qsParams[$q]);
                $iF = -1;
                for ($f = 0; $f < count($arrFields); $f++) {
                    if ($qsParam[0] == $arrFields[$f]) {
                        $iF = $f;
                        break;
                    }
                }
                if ($iF == -1)
                    $qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
            }
        } else {
            $qString = $query_string;
        }
    } else
        $qString = "";
    return $qString;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Carzonrent Admin</title>
        <?php
        include_once("./includes/cache-func.php");
        include_once("./includes/header-css.php");
        include_once("./includes/header-js.php");
        include_once("./includes/config.php");
        include_once("./classes/bb-mysql.class.php");
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
        <script type="text/javascript">
            qsArray = new Array();
        </script>
        <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/admin/js/chart.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                var minSDate = new Date();
                minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                var maxEDate = new Date();
                maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: minSDate, maxDate: maxEDate});
            });
        </script>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
<?php include_once("./includes/header.php"); ?>
        <div id="middle">	
            <div class="yellwborder">
                <div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
                    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Failed Bookings</h2>
                </div>
            </div>
            <div class="main" style="width:100%;">
                <div>
                    <?php
                    $url = "./myles-failedbooking.php";
                    $dlndURL = "./myles-failedbooking-download.php";
                    $db = new MySqlConnection(CONNSTRING);
                    $db->open();

                    $recPerPage = 100;
                    $maxPageNum = 5;
                    $cPage = 1;
                    $sDate = date_create(date("Y-m-d"));
                    $eDate = date_create(date("Y-m-d"));
                    $fld = "";
                    $val = "";
                    $sortFld = "uid";
                    $sortMode = "desc";
                    $sortDType = "varchar";
                    $isDtInc = false;
                    $isFiltered = false;
                    $arrFields = array("source" => "Source", "coric" => "CORIC", "tour_type" => "Tour Type", "package_id" => "Package ID", "car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID", "promotion_code" => "Discount Code");

                    if (isset($_GET["pg"]) && $_GET["pg"] != "")
                        $cPage = $_GET["pg"];

                    if (isset($_GET["sd"]) && $_GET["sd"] != "") {
                        $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
                        $isFiltered = true;
                        $isDtInc = true;
                    }

                    if (isset($_GET["ed"]) && $_GET["ed"] != "") {
                        $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
                        $isFiltered = true;
                        $isDtInc = true;
                    }

                    if (isset($_GET["f"]) && $_GET["f"] != "") {
                        $fld = $_GET["f"];
                        $isFiltered = true;
                    }

                    if (isset($_GET["q"]) && $_GET["q"] != "") {
                        $fVal = $_GET["q"];
                        $isFiltered = true;
                        if ($fld != "") {
                            if ($fld == "payment_mode") {
                                if (trim(strtolower($fVal)) == "online")
                                    $fVal = 1;
                                elseif (trim(strtolower($fVal)) == "pay to driver")
                                    $fVal = 2;
                            } elseif ($fld == "payment_status") {
                                if (trim(strtolower($fVal)) == "success")
                                    $fVal = 1;
                                elseif (trim(strtolower($fVal)) == "gateway")
                                    $fVal = 2;
                                elseif (trim(strtolower($fVal)) == "payment")
                                    $fVal = 3;
                                elseif (trim(strtolower($fVal)) == "failure" || trim(strtolower($fVal)) == "cancelled")
                                    $fVal = 4;
                                elseif (trim(strtolower($fVal)) == "unauthorised")
                                    $fVal = 5;
                            }
                        }
                    }

                    if (isset($_GET["inc"]) && $_GET["inc"] != "") {
                        $isDtInc = true;
                        $isFiltered = true;
                    }

                    if (isset($_GET["sf"]) && $_GET["sf"] != "")
                        $sortFld = $_GET["sf"];

                    if (isset($_GET["sm"]) && $_GET["sm"] != "")
                        $sortMode = $_GET["sm"];

                    if (isset($_GET["dt"]) && $_GET["dt"] != "")
                        $sortDType = "CONVERT(" . $sortFld . ", " . $_GET["dt"] . ")";
                    else {
                        if (isset($_GET["sf"]) && $_GET["sf"] != "")
                            $sortDType = $sortFld;
                        else
                            $sortDType = "CONVERT(" . $sortFld . ", unsigned integer)";
                    }
                    //$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    

                    $lStart = ($cPage - 1) * $recPerPage;
                                       

                    $totFailBooking = 0;
                    if ($fld != "" && $fVal != "") {
                        $sql = "SELECT * FROM cor_booking_new WHERE " . $fld . " = '" . $fVal . "' AND booking_id IS NULL AND payment_mode = 1 AND (payment_status = 3 OR payment_status = 1)";
                        if ($isDtInc) {
                            $sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
                        }
                    } else {
                        $sql = "SELECT * FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND booking_id IS NULL AND payment_mode = 1 AND (payment_status = 3 OR payment_status = 1)";
                    }
                    $r = $db->query("query", $sql);
                    $dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
//                    if ($dtDiff < 0) {
//                        $sql = "SELECT entry_date as entry_date, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive'";
//                        if ($fld == "" && $fVal != "") {
//                            $sql.= " AND " . $fld . " = '" . $fVal . "'";
//                        }
//                        $sql .= " GROUP BY entry_date ORDER BY entry_date ASC";
//                    } else {
//                        $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
//                        $sql = "SELECT entry_date as entry_date, COUNT(unique_id) as TotalSearch FROM customer_search WHERE entry_date BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive'";
//                        $sql .= " GROUP BY entry_date ORDER BY entry_date ASC";
//                    }
//                    $chartSearch = $db->query("query", $sql);
//
//                    if ($dtDiff < 0)
//                        $sql = "SELECT DATE(entry_date) as entry_date, COUNT(uid) as TotalBooking FROM cor_booking_new WHERE DATE(entry_date) BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive'AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
//                    else {
//                        $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
//                        $sql = "SELECT DATE(entry_date) as entry_date, COUNT(uid) as TotalBooking FROM cor_booking_new WHERE DATE(entry_date) BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive' AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
//                    }
//                    $chartBooking = $db->query("query", $sql);
//
//                    if ($dtDiff < 0)
//                        $sql = "SELECT DATE(entry_date) as entry_date, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE DATE(entry_date) BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive' AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
//                    else {
//                        $nsDate = strtotime($sDate->format("Y-m-d") . '-30 days');
//                        $sql = "SELECT DATE(entry_date) as entry_date, SUM(tot_fare + additional_srv_amt) as TotalCost FROM cor_booking_new WHERE DATE(entry_date) BETWEEN '" . date('Y-m-d', $nsDate) . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type='Selfdrive' AND booking_id IS NOT NULL GROUP BY DATE(entry_date) ORDER BY entry_date ASC";
//                    }
//                    $chartFare = $db->query("query", $sql);

                    $db->close();
                    //print_r($r);

                    $pagingHTML = "";
                    if ($numOfPage > 1) {
                        $nURL = "";
                        $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                        $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
                        $pagingHTML .= "<tr>";
                        $pagingHTML .= "<td>";
                        if ($cPage % $maxPageNum != 0)
                            $pgStart = $cPage - ($cPage % $maxPageNum);
                        else
                            $pgStart = $cPage - $maxPageNum;
                        $pgStart++;
                        if ($cPage > $maxPageNum) {
                            $pgURL = $nURL;
                            $pgPrev = $pgStart - 1;
                            $pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
                            $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
                        }
                        for ($p = $pgStart; $p <= $numOfPage; $p++) {
                            $pgURL = $nURL;
                            if ($p != $cPage) {
                                $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
                                $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
                            } else
                                $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
                            if ($p % $maxPageNum == 0)
                                break;
                        }
                        if ($numOfPage > $p) {
                            $p++;
                            $pgURL = $nURL;
                            $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
                            $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
                        }
                        $pgURL = $nURL;
                        $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
                        $pagingHTML .= "Go to page <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" style=\"float:none !important;width:auto !important;\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
                        for ($p = 1; $p <= $numOfPage; $p++) {
                            if ($p != $cPage) {
                                $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
                            } else {
                                $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
                            }
                        }
                        $pagingHTML .= "</select>";
                        $pagingHTML .= "</td>";
                        if (($lStart + $recPerPage) >= $totRec)
                            $pagingHTML .= "<td>&nbsp;" . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
                        else
                            $pagingHTML .= "<td>&nbsp;" . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
                        $pagingHTML .= "</tr>";
                        $pagingHTML .= "</table>";
                    } else {
                        $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
                        $pagingHTML .= "<tr>";
                        if (($lStart + $recPerPage) >= $totRec)
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
                        else
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
                        $pagingHTML .= "</tr>";
                        $pagingHTML .= "</table>";
                    }
                    ?>
                    <?php
                    if ($_SERVER["QUERY_STRING"] != "") {
                        ?>
                        <script type="text/javascript">
    <?php
    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
    for ($q = 0; $q < count($qsParams); $q++) {
        if (trim($qsParams[$q]) != "") {
            $qsParam = explode("=", $qsParams[$q]);
            ?>
                                    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
            <?php
        }
    }
    ?>
                        </script>
                        <?php
                    } else {
                        ?>
                        <script type="text/javascript">
                            qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
                            qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
                        </script>
                        <?php
                    }
                    ?>
                    <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
                        <tr>
                            <td>
                                <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
                                    <table cellspacing="1" cellpadding="5" border="0" align="left">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left: 10px;"><label>Start Date</label></td>
                                                <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 10px;">
                                                    <span class="datepick">
                                                        <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if (document.getElementById('inc').checked) {
                                                                document.getElementById('sds').value = this.value;
                                                            }" />						    
                                                    </span>
                                                </td>
                                                <td style="padding-left: 10px;"><span class="datepick">
                                                        <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if (document.getElementById('inc').checked) {
                                                                        document.getElementById('eds').value = this.value;
                                                                    }" />						    
                                                    </span>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
                                                </td>
                                                
                                                <td align="left" style="padding-left: 10px;font-size: 14px;">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                       <td>
                                            <a id="sum-report" href="javascript: void(0);" title="Click view report options">View reports
                                                <label>
                                                    <ul>
                                                        <li onclick="javascript: _showReport('source', 'mylesfailedbooking');">Source Report</li>
                                                        <li onclick="javascript: _showReport('tourtype', 'mylesfailedbooking');">Tour Type Report</li>
                                                        <li onclick="javascript: _showReport('origin', 'mylesfailedbooking');">Origin Report</li>
                                                    </ul>
                                                </label>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>    
                            </td>
                            <td align="right">
                                <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
                                    <?php
                                    if (isset($_GET["sd"])) {
                                        if ($_GET["sd"] == "") {
                                            ?>
                                            <input type="hidden" id="sds" name="sd" value="" />
                                            <?php
                                        } else {
                                            ?>
                                            <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (isset($_GET["ed"])) {
                                        if ($_GET["ed"] == "") {
                                            ?>
                                            <input type="hidden" id="eds" name="ed" value="" />
                                            <?php
                                        } else {
                                            ?>
                                            <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
                                        <?php
                                    }
                                    ?> 
                                    <table cellspacing="1" cellpadding="5" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding-right: 10px;"><label>Search</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" size="22" autocomplete="off" name="q" id="q" class="txtSBox" placeholder="Search query" value="<?php if (isset($_GET["q"]) && $_GET["q"] != "") {
                                        echo $_GET["q"];
                                    } ?>" /></td>
                                                <td>
                                                    <select name="f" id="f" class="ddlS" style="margin-left: 10px;">
                                                        <option>Select field&nbsp;</option>
                                                        <?php
                                                        foreach ($arrFields as $key => $val) {
                                                            $sel = "";
                                                            if ($key == $fld)
                                                                $sel = "selected='selected'";
                                                            ?>
                                                            <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" id="inc" name="inc" value="1" <?php if (!isset($_GET["sd"])) {
                                                            echo "checked=\"checked\"";
                                                        } elseif (isset($_GET["sd"]) && $_GET["sd"] != "") {
                                                            echo "checked=\"checked\"";
                                                        } ?> onclick="if (this.checked) {
                                                                            document.getElementById('sds').value = document.getElementById('sd').value;
                                                                            document.getElementById('eds').value = document.getElementById('ed').value;
                                                                        } else {
                                                                            document.getElementById('sds').value = '';
                                                                            document.getElementById('eds').value = '';
                                                                        }" <?php if ($isDtInc) {
                                                                    echo "checked='checked'";
                                                                } ?> />
                                                            </td>
                                                            <td>Include date filter</td>
                                                        </tr>
                                                    </table>
                                                </td> 
                                                <td colspan="2" valign="middle" align="right">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
                                                        <tr>
                                                            <td><a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                            <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>" style="color:#128a30;">Download</a></td>
                                                            <?php
                                                            if ($numOfPage > 1) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $dlndURL; ?>?<?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
                                                                <?php
                                                            }
                                                            if ($isDtInc || $isFiltered) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
    <?php
} else {
    ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
    <?php
}
?>
                                                        </tr>
                                                    </table>


                                                </td>
                                        </tbody>
                                    </table>
                                </form>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
                        
                        <tr>
                            <td align="right" style="padding-top:10px;">
                    <?php echo $pagingHTML; ?>
                            </td>
                        </tr>

                    </table>

                    <?php
                    /*if (!array_key_exists("response", $chartSearch)) {
                        $labels = "";
                        $dataSearch = "";
                        $dataBook = "";
                        $dataFare = "";
                        for ($c = 0; $c < count($chartSearch); $c++) {
                            $cDate = date_create($chartSearch[$c]["entry_date"]);
                            $labels .= $labels == "" ? $cDate->format("d-M") : ", " . $cDate->format("d-M");
                            $dataSearch .= $dataSearch == "" ? $chartSearch[$c]["TotalSearch"] : ", " . $chartSearch[$c]["TotalSearch"];
                            $dataBook .= $dataBook == "" ? $chartBooking[$c]["TotalBooking"] : ", " . $chartBooking[$c]["TotalBooking"];
                            $dataFare .= $dataFare == "" ? $chartFare[$c]["TotalCost"] : ", " . $chartFare[$c]["TotalCost"];
                        }
                        ?>
                        <div id="chartC" style="text-align: left;width:98%;padding-right: 2%;"><canvas id="search-booking-graph" height="150" width="998" style="margin:10px 0 0 0;"></canvas></div>

                        <script>
                            function _showChart(arrLabel, arrData) {

                                var lineChartData = {
                                    labels: arrLabel,
                                    datasets: [
                                        {
                                            fillColor: "rgba(254,246,207,0.5)",
                                            strokeColor: "rgba(250,196,130,1)",
                                            pointColor: "rgba(255,161,47,1)",
                                            pointStrokeColor: "#fff",
                                            data: arrData
                                        }
                                    ]
                                }
                                var myLine = new Chart(document.getElementById("search-booking-graph").getContext("2d")).Line(lineChartData);
                                //alert("'" + arrLabel.join("', '") + "'");
                            }
                            var aL = new Array(<?php echo "'" . str_ireplace(", ", "', '", $labels) . "'"; ?>);
                            var aDB = new Array(<?php echo $dataBook; ?>);
                            var aDS = new Array(<?php echo $dataSearch; ?>);
                            var aDF = new Array(<?php echo $dataFare; ?>);
                            _showChart(aL, aDB);
                        </script>
                        <div id="chartOptions" style="text-align: right;width:98%;padding-right: 2%;">Choose data point for chart <select id="ddlCharts" style="float:none !important;width:auto !important;" class="ddl" onchange="javascript: _changeChart(this.value);"><option value="bookings">Bookings</option><option value="searches">Searches</option><option value="fare">Total Fare</option></select> <a style="color:#128a30;" id="btnChClose" href="javascript: void(0);" onclick="javascript: _closeChart(this);">Hide Chart</a></div>
                        <div id="chartOptions2" style="display:none;text-align: right;width:98%;padding-right: 2%;"><a style="color:#128a30;" id="btnChClose" href="javascript: void(0);" onclick="javascript: _openChart(this);
                                            _showChart(aL, aDB);">Show Chart</a></div>
                        <?php
                            }
                            unset($chart);*/
                        ?>
            
                    <table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
                        <thead>
                            <tr>
                                <th align="left">#</th>
                                <?php
                                $sm = $sortFld == "source" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left">CORIC<br /><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=source&sm=" . $sm ?>">Source <?php if ($sortFld == "source") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
                                <?php
                                $sm = $sortFld == "booking_id" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=booking_id&sm=" . $sm ?>&dt=unsigned integer">Booking ID <?php if ($sortFld == "booking_id") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                    <?php
                                    $sm = $sortFld == "tour_type" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                    ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=tour_type&sm=" . $sm ?>">Type <?php if ($sortFld == "tour_type") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a><?php $sm = $sortFld == "origin_id" ? $sortMode == "asc" ? "desc" : "asc" : "asc"; ?><br /><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=origin_id&sm=" . $sm ?>">Origin <?php if ($sortFld == "origin_id") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a> - Destination</small></th>
                                <?php
                                $sm = $sortFld == "tot_fare" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=tot_fare&sm=" . $sm ?>&dt=unsigned integer">Fare <?php if ($sortFld == "tot_fare") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                <?php
                                $sm = $sortFld == "payment_status" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=payment_status&sm=" . $sm ?>&dt=unsigned integer">Status <?php if ($sortFld == "payment_status") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a>
                                <?php
                                $sm = $sortFld == "payment_mode" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?><br /><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=payment_mode&sm=" . $sm ?>&dt=unsigned integer">Payment Mode<?php if ($sortFld == "payment_mode") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></small></th>
                                <?php
                                $sm = $sortFld == "pickup_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=pickup_date&sm=" . $sm ?>&dt=datetime">Travel On <?php if ($sortFld == "pickup_date") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                <?php
                                $sm = $sortFld == "entry_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date&sm=" . $sm ?>&dt=datetime">Booked On <?php if ($sortFld == "entry_date" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a><br ><small><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=ip&sm=" . $sm ?>">IP <?php if ($sortFld == "ip") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a> | Browser</small></th>
                                <?php
                                $sm = $sortFld == "ip" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
                                ?>
                                <th align="left">PayBack<br /><small>Discount | Points | Status</small></th>
                                <th align="left">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!array_key_exists("response", $r)) {
                                $srno = $lStart + 1;
                                for ($i = 0; $i < count($r); $i++) {
                                    $cls = "od";
                                    $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showDetails(" . $r[$i]["uid"] . ", '" . $r[$i]["coric"] . "')\" title=\"Click on view details of " . $r[$i]["coric"] . "\">Details</a>";
                                    if ($i % 2 == 0)
                                        $cls = "ev";
                                    if ($r[$i]["payment_mode"] == 1 && ($r[$i]["payment_status"] == 1 || $r[$i]["payment_status"] == 3) && is_null($r[$i]["booking_id"]) && $r[$i]["AuthDesc"] == 'Y') {
                                        $cls = "er";
                                        $vTxt .= "&nbsp;<a href='javascript: void(0)' title='Failed booking. Click on View Details to create booking of " . $r[$i]["coric"] . "'>?</a>";
                                    }
                                    $payMode = "-";
                                    switch ($r[$i]["payment_mode"]) {
                                        case 1:
                                            $payMode = "Online";
                                            break;
                                        case 2:
                                            $payMode = "Pay to driver";
                                            break;
                                    }
                                    $payStatus = "-";
                                    if (intval($r[$i]["payment_mode"]) == 1) {
                                        switch ($r[$i]["payment_status"]) {
                                            case 1:
                                                $payStatus = "Success";
                                                break;
                                            case 2:
                                                $payStatus = "Gateway";
                                                break;
                                            case 3:
                                                $payStatus = "Payment";
                                                break;
                                            case 4:
                                                $payStatus = "Cancelled";
                                                break;
                                            case 5:
                                                $payStatus = "Unauthorised";
                                        }
                                    } elseif (intval($r[$i]["payment_mode"]) == 2) {
                                        switch ($r[$i]["payment_status"]) {
                                            case 1:
                                                $payStatus = "Success";
                                                break;
                                        }
                                    }
                                    $bookStyle = "";
                                    if ($payMode == "Online" && $payStatus == "Success") {
                                        if ($r[$i]["promotion_code"] == "PayBack")
                                            $bookStyle = "border-left:solid 5px #e2a115;";
                                        else
                                            $bookStyle = "border-left:solid 5px #1b9d06;";
                                    }
                                    elseif ($payMode == "Pay to driver" && $payStatus == "Success")
                                        $bookStyle = "border-left:solid 5px #060a9d;";
                                    //elseif($r[$i]["AuthDesc"] == 'N')a5109c
                                    //$payStatus = "Unauthorised";
                                    $ua = browser_info($r[$i]["ua"]);
                                    //echo $r[$i]["ua"] . "<pre>";
                                    $b = array_keys($ua);
                                    $v = array_values($ua);
                                    $pickDate = date_create($r[$i]["pickup_date"]);
                                    $pickTime = date_create($r[$i]["pickup_time"]);
                                    $dropDate = date_create($r[$i]["drop_date"]);
                                    if (!is_null($r[$i]["drop_time"]))
                                        $dropTime = date_create($r[$i]["drop_time"]);
                                    $bookDate = date_create($r[$i]["entry_date"]);
                                    ?>
                                    <tr class="<?php echo $cls; ?>">
                                        <td align="left" style="<?php echo $bookStyle; ?>"><?php echo $srno; ?></td>
                                        <td align="left"><?php echo $r[$i]["coric"]; ?><br /><small><?php echo $r[$i]["source"]; ?><?php if ($r[$i]["gclid"] != "") {
                                echo "&nbsp;|&nbsp;<span style='font-weight:bold;color:#a5109c;'>PPC</span>";
                            } ?></small></td>
                                        <td align="left"><?php echo $r[$i]["booking_id"] == "" ? "-" : $r[$i]["booking_id"]; ?></td>
                                        <td align="left"><?php echo $r[$i]["tour_type"]; ?><br /><small><?php echo $r[$i]["origin_name"]; ?> - <?php echo str_ireplace(",", ", ", $r[$i]["destinations_name"]); ?></small></td>
                                        <td align="left"><?php echo ($r[$i]["tot_fare"] + $r[$i]["additional_srv_amt"] + ceil($r[$i]["additional_srv_amt"] * $r[$i]["vat"] / 100)); ?></td>
                                        <td align="left"><?php echo $payStatus; ?><br /><small><?php echo $payMode; ?></small></td>
                                        <td align="left"><?php echo $pickDate->format('d M, Y'); ?>&nbsp;<small><?php echo $pickTime->format("is"); ?> HRS</small><br /><small><?php echo $dropDate->format('d M, Y'); ?>&nbsp;<?php if (!is_null($r[$i]["drop_time"])) {
                                echo $dropTime->format("is") . " HRS";
                            } ?></small></td>
                                        <td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookDate->format('H:i A'); ?></small><br /><small><?php echo $r[$i]["ip"]; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]); ?>&nbsp;<a href="javascript: void(0);" title="<?php echo $r[$i]["ua"]; ?>">?</a></small></td>
                                        <td align="left"><?php if ($r[$i]["disc_txn_status"] == "Approved") { ?>Rs. <?php echo intval($r[$i]["discount_amt"]); ?> | <?php echo $r[$i]["disc_value"]; ?> | <?php echo $r[$i]["disc_txn_status"]; ?><?php } else {
                                echo "-";
                            } ?></td>
                                        <td align="left"><?php echo $vTxt; ?></td>
                                    </tr>
        <?php
        $srno++;
    }
}
?>
                        </tbody>
                    </table>
                    <table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
                        <tr>
                            <td align="right" style="margin-right: 10px;">
<?php echo $pagingHTML; ?>
                            </td>
                        </tr>
                    </table>
<?php
unset($r);
?>
                </div>		
                <div class="clr"></div>
            </div>
            <p>&nbsp;</p>	    	
        </div>
<?php
include_once("./includes/footer.php");
?>
    </body>
</html>
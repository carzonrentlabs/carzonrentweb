<?php
    include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/admin/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/admin/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> SelfDrive Search Data Report</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./selfdrive-search-report.php";
			$dlndURL = "./selfdrive-search-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "unique_id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("source" => "Source", "coric" => "CORIC", "booking_id" => "Booking ID", "tour_type" => "Tour Type", "package_id" => "Package ID","car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			if(isset($_GET["f"]) && $_GET["f"] != ""){
			    $fld = $_GET["f"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["q"]) && $_GET["q"] != ""){
			    $fVal = $_GET["q"];
			    $isFiltered = true;
			    if($fld != ""){
				if($fld == "payment_mode"){
				    if(trim(strtolower($fVal)) == "online")
					$fVal = 1;
				    elseif(trim(strtolower($fVal)) == "pay to driver")
					$fVal = 2;
				} elseif($fld == "payment_status"){
				    if(trim(strtolower($fVal)) == "success")
					$fVal = 1;
				    elseif(trim(strtolower($fVal)) == "gateway")
					$fVal = 2;
				    elseif(trim(strtolower($fVal)) == "payment")
					$fVal = 3;
				    elseif(trim(strtolower($fVal)) == "failure" || trim(strtolower($fVal)) == "cancelled")
					$fVal = 4;
				    elseif(trim(strtolower($fVal)) == "unauthorised")
					$fVal = 5;
				}				
			    }
			}
			
			if(isset($_GET["inc"]) && $_GET["inc"] != ""){
			    $isDtInc = true;
			    $isFiltered = true;
			}
			
			if(isset($_GET["sf"]) && $_GET["sf"] != "")
			$sortFld = $_GET["sf"];
			
			if(isset($_GET["sm"]) && $_GET["sm"] != "")
			$sortMode = $_GET["sm"];
			
			if(isset($_GET["dt"]) && $_GET["dt"] != "")
			    $sortDType = "CONVERT(" . $sortFld . ", " . $_GET["dt"] . ")";
			else {
			    if(isset($_GET["sf"]) && $_GET["sf"] != "")
			    $sortDType = $sortFld;
			    else
			    $sortDType = "CONVERT(" . $sortFld . ", unsigned integer)";		    
			}
			//$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    
			
			$lStart = ($cPage - 1) * $recPerPage;
			$totATRec = 0;
			$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_booking_new";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totATRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$totRec = 0;if (isset($_REQUEST["ddlOrigin"]) && $_REQUEST["ddlOrigin"] != "") {
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(unique_id) as TotalRecord FROM customer_search WHERE " . $fld . " = '" . $fVal . "' AND tour_type = 'Selfdrive' AND origin_code=".$_REQUEST['ddlOrigin']." ";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'";
			    }
			} else {
			    $sql = "SELECT COUNT(unique_id) as TotalRecord FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type = 'Selfdrive' AND origin_code=".$_REQUEST['ddlOrigin']."";
                        }}else{
                            if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(unique_id) as TotalRecord FROM customer_search WHERE " . $fld . " = '" . $fVal . "' AND tour_type = 'Selfdrive' ";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'";
			    }
			} else {
			    $sql = "SELECT COUNT(unique_id) as TotalRecord FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type = 'Selfdrive'";
                        }
                        }
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			//$totRec = 0;
                        if (isset($_REQUEST["ddlOrigin"]) && $_REQUEST["ddlOrigin"] != "") {
                            if($fld != "" && $fVal != ""){
                                $sql = "SELECT * FROM customer_search WHERE " . $fld . " = '" . $fVal . "' AND tour_type = 'Selfdrive' AND origin_code=".$_REQUEST['ddlOrigin']."";
                                if($isDtInc){
                                    $sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' ";
                                }
                                $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
                            } else {
                                $sql = "SELECT * FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type = 'Selfdrive' AND origin_code=".$_REQUEST['ddlOrigin']." ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
                            }
                                        
                        } else {
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT * FROM customer_search WHERE " . $fld . " = '" . $fVal . "' AND tour_type = 'Selfdrive'";
			    if($isDtInc){
				$sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' ";
			    }
			    $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			} else {
			    $sql = "SELECT * FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "' AND tour_type = 'Selfdrive' ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
                                    }}
			//echo $sql;
			$r = $db->query("query", $sql);
			//echo "<pre>";
			//print_r($r);
			//echo "</pre>";
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));

			$db->close();
			//print_r($r);
			
			$pagingHTML = "";
			if($numOfPage > 1){
			    $nURL = "";
			    $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));			    
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    $pagingHTML .= "<td>";
				    if($cPage % $maxPageNum != 0)
					$pgStart = $cPage - ($cPage % $maxPageNum);
				    else
					$pgStart = $cPage - $maxPageNum;
				    $pgStart++;
				    if($cPage > $maxPageNum){
					$pgURL = $nURL;
					$pgPrev = $pgStart -  1;
					$pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
				    }
				    for($p = $pgStart; $p <= $numOfPage; $p++){
					$pgURL = $nURL;
					if($p != $cPage){
					    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
					}
					else
					    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
					if($p % $maxPageNum == 0)
					break;
				    }
				    if($numOfPage > $p){
					$p++;
					$pgURL = $nURL;
					$pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
				    }
				    $pgURL = $nURL;
				    $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
			    $pagingHTML .= "Go to page number <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
				    for($p = 1; $p <= $numOfPage; $p++){
					if($p != $cPage){
					    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
					} else {
					    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
					}
				    }
					$pagingHTML .= "</select>";
				    $pagingHTML .= "</td>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			} else {
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    if(($lStart + $recPerPage) >= $totRec)
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
				    else
				    $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			}
		?>
	    <?php
		if($_SERVER["QUERY_STRING"] != ""){
	    ?>
		<script type="text/javascript">
	    <?php
		    $qsParams = explode("&", $_SERVER["QUERY_STRING"]);
		    for($q = 0; $q < count($qsParams); $q++){
			if(trim($qsParams[$q]) != ""){
			    $qsParam = explode("=", $qsParams[$q]);
	    ?>
			    qsArray.push(new Array('<?php echo $qsParam[0]; ?>', '<?php echo $qsParam[1]; ?>'));
	    <?php
			}
		    }
	    ?>
		</script>
	    <?php
		} else {
	    ?>
		<script type="text/javascript">
		    qsArray.push(new Array('sd', '<?php echo $sd; ?>'));
		    qsArray.push(new Array('ed', '<?php echo $ed; ?>'));
		</script>
	    <?php
		}
	    ?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
                                            <td><label>Location</label></td>
                                            <td>
                                                <?php
                                                $db = new MySqlConnection(CONNSTRING);
                                                $db->open();
                                                $sql = "SELECT * FROM cor_location";
                                                $rloc = $db->query("query", $sql);
                                                if (!array_key_exists("response", $rloc)) {
                                                    ?>
                                                    <select class="ddlS" name="ddlOrigin" id="ddlOriginSF" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <option value="">Select One....</option>   <?php
                                                        for ($i = 0; $i < count($rloc); $i++) {
                                                           if($_REQUEST['ddlOrigin']==$rloc[$i]["city_id"]){
                                                               $sel = 'selected="selected"';
                                                           }else{
                                                               $sel = '';
                                                           } ?>
                                                        
                                                            <option <?php echo $sel;?> value="<?php echo $rloc[$i]["city_id"]; ?>"><?php echo $rloc[$i]["CityName"]; ?></option>
                                                            <?php ?>

                                                        <?php } ?>

                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
                                            <td>
                                                <?php 
                                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']!=''){
                                                        $db = new MySqlConnection(CONNSTRING);
                                                        $db->open();
                                                        $cityid = $_REQUEST['ddlOrigin'];
                                                        $sql = "SELECT * FROM customer_search where origin_code = '$_REQUEST[ddlOrigin]' and (entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."')";
                                                        $rx = $db->query("query", $sql);
                                                        if (!array_key_exists("response", $rx)) {
                                                            $hour = '';$days = '';
                                                            for ($i = 0; $i < count($rx); $i++) {

                                                                if($rx[$i]['rental_type']=='Hourly'){
                                                                    $hour +=  $rx[$i]['duration'];
                                                                    ?>
                                                                   
                                                                <?php }
                                                                else{
                                                                    $days +=  $rx[$i]['duration'];
                                                                    ?>
                                                        
                                                                    <?php }
                                                            } 
                                                            ?>
                                                <td style="padding-left: 30px;"><span class="dataLabel" style="min-width: 100px !important;">Total Hours: <b style="color:#333;"><?php echo ($hour=='' || !$hour)?'0':$hour;?></b></span></td> 
                                                                    <td><span class="dataLabel" style="min-width: 100px !important;">Total Days: <b style="color:#a5109c;"><?php echo ($days=='' || !$days)?'0':$days;?></b></span></td>
                                                                <?php
                                                        }
                                                    }?>
                                                
                                            </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				    <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
				    <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
				    <table cellspacing="1" cellpadding="5" border="0">
					<tbody>
					    <tr>
						<td colspan="2" valign="middle" align="right">
						    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
							<tr>
							    <td><a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if(filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != ""){echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));} ?>" style="color:#128a30;">Download</a></td>
						    <?php
							if($numOfPage > 1){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $dlndURL; ?>?<?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
						    <?php
							}
							if($isDtInc || $isFiltered){
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
						    <?php
							} else {
						    ?>
							    <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/admin/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
							    <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
						    <?php
							}
						    ?>
							</tr>
						    </table>
						    
						    
						</td>
					</tbody>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="width:50%;"><?php echo $pagingHTML; ?></td>
			    </tr>
			</table>
			<table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">#</th>
				<?php
				    $sm = $sortFld == "pickup_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=pickup_date&sm=" . $sm ?>&dt=datetime">Pickup Date <?php if ($sortFld == "pickup_date") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "drop_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=drop_date&sm=" . $sm ?>&dt=datetime">Dropoff Date <?php if ($sortFld == "drop_date") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "duration" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=duration&sm=" . $sm ?>&dt=datetime">Duration <?php if ($sortFld == "duration") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                <?php
				    $sm = $sortFld == "duration" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=duration&sm=" . $sm ?>&dt=datetime">Duration Type <?php if ($sortFld == "duration") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "origin_id" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=origin_id&sm=" . $sm ?>">Origin <?php if ($sortFld == "origin_id") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "entry_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date&sm=" . $sm ?>&dt=datetime">Searched On <?php if ($sortFld == "entry_date" || $sortFld == "uid") { ?><img src="<?php echo corWebRoot; ?>/admin/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    
				    if($i % 2 == 0)
					$cls = "ev";
				    
				    $pickDate = date_create($r[$i]["pickup_date"]);
				    $dDate = strtotime("+" . $r[$i]["duration"] . " days", strtotime($pickDate->format("Y") . "-" . $pickDate->format("m") . "-" . $pickDate->format("d")));
				    $bookDate = date_create($r[$i]["entry_date"]);
				    $bookTime = date_create($r[$i]["entry_time"]);
		    ?>
				    <tr class="<?php echo $cls; ?>">
					<td align="left"><?php echo $srno; ?></td>
					<td align="left"><?php echo $pickDate->format('d M, Y'); ?></td>
					<td align="left"><?php echo date("d M, Y", $dDate); ?></td>
					<td align="left"><?php echo $r[$i]["duration"]; ?></td>
                                        <td align="left"><?php echo $r[$i]["rental_type"]; ?></td>
					<td align="left"><?php echo $r[$i]["origin_name"]; ?></td>
					<td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookTime->format('H:i A'); ?></small></td>
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
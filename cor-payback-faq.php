<?php include_once('./includes/cache-func.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PAYBACK FAQ</title>
<link rel="stylesheet" href="css/payback-faq.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js"></script>
<script type="text/javascript">
function chkPBRegister()
{
	chk = true;
	chk = isFilledText(document.getElementById("txtname"), "", "Name can't be left blank.","err");
	if(chk == true)
	{
		chk = isFilledText(document.getElementById("txtmobile"), "10 digit mobile number", "Mobile number can't be left blank.","err");
		if(chk)
			chk = isFilledText(document.getElementById("txtmobile"), "", "Mobile number can't be left blank.","err");
		if(chk)
		{
			if(document.getElementById("txtmobile").value.length < 10)
			{
				document.getElementById("txtmobile").innerHTML = "Please enter a valid 10 digit mobile number.";
				chk = false;
			}
		}
	}
	if(chk == true)
		chk = isFilledSelect(document.getElementById("ddlday"), 0, "Day can't be left blank.", 0, "err");
	if(chk == true)
		chk = isFilledSelect(document.getElementById("ddlmonth"), 0, "Month can't be left blank.", 0, "err");
	if(chk == true)
		chk = isFilledSelect(document.getElementById("ddlyear"), 0, "Year can't be left blank.", 0, "err");
	if(chk == true)
		chk = isFilledSelect(document.getElementById("ddlgender"), 0, "Gender can't be left blank.", 0, "err");
	if(chk)
		chk = isFilledText(document.getElementById("txtemailid"), "", "Emailid can't be left blank.", "err");
	if(chk)
		chk = isEmailAddr(document.getElementById("txtemailid"), "Please fill in a valid email id.", "err");
	if(chk)
		chk = isFilledText(document.getElementById("txtcity"), "", "City can't be left blank.", "err");
	if(chk)
		chk = isFilledText(document.getElementById("txtpin"), "", "Pin can't be left blank.", "err");
	if(chk)
		_createPaybackRegistaion();
}
function _createPaybackRegistaion()
{
	var name = document.getElementById("txtname").value;
	var mobile = document.getElementById("txtmobile").value;
	var dob = document.getElementById('ddlyear').options[document.getElementById('ddlyear').selectedIndex].value + "-" + document.getElementById('ddlmonth').options[document.getElementById('ddlmonth').selectedIndex].value + "-" + document.getElementById('ddlday').options[document.getElementById('ddlday').selectedIndex].value;
	var gender = document.getElementById("ddlgender").value;
	var email = document.getElementById("txtemailid").value;
	var city = document.getElementById("txtcity").value;
	var pin = document.getElementById("txtpin").value;
	var source = document.getElementById("hdsource").value;
	if(window.XMLHttpRequest)
		aj = new XMLHttpRequest;
	else if(window.ActiveXObject)
		aj = new ActiveXObject("Microsoft.XMLHTTP");
	aj.open("POST","paybackregistration.php", true);
	aj.onreadystatechange = _httpRegisterPB;
	aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	var values = "name="+ name + "&mobile="+ mobile + "&dob="+ dob + "&gender="+ gender + "&email="+ email + "&city="+ city + "&pin=" + pin + "&source=" + source;
	aj.send(values);
};
_httpRegisterPB = function(){
    if(aj.readyState == 4)
    { 			
		var disp = aj.responseText;			
		if(disp != "" && disp != undefined)
		{
			if(disp == "SUCCESS")
			{
				document.getElementById('err').innerHTML = "Thank you. You have been successfully registered with PAYBACK.";
				document.getElementById("txtname").value = "";
				document.getElementById("txtemailid").value = "";
				document.getElementById("txtmobile").value = "";
				document.getElementById("txtcity").value = "";
				document.getElementById("ddlday").selectedIndex = 0;
				document.getElementById("ddlmonth").selectedIndex = 0;
				document.getElementById("ddlyear").selectedIndex = 0;
				document.getElementById("ddlgender").selectedIndex = 0;
				document.getElementById("txtpin").value = "";
			}
			else
			{
				document.getElementById('err').innerHTML = "Oops! There was an error while registering you with PAYBACK. Please try again.";		
			}
		}
    }
    else
		document.getElementById('err').innerHTML = "Please wait...";		
};
</script>
</head>

<body>
    <div class="payback-container">
<!--header Start-->    
    	<div class="paybhack-header">
        	<div class="easycabs-logo"><a href="./"><img src="images/COR-logo.png" /></a></div>
            <div class="payback-logo"><img src="images/payback-logo.png" /></div>
        <div class="clearfix"></div>
        </div>
<!--header Close-->        
        <div class="middle-body">
        	<div class="my-script"><b>Prompt on every call</b><br />

<p>Agent (after capturing all booking details) “Dear Sir/Mam, would you like to add PAYBACK points to your account for this booking?”<br />
Customer- Yes/No (the agent needs to checkmark the box when the customer says yes) and carry on in case of a “No” & if no further questions are asked by the customer.</p>

       					 	Or

<p>Customer- What is it about?/What is PAYBACK?<br />
Agent- Dear Sir/Mam, we have partnered with PAYBACK for a loyalty program where you can 40 PAYBACK points for every booking with us. May I transfer the call to our CRM team for more details on this program?<br />
Customer- No (carry on with the booking)<br />
Customer-Yes (transfer the call)<br />
CRM executive- Goodmorning/afternoon Mr./Ms. this is …. In regards to your query regarding the loyalty program, I would like to inform you that in order to offer loyalty benefits to our customers, we have come up with a loyalty program with our partner PAYBACK. Through this, you can earn 40 PAYBACK points for every booking with us. The PAYBACK points can be redeemed for a discount on our services in a fixed proportion or on any of the other PAYBACK partners accordingly.
The membership is exclusive & free of cost for you, would you want to become a PAYBACK member?<br />
Customer – No, will let you know later<br />
Agent – No issues sir/mam. May I assist you for your booking?....................</p>

 						Or
Customer- Yes
Agent – open ups enrollment form & captures details. (Name, Contact No., Email , Gender, City, Pin code, DOB)
</div>
        	<div class="faq">FAQ</div>
            <div class="mid-area">
<!--Question Start -->            
            	<div class="question">
            		<div class="all-question">Q. What is PAYBACK/Loyalty program?</div>
                    <div><p>A. In order to offer loyalty benefits to our customers, we have come up with a loyalty program with our partner PAYBACK. Through this, you can earn 8 PAYBACK points for every Rs.100 spent on your booking with us. Each PAYBACK point is valued at Rs.0.25/- & can be redeemed for a discount on our services or any of the other PAYBACK partners.</p></div>
                	
                    <div class="all-question">Q. Who are other the PAYBACK partners? Please name a few.</div>
                    <div><p>A. Book my show, Future Group(Big Bazaar, Pantaloon….), Vodafone, HPCL, Make my trip, Flipkart.com , etc</p></div>
                	
                    <div class="all-question">Q. What is the benefit to me?</div>
                    <div><p>A. You can get 8 PAYBACK points per Rs.100 spent on our services & they can be redeemed for a discount on our services or any of the other PAYBACK partners. Also, you can redeem/burn your accumulated PAYBACK points for a discount on our services.</p></div>
                    
                    <div class="all-question">Q. How are the points calculated?</div>
                    <div><p>A. For earn/add/credit points- the points are calculated as per the transaction value where you get 8 PAYBACK points for every Rs. 100 spent with us.<br />
                    For burn/redemption – 1 PAYBACK point can be redeemed at Rs.0.25 for a discount on our services.</p>
                   
                    </div>
                    
          			<div class="all-question">Q. What all services is the program/offer applicable?</div> 
                    <div><p>A. The points can be earned & burned on our local, outstation & self-drive services</p></div> 
                    
					<div class="all-question">Q. Is there a condition to earn or burn points?</div>
                    <div><p>A. Dear Sir/Mam, the only condition to earn/burn is that the booking needs to be executed/completed & the mobile number mentioned should be linked to your PAYBACK account.</p></div>
                            
                    <div class="all-question">Q. Where all can I earn/add points & how?</div>
                    <div><p>A. Currently the points can be earned at our call center, website & airport counters. For the call center & airport counters, you need to announce the PAYBACK membership while making your reservation.<br />
                    And at the website you need to checkmark the PAYBACK membership field- <b>“I want to earn PAYBACK points”</b> in the booking engine.</p>
                    
                    </div>
                    
                    <div class="all-question">Q. Where all can I burn/redeem points & how?</div>
					<div><p>A. You can burn/redeem the points at our website & call center for online payments only. At the call center, you need to announce the redemption while making your reservation & on the website-</p>
                    <p>1. In the discount/promotion code field of the booking engine, select “I want to redeem PAYBACK points”, enter card/membership number & the number of points to be redeemed. Click on “Redeem points & confirm booking” to proceed.<br />
                    2. Authenticate with your 4 digit PAYBACK Pin & confirm.<br />
                    3.  Pay online for your booking (post the discount)<br /> </p>
                    </div>   
                    <div class="all-question">Q. Can I also Earn/burn PAYBACK Points at the airport?</div>
                    <div><p>A. Currently, you can only earn points from the airport. The redemption can only happen on the website & the call center.</p></div>
                    <div class="all-question">Q. Can I redeem points on multiple PAYBACK cards in one booking?</div>
                    <div><p>A. No, you can only redeem points on one PAYBACK card in one transaction.</p></div>
                   <div class="all-question">Q. What if there is an error/transaction failure & the final amount does not reflect the discount & my points are deducted?</div>
                   <div><p>A. Dear Sir/Mam, while our systems are robust & error free, if in case the final amount does not reflect the discount post the point deduction, you may please contact PAYBACK at 1860-258-5000 & your points will be credited back within 7 days.</p>
                   </div>
                   <div class="all-question">Q. How will the points be added/credited to my account as you do not ask the PAYBACK membership number at the call center & on the website?</div>
                   <div><p>A. Dear Sir/Mam, the points will be added through the mobile number linked to your PAYBACK account. You are requested to fill in/mention the linked mobile number for your reservation. And, if in case your mobile number is not linked to your PAYBACK account or you want to check the same, we suggest you to call the PAYBACK contact center at 1860-258-5000.</p></div>
                   <div class="all-question">Q. How will I know if my mobile number is linked/updated? /What if my mobile number is not linked? Will I earn points then?</div>
                   <div><p>A. No, in case the mobile number is not linked, the points shall not be added/earned/credited. We suggest you to call the PAYBACK contact center at 1860-258-5000 to get the mobile number linked/checked for your account.</p></div>
                   <div class="all-question">Q. How can I enroll myself?</div>
                   <div><p>A. You just need to share your Name, Contact no. , gender, email id, city & Pin code with us and you will be enrolled as a PAYBACK member within few days.</p></div>
                   <div class="all-question">Q.  Where can I enroll myself?</div>
                   <div><p>A.	You can enroll for the PAYBACK loyalty program at our call center or with PAYBACK at 1860-258-5000.</p></div>
                   <div class="all-question">Q. Is there a cost of the membership?</div>
                   <div><p>A. No, as an exclusive member, the loyalty program membership is free of cost for you.</p></div>
                   <div class="all-question">Q. What is the eligibility criteria of the PAYBACK membership?</div>
                   <div><p>A. 18+ age & a valid mailing address.</p></div>
                   <div class="all-question">Q. When do the points get credited/added to my account?</div>
                   <div><p>A: The points shall be credited to your PAYBACK account within a maximum of 7 days post the journey completion.</p></div>
                   <div class="all-question">Q. I forgot to announce my membership at the call center/checkmark on the website while booking, can you add points now?(retro credits)</div>
                   <div><p>A. No, unfortunately we will not able to credit/add your points now. We suggest you to announce the membership in your next booking with us.</p></div>
                   <div class="all-question">Q. I have 2 or more PAYBACK cards? On which card will the points be transferred?</div>
                   <div><p>A. You will earn PAYBACK points on the account/card which is linked to the mobile number used for the booking.</p></div>
                   <div class="all-question">Q. Can I pay 100% of bill value through Points?</div>
                   <div><p>A. Yes, you can avail a 100% discount if the points value matches the transaction value.</p></div>
                   <div class="all-question">Q. Cancellation— what happens if the booking gets cancelled at your end & if I cancel the booking, what will happen to my points eared/added?</div>
                   <div><p>A.  In case of a cancellation, our system will not add/earn points to your account.</p></div>
                   <div class="all-question">Q. Cancellation— what happens if the booking gets cancelled at your end or I cancel the booking post point’s redemption?</div>
                   <div><p>A. In case of a cancellation, the points shall be credited back to your account within 7 days. </p></div>
                   
                   <div class="all-question">Q. Redemption – Will I get points on the full bill value even if I redeem points partly?</div>
                   <div><p>A. Dear Sir/Mam, as of now you can either burn or redeem points in one transaction.</p></div>
                   <div class="all-question">Q. Will this be a plastic card or an e-card?</div>
                   <div><p>A. Dear Sir/mam, this will be an e-card where you can quote your linked mobile number to utilize your membership. However, you can also request for a free card with Payback at 1860-258-5000. </p></div>
                   <div class="all-question">Q. How will the customer get to know the earn conditions/details on the website?</div>
                   <div><p>A. The customer will get to know through a disclaimer box placed with the checkmark text - “I want to earn Payback points”</p></div>
                   <div class="all-question">Q. Is this applicable for corporate bookings?</div>
                   <div><p>A. As of now this is only applicable for COR-Retail bookings.</p></div>
                  
                   
            </div>
<!--Question Ends -->            
            	<div class="my-form">
                	<div><h3>Customer Enrollment Form</h3></div>
                    <form>
						<input type="hidden" name="hdsource" id="hdsource" value="COR" />
                    	<div>
                        	<div class="form-text">Name</div>
							<input type="text" tabindex="1" id="txtname" name="txtname" value="" class="tbox" placeholder="Name" maxlength="50"/>
                        </div>
                        <div>
                        	<div class="form-text">Mobile Number</div>
                            <input type="text" tabindex="2" id="txtmobile" name="txtmobile" value="" class="tbox" placeholder="Mobile" onkeypress="javascript: return _allowNumeric(event);" maxlength="12"/>
                        </div>
                        <div>
                        	<div class="form-text">DOB</div>
                            	<select id="ddlday" name="ddlday" tabindex="3" class="dob">
                                	<option value="0">Date</option>
                                    <?php
										for($dd = 1; $dd <=31; $dd++)
										{
									?>
										<option value="<?php echo $dd; ?>"><?php echo $dd; ?></option>
									<?php
										}
									?>
                                </select>
                                <select id="ddlmonth" name="ddlmonth" tabindex="4" class="dob">
                                	<option value="0">Month</option>
                                    <?php
										for($mm = 1; $mm <=12; $mm++)
										{
									?>
										<option value="<?php echo $mm; ?>"><?php echo $mm; ?></option>
									<?php
										}
									?>
                                </select>
                                   <select id="ddlyear" name="ddlyear" tabindex="5" class="dob">
                                	<option value="0">Year</option>
                                    <?php
										for($yy = 1925; $yy <=2012; $yy++)
										{
									?>
										<option value="<?php echo $yy; ?>"><?php echo $yy; ?></option>
									<?php
										}
									?>
                                </select>
                        </div>
                        <div>
                        	<div class="form-text">Gender</div>
                            <select id="ddlgender" name="ddlgender" tabindex="6">
                            	<option value="0">Please select</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            
                            </select>
                        </div>
                        <div>
                        	<div class="form-text">Email</div>
                            <input type="text" tabindex="7" id="txtemailid" name="txtemailid" value="" placeholder="Email" maxlength="50"/>
                        </div>
                        <div>
                        	<div class="form-text">City</div>
                            <input type="text" tabindex="8" id="txtcity" name="txtcity" value="" placeholder="City" maxlength="50"/>
                        </div>
                        <div>
                        	<div class="form-text">Pincode</div>
                            <input type="text" tabindex="9" id="txtpin" name="txtpin" value="" placeholder="Pincode" onkeypress="javascript: return _allowNumeric(event);" maxlength="10"/>
                        </div>
                       <div id="err"></div>  
                      <div><input type="button" id="btnsubmit" name="btnsubmit" class="my-button" value="" onclick="chkPBRegister();" /></div>  
                    
                    </form>
                    
                </div>
         <div class="clearfix"></div>       
                
            </div>
        </div>
        
    </div>
</body>
</html>

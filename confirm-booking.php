<?php
    if(isset($_POST["monumber"])){
        //echo "<pre>";
        include_once('./classes/cor.ws.class.php');
        include_once('./classes/cor.xmlparser.class.php');
        include_once('./classes/cor.gp.class.php');
        $query_string = "";
        if ($_POST) {
          $kv = array();
          foreach ($_POST as $key => $value) {
                if($key != "corpassword")
                $kv[] = "$key=$value";
          }
          $query_string = join("&", $kv);
        }
        else {
          $query_string = $_SERVER['QUERY_STRING'];
        }
        $fullname = explode(" ", trim($_POST["name"]));
        if(count($fullname) > 1)
        {
            $firstName = $fullname[0];
            $lastName = $fullname[1];
        }
        else
        {
            $firstName = $fullname[0];
            $lastName = "";
        }
        $cor = new COR();
        $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>1684));
        $myXML = new CORXMLList();
        $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});            
        if(intval($arrUserId[0]["ClientCoIndivID"]) > 0){
            $mtime = round(microtime(true) * 1000);
            $pkgId = $_POST["hdPkgID"];
            $destination = $_POST["hdDestinationName"];            
            $TimeOfPickup = $_POST["picktime"];
            $address = $_POST["address"];
            $phone = $_POST["monumber"];
            $emailId = $_POST["email"];
            $userId = $_POST["cciid"];
            $paymentAmount = $_POST["totFare"];
            $paymentType = $_POST["rdoPayment"];
            $paymentStatus = "0";
            $trackId = $_POST["hdTrackID"];
            $transactionId = $_POST["hdTransactionID"];
            $discountPc = "0";
            $remarks = "";
            $totalKM = $_POST["hdDistance"];
            $visitedCities = $_POST["hdDestinationName"];
            $pincode = $_POST["pincode"];
            $gpAddress = $_POST["gpaddress"];
            $outStationYN = "true";
            if($_POST["hdTourtype"] != "Local")
                $outStationYN = "true";
            else
                $outStationYN = "false";
            if(isset($_POST["hdPickdate"]))
            {
                $pDate =  $_POST["hdPickdate"];
                $pickDate = date_create($pDate);
            }
            if(isset($_POST["hdDropdate"]))
            {
                $dDate =  $_POST["hdDropdate"];
                $dropDate = date_create($dDate);
            }
            $dateOut = $pickDate->format('m/d/Y');
            $dateIn = $dropDate->format('m/d/Y');
            
            if($_POST["rdoPayment"] == 2){
                $transactionDetails = array();
                $transactionDetails['customerDetails'] = array();
                $transactionDetails['orderDetails'] = array();
                $transactionDetails['additionalInformation'] = array();
                
                $transactionDetails['customerDetails']['address'] = $gpAddress;
                $transactionDetails['customerDetails']['contactNo'] = $phone;
                $transactionDetails['customerDetails']['email'] = $emailId;
                $transactionDetails['customerDetails']['firstName'] = $firstName;
                $transactionDetails['customerDetails']['lastName'] = $lastName;
                $transactionDetails['customerDetails']['prefix'] = "Mr/Ms";
                
                $transactionDetails['orderDetails']['pincode'] = $pincode;
                $transactionDetails['orderDetails']['clientOrderID'] = $trackId;
                $transactionDetails['orderDetails']['deliveryDate'] = $pickDate->format('d-m-Y');
                $transactionDetails['orderDetails']['orderAmount'] = $paymentAmount;
                $transactionDetails['orderDetails']['templateID'] = 1;
                
                $productDetails = array();
                $productDetails['productID'] = "COR-1684";
                $productDetails['productQuantity'] = 1;
                $productDetails['unitCost'] = $paymentAmount;
                
                $transactionDetails['orderDetails']['productDetails'] = array($productDetails);
                
                $gp = new GharPay();
                $var = $gp->_CORGPCreateOrder($transactionDetails);                                                             
                $transactionId = $var;
                unset($gp);
            }
            $corArrSTD = array();
            $corArrSTD["pkgId"] = $pkgId;
            $corArrSTD["destination"] = $destination;
            $corArrSTD["dateOut"] = $dateOut;
            $corArrSTD["dateIn"] = $dateIn;
            $corArrSTD["TimeOfPickup"] = $TimeOfPickup;
            $corArrSTD["address"] = $address;
            $corArrSTD["firstName"] = $firstName;
            $corArrSTD["lastName"] = $lastName;
            $corArrSTD["phone"] = $phone;
            $corArrSTD["emailId"] = $emailId;
            $corArrSTD["userId"] = $userId;
            $corArrSTD["paymentAmount"] = $paymentAmount;
            $corArrSTD["paymentType"] = $paymentType;
            $corArrSTD["paymentStatus"] = $paymentStatus;
            $corArrSTD["trackId"] = $trackId;
            $corArrSTD["transactionId"] = $transactionId;
            $corArrSTD["discountPc"] = $discountPc;
            $corArrSTD["remarks"] = $remarks;
            $corArrSTD["totalKM"] = $totalKM;
            $corArrSTD["visitedCities"] = $visitedCities;
            $corArrSTD["outStationYN"] = $outStationYN;
//print_r($corArrSTD);
            $res = $cor->_CORMakeBooking($corArrSTD);
//print_r($res);
            $myXML = new CORXMLList();
            if($res->{'SetTravelDetailsResult'}->{'any'} != ""){
                $arrUserId = $myXML->xml2ary($res->{'SetTravelDetailsResult'}->{'any'});
                if($arrUserId[0]["Column1"] > 0){
                    $url = "./my-account-trip-details.php?" . $query_string . "&resp=booksucc&bookid=" . $arrUserId[0]["Column1"];
                }
            }
            else{
                $url = "./search-result.php?" . $query_string . "&resp=bookfail";
            }            
        }
        else {
            $url = "./search-result.php?" . $query_string . "&err=invnum";
        }
        
        unset($cor);        
        header('Location: ' . $url);
    }
?>
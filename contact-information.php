<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COR</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="js/customSelect.jquery.js"></script>

<script type="text/javascript" src="js/_bb_general_v3.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/funtion_init.js"></script>

<script>
$(function(){
	$('select.time').customSelect();
	$('select.area').customSelect();
	$('select.styled').customSelect();
})
function resgiter_cont(){
	document.getElementById("register").style.display = 'none';  
	document.getElementById("travel_details").style.display = 'block';  
}
</script>


</head>
<body>
<!--Header Start Here-->
<div id="header">
	<div class="main">
    	<div class="logo"><a href="./" id="brand" class="easybrand"></a></div>
        <div class="logo-right">
        	<div class="logo-righttop">
            	<ul class="lf">
                	<li><a href="#">COR-Car rentals</a></li>
                    <li><a href="#" class="active">COR-Easycabs</a></li>
                    <li><a href="#">COR-leasing</a></li>
                </ul>
                <ul class="rt">
                	<li><a href="#">Corporate User</a></li>
                    <li><a href="#">Login</a></li>
                    <li class="no"><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="menu">
            	<ul>
                	<li><a href="#" class="active">Delhi-NCR </a></li>
                    <li><a href="#">Mumbai</a></li>
                    <li><a href="#">Bangalore</a></li>
                    <li><a href="#">Hyderabad</a></li>
                    <li class="more"><a href="#">More</a>
                    	<ul class="submenu">
                        	<li><a href="#">Mumbai</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Kolkatta </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Chandigarh  </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                            <li><a href="#">Ahmedabad </a></li>
                        </ul>
                    </li>
                    <li class="tfn">Call 24 hours (prefix city code)<strong>0888 222 2222</strong> </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_right"></div>
</div>
<!--Banner Start Here-->
<div class="tbbingouter">
  <div class="main">
    <ul id="countrytabs" class="shadetabst">
      <li class="step1"><a href="#" rel="country1">Step 1</a></li>
      <li class="step2"><a href="#" rel="country2" class="selected">Step 2</a></li>
      <li class="step3"><a href="#" rel="country3">Step 3</a></li>
    </ul>
  </div>
</div>
<!--Middle Start Here-->

<div class="tbbinginr">
  <div id="country2" class="tabcontent">
   <form action="" method="">
    <div class="main">
      <div class="innerpages">
        <div class="leftside">
          <div id="register">
          <h1>Travel Details</h1>
          <div class="middiv">
         
              <div class="tber">
              <div class="bdrbtm">
                <p>
                    <label>Pickup Time </label>
                    <span class="pickuphour">
                        <select name="tHour" id="tHour" onchange="javascript: _setTime();" class="styled wdth60">
                             <option>Hours</option> 
								<?php for($i = 0; $i <= 23; $i++) { if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
									<option value="<?php echo $t; ?>"><?php echo $t; ?></option> 
								<?php } ?>
                        </select>
                    </span>
                    <span class="pickupmin ml5">
                    <select  name="tMin" id="tMin" onchange="javascript: _setTime();" class="styled wdth60">
                       <option>Min</option>  
						<option value="00">00</option> 
						<option value="15">15</option> 
						<option value="30">30</option>
						<option value="45">45</option>
                    </select>
                    </span>
					<span id="selT" style="margin-left:6px" class="showtime"><span id="seltime"></span></span>
                </p>
                <p>
                    <label>Pickup Address</label>
                    <span class="address">
                        <textarea rows="" cols=""></textarea>
                    </span>
                </p>
                </div>
                <p>
                    <label>Name</label>
                    <input type="text" class="bigtxtbox" name="txtname" id="txtname" />
                </p>
                <p>
                    <label>Mobile</label>
                    <input type="text" class="bigtxtbox" name="txtmobile" id="txtmobile" />
                </p>
                <p>
                    <label>Email</label>
                    <input type="text" class="bigtxtbox" name="txtemailid" id="txtemailid" />
                </p>
                <p>
                    <label>&nbsp;</label>
                    <span class="confirmbooking" style="float:left;">
                        <a href="#"></a>
                    </span>
                </p>
              </div>
            </div>
            </div>
            <div id="travel_details" style="display:none">
            <h1>Travel Details</h1>
            <div class="middiv">
            <fieldset class="register">
              <div class="fieldrow">
                <div class="row_left">
                  <label>Pickup Time</label>
                </div>
                <div class="row_right">
                <div class="selecttime">
                  <select class="time">
                  	<option>Hours</option> 
                    <option>1</option> 
                    <option>2</option> 
                    <option>3</option>                   
                  </select>
                  </div>
                  <div class="selecttime">
                  <select class="time">
                  	<option>Min</option>  
                    <option>1</option> 
                    <option>2</option> 
                    <option>3</option>                                      
                  </select> 
                  </div>  
                  <span class="showtime">You Selected: 7:30 pm</span>               
			 </div>
              </div>
              <div class="fieldrow">
                <div class="row_left"><label>Area</label></div>
                <div class="row_right">
                <div class="areap">
                  <select class="area">
                  	<option>Min</option> 
                    <option>1</option> 
                    <option>2</option> 
                    <option>3</option>                  
                  </select>   
                  </div>
                </div>
              </div>
              <div class="fieldrow">
                <div class="row_left">
                  <label>Full Address</label>
                </div>
                <div class="row_right">
                 <textarea></textarea>
                </div>
              </div>
              
            </fieldset>
          
        </div>
      </div>
    </div>
    <div class="rightside">
      <h1>Booking Summary</h1>
      <div class="tpdiv"> <img src="images/car1.jpg" alt="" />
        <div class="clr"></div>
        <p><span>Car Type:</span> Economy - Indica/WagonR</p>
        <p><span>From:</span> Mumbai</p>
        <p><span>Service:</span> Outstation </p>
        <p><span>Destination:</span> Agra, Delhi, Pune</p>
        <p><span>Pickup Date:</span> 17th Apr '12</p>
        <p><span>Pickup Time:</span> Not Set</p>
        <h3>Payable Amount<br />
          <span><img src="images/re1.jpg" alt="" align="absmiddle" /> 3999/-</span></h3>
      </div>
      <div class="tpdiv">
        <p><span>Fare Details</span></p>
        <ul>
          <li>Rate: <img src="images/re2.jpg" alt="" align="absmiddle" /> 1000 / day + <img src="images/re2.jpg" alt="" align="absmiddle" /> 6 / km</li>
          <li>Daily rental charge = <img src="images/re2.jpg" alt="" align="absmiddle" /> 1000x2 </li>
          <li>Approx round trip distance charge = <img src="images/re2.jpg" alt="" align="absmiddle" /> 6 x450</li>
          <li>Toll and parking extra</li>
          <li>Chauffeur charges, state permit and
            service tax included</li>
        </ul>
      </div>
      <div class="tpdiv">
        <p>Final fare shall depend on actual kms traveled. </p>
        <div class="map"><img src="images/map1.jpg" alt="" /></div>
      </div>
    </div>
  </div>
    </div>
    </form>
</div>
</div>
<!--footer Start Here-->
<div id="footer">
	<div class="main">
    	
        <div class="fbtm">
        	<div class="cm">
            	<h3>Company</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/aboutus.php" target="_blank">About</a></li>
			<li><a href="http://www.carzonrent.com/services.php" target="_blank">Services</a></li>
			<li><a href="http://www.carzonrent.com/careers.php" target="_blank">Career</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Book a Cab</h3>
                <ul>
                	<li><a href="http://www.carzonrent.com/business-enquiries.php" target="_blank">Outstation</a></li>
			<li><a href="http://www.carzonrent.com/corporate-rental.php" target="_blank">Local</a></li>
			<li><a href="http://www.easycabs.com/" target="_blank">Easycabs</a></li>
                </ul>
            </div>
            <div class="cm">
            	<h3>Easy Cabs</h3>
                <ul>
                	<li><a href="http://www.easycabs.com/advertisewithus.php" target="_blank">Advertise with Us</a></li>
                        <li><a href="http://www.easycabs.com/fleet.php" target="_blank">Fleet</a></li>
			<li><a href="http://www.easycabs.com/tariff.php" target="_blank">Tariff</a></li>
                </ul>
            </div>
	    <div class="qu">
            	<h3>Corporate Leasing</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/corlease/comparative-analysis.php" target="_blank">Why Leasing</a></li>
			<li><a href="http://www.carzonrent.com/corlease/lease-benefits.php" target="_blank">Benefits</a></li>
			<li><a href="http://www.carzonrent.com/corlease/product-and-services.php" target="_blank">Products & Services</a></li>
                </ul>
            </div>
            <div class="qu">
            	<h3>Quick Links</h3>
                <ul>
			<li><a href="http://www.carzonrent.com/my-account.php" target="_blank">My Account</a></li>
			<li><a href="http://www.carzonrent.com/print-booking.php" target="_blank">Print Booking</a></li>
			<li><a href="http://www.carzonrent.com/vehicle-guide.php">Vehicle Guide</a></li>
			<li><a href="http://www.carzonrent.com/contact-us.php" target="_blank">Contact Us</a></li>
                </ul>
            </div>
	    
            <div class="socil">
            	<h3>Social</h3>
                <div class="soccd">
			<table cellpadding="5" cellspacing="0" border="0">
                        <tr>
                            <td><script src="http://connect.facebook.net/en_US/all.js#xfbml=1" type="text/javascript"></script><fb:like href="http://www.facebook.com/carzonrent/" send="false" layout="button_count" width="90" show_faces="false"></fb:like></td>
                            <td><a href="https://twitter.com/carzonrentIN" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @carzonrentIN</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
                            <td style="padding-left:15px;"><!-- Place this tag where you want the +1 button to render -->
<g:plusone size="medium" href="http://www.carzonrent.com"></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script></td>
                        </tr>
                    </table>
		</div>
                <!--<h3>Download Mobile App</h3>
                <p><img src="images/apps.png" alt="" /></p>-->
            </div>
        </div>
        <div class="copurit">Copyright &copy; Carzonrent. All Rights Reserved. Terms of Use.</div>
    </div>
</div>
</body>
</html>

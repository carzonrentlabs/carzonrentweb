<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>National Bunk Day</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="http://www.carzonrent.com/referralProgram/css/ReferralProgram.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<style>
.mylesResolu{
width:1095px;
margin:20px auto;
padding:10px;
position:relative;
left:-5px;
background-color:#E2E2E2;
border:1px solid #D8D8D8;
}
.clr{clear:both;}
.term_of_use{
width:1130px;
margin:0 auto;
}
#footer{margin-top:40px;}
.w31 img {border:1px solid #efefef;}
.show2, .show3{
cursor:pointer;
margin-top:20px;
}
#pop2.t_condition h3, #pop3.t_condition h3{padding-bottom:5px;}
.t_condition p, ul li{font-size:13px;}
ul li{line-height:18px;}
.t_condition{width:95.5%;}
#pop2.t_condition, #pop3.t_condition{margin-bottom:0px;width:95.9%;}
.tc_bg{
background:#FFFCFC;
padding:20px;
border:1px solid #DCDBDB;
}
</style>
<script>
$(document).ready(function(){
    $(".termsCod").click(function(){
		$("#pop2").slideUp();
		$("#pop3").slideUp();
        $(".t_condition.tc").slideToggle();
    });
	
	$('.show2').click(function(){
		$("#pop3").slideUp();
		$(".t_condition.tc").slideUp();
		$("#pop2").slideDown();
		$('#termsCod').ScrollTo();
    });
  
    $('.show3').click(function(){
		$("#pop2").slideUp();
		$(".t_condition.tc").slideUp();
		$("#pop3").slideDown();
		$('#termsCod').ScrollTo();
    });  
	
});
</script>
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div class="clr"> </div>
<div class="mylesResolu">
<img width="1094" height="304" src="http://www.carzonrent.com/images/NationalBunkDayTNC_cor.jpg" alt="what’s your excuse?" title="what’s your excuse?">
</div>
<div class="term_of_use">
<div class="tc_bg" style="margin-bottom:20px;">
<h3 style="padding-bottom:5px;">Offer Terms and Conditions:</h3>
<ul class="mp0" style="margin:0px 0 0px 20px;">
<li>Use code <strong>MJNBD10</strong> to get <strong>10% off</strong> on hourly, daily, weekly bookings.</li>
<li>Code <strong>MJNBD10</strong> applicable in all cities except Delhi/NCR and Mumbai.</li>
<li>Use code <strong>MJNBD20</strong> to get <strong>20% off</strong> on hourly, daily, weekly bookings.</li>
<li>Code <strong>MJNBD20</strong> applicable only in Delhi/NCR and Mumbai.</li>
<li>Offer valid on bookings made between 16th and 31st January 2016. </li>
<li>Code applicable on picks-up and drops made between 16th January and 31st January 2016.</li>
<li>Offer not applicable in case the booking is cancelled.</li>
<li>The discount amount is applicable on the base fare only.</li>
<li>Standing instructions/ pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Blackout dates apply</li>
<li>Other standard T&amp;C apply.</li>
</ul>
</div>
<div class="tc_bg">
<h3 style="padding-bottom:5px;">#NationalBunkDay Contest T&C </h3>
<ul class="mp0" style="margin:0px 0 0px 20px;">
<li>On Twitter every user needs to follow @MylesCars Every user needs to use #letsbunk & @MylesCars in all the tweets to be eligible for the contest. On Facebook, one must like the <a href="http://facebook.com/mylescars" target="_blank">facebook.com/mylescars</a> page to be eligible to participate in the contest</li>
<li>Final choice of winner lies with company and will be binding on all participants</li>
<li>#NationalBunkDay contest is only for people from India.  Only original entries with #NationalBunkDay will be accepted. #NationalBunkDay contest is open only for residents of India and above the age of 23 years.</li>
<li>Valid only on the self-drive service: Myles.</li>
<li>Valid for all cities and SUV car will be provided to winner as per availability and choice of company. Free dates for SUV must lie between 22nd and 26th Jan 2016</li>
<li>Booking subject to car availability. Prior booking is advised.</li>
<li>Prize from Adventure Nation is only for participants from Delhi and Chandigarh.
<br>
<br>
<strong>A. Inclusions</strong>
<ul style="margin:5px 0 10px 20px;">
<li>2 nights stay in Deluxe tent at Camp Panther Resort, Rishikesh</li>
<li>All meals</li>
<li>One rafting session (weather and other conditions permitting)</li>
<li>All taxes</li>
</ul>
<strong>B. Exclusions</strong>
<ul style="margin:5px 0 10px 20px;">
<li>Air/Train fare</li>
<li>Anything not mentioned under the head inclusions.</li>
<li>Cost incidental to any change in the itinerary / stay on account of flight cancellation due to bad weather, ill health, road blocks and / or any factors beyond control.</li>
<li>Uttarakhand Government Luxury Tax (Rs. 60 per night per tent)</li>
<li>Alcoholic beverages (we do not provide/supply/sell any)</li>
<li> Any Transport/rafting transfers (Transportation is required for transfer to put in point and back from take out point during rafting)</li>
</ul>
<strong>Note:</strong> Booking confirmation is subject to availability. Please contact us for availability on myles@adventurenation.com at least 5 days prior to your intended date of arrival.
<br>
<br>
</li>
<li>Standing instructions/ Pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ul>
</div>



<div class="clr"> </div>
</div>
<div class="clr"> </div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
    if(isset($_POST["btnMakePayment"]))
	{
		session_start();
    //error_reporting(E_ALL);
    //ini_set("display_errors", 1);
	    include_once('./classes/easycabs.ws.class.php');
		include_once('./classes/easycabs.xmlparser.class.php');

		
		$dataToPost = array();

		$timez = new DateTimeZone('Asia/Kolkata');
		$tdt = new DateTime();
		$tdt->setTimezone($timez);

		$day = $_POST["ddlDate"];//pick up day
		$daypart = explode("-", $day);
		$yy = $daypart[0];
		$mm = $daypart[1];
		$dd = $daypart[2];
		
		$hour = $_POST["tHour"];
		$min = $_POST["tMin"];
		$tdt->setDate($yy,$mm,$dd);
		$tdt->setTime($hour,$min,"00");
		
		
		$diff = abs(strtotime($tdt) - strtotime(date('Y-m-d H:i:s')));
		$dm = intval($diff / (365*60*60));
		//echo $dm;
		$dataToPost["keyWord"] = "Web";
		//$dataToPost["CustomerMobile"] = "9582872758";		
		$dataToPost["CustomerMobile"] = $_POST["txtMobile"];
		$dataToPost["GuestName"] = $_POST["txtName"];
		$dataToPost["PickupAdd"] = $_POST["txtAddress"];
		$dataToPost["PickupDateTime"] = $tdt->format('c');
		$dataToPost["PickupSubUrb"] = $_POST["ddlBranch"];
		$dataToPost["PickupLandMark"] = $_POST["hdLocationName"];
		$dataToPost["PickupLatLon"] = $_POST["ddlSubLocation"];
		$dataToPost["DestiAdd"] = $_POST["hdDestinationName"];
		$dataToPost["advanceBookingMinutes"] = 1024;
		 
		$pickupCity = $_POST["ddlBranch"];
		
		if($pickupCity == '1')
			$apiURL = "http://119.226.35.122:8888/Service.asmx";
		if($pickupCity == '2')
			$apiURL = "http://122.182.15.62:8888/Service.asmx";
		if($pickupCity == '3')
			$apiURL = "http://202.177.185.181:8888/Service.asmx";
		if($pickupCity == '4')
			$apiURL = "http://122.169.245.13:8888/Service.asmx";

		$soapClient = new SoapClient($apiURL ."?wsdl", array('trace' => 1));
//		$res = $soapClient->WebBookingApi($dataToPost);
		$res = $soapClient->WebBookingApi($dataToPost);

		$bookingID = $res->{'WebBookingApiResult'};

		$_SESSION["bookingId"] = $bookingID;
		$_SESSION["GuestName"] = $_POST["txtName"];
		$_SESSION["PickupAdd"] =  $_POST["txtAddress"];
		$_SESSION["PickupDateTime"] = $tdt->format('c');
		$_SESSION["PickupSubUrb"] = $_POST["ddlBranch"];
		$_SESSION["PickupLandMark"] = $_POST["hdLocationName"];
		$_SESSION["PickupLatLon"] = $_POST["ddlSubLocation"];
		$_SESSION["DestiAdd"] = $_POST["hdDestinationName"];

		$_SESSION["PickUpDate"] = $_POST["ddlDate"];
		$_SESSION["PickUpTime"] = $_POST["userTime"];
		
		if(stripos($bookingID, "Err:") !== false)
		    $url = "./my-account-trip-details.php?bookid=".$bookingID;
		else
		    $url = "./error-booking.php?e=".$bookingID;
		header('Location: ' . $url);

		//echo "<pre>";
		//print_r($res);
    }
	else
		echo "<pre> Access Denied";
?>
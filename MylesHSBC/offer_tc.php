<?php
	 error_reporting(1);
     //error_reporting(E_ALL);
     //ini_set("display_errors", 1);
     include_once('../classes/cor.ws.class.php');
     include_once('../classes/cor.xmlparser.class.php');
     include_once("../includes/cache-func.php");
     include_once('../classes/cor.mysql.class.php');
    
?>
<!DOCTYPE HTML>
<html>
     <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <title>Cor Mobile Apps terms</title>
	   <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/drivetowin/css/style.css">
	   <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
	  
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/myles-campaign/style.css">
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js?v=<?php echo mktime(); ?>"></script>
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/validation.js?v=<?php echo mktime(); ?>"></script>
	  <link rel="stylesheet" type="text/css" href="css/drivetowin.css">
	  <link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'>
	  <script>
	  document.createElement('article');
	  document.createElement('section');
	  document.createElement('aside');
	  document.createElement('hgroup');
	  document.createElement('nav');
	  document.createElement('header'); 
	  document.createElement('footer');
	  document.createElement('figure');
	  document.createElement('figcaption'); 
	  </script>
	 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/seo-metas.php"); ?>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php");
?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<!-- Slider image start -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/slider/slidesjs.css" type="text/css" media="screen" />
<!-- Slider image end -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>

	  
	 
	  
	  <!-- Tab end -->
	  <!-- OnClick Slider start -->
	  <link href="css/jcarousel.css" rel="stylesheet" type="text/css">
	  <!-- OnClick Slider end -->
	  <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
	  <script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
	  <style type="text/css">
	  body{
	  background:#fff;
	  }
	  </style>
     </head>
     <body>
	 <?php include_once("../includes/header.php"); ?>
	  <div class="clr"> </div>
	  <br>
	   <div class="main">
	
	
	<link href="css/term_condition.css" rel="stylesheet" type="text/css">
<!-- Term & Condition Start -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="css/tab.css">
<!-- Term & Condition End -->
<div class="app_banner">
<div class="app">
<ol class="hsbc">
<li>Log on to <a href="http://www.carzonrent.com/" target="_blank">www.carzonrent.com</a> Click on the service tab for which you want to avail the offer i.e. - Outstation/Local/Self-Drive.</li>
<li>Select the preferred city, date/time, car & the location.</li>
<li>Post filling in the details, enter the discount code in the “Discount/Promotion Code”. After entering the code, click on the Apply Code tab below the Discount/Promotion Code box to avail the discount.</li>
<li>Proceed with the booking & pay the balance amount through credit card/debit card/net banking.</li>
</ol>
<div class="term_heading">
HSBC offer terms and conditions:
</div>

<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab1">Myles Self-Drive  T&C </a>
	</li>
	<li>
		<a href="#tab2">Outstation and Local Upgrade Offer T&C </a>
	</li>
	<li>
		<a href="#tab3">Outstation and Local 15% Discount Offer T&C </a>
	</li>
</ul>

<div id="tab1" class="tab-content active">
<div>
<div class="discount_h"><span>`</span>500 discount</div>
<div class="ptb20_lr40">
<ol>
<li>Code is applicable on a minimum booking of Rs. 2000.</li>
<li>Valid for all Myles Self-Drive cars & cities.</li>
<li>Booking subject to car availability.</li>
<li>Standing instructions / Pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Valid for reservations made till 30th June 2015.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ol>
<div class="clr"> </div>
</div>
</div>
</div>
<div id="tab2" class="tab-content hide">
<div>
<div class="discount_h">Upgrade your ride</div>
<div class="ptb20_lr40">
<ol>
<li>A complimentary upgrade to the next category –Applicable on budget, family and business class segment only.</li>
<li>Valid on Chauffeur Drive service only.</li>
<li>Valid for all available cities.</li>
<li>Booking subject to car availability.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Valid for reservations made till 30th June 2015.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ol>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
</div>
<div id="tab3" class="tab-content hide">
<div>
<div class="discount_h">15% Discount</div>
<div class="ptb20_lr40">
<ol>
<li>15% off on luxury cars i.e: Corolla, Elantra, Cruze, Accord, Camry and Mercedes-C Class.</li>
<li>Valid on Chauffeur drive service only.</li>
<li>Valid for all available cities.</li>
<li>Booking subject to car availability.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>The discount can be redeemed in full & not in parts.</li>
<li>Valid for reservations made till 30th June 2015.</li>
<li>Blackout dates apply.</li>
<li>Standard T&C apply.</li>
</ol>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
</div>




</div>
<div class="clr"> </div>
</div>
        
	

  
   </div>
   </br>
   
</div>
<div class="clr"> </div>
<!--Middle Start Here-->
<?php include_once("middle.php"); ?>


<div class="clr"> </div>
<?php include_once("../includes/footertc.php"); ?>






</body>
</html>
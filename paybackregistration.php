<?php
    if(isset($_POST["name"])){
	include_once('./classes/cor.mysql.class.php');

	$dataToSave = array();
	$db = new MySqlConnection(CONNSTRING);
	$db->open();

	$dataToSave["name"] = $_POST["name"];
	$dataToSave["mobile"] = $_POST["mobile"];
	$dataToSave["dob"] = $_POST["dob"];
	$dataToSave["gender"] = $_POST["gender"];
	$dataToSave["email_id"] = $_POST["email"];
	$dataToSave["city"] = $_POST["city"];
	$dataToSave["pincode"] = $_POST["pin"];
	$dataToSave["source"] = $_POST["source"];
	$dataToSave["entry_date"] = date('Y-m-d H:i:s');
	
	$r = $db->insert("payback_registration", $dataToSave);
	if(array_key_exists("response", $r))
		if($r["response"] == "SUCCESS")
			echo "SUCCESS";
	else
		echo "FAIL";
	unset($r);
	unset($dataToSave);
	$db->close();
    }
?>
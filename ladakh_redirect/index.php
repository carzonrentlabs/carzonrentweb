﻿<?php
//header("location:http://carzonrent.com");
//exit;

     error_reporting(0);
     //error_reporting(E_ALL);
     //ini_set("display_errors", 1);
     include_once('../classes/cor.ws.class.php');
     include_once('../classes/cor.xmlparser.class.php');
     include_once("../includes/cache-func.php");
     include_once('../classes/cor.mysql.class.php');
     $cor = new COR();
     $res = $cor->_CORGetCities();	
     $myXML = new CORXMLList();
     
     $isHrAvail = true;
     
     unset($arr);
     $arr = array();
     $arr["CityID"] = 2;
     $arr["PkgType"] = "Daily";
     $res = $cor->_CORGetPackageDetails_Ladakh($arr);	
     $pkgsDaily = $myXML->xml2ary($res->{"GetPackageDetails_LadakhResult"}->{"any"});
     
?>
<!DOCTYPE HTML>
<html>
     <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <?php include_once("../includes/seo-metas.php"); ?>
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/myles-campaign/style.css">
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js?v=<?php echo mkdir(); ?>"></script>
	  <link rel="stylesheet" type="text/css" href="css/ladakh.css">
	  <link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'>
	  <script>
	  document.createElement('article');
	  document.createElement('section');
	  document.createElement('aside');
	  document.createElement('hgroup');
	  document.createElement('nav');
	  document.createElement('header'); 
	  document.createElement('footer');
	  document.createElement('figure');
	  document.createElement('figcaption'); 
	  </script>
	  <!-- Internet Explorer HTML5 enabling code: -->
	  <!--[if IE]>
	  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	  <style type="text/css">
	  .clear {
	    zoom: 1;
	    display: block;
	  }
	  </style>
	  <![endif]-->
	  <!-- Tab start -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	  <script type="text/javascript">
	       _getMonthInt = function(ms){
		    var m = 0;
		    switch(ms.toLowerCase()){
			case "jan":
			    m = 0;
			break;
			case "feb":
			    m = 1;
			break;
			case "mar":
			    m = 2;
			break;
			case "apr":
			    m = 3;
			break;
			case "may":
			    m = 4;
			break;
			case "jun":
			    m = 5;
			break;
			case "jul":
			    m = 6;
			break;
			case "aug":
			    m = 7;
			break;
			case "sep":
			    m = 8;
			break;
			case "oct":
			    m = 9;
			break;
			case "nov":
			    m = 10;
			break;
			case "dec":
			    m = 11;
			break;
		    }
		    return m;
		};
	       var month=new Array();
	       month[0]="Jan";
	       month[1]="Feb";
	       month[2]="Mar";
	       month[3]="Apr";
	       month[4]="May";
	       month[5]="Jun";
	       month[6]="Jul";
	       month[7]="Aug";
	       month[8]="Sep";
	       month[9]="Oct";
	       month[10]="Nov";
	       month[11]="Dec";
	       
		function _setDropDateMyles(pi, di){
		   pd = document.getElementById(pi).value.trim().split(" ");
		   dd = document.getElementById(di).value.trim().split(" ");
		   var p = new Date();
		       var d = new Date();
		       m = _getMonthInt(pd[1].replace(",",""));
		       var nxtD = eval(parseInt(pd[0]) + 1);
		       p.setFullYear(pd[2], m, pd[0]);
		       d.setFullYear(pd[2], m, nxtD);
		       document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
		       try {
			   $('#inputFieldSF2').datetimepicker({
			   format:'d M, Y H:i',
			   minDate:d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
			   step:30
			   });
		       } catch(e) {
		       }
		       
		       document.getElementById('pickdate').value = (p.getMonth() + 1) + "/" + p.getDate() + "/" + p.getFullYear();
		       if(document.getElementById('dropdate'))
		       document.getElementById('dropdate').value = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
		       var tm = pd[3].split(":");
	       
		       document.getElementById('tHourP').value = tm[0];
		       document.getElementById('tMinP').value = tm[1];
		       
		       if(document.getElementById('dropdate')){
			       document.getElementById('tHourD').value = tm[0];
			       document.getElementById('tMinD').value = tm[1];
		       }
		       var oneDay = 1000 * 60 * 60 * 24;
		    var duration = d.getTime() - p.getTime();
		    document.getElementById('duration').value = parseInt(duration / oneDay);
		    var pgkRate = document.getElementById('PkgRate').value;
		    var vat = document.getElementById('vat').value;
		    var totAmount = parseInt(duration / oneDay) * pgkRate;
		    var vatAmt = parseInt((totAmount * vat) / 100);
		    var totFare = parseInt(totAmount + vatAmt);
		    document.getElementById('ffarepp').innerHTML = "Rs " + pgkRate + " / Day";
		    document.getElementById('minBill').innerHTML = totAmount;
		    document.getElementById('vat').innerHTML = vatAmt;
		    document.getElementById('OriginalAmt').value = document.getElementById('BasicAmt').value = document.getElementById('totAmount').value = totAmount;
		    document.getElementById('totFareS').innerHTML = document.getElementById('totFare').value = totFare;
		    document.getElementById('secDepositSP').innerHTML = document.getElementById('secDeposit').value;
		    
		};
	       
		_setDropOffDate = function(di)
	       {
		    if (document.getElementById('inputFieldSF1').value.trim() != "" && document.getElementById(di).value.trim() != "") {
			 pd = document.getElementById('inputFieldSF1').value.trim().split(" ");
			 dd = document.getElementById(di).value.trim().split(" ");
			 var p = new Date();
			 m = _getMonthInt(pd[1].replace(",",""));
			 p.setFullYear(pd[2], m, pd[0]);
			 var d = new Date();
			 m = _getMonthInt(dd[1].replace(",",""));
			 d.setFullYear(dd[2], m, dd[0]);
			 document.getElementById('dropdate').value = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
			 var tm = dd[3].split(":");
			 document.getElementById('tHourD').value = tm[0];
			 document.getElementById('tMinD').value = tm[1];
			 var oneDay = 1000 * 60 * 60 * 24;
			 var duration = d.getTime() - p.getTime();
			 document.getElementById('duration').value = parseInt(duration / oneDay);
			 var pgkRate = document.getElementById('PkgRate').value;
			 var vat = document.getElementById('vat').value;
			 var totAmount = parseInt(duration / oneDay) * pgkRate;
			 var vatAmt = parseInt((totAmount * vat) / 100);
			 var totFare = parseInt(totAmount + vatAmt);
			 document.getElementById('ffarepp').innerHTML = "Rs " + pgkRate + " / Day";
			 document.getElementById('minBill').innerHTML = totAmount;
			 document.getElementById('vatS').innerHTML = vatAmt;
			 document.getElementById('OriginalAmt').value = document.getElementById('BasicAmt').value = document.getElementById('totAmount').value = totAmount;
			 document.getElementById('totFareS').innerHTML = document.getElementById('totFare').value = totFare;
			 document.getElementById('secDepositSP').innerHTML = document.getElementById('secDeposit').value;
		    } else {
			 var duration = 1;
			 document.getElementById('duration').value = 1;
			 var pgkRate = document.getElementById('PkgRate').value;
			 var vat = document.getElementById('vat').value;
			 var totAmount = parseInt(pgkRate);
			 var vatAmt = parseInt((totAmount * vat) / 100);
			 var totFare = parseInt(eval(totAmount + vatAmt));
			 
			 document.getElementById('ffarepp').innerHTML = "Rs " + pgkRate + " / Day";
			 document.getElementById('minBill').innerHTML = totAmount;
			 document.getElementById('vatS').innerHTML = vatAmt;
			 document.getElementById('OriginalAmt').value = document.getElementById('BasicAmt').value = document.getElementById('totAmount').value = totAmount;
			 document.getElementById('totFareS').innerHTML = document.getElementById('totFare').value = totFare;
			 document.getElementById('secDepositSP').innerHTML = document.getElementById('secDeposit').value;
		    }
	       }
	       var dPkgID = new Array();
	       var dCarImg = new Array();
	       var dCarName = new Array();
	       var dCarDec = new Array();
	       var dPkgRate = new Array();
	       var dSecDeposit = new Array();
	       var dModelID = new Array();
	       var dVat = new Array();
	       <?php
		    if(count($pkgsDaily) > 0) {
			 for($pk = 0; $pk < count($pkgsDaily); $pk++){
	       ?>
			      dPkgID.push('<?php echo $pkgsDaily[$pk]["PkgID"]; ?>');
			      dCarImg.push('<?php echo trim(strtolower($pkgsDaily[$pk]["PkgName"])); ?>');
			      dCarName.push('<?php echo $pkgsDaily[$pk]["PkgName"]; ?>');
			      dCarDec.push('<?php echo $pkgsDaily[$pk]["PkgDesc"]; ?>');
			      dPkgRate.push(<?php echo intval($pkgsDaily[$pk]["PkgRate"]); ?>);
			      dSecDeposit.push('<?php echo intval($pkgsDaily[$pk]["DepositAmt"]); ?>');
			      dModelID.push('<?php echo intval($pkgsDaily[$pk]["CarModelId"]); ?>');
			      dVat.push('<?php echo intval($pkgsDaily[$pk]["VatRate"]); ?>');
	       <?php
			 }
		    }
	       ?>
	       dPlaceName = new Array();
	       dPlaceName.push('THIKSEY MONASTERY');
	       dPlaceName.push('PANPONG LAKE');
	       dPlaceName.push('NUBRA VALLEY');
	       dPlaceName.push('KHARDUNG LA');
	       dPlaceImg = new Array();
	       dPlaceImg.push('place-1.jpg');
	       dPlaceImg.push('place-2.jpg');
	       dPlaceImg.push('place-3.jpg');
	       dPlaceImg.push('place-4.jpg');
	       dPlaceDesc = new Array();
	       dPlaceDesc.push('Thiksey is situated about 20 km far from the town of Leh and ranks among the most important monasteries in Ladakh. It is the seat of Thiksey Rinpoche, the main leader of the Gelug School in Ladakh and is the main and leading monastery for more than ten other famous Ladakhi monasteries. Thiksey monastery is a few miles away from the River Indus on a sacred hill above a village Thiksey.');
	       dPlaceDesc.push('Positioned 13,900 ft above sea level is the Pangong Tso lake, which literally translates to \'long, narrow, enchanted lake\'. A five hour drive from the capital Leh, through the stunning countryside people come from all across the globe to see. Pangong Tso\'s bright pristine blue water and a rocky shoreline will remain in your memories forever.');
	       dPlaceDesc.push('Nubra Valley is located 150 km north of Leh is a large valley that separates Ladakh and Karakoram Ranges. The Karakoram Pass of the famous silk route lie to the northwest of the valley and connect Nubra with Xinjiang Providence of China. From snow capped mountains to sand dunes and double humped camel rides, the Nubra Valley is a must see.');
	       dPlaceDesc.push('The Khardung La pass or the "Pass of Lower Castle" is situated 18,380 ft above sea level and lies on the road between Nubra valley and Leh in Ladakh. Magnificent views of white snow capped mountain peaks surround you and feel like they are almost at arms length. The excitement of visiting the highest pass in the world is sure to strike up some emotions.');
	       
	       
	       dEatName = new Array();
	       dEatName.push('BON APPETIT');
	       dEatName.push('CHOPSTICK');
	       dEatName.push('PENGUIN GARDEN');
	       dEatName.push('GESMO');
	       dEatImg = new Array();
	       dEatImg.push('eat-1.jpg');
	       dEatImg.push('eat-2.jpg');
	       dEatImg.push('eat-3.jpg');
	       dEatImg.push('eat-4.jpg');
	       dEatDesc = new Array();
	       dEatDesc.push('Hidden down unlikely footpaths, Leh\'s most imaginative restaurant is a stylish exercise in Ladakhi minimalist architecture and offers a wide panorama of the southern mountains. A limited but thoroughly scrumptious selection of artistically prepared dishes includes sublime cashew chicken in pesto sauce along with succulent tandoori grills.');
	       dEatDesc.push('This stylish pan-Asian restaurant knocks spots off most of the competition in tems of both style and flavour. Their \'Wonderwok\' stir-fries, momos and Thai green curry are all excellent and prices are very fair, given the high quality of service.');
	       dEatDesc.push('Sit beneath the apricot trees and listen to the gushing stream at this slightly hidden but constantly popular garden restaurant. If the fresh-cooked tandoori chicken has run out (typical by early evening) there is still a world of cuisines to explore. Manali trout is occasionally available for INR400 if you dare. Snappy if casual service.');
	       dEatDesc.push('This age-old traveller haunt has been unchanged so long that its bag-lamps and chequerboard ceilings now seem almost like novel retro-design features.Good-value meals range from curries to cakes to yak-cheese pizza.');

//Read more: http://www.lonelyplanet.com/india/jammu-and-kashmir/leh/restaurants/other/bon-appetit#ixzz36rVq5Mgt');
//Read more: http://www.lonelyplanet.com/india/jammu-and-kashmir/leh/restaurants/other/penguin-garden#ixzz36rWAvIKC
		    
	       _setCar = function(idx){
		    <?php
			 for($pk = 0; $pk < count($pkgsDaily); $pk++){
		    ?>
			 document.getElementById('cars-<?php echo $pk; ?>').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>0.png';
		    <?php
			 }
		    ?>
		    document.getElementById('carname').innerHTML = document.getElementById('pkgname').innerHTML = dCarName[idx];
		    document.getElementById('cars-' + idx).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '1.png';
		    document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '.png';
		    document.getElementById('carbrandcost').innerHTML = dPkgRate[idx];
		    document.getElementById('carbrandline').innerHTML = dCarDec[idx];
		    document.getElementById('PkgRate').value = dPkgRate[idx];
		    document.getElementById('secDeposit').value = dSecDeposit[idx];
		    //document.getElementById('hdCarModel').value = dCarName[idx];
		    document.getElementById('vat').value = dVat[idx];
		    
		    _setDropOffDate('inputFieldSF2');
	       }
	       _setPlace = function(idx){		    
		    document.getElementById('placename').innerHTML = dPlaceName[idx - 1];
		    document.getElementById('placeimg').src = '<?php echo corWebRoot; ?>/ladakh/images/' + dPlaceImg[idx - 1];
		    document.getElementById('pldesc').innerHTML = dPlaceDesc[idx - 1];
	       }
	       _setEat = function(idx){
		    document.getElementById('eatname').innerHTML = dEatName[idx - 1];
		    document.getElementById('eatimg').src = '<?php echo corWebRoot; ?>/ladakh/images/' + dEatImg[idx - 1];
		    document.getElementById('eatdesc').innerHTML = dEatDesc[idx - 1];
	       }
	       _setTab = function(tbID){
		    for(i = 1; i <= 3; i++){
			 document.getElementById('tab' + i).style.display = 'none';
			 document.getElementById('tabA' + i).className = '';
		    }
		    document.getElementById('tab' + tbID).style.display = 'block';
		    document.getElementById('tabA' + tbID).className = 'active';
		    jQuery('#mycarousel-' + tbID).jcarousel({
		    start: 1
		    });
	       }
	       _validate = function(){
		    var chk = false;
		    chk = isFilledText(document.getElementById("inputFieldSF1"), "", "Pickup date can't be left blank.");
		    if(chk == true)
			    chk = isFilledText(document.getElementById("inputFieldSF2"), "", "Drop date can't be left blank.");
		    if(chk){
			    pd = document.getElementById("inputFieldSF1").value.trim().split(" ");
			    if(pd.length < 3){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[2].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[1].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[0].length < 1 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
		    }
		    if(chk && document.getElementById("inputFieldSF2")){
			    dd = document.getElementById("inputFieldSF2").value.trim().split(" ");
			    if(dd.length < 3){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[2].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[1].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[0].length < 1 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
		    }
		    if (chk)
		    document.getElementById('formBook').submit();
	       };
	  </script>
	  <!-- Tab end -->
	  <!-- OnClick Slider start -->
	  <link href="css/jcarousel.css" rel="stylesheet" type="text/css">
	  <!-- OnClick Slider end -->
	  <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
	  <script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
     </head>
     <body>
	  <?php include_once("../includes/header.php"); ?>
	  <div class="clr"> </div>
	  <section class="main">
	       <div class="center">
		    <div class="sliderMain">
			 <div id="slider" class="nivoSlider" style="float:left !important;height:279px;width:1080px;">
			      <img src="<?php echo corWebRoot; ?>/ladakh/images/ladakh01.jpg" alt="" title="" />
			      <img src="<?php echo corWebRoot; ?>/ladakh/images/ladakh02.jpg" alt="" title="" />
			      <img src="<?php echo corWebRoot; ?>/ladakh/images/ladakh03.jpg" alt="" title="" />
			 </div>
		    </div>
	       </div>
		   <div class="f_l w61">
	       <ul class='tabs'>
		    <li><a href='javascript: void(0);' id="tabA1" class="active" onclick="javascript: _setTab('1');">CARS FOR LADAKH</a></li>
		    <li><a href='javascript: void(0);' id="tabA2" onclick="javascript: _setTab('2');">MUST SEE IN LADAKH</a></li>
		    <li><a href='javascript: void(0);' id="tabA3" onclick="javascript: _setTab('3');">EAT IN LEH</a></li>
		    <!-- <li><a href='javascript: void(0);' id="tabA4" onclick="javascript: _setTab('4');">Watch Video</a></li> -->
	       </ul>
	  <div id='tab1' style="display:block;">
	       <div class="sl-main">
	       <!-- Content -->
		    <div class="tab1_bg">
			 <div class="sl-content">
			      <div class="sl-slide visible">
				   <div class="w40 f_l">
					<div class="img_design"><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[0]["PkgName"]); ?>.png" id="carbrandimage" alt="" title=""/></div>
				   </div>
				   <div class="w57 pr3 f_l pt19 greyTxt">
					<div class="sl-title" id="carname"><?php echo $pkgsDaily[0]["PkgName"]; ?></div>
					<div id="carbrandline"><?php echo $pkgsDaily[0]["PkgDesc"]; ?></div>
					<div class="h10"> </div>
					<div class="w48 f_l pr2">
					     <input type="text" class="from picktime" id="inputFieldSF1" onchange="javascript: _setDropDateMyles('inputFieldSF1', 'inputFieldSF2');" placeholder="Pick Up Date and Time" readonly="readonly" />
					</div>
					<div class="w50 f_l">
					     <input type="text" class="from picktime" id="inputFieldSF2" placeholder="Drop Off Date and Time" readonly="readonly" onchange="javascript: _setDropOffDate(this.id);" />
					</div>
				   </div>
				   <div class="f_l">
				   <button type="button" class="select button selectN" onclick="javascript: _validate();">Select</button>
				   </div>
				   <div class="fDetail f_r">
				   <div class="greyTxt">
					<a href="javascript: void(0);" onclick="javascript: document.getElementById('popupbox').style.display = 'block';" class="fareD button">Fare details</a>
					<div class="xuv"><span id="pkgname"><?php echo $pkgsDaily[0]["PkgName"]; ?></span><br>
					     <img class="rs1" src="images/rs1.png"/> <span id="carbrandcost"><?php echo intval($pkgsDaily[0]["PkgRate"]); ?></span>/Day
					     <!--Seats: 5-->
					</div>
					<div class="details_wrapper" id="popupbox">
					     <div class="toolarrow"></div>
					     <div class="heading">Fare Details <span class="closedpop" onclick="javascript: document.getElementById('popupbox').style.display = 'none';">X</span></div>
						  <p><span>Rate</span></p>
						  <p><big><span id="ffarepp">Rs <?php echo intval($pkgsDaily[0]["PkgRate"]); ?> / Day</span></big></p>
						  <p><span>Fare Details</span></p>
						  <?php $vat = 12.5; ?>
						  <ul>
						     <li>Minimum Billing: Rs <span id="minBill"><?php echo intval($pkgsDaily[0]["PkgRate"]); ?></span>/-</li>
						     <li>VAT (@<?php echo number_format($vat, 2); ?>%): Rs <span id="vatS"><?php echo intval(ceil(($pkgsDaily[0]["PkgRate"] * $vat) / 100)); ?></span>/-</li>
						     <li>Total Fare: Rs <span id="totFareS"><?php echo intval(ceil($pkgsDaily[0]["PkgRate"] + ($pkgsDaily[0]["PkgRate"] * $vat) / 100)); ?></span>/-</li>
						     <li>Refundable Security Deposit (Pre Auth from card): Rs <span id="secDepositSP"><?php echo $pkgsDaily[0]["DepositAmt"]; ?></span> (Mastercard and Visa only)</li>
						  </ul>
						  <p><span>Mandatory Documents</span></p>
						  <ul>
						       <li>Passport / Voter ID card</li>
						       <li>Driving License</li>
						       <li>Credit Card</li>
						  </ul>
					     </div>
					</div>
				   </div>
					<form id="formBook" name="formBook" method="GET" action="../search-result.php">
					<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
					<input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" />
					<input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo date('h:i A');?>" />
					<input type="hidden" name="userTime" id="userTimeSF" value="" />
					<input type="hidden" name="onlyE2C" id="onlyE2C" value="0" />
					<input type="hidden" name="chkPkgType" id="chkPkgType" value="Daily" />
					<input type="hidden" name="customPkg" id="customPkg" value="1" />
					<input type="hidden" name="tHourP" id="tHourP" value="" />
					<input type="hidden" name="tMinP" id="tMinP" value="" />
					<input type="hidden" name="tHourD" id="tHourD" value="" />
					<input type="hidden" name="tMinD" id="tMinD" value="" />
					<input type="hidden" name="pickdate" id="pickdate" value="" />
					<input type="hidden" name="dropdate" id="dropdate" value="" />
					
					<input type="hidden" name="hdOriginName" id="hdOriginName" value="Delhi" />
					<input type="hidden" name="hdOriginID" id="hdOriginID" value="2"/>
					<input type="hidden" name="hdDestinationName" id="hdDestinationName" value="Delhi"/>
					<input type="hidden" name="hdDestinationID" id="hdDestinationID" value="2"/>
					<input type="hidden" name="hdPkgID" id="hdPkgID" value="<?php echo $pkgsDaily[0]["PkgID"]; ?>" />
					<input type="hidden" name="PkgRate" id="PkgRate" value="<?php echo $pkgsDaily[0]["PkgRate"]; ?>" />
					<input type="hidden" name="duration" id="duration" value="1" />
					<input type="hidden" name="totAmount" id="totAmount" value="<?php echo $pkgsDaily[0]["PkgRate"]; ?>" />
					<input type="hidden" name="totFare" id="totFare" value="<?php echo $pkgsDaily[0]["PkgRate"]; ?>" />
					<input type="hidden" name="vat" id="vat" value="<?php echo $pkgsDaily[0]["VatRate"]; ?>" />
					<input type="hidden" name="secDeposit" id="secDeposit" value="<?php echo $pkgsDaily[0]["DepositAmt"]; ?>" />
					<input type="hidden" name="OriginalAmt" id="OriginalAmt" value="<?php echo $pkgsDaily[0]["PkgRate"]; ?>" />
					<input type="hidden" name="BasicAmt" id="BasicAmt" value="<?php echo $pkgsDaily[0]["PkgRate"]; ?>" />
					</form>					
				   </div>
			      </div>
			 </div>
		    </div>
		    <div class="scrolling_thumb">
			 <?php
			 
			 
			      if(count($pkgsDaily) > 0){
			 ?>
			 <ul id="mycarousel-1" class="jcarousel-skin-tango">
			 <?php
				 for($pk = 0; $pk < count($pkgsDaily); $pk++){
					if($pk == 0){
			 ?>
					<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>1.png" id="cars-<?php echo $pk; ?>" onclick="javascript: _setCar(<?php echo $pk;?>);" width="85" height="63"/></li>
			 <?php
					} else {
			 ?>
					<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>0.png" id="cars-<?php echo $pk; ?>" onclick="javascript: _setCar(<?php echo $pk; ?>);" width="85" height="63"/></li>
			 <?php
					}
				   }
			 ?>
			 </ul>
			 <?php
			      }
			 ?>
		    </div>
	       </div>
	  <div id='tab2' style="display: none;">
	       <div class="sl-main">
		    <div class="tab2_bg">
			 <div class="sl-content">
			      <div class="sl-slide visible">
				   <div class="w49 f_l">
					<div class="img_place"><img src="images/place-1.jpg" id="placeimg" width="300" height="200"></div>
				   </div>
				   <div class="w48 pr3 f_l pt17 greyTxt">
					<div class="sl-title" id="placename">THIKSEY MONASTERY</div>
					<span id="pldesc">Thiksey is situated about 20 km far from the town of Leh and ranks among the most important monasteries in Ladakh. It is the seat of Thiksey Rinpoche, the main leader of the Gelug School in Ladakh and is the main and leading monastery for more than ten other famous Ladakhi monasteries. Thiksey monastery is a few miles away from the River Indus on a sacred hill above a village Thiksey.</span>
					<div class="h20"> </div>
				   </div>
			      </div>
			 </div>
		    </div>
		    <div style="float:left;width:100%;margin:25px 0px 20px 0px">
			 <ul id="mycarousel-2" class="jcarousel-skin-tango">
			 <?php
			      for($pk = 1; $pk <= 4; $pk++){
			 ?>
			      <li><img src="<?php echo corWebRoot; ?>/ladakh/images/place-s-<?php echo $pk; ?>.jpg" id="places-<?php echo $pk; ?>" onclick="javascript: _setPlace(<?php echo $pk; ?>);" width="85" height="63"/></li>
			 <?php
			      }
			 ?>
			 </ul>
		    </div>
	       </div>
	  </div>     
	  <div id='tab3' style="display: none;">
	       <div class="sl-main">
		    <div class="tab2_bg">
			 <div class="sl-content">
			      <div class="sl-slide visible">
				    <div class="w49 f_l">
					<div class="img_place"><img src="images/eat-1.jpg" id="eatimg" width="300" height="200"></div>
				   </div>
				   <div class="w48 pr3 f_l pt17 greyTxt">
					<div class="sl-title" id="eatname">BON APPETIT</div>
					<span id="eatdesc">Hidden down unlikely footpaths, Leh's most imaginative restaurant is a stylish exercise in Ladakhi minimalist architecture and offers a wide panorama of the southern mountains. A limited but thoroughly scrumptious selection of artistically prepared dishes includes sublime cashew chicken in pesto sauce along with succulent tandoori grills.</span>
					<div class="h20"> </div>
				   </div>
			      </div>
			 </div>
		    </div>
		    <div style="float:left;width:100%;margin:25px 0px 20px 0px">
			 <ul id="mycarousel-3" class="jcarousel-skin-tango">
			 <?php
			      for($pk = 1; $pk <= 4; $pk++){
			 ?>
			      <li><img src="<?php echo corWebRoot; ?>/ladakh/images/eat-s-<?php echo $pk; ?>.jpg" id="eat-<?php echo $pk; ?>" onclick="javascript: _setEat(<?php echo $pk; ?>);" width="85" height="63"/></li>
			 <?php
			      }
			 ?>
			 </ul>
		    </div>
	       </div>	   
	  </div>
	  </div>
	  <div class="f_l w38 tac">
	  <div class="watch_video">Explore Ladakh in a Myles car</div>
	  <iframe width="398" height="224" src="https://www.youtube.com/embed/lZBMKqgBth0" frameborder="0" allowfullscreen></iframe>
	  <!-- <a href="https://youtu.be/lZBMKqgBth0" target="_blank"><img src="http://www.carzonrent.com/ladakh/images/video.jpg" alt="Watch Video" title="Watch Video"></a>
	  -->
	  </div>
	  <!--
	  <div id='tab4' style="display: none;">
	  <div class="sl-main">
	  <a href="https://youtu.be/lZBMKqgBth0" target="_blank"><img src="<?php echo corWebRoot; ?>/ladakh/images/video.jpg" alt="Watch Video" title="Watch Video" /></a>
	  </div>
	  </div>
	  -->
     </section>
	  <div class="clr"> </div>
     <footer>
	  <div class="main">
	  <div class="f_l w50">
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/aboutus.php">About Us</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/services.php">Our Service</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
		    <!--<li><a href="#">Media</a></li>-->
		    <li><a href="http://careers.carzonrent.com/">Careers</a></li>
	       </ul>
	  </div>
	  <div class="f_r w50 t_a_r">
	       <a href="https://www.facebook.com/carzonrent" target="_blank"><img src="images/facebook.png" border="0" class="mr2"/></a>
	       <a href="https://plus.google.com/+carzonrent" target="_blank"><img src="images/google+.png" border="0" class="mr2"/></a>
	       <a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="images/twitter.png" border="0"/></a>
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/login.php">Account Summary</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
	       </ul>
	       <div class="copyright">
		    Copyright © 2014 Carzonrent India Pvt Ltd. All Rights Reserved.
	       </div>
	  </div>
	  <div class="clr"> </div>
     </div>
     </footer>
     <!-- slider start --> 	
     <script type='text/javascript' src='js/slider/jquery.min.js'></script>
     <script type='text/javascript' src='js/slider/jquery.mobile.customized.min.js'></script>
     <script type='text/javascript' src='js/slider/jquery.easing.1.3.js'></script> 
     
     <!-- slider end --> 
     <!-- date picker start --> 
     <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
     <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
     <script type="text/javascript" src="js/jquery.ui.core.js"></script>
     <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
     <script type="text/javascript">
     var j=jQuery.noConflict();
	     j('#inputFieldSF1').datetimepicker({
	     format:'d M, Y H:i',
	     minDate:'d M, Y',
	     step:30,
	     roundTime:'ceil'
	     });
	     j('#inputFieldSF2').datetimepicker({
	     format:'d M, Y H:i',
	     minDate:'d M, Y',
	     step:30
	     });
     </script>
     <!-- date picker end -->
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/scripts/jquery-1.4.3.min.js"></script>
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/jquery.nivo.slider.pack.js"></script>
     <script type="text/javascript">
     $(window).load(function() {
	 $('#slider').nivoSlider();
     });
     </script>
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.jcarousel.min.js"></script>
     <script type="text/javascript">
	  jQuery(document).ready(function() {
	  
	       jQuery('#mycarousel-1').jcarousel({
		    start: 1
	       });
	  
	//       jQuery('#mycarousel-2').jcarousel({
	//	   start: 1
	//       });
	//       
	//       jQuery('#mycarousel-3').jcarousel({
	//	   start: 1
	//       });
	  });
     </script> 
	 
	 <script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-22195130-1', 'auto');
  ga('send', 'pageview');

		</script>
		
		<!-- Google Code for Remarketing tag -->
		<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide:google.com/ads/remarketingsetup -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 957885192;
		var google_conversion_label = "FhZ6COil-gUQiNbgyAM";
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957885192/?value=0&amp;label=FhZ6COil-gUQiNbgyAM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>



	<div id='invtrflfloatbtn'></div>
	<script type="text/javascript">	
	//<![CDATA[
	var invite_referrals = window.invite_referrals || {}; (function() {
	invite_referrals.auth = { bid_e : '1B3596545F3AF427193DA8B36E523D4F', bid : '2926', t : '420', orderID : '', email : '' };
	var script = document.createElement('script');script.async = true;
	script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
	var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
	  //]]>
	</script>





	 
     </body>
</html>
<!DOCTYPE html>
<?php 
include("includes/dbconnect.php");
error_reporting(0);
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$useragent=$_SERVER['HTTP_USER_AGENT'];  
if(preg_match('/android|avantgo|samsung|HTC|nokia|iphone|Opera Mini|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))  
header('Location: http://m.carzonrent.com/');  
?> 
<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	//$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	//$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrent India Help Center</title>
<meta name="description" content="Carzonrent India help center.">
<meta name="keywords" content="Carzonrent, Help Center">
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<script type="text/javascript">
	$(function(){
		$('select.styled').customSelect();
		$('#slidesLocal').slides({
			preload: false,
			preloadImage: '/images/loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesOutstation').slides({
			preload: false,
			preloadImage: '/images/loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesSelfDrive').slides({
			preload: false,
			preloadImage: '/images/loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
	});

	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
	arrDestination = new Array();
<?php
	for($i = 0; $i < count($des); $i++){
?>
		arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
</script>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
function _setSFcss() {
    var pg = document.getElementsByClassName('pagination');
    for(p = 0; p < pg.length; p++)
    pg[p].className = 'paginationR';
    
    var pg = document.getElementsByClassName('showtime');
    for(p = 0; p < pg.length; p++){
	pg[p].style.backgroundColor = '#fff';
	pg[p].style.color = '#db4626';
	pg[p].style.borderColor = '#db4626';
    }
    var pg = document.getElementsByClassName('showhour');
    for(p = 0; p < pg.length; p++){
	pg[p].style.backgroundColor = '#fff';
	pg[p].style.color = '#db4626';
	pg[p].style.borderColor = '#db4626';
    }
    
    if (document.getElementById('feedbackpopup'))
    document.getElementById('feedbackpopup').style.backgroundImage = 'url(./images/feedback-r.jpg)';
    if (document.getElementById('feedbacksubmit'))
    document.getElementById('feedbacksubmit').style.backgroundImage = 'url(./images/submit.png)';
    if (document.getElementById('registration'))
    document.getElementById('registration').style.borderColor = '#db4626';
    
    document.getElementById('outstation').style.backgroundColor = '#db4626';
    document.getElementById('local').style.backgroundColor = '#db4626';
    document.getElementById('easycabs').style.backgroundColor = '#db4626';
    document.getElementById('selfdrive').style.backgroundColor = '#fff';
    document.getElementById('international').style.backgroundColor = '#db4626';
    document.getElementById('outstation').style.color = '#fff';
    document.getElementById('local').style.color = '#fff';
    document.getElementById('easycabs').style.color = '#fff';
    document.getElementById('selfdrive').style.color = '#000';
    document.getElementById('international').style.color = '#fff';
};
function _setALLcss(sDiv) {
    var pg = document.getElementsByClassName('paginationR');
    for(p = 0; p < pg.length; p++)
    pg[p].className = 'pagination';
    
    var pg = document.getElementsByClassName('showtime');
    for(p = 0; p < pg.length; p++){
	pg[p].style.backgroundColor = '#FAEAA3';
	pg[p].style.color = '#D68300';
	pg[p].style.borderColor = '#EFC14D';
    }
    var pg = document.getElementsByClassName('showhour');
    for(p = 0; p < pg.length; p++){
	pg[p].style.backgroundColor = '#FAEAA3';
	pg[p].style.color = '#D68300';
	pg[p].style.borderColor = '#EFC14D';
    }
    
    if (document.getElementById('feedbackpopup'))
    document.getElementById('feedbackpopup').style.backgroundImage = 'url(./images/feedback.png)';
    if (document.getElementById('feedbacksubmit'))
    document.getElementById('feedbacksubmit').style.backgroundImage = 'url(./images/submit.jpg)';
    if (document.getElementById('registration'))
    document.getElementById('registration').style.borderColor = '#F2C900';
    
    document.getElementById('outstation').style.backgroundColor = '#F2C900';
    document.getElementById('local').style.backgroundColor = '#F2C900';
    document.getElementById('easycabs').style.backgroundColor = '#F2C900';
    document.getElementById('selfdrive').style.backgroundColor = '#F2C900';
    document.getElementById('international').style.backgroundColor = '#F2C900';
    document.getElementById(sDiv).style.backgroundColor = '#fff';
    document.getElementById('outstation').style.color = '#615000';
    document.getElementById('local').style.color = '#615000';
    document.getElementById('easycabs').style.color = '#615000';
    document.getElementById('selfdrive').style.color = '#615000';
    document.getElementById('international').style.color = '#615000';
};
$(function() {
	_setSFcss();
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
<script type="text/javascript">
function _setActive(tab)
{
    if (document.getElementById('brand'))
    document.getElementById('brand').className = 'corbrand';
    if (document.getElementById('TripleTabs'))
    document.getElementById('TripleTabs').className = '';

    if (document.getElementById('outstation'))
    document.getElementById('outstation').className = '';
    if (document.getElementById('local'))
    document.getElementById('local').className = '';
    if (document.getElementById('easycabs'))
    document.getElementById('easycabs').className = '';
    //if (document.getElementById('international'))
    //document.getElementById('international').className = '';
    if (document.getElementById('selfdrive'))
    document.getElementById('selfdrive').className = '';
    
    if (document.getElementById('outstationDiv'))
    document.getElementById('outstationDiv').style.display = 'none';
    if (document.getElementById('localDiv'))
    document.getElementById('localDiv').style.display = 'none';
    if (document.getElementById('internationalDiv'))
    document.getElementById('internationalDiv').style.display = 'none';
    if (document.getElementById('selfdriveDiv'))
    document.getElementById('selfdriveDiv').style.display = 'none';
    
    var tabName = tab;
    document.getElementById(tab).className = 'activeTab';
    document.getElementById(tab + 'Div').style.display = 'block';
    if (tab == "local") {
	if (document.getElementById('slidesLocal')) {
	    document.getElementById('slidesOutstation').style.display = 'none';
	    document.getElementById('slidesSelfDrive').style.display = 'none';
	    document.getElementById('slidesLocal').style.display = 'block';
	    _setALLcss("local");
	}
    } else if (tab == "outstation") {
	if(document.getElementById('slidesOutstation')) {
	    document.getElementById('slidesLocal').style.display = 'none';
	    document.getElementById('slidesSelfDrive').style.display = 'none';
	    document.getElementById('slidesOutstation').style.display = 'block';
	    _setALLcss("outstation");
	}
    } else if (tab == "selfdrive") {
	if(document.getElementById('slidesSelfDrive')) {
	    document.getElementById('slidesLocal').style.display = 'none';
	    document.getElementById('slidesOutstation').style.display = 'none';
	    document.getElementById('slidesSelfDrive').style.display = 'block';
	    _setSFcss();
	}
    }
};
</script>
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js"></script>
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>	

<!-- new code end -->

	
		
<?php include_once("./includes/header.php"); ?>
		
		
		<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/js/jquery.multilevelpushmenu.css">
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/covermode.css">
        <script type="text/javascript" src="http://oss.maxcdn.com/libs/modernizr/2.6.2/modernizr.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<script>
function dataVal(id)
{

var dataString = 'id='+ id;
$.ajax({type:"POST", url: "<?php echo corWebRoot; ?>/ajaxdata.php",
data:dataString, success: function(response) {
var txt=response;
document.getElementById("questionval").innerHTML="";
document.getElementById("question").innerHTML=txt;
}});
}
</script>
<script>
function ajaxprocess(id)
{

var dataString = 'id='+ id;
$.ajax({type:"POST", url: "<?php echo corWebRoot; ?>/ajaxdataquestion.php",
data:dataString, success: function(response) {
var txt=response;

document.getElementById("questionval").innerHTML=txt;
}});
}

function getanswer(questionid)
{
var dataString = 'questionid='+ questionid;
$.ajax({type:"POST", url: "<?php echo corWebRoot; ?>/ajaxdataanswer.php",
data:dataString, success: function(response) {
var txt=response;
var res = txt.split("-"); 
var iddata="#xy_"+res[0];
var valtext=res[1];
if(txt)
{
$(iddata).html(valtext);

}

}});

}


</script>
		
	<style>
	
	
</style>
<div class="wrapper">
<div class="middle_width">
<div id="pushobj" class="helpC">
   <div id="questionval" class="questiontxt"><span class="textVal"><h1>Carzonrent India Help Center<h1></center>
   
    <div class="helpcenterboder"><img src="<?php echo corWebRoot; ?>/images/HelpCenter_banner.jpg">
	<p>
	 MUMBAI: Mercedes-Benz and car rental company Carzonrent today announced the country's first luxury self- drive concept under the brand of Myles.

The association will offer C-Class & E-Class models of Merc in cities like New Delhi, Mumbai, Bangalore, Hyderabad and Chennai, and will also introduce other models into the luxury self-drive service such as the SLK and AMG versions, the companies said in a statement today.
	</p>
	<p>
	Myles self drive service can be availed from any of the 20 locations in the city (30 in Delhi). Customers can also book a car Online or through the 24x7 reservation desk.

The cost of Myles self drive service varies from car to car. While Suzuki Swift VDI is available for Rs 150 per hour, including fuel, a Toyota Innova will cost Rs 399 per hour. Some of the other cars available and their prices are as follows: Toyota Etios GD - Rs 299/h; Volkswagen Polo - Rs 269/h; Volkswagen Vento - Rs 329/h; Maruti Ertiga - 319/h; Nissan Sunny - Rs 329/h
</p>
	</div>
   
   </div>
</div>
        <div id="menu" class="mainmenu">
            <nav class="menuheight" style="min-width:200px;">
                
				<?php

				$selectQuery=mysql_query("select * from  category where level='0'");
				$num_rows=mysql_num_rows($selectQuery);
				
				?>
				
				<h2><i  style="color:#FFF;"></i><span class="maincat">All Categories</span></h2>
                <ul>
                    <li style="width:195px;">
					<?php 
						if($num_rows>0)
						{
							while($rec=mysql_fetch_array($selectQuery))
							{
							$catid=$rec['categoryid'];
						
							

								?>
								<a href="javascript:void(0);" onclick="return dataVal(<?php echo $rec['categoryid'] ?>)"><?php echo $rec['categoryname'];?></a>
								<?php
								
						}
						
						}
						?>
						<h2><i style="color:#FFF;"></i><span class="maincat">Sub Category</span></h2>
                        <ul>
							<div id='question'></div>
                        </ul>
					</li>
                    
                </ul>
				
            </nav>
        </div>
        </div>		
		<div class="clr"> </div>
		</div>
		<?php include_once("./includes/footer.php"); ?>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="<?php echo corWebRoot; ?>/js/jquery.multilevelpushmenu.min.js"></script>
        <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/covermode.js"></script>
    </body>
	
</html>

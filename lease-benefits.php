<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder">
		<div class="magindiv" style="padding:7px 0px 7px 0px;">
			<h2>Corporate Leasing - Benefits</h2>
		</div>
	</div>	
	<div style="margin:0 auto;width:968px;">
			<div class="divtabtxt">
				<h3 style="margin:30px 0px 20px 30px;">A Comparative Analysis</h3>
				<table border="1" bordercolor="#666666" cellpadding="5" style="float:left;text-align:left;border-collapse:collapse;margin:0px 0px 30px 30px;color:#494949;" width="620px" height="520px">
					<tr class="leasintable" >
						<td width="200px">Particulars</td>
						<td>Finance Lease</td>
						<td>Operating Lease</td>
					</tr>
					<tr style="background-color:#f5f4f4;">
						<td>Legal Owner of the Asset</td>
						<td>Leasing Company</td>
						<td>Leasing Company</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>Registered Owner of the Asset</td>
						<td>Lessee/Lessor</td>
						<td>Lessor</td>
					</tr>
					<tr>
						<td>Risks and Rewards Related to asset</td>
						<td>With Lessee</td>
						<td>With Lessor</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>PV of Future Lease Rental</td>
						<td>90% of the Asset Value</td>
						<td>90% of the Asset Value</td>
					</tr>
					<tr>
						<td>Residual Value</td>
						<td>10% of the Asset Value (PV)</td>
						<td>10% of the Asset Value)</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>Ownership Transfer</td>
						<td>Lessee/Lessor</td>
						<td>Lessor</td>
					</tr>
					<tr>
						<td>Capitalization (Lessee)</td>
						<td>Asset would be capitalized on the B/S</td>
						<td>Asset would not be capitalized on the B/S</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>Tax Benefits</td>
						<td colspan="2">The annual lease rental that you pay can be expensed out in the Profit & Loss account which would qualify for Tax benefits as per the prevailing laws of the Income Tax</td>
					</tr>
					<tr>
						<td>Depreciation as per company Act</td>
						<td>Lessee can claim</td>
						<td>Lessee cannot claim</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>Depreciation as per Income Tax Act</td>
						<td>Lessee cannot claim benefit</td>
						<td>Lessee cannot claim benefit</td>
					</tr>
					<tr>
						<td>Payments</td>
						<td>Lease Rentals are higher</td>
						<td>Lease Rentals are lower as compared to FL</td>
					</tr>
					<tr style="background-color:#e7e6e6;">
						<td>Insurance and Maintenance</td>
						<td>Optional</td>
						<td>Has to be part of the transaction</td>
					</tr>
					<tr>
						<td>Other Fleet Management Services</td>
						<td>Optional</td>
						<td>Optional</td>
					</tr>
				</table>
			</div>
			<div class="imagediv imghand">
				<img src="./images/leasingcar.png" border="0" width="228px" height="298px"/>
			</div>
	</div>	
</div>
<script type="text/javascript">
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

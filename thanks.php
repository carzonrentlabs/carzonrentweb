<?php
     //error_reporting(E_ALL);
     //ini_set("display_errors", 1);
     session_start();

     include_once("./includes/cache-func.php");
     include_once('./classes/cor.mysql.class.php');

     $xmlToSave1 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
     $xmlToSave1 .= "<ccvtrans>";    
     $query_string = "";
     if ($_POST) {
	  $kv = array();
	  foreach ($_POST as $key => $value) {
	       if($key != "corpassword"){
		    $kv[] = "$key=$value";
		    $xmlToSave1 .= "<$key>$value</$key>";
	       }
	  }	  	  
	  $query_string = join("&", $kv);
     }
     else
	  $query_string = $_SERVER['QUERY_STRING'];
     $xmlToSave1 .= "</ccvtrans>";
     if(!is_dir("./xml/trans/" . date('Y')))
     mkdir("./xml/trans/" . date('Y'), 0775);
     if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
     mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
     if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
     mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
     if(isset($_REQUEST['Order_Id']))
     createcache($xmlToSave1, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . ".xml");
     else
     createcache($xmlToSave1, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . uniqid("F-") . ".xml");
     
     require_once("./includes/libfuncs.php");
     $WorkingKey = "72dxgbita13wdybvv9ar8du7bzizgqhq" ; //put in the 32 bit working key in the quotes provided here
     $Merchant_Id= $_REQUEST['Merchant_Id'];
     $Amount= $_REQUEST['Amount'];
     $Order_Id= $_REQUEST['Order_Id'];
     $Merchant_Param= $_REQUEST['Merchant_Param'];
     $Checksum= $_REQUEST['Checksum'];
     $AuthDesc=$_REQUEST['AuthDesc'];
     $ChecksumTF = verifyChecksum($Merchant_Id, $Order_Id , $Amount,$AuthDesc,$Checksum,$WorkingKey);
     
     if($ChecksumTF == true && isset($_REQUEST["AuthDesc"])){
	  include_once('./classes/cor.ws.class.php');
	  include_once('./classes/cor.xmlparser.class.php');
	  include_once('./classes/cor.gp.class.php');
	  
	  if($_REQUEST["AuthDesc"] == "Y" || $_REQUEST["AuthDesc"] == "B"){
	       
	       if(isset($_REQUEST['Order_Id'])){		    
		    // Added by Iqbal on 28Aug2013 START
		    $dataToSave = array();
		    $db = new MySqlConnection(CONNSTRING);
		    $db->open();
	    
		    $dataToSave["payment_mode"] = "1";
		    $dataToSave["payment_status"] = "3";//3 successful transaction at PG		    
		    $dataToSave["billing_cust_name"] = $_POST["billing_cust_name"];
		    $dataToSave["billing_cust_address"] = $_POST["billing_cust_address"];
		    $dataToSave["billing_cust_state"] = $_POST["billing_cust_state"];
		    $dataToSave["billing_zip_code"] = $_POST["billing_zip_code"];
		    $dataToSave["billing_cust_city"] = $_POST["billing_cust_city"];
		    $dataToSave["billing_cust_country"] = $_POST["billing_cust_country"];
		    $dataToSave["billing_cust_tel"] = $_POST["billing_cust_tel"];
		    $dataToSave["billing_cust_email"] = $_POST["billing_cust_email"];
		    $dataToSave["AuthDesc"] = $_POST["AuthDesc"];
		    $dataToSave["Checksum"] = $_POST["Checksum"];
		    $dataToSave["Merchant_Param"] = $_POST["Merchant_Param"];
		    $dataToSave["nb_bid"] = $_POST["nb_bid"];
		    $dataToSave["nb_order_no"] = $_POST["nb_order_no"];
		    $dataToSave["card_category"] = $_POST["card_category"];
		    $dataToSave["bank_name"] = $_POST["bank_name"];
		    $dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		    
		    $whereData = array();
		    $whereData["coric"] = $_REQUEST['Order_Id'];		
		    $r = $db->update("cor_booking_new", $dataToSave, $whereData);
		    unset($whereData);
		    unset($dataToSave);
		    $db->close();
		    
		    // Added by Iqbal on 28Aug2013 ENDS
	       }
	  
	       $url = "";
	       if(isset($_SESSION["rurl"])){
		    $url = $_SESSION["rurl"];
		    unset($_SESSION["rurl"]);
	       }
	       $fullname = explode(" ", $_SESSION["name"]);
	       if(count($fullname) > 1){
		    $firstName = $fullname[0];
		    $lastName = $fullname[1];
	       } else {
		    $firstName = $fullname[0];
		    $lastName = "";
	       }
	       $cor = new COR();
	       $myXML = new CORXMLList();
	       $mtime = round(microtime(true) * 1000);
	       
	       $pkgId = $_SESSION["pkgId"];
	       $destination = $_SESSION["hdDestinationName"];
	       $totalKM = $_SESSION["totalKM"];
	       $origin = $_SESSION["hdOriginName"];
	       $TimeOfPickup = $_SESSION["picktime"];
	       $address = $_SESSION["address"];
	       $phone = $_SESSION["monumber"];
	       $emailId = $_SESSION["email"];
	       $userId = $_SESSION["cciid"];
	       $paymentAmount = $_REQUEST["Amount"];
	       $paymentType = "1";
	       $paymentStatus = "1";
	       $trackId = $_REQUEST["Order_Id"];
	       $transactionId = $_REQUEST["nb_order_no"];
	       $discountPc = $_SESSION["dispc"];
	       $discountAmt = $_SESSION["discountAmt"];
	       $empcode = $_SESSION["empcode"];
	       if(trim($empcode) == "Promotion code")
	       $empcode = "";
	       $disccode = $_SESSION["disccode"];
	       if(trim($disccode) == "Discount coupon number")
	       $disccode = "";
	       $remarks = $_SESSION["remarks"];
	       $visitedCities = $_SESSION["hdDestinationName"];
	       $outStationYN = "true";
	       if(isset($_COOKIE["corsrc"]))
	       $srcCookie = $_COOKIE["corsrc"];//Aamir 1-Aug-2012
	       if(isset($_COOKIE["gclid"]))
	       $srcCookie = $_COOKIE["gclid"];//Iqbal 10-Oct-2012
	       $tourType = $_SESSION["hdTourtype"];
	       if($_SESSION["hdTourtype"] != "Local")
	       $outStationYN = "true";
	       else
	       $outStationYN = "false";
	       $addServ = $_SESSION["addServ"];
	       $addServAmt = $_SESSION["addServAmt"];
	       if(isset($_SESSION["IsPayBack"]) && $_SESSION["IsPayBack"] != "")
	       $IsPB = $_SESSION["IsPayBack"];
	       else
	       $IsPB = "0";
	       if(isset($_SESSION["disc_txn_id"]) && $_SESSION["disc_txn_id"] != "")
	       $DiscTxnID = $_SESSION["disc_txn_id"];
	       else
	       $DiscTxnID = "0";
	       if(isset($_SESSION["subLoc"]) && $_SESSION["subLoc"] != "")
	       $subLoc = $_SESSION["subLoc"];
	       else
	       $subLoc = "0";
	       
	       
	       $MParam = explode("-", $Merchant_Param);
	       if(count($MParam) > 1){
		  $dispc = "10";
		  $discountAmt = intval($MParam[0]);
		  $disccode = "AMEX";
	       }
	       
	       if(isset($_SESSION["hdPickdate"])){
		    $pDate =  $_SESSION["hdPickdate"];
		    $pickDate = date_create($pDate);
	       }
	       if(isset($_SESSION["hdDropdate"])){
		    $dDate =  $_SESSION["hdDropdate"];
		    $dropDate = date_create($dDate);
	       }
	       $dateOut = $pickDate->format('Y-m-d');
	       $dateIn = $dropDate->format('Y-m-d');
	       
	       if(isset($_SESSION["droptime"]))
	       $dTime =  $_SESSION["droptime"];
		    
	       if($_SESSION["hdTourtype"] == "Selfdrive"){	       
		    $corArrSTD = array();
		    $corArrSTD["pkgId"] = $pkgId;
		    $corArrSTD["PickUpdate"] = $dateOut;
		    $corArrSTD["PickUptime"] = $TimeOfPickup;
		    $corArrSTD["DropOffDate"] = $dateIn;
		    $corArrSTD["DropOfftime"] = $dTime;
		    $corArrSTD["FirstName"] = $firstName;
		    $corArrSTD["LastName"] = $lastName;
		    $corArrSTD["phone"] = $phone;
		    $corArrSTD["paymentMode"] = "1";
		    $corArrSTD["emailId"] = $emailId;
		    $corArrSTD["userId"] = $userId;
		    $corArrSTD["paymentAmount"] = $paymentAmount;
		    $corArrSTD["paymentType"] = $paymentType;
		    $corArrSTD["paymentStatus"] = $paymentStatus;
		    $corArrSTD["visitedCities"] = $visitedCities;
		    $corArrSTD["AdditionalService"] = $addServ;
		    $corArrSTD["ServiceAmount"] = $addServAmt;
		    $corArrSTD["trackId"] = $trackId;
		    $corArrSTD["transactionId"] = $transactionId;
		    $corArrSTD["remarks"] = $remarks;
		    $corArrSTD["chkSum"] = $Checksum;
		    $corArrSTD["OriginCode"] = $srcCookie;
		    $corArrSTD["discountPc"] = $discountPc;
		    $corArrSTD["discountAmount"] = $discountAmt;
		    $corArrSTD["DiscountCode"] = $disccode;
		    $corArrSTD["PromotionCode"] = $empcode;
		    $corArrSTD["IsPayBack"] = $IsPB;
		    $corArrSTD["discountTrancastionID"] = $DiscTxnID;
		    $corArrSTD["SubLocationID"] = $subLoc;
		    
		    $res = $cor->_CORSelfDriveCreateBooking($corArrSTD);
		    $myXML = new CORXMLList();
		    if($res->{'SelfDrive_CreateBookingResult'}->{'any'} != ""){
			 $arrUserId = $myXML->xml2ary($res->{'SelfDrive_CreateBookingResult'}->{'any'});
			 if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){
			      $url = corWebRoot . "/self-drive/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			      $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			      $xmlToSave .= "<cortrans>";
			      $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
			      $xmlToSave .= "</cortrans>";
			      if(!is_dir("./xml/trans/" . date('Y')))
			      mkdir("./xml/trans/" . date('Y'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			      if(isset($_REQUEST['Order_Id']))
			      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-B.xml");
			 
			      $dataToSave = array();
			      $dataToSave["BookingID"] = intval($arrUserId[0]["Column1"]);
			      $dataToSave["PaymentStatus"] = 1;
			      $dataToSave["PaymentAmount"] = intval($paymentAmount);
			      $dataToSave["TransactionId"] = $_REQUEST["nb_order_no"];
			      $dataToSave["ckhSum"] = $Checksum;
			      $dataToSave["Trackid"] = $trackId;
			      $dataToSave["discountPc"] = "0";
			      $dataToSave["discountAmount"] = $discountAmt;
			      $dataToSave["DisCountCode"] = $empcode;
			      $dataToSave["DiscountTransactionId"] = $DiscTxnID;
			      $res = $cor->_CORSelfDriveUpdatePayment($dataToSave);
			      unset($dataToSave);
			      
			      if(isset($_REQUEST['Order_Id'])){
				   $db = new MySqlConnection(CONNSTRING);
				   $db->open();
				   $dataToSave = array();
				   $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
				   $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
				   $dataToSave["payment_mode"] = "1";
				   $whereData = array();
				   $whereData["coric"] = $_REQUEST['Order_Id'];		
				   $r = $db->update("cor_booking_new", $dataToSave, $whereData);
				   unset($whereData);
				   unset($dataToSave);
				   
				   if($disccode != ""){
					$whereData = array();
					$whereData["voucher_number"] = $disccode;
					$dataToSave = array();
					$dataToSave["is_booked"] = "1";
					$r = $db->update("dicount_vouchers_master", $dataToSave, $whereData);
					unset($whereData);
					unset($dataToSave);
				   }
				   
				   $db->close();
			      }
			 }
			 else{
			      $url = str_replace("search-result.php", "booking-fail.php", $url);
			      $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			      $xmlToSave .= "<cortrans>";
			      $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			      $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			      $xmlToSave .= "</cortrans>";
			      if(!is_dir("./xml/trans/" . date('Y')))
			      mkdir("./xml/trans/" . date('Y'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			      if(isset($_REQUEST['Order_Id']))
			      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
			      
			      $dataToMail= array();
			      $dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			      $dataToMail["Subject"] = "Booking Failure: " . $_REQUEST['Order_Id'] . " Case 1"; // Added by Iqbal on 28Aug2013 START
			      $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
			      
			      $res = $cor->_CORSendMail($dataToMail);
			      unset($dataToMail);
			 }
		    }
		    else {
			 $url = str_replace("search-result.php", "booking-fail.php", $url);
			 $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			 $xmlToSave .= "<cortrans>";
			 $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			 $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			 $xmlToSave .= "</cortrans>";
			 if(!is_dir("./xml/trans/" . date('Y')))
			 mkdir("./xml/trans/" . date('Y'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			 if(isset($_REQUEST['Order_Id']))
			 createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
			 
			 $dataToMail= array();
			 $dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			 $dataToMail["Subject"] = "Booking Failure: " . $_REQUEST['Order_Id'] . " Case 2"; // Added by Iqbal on 28Aug2013 START
			 $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
			 
			 $res = $cor->_CORSendMail($dataToMail);
			 unset($dataToMail);
		    }
	       }
	       else {       
		    $corArrSTD = array();
		    $corArrSTD["pkgId"] = $pkgId;
		    $corArrSTD["destination"] = $destination;
		    $corArrSTD["dateOut"] = $dateOut;
		    $corArrSTD["dateIn"] = $dateIn;
		    $corArrSTD["TimeOfPickup"] = $TimeOfPickup;
		    $corArrSTD["address"] = str_ireplace(array("<br>","<br />"), "", nl2br($address));
		    $corArrSTD["firstName"] = $firstName;
		    $corArrSTD["lastName"] = $lastName;
		    $corArrSTD["phone"] = $phone;
		    $corArrSTD["emailId"] = $emailId;
		    $corArrSTD["userId"] = $userId;
		    $corArrSTD["paymentAmount"] = $paymentAmount;
		    $corArrSTD["paymentType"] = $paymentType;
		    $corArrSTD["paymentStatus"] = $paymentStatus;
		    $corArrSTD["trackId"] = $trackId;
		    $corArrSTD["transactionId"] = $transactionId;
		    $corArrSTD["discountPc"] = $discountPc;
		    $corArrSTD["discountAmount"] = $discountAmt;
		    $corArrSTD["remarks"] = $remarks;
		    $corArrSTD["totalKM"] = $totalKM;
		    $corArrSTD["visitedCities"] = $visitedCities;
		    $corArrSTD["outStationYN"] = $outStationYN;
		    $corArrSTD["OriginCode"] = $srcCookie; //Aamir 1-Aug-2012
		    $corArrSTD["chkSum"] = $Checksum;
		    $corArrSTD["DisCountCode"] = $disccode;
		    $corArrSTD["PromotionCode"] = $empcode;
		    $corArrSTD["IsPayBack"] = $IsPB;
		    $corArrSTD["discountTrancastionID"] = $DiscTxnID;
	       
		    $res = $cor->_CORMakeBooking($corArrSTD);
	       
		    $myXML = new CORXMLList();
		    if($res->{'SetTravelDetailsResult'}->{'any'} != ""){
			 $arrUserId = $myXML->xml2ary($res->{'SetTravelDetailsResult'}->{'any'});
			 if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){
			      if($tourType != "Local")
				   $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "-to-" . str_ireplace(" ", "-", strtolower($destination)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			      else
				   $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($origin)) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
			      $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			      $xmlToSave .= "<cortrans>";
			      $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
			      $xmlToSave .= "</cortrans>";
			      if(!is_dir("./xml/trans/" . date('Y')))
			      mkdir("./xml/trans/" . date('Y'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			      if(isset($_REQUEST['Order_Id']))
			      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-B.xml");
			      
			      if(isset($_REQUEST['Order_Id'])){
				   $db = new MySqlConnection(CONNSTRING);
				   $db->open();
				   $dataToSave = array();
				   $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
				   $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
				   $dataToSave["payment_mode"] = "1";
				   $whereData = array();
				   $whereData["coric"] = $_REQUEST['Order_Id'];		
				   $r = $db->update("cor_booking_new", $dataToSave, $whereData);
				   unset($whereData);
				   unset($dataToSave);
				   
				   if($disccode != ""){
					$whereData = array();
					$whereData["voucher_number"] = $disccode;
					$dataToSave = array();
					$dataToSave["is_booked"] = "1";
					$r = $db->update("dicount_vouchers_master", $dataToSave, $whereData);
					unset($whereData);
					unset($dataToSave);
				   }
				   $db->close();
			      }
			 }
			 else{
			      $url = str_replace("search-result.php", "booking-fail.php", $url);
			      $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			      $xmlToSave .= "<cortrans>";
			      $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			      $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			      $xmlToSave .= "</cortrans>";
			      if(!is_dir("./xml/trans/" . date('Y')))
			      mkdir("./xml/trans/" . date('Y'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			      if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			      mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			      if(isset($_REQUEST['Order_Id']))
			      createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
			      
			      $dataToMail= array();
			      $dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			      $dataToMail["Subject"] = "Booking Failure: " . $_REQUEST['Order_Id'] . " Case 3"; // Added by Iqbal on 28Aug2013 START
			      $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
			      
			      $res = $cor->_CORSendMail($dataToMail);
			      unset($dataToMail);
			 }
		    }
		    else{
			 $url = str_replace("search-result.php", "booking-fail.php", $url);
			 $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			 $xmlToSave .= "<cortrans>";
			 $xmlToSave .= "<error1>" . serialize($res) . "</error1>";
			 $xmlToSave .= "<bookingDetails>" . serialize($corArrSTD) . "</bookingDetails>";	
			 $xmlToSave .= "</cortrans>";
			 if(!is_dir("./xml/trans/" . date('Y')))
			 mkdir("./xml/trans/" . date('Y'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
			 if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			 mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			 if(isset($_REQUEST['Order_Id']))
			 createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
			 
			 $dataToMail= array();
			 $dataToMail["To"] = "abhishek.bhaskar@carzonrent.com";
			 $dataToMail["Subject"] = "Booking Failure: " . $_REQUEST['Order_Id'] . " Case 4"; // Added by Iqbal on 28Aug2013 START
			 $dataToMail["MailBody"] = $xmlToSave . "<br />" . $xmlToSave1;
			 
			 $res = $cor->_CORSendMail($dataToMail);
			 unset($dataToMail);
		    }
	       }
	       //createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $trackId . ".xml");
	       unset($cor);
	       header('Location: ' . urldecode($url));
	  }
	  else {
	       $url = "booking-fail.php";
	       $url .= "?resp=bookfail&bookid=" . $_SESSION["hdBookingID"];
	       $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	       $xmlToSave .= "<cortrans>";
	       $xmlToSave .= "<error3>Card Error.</error3>";
	       $xmlToSave .= "<errorDetails>Card authentication failed.</errorDetails>";
	       $xmlToSave .= "</cortrans>";
	       if(!is_dir("./xml/trans/" . date('Y')))
	       mkdir("./xml/trans/" . date('Y'), 0775);
	       if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
	       mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
	       if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
	       mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
	       if(isset($_REQUEST['Order_Id']))
	       createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
	       else
	       createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . uniqid("C-") . ".xml");
	       
	       // Added by Iqbal on 28Aug2013 START
	       $dataToSave = array();
	       $db = new MySqlConnection(CONNSTRING);
	       $db->open();
       
	       $dataToSave["payment_status"] = "4";//4 Pressed Cancel Button at PG
	       $dataToSave["billing_cust_name"] = $_POST["billing_cust_name"];
	       $dataToSave["billing_cust_address"] = $_POST["billing_cust_address"];
	       $dataToSave["billing_cust_state"] = $_POST["billing_cust_state"];
	       $dataToSave["billing_zip_code"] = $_POST["billing_zip_code"];
	       $dataToSave["billing_cust_city"] = $_POST["billing_cust_city"];
	       $dataToSave["billing_cust_country"] = $_POST["billing_cust_country"];
	       $dataToSave["billing_cust_tel"] = $_POST["billing_cust_tel"];
	       $dataToSave["billing_cust_email"] = $_POST["billing_cust_email"];
	       $dataToSave["AuthDesc"] = $_POST["AuthDesc"];
	       $dataToSave["Checksum"] = $_POST["Checksum"];
	       $dataToSave["Merchant_Param"] = $_POST["Merchant_Param"];
	       $dataToSave["nb_bid"] = $_POST["nb_bid"];
	       $dataToSave["nb_order_no"] = $_POST["nb_order_no"];
	       $dataToSave["card_category"] = $_POST["card_category"];
	       $dataToSave["bank_name"] = $_POST["bank_name"];
	       $dataToSave["payment_update_date"] = "NOW()";
	  
	       $whereData = array();
	       $whereData["coric"] = $_REQUEST['Order_Id'];		
	       $r = $db->update("cor_booking_new", $dataToSave, $whereData);
	       unset($whereData);
	       unset($dataToSave);
	       $db->close();
	       // Added by Iqbal on 28Aug2013 ENDS
	       
	       header('Location: ' . urldecode($url));
	  }
     }
     else {
	  $url = "booking-fail.php";
	  $url .= "?resp=bookfail&bookid=" . $_SESSION["hdBookingID"];
	  $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	  $xmlToSave .= "<cortrans>";
	  $xmlToSave .= "<error3>Checksum not verified.</error3>";
	  $xmlToSave .= "<errorDetails>Checksum failed.</errorDetails>";
          $xmlToSave .= "</cortrans>";
	  if(!is_dir("./xml/trans/" . date('Y')))
	  mkdir("./xml/trans/" . date('Y'), 0775);
	  if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
	  mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
	  if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
	  mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
	  if(isset($_REQUEST['Order_Id']))
	  createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_REQUEST['Order_Id'] . "-E.xml");
	  else
	  createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . uniqid("C-") . ".xml");
	  
	  // Added by Iqbal on 28Aug2013 STARTS
	  $dataToSave = array();
	  $db = new MySqlConnection(CONNSTRING);
	  $db->open();
  
	  $dataToSave["payment_status"] = "5";//5 Unauthorised payment at PG
	  $dataToSave["billing_cust_name"] = $_POST["billing_cust_name"];
	  $dataToSave["billing_cust_address"] = $_POST["billing_cust_address"];
	  $dataToSave["billing_cust_state"] = $_POST["billing_cust_state"];
	  $dataToSave["billing_zip_code"] = $_POST["billing_zip_code"];
	  $dataToSave["billing_cust_city"] = $_POST["billing_cust_city"];
	  $dataToSave["billing_cust_country"] = $_POST["billing_cust_country"];
	  $dataToSave["billing_cust_tel"] = $_POST["billing_cust_tel"];
	  $dataToSave["billing_cust_email"] = $_POST["billing_cust_email"];
	  $dataToSave["AuthDesc"] = $_POST["AuthDesc"];
	  $dataToSave["Checksum"] = $_POST["Checksum"];
	  $dataToSave["Merchant_Param"] = $_POST["Merchant_Param"];
	  $dataToSave["nb_bid"] = $_POST["nb_bid"];
	  $dataToSave["nb_order_no"] = $_POST["nb_order_no"];
	  $dataToSave["card_category"] = $_POST["card_category"];
	  $dataToSave["bank_name"] = $_POST["bank_name"];
	  $dataToSave["payment_update_date"] = "NOW()";
	  
	  $whereData = array();
	  $whereData["coric"] = $_REQUEST['Order_Id'];		
	  $r = $db->update("cor_booking_new", $dataToSave, $whereData);
	  unset($whereData);
	  unset($dataToSave);
	  $db->close();
	  // Added by Iqbal on 28Aug2013 ENDS
	       
	  header('Location: ' . urldecode($url));
     }
?>
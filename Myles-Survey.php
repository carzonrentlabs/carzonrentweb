<?php
 error_reporting(1);
 //error_reporting(E_ALL);
 //ini_set("display_errors", 1);
 include_once('./classes/cor.ws.class.php');
 include_once('./classes/cor.xmlparser.class.php');
 include_once("./includes/cache-func.php");
 include_once('./classes/cor.mysql.class.php');
 $cor = new COR();
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Myles Servey</title>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/drivetowin/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/seo-metas.php"); ?>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php");
?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<!-- Tab end -->
<!-- OnClick Slider start -->
<link href="css/jcarousel.css" rel="stylesheet" type="text/css">
<!-- OnClick Slider end -->
<link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
<script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
<style type="text/css">
body{
background:#fff;
}
</style>
</head>
<body>
<?php include_once("./includes/header-with-service.php"); ?>
<div class="clr"> </div>
<br>
<div class="main">

<div><img src='images/myles.png'></div>
<br>
<br>
<h2><u style="color:#000;">How to avail / book</u></h2>
<br>
<span>1. Log on to <a target="_blank" href="http://www.carzonrent.com">www.carzonrent.com</a> .Select the preferred city, date/time, car & the location.</span><br /><br />
   <span>2. Post filling in the details, enter the discount code sent to your registered Email-ID in the “Discount/Promotion Code” box.  </span><br /><br />
   <span>3. Proceed with the booking & pay the balance amount through credit card/debit card/net banking. </span><br /><br />
   <span>Know more about the service at <a target="_blank" href="http://www.carzonrent.com/myles">www.carzonrent.com/myles</a></span>
<br>
<br>
<br>
<br>
	<h2><u style="color:#000;">Terms and Conditions</u></h2><br>

   <span>1. Code is applicable on a minimum booking of 2 days.</span><br /><br />
   <span>2. Valid only on the Self-Drive service: Myles. </span><br /><br />
   <span>3. Valid for all cities & cars. </span><br /><br />
   <span>4. Booking subject to car availability. </span><br /><br />
   <span>5. Standing instructions/ Pre-authorization will be made on the credit card based on the car type at the time of pick-up.</span><br /><br />
   <span>6. Offer not to be clubbed with any other promotion.</span><br /><br />
   <span>7. The discount can be redeemed in full & not in parts.</span><br /><br />
   <span>8. Code is limited to one time use only.</span><br /><br />
   <span>9. Valid for reservations made till 28 February 2015.</span><br /><br />
   <span>10. Blackout dates apply.</span><br /><br />
   <span>11. Standard T&C apply.</span><br /><br />
   </br>
   </br>
  
   </div>
   </br>
   
</div>
<div class="clr"> </div>
<?php include_once("./includes/footertc.php"); ?>

	<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-22195130-1']);
	_gaq.push(['_setDomainName', '.carzonrent.com']);
	_gaq.push(['_trackPageview']);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	</script>

     </body>
</html>
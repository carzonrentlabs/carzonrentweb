<?php
	error_reporting(1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once('./classes/cor.mysql.class.php');
	include_once('./classes/cor.gp.class.php');
	include_once("./includes/cache-func.php");
	echo "COR";
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	unset($res);
	unset($cor);
	$tCCIID = 0;
	if(isset($_POST["hdOriginName"])){
		$orgName =  $_POST["hdOriginName"];
		$orgNames = explode(",", $orgName);
	}
	if(isset($_POST["hdOriginID"])){
		$orgID =  $_POST["hdOriginID"];
		$orgIDs = explode(",", $orgID);
	}
	if(isset($_POST["hdDestinationName"])){
		$destName =  $_POST["hdDestinationName"];
		$destNames = explode(",", $destName);
	}
	if(isset($_POST["hdDestinationID"])){
		$destID =  $_POST["hdDestinationID"];
		$destIDs = explode(",", $destID);
	}
	if(isset($_POST["pickdate"])){
		$pDate =  str_ireplace(",", "", $_POST["pickdate"]);
		$pickDate = date_create($pDate);
	}
	if(isset($_POST["dropdate"]))
	{
		if(trim($_POST["hdTourtype"]) == "Selfdrive")
		{
			if(isset($_POST["chkPkgType"]) && trim($_POST["chkPkgType"]) == "Hourly")
			{
				$dDate =  str_ireplace(",", "", $_POST["pickdate"]);
				$dropDate = date_create($dDate . " " . $_POST["tHourP"] . ":" . $_POST["tMinP"] . ":00");
				$dropDate->modify("+" . intval($_POST["nHours"])  . " hours");
			}
			else
			{
				$dDate =  str_ireplace(",", "", $_POST["dropdate"]);
				$dropDate = date_create($dDate . " " . $_POST["tHourD"] . ":" . $_POST["tMinD"] . ":00");
				//echo "Drop Date (Daily): " . $dropDate->format('d M, Y H:i');
			}
		}
	}
     $tab = 1;
     if(isset($_POST["tab"]))
	{
		$tab =  $_POST["tab"];	
	}
     if($tab == 1)
     {
		$offerStartDate = date_create("2013-10-10");
		$offerEndDate = date_create("2013-10-14");
		$isOffer = false;
	     if(isset($_POST["hdOriginID"]) && $_POST["hdOriginID"] == 15)
		{
			if((strtotime($pickDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($pickDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
			$isOffer = true;
			if($_POST["hdTourtype"] != "Local")
			{
				if((strtotime($dropDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($dropDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
				$isOffer = true;
			}
		}
		$offerStartDate = date_create("2013-10-13");
		$offerEndDate = date_create("2013-10-14");
		if(isset($_POST["hdOriginID"]) && $_POST["hdOriginID"] == 7)
		{
			if((strtotime($pickDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($pickDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
			$isOffer = true;
			if($_POST["hdTourtype"] != "Local"){
				if((strtotime($dropDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($dropDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
			$isOffer = true;
			}
		}
	     $ll = "";
	     $dll = "";
                if($_POST["hdTourtype"] == "Selfdrive")
                {
			$isInHrs = 0;
			$interval = strtotime($dropDate->format('Y') . "-" . $dropDate->format('m') . "-" . $dropDate->format('d') . " " . $_POST["tHourD"]. ":" . $_POST["tMinD"]. ":00") - strtotime($pickDate->format('Y') . "-" . $pickDate->format('m') . "-" . $pickDate->format('d') . " " . $_POST["tHourP"]. ":" . $_POST["tMinP"]. ":00");
			//echo $interval / (60 * 60 * 24);
			$intervalHrs = intval($interval / (60 * 60));
			$totHrs = $intervalHrs;
			$extraHrs = $intervalHrs % 24;
			if($intervalHrs < 2)
				$intervalHrs = 2;
			if($_POST["hdOriginID"] == "69")
			{
				$daysI = intval($interval / (60 * 60 * 24));
				//echo "Days " . $daysI;
				if($extraHrs > 0)
				$daysI++;
				if(intval($_POST["tHourP"]) < 8)
				$daysI++;
				elseif(intval($_POST["tHourD"]) > 8)
				$daysI++;
				elseif(intval($_POST["tHourD"]) == 8){
				   if(intval($_POST["tMinD"]) > 0)
				   $daysI++;
				}
				$interval = $daysI;
			}
			else
			{
				$daysI = intval($interval / (60 * 60 * 24));
				if(($interval / (60 *60)) % 24 > 1)
				{
					if($daysI > 0)
					$interval = $daysI + 1;
					else
					$interval = 0;
				}
				else
				   $interval = $daysI;
			}
			if($interval == 0 && $intervalHrs >= 2)
			{
				$interval = 1;
				$isInHrs = 1;
				//echo "hi";
			}
                }
	     if($interval <= 0 && $intervalHrs < 2)
			header("Location: " . corWebRoot);
	      // Search data capture by Iqbal 28Aug2013 STARTS
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		
		$isDiscountGiven = 0;
		$ip = $_SERVER["REMOTE_ADDR"];
		$ua = $_SERVER["HTTP_USER_AGENT"];
		$tType = $_POST["hdTourtype"];
		$isSearched = 0;
		if(!isset($_COOKIE["CORCustID"]))
		{
			$CORCustID = strtoupper(uniqid("CCID-"));
			setcookie("CORCustID", $CORCustID, time()+3600*24, "/");
		}
		else
		{
			$CORCustID = $_COOKIE["CORCustID"];
			$rs = $db->query("stored procedure", "cor_check_search_data('" . $CORCustID . "', '" . $orgID . "', '" . $destID . "', '" . $pickDate->format('Y-m-d') . "', '" . $interval . "', '" . $tType . "', 'COR')");
			$isSearched = $rs[0]["Total"];
		}
		if($isSearched == 0)
		{
			$eTime = date("H:i:s");
			$searchData = array();
			$searchData["customer_id"] = $CORCustID;
			$searchData["origin_name"] = $orgName;
			$searchData["origin_code"] = $orgID;
			$searchData["destination_name"] = $destName;
			$searchData["destination_code"] = $destID;
			$searchData["pickup_date"] = $pickDate->format('Y-m-d');
			$searchData["duration"] = $interval;
			$searchData["distance"] = ceil(($totalKM/1000));
			$searchData["ip"] = $ip;
			$searchData["user_agent"] = $ua;
			$searchData["entry_date"] = "CURDATE()";
			$searchData["entry_time"] = $eTime;
			$searchData["tour_type"] = $tType;
			$searchData["website"] = "COR";
			$rs = $db->insert("customer_search", $searchData);
			unset($searchData);
		}
		$db->close();
		// Search data capture by Iqbal 28Aug2013 ENDS
		$isDiscountShow = 0;
    }
	
     $totalKM = 0;
	
     if($tab == 4)
     {
		$fullname = explode(" ", trim($_POST["name"]));
		if(count($fullname) > 1)
		{
			$firstName = $fullname[0];
			$lastName = $fullname[1];
		}
		else
		{
			$firstName = $fullname[0];
			$lastName = "";
		}
		$cor = new COR();
		$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
		$myXML = new CORXMLList();
		$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
		//print_r($arrUserId);
                     if(intval($arrUserId[0]["ClientCoIndivID"]) > 0){
			$mtime = round(microtime(true) * 1000);
			$pkgId = $_POST["hdPkgID"];
			$destination = $_POST["hdDestinationName"];            
			$TimeOfPickup = $_POST["picktime"];
			$address = $_POST["address"];
			$phone = $_POST["monumber"];
			$emailId = $_POST["email"];
			$userId = $_POST["cciid"];
			$paymentAmount = $_POST["totFare"];
			//$paymentType = $_POST["rdoPayment"];
			$paymentType = "5";
			$paymentStatus = "0";
			$trackId = $_POST["hdTrackID"];
			$transactionId = $_POST["hdTransactionID"];
			$remarks = $_POST["hdRemarks"];
			$totalKM = $_POST["hdDistance"];
			$visitedCities = $_POST["hdDestinationName"];
			$pincode = $_POST["pincode"];
			$gpAddress = $_POST["gpaddress"];
			$dispc = $_POST["discount"];
			$discountAmt = $_POST["discountAmt"];
			$empcode = $_POST["empcode"];
			if(trim($empcode) == "Promotion code")
				$empcode = "";
			$disccode = $_POST["disccode"];
			if(trim($disccode) == "Discount coupon number")
				$disccode = "";
			$interval  = $_POST["duration"];
			$outStationYN = "true";
			if(isset($_COOKIE["corsrc"]))
				$srcCookie = $_COOKIE["corsrc"];//Aamir 1-Aug-2012
			if(isset($_COOKIE["gclid"]))
				$srcCookie = $_COOKIE["gclid"];//Iqbal 10-Oct-2012
			$tourType = $_POST["hdTourtype"];
			if($_POST["hdTourtype"] != "Local")
				$outStationYN = "true";
			else
			    $outStationYN = "false";
			if(isset($_POST["hdPickdate"]))
			{
			    $pDate =  $_POST["hdPickdate"];
			    $pickDate = date_create($pDate);
			}
			if(isset($_POST["hdDropdate"]))
			{
			    $dDate =  $_POST["hdDropdate"];
			    $dropDate = date_create($dDate);
			}
			$dateOut = $pickDate->format('m/d/Y');
			$dateIn = $dropDate->format('m/d/Y');
			
			$corArrSTD = array();
			$corArrSTD["pkgId"] = $pkgId;
			$corArrSTD["destination"] = $destination;
			$corArrSTD["dateOut"] = $dateOut;
			$corArrSTD["dateIn"] = $dateIn;
			$corArrSTD["TimeOfPickup"] = $TimeOfPickup;
			$corArrSTD["address"] = $address;
			$corArrSTD["firstName"] = $firstName;
			$corArrSTD["lastName"] = $lastName;
			$corArrSTD["phone"] = $phone;
			$corArrSTD["emailId"] = $emailId;
			$corArrSTD["userId"] = $userId;
			$corArrSTD["paymentAmount"] = $paymentAmount;
			$corArrSTD["paymentType"] = $paymentType;
			$corArrSTD["paymentStatus"] = $paymentStatus;
			$corArrSTD["trackId"] = $trackId;
			$corArrSTD["transactionId"] = $transactionId;
			$corArrSTD["discountPc"] = $dispc;
			$corArrSTD["discountAmount"] = $discountAmt;
			$corArrSTD["remarks"] = $remarks;
			$corArrSTD["totalKM"] = $totalKM;
			$corArrSTD["visitedCities"] = $visitedCities;
			$corArrSTD["outStationYN"] = $outStationYN;
			$corArrSTD["OriginCode"] = $srcCookie; //Aamir 1-Aug-2012
			$corArrSTD["chkSum"] = "0";
			$corArrSTD["DisCountCode"] = $disccode; //Iqbal 09-Jan-2013
			$corArrSTD["PromotionCode"] = $empcode; //Iqbal 09-Jan-2013
			$corArrSTD["IsPayBack"] = $_SESSION["IsPayBack"];
			
			$res = $cor->_CORMakeBooking($corArrSTD);
			$myXML = new CORXMLList();
			if($res->{'SetTravelDetailsResult'}->{'any'} != ""){
				$arrUserId = $myXML->xml2ary($res->{'SetTravelDetailsResult'}->{'any'});
				if($arrUserId[0]["Column1"] > 0){
					$db = new MySqlConnection(CONNSTRING);
					$db->open();
					$whereData = array();
					$dataToSave = array();
					$whereData["coric"] = $transactionId;
					$dataToSave["payment_mode"] = "2";
					$dataToSave["payment_status"] = "1";
					$dataToSave["booking_id"] = $arrUserId[0]["Column1"];
					$r = $db->update("cor_booking_new", $dataToSave, $whereData);
					unset($whereData);
					unset($dataToSave);
					if($disccode != "")
					{
						$whereData = array();
						$whereData["voucher_number"] = $disccode;
						$dataToSave = array();
						$dataToSave["is_booked"] = "1";
						$r = $db->update("dicount_vouchers_master", $dataToSave, $whereData);
						unset($whereData);
						unset($dataToSave);
					}
					$db->close();
					
					if($tourType != "Local")
						$url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_POST["hdOriginName"])) . "-to-" . str_ireplace(" ", "-", strtolower($_POST["hdDestinationName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
					else
						$url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_POST["hdOriginName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
					$xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
					$xmlToSave .= "<cortrans>";
					$xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
					$xmlToSave .= "</cortrans>";
					if(!is_dir("./xml/trans/" . date('Y')))
					mkdir("./xml/trans/" . date('Y'), 0775);
					if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
					mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
					if(!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
					mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
					if(isset($transactionId))
					createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $transactionId . "-B.xml");
				}
			}
			else
				$tab = 4;
		}
		else
			$tab = 4;
			
          unset($cor);
          header('Location: ' . $url);
     }
     if($tab == 3)
	{
		if(isset($_COOKIE["cciid"]) && isset($_COOKIE["emailid"]))
		{
			if($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != "")
			{
				$tab = 3;
			}
		}
          else
		{
               $cor = new COR();
               if($_POST["corpassword"] == "")
			{
                    $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
                    $myXML = new CORXMLList();
                    $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
                    if(key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0){
                        //$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                        $tab = 2;
                    }
                    else
				{
                        //echo $arrUserId[0]["ClientCoIndivID"];
                        setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
				$tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                        $tab = 3;
                        //$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                    }
                    unset($myXML);            
               }
               else
			{
                    if(isset($_POST["isExistUser"]))
				{
					if($_POST["isExistUser"] == "1")
					{
						$res = $cor->_CORCheckLogin(array("PhoneNo"=>$_POST["monumber"],"password"=>$_POST["corpassword"],"clientid"=>2205));
						$myXML = new CORXMLList();
						if($res->{'verifyLoginResult'}->{'any'} != "")
						{
							$arrUserId = $myXML->xml2ary($res->{'verifyLoginResult'}->{'any'});
							if($arrUserId[0]["ClientCoIndivID"] > 0)
							{
								setcookie("cciid", $arrUserId[0]["ClientCoIndivID"]);
								setcookie("emailid", $arrUserId[0]["EmailID"]);
								setcookie("phone1", $arrUserId[0]["Phone1"]);
								//$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
								$tab = 3;
							}
						}
						else
						{
							$tab = 2;
						}
						unset($myXML);
                        }
                    }
                    else
				{
					$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
					$myXML = new CORXMLList();
					$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
					if(key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0){
					    //$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
					    $tab = 2;
					}
					else
					{
						setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
						$tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
						//$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
						$tab = 3;
					}
                        unset($myXML);
                    }
               }
			session_start();
			echo "<!--";
			echo "Email In Session: " . $_SESSION["email"];
			echo "-->";
			$fullname = explode(" ", $_POST["name"]);
			if(count($fullname) > 1)
			{
				$fname = $fullname[0];
				$lname = $fullname[1];
			}
			else
			{
				$fname = $fullname[0];
				$lname = "";
			}
			//$cor = new COR();
			$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["monumber"],"clientid"=>2205));
			$myXML = new CORXMLList();
			$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
			//print_r($res);
			if(key_exists("Column1", $arrUserId[0]) && intval($arrUserId[0]["Column1"]) == 0)
			{
				if(isset($_POST["corpassword"]))
					$pword = $_POST["corpassword"];
				else
					$pword = "retail@2012";
				$res = $cor->_CORRegister(array("fname"=>$fname, "lname"=>$lname, "PhoneNumber"=>$_POST["monumber"], "emailId"=>$_POST["email"], "password"=>$pword));
				$arrUserId = $myXML->xml2ary($res->{'RegistrationResult'}->{'any'});
				if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0)
				{
					setcookie("tcciid", $arrUserId[0]["Column1"]);
					$tCCIID = trim($arrUserId[0]["Column1"]);
					//$url = "search-result.php?" . $query_string;
					$tab = 3;
				}            
			}
			else
			{
				if(intval($arrUserId[0]["ClientCoIndivID"]) > 0)
				{
					setcookie("tcciid", $arrUserId[0]["ClientCoIndivID"]);
					$tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
					$tab = 3;
				}
			}
			unset($cor);
          }
     }
	if($_POST["hdTourtype"] != "Selfdrive" && $_POST["hdTourtype"] != "Outstation" && $_POST["hdTourtype"] != "Local")
		header("Location: " . corWebRoot);
	else
		$tourType = $_POST["hdTourtype"];
?>
<!DOCTYPE>
<html>
<head>
<?php
    include_once("./includes/head-meta.php");
?>
<script>
    $(function(){
        $('select.styled').customSelect();
        
        });
</script>
 <title>Myles | Search Result</title>
</head>
<body>
    <div class="body">
        <div class="main">
            <div class="car_zon_main">
                <?php
                    include_once("./includes/header-myles.php");
                ?>
                <div class="most_page">
                    <div class="white_backgraund">
                        <div class="location">
                            <div class="bookmyles mt50">
                                <span class="book_text">VEHICLE SELECTOR</span>
                            </div> <!--close bookmyles div-->
                            <div id="search_vehicle">
                                <span class="city_text w220 mb20">Sort by Pick Up Location</span>
<?php
        $isOutStation = TRUE;
        $cor = new COR();
        if(trim($_POST["chkPkgType"]) == "Daily")
             $res = $cor->_CORSelfDriveGetCabs(array("CityId"=>$orgIDs[0],"fromDate"=>$pickDate->format('Y-m-d'),"DateIn"=>$dropDate->format('Y-m-d'),"PickupTime"=>$_POST["tHourP"] . $_POST["tMinP"],"DropOffTime"=>$dropDate->format('H') . $dropDate->format('i'), "SubLocation"=>"0", "Duration"=>$interval, "PkgType"=>$_POST["chkPkgType"]));
        elseif(trim($_POST["chkPkgType"]) == "Hourly")
             $res = $cor->_CORSelfDriveGetCabs(array("CityId"=>$orgIDs[0],"fromDate"=>$pickDate->format('Y-m-d'),"DateIn"=>$dropDate->format('Y-m-d'),"PickupTime"=>$_POST["tHourP"] . $_POST["tMinP"],"DropOffTime"=>$dropDate->format('H') . $dropDate->format('i'), "SubLocation"=>"0", "Duration"=>$_POST["nHours"], "PkgType"=>$_POST["chkPkgType"]));
        
        $myXML = new CORXMLList();
        $arr = $myXML->xml2ary($res);
?>
                                <span class="location w220">
                                   <label>
                                    <select style="width: 220px;" onchange="javascript: _getSRSFSubLoc(this.value,<?php echo $orgIDs[0]; ?>, '<?php echo $orgNames[0]; ?>', '<?php echo $pickDate->format('Y-m-d'); ?>', '<?php echo $dropDate->format('Y-m-d'); ?>', '<?php echo $_POST["tHourP"]; ?>', '<?php echo $_POST["tMinP"]; ?>' ,'<?php echo $_POST["tHourP"]; ?>', '<?php echo $_POST["tMinP"]; ?>', this, '<?php echo $_POST["chkPkgType"]; ?>', '<?php if($_POST["chkPkgType"] == "Hourly"){ echo $_POST["nHours"];}elseif($_POST["chkPkgType"] == "Daily"){echo $interval;} ?>');">
<?php
        $res = $cor->_CORSelfDriveGetSubLocationsName(array("CityID"=>$orgIDs[0]));
        $arrSL = $myXML->xml2ary($res);
        if(count($arrSL) > 0){
             $isSLName = array();
             foreach($arrSL as $k => $v)
             $isSLName[$k] = $v['SubLocationName'];
             array_multisort($isSLName, SORT_ASC, $arrSL);
?>
            <option>All Locations</option>
<?php
                  for($i = 0; $i < count($arrSL); $i++){
?>
            <option value="<?php echo $arrSL[$i]["SubLocationID"]; ?>" ><?php echo $arrSL[$i]["SubLocationName"]; ?></option>
<?php
                  }
        }
         unset($arrSL);
?>
                                       </select>
                                   </label>
                               </span>
                                <div class="location_choose">
                                   <!-- <span style="float: left;">
                                        <img src="<?php echo corWebRoot; ?>/images-myles/cancel.png"/>
                                    </span>
                                    <span class="location_text">Plesase Choose a Location</span>-->
                                    <span class="drop_select">
                                        <!--<img src="<?php //echo corWebRoot; ?>/images-myles/orange.png"/>-->
                                    </span>
                                </div>
                            </div> 
                        </div> <!--close location div-->
                        <div class="booking">
                            <div class="detail_border">
                                <div class="you_search">
                                    <span class="service_text fma">YOU SEARCHED FOR</span>
                                </div>
                                <div class="you_search">
                                    <div class="duration">
                                        <span class="find_detail fma"><b>From</b><div class="cb"></div><?php echo $orgName; ?></span>
                                        <span class="find_detail fma"><b>Destination</b><div class="cb"></div><?php echo $destName; ?></span>
                                        <span class="find_detail fma"><b>Pick up Date and Time</b><div class="cb"></div><?php echo $pickDate->format('D d-M, Y'); ?><?php echo " " . $_POST["tHourP"] . $_POST["tMinP"] . "Hrs"; ?></span>
                                        <span class="find_detail fma"><b>Drop off</b><div class="cb"></div><?php echo $dropDate->format('D d-M, Y'); ?><?php echo " " . $_POST["tHourD"] . $_POST["tMinD"] . "Hrs"; ?></span>
<?php
		$pickDate = date_create(date('Y-m-d', strtotime($pDate)));
		if($_POST["chkPkgType"] == "Daily"){
?>
                                        <span class="find_detail fma"><b>Duration</b><div class="cb"></div><?php echo $interval; ?> Days</span>
<?php
                    }
                    elseif($_POST["chkPkgType"] == "Hourly")
                    {
?>
                                        <span class="find_detail fma"><b>Duration</b><div class="cb"></div><?php echo $_POST["nHours"] . " Hrs"; ?></span>
<?php
                    }
?>
                                        
                                    </div>
                                    <!--<input type="button" id="booking_change" name="booking_change" class="change_btn" value="CHANGE"/>-->
                                </div>
                            </div>
                            <div class="payment">
                                <div class="white_backgraund">
                                    <div class="car_detail">
                                        <span class="pay_button fma">SELECT A CAR</span>
                                    </div>
                                    <div class="car_detail">
                                        <span class="show_car fma">BOOKING DETAIL</span>
                                    </div>
                                    <div class="car_detail">
                                        <span class="show_car fma">PAYMENT</span>
                                    </div>
                                </div>
<script type="text/javascript">
    var ahdCarModel = new Array();
    var ahdCarModelID = new Array();
    var ahdOriginName = new Array();
    var ahdOriginID = new Array();
    var ahdDestinationName = new Array();
    var ahdDestinationID = new Array();
    var ahdCarID = new Array();
    var ahdCarCatID = new Array();
    var ahdPkgID = new Array();
    var ahdCat = new Array();
    var aPkgRate = new Array();
    var aduration = new Array();
    var atotAmount = new Array();
    var atotFare = new Array();
    var atab = new Array();
    var ahdPickdate = new Array();
    var ahdDropdate = new Array();
    var ahdTourtype = new Array();
    var atHourP = new Array();
    var atMinP = new Array();
    var atHourD = new Array();
    var atMinD = new Array();
    var avat = new Array();
    var asecDeposit = new Array();
    var asubLoc = new Array();
    var achkPkgType = new Array();
    var anHours = new Array();
    var aOriginalAmt = new Array();
    var aBasicAmt = new Array();
    var aallSubLoc = new Array();
    var aKMIncluded = new Array();
    var aExtraKMRate = new Array();
    var aAvailableAt = new Array();
    var aCostPerday = new Array();
    var aSeatingCapacity = new Array();
<?php
    $isOutStation = TRUE;
    $cor = new COR();
    if(trim($_POST["chkPkgType"]) == "Daily")
         $res = $cor->_CORSelfDriveGetCabs(array("CityId"=>$orgIDs[0],"fromDate"=>$pickDate->format('Y-m-d'),"DateIn"=>$dropDate->format('Y-m-d'),"PickupTime"=>$_POST["tHourP"] . $_POST["tMinP"],"DropOffTime"=>$dropDate->format('H') . $dropDate->format('i'), "SubLocation"=>"0", "Duration"=>$interval, "PkgType"=>$_POST["chkPkgType"]));
    elseif(trim($_POST["chkPkgType"]) == "Hourly")
         $res = $cor->_CORSelfDriveGetCabs(array("CityId"=>$orgIDs[0],"fromDate"=>$pickDate->format('Y-m-d'),"DateIn"=>$dropDate->format('Y-m-d'),"PickupTime"=>$_POST["tHourP"] . $_POST["tMinP"],"DropOffTime"=>$dropDate->format('H') . $dropDate->format('i'), "SubLocation"=>"0", "Duration"=>$_POST["nHours"], "PkgType"=>$_POST["chkPkgType"]));
    
    $myXML = new CORXMLList();
    $arr = $myXML->xml2ary($res);
    echo "<pre>";
    print_r($arr);
    $dayFare = array();
    $isAvail = array();
    foreach($arr as $k => $v)
    {
         $dayFare[$k] = $v['PkgRate'];
         $isAvail[$k] = $v['IsAvailable'];
    }
    array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
    for($i = 0; $i < count($arr); $i++)
    {
    $vat = $arr[$i]["VatRate"];
    $totAmount = $arr[$i]["BasicAmt"];
    $carModel = "";
    $carCatgName = "";
    if($arr[$i]["CarCatName"] == '4 Wheel Drive')
         $carCatgName = "SUV";
    else
         $carCatgName = $arr[$i]["CarCatName"];
    $carModel = $arr[$i]["model"];
    $secDeposit = $arr[$i]["DepositeAmt"];
    $CarDetail = "";
    $CarMake = "";
    if(intval($_POST["hdOriginID"]) != "69"){
         if(strtolower(trim($carModel)) == "toyota innova"){
              $CarDetail = trim($carModel) . " - GX";
              $CarMake = "<small>Make - 2012</small>";
         }
         else if(strtolower(trim($carModel)) == "maruti swift"){
              $CarDetail = trim($carModel) . " - VDI";
              $CarMake = "<small>Make - 2012</small>";
         }
         else if(strtolower(trim($carModel)) == "toyota etios"){
              $CarDetail = trim($carModel) . " - GD";
              $CarMake = "<small>Make - 2012</small>";
         }
         else if(strtolower(trim($carModel)) == "mahindra xuv-500"){
              $CarDetail = trim($carModel) . " - W6";
              $CarMake = "<small>Make - 2012</small>";
         }
         else if(strtolower(trim($carModel)) == "volkswagen polo" || strtolower(trim($carModel)) == "maruti ertiga" || strtolower(trim($carModel)) == "volkswagen vento" || strtolower(trim($carModel)) == "nissan sunny" || strtolower(trim($carModel)) == "ford endeavour" || strtolower(trim($carModel)) == "toyota fortuner"){
              $CarDetail = trim($carModel);
              $CarMake = "<small>Make - 2013</small>";
         }
         else if(strtolower(trim($carModel)) == "mahindra e2o"){
              $CarDetail = trim($carModel) . "<br />" . "<small><b>Electric Car</b></small>";
              $CarMake = "<small>Make - 2013</small>";
         }
         else
              $CarDetail = trim($carModel);
    } else {
          $CarDetail = trim($carModel);
    }
    if($isShow){
         if($carCatgName == "Super")
         $carCatgName = "Super-Luxury";
    }
?>
    ahdCarModel.push('<?php echo trim($carModel); ?>');
    ahdCarModelID.push('<?php echo $arr[$i]["ModelID"]; ?>');
    ahdOriginName.push('<?php echo strtolower($_POST["hdOriginName"]) ?>');
    ahdOriginID.push('<?php echo $_POST["hdOriginID"]?>');
    ahdDestinationName.push('<?php echo $_POST["hdDestinationName"]; ?>');
    ahdDestinationID.push('<?php echo $_POST["hdDestinationID"]?>');
    ahdCarID.push('<?php echo $arr[$i]["CarID"]; ?>');
    ahdCarCatID.push('<?php echo $arr[$i]["CarCatID"]; ?>');
    ahdPkgID.push('<?php echo $arr[$i]["pkgid"]; ?>');
    ahdCat.push('<?php echo $carCatgName; ?>');
    aPkgRate.push('<?php echo intval($arr[$i]["DayRate"]); ?>');
<?php
        if($_POST["chkPkgType"] == "Daily"){
?>
        aduration.push('<?php echo $interval; ?>');
   <?php
        } else {
   ?>
        aduration.push('<?php echo $_POST["nHours"]; ?>');
   <?php
        }
   ?>
    atotAmount.push('<?php echo $totAmount; ?>');
    atotFare.push('<?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?>');
    atab.push('2');
    
    ahdPickdate.push('<?php echo $pickDate->format('m/d/Y');?>');
    ahdDropdate.push('<?php echo $dropDate->format('m/d/Y'); ?>');
    
    ahdTourtype.push('<?php echo $_POST["hdTourtype"]?>');
    atHourP.push('<?php if(isset($_POST["tHourP"])){echo $_POST["tHourP"];} ?>');
    atMinP.push('<?php if(isset($_POST["tMinP"])){echo $_POST["tMinP"];} ?>');
    atHourD.push('<?php echo $dropDate->format('H'); ?>');
    atMinD.push('<?php echo $dropDate->format('i'); ?>');
    avat.push('<?php echo $vat; ?>');
    asecDeposit.push('<?php echo $secDeposit; ?>');
    asubLoc.push('');
    achkPkgType.push('<?php echo $_POST["chkPkgType"]?>');
    anHours.push('<?php echo $_POST["nHours"]?>');
    aOriginalAmt.push('<?php echo ceil($arr[$i]["OriginalAmt"]); ?>');
    aBasicAmt.push('<?php echo ceil($arr[$i]["BasicAmt"]); ?>');
    aallSubLoc.push('<?php echo $arr[$i]["DetailID"]; ?>');
    aKMIncluded.push('<?php echo $arr[$i]["KMIncluded"]; ?>');
    aExtraKMRate.push('<?php echo $arr[$i]["ExtraKMRate"]; ?>');
    aAvailableAt.push('<?php echo str_ireplace(':', ', ',(str_ireplace(',', ' ', $arr[$i]["Detail"]))) ?>');
    aSeatingCapacity.push('<?php echo $arr[$i]["SeatingCapacity"]; ?>');
<?php
    }
?>
_selectACar = function(id, bx)
{
    var blocks = document.getElementsByName('block');
    for(k = 0; k < blocks.length; k++)
    {
        blocks[k].className = 'show_allcar';
    }
    document.getElementById('block-' + bx).className = 'car_fix';
    document.getElementById('carbrandimage').src = "<?php echo corWebRoot; ?>/images-myles/" + ahdCarModel[id].replace(" ","-") + ".jpg";
    document.getElementById('carbrandname').innerHTML = "<br />" + ahdCarModel[id];
    if (achkPkgType[id] == "Daily") 
        document.getElementById('carbrandcost').innerHTML = "<br />&#8377;&nbsp;" + parseInt(atotAmount[id]) + "<br />";
    else
        document.getElementById('carbrandcost').innerHTML = "<br />&#8377;&nbsp;" + parseInt(atotAmount[id]) + "<br />";
    document.getElementById('availableat').innerHTML = "Available at: " + aAvailableAt[id];
    document.getElementById('capacity').innerHTML = "Seats: " + aSeatingCapacity[id];
    document.getElementById('hdCarModel').value = ahdCarModel[id];
    document.getElementById('hdCarModelID').value = ahdCarModelID[id];
    document.getElementById('hdOriginName').value = ahdOriginName[id];
    document.getElementById('hdOriginID').value = ahdOriginID[id];
    document.getElementById('hdDestinationName').value = ahdDestinationName[id];
    document.getElementById('hdDestinationID').value = ahdDestinationID[id];
    document.getElementById('hdCarID').value = ahdCarID[id];
    document.getElementById('hdCarCatID').value = ahdCarCatID[id];
    document.getElementById('hdPkgID').value = ahdPkgID[id];
    document.getElementById('hdCat').value = ahdCat[id];
    document.getElementById('PkgRate').value = aPkgRate[id];
    document.getElementById('duration').value = aduration[id];
    document.getElementById('totAmount').value = atotAmount[id];
    document.getElementById('totFare').value = atotFare[id];
    document.getElementById('tab').value = atab[id];
    document.getElementById('hdPickdate').value = ahdPickdate[id];
    document.getElementById('hdDropdate').value = ahdDropdate[id];
    document.getElementById('hdCarModel').value = ahdCarModel[id];
    document.getElementById('hdTourtype').value = ahdTourtype[id];
    document.getElementById('tHourP').value = atHourP[id];
    document.getElementById('tMinP').value = atMinP[id];
    document.getElementById('tHourD').value = atHourD[id];
    document.getElementById('tMinD').value = atMinD[id];
    document.getElementById('vat').value = avat[id];
    document.getElementById('secDeposit').value = asecDeposit[id];
    document.getElementById('subLoc').value = asubLoc[id];
    document.getElementById('chkPkgType').value = achkPkgType[id];
    document.getElementById('nHours').value = anHours[id];
    document.getElementById('OriginalAmt').value = aOriginalAmt[id];
    document.getElementById('BasicAmt').value = aBasicAmt[id];
    document.getElementById('allSubLoc').value = aallSubLoc[id];
    document.getElementById('KMIncluded').value = aKMIncluded[id];
    document.getElementById('ExtraKMRate').value = aExtraKMRate[id];
    document.getElementById('SeatingCapacity').value = aSeatingCapacity[id];
    document.getElementById('AvailableAt').value = aAvailableAt[id];
}
</script>

<form id="formBook" name="formBook" method="POST" action="./contact-myles.php">
    <input type="hidden" id="hdCarModel" name="hdCarModel" value="" />
		<input type="hidden" name="hdCarModelID" id="hdCarModelID" value="" />
		<input type="hidden" name="hdOriginName" id="hdOriginName" value="" />
		<input type="hidden" name="hdOriginID" id="hdOriginID" value=""/>
		<input type="hidden" name="hdDestinationName" id="hdDestinationName" value=""/>
		<input type="hidden" name="hdDestinationID" id="hdDestinationID" value=""/>
		<input type="hidden" name="hdCarID" id="hdCarID" value="" />
		<input type="hidden" name="hdCarCatID" id="hdCarCatID" value="" />
		<input type="hidden" name="hdPkgID" id="hdPkgID" value="" />
		<input type="hidden" name="hdCat" id="hdCat" value=""/>
		<input type="hidden" name="PkgRate" id="PkgRate" value="" />
		<input type="hidden" name="duration" id="duration" value="" />
		<input type="hidden" name="totAmount" id="totAmount" value="" />
		<input type="hidden" name="totFare" id="totFare" value="" />
		<input type="hidden" name="tab" id="tab" value="" />				
		<input type="hidden" name="hdPickdate" id="hdPickdate" value="" />
		<input type="hidden" name="hdDropdate" id="hdDropdate" value=""/>
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="" />
		<input type="hidden" name="tHourP" id="tHourP" value="" />
		<input type="hidden" name="tMinP" id="tMinP" value="" />
		<input type="hidden" name="tHourD" id="tHourD" value="" />
		<input type="hidden" name="tMinD" id="tMinD" value="" />
		<input type="hidden" name="vat" id="vat" value=""/>
		<input type="hidden" name="secDeposit" id="secDeposit" value=""/>
		<input type="hidden" name="subLoc" id="subLoc" value="" />
		<input type="hidden" name="chkPkgType" id="chkPkgType" value="" />
		<input type="hidden" name="nHours" id="nHours" value="" />
		<input type="hidden" name="OriginalAmt" id="OriginalAmt" value="" />
		<input type="hidden" name="BasicAmt" id="BasicAmt" value="" />
		<input type="hidden" name="allSubLoc" id="allSubLoc" value="" />
		<input type="hidden" name="KMIncluded" id="KMIncluded" value="" />
		<input type="hidden" name="ExtraKMRate" id="ExtraKMRate" value=""/>
                    <input type="hidden" name="SeatingCapacity" id="SeatingCapacity" value=""/>
                    <input type="hidden" name="AvailableAt" id="AvailableAt" value=""/>
                
                                <div class="image_border">
                                    <div class="down_car">
                                        <img id="carbrandimage" src="<?php echo corWebRoot; ?>/images-myles/car2.png"/>
                                    </div>
                                    <div class="secar_des">
                                        <span class="car_name fma"><span id="carbrandname">XUV 500<br/></span><span id="carbrandcost">&#8377; 2699/Day<br/></span><span id="capacity">Seats: </span></span><div class="cb"></div>
                                        <span class="des_text fma" id="availableat">Available at: Airport, Terminal - 1</span><div class="cb"></div>
                                        <input type="button" class="free_btn" id="free_detail" name="free_detail" value="Fare Details"/>
                                        <input type="button" id="btnBookNow" name="btnBookNow" onclick="javascript: this.form.submit();" class="right_btn" id="sel_btn" name="sel_btn" value="SELECT"/>
                                    </div>
                                </div>
                    </form>

                                <div class="white_backgraund mt10" id="allCars">
<?php
    $arrSL = array();
    for($i = 0; $i < count($arr); $i++)
    {
        $isSLF = 1;
		     $isShow = false;
		     if(!isset($_POST["onlyE2C"]) || $_POST["onlyE2C"] == 0)
			     $isShow = true;
		     else {
			if($arr[$i]["CarCatID"] == 21){
				$isShow = true;
			}
		     }
		     $btnid = 1;
		     $passCount = $arr[$i]["SeatingCapacity"];
		     $totAmount = $arr[$i]["BasicAmt"];
		     $vat = $arr[$i]["VatRate"];
		     $carModel = "";
		     $carCatgName = "";
		     if($arr[$i]["CarCatName"] == '4 Wheel Drive')
			$carCatgName = "SUV";
		     else
			$carCatgName = $arr[$i]["CarCatName"];
		     $carModel = $arr[$i]["model"];
		     $secDeposit = $arr[$i]["DepositeAmt"];
		     $CarDetail = "";
		     $CarMake = "";
		     if(intval($_POST["hdOriginID"]) != "69"){
			if(strtolower(trim($carModel)) == "toyota innova"){
			     $CarDetail = trim($carModel) . " - GX";
			     $CarMake = "<small>Make - 2012</small>";
			}
			else if(strtolower(trim($carModel)) == "maruti swift"){
			     $CarDetail = trim($carModel) . " - VDI";
			     $CarMake = "<small>Make - 2012</small>";
			}
			else if(strtolower(trim($carModel)) == "toyota etios"){
			     $CarDetail = trim($carModel) . " - GD";
			     $CarMake = "<small>Make - 2012</small>";
			}
			else if(strtolower(trim($carModel)) == "mahindra xuv-500"){
			     $CarDetail = trim($carModel) . " - W6";
			     $CarMake = "<small>Make - 2012</small>";
			}
			else if(strtolower(trim($carModel)) == "volkswagen polo" || strtolower(trim($carModel)) == "maruti ertiga" || strtolower(trim($carModel)) == "volkswagen vento" || strtolower(trim($carModel)) == "nissan sunny" || strtolower(trim($carModel)) == "ford endeavour" || strtolower(trim($carModel)) == "toyota fortuner"){
			     $CarDetail = trim($carModel);
			     $CarMake = "<small>Make - 2013</small>";
			}
			else if(strtolower(trim($carModel)) == "mahindra e2o"){
			     $CarDetail = trim($carModel) . "<br />" . "<small><b>Electric Car</b></small>";
			     $CarMake = "<small>Make - 2013</small>";
			}
			else
			     $CarDetail = trim($carModel);
		     } else {
			 $CarDetail = trim($carModel);
		     }
		     if($isShow){
			if($carCatgName == "Super")
			$carCatgName = "Super-Luxury";
		     }
?>
                                    <div class="main_car mr10">
                                        <div class="show_allcar" name="block" id="block-<?php echo $arr[$i]["pkgid"]; ?>" onclick="javascript: _selectACar(<?php echo $i; ?>,<?php echo $arr[$i]["pkgid"]; ?>)"><!--car_fix-->
                                            <a href="javascript:void(0)"><img width="100px" src="<?php echo corWebRoot; ?>/images-myles/<?php echo str_ireplace(" ", "-", trim($carModel)); ?>.jpg" alt="" /></a>
                                        </div>
                                        <?php
                                            if($_POST["chkPkgType"] == "Hourly"){
                                        ?>
                                        <span class="main_car_rate"></span>&#8377; <?php echo intval($arr[$i]["PkgRate"]); ?> &#x2044; Hour
                                        <?php
                                            } elseif($_POST["chkPkgType"] == "Daily"){
                                        ?>
                                        <span class="main_car_rate"></span>&#8377; <?php echo intval($arr[$i]["PkgRate"]); ?> &#x2044; Day
                                        <?php
                                            }
                                        ?>
                                    </div>
<?php
    }
?>
                                </div> <!--close white_backgraund div-->
                            </div> <!--close payment div-->
                        </div> <!--close booking div-->
                    </div> <!--close white_backgraund div-->
                </div> <!--close most_page div-->
                <?php
                    include_once("./includes/footer-myles.php");
                ?>
            </div> <!--close car_zon_main div-->
        </div> <!--close main div-->
    </div> <!--close body-->
    <script>
        _selectACar(0, <?php echo $arr[0]["pkgid"]; ?>);
    </script>
</body>
</html>
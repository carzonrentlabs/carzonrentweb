<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Car Rental Services We Offer at Carzonrent.com</title>
<meta name="description" content="Carzonrent(India) Pvt. Ltd. � Offer airport transfer, chauffeur & self-drive, limousine, operating lease, driving holidays and corporate travels services with quality services at affordable price." />
<meta name="keywords" content="Carzonrent services, airport transfer service, chauffeur & self-drive service, limousine services, operating lease services, Driving holidays service, corporate travels service." /> 
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder" style="margin-top: 20px">
		<div class="yellodivtxt">
			<h2>Services</h2>
		</div>
	</div>
	<div class="lightborder service">
		<div class="magindiv">
			<ul>
				<li><a href="javascript:void(0);" id="airp" class="active" onclick="javascript: _setActiveservice(this.id);">Airport Transfer</a></li>
				<li><a href="javascript:void(0);" id="chau"  onclick="javascript: _setActiveservice(this.id);">Chauffeur Drive</a></li>
				<li><a href="javascript:void(0);" id="self" onclick="javascript: _setActiveservice(this.id);">Self Drive</a></li>
				<li><a href="javascript:void(0);" id="limo" onclick="javascript: _setActiveservice(this.id);">Limousine</a></li>
				<li style="display:none;"><a href="javascript:void(0);" id="oper" onclick="javascript: _setActiveservice(this.id);"></a></li>
				<li><a href="javascript:void(0);" id="driv" onclick="javascript: _setActiveservice(this.id);">Driving Holidays</a></li>
				<li><a href="javascript:void(0);" id="corp" onclick="javascript: _setActiveservice(this.id);">Corporate Travels</a></li>
			</ul>
		</div>
		<div class="yellodivtxt">
			<img id="arrowmoment" width="18" height="8" src="./images/selecttbimg.png" border="0"/>
		</div>
	</div>
	<div class="divcenter mainW960">
	<div id="divairp" class="tabbbox" style="display:block">
		<div class="divtabtxt" style='width: 885px;'>
			<p>Airport Transfer - Some more words</p>
			<span class="txtcar">The key to an efficient Airport Transfer Service is timely and updated information so as to ensure all arrivals and departures are coordinated to the last minute. Our emphasis on technology and communication, which comes as part of the Carzonrent advantage, ensures timely services for both departure and arrival airport services. </span>
			<span class="txtcar">Furthermore Carzonrent has its hubs conveniently located near all major airport terminals in India thereby ensuring on-time delivery consistently. More importantly these locations also enable handling of last minute bookings.</span>
			<span class="txtcar">Needless to say you can hire a car at the airport from our complete fleet available for airport transfers - be it an Indica, Ford Ikon or the Mercedes Benz. Additionally should you require multiple transfers during a conference or a seminar, based on numbers, we could provide a dedicated resource to coordinate all movement.</span>
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	</div><!-- end of divairp-->
	<div id="divchau" class="tabbbox">
		<div style="float:left;width:885px;">
			<p>Chauffeur Drive</p>
			<span class="txtcar">Carzonrent's Chauffeur Drive service is a boon when you need superior transportation but don't want to do the driving. Our Airport Transfer Service is timely, also provides updated information (so you are awaited, or dropped off in good and correct time) and our stations at all the 4 major Indian airports offer you choice from a complete fleet - be it an Indica, Ford Ikon or the Mercedes Benz.</span>
			<span class="txtcar">Our local usage packages are flexible and allow you to use the car for any number of hours or kms. They come in 1hr/10km multiples and 4 hr/40km, 8hr/80 km, etc. Our trained chauffeurs carry mobile phones and ensure your ability to contact them by handing you their personal card so you never have to search or wait for your vehicle, wherever you take it.</span>
			<span class="txtcar">The Carzonrent "Zero Stress Drive" assurance is the reason enough to choose our Inter City Chauffeur Drive packages. Our drivers are professionally trained to handle the traffic on these intercity highways and are also well versed with getting around quickly. Whether you are availing car rental services in Delhi, Punjab, Rajasthan, Mumbai, Bangalore, Hyderabad or Chennai you can be assured of the Carzonrent quality and efficient service across the network.</span>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Features and Benefits</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="40px">
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Highly skilled chauffeurs</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Perfect choice for non-hassle drive</td>						
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Safety and comfort</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	</div><!-- end of divchau-->
	<div id="divself" class="tabbbox">
		<div style="float:left;width:885px;">
			<p>Self Drive</p>
			<span class="txtcar">When you need to drive on your own terms, nothing else beats the world-class Carzonrent Self Drive Service.After gaining thousands of satisfied customers world-over, we know how to deliver the finest of service. Our tariffs are also extremely reasonable: charges are on the car usage, on a per day 150 km basis. So if the freedom to drive combined with first-rate service and smart pricing is what you're looking for, then 'Carzonrent Self-Drive' is the way to go.</span>
			<div class="selfdivpanal">
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Services</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="40px">
					<tr>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>One Way Rental</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Weekly Rentals</td>
					</tr>
					<tr>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Intercity Rentals</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Monthly Rentals</td>
					</tr>
				</table>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Features and Benefits</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="50px">
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>No Hidden Costs</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Inter-city Rentals</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Independence</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Weekend Rentals</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Cost Effective</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Road Assistance</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Wide Range of Cars</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Accident Coverage</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Easy Processing</td>
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Rental Options</td>
					</tr>
				</table>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Self Drive for Business</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">When business comes calling, it can take you all around the country. And when you know a city well, there is no greater convenience than the control you have over travelling with Carzonrent Self Drive. Step in, choose your vehicle from our wide fleet, and drive away, knowing fully well that your vehicle has been maintained to a meticulous quality regimen and will ferry you all over flawlessly. Expect nothing short of the best from us.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Self Drive for Leisure</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Quality family time is a luxury these days... everyone yearns for that break when they can catch up on all good things with their family. With our self-drive service, all you have to do is make your plans. Drive off on a splendid family adventure. Enjoy the drive, knowing fully well that your vehicle will take you anywhere and never let you down. With a Carzonrent Self Drive you get a world of choice not just in cars but also in plush features such as TV, Stereo, Gaming etc. In fact, you don't really need to go out of town to treat your family to a great driving trip, justdrop in, pick a vehicle and drive to a good spot not too far, and enjoy the drive as much as the outing.</h4>
			</div>
		</div>
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	 </div><!-- end of divself-->
	 <div id="divlimo" class="tabbbox">
		<div class="divtabtxt" style="width: 885px;">
			<p>Limousine</p>
			<span class="txtcar">We know the value of making a good impression. Whether you want to make a important visitor feel special, or just book a car for a special evening with that special someone, we work closely with you to make it a memorable experience with the Carzonrent Limousine Drive.</span>
			<span class="txtcar">It's not just our car that makes the difference; our chauffeurs conduct your ride with a particular professionalism and greet you and your guests with our trademark care. So if luxury is the need and class is what you desire, Carzonrent's Limousine Drive is the answer.</span>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Features and Benefits</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="40px">
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Smartly groomed chauffeurs</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Personalised customer service</td>						
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Wide fleet of Basic, Premium and super luxury cars</td>
					</tr>
				</table>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Product Range</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="40px">
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Basic - Camry/ Mercedes C class</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Premium - Mercedes E class/ BMW - 5 series</td>						
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Super Luxury - BMW 7 series / Audi</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	</div><!-- end of limo-->
	<div id="divoper" class="tabbbox" style="width: 885px;">
		<div class="divtabtxt">
			<p>Operating Lease</p>
			<span class="txtcar">Our Operating Lease division caters to over 100 corporate clients across 12 cities.<br/>Apart from offering the most tax efficient structures, we assist clients with prudent cost budgeting, vehicle upgrade planning, and hassle free vehicle management along with cost efficient monthly payment structures.</span>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Features and Benefits</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="330px" height="40px">
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Vehicle Acquisition and Delivery</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Insurance Cover and Management</td>						
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Residual Value Management</td>
					</tr>
					<tr height="22px">
						<td><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Comprehensive Maintenance</td>
					</tr>
				</table>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Value Added Services</h4>
				<table style="margin:15px;"cellspacing="0" cellpadding="0" border="0" width="610px" height="40px">
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span><b>Uninterrupted Mobility:</b>The client's employee's vehicle is picked up and dropped in case of routine maintenance or/and breakdown repairs.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/></span><b>Accident Depreciation Waiver (ADW):</b>In case of any accidental damage or repair, Carzonrent would spare users from paying any outstanding repair bills.</td>					
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/></span><b>Total Loss Retention:</b>In case of any unforeseen circumstance resulting in total loss and/or theft, a predefined amount would be payable.</td>
					</tr>
					<tr height="40px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/></span><b>Tyre and Battery Maintenance:</b>This facility covers tyre and battery replacement extended up to providing wheel alignment and balancing.</td>
					</tr>
				</table>
			</div>
			<span class="txtcar">For more details on operating lease and fleet management services kindly write to <a href="mailto:reserve@carzonrent.com" class="txtmail">leaseinfo@carzonrent.com</a></span>
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	</div><!-- end of oper-->
	<div id="divdriv" class="tabbbox">
		<div class="divtabtxt" style="width: 100%;">
			<p>Driving Holidays - Some more words</p>
			<div class="selfdivp" style="margin-top:15px;width:98%">
				<h4 style="margin-top:5px;">Bangalore Tours</h4>
				<div style="float:left;width:136px;height:175px;background-color:#d0cfcf;margin:10px 10px 10px 0px;">
					
				</div>
				<h4 style="margin-top:8px;">Bangalore - Mysore - Bangalore</h4>
				<table style="margin:5px 0px 15px 10px;"cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/>Drive to Mysore. Visit the Mysore Palace, take a leisure trip in the city and the Musical fountain in the evening.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive back at you own time to Bangalore.</td>					
					</tr>
				</table>
				<h4 style="margin-top:5px;">Bangalore - Puttaparthy - Bangalore</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span>Leave for Puttaparthy. On arrival check into Hotel. Puttaparthy is the adobe of The Sathya Sai Baba, Popular for his miracles. Evening blessings and darshan of Sathya Sai Baba. Night Stay in Hotel.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Leave Puttaparthy for Bangalore</td>					
					</tr>
				</table>
			</div><!-- end of selfdivp -->
			<div class="selfdivp" style="margin-top:15px;width:98%">
				<h4 style="margin-top:5px;">Delhi Tours</h4>
				<div style="float:left;width:136px;height:175px;background-color:#d0cfcf;margin:10px 10px 10px 0px;">
					
				</div>
				<h4 style="margin-top:8px;">Delhi - Agra - Ranthambore - Delhi</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span>Drive from Delhi to Agra (203km/4Hrs), enroute viewing Sikandra. Check in at Agra, visit the Taj Mahal, Agra fort and local market.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Leave Agra for the Ranthambore (220Km/5-6Hrs) enroute visiting Fatehpur Sikri</td>		
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive back to Delhi.</td>		
					</tr>
				</table>
				<h4 style="margin-top:5px;">Delhi - Haridwar - Mussoorie - Delhi</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span>Leave for Haridwar (205Km/4-5Hrs) on arrival in Haridwar visit Har ki Pauri and the famous temples and later drive to Rishikesh, visit the famous Lakshman Jhula and the ashrams and in the ensuring drive to Mussoorie. Night stay in Mussoorie.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Visit to the Kempty falls and a leisure walk at the malls.</td>					
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive back to Delhi (305Km/7-8Hrs).</td>
					</tr>
				</table>
			</div><!-- end of selfdivp -->
			<div class="selfdivp" style="margin-top:15px;width:98%">
				<h4 style="margin-top:5px;">Hyderabad Tours</h4>
				<div style="float:left;width:136px;height:175px;background-color:#d0cfcf;margin:10px 10px 10px 0px;">
					
				</div>
				<h4 style="margin-top:8px;">Hyderabad - Srisailam - Hyderabad</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/>Drive to Serene Srisailam, 232 Km south of Hyderabad- situated on the bank of the river Krishna.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Spend the day exploring the 512-meter long Srisailam Dam located here which is surrounded by natural beauty.</td>					
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive back to Hyderabad visiting Pathalaganga, Paladhara, Panchadhara and Shikaram.</td>
					</tr>
				</table>
				<h4 style="margin-top:5px;">Hyderabad - Warangal - Hyderabad</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" style="margin-right:2px;line-height:25px;" width="7" height="12" border="0"/>Located at a distance of 150 kilometers from the capital city of Hyderabad, reach Warangal with ease.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/></span>Replete with azure lakes, wild life sanctuaries, the city is also known for its rich and historic architecture that is well articulated through its fine monuments, forts, temples and the recent feathers on its cap-the rock gardens.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive Back to Hyderabad.</td>
					</tr>
				</table>
			</div><!-- end of selfdivp -->
			<div class="selfdivp" style="margin-top:15px;width:98%">
				<h4 style="margin-top:5px;">Mumbai Tours</h4>
				<div style="float:left;width:136px;height:175px;background-color:#d0cfcf;margin:10px 10px 10px 0px;">
					
				</div>
				<h4 style="margin-top:8px;">BanMumbai - Matheran - Mumbai</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span>Leave for Matheran. Matheran is a fascinating hill-station 80 Km East of Mumbai in Alibag district, is situated at an altitude of 800m. Spend the night at your hotel.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>At Matheran, you may like to visit Alexander Point, Charlotte Lake, Echo Point, Panorma Point, Night stay at your hotel.</td>					
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Return to Mumbai.</td>					
					</tr>
				</table>
				<h4 style="margin-top:5px;">Mumbai - Lonavla / Khandala - Pune - Mumbai</h4>
				<table style="margin:5px 0px 15px 10px; "cellspacing="0" cellpadding="0" border="0" width="700px" height="40px">
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;line-height:25px;" border="0"/></span>Leave for Lonavla. Lonavla and Khandla are two charming little hill stations on the western slopes of the Sahyadris, 5 Km apart, that straddle the Mumbai - Pune highway at an altitude of 625m, 101 Kms from Mumbai, quite popular as health resorts.</td>
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><span style="float:left;height:30px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/></span>Leave for Pune and on arrival check into the Hotel. Afternoon city tour of Pune includes visit to Ashram of OSHO. Night stay in hotel.</td>	
					</tr>
					<tr height="22px">
						<td style="line-height:18px;"><img src="./images/arrow2.png" width="7" height="12" style="margin-right:2px;" border="0"/>Drive back to Mumbai.</td>	
					</tr>
				</table>
			</div><!-- end of selfdivp -->
		</div><!-- end of divtabtxt -->
	</div><!-- end of driv-->	
	<div id="divcorp" class="tabbbox">
		<div class="divtabtxt" style="width:885px;">
			<p>Corporate Travel</p>
			<span class="txtcar" style="font-weight:bold;margin-top:15px;">Our Corporate Travel product has been designed for the discerning taste of its clients where they get the best in Class standards of service and impeccable quality. </span>
			<span class="txtcar">For the business travelers time is money and we understand that. It is our aim to make business travel simple, hassle free and cost effective. We understand today's competitive business environment that puts a premium on value and high quality service delivered at a reasonable price</span>
			<span class="txtcar">For partnering with us for your corporate travel please get in touch with<br/>Landline: 91-11-43083000<br />E-mail:
			<a href="mailto:reserve@carzonrent.com" class="txtmail">reserve@carzonrent.com</a>
			</span>			
		</div>
		<div class="imagediv">
			<img src="./images/carfront.png" border="0" width="228" height="296"/>
		</div>
	</div><!-- end of corp-->
	</div><!-- end of divcenter -->
</div>
<div id="backgroundPopup"></div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

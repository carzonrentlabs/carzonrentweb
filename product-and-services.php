<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/tabbing.js"></script>
<script type="text/javascript" src="js/jquery.tool.min.js" language="javascript"></script>
<script type="text/javascript" src="js/popup1.js" language="javascript"></script>
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder">
		<div class="yellodivtxt" style="padding:7px 0px 7px 0px;">
			<h2>Coproate Leasing</h2>
		</div>
	</div>
	<div class="lightborder">
		<div class="magindiv">
			<ul>
				<li><a href="javascript:void(0);" id="servi" class="active" onclick="javascript: _setActiveoffering(this.id);">Service & Product Offering</a></li>
				<li><a href="javascript:void(0);" id="value"  onclick="javascript: _setActiveoffering(this.id);">Value Added Services</a></li>
			</ul>
		</div>
		<div class="yellodivtxt">
			<img id="arrowmoment" src="./images/selecttbimg.png" border="0"/>
		</div>
	</div>
	<div class="divcenter">
	<div id="divservi" class="tabbbox" style="display:block">
		<div class="divtabtxt">
			<div class="selfdivp" style="margin-top:35px;">
				<h4 style="margin-top:5px;">Vehicle Acquisition and Delivery</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">
				The car would be leased as per the requirements of the client's employees. We take care of vehicle price quotes, ordering, purchasing, registration, insurance, taxes, fitting accessories etc. , thereby ensuring a hassle free experiance.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Insurance Cover and Management</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">We ensure comprehensive insurance cover for the entire period of the contract. Insurance management includes claim filing, claim processing and ensuring delivery after repair of the vehicle.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Residual Value Management</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">At the start of the leasing process, we commit a resale price applicable at the end of the tenure. Thus taking the responsibility of sale of old cars. This also provides you cover from the risk of changing resale prices. Residual management can be customized to accommodate varied requirements.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Comprehensive Maintenance</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Carzonrent lease will take care of the maintenance of the fleet throughout the contract life-cycle. We undertake periodic maintenance schedules as defined by vehicle manufacturer that cover all kinds of breakdown repairs and mechanical wear and tear. Our expertise in fleet management and multi city garage tie-ups ensure timely and cost effective vehicle maintenance services.</h4>
			</div>
		</div>
		<div class="imagediv">
			<img src="./images/blackboy.png" border="0" width="228px" height="296px"/>
		</div>
	</div><!-- end of divairp-->
	<div id="divvalue" class="tabbbox">
		<div class="divtabtxt">
			<div class="selfdivp" style="margin-top:35px;">
				<h4 style="margin-top:5px;">Uninterrupted Mobility</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">Carzonrent would get vehicles picked up and dropped back for routine maintenance and breakdown repairs. Along with this exclusive facility, we also provide replacement vehicles to ensure uninterrupted mobility.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Accident Depreciation Waiver (ADW)</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">In case of an accidental damage, Carzonrent would exempt clients from paying any amount linked to repair bill not accounted for by the insurance company. Under ADW, the same gets covered and clients can get their vehicles easily from the garage without paying any amount.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Total Loss Retention</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">In case of any unforeseen circumstances resulting in total loss and theft, an agreed amount would be payable, depending on the make and the model of the vehicle.</h4>
			</div>
			<div class="selfdivp">
				<h4 style="margin-top:5px;">Tyre and Battery Maintenance</h4>
				<h4 style="margin-top:5px;font-weight:normal;line-height:19px">This facility covers tyre and battery replacement and also includes wheel alignment and balancing as per manufacturer guideline.</h4>
			</div>
		</div>
		<div class="imagediv">
			<img src="./images/blackboy.png" border="0" width="228px" height="296px"/>
		</div>
	</div><!-- end of divchau-->	
	</div><!-- end of divcenter -->
</div>
<div id="backgroundPopup"></div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	//$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	//$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrent Pvt. Ltd</title>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/404.css" />
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js"></script>
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>	
<!--Header Start Here-->
<?php include_once("./includes/header-with-service.php"); ?>
<div class="banner">
<div class="error-main">
    <div class="heading-text">404! We couldn't find the page!</div>
    <div class="oops">Oops... That was a bad breakdown</div>
    <div class="main-text">The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable. Try returning to Home Page and findt he page you were looking for. </div>
    <div class="curve-img">Visit some of our working pages</div>
	<div class="my-links">
		<div style="float:left">
			<ul>
				<li><a href="http://www.carzonrent.com/">Home</a></li>
				<li><a href="http://www.carzonrent.com/aboutus.php">About Us</li>
				<li><a href="http://www.carzonrent.com/#outstation">Outstation</a></li>
				<li><a href="http://www.carzonrent.com/#local">Local</a></li>
			</ul>
		</div>
		<div style="float:left">
			 <ul>
				 <li><a href="http://www.carzonrent.com/#selfdrive">Self Drive</a></li>
				 <li><a href="http://www.carzonrent.com/contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<div style="clear:both"></div>
		<div style="float:left; margin:-300px 0 0 478px"><img src="<?php echo corWebRoot; ?>/images/car-repai.gif" /></div>
		<div style="clear:both"></div>
	</div>
</div>
</div>
<?php include_once("./includes/footer.php"); ?>
<!--footer Ends Here-->
</body>
</html>

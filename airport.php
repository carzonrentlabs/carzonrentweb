<?php
error_reporting(0);
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if (isset($_GET["gclid"]) && $_GET["gclid"] != "")
    setcookie('gclid', $_GET["gclid"], time() + 3600 * 24, "/");

include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
include_once("./includes/cache-func.php");
include_once('./classes/cor.mysql.class.php');
setcookie("tcciid", "", time() - 60);
$cor = new COR();
$res = $cor->_CORGetAirportLocation(array("ClientID"=>2205));
$myXML = new CORXMLList();
$org = $myXML->xml2ary($res);
$orgH = $org;
$orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
$orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
$orgH[] = array("CityID" => 39, "CityName" => "Manesar");
$orgH[] = array("CityID" => 41, "CityName" => "Bhubaneswar");
$res = $cor->_CORGetDestinations();
$des = $myXML->xml2ary($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include_once("./includes/header-css.php"); ?>
        <?php include_once("./includes/header-js-no-ga.php"); ?>
        <script type="text/javascript">
            $(function () {
                $('select.styled').customSelect();
                $('#slidesLocal').slides({
                    preload: false,
                    preloadImage: '/images/loader.gif',
                    play: 5000,
                    pause: 2500,
                    hoverPause: true,
                    animationStart: function (current) {
                        $('.caption').animate({
                            bottom: -35
                        }, 100);
                    },
                    animationComplete: function (current) {
                        $('.caption').animate({
                            bottom: 0
                        }, 200);
                    },
                    slidesLoaded: function () {
                        $('.caption').animate({
                            bottom: 0
                        }, 200);
                    }
                });
            });
            if (typeof String.prototype.startsWith != 'function') {
                String.prototype.startsWith = function (str) {
                    return this.indexOf(str) == 0;
                };
            }
        </script>

        <!--  For Calender -->
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                var dateToday = new Date();
                dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: dateToday});
            });
        </script>
    </head>
    <body>
        <script type="text/javascript">var _kiq = _kiq || [];</script>
        <script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js"></script>
        <?php
        if (isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != "") {
            ?>
            <script type="text/javascript" charset="utf-8">
                _kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
            </script>
            <?php
        }
        ?>   	
        <div class="banner airport">
            <div class="lfts">

                <div class="book">Book a Cab for Airport Transfer</div>
                <div id="TripleTabs">
                    <div id="localFrm" class="tber">
                        <input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y'); ?>" />
                        <form id="form2" name="form2" action="<?php echo corWebRoot; ?>/search-result.php" method="post" onsubmit="javascript: return _validateAirport();" target="_top">
                            <input type="hidden" name="hdOriginName" id="hdOriginNameL" />
                            <input type="hidden" name="hdOriginID" id="hdOriginIDL" />

                            <input type="hidden" name="picuploactionName" id="picuploactionName" />
                            <input type="hidden" name="picuploactionid" id="picuploactionid" />

                            <input type="hidden" name="hdDestinationName" id="hdDestinationNameL" />
                            <input type="hidden" name="hdDestinationID" id="hdDestinationIDL" />
                            <input type="hidden" name="hdTourtype" id="hdTourtypeL" value="airport" />
                            <input type="hidden" name="bookTime" id="bookTime" value="<?php echo date('h:i A'); ?>" />
                            <input type="hidden" name="userTime" id="userTime" value="" />
                            <input type="hidden" name="chkPkgType" id="chkPkgType" value="Hourly" />
                            <input type="hidden" id='cabid' name="cabid" value="">
							<input type="hidden" name="refclient" id="refclient" value="<?php echo $_SESSION['refclient']; ?>" />


                                <div class="bdrbtmL">


                                    <p>
                                        <label>Cab Purpose</label>
                                        <span class="">
                                            <select class="" name="airportddlOrigin" id="airportddlOrigin" onchange="_setcabpurpose(this)">
                                                <option value="0">Select Purpose</option>
                                                <option value="1">Airport pickup</option>
                                                <option value="2">Airport drop off</option>
                                            </select>
                                    </p>



                                    <p>
                                        <label>City</label>
                                        <span class="">
                                            <select class="" name="ddlOrigin" id="ddlOriginL"  onchange="javascript:  _setairportOrigin(this);">
                                                <option>Select City</option>
                                                <?php
                                                $selCityId = 2;
                                                $selCityName = "Delhi";
                                                for ($i = 0; $i < count($org); $i++) {
                                                    if ($org[$i]["CityName"] == $cCity) {
                                                        $selCityId = $org[$i]["CityID"];
                                                        $selCityName = $org[$i]["CityName"];
                                                        ?>
                                                        <option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
                                                        <script type="text/javascript" language="javascript">
                                                            document.getElementById('hdOriginIDL').value = '<?php echo $org[$i]["CityID"]; ?>';
                                                            document.getElementById('hdOriginNameL').value = '<?php echo $org[$i]["CityName"]; ?>';
                                                        </script>
        <?php
    } else {
        ?>
                                                        <option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select><span id='loader5'></span>
                                        </span>
                                    </p>

                                    <p>

                                        <label>Pickup location</label>
                                        <span class="">

                                            <select class="" name="ddltermainal" id="ddltermainal" onchange="_settrevelRecord(this);">
                                                <option value="0">Select pickup location</option>
                                            </select><span id='loader6'></span>

                                            <script type="text/javascript" language="javascript">
                                                document.getElementById('picuploactionid').value = '<?php echo $valrecord["CityID"]; ?>';
                                                document.getElementById('picuploactionName').value = '<?php echo $valrecord["CityName"]; ?>';
                                            </script>



                                        </span>


                                    </p>	


                                    <p>
                                        <label>Travelling to</label>
                                        <span class="">
                                            <select class="" name="travelorigen" id="travelorigenL"  onchange="_setTravellingTo(this);">
                                                <option value="0">Select travel location</option>

                                            </select>
                                        </span>
                                    </p>




<!--<p>
<label>I want a cab for</label>
<span class="cabhours"><select class="styled wdth145" style="width:200px;" name="cabHour" id="cabHour" onchange="javascript: _setHour(this.value);">
<?php
for ($i = 4; $i <= 20; $i++) {
    ?>
    <option value="<?php echo $i; ?>"><?php echo $i; ?> hours</option>
                                        <?php
                                    }
                                    ?>
</select></span>
<span id="selCH" style="margin-left:6px;float:left;" class="showhour"><span id="selCabHour">40 kms included</span></span>
</p>-->
                                    <p>
                                        <label>Pickup Date</label>
                                        <span class="datepick">
                                            <input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField5" class="datepicker" />
                                        </span>
                                    </p>
                                    <p>
                                        <label>Pickup Time </label>
                                        <span class="pickuphour">
                                            <select class="styled wdth60" name="tHour" id="tHour" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
                                                <!--<option>Hours</option> -->
<?php for ($i = 0; $i <= 23; $i++) {
    $sel = "";
    if ($i == 10) {
        $sel = "selected='selected'";
    } if ($i < 10) {
        $t = "0" . $i;
    } else {
        $t = $i;
    } ?>
                                                    <option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
<?php } ?>
                                            </select>
                                        </span>
                                        <span class="pickupmin ml5">
                                            <select class="styled wdth60" name="tMin" id="tMin" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
                                             
                                                <option value="00">00</option> 
                                                <option value="15">15</option> 
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                            </select>
                                        </span>
                                        <span id="selT" style="margin-left:6px" class="showtime"><span id="seltime"></span></span>
                                        <script type="text/javascript">
                                            _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');
                                        </script>
                                    </p>
                                </div>

                                <p>
                                    <label>&nbsp;</label>
                                    <span class="mkebtn" style="float:left;">
                                        <!--<a href="javascript: void(0);">Make Your Booking Now</a>-->
                                        <input type="submit" id="btnMakePaymentL" name="btnMakePayment" value="Make Your Booking Now" />
                                    </span>
                                </p>
                        </form>  
                    </div>
                </div>

                <!--TripleTabs Ends Here-->
            </div>
            <div class="rgts">
			<!--	<div class="righttb">
	  <a href="http://www.mylescars.com/landing/First_time/tnc?utm_source=Carzonrent&utm_medium=Banner&utm_campaign=carzonrent-banner-700off" target="_blank"><img src="<?php echo corWebRoot; ?>/images/Flat-700off-Self-Drive-Car-Cor.jpg" alt="<?php echo $imageval['alt_desc'];?>" title="<?php echo $imageval['img_title'];?>" width="346px" height="80px" /></a>		
	</div>-->
                <div class="righttb">
                    <div id="slidesLocal">
                        <div class="slides_container" style="height: 390px;">		
                            <?php
                            $db = new MySqlConnection(CONNSTRING);
                            $db->open();
                            $sql_slider = "select * from cor_local_banner where banner_type='local' and status='1' ORDER BY `orderimage` ASC";
                            $slider = $db->query("query", $sql_slider);
                            $db->close();
                            foreach ($slider as $imageval) {
                                ?>			
                                <div class="slide">
                                    <a href="<?php echo $imageval['link_url']; ?>" target="_blank"><img src="<?php echo corWebRoot; ?>/images/<?php echo $imageval['image']; ?>" alt="<?php echo $imageval['alt_desc']; ?>" title="<?php echo $imageval['img_title']; ?>" width="346px" height="392px" /></a>
                                </div>
<?PHP } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".custom-select").each(function () {
                    $(this).wrap("<span class='select-wrapper choseF'></span>");
                    $(this).after("<span class='holder choseF'></span>");
                });
                $(".custom-select").change(function () {
                    var selectedOption = $(this).find(":selected").text();
                    $(this).next(".holder").text(selectedOption);
                }).trigger('change');
            })
        </script>
    </body>
</html>

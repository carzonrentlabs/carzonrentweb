<?php
	error_reporting(0);
	
	if(isset($_REQUEST['cityId']) && $_REQUEST['cityId'] != '')
	{
		$CityID=$_REQUEST['cityId'];
	}else{	
		$CityID = '2';
	}
	include_once('classes/cor.ws.class.php');
	include_once('classes/cor.xmlparser.class.php');
	include_once('includes/cache-func.php');
	include_once('classes/cor.mysql.class.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Myles Locations</title>

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<!--Header Start Here-->
<?php include_once("includes/header-with-service.php"); ?>

<div id="middle" class="cor_map">
	<div class="city-bg">
	<br>
	<h1>Myles Locations</h1>
				<ul>
					<li>
						<?php
							if($CityID == 2){
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=2" class="selected">Delhi</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=2" >Delhi</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 4){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=4" class="selected">Mumbai</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=4">Mumbai</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 7){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=7" class="selected">Bangalore</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=7">Bangalore</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
							if($CityID == 6){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=6" class="selected">Hyderabad</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=6">Hyderabad</a>
						<?php
							}
						?>
						
					</li>
					
					<li>
						<?php
							if($CityID == 69){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=69" class="selected">Goa</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=69">Goa</a>
						<?php
							}
						?>
						
					</li>
					
					<li>
						<?php
							if($CityID == 10){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=10" class="selected">Jaipur</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=10">Jaipur</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 8){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=8" class="selected">Chennai</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=8">Chennai</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 5){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=5" class="selected">Pune</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=5">Pune</a>
						<?php
							}
						?>	
					</li>
					
					<li>
						<?php
						if($CityID == 1){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=1" class="selected">Ahmedabad</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=1">Ahmedabad</a>
						<?php
							}
						?>	
					</li>
					
					<li>
						<?php
						if($CityID == 9){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=9" class="selected">Chandigarh</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=9">Chandigarh</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 49){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=49" class="selected">Visakhapatnam</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=49">Visakhapatnam</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
						if($CityID == 43){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=43" class="selected">Amritsar</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=43">Amritsar</a>
						<?php
							}
						?>
						
					</li>
				
					<li>
						<?php
						if($CityID == 45){
					?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=45" class="selected">Coimbatore</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=45">Coimbatore</a>
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
							if($CityID == 41){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=41" class="selected">Bhubaneswar</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=41">Bhubaneswar</a>
						<?php
							}
						?>
						
					</li>
					
					<li>
						<?php
							if($CityID == 11){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=11" class="selected">Noida</a>
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=11">Noida</a>
						<?php
							}
						?>
						
					</li>
					
					<li>
						<?php
							if($CityID == 3){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=3" class="selected">Gurgaon</a> 
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=3">Gurgaon</a>
						<?php
							}
						?>
						
					</li>
					
					<li>
						<?php
							if($CityID == 80){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=80" class="selected">Mangalore</a> 
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=80">Mangalore</a>
						<?php
							}
						?>
						
					</li>
					<li>
						<?php
							if($CityID == 81){
						?>
						<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=81" class="selected">Mysore</a> 
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=81">Mysore</a> 
						<?php
							}
						?>
						
					</li>
					<li>
						<?php
							if($CityID == 44){
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=44" class="selected">Udaipur</a> 
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=44">Udaipur</a> 
						<?php
							}
						?>
					</li>
					
					<li>
						<?php
							if($CityID == 82){
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=82" class="selected">Ghaziabad</a> 
						<?php	
							}else{
						?>
							<a href="<?php echo corWebRoot; ?>/cor_map.php?cityId=82">Ghaziabad</a> 
						<?php
							}
						?>
					</li>
					
				</ul>
			
			<div class="clr"> </div>
			<br/>
			<?php 

			include("map/citywise.php");

			?>
			<br/>
	</div>
</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once('./classes/cor.mysql.class.php');
	$rurl = corWebRoot;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Print Online Your Car Rental Invoice.</title>
<meta name="description" content="Print online your car rental receipt or car rental invoice by simply enter your mobile number and COR id to print your invoice at Carzonrent." />
<meta name="keywords" content="car rental receipt, rental receipt, car rent receipt, car rental invoice, print car rental invoice." />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder" style="margin-top: 20px;">
		<div class="magindiv" style="padding:7px 0px 7px 0px;width: 1130px;">
			<h2>Print Invoice</h2>
		</div>
	</div>	
	<div style="margin:0 auto;width:968px; margin-top:10px;">
			<div class="divtabtxt">
			
			<div class="businessEnquiry"  style="margin:49px -249px 40px -40px">
			<form action="send-business-enquiries.php" method="post" name="frmBusinessEnquiries" id="frmBusinessEnquiries">
				<table width="100%" cellpadding="0" cellspacing="10" border="0" class="border-radius">
				
				
					<tr>
					<td align="left" valign="middle" colspan="2">	
					<span class="regi_l"><a href="<?php echo $rurl ?>"><img src="<?php echo $rurl; ?>/images/512_512_logo.png" width="50" height="50" alt="We take You Places" title="We take You Places"/></a>
					</span>
					<span class="regi_r">
					<span class="heading" style="font-size:16px;">Enter details recevied at time of booking.</span>
					</span>
					</td>
					</tr>
					<tr>
						<td colspan="2" valign="middle" align="center"><span id="msg"></span></td>

					</tr>
					 <tr>
						<td align="right" valign="middle"><label>Mobile No</label></td>
						<td align="left" valign="top" ><input type="text" name="txtMobile" maxlength="10" id="txtMobile" onKeyPress="javascript: return _allowNumeric(event);" value="<?php echo $mobile; ?>" /></td>
					</tr>
					<tr>
						<td align="right" valign="middle"><label>Booking Id</label></td>
						<td align="left" valign="top" ><input type="text" name="txtBookingID" maxlength="50" id="txtBookingID" value="<?php echo $bookID; ?>" /></td>
					</tr>
<!--<tr>
						<td align="right" valign="middle"><label>Tour Type</label></td>
                                                <td align="left" valign="top" ><select class="service_type" name="tourtype" id="tourtype"><option value="2">Local/Outstation</option></select></td>
					</tr> --> 
					<tr>
						<td align="right" valign="middle"></td>
						<td align="left" valign="top" ><div class="booking" style="width:154px;"><a href="javascript:void(0)" onclick="javascript: _checkInvoice();"></a></div></td>
					</tr>    
				</table>
			</form>
			</div>
		</div>
	</div>	
</div>
<script type="text/javascript">
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<style>
.clear {
                clear: both;
            }
            #map_canvas {
                float: left;
                height: 500px;
                width: 500px;
            }
            #directionsPanel {
                float: left;
                width: 300px;
            }
			</style>


    <html>
    <head>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
		
     </head>
    <body>
        <form id="routeForm">
            <label for="routeStart">GET DIRECTIONS FROM</label><input type="text" id="routeStart" name="routeStart">
            <label for="routeEnd"></label><input type="hidden" id="routeEnd" name="routeEnd" value="<?php echo $_REQUEST['routeEnd']; ?>">
            <input type="submit" value="Find Route">
        <form>
        <div class="clear"></div>
        <div id="map_canvas">
		
		<iframe src="https://mapsengine.google.com/map/embed?mid=zROvKwPe-PNM.kzxvNPJLWrgg" width="640" height="480"></iframe>
		</div>
        <div id="directionsPanel"></div>
    </body>
</html>
			
			
			<script>



var map, directionsService, directionsDisplay, geocoder, startLatlng, endLatlng, routeStart, routeEnd, markers = [];

function initialize() {
    var latlng = new google.maps.LatLng(51.764696, 5.526042);
    routeStart = document.getElementById('routeStart');
    routeEnd = document.getElementById('routeEnd');
    geocoder = new google.maps.Geocoder();
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer({
        animation: google.maps.Animation.DROP,
        draggable: true
    });
    var myOptions = {
        zoom: 14,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("directionsPanel"));

    var form = document.getElementById("routeForm");
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        var start = this.elements["routeStart"].value;
        var end = this.elements["routeEnd"].value;

        if (start.length && end.length) {
            calcRoute(start, end);
        }
    });

    google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
        var directions = this.getDirections();
        var overview_path = directions.routes[0].overview_path;
        var startingPoint = overview_path[0];
        var destination = overview_path[overview_path.length - 1];
        if (typeof startLatlng === 'undefined' || !startingPoint.equals(startLatlng)) {
            startLatlng = startingPoint;
            getLocationName(startingPoint, function(name) {
                routeStart.value = name;
            });
        }
        if (typeof endLatlng === 'undefined' || !destination.equals(endLatlng)) {
            endLatlng = destination;
            getLocationName(destination, function(name) {
                routeEnd.value = name;
            });
        }
    });
}

function getLocationName(latlng, callback) {
    geocoder.geocode({
        location: latlng
    }, function(result, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var i = -1;
            console.log(result);
            // find the array index of the last object with the locality type
            for (var c = 0; c < result.length; c++) {
                for (var t = 0; t < result[c].types.length; t++) {
                    if (result[c].types[t].search('locality') > -1) {
                        i = c;
                    }
                }
            }
            var locationName = result[i].address_components[0].long_name;
            callback(locationName);
        }
    });
}

function calcRoute(start, end) {
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}

window.onLoad = initialize();
</script>




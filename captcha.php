<?php
    session_start();
    // Create a 100*30 image
    $rNum = rand();
    $_SESSION["capcha"] = $rNum;
    $im = imagecreate(70, 50);
    
    // White background and blue text
    $bg = imagecolorallocate($im, 255, 255, 255);
    $textcolor = imagecolorallocate($im, 255, 0, 0);
    
    // Write the string at the top left
    imagestring($im, 10, 0, 0, $rNum, $textcolor);
    
    // Output the image
    header('Content-type: image/jpeg');
    
    imagejpeg($im);
    imagedestroy($im);
?>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 6500 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<link rel="stylesheet" type="text/css" href="http://www.carzonrent.com/referralProgram/css/ReferralProgram.css" />
<?php
	include_once("./includes/header-css.php");
	include_once("./includes/header-js.php");
?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<style>
.mylesResolu{
width:1095px;
margin:20px auto;
padding:10px;
position:relative;
left:-5px;
background-color:#E2E2E2;
border:1px solid #D8D8D8;
}
.clr{clear:both;}
.term_of_use{
width:1130px;
margin:0 auto;
}
#footer{margin-top:40px;}
.w31 img {border:1px solid #efefef;}
.show2, .show3{
cursor:pointer;
margin-top:20px;
}
#pop2.t_condition h3, #pop3.t_condition h3{padding-bottom:5px;}
.t_condition p, ul li{font-size:13px;}
ul li{line-height:18px;}
.t_condition{width:95.5%;}
#pop2.t_condition, #pop3.t_condition{margin-bottom:0px;width:95.9%;}
</style>
<script>
$(document).ready(function(){
    $(".termsCod").click(function(){
		$("#pop2").slideUp();
		$("#pop3").slideUp();
        $(".t_condition.tc").slideToggle();
    });
	
	$('.show2').click(function(){
		$("#pop3").slideUp();
		$(".t_condition.tc").slideUp();
		$("#pop2").slideDown();
		$('#termsCod').ScrollTo();
    });
  
    $('.show3').click(function(){
		$("#pop2").slideUp();
		$(".t_condition.tc").slideUp();
		$("#pop3").slideDown();
		$('#termsCod').ScrollTo();
    });  
	
});
</script>
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div class="clr"> </div>
<div class="mylesResolu">
<img width="1094" height="304" src="http://www.carzonrent.com/myles-campaign/images/9587080184.jpeg" alt="MylesResolution_Offers" title="MylesResolution_Offers">
</div>
<div class="term_of_use">
<h3 style="padding-bottom:5px;">Offer – Get a discount on Myles self-drive and amazing partner offers when you book a car with us.</h3>
<ul class="mp0" style="margin:10px 0 0px 20px;">
<li>Use code <strong>MJANRES500</strong> and get <strong>Rs. 500 off</strong> on your booking.</li>
<li>Use code <strong>MJANRES10</strong> and get <strong>10% off</strong> on your booking.</li>
</ul>
<div class="f_r">
<div id="termsCod" class="termsCod" style="margin-top:0px;">
More details
</div>
<div class="clr"> </div>
</div>
<div class="t_condition tc">
<h3>Process</h3>
<ul>
<li>Book a Myles car between 1<sup>st</sup> January and 15<sup>th</sup> January 2016, applicable on pick up and drops made between 5<sup>th</sup> January and 31<sup>st</sup> January 2016</li>
<li>Use code – <strong>MJANRES500</strong> to get <strong>Rs 500 off</strong> on daily, weekly and monthly bookings or <strong>MJANRES10</strong> to get <strong>10% off</strong> on hourly, daily, weekly and monthly bookings.</li>
<li>Once the booking is made, you get a mail with all the offers.</li>
<li>Click on the offer you like the most. Each offer will redirect you to the offer landing page where special codes will be provided for redemption.</li>
<li>Read the terms and conditions before availing any offer.</li>
<li>Select one or chose them all – Happy New Year !</li>
</ul>
<h3>Terms and Conditions:</h3>
<ul>
<li>Flat Rs 500 off applicable only on daily, weekly and monthly bookings.</li>
<li>Flat 10% off applicable only on hourly daily, weekly and monthly bookings.</li>
<li>Extra offers from our partners on every successful booking.</li>
<li>Offer not applicable in case the booking is cancelled.</li>
<li>Code applicable on bookings made between 5<sup>th</sup> January and 15 January 2016.</li>
<li>Code applicable on picks-up and drops made from 5<sup>th</sup> January and 31<sup>st</sup> January 2016.</li>
<li>The discount amount is applicable on the base fare only.</li>
<li>As a mandate, Rs. 1 to be charged at the time of making the booking in-case the discount amount is equal or less than the booking amount.</li>
<li>Standing instructions/ pre-authorization will be made on the credit card based on the car type at the time of pick-up.</li>
<li>Offer not to be clubbed with any other promotion.</li>
<li>Myles shall not be held responsible for any liabilities/claims arising out of use of the partner coupon codes. In case of any information/disputes/claims regarding the offer, customer needs to contact the partner helpline number shared in the offer details of individual partners.</li>
<li>The discount can be redeemed in full &amp; not in parts.</li>
<li>Blackout dates apply.</li>
<li>Other standard T&amp;C apply.</li>
</ul>
</div>
<div class="clr"> </div>
<h1 style="background-color: #ECECEC;
    padding: 10px;
    font-size: 19px;
    border: 1px solid #eee;
    border-bottom: none;">Myles Resolution Offers</h1>
<div class="clr"> </div>
<div id="pop2" class="t_condition">
<h3>About</h3>
<p><strong>UrbanClap</strong> – India’s Largest Services Marketplace</p>
<h3>Offer</h3>
<p style="margin-top:0px;">
Rs. 300 Off on select categories:- Salon @ Home, Pest Control, Kitchen Cleaning, Bathroom Sofa Cleaning, Car Cleaning.</p>
<p>Also, get trial classes for fitness trainers and yoga instructors*.</p>
<!--strong>Offer Code</strong>
<p style="margin-top:0px;">MYLES300</p-->
<h3>Validity</h3>
<p style="margin-top:0px;">February 29<sup>th</sup> 2016</p>
<!--strong>Redemption Process</strong>
<p style="margin-top:0px;"><strong>Offer Applicable on select categories:-</strong> Salon @ Home, Pest Control, Kitchen Cleaning, Bathroom Cleaning, Sofa Cleaning, Car Cleaning.</p>
<strong>Contact Details</strong>
<p style="margin-top:0px;">9879532289</p-->
<!--ul>
<li><strong>Offer –  </strong>Discount – 300,  Minimum Booking Amount - 1000</li>
<li><strong>Offer Code (any other details) – </strong>MYLES300</li>
<li><strong>Validity – </strong>Till February 29<sup>th</sup> 2015</li>
<li><strong>Redemption – </strong> <strong>Categories:</strong> Salon @ Home, Pest Control, Kitchen Cleaning, Bathroom Cleaning, Sofa Cleaning, Car Cleaning</li>
<li><strong>Contact Details – </strong>9879532289</li>
</ul-->
<h3>Terms and conditions</h3>
<ul class="italic">
<li>Code applicable on a Minimum Booking Amount of Rs. 1000</li>
<li>Yoga and fitness trainer trial classes are subject to the professional’s availability.</li>
</ul>
</div>
<div id="pop3" class="t_condition">
<h3>About</h3>
<p>Lemon Tree Hotels are fresh, cool and sparkling with zest. Cheery greetings, a friendly smile and a whiff of the signature lemon fragrance. All this, at an unbeatable value.</p>
<h3>Offer</h3>
<p style="margin-top:0px;">Get 15% discount on best available room rates – With Free Buffet Breakfast, Free WiFi, 20% discount on FnB outlets and 30 mins free session at SPA.</p>
<!--ul>
<li>15% discount on best available room rates</li>
<li>Free Buffet Breakfast</li>
<li>Free WiFi</li>
<li>20% discount on FnB outlets</li>
<li>30 mins free session at SPA wherever applicable</li>
</ul>
<strong>Offer Code</strong>
<p style="margin-top:0px;">MYLESLT</p-->
<h3>Validity</h3>
<p style="margin-top:0px;">From 5<sup>th</sup> Jan’16 to 30<sup>th</sup> Sep’16</p>
<!--strong>Redemption Process</strong>
<p style="margin-top:0px;">Online at <a href="http://www.lemontreehotels.com/" target="_blank">www.lemontreehotels.com</a></p-->
<h3>Terms and conditions</h3>
<ul class="italic">
<li>As applicable on online bookings</li>
</ul>
</div>
<div class="clr"> </div>
<div class="w31 mr3 mb3 show2 f_l b_box">
<img src="http://www.mylescars.com/img/myles_resolution02.jpg">
</div>
<div class="w31 mr3 f_l show3 mb3 b_box">
<img src="http://www.mylescars.com/img/myles_resolution03.jpg">
</div>
</div>
<div class="clr"> </div>
<!--<div id="backgroundPopup"></div>-->
<div id="overlay123"></div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
  error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/seo-metas.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />

</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header-js.php");
	include_once("./includes/header.php"); 
	include_once('./classes/cor.mysql.class.php');
	include_once("./includes/dbconnect.php");
?>
<!--Banner Start Here-->

<!--Middle Start Here-->


<div id="middle" style="padding-top:10px;">
<h1 class="yellwborder" style="background-color:#F2C900;">
<div class="w1130">Car Rental Agreement</div>
</h1>
<div class="main">

<div class="aboutus">
<div class="tabcontent agreement">

<div class="term_head">NOW, THEREFORE, THIS AGREEMENT WITNESSETH AND IT IS HEREBY AGREED BY AND BETWEEN THE PARTIES AS FOLLOWS:</div>
<ol>
<li>The Company hereby agrees to rent to the User and the User hereby agrees to take on rent the Vehicle, upon placement of the Rental Order (as defined hereinafter) for a Vehicle, subject to all the terms and conditions contained herein.</li>
<li>The User shall from time to time and as and when required by him/her, during the term of this Agreement, place the order for Vehicle <strong>(“Rental Order”)</strong> specifying the time period for which the Vehicle is to be used by the User and the Company shall provide the Vehicle to the User on rent in accordance with the terms and conditions as specified in this Agreement. </li>
<li>The User shall simultaneously with the placement of the Rental Order, submit with the Company a copy of valid <strong>driving license, passport/ Voter ID</strong> of the User and any other document that may be required by the Company. The User hereby represents and warrants that the User shall submit genuine, correct and accurate documents, set out in this Clause, to the Company. In the event of the driving license being changed, altered, expired or renewed, the same shall be uploaded on the web portal www.mylescars.com of the Company.</li>
<li>The Company upon the receipt of the Rental Order shall issue an invoice for the payment of rental amount. The User shall make the payment of the rental amount as specified in the invoice, within 2 hours of the receipt of the invoice, to the Company. Upon receipt of the rental amount by the Company, the Company shall provide the User, the login id and the password for the purposes of using the Vehicle. Upon the receipt of the login id and the password, the User shall have been considered to have taken over the possession of the Vehicle for rental purposes for a period mentioned in the invoice <strong>(“Rental Period”).</strong> The hourly and daily rental terms and conditions with respect to the renting of the Vehicles under this Agreement are appended as <strong>Annexure A.</strong></li>
<li>The User represents and assures the Company that the Vehicle rented to it by the Company shall be self driven by the User only and the User shall not assign/sub-rent/license the rights in the Vehicle to any other person whatsoever.</li>
<li>In case the Vehicle is supposed to be driven by any other person apart from the User, the User shall ensure that such driver is holding a valid driving license and the User shall obtain prior written consent of the Company for such person at the time of placing the Rental Order, such that the additional driver’s name, driving license or any other document as may be demanded by the Company must be submitted along with the Rental Order and the same is approved by the Company.</li>
<li>The User agrees that it will return the Vehicle, together with all its tyres, tools, Vehicle documents, accessories and equipment, to the agreed return location specified on the date specified in the invoice, unless the User requests and Company agrees to an extension in writing or sooner upon demand being made by the Company. The User shall ensure that the Vehicle, along with accessories and requisite documents, are in proper operating condition, clean and fit for the purpose for which it is required to the satisfaction of the Company. The five tyres are in good condition without puncture or any other defect that renders the tyres non workable. </li>
<li>In event of damage caused to any part of the Vehicle or any accessory thereof, other than normal wear thereof, the User agrees to repair and/or replace the same forthwith at his/her own expense with a part/accessory/tyre of the same dimension/brand and in equivalent state of wear at its own cost. The odometer and accessories of the Vehicle shall not be damaged//tampered and in case it is done, the User is liable for the replacement cost of the same. No parts of the Vehicle shall be replaced without obtaining a prior written consent of the Company. </li>
<li>That User hereby acknowledges and agrees to have received the Vehicle in good order and condition. The User shall be solely liable for cost of all fuel consumed during the Rental Period (In case of daily rental option only) and the restoration. User of the Vehicle hereby acknowledges and agrees to the following:
<ol start="a">
<li>Restoration of the Vehicle in the same good order and condition where the Company feels warranted, wherein the User shall maintain the Vehicle as a prudent man.</li>
<li>The Vehicle will be restored with the Company containing the same amount of fuel in it as was provided before the handing it over to the User. In the event User does not provide the fuel as has been mentioned in the aforesaid clause, the Company shall have the right to charge the User as per terms mentioned in the invoice.</li>
</ol>
</li>
<li>That the User represents and warrants that after being handed over with the possession of the Vehicle, the Vehicle shall not be operated:
<ol start="a">
<li>To transport goods in violation of Customs regulations or in any other illegal manner.</li>
<li>To carry passengers or property for a consideration express or implied;</li>
<li>To propel or tow any Vehicle or trailer without obtaining a prior written consent of the Company;</li>
<li>In motor sport events (including racing pacemaking, reliability trials, driving lessons speed testing and any other events of similar nature):</li>
<li>By any person driving when unfit through drink or drugs or with blood alcohol concentration above the limit prescribed by the applicable law;</li>
<li>By any person other than the User or any person(s) nominated or employed by the User who is approved by the Company; who is at least 23 years of age or older as stated on the Company’s tariff, is duty qualified and holds and has held a current valid full driving licence for at least one year, or in the case of breakdown or accident, a motor Vehicle repairer provided that he is duly qualified and licensed.</li>
<li>Outside geographical boundaries of the Republic of India without the express agreement of the Company.</li>
<li>User will not take a Vehicle to high terrain areas, terrorism/naxal affected areas.</li>
<li>The User shall not carry and shall not cause or allow to be carried any contraband, drug or psychotropic substances in the Vehicle. </li>
<li>The User shall ensure that the no explosive substance is carried inside the Vehicle.</li>
</ol>
</li>
<li>The User is personally liable to pay the Company on demand:
<ol start="a">
<li>A mileage charge computed at the rate specified in enclosed invoice for the mileage covered by the Vehicle until the Vehicle is returned (the number of miles over which the Vehicle is operated shall be determined by reading the odometer installed by the manufacturer, if odometer fails the mileage charge shall be calculated from the road map distance of the journey traveled):</li>
<li>Time, damage waiver (if any), personal accident insurance (if any), pick up service, drop service. Fuel and miscellaneous charges at the rates specified in invoice.</li>
<li>All fines and court costs for parking, traffic or other legal violations assessed against the Vehicle, the User, other driver or the Company until the Vehicle is returned, except where caused through fault of the Company.</li>
<li>The Company’s cost, including reasonable legal fees where permitted by law, incurred collecting payments due from the User hereunder, and</li>
<li>The Company’s cost for repairing collision or upset damages to the Vehicle, provided, however, if the Vehicle is operated in accordance with all the terms hereof, the User’s liability for such damage shall be waived if the User has purchased the damage waiver in advance subject to damage waiver policy.</li>
<li>Value Added Tax and all other applicable taxes shall be payable extra and in addition to the rental amount.
<br>
<br>
All the above expenses shall be adjusted from the Security Deposit (as defined below). In the above expenses exceed the amount of Security Deposit, the User shall pay such extra amount to the Company.
</li>
</ol>
</li>
<li>That the User undertakes to take all necessary steps to protect the interest of the Company and the Company’s insurance company and shall ensure that the Vehicle is not used for any purpose not permitted by the terms and conditions of the relevant policy of insurance. </li>
<li>The User shall not do or allow to be done any act or thing where by any such policy of insurance may be avoided nor taken outside any territorial limit stipulated in such policy of insurance. The User and any authorized User shall further participate as an insured under an automobile insurance policy .The User is bound by and agrees to the terms and conditions thereof. The User agrees further to protect the interests of the Company and its insurance company in case of accident or theft by doing the following, as applicable:
<ol start="a">
<li>By co-operating with the Company to comply with necessary documentation and other formalities required for claiming insurance.</li>
<li>Obtaining names and addresses of the parties involved, and of witnesses;</li>
<li>Not admitting liability or guilt or giving money to any persons involved;</li>
<li>Not abandoning the Vehicle without adequate provision for safeguarding and securing same.</li>
<li>Calling the Company within 24 hours by telephone and forward a copy of FIR (if applicable) even in case of slight damage; further completing the Company’s accident report including diagram as required on return of Vehicle; and </li>
<li>Notifying the police immediately if another party’s guilt has to be ascertained, or if any person is injured.</li>
</ol>
</li>
<li>The Company hereby declines that it has no responsibility for subjects left in the Vehicle during the Rental Period of the Vehicle. The Company further declares that it has no responsibility for injury to third parties or damages to the Vehicle, which the User may cause during the Rental Period. The User shall return the Vehicle to the Company in the condition in which the delivery of the same was taken, but subject to normal wear and tear. The User and/or any other authorized User/drivers shall be liable for any breach, infringements, criminal liability committed by them while using/driving the Vehicle.</li>
<li>That User hereby releases and agrees to indemnify the Company and hold harmless from and against any liability for loss or for damage to any property (including costs relating thereto) left, stored or transported by the User or any other person in or upon the Vehicle before or after return of the Vehicle to the Company.</li>
<li>That the Company whilst taking all precautions and using its best efforts to prevent such happening shall not be liable for any loss or damage arising from any fault or defect in or from mechanical failure of the Vehicle of any consequential loss or damage.</li>
<li>That User shall during the continuance of the Rental Period:
<ol start="a">
<li>Always lock the Vehicle when not in use and ensure it is adequately protected against damage due to adverse weather conditions.</li>
<li>Not allow any person without the prior authorization of the Company to carry any work which otherwise interferes with the Vehicle or any part thereof except if the prior authorization cannot be obtained and the repair is minor to the tune of Rs. 500/- (Rupees Five Hundred Only).</li>
<li>Not hold himself cut as or purports to act as the agent of the Company for any purpose whatsoever.</li>
<li>Be fully responsible for any loss or damage caused to the Vehicle howsoever occasioned other than normal wear and tear. The User shall give immediate notice to the Company and subsequently confirm in writing by sending through speed post/courier within 24 hours of any loss or damage caused to the Vehicle or any breakdown, malfunction or other failure thereof. The User shall not continue to use the Vehicle in the event of damage to or a breakdown of the Vehicle if to do so would or might cause further damage to the Vehicle. The User shall take the Vehicle to the nearest authorized service centre for repair and pay such cost as estimated by the authorized service centre and shall ensure delivery of the Vehicle of the Company. The User shall be responsible to bear all cost for handover of the Vehicle.</li>
<li>Keep the Company fully indemnified including its directors, employees and shareholders against all losses, liabilities, costs actions, claims, any fines or penalties imposed (“Losses”) in respect of or arise out of use of the Vehicle including Losses on account of any third party liability, damage to the Vehicle, accidents, illegal or otherwise use of the Vehicle, misrepresentation or breach of its obligations by the User.</li>
<li>Without prejudice to provision for Company’s right to indemnification under this Agreement, the User agrees to pay to the Company for each and every breach of terms and condition of this Agreement as detailed in this agreement.
<br>
<br>
The User agrees that subsequent to taking over of the possession of the Vehicle by the Company from the User after the expiry of the Rental Period if the Company detects any damage in the Vehicle, the Company shall intimate details of such damage and the cost payable for such damage to the User. If the User fails to pay such cost even after intimation from the Company for three times, it shall be deemed acceptance by the User of his/her liability to pay for such damage and on such happening, the User authorizes the Company to deduct cost of such damage as mentioned below from the Security Deposit paid to the Company at the time of booking.
</li>
<li>Not sell, assign, pledge, let or hire or otherwise dispose of the Vehicle and/or its equipment or attempt to do any of these things. Not allow any person to use or drive the Vehicle without prior written consent of the company save and except as provided in this agreement.</li>
<li>Neither remove nor change any name or other mark identifying the ownership of the Vehicle; and (ii) neither use nor allow the Vehicle to be used for any illegal purposes or for any purpose of which it is not suitable or desirable.</li>
<li>Neither use nor allow anyone to carry passengers more than permitted by the registration paper.</li>
<li>Neither use nor drive the Vehicle under the influence of alcohol or narcotic drugs.</li>
<li>Neither drive the Vehicle beyond permissible speed limit nor contravene with the provisions, of any statue, statutory, instrument, regulation relating either to the Vehicle to the Vehicle of its use and procure that driver of the Vehicle shall observe and perform the terms and conditions of all policies or contracts of insurance relating to the Vehicle for its use.</li>
<li>Acknowledge that the Vehicle is and shall be throughout the period of its hiring be the sole property of the company and/or its affiliates and all rights thereto shall vest in the company and/or its affiliates.</li>
<li>Not acknowledge or compound any claim either partial or in full in respect of any accident involving the Vehicle.The User agrees to comply with the ‘Car Usage Conditions’ appended as Annexure B to this Agreement.
<!--p>The cost/penalty payable by the User for repair/replacement of any part of the Vehicle is listed in detail in Annexure C of this Agreement (hereinafter referred to as “Damages/Penalty”). The User’s liability to pay for damages shall be limited to Rs. 5,000 (Rupees Five Thousand Only). </p>
<p>It is hereby clarified that notwithstanding Company’s insurance of the Vehicles, if the Damages are up to Rs.5,000 (Rupees Five Thousand Only) the User shall be responsible to pay for it. The Company shall claim insurance for damages exceeding Rs.5,000 (Rupees Five Thousand Only).</p>
<p>Further, notwithstanding any provision of this Agreement, the User shall be liable to pay for damages not covered under the Company’s insurance policy for the Vehicles.</p-->
</li>
</ol>
</li>
<li>The User hereby further represents and warrants that the information and documents of the User supplied by him/her to the company are true, correct and accurate.</li>
<li>The User shall be solely responsible for and hold the Company fully indemnified against any loss, theft, damage, costs, accident or injury (including death) to persons or property, including loss, theft, damage, accident of the Vehicle, accruing in connection with the Vehicle or as a result of the negligent use thereof and against any breach of obligation by the User of the Vehicle. This indemnification shall survive termination of this Agreement.</li>
<li>The User hereby acknowledge that for the purposes of the Motor Vehicles Act, 1988, the User shall be liable in respect of the Vehicle including but not limited to any challans, penalties, third party liability, accidents and the User shall indemnify the Company against any losses, damage, costs and liabilities etc., in this regard.</li>
<li>In the event of any breach by the User of any of the terms and conditions hereof, the Company may without notice repossess the Vehicle and for such purpose may enter upon premises where the Vehicle may be and remove the same and the User shall be responsible for and indemnify the company against all actions, claims, costs and damage consequent upon or arising from such repossession and removal.</li>
<li>The User hereby understands and agrees that any fine arising from parking, traffic or driving violation will be charged along with 100% surcharge on such amount, VAT/Service Tax at applicable rates will be charged extra. Any increase in tax or the incidence of new taxes will be charged directly from the User in additional to the rental charges.</li>
<!--li>If, by virtue of placing the Rental Order, it is the User’s intention to pay by credit card or charge card, then the User hereby authorizes the Company to compute and debit the final total charges against the User’s account with the specified card issuing organization. The rate of exchange used on any currency conversion shall be conclusively determined by the Company. The User gives the right to the Company to block an advance deposit amount of Rs. 5,000 (Rupees Five Thousand Only) for normal Cars and  Rs. 50,000(Rupees Fifty Thousand Only) for Luxury Cars  on the credit card for security purpose (“Security Deposit”). The User authorizes the Company to deduct the Damages and any additional charges under this Agreement, if any directly from the Security Deposit and in case Penalty exceeds the amount of Security Deposit, then from the Credit or the Debit Card, the details of which had been shared by the User at the time of booking of the company.</li-->
<li>The User hereby understands and agrees that release of Security Deposit or the inadvertent charged amount on his/her credit cards details shall be released/credited to his/her credit card by the concerned bank. The Company shall not be responsible for any delay by the concerned bank in this regard.
The Security Deposit shall be refunded, after deduction of Damages and any other amount payable under this Agreement.</li>
<li>In the event of outstation trips, if the Vehicle is damaged for any reason attributable to User, the User shall be liable to get the same rectified at its own costs at authorized service station of such Vehicle. The decision of the Company in this regard shall be final. The User shall take the Vehicle to the nearest authorized service centre for repair and pay such cost as estimated by the authorized service centre and shall ensure delivery of the Vehicle to the Company upon the expiry of the Rental Period in proper and fit condition to the satisfaction of the Company. The User shall be responsible to bear all cost for handover of the Vehicle.</li>
<li>Any addition or alteration to these terms and conditions of this Agreement shall be null and void unless agreed upon in writing by the Parties.</li>
<li>In this Agreement, the word “Vehicle” shall in addition to the meaning ascribed hereto shall also include any replacement thereof and shall include all equipment , accessories, tools and spare tyre relating to the same and the singular shall where as appropriate including the plural and vice-versa. Any reference to any statutes shall be deemed to refer to any statutory modification or re-enactment, or any rules, regulations, notifications, circulars, etc. made or issued thereunder for the time being in force.</li>
<li>The User agrees that the data provided in this Agreement and at the time of placing the Rental Order may be stored, processed and transmitted manually/electronically by the Company. The User also agrees to provide accurate information Rental Order and shall be liable for any damages and disputes arising due to the inaccuracy of the information.</li>
<li>The User may be allowed to extend the duration of rental over the time period mentioned in the invoice after getting a written communication from the Company. The Company holds the final authority over the extension and shall charge for additional days or hours, as the case may be, as per the rates specified in the invoice. Unless the Company allows the extension in writing, the User shall be bound to return the Vehicle back as per the tenure mentioned in the invoice.</li>
<li>The User shall not transfer or assign any of its rights or obligations under this Agreement without the prior written approval of the Company.</li>
<li>Unless otherwise stated, all notices, approvals, instructions and other communications for the purposes of this Agreement shall be given in writing and may be given by facsimile, by e-mails, by personal delivery or by sending the same by registered acknowledgement due or courier addressed to the party concerned at the address first stated herein above or any other address subsequently notified to the other party. Such notice shall be deemed to be delivered on receipt thereof.</li>
<li>In the event of any dispute or difference arising out of or in connection with this Agreement, the Parties shall endeavor to resolve the same mutually through amicable discussions. In the event the Parties fall to arrive at an amicable settlement within 15 days from the date of reference thereof for amicable discussions, such dispute/difference shall be settled through arbitration and shall be referred to the sole arbitrator to be appointed by the Company. The arbitration proceedings shall be conducted in accordance with the provisions of the Arbitration and Conciliation Act, 1996 and/or any amendments and modifications made thereto. The arbitration proceedings shall be conduct in the English language and the place of arbitration shall be Delhi. It is hereby further agreed that the award of the arbitrator shall be final and binding on the parties. This clause shall survive expiry or termination of this Agreement.</li>
<li>This Agreement shall be governed by and construed in accordance with the laws of India and subject to dispute resolution mechanism hereby agreed, the Parties have agreed to submit themselves to the jurisdiction of the courts of the place where the Vehicle is delivered by the Company to the User.</li>
<li>This Agreement constitutes the entire agreement between the Parties with respect to the subject matter hereof and supersedes all prior communications negotiations and representations either oral or written, between the Parties, in relation hereto.The terms and conditions of the rental of the Vehicles to the User shall be governed by this Agreement.</li>
<li>All-purpose necessities
<ol start="a">
<li>The User must swiftly report any incident involving loss or damage to the Vehicle, and no later than hours of the occurrence of such loss or damage, while rented under this Agreement or to any property or person to the company location from where the Vehicle was hired and will deliver to the Company immediately, every summons, complaint or paper in relation to such loss. </li>
<li>The User shall release and hold harmless the Company (and its directors, officers and employees) from all claims for loss or damage to their personal property. Or that of any other person’s property left in the Vehicle, or which is received, handled or stored by the Company at any time before, during or after this Rental Period, whether due to the Company’s negligence of otherwise.</li>
<li>No right of the Company under this Agreement may be waived except in writing by legal department of the Company.</li>
<li>The Company shall not be held liable for any injury, damages or any other occurrence as a result of child restraints not being correctly fitted or used.</li>
</ol>
</li>
<li>In an event wherein cancellation of the Rental Order is done by the Company for whatsoever reason, Company shall refund booking amount and shall pay 10% of the rental amount or INR 1500/- (Rupees Fifteen Hundred Only) whichever is lower. Cases related to cancellations due to force majeure, fog, earthquake, floods, fire, riots, strike or circumstances beyond Company control will not be covered under this clause and in such cases only booking amount will be refunded. Parties hereby agree that under no circumstances the liability of the Company shall exceed the Vehicle rental charges. This clause is related to booking screen/confirmation mail to User as pre-condition and it may be changed time to time.</li>
<li>Notwithstanding anything in this Agreement, the Company has the sole right to refuse delivery of the Vehicle to the User upon placement of the Rental Order without any reason subject to refund of amounts paid by the User to the Company and 10% of the rental amount or INR 1500/- whichever is lower.</li>
<!--li>Notwithstanding anything contained in this Agreement, the Company reserves the right to take back the Vehicle at any time during the continuance of Rental Period or thereafter, if in the opinion of the Company, the User or any third party may damage the Vehicle or may use the Vehicle for any unauthorized purpose.</li>
<li>Notwithstanding anything contained in this Agreement, the Company reserves the right to change, amend and modify the terms of this agreement and same shall be incorporated on the web portal www.mylescars.com of the Company which when accepted by the User shall form part of this agreement. </li-->
<li>Payment of any charge/fees payable under this Agreement shall not be allowed to be made in cash.
<p>IN WITNESS WHEREOF, the Parties have executed this Agreement through their duly authorized representatives at the place and on the date, month and year first above written.</p>
</li>
</ol>
<div class="term_head">ANNEXURE A, TERMS AND CONDITIONS, Daily rental Terms and Conditions</div>
<table class="agree_ment">
<tr>
<th width="30%">Daily Rental- Terms and Conditions</th>
<th>Description</th> 
</tr>
<tr>
<td valign="top">Eligibility Age</td>
<td>For Tata Nano – 18 years or above <br>
For other Myles Cars – 23 years above
</td> 
</tr>
<tr>
<td valign="top">Documents</td>
<td>Passport/Voter ID, Driving license, Credit Card with agreed limit of Security Deposit of Rs. 5,000 (Rupees Five Thousand Only) for normal Cars and  Rs. 50,000(Rupees Fifty Thousand Only) for Luxury Cars. If the User does not present the above documents, in original at the time of obtaining delivery of the Vehicle, the Company shall be entitled to refuse the delivery of the Vehicle and cancel this Agreement in such an event 50% of the amount paid by the User shall be forfeited by the Company subject to minimum of one day rental.</td> 
</tr>
<tr>
<td valign="top">Rental Period</td>
<td>Rental period will be minimum of 24 Hrs. from the user booking time. The said 24 hours shall be calculated from the time the User collects the Vehicle and up to the time the User returns the rental vehicle.</td> 
</tr>
<tr>
<td valign="top">Other terms</td>
<td>As per terms given below</td> 
</tr>
<tr>
<td valign="top">VAT</td>
<td>As per State rules</td> 
</tr>
<tr>
<td valign="top">Pre authorization from credit card</td>
<td>Will be done at the time of Vehicle delivery to the User at a rate provided in the Rental Agreement attached herewith. This will be treated as Security Deposit. In case of any damage in the vehicle, cost of damage will be deducted from the pre authorized amount subject to terms of the Agreement.
Payment in cash not acceptable</td> 
</tr>
<tr>
<td valign="top">Delivery /Collection</td>
<td>Vehicle must be returned at the location from which it was picked up/hired.</td> 
</tr>
<tr>
<td>Included KMs</td>
<td>Unlimited</td> 
</tr>
<tr>
<td>Extra KM Rate</td>
<td>N/A</td> 
</tr>
<tr>
<td valign="top">Fuel</td>
<td>The Vehicle will be provided with a full tank of fuel. In case the tank is not full at the time of return of Vehicle, the User will be charged for short fuel along with 10% surcharge on fuel price.</td> 
</tr>
<tr>
<td valign="top">Extra day</td>
<td>In case User wants to extend his/her travel, there will additional charge 30% on per day rental over and above the normal rental charges.</td> 
</tr>
<tr>
<td valign="top">GPS Navigation system/Child Safety Seat</td>
<td>In case User wants to extend his/her travel, there will additional charge 30% on per day rental over and above the normal rental charges.
GPS Navigation / Child safety seat shall be available on request with additional charges (Subject to availability)</td> 
</tr>
<tr>
<td valign="top">Threshold time</td>
<td>Threshold time	User will be allowed a leverage of 1 hours for returning the Vehicle Post which 50% surcharge would be applicable, each hour as per hourly charges.</td> 
</tr>
<tr>
<td valign="top">Cancellation</td>
<td>If booking is cancelled 24 hours prior to the scheduled pick up time, no cancellation charges will be applied. If the booking is cancelled within 24 hours of the scheduled pick up time, 1 day rental will be charged and if booking is cancelled on or after pickup time, it will be treated as no show and no amount will be refunded.</td> 
</tr>
<tr>
<td>No show</td>
<td>In case of No-Show no amount will be refunded to the User</td> 
</tr>
<tr>
<td valign="top">Vehicle Damage</td>
<td>Damage charges to be paid by User in terms of the Rental Agreement. In case damage cost goes beyond INR 5,000/- insurance claim may be filed and Repair/depreciation amount (In case of insurance claim) & Parts which are not covered under insurance will be recovered from the User. The assessment of damage made by Company will be final. Insurance claim is subject to Company’s discretion.</td> 
</tr>
<tr>
<td valign="top">Outstation</td>
<td>When traveling out of state, it is always important to step at RTO check post to pay the government applicable fax. Cost/penalties pertaining to such interstate tax, toll or any other govt. levied tax will be borne by the User directly.</td> 
</tr>
<tr>
<td valign="top">Traffic Rule Violation</td>
<td>Cost for any traffic rule violation during the Rental Period will be borne by the User directly. In cases, where challans is received by the Company via post upon completion of the Agreement, the User authorizes the Company to charge the same on the credit card number of the User as mentioned in this Agreement offline. The User shall follow all the rules and regulation under applicable laws while driving the Vehicle and otherwise including applicable speed limits.</td> 
</tr>
<tr>
<td valign="top">Cleanliness</td>
<td>Company does not allow the Vehicle to be used to carry pets, goods or any other object which can annihilate the upholstery/any of the vehicles. In such event, the User will be penalized with Rs. 1000/-(Rs. One Thousand Only) + applicable damage costs related to interiors towards repairing/cleaning of interior / exterior of vehicle.</td> 
</tr>
<tr>
<td valign="top">Out of Reach</td>
<td>During the continuance of Rental Period or any time after it, if User remains non-contactable for a continuous period of 24 Hours, then the Company shall be entitled to immediately take back the Vehicle and terminate this Agreement.</td> 
</tr>
</table>
<br>
<br>
<div class="term_head">Hourly rental terms and conditions</div>
<table class="agree_ment">
<tr>
<th width="30%">Hourly Rental- Terms and Conditions</th>
<th>Description</th>
</tr>
<tr>
<td valign="top">Eligibility Age</td>
<td>
For Tata Nano – 18 years or above <br>
For other Myles Cars – 23 years above
</td>
</tr>
<tr>
<td valign="top">Documents</td>
<td>Passport/Voter ID, Driving license, Credit Card with desired limit of Security Deposit of Rs. 5,000 (Rupees Five Thousand Only) for normal Cars and  Rs. 50,000(Rupees Fifty Thousand Only) for Luxury Cars.. If the User does not present the above documents, in original at the time of obtaining delivery of the Vehicle, the Company shall be entitled to refuse the delivery of the Vehicle and cancel this Agreement in such an event 50% of the amount paid by the User shall be forfeited by the Company subject to minimum of one day rental.</td>
</tr>
<tr>
<td valign="top">Rental Period</td>
<td>Rental period will be minimum of 2 Hrs. from the user booking time up to maximum of 23 Hrs. The said hours shall be calculated from the time the User collects the Vehicle and up to the time the User returns the rental vehicle..</td>
</tr>
<tr>
<td valign="top">Other terms</td>
<td>As per terms given below</td>
</tr>
<tr>
<td valign="top">VAT</td>
<td>As per State rules</td>
</tr>
<tr>
<td valign="top">Pre authorization from credit card</td>
<td>Will be done at the time of Vehicle delivery to the User at a rate provided in the Rental Agreement attached herewith. This will be treated as Security Deposit. In case of any damage in the Vehicle, cost of damage will be deducted from the pre authorized amount subject to terms of the Agreement.
Payment in cash not acceptable</td>
</tr>
<tr>
<td valign="top">Delivery /Collection</td>
<td>Vehicle must be returned at the location from which it was picked up/hired.</td>
</tr>
<tr>
<td valign="top">Included KMs</td>
<td>For all Myles Cars – Max. up to 10 Kms. Per hour, beyond which extra Km rate shall apply. However, during the day, the maximum limit is 230 Kms. Beyond which extra Km rate shall apply.</td>
</tr>
<tr>
<td valign="top">Extra KM Rate</td>
<td>As mentioned in the invoice. Amount equivalent to extra Kms. shall be debited from the security deposit.</td>
</tr>
<tr>
<td valign="top">Fuel</td>
<td>The Vehicle will be provided with a full tank of fuel.</td>
</tr>
<tr>
<td valign="top">Extra Hour</td>
<td>In case User wants to extend his/her travel, there will additional charge 30% on per hour rental over and above the normal rental charges.</td>
</tr>
<tr>
<td valign="top">GPS Navigation system/Child Safety Seat</td>
<td>GPS Navigation / Child safety seat shall be available on request with additional charges (Subject to availability)</td>
</tr>
<tr>
<td valign="top">Threshold time</td>
<td>User will be allowed 30 minutes post completion of Rental Period to return the Vehicle subject to payment of Returning Vehicle late charges as below, post which next hour rental will be applicable.</td>
</tr>
<tr>
<td valign="top">Returning vehicle late</td>
<td>Regular hourly tariff PLUS INR 500 per hour late if it causes inconvenience to another member.</td>
</tr>
<tr>
<td valign="top">Cancellation</td>
<td>No refund on cancellation.</td>
</tr>
<tr>
<td valign="top">Vehicle Damage</td>
<td>Damage charges to be paid by User in terms of the Rental Agreement. In case damage cost goes beyond INR 5,000/- (Rupees Five Thousand Only) insurance claim may be filed and Repair/depreciation amount (In case of insurance claim) & Parts which are not covered under insurance will be recovered from the User. The assessment of damage made by Company will be final. Insurance claim is subject to Company’s discretion.</td>
</tr>
<tr>
<td valign="top">Replacement Car</td>
<td>Replacement Vehicle shall be provided within a city limits only (Subject to availability of similar segment in the city) in all such cases where in a Vehicle develops mechanical error and restrain the User from its use. In a case, where in Vehicle develops such error outside the city limit, replacement Vehicle will not be provided and amount equivalent to agreed rental will be charged. Company decision related to replacement vehicle will be final.</td>
</tr>
<tr>
<td valign="top">Toll and Taxes</td>
<td>When travelling out of state, it is always important to stop at RTO check post to pay the government applicable tax. Cost/penalties pertaining to such interstate tax, toll or any other govt. levied tax will be borne by the User directly.</td>
</tr>
<tr>
<td valign="top">Traffic Rule Violation</td>
<td>Cost for any traffic rule violation during the Rental Period will be borne by the User directly. In cases, where challans is received by the Company via post upon completion of the Agreement, the User authorizes the Company to charge the same on the credit card number of the User as mentioned in this Agreement offline. The User shall follow all the rules and regulation under applicable laws while driving the Vehicle and otherwise including applicable speed limits.</td>
</tr>
<tr>
<td valign="top">Cleanliness</td>
<td>Company does not allow the Vehicle to be used to carry pets, goods or any other object which can annihilate the upholstery/any of the Vehicles. In such event, the User will be penalized with Rs. 1000/-(Rs. One Thousand Only) + applicable damage costs related to interiors towards repairing/cleaning of interior / exterior of vehicle. </td>
</tr>
<tr>
<td valign="top">Out of Reach</td>
<td>During the continuance of Rental Period or any time after it, if User remains non-contactable for a continuous period of 10 Hours, then the Company shall be entitled to immediately take back the Vehicle and terminate this Agreement</td>
</tr>
</table>
<br>
<br>
<div class="term_head">ANNEXURE B, CAR USAGE CONDITIONS</div>
<p>Following conditions will be considered as Myles car usage. Users will be fined if found involved in any of the following:</p>
<ol>
<!--li><strong>Over speeding:</strong> Users are allowed to drive the Vehicle up to a maximum speed of 100 km/hr (63 miles/hr), beyond which the Vehicle will be considered as over speeding. A penalty of Rs. 200 will be charged on the first instance, followed by a further penalty of Rs. 500 and Rs. 1000 for 2nd instance and 3rd instance respectively making it a total of Rs. 1,700/- at third instance. After the third instance, Booking will stand cancelled and the Company shall get the right to take back the Vehicle and terminate this Agreement.</li-->
<li><strong>Traffic violation:</strong> Users are liable to pay for any traffic violation tickets received during the Rental Period. Company can charge traffic violation from the User’s credit/debit card on actual if the violations tickets are sent directly to the Company by the traffic department.</li>
<li><strong>Car spare part changed:</strong> Users should not charge or remove any car spare parts. In case of emergency, the User should inform Company and act as per advice. Users will be charged a penalty of Rs. 5000 over and above the cost of spare part.</li>
<li><strong>Tyre misuse:</strong> In case of any tyre damages resulting from driving in bad terrain and continued driving in case of tyre puncture, User will be charged for the cost of tyre on actual .</li>
<li><strong>Running Vehicle in damaged conditions:</strong> Users are not advised to drive the Vehicle if it gets damaged in an accident. Users are advised to inform the Company immediately. In such case, Users will be charged for the cost of spare parts on actual as per this Agreement.</li>
<li><strong>Unauthorized activity in the Vehicle:</strong> Users are not allowed to carry arms ammunitions and banned drugs. In addition, use of Vehicle for commercial activity such as product sell and promotion, and carry goods is strictly prohibited. In such cases, Users will be charged a penalty of Rs. 5000.</li>
<li><strong>External branding:</strong> Any form of external branding on the Vehicle is prohibited. Users are not allowed to paste or paint any external brand promotion on the Vehicle. Users will have to pay a penalty of Rs. 5000.</li>
<li><strong>Tampering with devices:</strong> Users are not allowed to tamper with the odometer, GPS device and in car technology devices. Users will have to pay a penalty of Rs. 1000 and over in additional to the actual cost of the device.</li>
<li><strong>Deliberately driving the car in water:</strong> Users will be charged for the actual cost of repair and spare parts in terms of this Agreement.</li>
<li><strong>Producing fake and tampered personal documents:</strong> Users will be charged a penalty of Rs. 1000 if found guilty.</li>
<li><strong>Diversion:</strong> Users are not allowed to drive the car into unauthorized or government banned areas. Users are advised to inform the Company in case they change the course of their trip. All Myles Vehicles are geo-fenced and users have to pay a penalty Rs. 10,000 if the Vehicle trespasses into banned areas, naxal hit areas, international border of republic of India and extreme end of ladakh.</li>
<!--li><strong>Tobacco Use:</strong> Users are not allowed to consume, chew or use any type of Tobacco, Cigarettes, Drugs or any type of Narcotics and Psychotropic Substances violating which User shall be liable to penalty of Rs. 1000(Rupees One Thousand Only) and further it shall entitle the Company to take back the Vehicle and terminate this Agreement.</li-->
</ol>
<br>
<br>
<div class="term_head">ANNEXURE C, LIST OF PENALTY/ DAMAGES</div>
<table class="agree_ment">
<tr>
<th align="center">SEGMENT</th>
<th align="center" colspan="6">VARIANT Details</th>
</tr>
<tr>
<td align="center" valign="top">A</td>
<td align="center" valign="top" colspan="6">MARUTI ALTO, MAHINDRA E2O,TATA NANO,FORD FIGO,HYUNDAI I10, HYUNDAI I 10 GRAND,MARUTI SWIFT,MARUTI RITZ</td>
</tr>
<tr>
<td align="center" valign="top">B</td>
<td align="center" valign="top" colspan="6">VOLKSWAGON VENTO,MARUTI ERTIGA,NISSAN SUNNY,HONDA CITY,HONDA AMAZE,VOLKSWAGON POLO, HYUNDAI I20,TOYOTA ETIOS,MARUTI CIAZ</td>
</tr>
<tr>
<td align="center" valign="top">C</td>
<td align="center" valign="top" colspan="6">FORD ECOSPORTS, RENAULT DUSTER, MAHINDRA SCORPIO,TOYOTA INNOVA,TATA SAFARI, HONDA ACCORD,COROLLA ALTIS</td>
</tr>
<tr>
<td align="center" valign="top">D</td>
<td align="center" valign="top" colspan="6">MAHINDRA XUV, FORD ENDEVOR, TOYOTA FORTUNER,TOYOTA CAMARY</td>
</tr>
<tr>
<td align="center" valign="top">E</td>
<td align="center" valign="top" colspan="6">MERCEDES BENZ C, E, S CLASS</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="center" valign="middle"><strong>PANEL NAME</strong></td>
<td width="14%" align="center" valign="middle">A</td>
<td width="14%" align="center" valign="middle">B</td>
<td width="14%" align="center" valign="middle">C</td>
<td width="14%" align="center" valign="middle">D</td>
<td align="center" valign="middle">E</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">1</td>
<td width="14%" align="left" valign="middle">FRONT BUMPER</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">4500</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">2</td>
<td width="14%" align="left" valign="middle">HOOD</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">3</td>
<td width="14%" align="left" valign="middle">FENDER</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">4</td>
<td width="14%" align="left" valign="middle">DOOR</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">5</td>
<td width="14%" align="left" valign="middle">QUARTER PANEL</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">6</td>
<td width="14%" align="left" valign="middle">BOOT LID</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">7</td>
<td width="14%" align="left" valign="middle">REAR BUMPER</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">2200</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">3000</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">8</td>
<td width="14%" align="left" valign="middle">ROOF</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">3000</td>
<td width="14%" align="left" valign="middle">3000</td>
<td width="14%" align="left" valign="middle">3500</td>
<td align="center" valign="middle">5000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">9</td>
<td width="14%" align="left" valign="middle">REAR VIEW MIRROR</td>
<td width="14%" align="left" valign="middle">500</td>
<td width="14%" align="left" valign="middle">600</td>
<td width="14%" align="left" valign="middle">1000</td>
<td width="14%" align="left" valign="middle">1000</td>
<td align="center" valign="middle">1500</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">10</td>
<td width="14%" align="left" valign="middle">RUNNING BOARD</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td align="center" valign="middle">2000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">11</td>
<td width="14%" align="left" valign="middle">A PILLAR</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td align="center" valign="middle">2000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">12</td>
<td width="14%" align="left" valign="middle">WHEEL CAPS</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1500</td>
<td align="center" valign="middle"></td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">13</td>
<td width="14%" align="left" valign="middle">TAIL LAMP</td>
<td width="14%" align="left" valign="middle">2500</td>
<td width="14%" align="left" valign="middle">4500</td>
<td width="14%" align="left" valign="middle">5000</td>
<td width="14%" align="left" valign="middle">5000</td>
<td align="center" valign="middle">11000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">14</td>
<td width="14%" align="left" valign="middle">FOG LAMP</td>
<td width="14%" align="left" valign="middle">1800</td>
<td width="14%" align="left" valign="middle">2000</td>
<td width="14%" align="left" valign="middle">4000</td>
<td width="14%" align="left" valign="middle">4000</td>
<td align="center" valign="middle">11000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle">15</td>
<td width="14%" align="left" valign="middle">NUMBER PLATE</td>
<td width="14%" align="left" valign="middle">600</td>
<td width="14%" align="left" valign="middle">600</td>
<td width="14%" align="left" valign="middle">600</td>
<td width="14%" align="left" valign="middle">600</td>
<td align="center" valign="middle">1000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td align="center" valign="middle"></td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle">REMOVE AND FIT SCHEDULE</td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td width="14%" align="left" valign="middle"></td>
<td align="center" valign="middle"></td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="left" valign="middle">REAR VIEW MIRROR</td>
<td width="14%" align="left" valign="middle">150</td>
<td width="14%" align="left" valign="middle">200</td>
<td width="14%" align="left" valign="middle">250</td>
<td width="14%" align="left" valign="middle">300</td>
<td align="center" valign="middle">400</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="left" valign="middle">FRONT WINDSHIELD</td>
<td width="14%" align="left" valign="middle">1000</td>
<td width="14%" align="left" valign="middle">1200</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1800</td>
<td align="center" valign="middle">2000</td>
</tr>
<tr>
<td width="14%" align="center" valign="middle"></td>
<td width="14%" align="left" valign="middle">REAR WINDSHIELD</td>
<td width="14%" align="left" valign="middle">1000</td>
<td width="14%" align="left" valign="middle">1200</td>
<td width="14%" align="left" valign="middle">1500</td>
<td width="14%" align="left" valign="middle">1800</td>
<td align="center" valign="middle">2000</td>
</tr>
</table>
<br>
<br>
<div class="term_head">ANNEXURE C, LIST OF PENALTY/ DAMAGES</div>
<table class="agree_ment">
<tr>
<td width="20%" align="left" valign="top">Smoking</td>
<td align="left" valign="top">Rs. 1000 + applicable damages to interiors</td>
</tr>
<tr>
<td align="left" valign="top">Overspeeding>= 100 km/hr</td>
<td align="left" valign="top">
Rs. 200 will be charged on the first instance <br>
Rs. 500 will be charged on the second instance <br>
Rs. 1000 will be charged on the third instance <br>
</td>
</tr>
<tr>
<td align="left" valign="top">Carrying Pets</td>
<td align="left" valign="top">Rs. 500 + applicable damages to interiors</td>
</tr>
<tr>
<td align="left" valign="top">Late Return</td>
<td align="left" valign="top">
<p>One hour leverage is given in case of Daily bookings. Post which 50% surcharge would be applicable, each hour as per hourly charges.</p>
<p>30 minutes leverage is given in case of Hourly bookings. Post which a penalty of Rs 500 per hour plus the regular hourly rate will be charged.</p>
</td>
</tr>
<tr>
<td align="left" valign="top">Cancellation</td>
<td align="left" valign="top">
<p>If booking is cancelled 24 hours prior to the scheduled pick up time, no cancellation charges will be applied. If the booking is cancelled within 24 hours of the scheduled pick up time, 1 day rental will be charged and if booking is cancelled on or after pickup time, it will be treated as no show and no amount will be refunded.</p>
</td>
</tr>
</table>
</div>


</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>

<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
    include_once('./classes/cor.ws.class.php');
    include_once('./classes/cor.xmlparser.class.php');
    
    $cor = new COR();
    $myXML = new CORXMLList();
    $userId = 0;
    $res = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["mb"],"clientid"=>2205));
    if($res->{'VerifyPhoneResult'}->{'any'} != ""){
	$arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'}); 
        if(intval($arrUserId[0]["ClientCoIndivID"]) > 0){
            $userId = intval($arrUserId[0]["ClientCoIndivID"]);
        } else {
            $fullname = explode(" ", $_POST["nm"]);
            if(count($fullname) > 1){
                $fname = $fullname[0];
                $lname = $fullname[1];
            } else {
                $fname = $fullname[0];
                $lname = "";
            }
            $res = $cor->_CORRegister(array("fname"=>$fname, "lname"=>$lname, "PhoneNumber"=>$_POST["mb"], "emailId"=>$_POST["em"], "password"=>"retail@2012"));
            $arrUserId = $myXML->xml2ary($res->{'RegistrationResult'}->{'any'});
            if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){
                $userId = intval($arrUserId[0]["Column1"]);
            }
        }
        $vat = $cor->decrypt($_POST["vat"]);
        $corArrSTD = array();
        $corArrSTD["carCatId"] = trim($_POST["ccid"]);
        $corArrSTD["cityId"] = trim($_POST["ctid"]);
        $corArrSTD["promotionCode"] = trim($_POST["pc"]);
        $corArrSTD["pickUpDate"] = trim($_POST["pd"]);
        $corArrSTD["TotalCost"] = intval(trim($cor->decrypt($_POST["amt"])));
        $corArrSTD["ClientIndivID"] = $userId;
        
        $res = $cor->_CORVerifyRetailDiscount_UserID($corArrSTD);
        $dis = $myXML->xml2ary($res);
            
        //echo $corArrSTD["pickUpDate"];
        //print_r($corArrSTD);
        //unset($corArrSTD);
        
        $resp = "";
        //if((double)$dis[0]["PerDiscount"] > 0)
        //    $resp = $dis[0]["PerDiscount"] . "% of discount on total billing.<br ><a href=\"javascript: void(0);\" onclick=\"javascript: _applyDiscount('" . $dis[0]["PerDiscount"] . "');\">Apply</a>";
        //else
        //    $resp =  "No discount due to following reason:<br >" . $dis[0]["Reason"];
        $resp = $dis[0]["PromotionCode"] . "|#|" . 
                $dis[0]["PerDiscount"] . "|#|" . 
                $dis[0]["ValidYN"] . "|#|" . 
                $dis[0]["Reason"]. "|#|" . 
                $dis[0]["Type"]."|#|".
                $dis[0]['MaxDiscount']. "|#|".
                $cor->decrypt($_POST["amt"]). "|#|".
                $vat;
        unset($res);
        unset($dis);
        unset($cor);
        echo $resp;
    }
?>
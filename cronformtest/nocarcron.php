<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');
//include_once('Paynimo/gateway.php');

$Myle = new Myles();
$arr = array();


$db = new MySqlConnection(CONNSTRING);
$db->open();

//$book = $db->query("stored procedure", "myles_bookings_failed()");
//payment_status = 4 => payment sucessful but booking id not found

//$book_query = "SELECT * FROM `myles_bookings` WHERE payment_status = 4 AND booking_id =0 ";
//echo '$book_query = ' . $book_query;

$book_query = "SELECT * FROM `myles_bookings` WHERE coric='CORIC1481267941202'";

$book = $db->query("query", $book_query);

if (!array_key_exists("response", $book)) {
//
//     echo '<pre>';
//      print_r($book);
//     // die;
    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        $totfare = $book[$i]['tot_fare'];


        $juspaydata = array();
        $paybackdata = array();
        $walletdata = array();
        $discountdata = array();
        $juspay = array();
        $payback = array();
        $wallet = array();
        $discount = array();

        $juspay_amount = 0;
        $redeem_amount = 0;
        $wallet_amount = 0;
        $discount_amount = 0;
        $juspay_query = "SELECT * FROM  myles_juspay_trans WHERE coric= '" . $coric . "' AND payment_source_id = '1'";

        $rjuspay = $db->query("query", $juspay_query);
        //echo '<br/>';
        //echo '<pre>';
        // print_r($rjuspay);
        if (array_key_exists(0, $rjuspay)) {
            $juspay = $rjuspay[0];
        }
        if ((count($juspay) > 0) && ($juspay["juspay_status"] == "CHARGED")) {
            $datcreate = date_create($juspay["created"]);
            $juspay_amount = $juspay["juspay_amount"];
            $juspaydata['TransactionDateTime'] = $datcreate->format('Y-m-d H:i:s');
            $juspaydata["TransactionPlatform"] = $juspay["platform"];
            $juspaydata["TransactionType"] = 1;
            $juspaydata["ReferenceID"] = $juspay["coric"];
            $juspaydata["TransactionModeID"] = 2;
            $juspaydata["PaymentSourceID"] = $juspay['payment_source_id'];
            $juspaydata["OrderID"] = $juspay["juspay_order_id"];
            $juspaydata["Amount"] = $juspay["juspay_amount"];
            $juspaydata["ResposeCode"] = $juspay['juspay_trans_id'];
            $juspaydata["OtherTrnsDetails"] = 0;
            $juspaydata["isActive"] = 1;
            $juspaydata["CreatedBy"] = $juspay["juspay_customer_id"];
        }


        $payback_query = "SELECT * FROM  myles_payback_trans WHERE coric= '" . $coric . "' AND payment_source_id = '1'";

        $rpayback = $db->query("query", $payback_query);

        if (array_key_exists(0, $rpayback)) {
            $payback = $rpayback[0];

            if ((count($payback) > 0) && ($payback["redeem_amount"] != '')) {
                $datcreate = date_create($payback["created"]);
                $redeem_amount = $payback["redeem_amount"];
                $paybackdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
                $paybackdata["TransactionPlatform"] = $payback["platform"];
                $paybackdata["TransactionType"] = 1;
                $paybackdata["ReferenceID"] = $payback["coric"];
                $paybackdata["TransactionModeID"] = 4;
                $paybackdata["PaymentSourceID"] = $payback['payment_source_id'];
                $paybackdata["OrderID"] = $payback["payback_order_id"];
                $paybackdata["Amount"] = $payback["redeem_amount"];
                $paybackdata["ResposeCode"] = $payback['payback_trans_id'];
                $paybackdata["OtherTrnsDetails"] = $payback['amount_in_card'];
                $paybackdata["isActive"] = 1;
                $paybackdata["CreatedBy"] = $full_name;
            }
        }
        $wallet_query = "SELECT * FROM  myles_wallet_trans WHERE coric= '" . $coric . "' AND payment_source_id = '1'";

        $rwallet = $db->query("query", $wallet_query);
        
        if (array_key_exists(0, $rwallet)) {
            $wallet = $rwallet[0];
        }


        if ((count($wallet) > 0) && ($wallet["wallet_status"] == 'Success')) {
            $datcreate = date_create($wallet["wallet_transaction_time"]);
            $wallet_amount = $wallet["wallet_amount"];
            $walletdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
            $walletdata["TransactionPlatform"] = $wallet["platform"];
            $walletdata["TransactionType"] = 1;
            $walletdata["ReferenceID"] = $wallet["coric"];
            $walletdata["TransactionModeID"] = 1;
            $walletdata["PaymentSourceID"] = $wallet['payment_source_id'];
            $walletdata["OrderID"] = $wallet["wallet_order_id"];
            $walletdata["Amount"] = $wallet["wallet_amount"];
            $walletdata["ResposeCode"] = $wallet['wallet_ref_id'];
            $walletdata["OtherTrnsDetails"] = 0;
            $walletdata["isActive"] = 1;
            $walletdata["CreatedBy"] = $wallet["wallet_created_by"];
        }

        $discount_query = "SELECT * FROM  myles_discounts WHERE coric= '" . $coric . "' AND payment_source_id = '1'";


        $rdiscount = $db->query("query", $discount_query);
        //echo '<br/>';
        //echo '<pre>';
        //print_r($rdiscount);
        if (array_key_exists(0, $rdiscount)) {
            $discount = $rdiscount[0];
        }

        if ((count($discount) > 0) && (intval($discount["discount_amount"]) > 0)) {
            $datcreate = date_create($discount["created"]);
            $discount_amount = $discount["discount_amount"];
            $discountdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
            $discountdata["TransactionPlatform"] = $discount["platform"];
            $discountdata["TransactionType"] = 1;
            $discountdata["ReferenceID"] = $discount["coric"];
            $discountdata["TransactionModeID"] = 3;
            $discountdata["PaymentSourceID"] = $discount['payment_source_id'];
            $discountdata["OrderID"] = $discount["discount_order_id"];
            $discountdata["Amount"] = $discount["discount_amount"];
            $discountdata["ResposeCode"] = $discount['discount_code'];
            $discountdata["OtherTrnsDetails"] = 0;
            $discountdata["isActive"] = 1;
            $discountdata["CreatedBy"] = $full_name;
        }
        //echo 'totfare = ' . $totfare . '<br/>';
        $totalFromAllNode = $juspay_amount + $redeem_amount + $wallet_amount;
        if ($totfare == $totalFromAllNode) {
            $fullname = explode(" ", $book[$i]["full_name"]);
            if (count($fullname) > 1) {
                $firstName = $fullname[0];
                $lastName = $fullname[1];
            } else {
                $firstName = $fullname[0];
                $lastName = "";
            }


            $totFare = $book[$i]["tot_fare"];
            $corArrSTD = array();
            $corArrSTD["pkgId"] = $book[$i]["package_id"];
            $corArrSTD["PickUpdate"] = $book[$i]["pickup_date"];
            $corArrSTD["PickUptime"] = $book[$i]["pickup_time"];
            $corArrSTD["DropOffDate"] = $book[$i]["drop_date"];
            $corArrSTD["DropOfftime"] = $book[$i]["drop_time"];
            $corArrSTD["FirstName"] = $firstName;
            $corArrSTD["LastName"] = $lastName;
            $corArrSTD["phone"] = $book[$i]["mobile"];

            $corArrSTD["emailId"] = $book[$i]["email_id"];
            $corArrSTD["userId"] = $book[$i]["cciid"];
            $corArrSTD["GrossAmount"] = $book[$i]["tot_gross_amount"];
            $corArrSTD["NetPaidAmount"] = $totFare;

            $corArrSTD["VisitedCities"] = $book[$i]["origin_name"] . '-' . $book[$i]["subLocName"];
            $corArrSTD["AdditionalService"] = $book[$i]["additional_srv"];
            $corArrSTD["ServiceAmount"] = $book[$i]["additional_srv_amt"];
            $corArrSTD["ReferenceID"] = $book[$i]["coric"];
            $corArrSTD["OriginCode"] = $book[$i]["source"];
            $corArrSTD["IsPayBack"] = 1;

            $corArrSTD["SubLocationID"] = $book[$i]["subLoc"];
            $corArrSTD["TotalDuration"] = $book[$i]["duration"];
            $corArrSTD["ServiceAmtAll"] = $book[$i]["add_service_cost_all"];
            $corArrSTD["SubAirportCost"] = $book[$i]["airportCharges"];
            $corArrSTD["WeekDayDuration"] = $book[$i]["WeekDayDuration"];
            $corArrSTD["WeekEndDuration"] = $book[$i]["WeekEndDuration"];
            $corArrSTD["FreeDuration"] = $book[$i]["FreeDuration"];
            $corArrSTD["sPickUpDropOffAddress"] = $book[$i]["pickupdrop_address"];
            $corArrSTD["ClientCoID"] = 2205;
            $corArrSTD["Version"] = $book[$i]["app_version"];
           
            $bookingid = $Myle->_MYLESCreateBookingFromWebSite('CreateBooking', $corArrSTD);

            
           

            if (intval($bookingid) > 0) {

                $json_val = "";

                if (count($juspaydata) > 0) {
                    $juspaydata["BookingID"] = intval($bookingid);
                    $json_val .= json_encode($juspaydata);
                }

                if (count($walletdata) > 0) {
                    $walletdata["BookingID"] = intval($bookingid);
                    $json_val .= json_encode($walletdata);
                }

                if ((count($walletdata) > 0) && (count($juspaydata) > 0)) {
                    $walletdata["BookingID"] = intval($bookingid);
                    $juspaydata["BookingID"] = intval($bookingid);
                    $json_val = "";
                    $json_val .= json_encode($juspaydata) . "," . json_encode($walletdata);
                }

                if (count($paybackdata) > 0) {
                    $paybackdata["BookingID"] = intval($bookingid);
                    $json_val .= "," . json_encode($paybackdata);
                }


                if ((count($walletdata) == 0) && (count($juspaydata) == 0) && count($paybackdata) > 0) {
                    $paybackdata["BookingID"] = intval($bookingid);
                    $json_val = "";
                    $json_val .= json_encode($paybackdata);
                }

                if (count($discountdata) > 0) {
                    $discountdata["BookingID"] = intval($bookingid);
                    $json_val .= "," . json_encode($discountdata);
                }


                $arr = '{"jsonData":[' . $json_val . ']}';


                


                $res = $Myle->_MYLESUpdatePayment('UpdatePaymentV1', $arr);
               
                $response = $res['status'];
                if (intval($response) == 1) {

                    $dataToSave = array();
                    $where = array();
                    $where["coric"] = $coric;

                    $dataToSave["payment_mode"] = "1";
                    $dataToSave["payment_status"] = "1";
                    $dataToSave["booking_id"] = intval($bookingid);


                    $up = $db->update("myles_bookings", $dataToSave, $where);

                    unset($dataToSave);
                    unset($where);
                }
            }
            else
            {
                echo "abc";
                die;
            }
        }
    }
}
$db->close();

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');
//include_once('Paynimo/gateway.php');

$Myle = new Myles();
$arr = array();


$db = new MySqlConnection(CONNSTRING);
$db->open();

$juspay_url = "https://sandbox.juspay.in/";

$juspay_key = "D774B8A9E3D3452D9F8AADBB53497221";

$merchantId = "mylescars";


$juspay_query = "SELECT * FROM  myles_juspay_fail WHERE status  = '0' and payment_source_id = 1";

$juspay = $db->query("query", $juspay_query);

if (!array_key_exists("response", $rjuspay)) {

 for ($i = 0; $i < count($rjuspay); $i++) {

 if (array_key_exists(0, $rjuspay)) {
            $juspay = $rjuspay[0];
        
$ch = curl_init($juspay_url . 'order_status');
curl_setopt($ch, CURLOPT_POSTFIELDS, array('orderId' => $juspay['juspay_order_id'], 'merchantId' => $merchantId));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERPWD, $juspay_key);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$jsonResponse = json_decode(curl_exec($ch));

  if ($jsonResponse->{'status'} == 'CHARGED') {
           
            $ch = curl_init($juspay_url . 'order/refund');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $juspay_key);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('order_id' => $jsonResponse->{'orderId'}, 'unique_request_id' => $jsonResponse->{'txnId'},
                'amount' => $jsonResponse->{'amount'}));
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);

            $jsonResp = json_decode(curl_exec($ch));
            $err = curl_error($ch);
        
            $juspaydata = array();
            if (intval($jsonResp->{'refunded'}) == 1) {
                    $juspaydata['coric'] = $juspay['coric'];
                    $juspaydata['platform'] = $juspay['platform'];
                    $juspaydata['payment_source_id'] = 1;
                    $juspaydata['juspay_merchant_id'] = $jsonResp->{'merchant_id'};
                    $juspaydata['juspay_customer_id'] = $jsonResp->{'customer_id'};
                    $juspaydata['juspay_order_id'] = $jsonResp->{'order_id'};
                    $juspaydata['juspay_status'] = 'REFUNDED';

                    $juspaydata['juspay_amount'] = 0;
                    $juspaydata['juspay_refunded'] = $jsonResp->{'refunded'};
                    $juspaydata['juspay_amount_refunded'] = $jsonResp->{'amount_refunded'};

                    $juspaydata['juspay_trans_id'] = $jsonResp->{'txn_id'};
                    $juspaydata['juspay_gateway_id'] = $jsonResp->{'gateway_id'};
                    $juspaydata['juspay_bank_error_code'] = $jsonResp->{'bank_error_code'};
                    $juspaydata['juspay_bank_error_message'] = $jsonResp->{'bank_error_message'};
                    $juspaydata['created'] = date("Y-m-d H:i:s");
                   
                    $insert = $db->insert("myles_juspay_trans", $juspaydata);
                    unset($juspaydata);                 
              
            }
        }


      }
 }

}




$db->close();
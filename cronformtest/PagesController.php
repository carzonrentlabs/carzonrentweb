<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Controller\BookingsController;
use Cake\Core\Configure;
use Cake\Network\Session;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\MylesController;

class PagesController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public $helpers = ['Session'];
    public $components = ['Cookie'];

    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function home() {


        session_start();
        $session = $this->request->session();
        $sessionid = $session->id();
        //print_r($sessionid);die('hiiiii');
        $Myle = new MylesController();


        if ($this->request->is(['get']) && $this->request->query['refsite'] != "") {
            $this->loadModel('MylesLms');

            $refsite = $this->request->query['refsite'];
            $adunit = $this->request->query['adunit'];
            $channel = $this->request->query['channel'];
            $campaign = $this->request->query['campaign'];


            $session->write([
                'refsite' => $refsite,
                'adunit' => $adunit,
                'channel' => $channel,
                'campaign' => $campaign
            ]);
            $data = [
                'refsite' => $refsite,
                'adunit' => $adunit,
                'channel' => $channel,
                'campaign' => $campaign,
                'usertrack' => 'landing page'
            ];
            $MylesLms = $this->MylesLms->newEntity();
            $MylesLms = $this->MylesLms->patchEntity($MylesLms, $data);
            $lastinsertid = $this->MylesLms->save($MylesLms);
            $session->write([
                'lmsId' => $lastinsertid['id']
            ]);
        }

        /* this is for user tarck which come via url parameters end */
        /* for utm source */
        if ($this->request->is(['get']) && $this->request->query['utm_source'] != "") {
            $utm_source = $this->request->query['utm_source'];
            $utm_medium = $this->request->query['utm_medium'];
            $utm_campaign = $this->request->query['utm_campaign'];
            $session->write([
                'utm_source' => $utm_source,
                'utm_medium' => $utm_medium,
                'utm_campaign' => $utm_campaign
            ]);
        }
        /* for utm source */
        $this->layout = 'home';

        //$this->loadModel('MylesMainSliders');
        //$mylesMainSliders = $this->MylesMainSliders->find("all", ['order' => ['MylesMainSliders.slider_order' => 'ASC'], 'conditions' => ['MylesMainSliders.status' => 1]]);
        //echo "<pre>";print_r($mylesMainSliders);

        $this->loadModel('MylesSliders');
        $mylesMainSliders = $this->MylesSliders->find("all", ['order' => ['MylesSliders.slider_order' => 'ASC'], 'conditions' => ['MylesSliders.status' => 1]]);
        //echo "<pre>";print_r($mylesMainSliders);

        $this->loadModel('MylesSliders');
        $mylesSliders = $this->MylesSliders->find("all", ['order' => ['MylesSliders.imageorder' => 'ASC'], 'conditions' => ['MylesSliders.status' => 1]]);


        $this->loadModel('MylesVideos');
        $MylesVideos = $this->MylesVideos->find("all", ['order' => ['MylesVideos.videos_order' => 'ASC'], 'conditions' => ['MylesVideos.status' => 1]])->first();


        $this->loadModel('MylesNews');
        $MylesNews = $this->MylesNews->find("all", ['conditions' => ['MylesNews.status' => 1]])->first();

        $this->loadModel('MylesTestimonials');
        $mylesTestimonials = $this->MylesTestimonials->find("all", ['order' => ['MylesTestimonials.testimonials_sequence' => 'ASC'], 'conditions' => ['MylesTestimonials.status' => 1]]);

        $this->loadModel('MylesMediasContents');
        $mediahomeimage = $this->MylesMediasContents->find('all', array('conditions' => array('media_master_id' => 2, 'status' => 1, 'home_image' => 1)))->first();

        $this->loadModel('MylesCoupans');
        $mylescoupans = $this->MylesCoupans->find('all')->where(['status' => '1'])->first();




        //$this->loadModel('MylesLocations');
        //$cities = $this->MylesLocations->find('list', ['keyField' => 'location_id','valueField' => 'location_name'])->order(['MylesLocations.location_name' =>'ASC']);

        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];

		
		
        for ($j = 0; $j < count($city_arrs); $j++) {

            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }




        $this->loadModel('MylesWeeks');
        $weeks = $this->MylesWeeks->find('list', ['keyField' => 'id', 'valueField' => 'name']);
        $this->loadModel('MylesMonths');
        $months = $this->MylesMonths->find('list', ['keyField' => 'id', 'valueField' => 'name']);

        //$city_query = $this->MylesLocations->find("all", ['order' => ['MylesLocations.location_name' => 'ASC']]);
        //$city_arrs = $city_query->toArray();

        $this->loadModel('MylesMetatags');
        $action = $this->request->params['action'] == 'home' ? 'home' : 'car_list';
        $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => $action, 'status' => 1])->first();
        $this->set('pagetitle', $MylesMetatags{'title'});
        $this->set('keywords', $MylesMetatags{'keywords'});
        $this->set('description', $MylesMetatags{'descriptions'});
        $this->set('content', $MylesMetatags{'contents'});

        $book = new BookingsController();
        $this->set('userAgent', $book->check_user_agent());

        $categories = $Myle->_MYLESGetMethod('GetCarCategoryModels');
        $this->set(compact('categories', 'city_arrs', 'weeks', 'months', 'cities', 'title_for_layout', 'mylesTestimonials', 'mylesSliders', 'MylesVideos', 'MylesNews', 'mediahomeimage', 'mylesMainSliders', 'mylescoupans'));
        $this->set('cCityID', 2);
    }

    public function getPackageDataByName() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $Myle = new MylesController();
        if (!empty($_REQUEST['ClientID'])) {
            $cityval = array();

            /* $cityval = explode("(", $_REQUEST['CityName']);
              if ($cityval[0] == 'undefined') {
              //$cityid = '2';
              //$cityname = 'Delhi';
              } else {
              $cities_arr = $this->getCityNameValue($cityval[0]);
              $cityid = $cities_arr["CityId"];
              $cityname = $cities_arr["CityName"];
              } */
            $cities_arr = $this->getSearchCityNameValue($_REQUEST['CityName']);
            $cityid = $cities_arr["CityId"];
            $cityname = $cities_arr["CityName"];

            $Myle = new MylesController();
            $act_package = $Myle->_MYLESGetActivePkgType('GetActivePkgType', $_REQUEST['ClientID'], $cityid);

            $html = '<select name="package" class="book_option" onchange="javascript: _changeSFPkgType(this.value);" id="chkPkgType">';
            $i = 0;
            // $html .= '<option value = " " ></option>';
            foreach ($act_package as $row) {
                $id = $row['PkgTypeID'];
                $name = $row['PkgType'];
                $list[$id] = $name;

                if (($i == 1) || ($cityid == 69)) {
                    $html .= '<option value = "' . $id . '" selected >' . $name . '</option>';
                } else {
                    $html .= '<option value = "' . $id . '">' . $name . '</option>';
                }
                $i++;
            }
            $html .= '</select>&' . $cityid . '&' . $cityname;
            echo $html;
            exit;
        }
    }

    public function ladakh() {
        $Myle = new MylesController();
        $arr = array();
        $arr["CityID"] = 2;
        $arr["PkgType"] = "Daily";
        $ladhakserviceDetails = $Myle->_MYLESGetPackageDetailsLadakh('GetPackageDetailsLadakh', $arr);
		
		
        $this->set('ladakhdata', $ladhakserviceDetails);

        $pagetitle = 'Myles Ladakh';
        $keywords = 'Myles, Ladakh';
        $content = 'robot';
        $description = '';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
    }

    public function alliances_and_enquiry() {
        $this->set('pagetitle', 'Alliance And Enquiry || Mylescars.com');
        $this->set('keywords', 'Alliance And Enquiry');
        $this->set('description', 'Alliance And Enquiry');
        $this->set('content', '');
        if ($this->request->is('Post') && $this->request->data['fname'] != "") {
            $arr = array();
            $subject = 'Selfdrive';
            $href = 'https://www.mylescars.com/';
            $logo = 'https://www.mylescars.com/img/myles-logo.png';
            $alt = 'Mylescars logo';
            $title = 'Mylescars.com';
            $arr['fname'] = trim($this->request->data['fname']);
            $arr['brand'] = trim($this->request->data['brand']);
            $arr['mobile'] = trim($this->request->data['mobile']);
            $arr['email'] = trim($this->request->data['email']);
            $arr['enquiry'] = trim($this->request->data['enquiry']);

            /* $MylesExperience = $this->MylesExperience->newEntity();
              $MylesExperience = $this->MylesExperience->patchEntity($MylesExperience,$arr);
              $lastinsertid=$this->MylesExperience->save($MylesExperience); */
            $msg = '<table width="641" align="center" style="font-family:arial;font-size:12px;background:#e04727;padding:0px;border:4px solid #e04727;">
		<tr style="background:none; border:none;margin:0px;padding:0px;">
		<td align="left" style="height:100px;margin:0px;padding:0px;background:url(https://www.carzonrent.com/beta/mylesimages/carzonrent_top.jpg) no-repeat;" valign="top" colspan="2"><table width="100%"><tr><td align="left" width="50%" valign="middle" style="padding:20px 0 0 20px;">
		<a href="https://mylescars.com/" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/myles_conf.jpg" border="0"/></a></td>
		<td align="right" valign="middle" style="padding-top:20px;padding-right:20px;" width="50%"><table><tr><td align="right" valign="top">
		<a href="https://mylescars.com/" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/cor_number.jpg" border="0"/></a></td></tr><tr><td align="left" valign="top"><a href="https://www.carzonrent.com/" target="_blank"><img src="https://www.mylescars.com/img/marketing/mylescars.jpg" border="0"/></a></td></tr></table></td></tr></table></td></tr><tr>
		<td colspan="2" style="background:#e04727;padding:20px 30px;color:#fff;font-size:14px;"><strong>Hey ' . ucfirst($this->request->data['fname']) . ',</strong><p>
		Thank you for your details.</p><table width="50%" cellpadding="3" style="border:1px solid #fff;border-collapse:collapse;"><tr>
		<td align="left" width="45%" valign="middle" style="border:1px solid #fff;border-collapse:collapse;"><strong>Name:</strong></td>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;">' . $this->request->data['brand'] . '</td></tr><tr>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;"><strong>Email ID:</strong></td>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;">' . $this->request->data['email'] . '</td></tr><tr>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;"><strong>Mobile No:</strong></td>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;">' . $this->request->data['mobile'] . '</td></tr><tr>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;"><strong>Brand</strong></td>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;">' . $arr['brand'] . '</td></tr><tr>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;"><strong>Enquiry:</strong></td>
		<td align="left" valign="middle" style="border:1px solid #fff;border-collapse:collapse;">' . $arr['enquiry'] . '</td></tr>
		</table>
		<br>
		<div>We will be in touch shortly.</div>
		
		<br>Thanks,<br>Team Myles</td></tr><tr><td colspan="2" align="left" valign="top" style="background:url(https://www.mylescars.com/marketing_emailer/booking_footer.jpg) no-repeat;"><table cellpadding="0" cellspacing="0"><tr><td align="middle" valign="middle" width="180" style="padding-left:25px;padding-right:10px;" height="75"><a href="https://mylescars.com/" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/connect_gray.jpg" border="0"/></a></td><td align="left" valign="middle" style="padding-right:5px;">
		<a href="https://www.facebook.com/Mylescars" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/facebook_conformation.jpg" border="0"/></a></td><td align="left" valign="middle" style="padding-right:5px;">
		<a href="https://twitter.com/Myles_IN" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/twitter_conformation.jpg" border="0"/></a>
		</td><td align="left" valign="middle"><a href="https://instagram.com/mylescars/" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/instagram.jpg" border="0"/></a></td><td align="left" width="180" valign="middle"></td><td width="132" align="right" valign="middle"><a href="https://www.carzonrent.com/" target="_blank"><img src="https://www.mylescars.com/marketing_emailer/cor_logo2.png" border="0"/></a></td></tr></table></td></tr></table>';

            $arr = [];
            $arr['company'] = $this->request->data['brand'];
            $arr['phone'] = $this->request->data['mobile'];
            $arr['email'] = $this->request->data['email'];
            $arr['enquiry'] = $this->request->data['enquiry'];
            $this->loadModel('MylesAlliances');
            $MylesAlliances = $this->MylesAlliances->newEntity();
            $MylesAlliances = $this->MylesAlliances->patchEntity($MylesAlliances, $arr);
            $this->MylesAlliances->save($MylesAlliances);

            $dataToMail["To"] = "vinod.maurya@carzonrent.com;alok.singh@carzonrent.com;" . $arr['email'] . "";
            //$dataToMail["To"] = 'alok.singh@carzonrent.com'; 	 		
            $dataToMail["MailBody"] = $msg;
            $dataToMail["Subject"] = "Alliances And Enquiry";
            $Myle = new MylesController();
            $response1 = $Myle->_MYLESSendMail('SendMail', $dataToMail);
            if ($response1 > 0) {
                $msg = 'Thank you for your details. We will be in touch shortly.';
            } else {
                $msg = 'Message not send successfully.';
            }
            $this->set('msg', $msg);
        }
    }

    public function senddownloadmail() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $Myle = new MylesController();
        $mobileNo = trim($_REQUEST['mobile']);
        //$argArr['message']="Hello Myler. Your Myles account has been created successfully. Book your first Myles Car today and get Rs. 500 off with code FIRSTTIMEUSER!";
        //$Myle = new MylesController();
        // $mylesData = $Myle->_MYLESsendSMS('sendSMS', $argArr);

        $msg = "Get driving with Myles by downloading our latest app from the link:https://goo.gl/CvXbc4";
        $message2 = urlencode($msg);
        $url = "https://www.meru.co.in/wip/sendsms?username=crzrntpr&password=crzrnt987&to=" . $mobileNo . "&message=" . $message2 . "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); //Change this to a 1 to return headers
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "frnds_list=".$frnds_list);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_POST, 1);
        $data = curl_exec($ch);

        curl_close($ch);
        echo 1;
        die;
    }

    public function getPackage() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $Myle = new MylesController();
        if (!empty($_REQUEST['CityName']) && !empty($_REQUEST['ClientID'])) {
            $cityval = array();

            $cityval = explode("(", $_REQUEST['CityName']);
            if ($cityval[0] == 'undefined') {
                //$cityid = '2';
                //$cityname = 'Delhi';
            } else {
                $cities_arr = $this->getCityNameValue($cityval[0]);
                $cityid = $cities_arr["CityId"];
                $cityname = $cities_arr["CityName"];
            }

            $Myle = new MylesController();
            $act_package = $Myle->_MYLESGetActivePkgType('GetActivePkgType', $_REQUEST['ClientID'], $cityid);

            $html = '<select name="package" class="book_option" onchange="javascript: _changeSFPkgType(this.value);" id="chkPkgType">';
            $i = 0;
            $html .= '<option value = " " ></option>';
            foreach ($act_package as $row) {
                $id = $row['PkgTypeID'];
                $name = $row['PkgType'];
                $list[$id] = $name;

                if (($i == 1) || ($cityid == 69)) {
                    $html .= '<option value = "' . $id . '" >' . $name . '</option>';
                } else {
                    $html .= '<option value = "' . $id . '">' . $name . '</option>';
                }
                $i++;
            }
            $html .= '</select>';
            echo $html . '&' . $cityid . '&' . $cityname;
            exit;
        }
    }

    public function moveElement(&$array, $a, $b) {
        $out = array_splice($array, $a, 1);
        array_splice($array, $b, 0, $out);
    }

    public function array_insert(&$array, $position, $insert) {
        if (is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos = array_search($position, array_keys($array));
            $array = array_merge(
                    array_slice($array, 0, $pos), $insert, array_slice($array, $pos)
            );
        }
    }

    public function getCarbyPackage() {


        $this->layout = 'ajax';
        $this->autoRender = false;
        $pkgs_arr = array();


        $arr = array();
        $arr['CityID'] = $_REQUEST['CityID'];
        $arr['PkgType'] = $_REQUEST['PackageID'];
        $arr['CarCatName'] = $_REQUEST['CarCatName'];

        if ($_REQUEST['PackageID'] == 'Hourly') {
            $pakgeval = 'hr';
        } else if ($_REQUEST['PackageID'] == 'Daily') {
            $pakgeval = 'day';
        } else if ($_REQUEST['PackageID'] == 'Weekly') {
            $pakgeval = 'week';
        } else if ($_REQUEST['PackageID'] == 'Monthly') {
            $pakgeval = 'month';
        } else {
            $pakgeval = '';
        }


        $Myle = new MylesController();

        $pkgs_arr = $Myle->_MYLESPostMethod('GetPackageRateOfCarModels', $arr);

        $carModelIDposition = '';
        if (count($pkgs_arr) > 0) {

            foreach ($pkgs_arr as $key => $val) {

                if ($_REQUEST["carModelID"] == $val['CarModelID']) {
                    $carModelIDposition = '' . $key;
                    break;
                }
            }

            /* if ($carModelIDposition == '') {
              $queue = array(array('CarCategory' => $_REQUEST['CarCatName'],
              "CarModelID" => "blank",
              "CarModelName" => 'no_car',
              "DiscountAmount" => '0.00',
              "DiscountPercent" => '0.00',
              "PkgID" => "",
              "PkgRate" => "N/A"));

              $postionchange = intval($_REQUEST['carModelImagePosition']) - 1;

              $this->array_insert($pkgs_arr, $postionchange, $queue);
              } else if ($carModelIDposition != '') {

              } */
        }
        ?>

        <?php
        if (count($pkgs_arr) > 0) {
            count($pkgs_arr);
        }
        ?>
        <?php
        if (count($pkgs_arr) > 0) {
            $i = 1;
            ?>
            <!-- vinod  cod e-->
            <div class="mylesContaniner"><p class="carsAb"><?php echo count($pkgs_arr); ?> <?php echo $pkgs_arr[0]['CarCategory']; ?></p></div>
            <div class="wrapper wrapper--demo">
                <div id="preloader" style="display:none;"></div>
                <div class="carousel">
                    <div class="carousel__content1">

                        <div class="item">
            <?php
            for ($pk = 0; $pk < count($pkgs_arr); $pk++) {
                $carval = strtolower($pkgs_arr[$pk]["CarModelName"]);
                $record = explode("-", $carval);
                ?>

                                <div class="car f_l">
                                    <div class="carBr">
                                        <img class="carimg" style="cursor:auto;" src="<?php echo $this->request->webroot; ?>myles-campaign/car-images-big/<?php echo trim($record[1]); ?>.png" alt="<?php echo trim(ucwords($record[1])) . ' Car Rental India'; ?>"/>
                                        <div class="carTextP">
                                            <div class="f_l">
                                                <ul class="carName">

                                <?php $record = explode("-", $carval); ?>
                                                    <li><?php echo ucfirst($record[0]); ?></li>
                                                    <li><?php echo $record[1]; ?></li>
                                                </ul>
                                            </div>
                                            <div class="f_r">
                                                <ul class="carPrice">
                                                    <li>
                                                        <i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format(intval($pkgs_arr[$pk]["PkgRate"])); ?>* /<?php
                /* if ($cCityID == '69')
                  echo "day<span class='onwards'>Onwards</span>";
                  else
                  echo "hr<span class='onwards'>Onwards</span>"; */
                ?>
                                                    </li>
                                                    <li><?php echo $pakgeval; ?></li>
                                                </ul>
                                            </div>
                                            <div class="clr"></div>
                                            <div class="line"> </div>
                                            <div class="clr"></div>
                                            <div class="carAbt">
                                                <ul>
                                                    <li><img alt="Transmission" title="Transmission" src="<?php echo $this->request->webroot; ?>images/manual.png"><?php
                                                        if ($pkgs_arr[$pk]["TransmissionType"] == 0) {
                                                            echo "Manual";
                                                        } else {
                                                            echo "Automatic";
                                                        }
                                                        ?></li>
                                                    <li>Transmission</li>
                                                </ul>
                                                <ul>
                                                    <li><img alt="Seats" title="Seats" src="<?php echo $this->request->webroot; ?>images/seats.png"><?php echo $pkgs_arr[$pk]["Seats"]; ?></li>
                                                    <li>Seats</li>
                                                </ul>
                                                <ul>
                                                    <li><img alt="Fuel type" title="Fuel type" src="<?php echo $this->request->webroot; ?>images/fuelType.png"><?php echo $pkgs_arr[$pk]["FuelType"]; ?></li>
                                                    <li>Fuel type</li>
                                                </ul>
                                                <ul>
                                                    <li><img alt="Luggage" title="Luggage" src="<?php echo $this->request->webroot; ?>images/luggage.png"><?php echo $pkgs_arr[$pk]["Luggage"]; ?> </li>
                                                    <li>Luggage</li>
                                                </ul>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <?php
                if ($i > 2) {
                    ?>
                                </div><div  class="item">
                    <?php
                    $i = 0;
                }
                ?>

                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                            <?php
                        } else {
                            ?>
                        <div class="item">   
                            <!-- <div class="car f_l">
                                 <div class="carBr">
                                     <img class="carimg" src="<?php echo $this->request->webroot; ?>myles-campaign/car-images-big/sorry_icn.png"  width="100px" height="100px">
                                     <div class="carTextP">
                                         <div class="f_l">
                                             <ul class="carName">
                                                 <li></li>
                                                 <li></li>
                                             </ul>
                                         </div>
                                         <div class="f_r">
                                             <ul class="carPrice">
                                                 <li>
                                                     <i class="fa fa-inr" aria-hidden="true"></i> <?php echo 'N/A'; ?>/
                                                 </li>
                                                 <li></li>
                                             </ul>
                                         </div>
                                         <div class="clr"></div>
                                         <div class="line"> </div>
                                         <div class="clr"></div>
                                         <div class="carAbt">
                                             <ul>
                                                 <li><img alt="Transmission" title="Transmission" src="images/manual.png"></li>
                                                 <li>Transmission</li>
                                             </ul>
                                             <ul>
                                                 <li><img alt="Seats" title="Seats" src="images/seats.png"></li>
                                                 <li>Seats</li>
                                             </ul>
                                             <ul>
                                                 <li><img alt="Fuel type" title="Fuel type" src="images/fuelType.png"></li>
                                                 <li>Fuel type</li>
                                             </ul>
                                             <ul>
                                                 <li><img alt="Luggage" title="Luggage" src="images/luggage.png"></li>
                                                 <li>Luggage</li>
                                             </ul>
                                             <div class="clr"></div>
                                         </div>
                                     </div>
                                 </div>
                             </div>-->

                            <img  alogin="center" src="<?php echo $this->request->webroot; ?>myles-campaign/car-images-big/sorry_msg.png">
                        </div>     



            <?php
        }
        ?>


                </div>
        <?php
        if (count($pkgs_arr) > 3) {
            ?>
                    <div class="carousel__nav"> <a class="nav nav--left1"></a> <a class="nav nav--right1"></a> </div>
                        <?php
                    }
                    ?>
            </div>
        </div>
        <div class="clr"> </div>
        <div class="clr"> </div>


        <script>
            (function () {
                var carouselContent, carouselIndex, carouselLength, firstClone, firstItem, isAnimating, itemWidth, lastClone, lastItem;

                carouselContent = $(".carousel__content1");

                carouselIndex = 0;

                carouselLength = carouselContent.children().length;

                isAnimating = false;

                itemWidth = 100 / carouselLength;

                firstItem = $(carouselContent.children()[0]);

                lastItem = $(carouselContent.children()[carouselLength - 1]);

                firstClone = null;

                lastClone = null;

                carouselContent.css("width", carouselLength * 100 + "%");

                carouselContent.transition({
                    x: "" + (carouselIndex * -itemWidth) + "%"
                }, 0);

                $.each(carouselContent.children(), function () {
                    return $(this).css("width", itemWidth + "%");
                });

                $(".nav--left1").on("click", function () {
                    if (isAnimating) {
                        return;
                    }
                    isAnimating = true;
                    carouselIndex--;
                    if (carouselIndex === -1) {
                        lastItem.prependTo(carouselContent);
                        carouselContent.transition({
                            x: "" + ((carouselIndex + 2) * -itemWidth) + "%"
                        }, 0);
                        return carouselContent.transition({
                            x: "" + ((carouselIndex + 1) * -itemWidth) + "%"
                        }, 1000, "easeInOutExpo", function () {
                            carouselIndex = carouselLength - 1;
                            lastItem.appendTo(carouselContent);
                            carouselContent.transition({
                                x: "" + (carouselIndex * -itemWidth) + "%"
                            }, 0);
                            return isAnimating = false;
                        });
                    } else {
                        return carouselContent.transition({
                            x: "" + (carouselIndex * -itemWidth) + "%"
                        }, 1000, "easeInOutExpo", function () {
                            return isAnimating = false;
                        });
                    }
                });

                $(".nav--right1").on("click", function () {
                    if (isAnimating) {
                        return;
                    }
                    isAnimating = true;
                    carouselIndex++;
                    return carouselContent.transition({
                        x: "" + (carouselIndex * -itemWidth) + "%"
                    }, 1000, "easeInOutExpo", function () {
                        isAnimating = false;
                        if (firstClone) {
                            carouselIndex = 0;
                            carouselContent.transition({
                                x: "" + (carouselIndex * -itemWidth) + "%"
                            }, 0);
                            firstClone.remove();
                            firstClone = null;
                            carouselLength = carouselContent.children().length;
                            itemWidth = 100 / carouselLength;
                            carouselContent.css("width", carouselLength * 100 + "%");
                            $.each(carouselContent.children(), function () {
                                return $(this).css("width", itemWidth + "%");
                            });
                            return;
                        }
                        if (carouselIndex === carouselLength - 1) {
                            carouselLength++;
                            itemWidth = 100 / carouselLength;
                            firstClone = firstItem.clone();
                            firstClone.addClass("clone");
                            firstClone.appendTo(carouselContent);
                            carouselContent.css("width", carouselLength * 100 + "%");
                            $.each(carouselContent.children(), function () {
                                return $(this).css("width", itemWidth + "%");
                            });
                            return carouselContent.transition({
                                x: "" + (carouselIndex * -itemWidth) + "%"
                            }, 0);
                        }
                    });
                });







            }).call(this);
        </script>



        <!-- vinod end -->




        <?php
    }

    public function get_car_rate() {
        $this->layout = 'ajax';

        if (!empty($_REQUEST['CityID']) && !empty($_REQUEST['CarCatName'])) {
            $arr = array();
            $pkgs = array();
            $cCityID = $_REQUEST['CityID'];

            $arr['CityID'] = $_REQUEST['CityID'];

            $arr['PkgType'] = $_REQUEST['PackageID'];

            $arr['CarCatName'] = $_REQUEST['CarCatName'];

            $Myle = new MylesController();

            $pkgs = $Myle->_MYLESPostMethod('GetPackageRateOfCarModels', $arr);

            $this->set('carcategoryName', $_REQUEST['CarCatName']);

            $this->set('cCityID', $cCityID);
            $this->set(compact('pkgs'));
        }
    }

    function imgvalData() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $imgID = $_REQUEST['imgval'];
        echo $imgID;
        die;
    }

    /*  public function pickup_locations() {
      $this->layout = 'map';
      $cityName = $this->request->params['cityName']!=""? $this->request->params['cityName']:$_REQUEST['cityName'];
      $this->loadModel('MylesMetatags');
      $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => $cityName, 'status' => 1])->first();
      $this->set('pagetitle', $MylesMetatags{'title'});
      $this->set('keywords', $MylesMetatags{'keywords'});
      $this->set('description', $MylesMetatags{'descriptions'});
      $this->set('content', $MylesMetatags{'contents'});
      $this->set(compact('cCityID', 'cityName', 'title_for_layout'));
      } */

    // for  pickup loaction  page
    public function pickup_locations() {
        $this->layout = 'map';
        $cityNameUrl = trim($this->request->params['id'], '-');
        $Myle = new MylesController();
        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];
        for ($j = 0; $j < count($city_arrs); $j++) {

            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }
        $res = array_search(ucfirst($cityNameUrl), $cities);
        if ($res) {
            $cityName = lcfirst($cities[$res]);
            $cityId = $res;
        }
        $this->loadModel('MylesMetatags');
        $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => $cityName, 'status' => 1])->first();
        $this->set('pagetitle', $MylesMetatags{'title'});
        $this->set('keywords', $MylesMetatags{'keywords'});
        $this->set('description', $MylesMetatags{'descriptions'});
        $this->set('content', $MylesMetatags{'contents'});
        $this->loadModel('MylesCitydescriptions');
        $MylesCitydescriptions = $this->MylesCitydescriptions->find("All")->where(['cityName' => ucfirst($cityName), 'status' => 1])->first();
        $this->set('citydescriptions', $MylesCitydescriptions{'cityDescription'});
        $this->set('cities', $cities);
        //for city//
        $controller = $this->request->params['controller'];
        $this->set('controller', $controller);
        $this->set('cityId', $cityId);
        $this->set(compact('cityName', 'title_for_layout'));
        $this->render('pickup_locations');
    }

    /* public function pickup_locations() 
      {
      $this->layout = 'map';
      $cityName = $this->request->params['cityName']!=""?$this->request->params['cityName']:$_REQUEST['cityName'];
      $this->loadModel('MylesMetatags');
      $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => $cityName, 'status' => 1])->first();
      $this->set('pagetitle', $MylesMetatags{'title'});
      $this->set('keywords', $MylesMetatags{'keywords'});
      $this->set('description', $MylesMetatags{'descriptions'});
      $this->set('content', $MylesMetatags{'contents'});
      $this->loadModel('MylesCitydescriptions');
      $MylesCitydescriptions = $this->MylesCitydescriptions->find("All")->where(['cityName' => ucfirst($cityName), 'status' => 1])->first();
      $this->set('citydescriptions', $MylesCitydescriptions{'cityDescription'});
      //for city//
      $Myle = new MylesController();
      $city_query = $Myle->_MYLESfetchCities('fetchCities');
      $city_arrs = $city_query["response"];
      for ($j = 0; $j < count($city_arrs); $j++) {

      $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
      }
      $this->set('cities',$cities);
      //for city//
      $cityId = $this->request->params['cCityID'];
      $controller = $this->request->params['controller'];
      $this->set('controller',$controller);
      $this->set('cityId',$cityId);
      $this->set(compact('cityId', 'cityName', 'title_for_layout'));
      } */

    // for  pickup loaction  page
    public function terms_of_use() {
        $action = $this->request->action;
        $segment = 'terms_of_use';
        $this->loadModel('MylesCms');
        $MylesCms = $this->MylesCms->find('All')->where(['status' => 1, 'action' => $segment])->first();
        $pagetitle = $MylesCms{'seo_pagetitle'};
        $keywords = $MylesCms{'seo_keywords'};
        $description = $MylesCms{'seo_description'};
        $content = 'robot';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('MylesCms', $MylesCms);
    }
    	public function terms_of_use_app() {
		$this->layout = false;
        $segment = 'terms_of_use';
        $this->loadModel('MylesCms');
        $MylesCms = $this->MylesCms->find('All')->where(['status' => 1, 'action' => $segment])->first();
        $this->set('MylesCms', $MylesCms);
		$this->render('terms_of_use');
    }
    public function privacy_policy() {
        $action = $this->request->action;
        $segment = 'privacy_policy';
        $this->loadModel('MylesCms');
        $MylesCms = $this->MylesCms->find('All')->where(['status' => 1, 'action' => $segment])->first();
        $pagetitle = $MylesCms{'seo_pagetitle'};
        $keywords = $MylesCms{'seo_keywords'};
        $description = $MylesCms{'seo_description'};
        $content = 'robot';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('MylesCms', $MylesCms);
    }

    public function contact_us() {
        $action = $this->request->action;
        $segment = 'contact_us';
        $this->loadModel('MylesCms');
        $MylesCms = $this->MylesCms->find('All')->where(['status' => 1, 'action' => $segment])->first();
        $pagetitle = $MylesCms{'seo_pagetitle'};
        $keywords = $MylesCms{'seo_keywords'};
        $description = $MylesCms{'seo_description'};
        $content = 'robot';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('MylesCms', $MylesCms);
    }

    public function what_is_myles() {
        $action = $this->request->action;
        $segment = 'what_is_myles';
        $this->loadModel('MylesCms');
        $MylesCms = $this->MylesCms->find('All')->where(['status' => 1, 'action' => $segment])->first();

        $pagetitle = $MylesCms{'seo_pagetitle'};
        $keywords = $MylesCms{'seo_keywords'};
        $description = $MylesCms{'seo_description'};
        $content = 'robot';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('MylesCms', $MylesCms);
    }

    public function search() {
        
    }

    public function sitemap() {
        $pagetitle = 'Myles Sitemap';
        $keywords = 'Myles, Sitemap';
        $content = 'robot';
        $description = '';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
    }

    public function faq() {
        $pagetitle = 'FAQs - Car Rental Services, Self Drive Car Rental - Myles ';
        $keywords = 'Myles, FAQ, Mylescars';
        $content = 'robot';
        $description = 'Find the answers to your questions about car rental, self drive car booking, rent a car services from Myles.';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
    }

	public function franchise() {
        $this->layout = false;
        $this->loadModel('MylesMetatags');
        $this->loadModel('MylesFranchisestate');
        $statere = $this->MylesFranchisestate->find('all', array('order' => 'state asc'));

        $Myle = new MylesController();
        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];

        for ($j = 0; $j < count($city_arrs); $j++) {

            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }


        $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => 'franchise', 'status' => 1])->first();
		
        $this->set('pagetitle', $MylesMetatags{'title'});
        $this->set('keywords', $MylesMetatags{'keywords'});
        $this->set('description', $MylesMetatags{'descriptions'});
        $this->set('content', $MylesMetatags{'contents'});
        $this->set('state', $statere);
    }
	
	/**
    public function franchise() {
        $this->loadModel('MylesMetatags');
        $Myle = new MylesController();
        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];

        for ($j = 0; $j < count($city_arrs); $j++) {

            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }


        $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => 'franchise', 'status' => 1])->first();
        $this->set('pagetitle', $MylesMetatags{'title'});
        $this->set('keywords', $MylesMetatags{'keywords'});
        $this->set('description', $MylesMetatags{'descriptions'});
        $this->set('content', $MylesMetatags{'contents'});
        $this->set('cities', $city_arrs);
    }
	*/

    public function gallery() {
        $pagetitle = 'Myles Franchise - Self-Drive Car Rental Service';
        $keywords = 'Myles Franchise - Self-Drive Car Rental Service';
        $content = '';
        $description = '';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->layout = 'gallery';
    }

    public function term_and_condition() {
        $pagetitle = '';
        $keywords = '';
        $content = '';
        $description = '';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
    }

    public function referralprogram() {
        $this->layout = 'referral';
        $pagetitle = 'Referral Program';
        $keywords = 'Referral Program';
        $content = 'Referral Program';
        $description = '';
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
    }
    ////////////////\\\\\\\\\\\\\\feedback ///////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public function myles_feedback() {
        $session = $this->request->session();
		$this->loadModel('MylesFeedbacks');
        $this->layout = 'feedback';
        $pagetitle = 'Myles Feedback || mylescars.com';
        $keywords = 'Myles Feedback || mylescars.com';
        $content = 'robot';
        $description = '';
        $bookingid = base64_decode($this->request->query['bookingid']);
        $Myle = new MylesController();
        $valRecord = $Myle->_MYLESGetBookingsDetail('GetBookingDetail', $bookingid);
		$cityName = $valRecord[0]['OriginName'];
        $ModelName = $valRecord[0]['ModelName'];
        $SublocationName = $valRecord[0]['SublocationName'];
		$drop_date = $valRecord[0]['DropOffDate'];
		//for link expire//
        $add_days = 5;
        $dropdatecheck = strtotime(date('Y-m-d', strtotime($drop_date) + (24 * 3600 * $add_days)));
        $currentdatecheck = strtotime(date('Y-m-d'));
        $expire = "";
        if ($dropdatecheck <= $currentdatecheck) {
            $expire = 'expiredlink';
        }
		//for link expire//
		
		//already given feedback
		$MylesFeedbacks = $this->MylesFeedbacks->find("All")->where(['booking_id' => $bookingid])->hydrate(false)->first();
        //already given feedback
        $this->set('expireday', $expire);
        $this->set('deatils', $MylesFeedbacks);
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('cityName', $cityName);
        $this->set('ModelName', $ModelName);
        $this->set('SublocationName', $SublocationName);
        if ($this->request->is('Post')) {
            $arr['model_name'] = $this->request->data['ModelName'];
            $arr['location_name'] = $this->request->data['SublocationName'];
            $arr['city'] = $this->request->data['cityName'];
            $arr['booking_id'] = base64_decode($this->request->data['bookingid']);
            $arr['book_now'] = $this->request->data['booknow'];
            $arr['payment_exp'] = $this->request->data['Paymentexperience'];
            $arr['car_quality'] = $this->request->data['Carquality'];
            $arr['handling_car'] = $this->request->data['Timelyhandeling'];
            $arr['executive_behaviour'] = $this->request->data['Behaviourofoperation'];
            $arr['customer_support'] = $this->request->data['CustomerSupport'];
			$arr['overallRating'] = $this->request->data['overallrating'];
			$arr['drivingquality'] = $this->request->data['Drivingquality'];
            $arr['reasonnotlisted'] = $this->request->data['Reasonnotlisted'];
			$arr['comment'] = $this->request->data['comment'];
            $arr['status'] = 1;   		
            $MylesFeedbacks = $this->MylesFeedbacks->newEntity();
            $MylesFeedbacks = $this->MylesFeedbacks->patchEntity($MylesFeedbacks, $arr);
            $lastinsertid = $this->MylesFeedbacks->save($MylesFeedbacks);
            if ($lastinsertid) {
                echo 'success';
                exit;
            }
        }
    }

    function captcha_code() {
        $session = $this->request->session();
        $random_alpha = md5(rand());
        $captcha_code = substr($random_alpha, 0, 3);
        $session->write([
            'captcha_code' => $captcha_code
        ]);
        $target_layer = imagecreatetruecolor(70, 30);
        $captcha_background = imagecolorallocate($target_layer, 255, 160, 119);
        imagefill($target_layer, 0, 0, $captcha_background);
        $captcha_text_color = imagecolorallocate($target_layer, 0, 0, 0);
        imagestring($target_layer, 5, 5, 5, $captcha_code, $captcha_text_color);
        header("Content-type: image/jpeg");
        echo imagejpeg($target_layer);
        exit;
    }

    ////////////////\\\\\\\\\\\\\\feedback end ///////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function press_release() {
        $this->layout = 'media';
        $pagetitle = 'Media Coverage';
        $keywords = 'Media Coverage';
        $content = 'Media Coverage';
        $description = 'Media Coverage';
        $this->loadModel('MylesMedias');
        $MylesMedias = $this->MylesMedias->find('all', array('conditions' => array('status' => 1)))->order(['id' => 'asc']);
        $mediamaster_id = 1;
        $this->loadModel('MylesMediasContents');
        $MylesMediasContents = $this->MylesMediasContents->find('all', array('conditions' => array('media_master_id' => $mediamaster_id, 'status' => 1)))->order(['order_no' => 'asc']);
        $content_media = '';
        $content_media .='<div class="tab-content active"><div class="imageRange">';
        foreach ($MylesMediasContents as $MylesMediasContents) {
            $content_media .='<div class="w_32 f_l mb2">
                <iframe width="100%" height="200" src=' . $MylesMediasContents{'youtube_url'} . ' frameborder="0" allowfullscreen></iframe>
            </div>';
        }
        $content_media .='</div></div>';

        $this->set('MylesMedias', $MylesMedias);
        $this->set('pagetitle', $pagetitle);
        $this->set('keywords', $keywords);
        $this->set('description', $description);
        $this->set('content', $content);
        $this->set('content_media', $content_media);
    }

    public function press_release_ajax() {
        $this->layout = 'media';
        $mediamaster_id = intval($this->request->data['mediamasterid']);
        $title = $this->request->data['title'];
        $this->loadModel('MylesMediasContents');
        $MylesMediasContents = $this->MylesMediasContents->find('all', array('conditions' => array('media_master_id' => $mediamaster_id, 'status' => 1)))->order(['order_no' => 'asc']);
        $content_media = '';
        $content_media .='<div class="tab-content active"><div class="imageRange">';
        foreach ($MylesMediasContents as $MylesMediasContents) {
            $content_media .='<div class="w_32 f_l mb2">';
            if ($title == 'ELECTRONICS') {
                $content_media .='<iframe width="100%" height="200" src=' . $MylesMediasContents{'youtube_url'} . ' frameborder="0" allowfullscreen></iframe> <hr/>';
            } else if ($title == 'PRINT') {
                $content_media .='<a href="javascript:void(0);" class="videoBox fancybox.iframe" id=' . $this->request->webroot . 'media/' . $MylesMediasContents{'media_image'} . ' onclick="show_popup(this.id);"><img src=' . $this->request->webroot . 'media/thumb/' . $MylesMediasContents{'media_image_thumb'} . ' alt="' . $MylesMediasContents{'alttag'} . '" title="' . $MylesMediasContents{'imagetitle'} . '" style="height: 192px;width: 317px;"></a> <hr/>';
            } else if ($title == 'ONLINE') {
                $content_media .='<a href=' . $MylesMediasContents{'online_media_url'} . ' class="videoBox fancybox.iframe" target="_blank">
                    <img src=' . $this->request->webroot . 'media/' . $MylesMediasContents{'media_image'} . ' alt="' . $MylesMediasContents{'alttag'} . '" title="' . $MylesMediasContents{'imagetitle'} . '" style="height: 192px;width: 317px;"></a> <hr/>';
            }

            $content_media .='</div>';
        }
        $content_media .='</div></div>';
        echo $content_media;
        exit;
    }

    //////////////////////////faq\\\\\\\\\\\\\\\\\\\\\\\
    function faqs() {
        $pagetitle = 'FAQ';
        $keywords = 'FAQ';
        $content = 'FAQ';
        $description = '';
        $content_faq = '';
        $this->loadModel('MylesFaqs');
        $faqsmaster = $this->MylesFaqs->find('all', array('conditions' => array('status' => 1)))->order(['id' => 'asc']);
        $this->set('faqsmaster', $faqsmaster);
        $faqmaster_id = 1;
        $this->loadModel('MylesFaqsContents');
        $MylesFaqsContents = $this->MylesFaqsContents->find('all', array('conditions' => array('faq_master_id' => $faqmaster_id, 'status' => 1)))->order(['faq_order' => 'asc']);
        $i = 0;
        foreach ($MylesFaqsContents as $MylesFaqsContents) {
            if ($i == 0) {
                $content_faq .= '<div class="accordion-item open" id=que_' . $MylesFaqsContents{'id'} . ' onclick="hide_show(this.id);">
			<img class="imgs" id=down_' . $MylesFaqsContents{'id'} . ' src="' . $this->request->webroot . 'img/downArrow.png">
			' . $MylesFaqsContents{'faq_question'} . '
			</div>';
                $content_faq .='<div class="data" style="display: block;" id=ans_' . $MylesFaqsContents{'id'} . '>' . $MylesFaqsContents{'faq_answer'} . '</div>';
            } else {
                $content_faq .= '<div class="accordion-item open" id=que_' . $MylesFaqsContents{'id'} . ' onclick="hide_show(this.id);">
			<img  class="imgs" id=down_' . $MylesFaqsContents{'id'} . ' src="' . $this->request->webroot . 'img/arrowRight.png">
			' . $MylesFaqsContents{'faq_question'} . '
			</div>';
                $content_faq .='<div class="data" style="display: none;" id=ans_' . $MylesFaqsContents{'id'} . '>' . $MylesFaqsContents{'faq_answer'} . '</div>';
            }
            $i++;
        }
        $this->loadModel('MylesMetatags');
        $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => 'faqs', 'status' => 1])->first();
        $this->set('pagetitle', $MylesMetatags{'title'});
        $this->set('keywords', $MylesMetatags{'keywords'});
        $this->set('description', $MylesMetatags{'descriptions'});
        $this->set('content', $MylesMetatags{'contents'});
        $this->set('faqsmaster', $faqsmaster);
        $this->set('content_faq', $content_faq);
    }

    function faqs_ajax() {
        $content_faq = '';
        $faqmaster_id = intval($this->request->data['faqmasterid']);
        $this->loadModel('MylesFaqsContents');
        $MylesFaqsContents = $this->MylesFaqsContents->find('all', array('conditions' => array('faq_master_id' => $faqmaster_id, 'status' => 1)))->order(['faq_order' => 'asc']);
        $i = 0;
        foreach ($MylesFaqsContents as $MylesFaqsContents) {
            if ($i == 0) {
                $content_faq .= '<div class="accordion-item open" id=que_' . $MylesFaqsContents{'id'} . ' onclick="hide_show(this.id);">
			<img class="imgs" id=down_' . $MylesFaqsContents{'id'} . ' src="' . $this->request->webroot . 'img/downArrow.png">
			' . $MylesFaqsContents{'faq_question'} . '
			</div>';
                $content_faq .='<div class="data" style="display: block;" id=ans_' . $MylesFaqsContents{'id'} . '>' . $MylesFaqsContents{'faq_answer'} . '</div>';
            } else {
                $content_faq .= '<div class="accordion-item open" id=que_' . $MylesFaqsContents{'id'} . ' onclick="hide_show(this.id);">
			<img class="imgs" id=down_' . $MylesFaqsContents{'id'} . ' src="' . $this->request->webroot . 'img/arrowRight.png">
			' . $MylesFaqsContents{'faq_question'} . '
			</div>';
                $content_faq .='<div class="data" style="display: none;" id=ans_' . $MylesFaqsContents{'id'} . '>' . $MylesFaqsContents{'faq_answer'} . '</div>';
            }
            $i++;
        }
        echo $content_faq;
        exit;
    }

    public function getSearchCityNameValue($cityname = null) {

        $cities = [];
        $Myle = new MylesController();
        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];

        for ($j = 0; $j < count($city_arrs); $j++) {
            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }

        foreach ($cities as $key => $value) {

            if (strtolower($value) == strtolower($cityname))
                $city_arr = ['CityId' => $key, 'CityName' => $value];
        }

        return $city_arr;
    }

    //////////////////////////faq\\\\\\\\\\\\\\\\\\\\\\\

    public function printdoc($val = 1) {
        $this->layout = 'print';
        $this->set('val', $val);
    }

    public function car_list() {
        $this->redirect('/');
    }

    public function getCityNameValue($cityname = null) {
        $cities = [];
        $Myle = new MylesController();
        $city_query = $Myle->_MYLESfetchCities('fetchCities');
        $city_arrs = $city_query["response"];

        for ($j = 0; $j < count($city_arrs); $j++) {
            $cities[$city_arrs[$j]['CityId']] = $city_arrs[$j]['CityName'];
        }

        foreach ($cities as $key => $value) {
            if ($value == $cityname)
                $city_arr = ['CityId' => $key, 'CityName' => $value];
        }

        return $city_arr;
    }
	
	
	 public function checkHomePickUpDropOffAvailability() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $Myle = new MylesController();
        if (!empty($_REQUEST['CityID'])) {


            $arr = array();
            $arr["CityID"] = $_REQUEST['CityID'];

            $result = $Myle->_MYLESGetListOfZones('CheckHomePickUpDropOffAvailability', $arr);


            if ($result == 1) {
                ?>
                <span class="homeimage"><img src="images/doorstep_delivery_icn.png" height="100%" alt="Doorstep Delivery Icon" /></span>
                <span class="hometext">HOME PICK-UP/DROP-OFF<br />AFTER CAR SELECTION
                <?php
            }
        }
        exit;
    }
	
	public function franchisefaq() {
            $this->layout = false;

            $this->loadModel('MylesMetatags');
            $this->loadModel('MylesFranchisestate');
            $statere = $this->MylesFranchisestate->find()->all();


            $MylesMetatags = $this->MylesMetatags->find("All")->where(['action' => 'franchise', 'status' => 1])->first();
            $this->set('pagetitle', $MylesMetatags{'title'});
            $this->set('keywords', $MylesMetatags{'keywords'});
            $this->set('description', $MylesMetatags{'descriptions'});
            $this->set('content', $MylesMetatags{'contents'});
            $this->set('state', $statere);
        }
		
		 public function city() {
            $this->layout = 'ajax';
            $this->autoRender = false;

            $this->loadModel('MylesFranchisecity');
            $stateid = $_REQUEST['stateid'];
          
            $franchisecity = $this->MylesFranchisecity->find('all', array('order' => 'city asc'))->where(['stateid' => $stateid]);
            ?>
            <label>*City of your choice</label>        
            <select name="city" id='city' >
                <option value="0" >Select City</option>
                <?php
                foreach ($franchisecity as $key => $val) {
                  
                    ?>


                        <option value="<?php echo $val->id; ?>"><?php echo $val->city; ?></option>
                 

                    <?php
                }
                ?>
            </select>
                <?php
                exit;
            }

        

}

<?php
    $fld = "";
    $val = "";
    $isDtInc = false;
    $isFiltered = false;
    $sortFld = "unique_id";
    $sortMode = "desc";
    $sortModeC = SORT_DESC;
    $sDate = date_create(date("Y-m-d"));
    $eDate = date_create(date("Y-m-d"));
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
    $cPage = $_GET["pg"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "")
        $sDate = date_create($_GET["sd"]);
        
    if(isset($_GET["ed"]) && $_GET["ed"] != "")
        $eDate = date_create($_GET["ed"]);
    
    if(isset($_GET["f"]) && $_GET["f"] != ""){
        $fld = $_GET["f"];
        $isFiltered = true;
    }
    
    if(isset($_GET["q"]) && $_GET["q"] != ""){
        $fVal = $_GET["q"];
        $isFiltered = true;
    }
    
    if(isset($_GET["inc"]) && $_GET["inc"] != ""){
        $isDtInc = true;
        $isFiltered = true;
    }
    if(isset($_GET["sf"]) && $_GET["sf"] != "")
    $sortFld = $_GET["sf"];
    
    if(isset($_GET["sm"]) && $_GET["sm"] != ""){
        $sortMode = $_GET["sm"];
        if(strtolower($_GET["sm"]) == "asc")
        $sortModeC = SORT_ASC;
    }
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "" && isset($_GET["ed"]) && $_GET["ed"] != "")                   
        $filename = "myles_visits_vs_enquiries" . $sDate->format('d-m-Y') . "_" . $eDate->format('d-m-Y');
    else
        $filename = "myles_visits_vs_enquiries_" . $sDate->format('d-m-Y');
    
    $filename .= ".csv";
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // fetch the data
    include_once("../admin/includes/config.php");
    include_once("../admin/classes/bb-mysql.class.php");

    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    if($fld != "" && $fVal != ""){
        $sql = "SELECT UPPER(ref_code) as ref_code, COUNT(uid) as 'visits' FROM cor_camaign_tracking WHERE " . $fld . " = '" . $fVal . "'";
        if($isDtInc){
            $sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ";
        }
        $sql .= " GROUP BY ref_code";
    } else {
        $sql = "SELECT UPPER(ref_code) as ref_code, COUNT(uid) as 'visits' FROM cor_camaign_tracking WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' GROUP BY ref_code";
    }
    $totVisits = 0;
    $totEnquiries = 0;
    $r = $db->query("query", $sql);
    if(!array_key_exists("response", $r)){
        for($i = 0; $i < count($r); $i++){
            $totVisits += $r[$i]["visits"];
            if($r[$i]["ref_code"] != "")
            $sql = "SELECT COUNT(unique_id) as 'conversions' FROM myles_enquiry WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND ref_code = '" . $r[$i]["ref_code"] . "'";
            else
            $sql = "SELECT COUNT(unique_id) as 'conversions' FROM myles_enquiry WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND ref_code IS NULL";
            $conv = $db->query("query", $sql);
            if(!array_key_exists("response", $conv)){
                $r[$i]["conversions"] = $conv[0]["conversions"];
                $totEnquiries += $conv[0]["conversions"];
            } else {
                $r[$i]["conversions"] = "0";
            }
            unset($conv);
        }
    }
    $fSort = array();
    foreach($r as $k => $v)
        $fSort[$k] = $v[$sortFld];
    array_multisort($fSort, $sortModeC, $r);  
    // loop over the rows, outputting them
    if(!array_key_exists("response", $r)){
        // output the column headings
        $fieldNames = array();
        $fieldNames = array_keys($r[0]);
        fputcsv($output, $fieldNames);
        unset($fieldNames);
        for($i = 0;$i< count($r);$i++){        
            
            $fieldValues = array();
            $fieldValues = $r[$i];
            fputcsv($output, $fieldValues);
            unset($fieldValues);
        }
    }
?>
<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');
include_once('classes/rest.php');
$url = 'https://myles.carzonrent.com/services/mylesservice.svc/';
$service_url = $url . "GetTrnsDetails";
$Myle = new Myles();

$db = new MySqlConnection(CONNSTRING);
$db->open();

$book_query = "SELECT l.coric,l.order_id,sec_deposit,booking_id,b.id,l.id paylogid,l.platform,w.wallet_amount "
        . "FROM myles_payment_logs l INNER JOIN myles_bookings b ON b.coric=l.coric INNER JOIN myles_wallet_trans w ON b.coric= w.coric "
        . "WHERE l.booking_status = 0 AND l.payment_source_id=2 AND pg_source='Wallet' and l.status='Success'";
//	AND "
//     . "final_status_update_time <= DATE_ADD(Now(), INTERVAL '-5' MINUTE) AND "
//    . "final_status_update_time >= DATE_ADD(Now(), INTERVAL '-15' MINUTE) AND "
//     . "final_status_update_time > first_status_create_time AND pg_source='JusPay'";

$book = $db->query("query", $book_query);

//echo '<pre>';
//print_r($book);
if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];

        $data = array();
        ////////////////////////
        $data['coricId'] = $book[$i]["coric"];
        $data['PaymentSource'] = 'S';
        $curl = curl_init($service_url);
        $data_json = json_encode($data);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);


        $curl_response = curl_exec($curl);


        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);
//        echo '<pre>';
//        print_r($decoded);
//        echo $book[$i]["coric"];
//        echo "<br/>";
//        echo $book[$i]["order_id"];
//        echo '<pre>';
//        print_r($decoded['response'][0]['OrderID']);
//        echo "<br/>";
//        die;
        

        if (($decoded['response'][0]['OrderID'] != $book[$i]["order_id"]) && ($decoded['response'][0]['OrderID'] != '')) {
            
        } else {




            if (($book[$i]['sec_deposit'] == $book[$i]['wallet_amount']) && ($decoded['response'][0]['OrderID'] == '')) {

                ///////////////////////////////////////////Wallet Data/////////////////////////////
//                    echo "bb";
//            die;
                // insert data into SQLSERVER
                // security
                //////////////////////////////////////

                $wallet_query = "SELECT * FROM myles_wallet_trans WHERE coric='" . $book[$i]['coric'] . "' AND payment_source_id = 2";

                $walletResultArray = $db->query("query", $wallet_query);

echo '<pre>';
print_r($walletResultArray);

                if (!array_key_exists("response", $walletResultArray)) {
                    $walletResult = $walletResultArray[0];
                    $datcreate = date_create($walletResult["wallet_transaction_time"]);
                    $wallet_amount = $walletResult["wallet_amount"];
                    $walletdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
                    $walletdata["TransactionPlatform"] = $walletResult["platform"];
                    $walletdata["TransactionType"] = 1;
                    $walletdata["ReferenceID"] = $walletResult["coric"];
                    $walletdata["TransactionModeID"] = 1;
                    $walletdata["PaymentSourceID"] = $walletResult['payment_source_id'];
                    $walletdata["OrderID"] = $walletResult["wallet_order_id"];
                    $walletdata["Amount"] = $walletResult["wallet_amount"];
                    $walletdata["ResposeCode"] = $walletResult['wallet_ref_id'];
                    $walletdata["OtherTrnsDetails"] = 0;
                    $walletdata["isActive"] = 1;
                    $walletdata["CreatedBy"] = $walletResult["wallet_created_by"];


                    $json_val = "";


                    if (count($walletdata) > 0) {
                        $walletdata["BookingID"] = intval($book[$i]['booking_id']);
                        $json_val .= json_encode($walletdata);
                    }
                    print_r($walletdata);
                    //die;
                    $arr = '{"jsonData":[' . $json_val . ']}';
                    $service_url = $url . 'preAuthPayment';
//                        echo '$service_url = ' . $service_url;
                    ///////////////////////////////////

                    $curl = curl_init($service_url);
                    $data_json = json_encode($arr);

//                        echo '<pre>';
//                        print_r($data_json);
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_json))
                    );
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    $curl_response = curl_exec($curl);

                    curl_close($curl);
                    $decoded = json_decode($curl_response, true);

                    /////////////////////////////////


                    $response = $decoded['status'];

                    if (intval($response) == 1) {
//                            echo "abc";
//                            die;
                        $dataToSave = array();
                        $where = array();
                        $where["coric"] = $book[$i]["coric"];
                        $dataToSave["preauth_status"] = "Paid";

                        $up = $db->update("myles_bookings", $dataToSave, $where);
                        $where1 = array();
                        $where1["id"] = $book[$i]["paylogid"];
//                             echo '<pre>';
//                            print_r($where1);

                        $payment_log1 = array();
                        $payment_log1['booking_status'] = 1;
//                            print_r($payment_log1);
                        $up = $db->update("myles_payment_logs", $payment_log1, $where1);
                    }
                }
            }
        }
    }
}

$db->close();


<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');
include_once('classes/rest.php');
$url = 'https://myles.carzonrent.com/services/mylesservice.svc/';
$service_url = $url . "GetTrnsDetails";
$JUSPAY_URL = "https://sandbox.juspay.in/";
$JUSPAY_SEQ_PASS = "083F91A5FDAC45AE99B956C308F10128";


$Myle = new Myles();

$db = new MySqlConnection(CONNSTRING);
$db->open();

$book_query = "SELECT l.coric,l.order_id,sec_deposit,booking_id,b.id,l.id paylogid,l.platform FROM myles_payment_logs l "
        . "INNER JOIN myles_bookings b ON b.coric=l.coric where l.booking_status = 0 AND "
        . "l.payment_source_id=2 AND pg_source='JusPay' AND status='CHARGED'";
//	AND "
//     . "final_status_update_time <= DATE_ADD(Now(), INTERVAL '-5' MINUTE) AND "
//    . "final_status_update_time >= DATE_ADD(Now(), INTERVAL '-15' MINUTE) AND "
//     . "final_status_update_time > first_status_create_time AND pg_source='JusPay'";

$book = $db->query("query", $book_query);

//echo '<pre>';
//print_r($book);
if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];

        $data = array();
        ////////////////////////
        $data['coricId'] = $book[$i]["coric"];
        $data['PaymentSource'] = 'S';
        $curl = curl_init($service_url);
        $data_json = json_encode($data);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);


        $curl_response = curl_exec($curl);


        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);
//        echo '<pre>';
//        print_r($decoded);
//        echo $book[$i]["coric"];
//        echo "<br/>";
//        echo $book[$i]["order_id"];
//        echo '<pre>';
//        print_r($decoded['response'][0]['OrderID']);
//        echo "<br/>";
//        die;
        $juspay_url = $JUSPAY_URL;
        ///////////t/////////get unique id and amount from juspay api//
        $curl = curl_init($juspay_url . 'order_status');
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('orderId' => $book[$i]["order_id"], 'merchantId' => 'mylescarssecurity'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERPWD, $JUSPAY_SEQ_PASS);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $jsonRespon = json_decode(curl_exec($curl));
        if (($decoded['response'][0]['OrderID'] != $book[$i]["order_id"]) && ($decoded['response'][0]['OrderID'] != '')) {

//            echo "a";
//            die;
            // echo '<pre>';
            // print_r($jsonRespon);
            if ($jsonRespon->{'refunded'} != 1) {
//                echo '<pre>';
//                print_r($jsonRespon);
//                echo $jsonRespon->{'txnId'};
//                echo '<br/>';
//               echo $jsonRespon->{'amount'};
                ////////////////////

                refundJuspay($book[$i]["order_id"], $jsonRespon->{'txnId'}, $jsonRespon->{'amount'}, $JUSPAY_SEQ_PASS, $book[$i]["coric"], $book[$i]["paylogid"], $book[$i]['platform']);
            }
        } else {

            if ($jsonRespon->{'refunded'} != 1) {


                if ($book[$i]['sec_deposit'] == $jsonRespon->{'amount'}) {
//                    echo "bb";
//            die;
                    // insert data into SQLSERVER
                    // security
                    //////////////////////////////////////
                    $juspayTransCheckQuery = "select * from myles_juspay_trans where juspay_order_id='" . $jsonRespon->{'orderId'} . "'";
                    $rjuspay = $db->query("query", $juspayTransCheckQuery);
                    if (!array_key_exists("response", $juspayTransCheckQuery)) {

                        $whereJuspayUpdate['juspay_order_id']=$jsonRespon->{'orderId'};
                        $juspay['coric'] = $book[$i]["coric"];
                        $juspay['platform'] = 'JusPay';
                        $juspay['payment_source_id'] = 2;
                        $juspay['juspay_merchant_id'] = $jsonRespon->{'merchantId'};
                        $juspay['juspay_customer_id'] = $jsonRespon->{'customerId'};
                        $juspay['juspay_order_id'] = $jsonRespon->{'orderId'};
                        $juspay['juspay_status'] = $jsonRespon->{'status'};

                        $juspay['juspay_amount'] = $jsonRespon->{'amount'};
                        $juspay['juspay_refunded'] = $jsonRespon->{'refunded'};
                        $juspay['juspay_amount_refunded'] = $jsonRespon->{'amount_refunded'};

                        $juspay['juspay_trans_id'] = $jsonRespon->{'txnId'};
                        $juspay['juspay_gateway_id'] = $jsonRespon->{'gatewayId'};
                        $juspay['juspay_bank_error_code'] = $jsonRespon->{'bankErrorCode'};
                        $juspay['juspay_bank_error_message'] = $jsonRespon->{'bankErrorMessage'};
                        $juspay['created'] = date("Y-m-d H:i:s");
                        $up = $db->update("myles_juspay_trans", $juspay, $whereJuspayUpdate);

                    } else {
                        $juspay['coric'] = $book[$i]["coric"];
                        $juspay['platform'] = 'JusPay';
                        $juspay['payment_source_id'] = 2;
                        $juspay['juspay_merchant_id'] = $jsonRespon->{'merchantId'};
                        $juspay['juspay_customer_id'] = $jsonRespon->{'customerId'};
                        $juspay['juspay_order_id'] = $jsonRespon->{'orderId'};
                        $juspay['juspay_status'] = $jsonRespon->{'status'};

                        $juspay['juspay_amount'] = $jsonRespon->{'amount'};
                        $juspay['juspay_refunded'] = $jsonRespon->{'refunded'};
                        $juspay['juspay_amount_refunded'] = $jsonRespon->{'amount_refunded'};

                        $juspay['juspay_trans_id'] = $jsonRespon->{'txnId'};
                        $juspay['juspay_gateway_id'] = $jsonRespon->{'gatewayId'};
                        $juspay['juspay_bank_error_code'] = $jsonRespon->{'bankErrorCode'};
                        $juspay['juspay_bank_error_message'] = $jsonRespon->{'bankErrorMessage'};
                        $juspay['created'] = date("Y-m-d H:i:s");

                        $db->insert('myles_juspay_trans', $juspay);
                    }


                    $juspay_query = "SELECT * FROM myles_juspay_trans WHERE coric='" . $book[$i]["coric"] . "' AND payment_source_id = 2";
                    $rjuspay = $db->query("query", $juspay_query);
                    if (array_key_exists(0, $rjuspay)) {
                        $juspay = $rjuspay[0];
                        $datcreate = date_create($juspay["created"]);
                        $juspay_amount = $juspay["juspay_amount"];
                        $juspaydata['TransactionDateTime'] = $datcreate->format('Y-m-d H:i:s');
                        $juspaydata["TransactionPlatform"] = $juspay["platform"];
                        $juspaydata["TransactionType"] = 1;
                        $juspaydata["ReferenceID"] = $juspay["coric"];
                        $juspaydata["TransactionModeID"] = 2;
                        $juspaydata["PaymentSourceID"] = $juspay['payment_source_id'];
                        $juspaydata["OrderID"] = $juspay["juspay_order_id"];
                        $juspaydata["Amount"] = $juspay["juspay_amount"];
                        $juspaydata["ResposeCode"] = $juspay['juspay_trans_id'];
                        $juspaydata["OtherTrnsDetails"] = 0;
                        $juspaydata["isActive"] = 1;
                        $juspaydata["CreatedBy"] = $juspay["juspay_customer_id"];

                        $json_val = "";

                        if (count($juspaydata) > 0) {
                            $juspaydata["BookingID"] = $book[$i]["booking_id"];
                            $json_val .= json_encode($juspaydata);
                        }
                        $arr = '{"jsonData":[' . $json_val . ']}';
                        $service_url = $url . 'preAuthPayment';
//                        echo '$service_url = ' . $service_url;
                        ///////////////////////////////////

                        $curl = curl_init($service_url);
                        $data_json = json_encode($arr);

//                        echo '<pre>';
//                        print_r($data_json);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_json))
                        );
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                        $curl_response = curl_exec($curl);

                        curl_close($curl);
                        $decoded = json_decode($curl_response, true);

                        /////////////////////////////////


                        $response = $decoded['status'];

                        if (intval($response) == 1) {
//                            echo "abc";
//                            die;
                            $dataToSave = array();
                            $where = array();
                            $where["coric"] = $book[$i]["coric"];
                            $dataToSave["preauth_status"] = "Paid";

                            $up = $db->update("myles_bookings", $dataToSave, $where);
                            $where1 = array();
                            $where1["id"] = $book[$i]["paylogid"];
//                             echo '<pre>';
//                            print_r($where1);

                            $payment_log1 = array();
                            $payment_log1['booking_status'] = 1;
//                            print_r($payment_log1);
                            $up = $db->update("myles_payment_logs", $payment_log1, $where1);
                        }
                    }
                    ///////////////////////////////////////////
                    // $juspay_query = $db->select('all', ['conditions' => ['MylesJuspayTrans.coric' => $bookings["coric"], 'MylesJuspayTrans.payment_source_id' => $session->read('payment_source_id')]]);
                    //$juspay = $juspay_query->first();
                } else {
                    //echo "cc";
                    //die;
                    // refund 
                    refundJuspay($book[$i]["order_id"], $jsonRespon->{'txnId'}, $jsonRespon->{'amount'}, $JUSPAY_SEQ_PASS, $book[$i]["coric"], $book[$i]["paylogid"], $book[$i]['platform']);
                }
            }
        }

        ////////////////////////
    }
}

$db->close();

function refundJuspay($order_id, $txnId, $amount, $JUSPAY_SEQ_PASS, $coric, $id, $platform) {

    $db = new MySqlConnection(CONNSTRING);
    $db->open();

    //////////////////////////////Start Wallet Refund ////////////////////////////
    echo $wallet_query = "SELECT * FROM myles_wallet_trans WHERE coric='" . $coric . "' and payment_source_id =2 AND wallet_status='Success'";

    $walletresult = $db->query("query", $wallet_query);

echo '<pre>';
print_r($walletresult);
    $WALLET_URL = "http://180.179.146.81/wallet/v1/";


    $WALLET_PASS = "ef32ae830b2f65e4006baa5e9d6b35f4:d8c5d4a2f108d0c72abe3541f4c10a29";

    if (!array_key_exists("response", $walletresult)) {
        $walletresultArray = $walletresult[0];
        echo '<pre>';
        print_r($walletresultArray);
//die;
        //////////////////////////
        $consumer_id = $walletresultArray['wallet_consumer_id'];
        $shmart_refID = $walletresultArray['wallet_ref_id'];
        $walletOrderId = $walletresultArray['wallet_order_id']; 
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $WALLET_URL . "debits/reverse",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $consumer_id . "\",\r\n\"shmart_refID\":\"" . $shmart_refID . "\"}\r\n",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . base64_encode($WALLET_PASS),
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));
//KIndly replace the value NzJjOGY2YjVjYzhhODE2N2E1YTFlMTdjYTE2NTVlOGY6OWU5ZGVlN2QwNzhmYjRmM2ZkMjVkMWMyZjU1ZTA0MTY= with base64_encode("user:password") as it takes base64 encoding
        $response = curl_exec($curl);
        echo '<pre>';
        print_r($response);
        //  die;
        $err = curl_error($curl);

        curl_close($curl);

        $responseArray = json_decode($response);
        if ($responseArray->status == 'success') {
            $refArray['pg_destination'] = 'Wallet';
            $refArray['platform'] = $platform;
            $refArray['coric'] = $coric;
            $refArray['payment_source_id'] = 2;
            $refArray['order_id'] = $walletresultArray['wallet_order_id'];
            $refArray['amount'] = $walletresultArray['wallet_amount'];
            $refArray['status'] = 'REFUNDED';
            $refArray['created'] = date("Y-m-d H:i:s");
            echo 'Wallet ';
            echo '<br/>';
            echo '<pre>';
            print_r($refArray);
            $insert = $db->insert("myles_refund_logs", $refArray);

            unset($refArray);
            unset($insert);
             if ($walletOrderId != 0) {
                    $dataToSave = array();
                    $where = array();
                    $where["order_id"] = $walletOrderId;
                    $dataToSave["booking_status"] = "2";
                    $up = $db->update("myles_payment_logs", $dataToSave, $where);
                }

//            $resval = $this->debitreversal($walletoddetalis['wallet_consumer_id'], $walletdetalis['wallet_ref_id']);
//            $walletResponse = json_decode($resval);
//            $walletUpdateData = array();
////            if (intval($wallletResponse->{'status'}) == 'success') {
//            $whereWalletUpdate['id'] = $walletresultArray['id'];
//            $walletUpdateData['wallet_status'] = 'REFUNDED';
//            $walletUpdateData['wallet_amount'] = 0;
//            $walletUpdateData['wallet_amount_refunded'] = $walletresultArray["wallet_amount"];
//            $walletUpdateData['wallet_message'] = $responseArray->message;
//
//
//            $up = $db->update("myles_wallet_trans", $walletUpdateData, $whereWalletUpdate);
        }



        //////////////////////////////
    }



    /////////////////////////////End Wallet Refund //////////////////////////////

    $JUSPAY_URL = "https://sandbox.juspay.in/";
    $juspayRefundArray = array('order_id' => $order_id, 'unique_request_id' => $txnId, 'amount' => $amount);
    echo '<pre>';
    print_r($juspayRefundArray);
    $ch = curl_init($JUSPAY_URL . 'order/refund');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, $JUSPAY_SEQ_PASS);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $juspayRefundArray);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);

    $jsonResponse = json_decode(curl_exec($ch));
    print_r($jsonResponse);
    $err = curl_error($ch);

    $refdata = array();
    $payment_log = array();
    if (intval($jsonResponse->{'refunded'}) == 1) {

        $refArray['pg_destination'] = 'JusPay';
        $refArray['platform'] = $platform;
        $refArray['coric'] = $coric;
        $refArray['payment_source_id'] = 2;
        $refArray['order_id'] = $jsonResponse->{'order_id'};
        $refArray['amount'] = $jsonResponse->{'amount_refunded'};
        $refArray['status'] = 'REFUNDED';
        $refArray['created'] = date("Y-m-d H:i:s");

        $insert = $db->insert("myles_refund_logs", $refArray);

        unset($refArray);
        unset($insert);
        
        
//        $refArray['pg_destination'] = 'JusPay';
//        $refArray['platform'] = $platform;
//        $refArray['coric'] = $coric;
//        $refArray['payment_source_id'] = 2;
//        $refArray['order_id'] = $jsonResponse->{'order_id'};
//        $refArray['amount'] = $jsonResponse->{'amount_refunded'};
//        $refArray['status'] = 'REFUNDED';
//        $refArray['created'] = date("Y-m-d H:i:s");
//
//        $insert = $db->insert("myles_refund_logs", $refArray);



        $where = array();
        $where["id"] = $id;
        $payment_log['booking_status'] = 2;
        $up = $db->update("myles_payment_logs", $payment_log, $where);
    }






    $db->close();
}

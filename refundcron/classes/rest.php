<?php
ini_set("soap.wsdl_cache_enabled", "0");
  
    class Myles{
       // var $url = 'https://api.mylescars.com/Services/MylesService.svc/';
       // var $auth_url = 'https://oauth.mylescars.com/Services/MylesTokenService.svc/';
	   
	    var $url = 'https://myles.carzonrent.com/services/mylesservice.svc/';
        var $auth_url = 'https://mylesaccesstoken.carzonrent.com/Services/MylesTokenService.svc/';
	   
	   
        public function ValidAuth($CustomerID = -1) {
        

		if(isset($_SESSION['sessId']) && !empty($_SESSION['sessId'])) {
            $sessId = $_SESSION['sessId'];
        } else {
			session_start();
            $sessionid = session_id();
			$_SESSION['sessId'] = $sessionid;
            $sessId = $_SESSION['sessId'];
        }  
		$data = array(
                'userID' => 'web',
                'password' => md5('WEB@@MYLES'),
                'IPRestriction' => 0,
                'customerID' => $CustomerID,
                'sessionID' => $sessId
            );
			
		
			
		$a_url = $this->auth_url.'RestW1AuthenticateUser';
		
		$curl = curl_init($a_url);
		$data_json = json_encode($data);
		
		
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curl_response = curl_exec($curl);
		
		
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($curl_response, 0, $header_size);


		$body = substr($curl_response, $header_size);


        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);

        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
		//echo '<pre>';print_r($decoded)	;die;
		$_SESSION['apikey'] = $decoded['accessToken'];
		
    }

    public function UpdateCustomerAgainstKey($CustomerID) {
        
        $data = array(
            'tokenKey' => $_SESSION['apikey'],
            'customerID' => $CustomerID,
            'sessionID' => $_SESSION['sessId'],
        );

        $a_url = $this->auth_url.'RestW1UpdateCustomerIDAgainstKey';
        $curl = curl_init($a_url);
        $data_json = json_encode($data);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);

        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        return $decoded;
    }

    public function getData($service_url = null) {

        if (isset($_SESSION['CustomerID']) && !empty($_SESSION['CustomerID'])) {
            $CustomerID = $_SESSION['CustomerID'];
        } else {
            $CustomerID = -1;
        }
		
		

        if (isset($_SESSION['apikey']) && !empty($_SESSION['apikey'])) {
            $key = $_SESSION['apikey'];
        } else {
            $this->ValidAuth($CustomerID);
            $key = $_SESSION['apikey'];
        }
		
		
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'hashVal:' . $key,
            'sessionID:' . $_SESSION['sessId'],
            'customerID:' . $CustomerID)
        );
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            $decoded = array();
            return $decoded;
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        return $decoded;
		
		
    }

    public function postData($service_url = null, $data = array()) {

         
        if (isset($_SESSION['CustomerID']) && !empty($_SESSION['CustomerID'])) {
            $CustomerID = $_SESSION['CustomerID'];
        } else {
            $CustomerID = -1;
        }
		
		

        if (isset($_SESSION['apikey']) && !empty($_SESSION['apikey'])) {
            $key = $_SESSION['apikey'];
        } else {
            $this->ValidAuth($CustomerID);
            $key = $_SESSION['apikey'];
        }
	

        $curl = curl_init($service_url);
        $data_json = json_encode($data);

		
		
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'hashVal:' . $key,
            'sessionID:' . $_SESSION['sessId'],
            'customerID:' . $CustomerID,
            'Content-Length: ' . strlen($data_json))
        );
		
	
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
		
        $decoded = json_decode($curl_response, true);

        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }

        return $decoded;
    }
        
        function _MYLESSendMail($method = null, $arr = array()) {
            $service_url = $this->url . $method;
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        function _MYLESCreateBookingFromWebSite($method = null, $arr = array()) {
            $service_url = $this->url . $method;
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }

        function _MYLESUpdatePayment($method = null, $arr = array()) {
            $service_url = $this->url . $method;
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
		function _MYLESGetPaymentStatusByBookingID($method = null, $bookingID = null) {
			$service_url = $this->url . $method . '/' . $bookingID;
			
			try {
				$response = $this->getData($service_url);
				return $response;
			} catch (Exception $e) {
				print $e->getMessage();
			}
        }
		
        function _MYLESpreAuthCreation($method = null, $arr = array()) {
            $service_url = $this->url . $method;
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
		
		
		function _MYLESGetRentalDetails($method = null, $arr = array()) {			 
			 $service_url = $this->url . $method;			 
            
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
		
		function _MYLESGetTrnsDetails($method = null, $arr = array()) {			 
			 $service_url = $this->url . $method;			 
            
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
		
		function _MYLESsendSMS($method = null, $arr = array()) {			 
			 $service_url = $this->url . $method;			 
            
            try {
                $response = $this->postData($service_url, $arr);
                return $response;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
		
		function _MYLESGetMultipleModePaymentsStatus($method = null, $coric = null) {
            $service_url = $this->url . $method . '/' . $coric;
			try {
				$response = $this->getData($service_url);
				return $response;
			} catch (Exception $e) {
				print $e->getMessage();
			}
        }
		
		function _MYLESGetConsumerID($method = null, $arr = array()) {
			$service_url = $this->url . $method;
			try {
				$response = $this->postData($service_url, $arr);

				return $response;
			} catch (Exception $e) {
				print $e->getMessage();
			}
		}
    };
?>
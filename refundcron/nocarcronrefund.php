<?php

//error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');
//include_once('Paynimo/gateway.php');

$Myle = new Myles();
$arr = array();


$db = new MySqlConnection(CONNSTRING);
$db->open();



$book = $db->query("stored procedure", "myles_paid_and_failed_refund()");



if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        $totfare = $book[$i]['tot_fare'];
        $full_name = $book[$i]["full_name"];
        $mobile = $book[$i]["mobile"];
        $email_id = $book[$i]["email_id"];
        $model_name = $book[$i]["model_name"];

        $juspay_url = 'https://sandbox.juspay.in/';
        $juspay_key = 'D774B8A9E3D3452D9F8AADBB53497221';

        $wallet_url = 'http://180.179.146.81/wallet/v1/';
        $wallet_pass = 'ef32ae830b2f65e4006baa5e9d6b35f4:d8c5d4a2f108d0c72abe3541f4c10a29';

        $juspaydata = array();
        $paybackdata = array();
        $walletdata = array();
        $discountdata = array();
        $juspay = array();
        $payback = array();
        $wallet = array();
        $discount = array();

        $juspay_amount = 0;
        $redeem_amount = 0;
        $wallet_amount = 0;
        $discount_amount = 0;
        $juspay_query = "SELECT * FROM  myles_juspay_trans WHERE coric= '" . $coric . "' AND payment_source_id = '1' AND juspay_status = 'CHARGED' ORDER BY created DESC";

        $rjuspay = $db->query("query", $juspay_query);
        //echo '<br/>';
        //echo '<pre>';
        // print_r($rjuspay);
        if (array_key_exists(0, $rjuspay)) {
            $juspay = $rjuspay[0];
        }
        if ((count($juspay) > 0) && ($juspay["juspay_status"] == "CHARGED")) {
            $datcreate = date_create($juspay["created"]);
            $juspay_amount = $juspay["juspay_amount"];
            $juspaydata['TransactionDateTime'] = $datcreate->format('Y-m-d H:i:s');
            $juspaydata["TransactionPlatform"] = $juspay["platform"];
            $juspaydata["TransactionType"] = 1;
            $juspaydata["ReferenceID"] = $juspay["coric"];
            $juspaydata["TransactionModeID"] = 2;
            $juspaydata["PaymentSourceID"] = $juspay['payment_source_id'];
            $juspaydata["OrderID"] = $juspay["juspay_order_id"];
            $juspaydata["Amount"] = $juspay["juspay_amount"];
            $juspaydata["ResposeCode"] = $juspay['juspay_trans_id'];
            $juspaydata["OtherTrnsDetails"] = 0;
            $juspaydata["isActive"] = 1;
            $juspaydata["CreatedBy"] = $juspay["juspay_customer_id"];
        }


        $payback_query = "SELECT * FROM  myles_payback_trans WHERE coric= '" . $coric . "' AND payment_source_id = '1'";

        $rpayback = $db->query("query", $payback_query);

        if (array_key_exists(0, $rpayback)) {
            $payback = $rpayback[0];

            if ((count($payback) > 0) && ($payback["redeem_amount"] != '')) {
                $datcreate = date_create($payback["created"]);
                $redeem_amount = $payback["redeem_amount"];
                $paybackdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
                $paybackdata["TransactionPlatform"] = $payback["platform"];
                $paybackdata["TransactionType"] = 1;
                $paybackdata["ReferenceID"] = $payback["coric"];
                $paybackdata["TransactionModeID"] = 4;
                $paybackdata["PaymentSourceID"] = $payback['payment_source_id'];
                $paybackdata["OrderID"] = $payback["payback_order_id"];
                $paybackdata["Amount"] = $payback["redeem_amount"];
                $paybackdata["ResposeCode"] = $payback['payback_trans_id'];
                $paybackdata["OtherTrnsDetails"] = $payback['amount_in_card'];
                $paybackdata["isActive"] = 1;
                $paybackdata["CreatedBy"] = $full_name;
            }
        }
        $wallet_query = "SELECT * FROM  myles_wallet_trans WHERE coric= '" . $coric . "' AND wallet_status = 'Success' AND payment_source_id = '1' ORDER BY created DESC";

        $rwallet = $db->query("query", $wallet_query);

        if (array_key_exists(0, $rwallet)) {
            $wallet = $rwallet[0];
        }


        if ((count($wallet) > 0) && ($wallet["wallet_status"] == 'Success')) {
            $datcreate = date_create($wallet["wallet_transaction_time"]);
            $wallet_amount = $wallet["wallet_amount"];
            $walletdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
            $walletdata["TransactionPlatform"] = $wallet["platform"];
            $walletdata["TransactionType"] = 1;
            $walletdata["ReferenceID"] = $wallet["coric"];
            $walletdata["TransactionModeID"] = 1;
            $walletdata["PaymentSourceID"] = $wallet['payment_source_id'];
            $walletdata["OrderID"] = $wallet["wallet_order_id"];
            $walletdata["Amount"] = $wallet["wallet_amount"];
            $walletdata["ResposeCode"] = $wallet['wallet_ref_id'];
            $walletdata["OtherTrnsDetails"] = 0;
            $walletdata["isActive"] = 1;
            $walletdata["CreatedBy"] = $wallet["wallet_created_by"];
        }

        $discount_query = "SELECT * FROM  myles_discounts WHERE coric= '" . $coric . "' AND payment_source_id = '1'";


        $rdiscount = $db->query("query", $discount_query);
        //echo '<br/>';
        //echo '<pre>';
        //print_r($rdiscount);
        if (array_key_exists(0, $rdiscount)) {
            $discount = $rdiscount[0];
        }

        if ((count($discount) > 0) && (intval($discount["discount_amount"]) > 0)) {
            $datcreate = date_create($discount["created"]);
            $discount_amount = $discount["discount_amount"];
            $discountdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
            $discountdata["TransactionPlatform"] = $discount["platform"];
            $discountdata["TransactionType"] = 1;
            $discountdata["ReferenceID"] = $discount["coric"];
            $discountdata["TransactionModeID"] = 3;
            $discountdata["PaymentSourceID"] = $discount['payment_source_id'];
            $discountdata["OrderID"] = $discount["discount_order_id"];
            $discountdata["Amount"] = $discount["discount_amount"];
            $discountdata["ResposeCode"] = $discount['discount_code'];
            $discountdata["OtherTrnsDetails"] = 0;
            $discountdata["isActive"] = 1;
            $discountdata["CreatedBy"] = $full_name;
        }
        //echo 'totfare = ' . $totfare . '<br/>';
        $totalFromAllNode = $juspay_amount + $redeem_amount + $wallet_amount;
        if ($totfare == $totalFromAllNode) {

            $rarr = array();
            $rarr['coricId'] = $coric;
            $rarr['PaymentSource'] = 'R';
            $result = $Myle->_MYLESGetTrnsDetails('GetTrnsDetails', $rarr);



            if ($result['status'] == '0') {

                if (count($juspaydata) > 0) {

                    $payment_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id= '" . $juspaydata['OrderID'] . "' AND logs.payment_source_id = '1' AND logs.booking_status=1 AND logs.pg_source = 'JusPay'";

                    $rjuspay = $db->query("query", $payment_query);
                    if (array_key_exists(0, $rjuspay)) {
                        $juspay = $rjuspay[0];
                        /* Update Status Start */
                        if ((count($juspay) > 0)) {

                            $where = array();
                            $where["id"] = $juspay['id'];
                            $payment_log['booking_status'] = '0';
                            $up = $db->update("myles_payment_logs", $payment_log, $where);
                            unset($payment_log);
                            unset($where);

                            /* Update Status End */

                            $curl = curl_init($juspay_url . 'order_status');
                            curl_setopt($curl, CURLOPT_POSTFIELDS, array('orderId' => $juspay['order_id'], 'merchantId' => 'mylescars'));
                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($curl, CURLOPT_USERPWD, $juspay_key);
                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                            $jsonRespon = json_decode(curl_exec($curl));


                            if ($jsonRespon->{'status'} == 'CHARGED') {

                                $ch = curl_init($juspay_url . 'order/refund');
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_USERPWD, $juspay_key);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, array('order_id' => $jsonRespon->{'orderId'}, 'unique_request_id' => $jsonRespon->{'txnId'},
                                    'amount' => $jsonRespon->{'amount'}));
                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 15);

                                $jsonResponse = json_decode(curl_exec($ch));

                                if (intval($jsonResponse->{'refunded'}) == 1) {
                                    $juspaydata = array();
                                    $amt = $jsonResponse->{'amount_refunded'};
                                    $juspaydata['pg_destination'] = 'Juspay';
                                    $juspaydata['platform'] = $juspay["platform"];
                                    $juspaydata['coric'] = $juspay["coric"];
                                    $juspaydata['payment_source_id'] = $juspay["payment_source_id"];
                                    $juspaydata['order_id'] = $jsonResponse->{'order_id'};
                                    $juspaydata['amount'] = $jsonResponse->{'amount_refunded'};
                                    $juspaydata['status'] = 'REFUNDED';
                                    $juspaydata['created'] = date("Y-m-d H:i:s");

                                    $insert = $db->insert("myles_refund_logs", $juspaydata);

                                    $arrSMS = array();

                                    $arrSMS['mobileNo'] = $mobile;
                                    $arrSMS['message'] = "Refund of Rs. " . intval($amt) . " has been initiated for your failed transaction having unique reference id: " . $jsonResponse->{'order_id'};


                                    $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);

                                    unset($juspaydata);
                                    unset($insert);


                                    $where = array();
                                    $where["id"] = $juspay['id'];
                                    $payment_log['booking_status'] = '2';
                                    $up = $db->update("myles_payment_logs", $payment_log, $where);
                                    unset($payment_log);
                                    unset($where);



                                    unset($arrSMS);
                                    unset($mobsms);

                                    $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
									<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
										<tr style="background:#fff;">
											<td align="left" valign="middle" style="padding:10px;">
												<a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
											</td>
											<td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
												<a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
												<a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
													</a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
											</td>
										</tr>		
									</table>

									<table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
										<tr style="background:#fff;">
									<td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
										<img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
									</td>
									</tr>
										<tr style="padding:0px 0px; display: block">
											<td valign="top" style="padding: 0px 10px;">
											  <p style="line-height: 18px;">Hi <strong>' . $full_name . '</strong>,</p>
											  <p style="line-height: 18px;">We are really sorry that your booking of ' . $model_name . ' has failed.</p>
												<p style="line-height: 18px;">We have initiated refund for Rs. ' . $amt . ' into your account. Your Bank/Card provider may take up to 7-8 days to credit in your account.</p>
												<p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
											</td>
										</tr>
									  
									   
									</table>

								  

									<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
									<tr>
											<td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
											</td>
										</tr>
										<tr>
											<td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
											</td>
										</tr>
								</table>
								</td>
								</tr>
								</table>
										
								</div>';

                                    $dataToMail = array();
                                    $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
                                    $dataToMail["Subject"] = "JusPay Refund";
                                    $dataToMail["MailBody"] = $html;

                                    $Myle->_MYLESSendMail('SendMail', $dataToMail);
                                    unset($dataToMail);
                                    unset($amt);
                                    unset($email_id);
                                }
                            }
                        }
                    }
                }

                if (count($walletdata) > 0) {

                    $payment_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id= '" . $walletdata['OrderID'] . "' AND logs.payment_source_id = '1' AND logs.booking_status=1 AND logs.pg_source = 'Wallet'";

                    $rwallet = $db->query("query", $payment_query);
                    if (array_key_exists(0, $rwallet)) {
                        $wallet = $rwallet[0];
                        /* Update Status Start */
                        if ((count($wallet) > 0)) {
                            $where = array();
                            $where["id"] = $wallet['id'];
                            $payment_log['booking_status'] = '0';
                            $up = $db->update("myles_payment_logs", $payment_log, $where);
                            unset($payment_log);
                            unset($where);
                            unset($wallet);
                        }
                        /* Update Status End */
                    }

                    $wallet_query = "SELECT logs.id,logs.platform,logs.order_id,logs.payment_source_id,logs.coric,wt.wallet_consumer_id,wt.wallet_ref_id,wt.wallet_status,wt.wallet_amount FROM myles_payment_logs logs,myles_wallet_trans wt WHERE logs.order_id= '" . $walletdata['OrderID'] . "' AND logs.payment_source_id = '1' AND logs.order_id = wt.wallet_order_id AND logs.booking_status=0 AND logs.pg_source = 'Wallet'";

                    $rwallet = $db->query("query", $wallet_query);

                    if (array_key_exists(0, $rwallet)) {
                        $wallet = $rwallet[0];

                        if ((count($wallet) > 0) && ($wallet["wallet_status"] == 'Success') && (trim($respon['OrderID']) == ($wallet['order_id']))) {

                            $curl = curl_init();

                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $wallet_url . "debits/reverse",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $wallet['wallet_consumer_id'] . "\",\r\n\"shmart_refID\":\"" . $wallet['wallet_ref_id'] . "\"}\r\n",
                                CURLOPT_HTTPHEADER => array(
                                    "authorization: Basic " . base64_encode($wallet_pass),
                                    "cache-control: no-cache",
                                    "content-type: application/json"
                                ),
                            ));

                            $response = curl_exec($curl);

                            $walletResponse = json_decode($response);

                            if (strtolower($walletResponse->{'status'}) == 'success') {
                                $walletdata = array();
                                $amt = $wallet['wallet_amount'];
                                $walletdata['pg_destination'] = 'Wallet';
                                $walletdata['platform'] = $wallet["platform"];
                                $walletdata['coric'] = $wallet["coric"];
                                $walletdata['payment_source_id'] = $wallet["payment_source_id"];
                                $walletdata['order_id'] = $wallet['order_id'];
                                $walletdata['amount'] = $wallet['wallet_amount'];
                                $walletdata['status'] = 'REFUNDED';
                                $walletdata['created'] = date("Y-m-d H:i:s");

                                $insertwallet = $db->insert("myles_refund_logs", $walletdata);
                                
                                $arrSMS = array();
                                $arrSMS['mobileNo'] = $mobile;
                                $arrSMS['message'] = "Refund of Rs. " . intval($amt) . " has been initiated for your failed transaction having unique reference id: " . $wallet['order_id'];
                                $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);

                                unset($walletdata);
                                unset($insertwallet);


                                $where = array();
                                $where["id"] = $wallet['id'];
                                $payment_log['booking_status'] = '2';
                                $up = $db->update("myles_payment_logs", $payment_log, $where);
                                unset($payment_log);
                                unset($where);

                                

                                unset($arrSMS);
                                unset($mobsms);

                                $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
									<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
										<tr style="background:#fff;">
											<td align="left" valign="middle" style="padding:10px;">
												<a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
											</td>
											<td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
												<a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
												<a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
													</a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
											</td>
										</tr>		
									</table>

									<table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
										<tr style="background:#fff;">
									<td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
										<img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
									</td>
									</tr>
										<tr style="padding:0px 0px; display: block">
											<td valign="top" style="padding: 0px 10px;">
											  <p style="line-height: 18px;">Hi <strong>' . $full_name . '</strong>,</p>
											  <p style="line-height: 18px;">We are really sorry that your booking of ' . $model_name . ' has failed.</p>
												<p style="line-height: 18px;">Your Wallet has been credited with Rs. ' . $amt . '.</p>
												<p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
											</td>
										</tr>
									  
									   
									</table>

								  

									<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
									<tr>
											<td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
											</td>
										</tr>
										<tr>
											<td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
											</td>
										</tr>
								</table>
								</td>
								</tr>
								</table>
										
								</div>';

                                $dataToMail = array();
                                $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
                                $dataToMail["Subject"] = "Wallet Refund";
                                $dataToMail["MailBody"] = $html;

                                $Myle->_MYLESSendMail('SendMail', $dataToMail);
                                unset($dataToMail);
                                unset($amt);
                                unset($email_id);
                            }
                        }
                    }
                }

                if (count($paybackdata) > 0) {

                    $payment_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id= '" . $paybackdata['OrderID'] . "' AND logs.payment_source_id = '1' AND logs.booking_status=1 AND logs.pg_source = 'PayBack'";

                    $rpayback = $db->query("query", $payment_query);
                    if (array_key_exists(0, $rpayback)) {
                        /* Update status start */
                        $payback = $rpayback[0];
                        if ((count($payback) > 0)) {
                            $where = array();
                            $where["id"] = $payback['id'];
                            $payment_log['booking_status'] = '0';
                            $up = $db->update("myles_payment_logs", $payment_log, $where);
                            unset($payment_log);
                            unset($where);
                        }
                        /* Update status end */
                    }

                    $payment_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id= '" . $paybackdata['OrderID'] . "' AND logs.payment_source_id = '1' AND logs.booking_status=0 AND logs.pg_source = 'PayBack'";

                    $rpayback = $db->query("query", $payment_query);

                    if (array_key_exists(0, $rpayback)) {
                        $payback = $rpayback[0];
                        if ((count($payback) > 0) && (intval($payback["amount"]) > 0) && (trim($respon['OrderID']) == ($payback['order_id']))) {

                            //PayBack Refund

                            $url = "https://pbiwstest.payback.in/PBExternalServices/v1/soap?wsdl";

                            try {
                                $soapClient = new \SoapClient($url);
                                $soapClient->__setLocation($url);
                            } catch (SoapFault $E) {
                                echo $E->faultstring;
                            }



                            try {

                                $params = new \stdClass;
                                $params->OrderId = $payback['trans_id'];
                                $params->RedemptionChannel = 'PONL';
                                $params->RefundPoints = $payback['amount'] * 4;
                                $params->PartnerExtReferenceNumber = $payback['order_id'];
                                $params->RefundRemarks = "Concelled by Cron User";
                                $result = $soapClient->ReverseVoucherRedemption($params);


                                if ($result->ReferenceTransactionId != '') {


                                    $paybackdata = array();
                                    $amt = $payback['amount'] * 4;
                                    $paybackdata['pg_destination'] = 'PayBack';
                                    $paybackdata['platform'] = 'WebSite';
                                    $paybackdata['coric'] = $payback["coric"];
                                    $paybackdata['payment_source_id'] = 1;
                                    $paybackdata['order_id'] = $payback['order_id'];
                                    $paybackdata['amount'] = $payback['amount'];
                                    $paybackdata['status'] = 'REFUNDED';
                                    $paybackdata['created'] = date("Y-m-d H:i:s");

                                    $insertpayback = $db->insert("myles_refund_logs", $paybackdata);
                                    $arrSMS = array();

                                    $arrSMS['mobileNo'] = $mobile;
                                    $arrSMS['message'] = "Refund of Point " . intval($amt) . " has been initiated for your failed transaction having unique reference id: " . $payback['order_id'];
                                    $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);


                                    unset($paybackdata);
                                    unset($insertpayback);


                                    $where = array();
                                    $where["id"] = $payback['id'];
                                    $payment_log['booking_status'] = '2';
                                    $payment_log['trans_id'] = $result->ReferenceTransactionId;
                                    $up = $db->update("myles_payment_logs", $payment_log, $where);
                                    unset($payment_log);
                                    unset($where);

                                    
                                    unset($arrSMS);
                                    unset($mobsms);

                                    $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
												<tr style="background:#fff;">
													<td align="left" valign="middle" style="padding:10px;">
														<a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
													</td>
													<td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
														<a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
														<a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
															</a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
													</td>
												</tr>		
											</table>

											<table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
												<tr style="background:#fff;">
											<td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
												<img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
											</td>
											</tr>
												<tr style="padding:0px 0px; display: block">
													<td valign="top" style="padding: 0px 10px;">
													  <p style="line-height: 18px;">Hi <strong>' . $name . '</strong>,</p>
													    <p style="line-height: 18px;">We are really sorry that your booking of ' . $model_name . ' has failed.</p>
														<p style="line-height: 18px;">Your payback account has been credited with ' . $amt . ' Points.</p>
														<p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
													</td>
												</tr>
											  
											   
											</table>

										  

											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
											<tr>
													<td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
													</td>
												</tr>
												<tr>
													<td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
													</td>
												</tr>
										</table>
										</td>
										</tr>
										</table>
												
										</div>';

                                    $dataToMail = array();
                                    $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
                                    $dataToMail["Subject"] = "PayBack Refund";
                                    $dataToMail["MailBody"] = $html;

                                    $Myle->_MYLESSendMail('SendMail', $dataToMail);
                                    unset($dataToMail);
                                    unset($amt);
                                    unset($email_id);
                                }
                            } catch (Exception $ex) {
                                
                            }
                        }
                    }
                }
            }
        }
    }
}

$db->close();

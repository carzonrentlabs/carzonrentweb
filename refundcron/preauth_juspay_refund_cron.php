<?php

//error_reporting(E_ALL);
//sini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');
include_once('classes/rest.php');
$url = 'https://myles.carzonrent.com/services/mylesservice.svc/';
$service_url = $url . "GetTrnsDetails";
$JUSPAY_URL = "https://sandbox.juspay.in/";
$JUSPAY_SEQ_PASS = "083F91A5FDAC45AE99B956C308F10128";


$Myle = new Myles();

$db = new MySqlConnection(CONNSTRING);
$db->open();

echo $book_query = "SELECT l.coric,l.order_id,sec_deposit,booking_id,b.id,l.id paylogid,l.platform,b.full_name,b.mobile,b.email_id,b.model_name "
 . "FROM "
 . "myles_payment_logs l "
 . "INNER JOIN myles_bookings b ON b.coric=l.coric where l.booking_status = 0 AND "
 . "l.payment_source_id=2 AND pg_source='JusPay' AND status='CHARGED' AND "
     . "final_status_update_time <= DATE_ADD(Now(), INTERVAL '310' MINUTE) AND "
    . "final_status_update_time >= DATE_ADD(Now(), INTERVAL '300' MINUTE)";

$book = $db->query("query", $book_query);

//echo '<pre>';
//print_r($book);
if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        $name = $book[$i]['full_name'];
        $mobile = $book[$i]['mobile'];
        $email_id = $book[$i]['email_id'];
        $model_name = $book[$i]['model_name'];
        $data = array();
        ////////////////////////
        $data['coricId'] = $book[$i]["coric"];
        $data['PaymentSource'] = 'S';
        $curl = curl_init($service_url);
        $data_json = json_encode($data);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);


        $curl_response = curl_exec($curl);


        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);
//        echo '<pre>';
//        print_r($decoded);
//        echo $book[$i]["coric"];
//        echo "<br/>";
//        echo $book[$i]["order_id"];
//        echo '<pre>';
//        print_r($decoded['response'][0]['OrderID']);
//        echo "<br/>";
//        die;
        $juspay_url = $JUSPAY_URL;
        ///////////t/////////get unique id and amount from juspay api//
        $curl = curl_init($juspay_url . 'order_status');
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('orderId' => $book[$i]["order_id"], 'merchantId' => 'mylescarssecurity'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERPWD, $JUSPAY_SEQ_PASS);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $jsonRespon = json_decode(curl_exec($curl));
        if (($decoded['response'][0]['OrderID'] != $book[$i]["order_id"]) && ($decoded['response'][0]['OrderID'] != '')) {

//            echo "a";
//            die;
            // echo '<pre>';
            // print_r($jsonRespon);
            if ($jsonRespon->{'refunded'} != 1) {
//                echo '<pre>';
//                print_r($jsonRespon);
//                echo $jsonRespon->{'txnId'};
//                echo '<br/>';
//               echo $jsonRespon->{'amount'};
                ////////////////////

                refundJuspay($book[$i]["order_id"], $jsonRespon->{'txnId'}, $jsonRespon->{'amount'}, $JUSPAY_SEQ_PASS, $book[$i]["coric"], $book[$i]["paylogid"], $book[$i]['platform'], $book[$i]['mobile'], $name, $email_id, $model_name);
            }
        } else {

            if ($jsonRespon->{'refunded'} != 1) {
                $totalAmount = $jsonRespon->{'amount'};
                ///////////////////////////////////////////////
                $wallet_order_id = str_replace('AWJS', 'AWWS', $book[$i]['order_id']);
                echo $getWalletAmountQuery = "SELECT "
                . "p.amount log_amount,"
                . "p.order_id log_order_id,"
                . "p.trans_id log_trans_id"
                . " FROM  "
                . "myles_payment_logs p  "
                . "WHERE p.status in ('success') "
                . "AND p.payment_source_id = 2 AND p.order_id='" . $wallet_order_id . "'";
                $getWalletAmountResult = $db->query("query", $getWalletAmountQuery);

                if (array_key_exists("0", $getWalletAmountResult)) {
                    //echo '<pre>';
                    //sprint_r($getWalletAmountResult);
                    $reattemptWalletResultArray = $getWalletAmountResult[0];
                    $wallet_amount = $reattemptWalletResultArray["log_amount"];
                    $totalAmount = $jsonRespon->{'amount'} + $wallet_amount;
                }
                //die;
                //////////////////////////////////////////////




                if ($book[$i]['sec_deposit'] == $totalAmount) {
//                    echo "bb";
//            die;
                    // insert data into SQLSERVER
                    // security
                    //////////////////////////////////////
                    $juspayTransCheckQuery = "select * from myles_juspay_trans where juspay_order_id='" . $jsonRespon->{'orderId'} . "'";
                    $rjuspay = $db->query("query", $juspayTransCheckQuery);
                    if (!array_key_exists("response", $rjuspay)) {

                        $whereJuspayUpdate['juspay_order_id'] = $jsonRespon->{'orderId'};
                        $juspay['coric'] = $book[$i]["coric"];
                        $juspay['platform'] = $book[$i]["platform"];
                        $juspay['payment_source_id'] = 2;
                        $juspay['juspay_merchant_id'] = $jsonRespon->{'merchantId'};
                        $juspay['juspay_customer_id'] = $jsonRespon->{'customerId'};
                        $juspay['juspay_order_id'] = $jsonRespon->{'orderId'};
                        $juspay['juspay_status'] = $jsonRespon->{'status'};

                        $juspay['juspay_amount'] = $jsonRespon->{'amount'};
                        $juspay['juspay_refunded'] = $jsonRespon->{'refunded'};
                        $juspay['juspay_amount_refunded'] = $jsonRespon->{'amount_refunded'};

                        $juspay['juspay_trans_id'] = $jsonRespon->{'txnId'};
                        $juspay['juspay_gateway_id'] = $jsonRespon->{'gatewayId'};
                        $juspay['juspay_bank_error_code'] = $jsonRespon->{'bankErrorCode'};
                        $juspay['juspay_bank_error_message'] = $jsonRespon->{'bankErrorMessage'};
                        $juspay['created'] = date("Y-m-d H:i:s");
                        $up = $db->update("myles_juspay_trans", $juspay, $whereJuspayUpdate);
                    } else {
                        $juspay['coric'] = $book[$i]["coric"];
                        $juspay['platform'] = $book[$i]["platform"];
                        $juspay['payment_source_id'] = 2;
                        $juspay['juspay_merchant_id'] = $jsonRespon->{'merchantId'};
                        $juspay['juspay_customer_id'] = $jsonRespon->{'customerId'};
                        $juspay['juspay_order_id'] = $jsonRespon->{'orderId'};
                        $juspay['juspay_status'] = $jsonRespon->{'status'};

                        $juspay['juspay_amount'] = $jsonRespon->{'amount'};
                        $juspay['juspay_refunded'] = $jsonRespon->{'refunded'};
                        $juspay['juspay_amount_refunded'] = $jsonRespon->{'amount_refunded'};

                        $juspay['juspay_trans_id'] = $jsonRespon->{'txnId'};
                        $juspay['juspay_gateway_id'] = $jsonRespon->{'gatewayId'};
                        $juspay['juspay_bank_error_code'] = $jsonRespon->{'bankErrorCode'};
                        $juspay['juspay_bank_error_message'] = $jsonRespon->{'bankErrorMessage'};
                        $juspay['created'] = date("Y-m-d H:i:s");

                        $db->insert('myles_juspay_trans', $juspay);
                    }
////////////////////////////////////////////////////////////////


                    echo $reattemptWalletQuery = "SELECT "
                    . "b.coric,"
                    . "b.booking_id,b.email_id email_id,b.cciid,b.mobile,b.full_name,"
                    . "p.created log_created,"
                    . "p.id logid,"
                    . "p.order_id logOrderId,"
                    . "p.status log_status,"
                    . "p.amount log_amount,"
                    . "p.platform log_platform,"
                    . "p.order_id log_order_id,"
                    . "p.trans_id log_trans_id"
                    . " FROM myles_bookings b "
                    . "INNER JOIN myles_payment_logs p ON p.coric=b.coric "
                    . "WHERE b.payment_status = 1 AND b.preauth_status='UnPaid' AND p.status in ('success') "
                    . "AND p.payment_source_id = 2 AND booking_id !='' AND p.order_id='" . $wallet_order_id . "'";
                    $reattemptWalletResult = $db->query("query", $reattemptWalletQuery);

                    if (array_key_exists("0", $reattemptWalletResult)) {

                        $reattemptWalletResultArray = $reattemptWalletResult[0];

                        $datcreate = date_create($reattemptWalletResultArray["log_created"]);
                        $wallet_amount = $reattemptWalletResultArray["log_amount"];
                        $walletdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
                        $walletdata["TransactionPlatform"] = $reattemptWalletResultArray["log_platform"];
                        $walletdata["TransactionType"] = 1;
                        $walletdata["ReferenceID"] = $reattemptWalletResultArray["coric"];
                        $walletdata["TransactionModeID"] = 1;
                        $walletdata["PaymentSourceID"] = 2; //$wallet['payment_source_id'];
                        $walletdata["OrderID"] = $reattemptWalletResultArray["log_order_id"];
                        $walletdata["Amount"] = $reattemptWalletResultArray["log_amount"];
                        $walletdata["ResposeCode"] = $reattemptWalletResultArray['log_trans_id'];
                        $walletdata["OtherTrnsDetails"] = 0;
                        $walletdata["isActive"] = 1;
                        $walletdata["CreatedBy"] = $reattemptWalletResultArray["email_id"];
                        $walletOrderId = $reattemptWalletResultArray["log_order_id"];
                    }

///////////////////////////////////////////////////////////////                    

                    $juspay_query = "SELECT * FROM myles_juspay_trans WHERE coric='" . $book[$i]["coric"] . "' AND payment_source_id = 2";
                    $rjuspay = $db->query("query", $juspay_query);
                    if (array_key_exists(0, $rjuspay)) {
                        $juspay = $rjuspay[0];
                        $datcreate = date_create($juspay["created"]);
                        $juspay_amount = $juspay["juspay_amount"];
                        $juspaydata['TransactionDateTime'] = $datcreate->format('Y-m-d H:i:s');
                        $juspaydata["TransactionPlatform"] = $juspay["platform"];
                        $juspaydata["TransactionType"] = 1;
                        $juspaydata["ReferenceID"] = $juspay["coric"];
                        $juspaydata["TransactionModeID"] = 2;
                        $juspaydata["PaymentSourceID"] = $juspay['payment_source_id'];
                        $juspaydata["OrderID"] = $juspay["juspay_order_id"];
                        $juspaydata["Amount"] = $juspay["juspay_amount"];
                        $juspaydata["ResposeCode"] = $juspay['juspay_trans_id'];
                        $juspaydata["OtherTrnsDetails"] = 0;
                        $juspaydata["isActive"] = 1;
                        $juspaydata["CreatedBy"] = $juspay["juspay_customer_id"];

                        $json_val = "";

                        if (count($juspaydata) > 0) {
                            $juspaydata["BookingID"] = $book[$i]["booking_id"];
                            $json_val .= json_encode($juspaydata);
                        }
                        if ((count($walletdata) > 0) && (count($juspaydata) > 0)) {
                            $walletdata["BookingID"] = $book[$i]["booking_id"];
                            $juspaydata["BookingID"] = $book[$i]["booking_id"];
                            $json_val = "";
                            $json_val .= json_encode($juspaydata) . "," . json_encode($walletdata);
                        }
                        $arr = '{"jsonData":[' . $json_val . ']}';
                        $service_url = $url . 'preAuthPayment';
//                        echo '$service_url = ' . $service_url;
                        ///////////////////////////////////

                        $curl = curl_init($service_url);
                        $data_json = json_encode($arr);

//                        echo '<pre>';
//                        print_r($data_json);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($data_json))
                        );
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                        $curl_response = curl_exec($curl);

                        curl_close($curl);
                        $decoded = json_decode($curl_response, true);

                        /////////////////////////////////


                        $response = $decoded['status'];

                        if (intval($response) == 1) {
//                            echo "abc";
//                            die;
                            $dataToSave = array();
                            $where = array();
                            $where["coric"] = $book[$i]["coric"];
                            $dataToSave["preauth_status"] = "Paid";

                            $up = $db->update("myles_bookings", $dataToSave, $where);
                            $where1 = array();
                            $where1["id"] = $book[$i]["paylogid"];
//                             echo '<pre>';
//                            print_r($where1);

                            $payment_log1 = array();
                            $payment_log1['booking_status'] = 1;
//                            print_r($payment_log1);
                            $up = $db->update("myles_payment_logs", $payment_log1, $where1);

                            /////////////////////////////////////////

                            if (count($walletdata) > 0) {
                                $dataToSave = array();
                                $where = array();
                                $where["order_id"] = $walletOrderId;
                                $dataToSave["booking_status"] = "1";
                                $up = $db->update("myles_payment_logs", $dataToSave, $where);
                            }


                            ///////////////////////////////////////
                        }
                    }
                    ///////////////////////////////////////////
                    // $juspay_query = $db->select('all', ['conditions' => ['MylesJuspayTrans.coric' => $bookings["coric"], 'MylesJuspayTrans.payment_source_id' => $session->read('payment_source_id')]]);
                    //$juspay = $juspay_query->first();
                } else {

                    refundJuspay($book[$i]["order_id"], $jsonRespon->{'txnId'}, $jsonRespon->{'amount'}, $JUSPAY_SEQ_PASS, $book[$i]["coric"], $book[$i]["paylogid"], $book[$i]['platform'], $book[$i]['mobile'], $name, $email_id, $model_name);
                }
            }
        }

        ////////////////////////
    }
}

$db->close();

function refundJuspay($order_id, $txnId, $amount, $JUSPAY_SEQ_PASS, $coric, $id, $platform, $mobile, $name, $email_id, $model_name) {
    $Myle = new Myles();

    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $wallet_amt = 0;
    $juspay_amt = 0;
    $walletCount = 0;
    $node = 0;
    $juspayCount = 0;

    //////////////////////////////Start Wallet Refund ////////////////////////////
    $wallet_query = "SELECT * FROM myles_wallet_trans WHERE coric='" . $coric . "' and payment_source_id =2 AND wallet_status='Success'";

    $walletresult = $db->query("query", $wallet_query);

    //echo '<pre>';
    //print_r($walletresult);
    $WALLET_URL = "http://180.179.146.81/wallet/v1/";
    $n = 0;

    $WALLET_PASS = "ef32ae830b2f65e4006baa5e9d6b35f4:d8c5d4a2f108d0c72abe3541f4c10a29";

    if (!array_key_exists("response", $walletresult)) {
        $walletresultArray = $walletresult[0];
        //echo '<pre>';
        //print_r($walletresultArray);
        $wallet_amt = $walletresultArray['wallet_amount'];
//die;
        //////////////////////////
        $consumer_id = $walletresultArray['wallet_consumer_id'];
        $shmart_refID = $walletresultArray['wallet_ref_id'];
        $walletOrderId = $walletresultArray['wallet_order_id'];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $WALLET_URL . "debits/reverse",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $consumer_id . "\",\r\n\"shmart_refID\":\"" . $shmart_refID . "\"}\r\n",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . base64_encode($WALLET_PASS),
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));
//KIndly replace the value NzJjOGY2YjVjYzhhODE2N2E1YTFlMTdjYTE2NTVlOGY6OWU5ZGVlN2QwNzhmYjRmM2ZkMjVkMWMyZjU1ZTA0MTY= with base64_encode("user:password") as it takes base64 encoding
        $response = curl_exec($curl);
        //echo '<pre>';
        //print_r($response);
        //  die;
        $err = curl_error($curl);

        curl_close($curl);

        $responseArray = json_decode($response);

        if ($responseArray->status == 'success') {
            $walletCount++;
            $node++;
            $arrSMS = array();

            $arrSMS['mobileNo'] = $mobile;
            $arrSMS['message'] = "Refund of Rs. ".intval($walletresultArray['wallet_amount'])." has been initiated for your failed transaction having unique reference id: ".$walletOrderId; 
                
                    
            
            $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);
            unset($arrSMSs);

            $refArray['pg_destination'] = 'Wallet';
            $refArray['platform'] = $platform;
            $refArray['coric'] = $coric;
            $refArray['payment_source_id'] = 2;
            $refArray['order_id'] = $walletresultArray['wallet_order_id'];
            $refArray['amount'] = $walletresultArray['wallet_amount'];
            $refArray['status'] = 'REFUNDED';
            $refArray['created'] = date("Y-m-d H:i:s");

            $insert = $db->insert("myles_refund_logs", $refArray);

            unset($refArray);
            unset($insert);
            if ($walletresultArray['wallet_order_id'] != '') {
                $dataToSave = array();
                $where = array();
                $where["order_id"] = $walletresultArray['wallet_order_id'];
                $dataToSave["booking_status"] = "2";
                $dataToSave["amount"] = $walletresultArray['wallet_amount'];
                $dataToSave["trans_id"] = $walletresultArray['wallet_ref_id'];
                $up = $db->update("myles_payment_logs", $dataToSave, $where);
            }
        }



        //////////////////////////////
    }



    /////////////////////////////End Wallet Refund //////////////////////////////

    $JUSPAY_URL = "https://sandbox.juspay.in/";
    $juspayRefundArray = array('order_id' => $order_id, 'unique_request_id' => $txnId, 'amount' => $amount);
    //echo '<pre>';
    //print_r($juspayRefundArray);
    $ch = curl_init($JUSPAY_URL . 'order/refund');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, $JUSPAY_SEQ_PASS);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $juspayRefundArray);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);

    $jsonResponse = json_decode(curl_exec($ch));
    //print_r($jsonResponse);
    $err = curl_error($ch);

    $refdata = array();
    $payment_log = array();
    if (intval($jsonResponse->{'refunded'}) == 1) {
        $node++;
        $juspayCount++;

        $arrSMS = array();
        $arrSMS['mobileNo'] = $mobile;
        $arrSMS['message'] = "Refund of Rs. ".intval($amount)." has been initiated for your failed transaction having unique reference id: ".$order_id; 
                
        $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);
        unset($arrSMSs);

        $refArray['pg_destination'] = 'JusPay';
        $refArray['platform'] = $platform;
        $refArray['coric'] = $coric;
        $refArray['payment_source_id'] = 2;
        $refArray['order_id'] = $jsonResponse->{'order_id'};
        $refArray['amount'] = $jsonResponse->{'amount_refunded'};
        $refArray['status'] = 'REFUNDED';
        $refArray['created'] = date("Y-m-d H:i:s");

        $insert = $db->insert("myles_refund_logs", $refArray);



        $where = array();
        $where["id"] = $id;
        $payment_log['booking_status'] = 2;
        $dataToSave["amount"] = $jsonResponse->{'amount_refunded'};
        $dataToSave["trans_id"] = $jsonResponse->{'order_id'};
        $up = $db->update("myles_payment_logs", $payment_log, $where);
        unset($refArray);
        unset($insert);
    }
    if ($node > 0) {


        $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
            <tr style="background:#fff;">
                <td align="left" valign="middle" style="padding:10px;">
                    <a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
                </td>
                <td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
                    <a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
                    <a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
                        </a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
                </td>
            </tr>		
        </table>

        <table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
            <tr style="background:#fff;">
        <td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
            <img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
        </td>
        </tr>
            <tr style="padding:0px 0px; display: block">
                <td valign="top" style="padding: 0px 10px;">
                  <p style="line-height: 18px;">Hi <strong>' . $name . '</strong>,</p>
                  <p style="line-height: 18px;">We are really sorry that your security deposit payment for ' . $model_name . ' has failed.</p>';
        if ($walletCount > 0) {
            $html .= '<p style="line-height: 18px;">Your Wallet has been credited with Rs. ' . $wallet_amt . '</p>';
        }

        $html .= '<p style="line-height: 18px;">We have initiated refund for Rs. ' . $amount . ' into your account. Your Bank/Card provider may take up to 7-8 days to credit in your account.</p>
                    <p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
                </td>
            </tr>
         </table>
         <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
            <tr>
                <td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
                </td>
            </tr>
            <tr>
                <td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
                </td>
            </tr>
    </table>
    </td>
    </tr>
    </table>
            
    </div>';

        $dataToMail = array();
        $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
        $dataToMail["Subject"] = "Security Refund";
        $dataToMail["MailBody"] = $html;

        $Myle->_MYLESSendMail('SendMail', $dataToMail);
        unset($dataToMail);
    }
    $db->close();
}

<?php

//error_reporting(E_ALL);
ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');

include_once('classes/mylesdb.php');
include_once('classes/rest.php');
//include_once('Paynimo/gateway.php');

$Myle = new Myles();
$arr = array();


$db = new MySqlConnection(CONNSTRING);
$db->open();

//$book = $db->query("stored procedure", "myles_paid_and_failed()");
//payment_status = 4 => payment sucessful but booking id not found
//$book_query = "select * from `myles_bookings` where (entry_date <= DATE_ADD(Now(), INTERVAL '325' MINUTE) AND entry_date >= DATE_ADD(Now(), INTERVAL '315' //MINUTE) AND payment_status in('0','1','4')";

$book_query = "select * from `myles_payment_logs` where final_status_update_time <= DATE_ADD(Now(), INTERVAL '320' MINUTE) 
AND final_status_update_time >= DATE_ADD(Now(), INTERVAL '310' MINUTE) AND pg_source='Wallet' 
AND final_status = 'Success' AND payment_source_id='1'";


$book = $db->query("query", $book_query);

if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        $orderId = $book[$i]["order_id"];
        $juspaydata = array();
        $paybackdata = array();
        $walletdata = array();

        $juspay = array();
        $payback = array();
        $wallet = array();

        //$juspay_url = 'https://sandbox.juspay.in/';
        //$juspay_key = 'D774B8A9E3D3452D9F8AADBB53497221';

        $wallet_url = 'http://180.179.146.81/wallet/v1/';
        $wallet_pass = 'ef32ae830b2f65e4006baa5e9d6b35f4:d8c5d4a2f108d0c72abe3541f4c10a29';
        $rarr = array();
        $rarr['coricId'] = $coric;
        $rarr['PaymentSource'] = 'R';
        $result = $Myle->_MYLESGetTrnsDetails('GetTrnsDetails', $rarr);


        if ($result['status'] == '0') {

            $mob_query = "SELECT mobile,cciid,full_name,email_id,model_name FROM myles_bookings WHERE coric='" . $coric . "' ORDER BY entry_date DESC LIMIT 1";
            $rmob = $db->query("query", $mob_query);
            $name = $rmob[0]['full_name'];
            $mobile = $rmob[0]['mobile'];
            $email_id = $rmob[0]['email_id'];
            $model_name = $rmob[0]['model_name'];
            $userid = $rmob[0]['cciid'];
            $wallet_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id= '" . $orderId . "' AND 
			logs.payment_source_id = '1' AND logs.booking_status=0 AND logs.pg_source = 'Wallet' 
			AND logs.final_status_update_time <= DATE_ADD(Now(), INTERVAL '320' MINUTE) 
			AND logs.final_status_update_time >= DATE_ADD(Now(), INTERVAL '310' MINUTE)";

            $rwallet = $db->query("query", $wallet_query);

            if (array_key_exists(0, $rwallet)) {
                $wallet = $rwallet[0];


                if ((count($wallet) > 0) && ($wallet["final_status"] == 'Success')) {

                    $responseGetConsumerID = array();
                    $arr['Userid'] = $userid;
                    $responseGetConsumerID = $Myle->_MYLESGetConsumerID('GetConsumerID', $arr);
                    $ConsumerID = $responseGetConsumerID['ConsumerID'];

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $wallet_url . "debits/reverse",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $ConsumerID . "\",\r\n\"shmart_refID\":\"" . $wallet['trans_id'] . "\"}\r\n",
                        CURLOPT_HTTPHEADER => array(
                            "authorization: Basic " . base64_encode($wallet_pass),
                            "cache-control: no-cache",
                            "content-type: application/json"
                        ),
                    ));

                    $response = curl_exec($curl);

                    $walletResponse = json_decode($response);

                    if (strtolower($walletResponse->{'status'}) == 'success') {
                        $amt = $wallet['amount'];
                        $walletdata['pg_destination'] = 'Wallet';
                        $walletdata['platform'] = $wallet["platform"];
                        $walletdata['coric'] = $wallet["coric"];
                        $walletdata['payment_source_id'] = $wallet["payment_source_id"];
                        $walletdata['order_id'] = $wallet['order_id'];
                        $walletdata['amount'] = $wallet['amount'];
                        $walletdata['status'] = 'REFUNDED';
                        $walletdata['created'] = date("Y-m-d H:i:s");

                        $insertwallet = $db->insert("myles_refund_logs", $walletdata);

                        $arrSMS = array();

                        $arrSMS['mobileNo'] = $mobile;
                        $arrSMS['message'] = "Refund of Rs. " . intval($amt) . " has been initiated for your failed transaction having unique reference id: " . $wallet['order_id'];


                        $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);
                        unset($walletdata);
                        unset($insertwallet);


                        $where = array();
                        $where["id"] = $wallet['id'];
                        $payment_log['booking_status'] = '2';
                        $up = $db->update("myles_payment_logs", $payment_log, $where);
                        unset($payment_log);
                        unset($where);


                        unset($arrSMS);
                        unset($mobsms);

                        $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
												<tr style="background:#fff;">
													<td align="left" valign="middle" style="padding:10px;">
														<a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
													</td>
													<td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
														<a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
														<a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
															</a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
													</td>
												</tr>		
											</table>

											<table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
												<tr style="background:#fff;">
											<td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
												<img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
											</td>
											</tr>
												<tr style="padding:0px 0px; display: block">
													<td valign="top" style="padding: 0px 10px;">
													  <p style="line-height: 18px;">Hi <strong>' . $name . '</strong>,</p>
														<p style="line-height: 18px;">Your Wallet has been credited with Rs. ' . $amt . '.</p>
														<p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
													</td>
												</tr>
											  
											   
											</table>

										  

											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
											<tr>
													<td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
													</td>
												</tr>
												<tr>
													<td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
													</td>
												</tr>
										</table>
										</td>
										</tr>
										</table>
												
										</div>';

                        $dataToMail = array();
                        $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
                        $dataToMail["Subject"] = "Wallet Refund";
                        $dataToMail["MailBody"] = $html;

                        $Myle->_MYLESSendMail('SendMail', $dataToMail);
                        unset($dataToMail);
                        unset($amt);
                        unset($email_id);
                    }
                }
            }
        } else if ($result['status'] == '1') {

            foreach ($result['response'] as $res) {
                if ($res['TranactionMode'] == 'Wallet') {
                    $mob_query = "SELECT mobile,cciid,full_name,email_id,model_name FROM myles_bookings WHERE coric='" . $coric . "' ORDER BY entry_date DESC LIMIT 1";
                    $rmob = $db->query("query", $mob_query);
                    $name = $rmob[0]['full_name'];
                    $mobile = $rmob[0]['mobile'];
                    $email_id = $rmob[0]['email_id'];
                    $model_name = $rmob[0]['model_name'];
                    $userid = $rmob[0]['cciid'];
                    $wallet_query = "SELECT * FROM myles_payment_logs logs WHERE logs.order_id != '" . $res['OrderID'] . "' AND 
					logs.payment_source_id = '1' AND logs.booking_status=0 AND final_status = 'Success' AND logs.pg_source = 'Wallet' 
					AND logs.final_status_update_time <= DATE_ADD(Now(), INTERVAL '320' MINUTE) 
					AND logs.final_status_update_time >= DATE_ADD(Now(), INTERVAL '310' MINUTE)";

                    $rwallet = $db->query("query", $wallet_query);

                    if (array_key_exists(0, $rwallet)) {
                        $wallet = $rwallet[0];


                        if ((count($wallet) > 0) && ($wallet["final_status"] == 'Success')) {

                            $responseGetConsumerID = array();
                            $arr['Userid'] = $userid;
                            $responseGetConsumerID = $Myle->_MYLESGetConsumerID('GetConsumerID', $arr);
                            $ConsumerID = $responseGetConsumerID['ConsumerID'];

                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $wallet_url . "debits/reverse",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $ConsumerID . "\",\r\n\"shmart_refID\":\"" . $wallet['trans_id'] . "\"}\r\n",
                                CURLOPT_HTTPHEADER => array(
                                    "authorization: Basic " . base64_encode($wallet_pass),
                                    "cache-control: no-cache",
                                    "content-type: application/json"
                                ),
                            ));

                            $response = curl_exec($curl);

                            $walletResponse = json_decode($response);

                            if (strtolower($walletResponse->{'status'}) == 'success') {
                                $amt = $wallet['amount'];
                                $walletdata['pg_destination'] = 'Wallet';
                                $walletdata['platform'] = $wallet["platform"];
                                $walletdata['coric'] = $wallet["coric"];
                                $walletdata['payment_source_id'] = $wallet["payment_source_id"];
                                $walletdata['order_id'] = $wallet['order_id'];
                                $walletdata['amount'] = $wallet['amount'];
                                $walletdata['status'] = 'REFUNDED';
                                $walletdata['created'] = date("Y-m-d H:i:s");

                                $insertwallet = $db->insert("myles_refund_logs", $walletdata);

                                $arrSMS = array();

                                $arrSMS['mobileNo'] = $mobile;
                                $arrSMS['message'] = "Refund of Rs. " . intval($amt) . " has been initiated for your failed transaction having unique reference id: " . $wallet['order_id'];


                                $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);

                                unset($walletdata);
                                unset($insertwallet);


                                $where = array();
                                $where["id"] = $wallet['id'];
                                $payment_log['booking_status'] = '2';
                                $up = $db->update("myles_payment_logs", $payment_log, $where);
                                unset($payment_log);
                                unset($where);



                                unset($arrSMS);
                                unset($mobsms);

                                $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
												<tr style="background:#fff;">
													<td align="left" valign="middle" style="padding:10px;">
														<a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
													</td>
													<td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
														<a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
														<a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
															</a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
													</td>
												</tr>		
											</table>

											<table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
												<tr style="background:#fff;">
											<td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
												<img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
											</td>
											</tr>
												<tr style="padding:0px 0px; display: block">
													<td valign="top" style="padding: 0px 10px;">
													  <p style="line-height: 18px;">Hi <strong>' . $name . '</strong>,</p>
													  <p style="line-height: 18px;">We are really sorry that your booking of ' . $model_name . ' has failed.</p>
														<p style="line-height: 18px;">Your Wallet has been credited with Rs. ' . $amt . '.</p>
														<p style="line-height: 18px; margin-bottom: 25px;">Cheers,<br />Team Myles</p>
													</td>
												</tr>
											  
											   
											</table>

										  

											<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
											<tr>
													<td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
													</td>
												</tr>
												<tr>
													<td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
													</td>
												</tr>
										</table>
										</td>
										</tr>
										</table>
												
										</div>';

                                $dataToMail = array();
                                $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
                                $dataToMail["Subject"] = "Wallet Refund";
                                $dataToMail["MailBody"] = $html;

                                $Myle->_MYLESSendMail('SendMail', $dataToMail);
                                unset($dataToMail);
                                unset($amt);
                                unset($email_id);
                            }
                        }
                    }
                }
            }
        }
    }
}
$db->close();

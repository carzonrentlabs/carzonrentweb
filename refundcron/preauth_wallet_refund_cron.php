<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
date_default_timezone_set('Asia/Calcutta');
include_once('classes/mylesdb.php');
include_once('classes/rest.php');
$url = 'https://myles.carzonrent.com/services/mylesservice.svc/';
$service_url = $url . "GetTrnsDetails";
$Myle = new Myles();

$db = new MySqlConnection(CONNSTRING);
$db->open();

$book_query = "SELECT l.coric,l.order_id,sec_deposit,booking_id,"
        . "b.id,l.id paylogid,l.platform,w.wallet_amount,b.full_name,b.mobile,b.email_id,b.model_name "
        . "FROM myles_payment_logs l INNER JOIN myles_bookings b ON b.coric=l.coric INNER JOIN myles_wallet_trans w ON b.coric= w.coric "
        . "WHERE l.booking_status = 0 AND l.payment_source_id=2 AND pg_source='Wallet' and l.status='Success' AND "
     . "final_status_update_time <= DATE_ADD(Now(), INTERVAL '310' MINUTE) AND "
    . "final_status_update_time >= DATE_ADD(Now(), INTERVAL '300' MINUTE)";
//     . "final_status_update_time > first_status_create_time AND pg_source='JusPay'";

$book = $db->query("query", $book_query);

//echo '<pre>';
//print_r($book);
if (!array_key_exists("response", $book)) {

    for ($i = 0; $i < count($book); $i++) {
        $coric = $book[$i]["coric"];
        $name = $book[$i]['full_name'];
        $mobile = $book[$i]['mobile'];
        $email_id = $book[$i]['email_id'];
        $model_name = $book[$i]['model_name'];

        $data = array();
        ////////////////////////
        $data['coricId'] = $book[$i]["coric"];
        $data['PaymentSource'] = 'S';
        $curl = curl_init($service_url);
        $data_json = json_encode($data);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json))
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);


        $curl_response = curl_exec($curl);


        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response, true);
//        echo '<pre>';
//        print_r($decoded);
//        echo $book[$i]["coric"];
//        echo "<br/>";
//        echo $book[$i]["order_id"];
//        echo '<pre>';
//        print_r($decoded['response'][0]['OrderID']);
//        echo "<br/>";
//        die;


        if (($decoded['response'][0]['OrderID'] != $book[$i]["order_id"]) && ($decoded['response'][0]['OrderID'] != '')) {


            refundJuspay($book[$i]["coric"], $book[$i]['platform'], $book[$i]["order_id"], $book[$i]["mobile"], $name, $email_id, $model_name);
        } else {




            if (($book[$i]['sec_deposit'] == $book[$i]['wallet_amount']) && ($decoded['response'][0]['OrderID'] == '')) {

                ///////////////////////////////////////////Wallet Data/////////////////////////////
//                    echo "bb";
//            die;
                // insert data into SQLSERVER
                // security
                //////////////////////////////////////

                $wallet_query = "SELECT * FROM myles_wallet_trans WHERE coric='" . $book[$i]["coric"] . "' AND payment_source_id =2 "
                        . "AND wallet_status='Success' AND wallet_order_id='" . $book[$i]["order_id"] . "'";

                $walletResultArray = $db->query("query", $wallet_query);

                //echo '<pre>';
                //print_r($walletResultArray);

                if (!array_key_exists("response", $walletResultArray)) {
                    $walletResult = $walletResultArray[0];
                    $datcreate = date_create($walletResult["wallet_transaction_time"]);
                    $wallet_amount = $walletResult["wallet_amount"];
                    $walletdata["TransactionDateTime"] = $datcreate->format('Y-m-d H:i:s');
                    $walletdata["TransactionPlatform"] = $walletResult["platform"];
                    $walletdata["TransactionType"] = 1;
                    $walletdata["ReferenceID"] = $walletResult["coric"];
                    $walletdata["TransactionModeID"] = 1;
                    $walletdata["PaymentSourceID"] = $walletResult['payment_source_id'];
                    $walletdata["OrderID"] = $walletResult["wallet_order_id"];
                    $walletdata["Amount"] = $walletResult["wallet_amount"];
                    $walletdata["ResposeCode"] = $walletResult['wallet_ref_id'];
                    $walletdata["OtherTrnsDetails"] = 0;
                    $walletdata["isActive"] = 1;
                    $walletdata["CreatedBy"] = $walletResult["wallet_created_by"];


                    $json_val = "";


                    if (count($walletdata) > 0) {
                        $walletdata["BookingID"] = intval($book[$i]['booking_id']);
                        $json_val .= json_encode($walletdata);
                    }
                    // print_r($walletdata);
                    //die;
                    $arr = '{"jsonData":[' . $json_val . ']}';
                    $service_url = $url . 'preAuthPayment';
//                        echo '$service_url = ' . $service_url;
                    ///////////////////////////////////

                    $curl = curl_init($service_url);
                    $data_json = json_encode($arr);

//                        echo '<pre>';
//                        print_r($data_json);
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_json))
                    );
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    $curl_response = curl_exec($curl);

                    curl_close($curl);
                    $decoded = json_decode($curl_response, true);

                    /////////////////////////////////


                    $response = $decoded['status'];

                    if (intval($response) == 1) {
//                            echo "abc";
//                            die;
                        $dataToSave = array();
                        $where = array();
                        $where["coric"] = $book[$i]["coric"];
                        $dataToSave["preauth_status"] = "Paid";

                        $up = $db->update("myles_bookings", $dataToSave, $where);
                        $where1 = array();
                        $where1["id"] = $book[$i]["paylogid"];
//                             echo '<pre>';
//                            print_r($where1);

                        $payment_log1 = array();
                        $payment_log1['booking_status'] = 1;
//                            print_r($payment_log1);
                        $up = $db->update("myles_payment_logs", $payment_log1, $where1);
                    }
                }
            }
        }
    }
}

$db->close();

function refundJuspay($coric, $platform, $orderId, $mobile, $name, $email_id, $model_name) {
    $Myle = new Myles();
    $db = new MySqlConnection(CONNSTRING);
    $db->open();

    //////////////////////////////Start Wallet Refund ////////////////////////////
    $wallet_query = "SELECT * FROM myles_wallet_trans WHERE coric='" . $coric . "' AND payment_source_id =2 "
            . "AND wallet_status='Success' AND wallet_order_id='" . $orderId . "'";

    $walletresult = $db->query("query", $wallet_query);


    $WALLET_URL = "http://180.179.146.81/wallet/v1/";


    $WALLET_PASS = "ef32ae830b2f65e4006baa5e9d6b35f4:d8c5d4a2f108d0c72abe3541f4c10a29";

    if (!array_key_exists("response", $walletresult)) {



        $walletresultArray = $walletresult[0];
        //echo '<pre>';
        //print_r($walletresultArray);
//die;
        //////////////////////////
        $consumer_id = $walletresultArray['wallet_consumer_id'];
        $shmart_refID = $walletresultArray['wallet_ref_id'];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $WALLET_URL . "debits/reverse",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n\"consumer_id\" : \"" . $consumer_id . "\",\r\n\"shmart_refID\":\"" . $shmart_refID . "\"}\r\n",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . base64_encode($WALLET_PASS),
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));
//KIndly replace the value NzJjOGY2YjVjYzhhODE2N2E1YTFlMTdjYTE2NTVlOGY6OWU5ZGVlN2QwNzhmYjRmM2ZkMjVkMWMyZjU1ZTA0MTY= with base64_encode("user:password") as it takes base64 encoding
        $response = curl_exec($curl);
        //echo '<pre>';
        //print_r($response);
        curl_close($curl);

        $responseArray = json_decode($response);
        if ($responseArray->status == 'success') {

            $amt = $walletresultArray['wallet_amount'];

            $arrSMS = array();

            $arrSMS['mobileNo'] = $mobile;
              $arrSMS['message'] = "Refund of Rs. ".intval($amt)." has been initiated for your failed transaction having unique reference id: ".$orderId;
            $mobsms = $Myle->_MYLESsendSMS('sendSMS', $arrSMS);
            unset($arrSMSs);
            $html = '<div style="background:#f1f1f1; margin:0px;padding:0px;font-family:arial;">
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 4px;">
            <tr style="background:#fff;">
                <td align="left" valign="middle" style="padding:10px;">
                    <a href="http://www.mylescars.com" target="_blank"><img height="30px;" src="http://www.mylescars.com/images/additional_images/myles_logo.png" border="0"></a>
                </td>
                <td align="right" valign="top" style="padding:0px 20px 0px 20px; font-size: 14px;"><img src="http://www.mylescars.com/images/additional_images/call_icon.png" alt="Call Icon"> +91 888 222 2222<br>
                    <a href="https://play.google.com/store/apps/details?id=com.org.cor.myles&amp;hl=en" target="_blank"><img src="http://www.mylescars.com/images/additional_images/play_store_icn.png" height="35px;"></a>
                    <a href="https://itunes.apple.com/in/app/myles-self-drive-car-rental/id1061852579" target="_blank"><img src="http://www.mylescars.com/images/additional_images/app_store_icn.png" height="35px;">
                        </a><a href="http://www.mylescars.com/" target="_blank"><img src="http://www.mylescars.com/images/additional_images/web_icn.png" height="35px;"></a>
                </td>
            </tr>		
        </table>

        <table width="600" align="center" cellpadding="0" cellspacing="0" style="background: #fff; color: #575757; font-size: 14px; line-height: 10px; margin-bottom: 0px;">
            <tr style="background:#fff;">
        <td align="left" valign="middle" style="padding:0px 0px 10px 0px;">
            <img src="https://www.mylescars.com/marketing_emailer/banner_credit.png">
        </td>
        </tr>
            <tr style="padding:0px 0px; display: block">
                <td valign="top" style="padding: 0px 10px;">
                  <p style="line-height: 18px;">Hi <strong>' . $name . '</strong>,</p>
                  <p style="line-height: 18px;">We are really sorry that your security deposit payment for ' . $model_name . ' has failed.</p>
           
               <p style="line-height: 18px;">Your Wallet has been credited with Rs. ' . $amt . '</p>
           
                </td>
            </tr>
         </table>
         <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:arial; padding:0;">
            <tr>
                <td align="center" style="line-height: 10px; padding-top: 10px; background: #414042;"><a href="https://www.facebook.com/Mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-fb.png" alt=""></a><a href="https://twitter.com/mylescars" style="text-decoration: none;"><img style="margin-right: 25px;" src="https://www.mylescars.com/marketing_emailer/cashback/b-twitter.png" alt=""></a><a href="https://www.instagram.com/mylescars/" style="text-decoration: none;"><img src="https://www.mylescars.com/marketing_emailer/cashback/b-instragram.png" alt=""></a>
                </td>
            </tr>
            <tr>
                <td align="center" style="line-height: 10px;"><a href="http://www.mylescars.com/" style="color: #fff; text-decoration: none;"><img style="width:100%;" src="https://www.mylescars.com/marketing_emailer/cashback/b-footer3.png" alt=""></a>
                </td>
            </tr>
    </table>
    </td>
    </tr>
    </table>
            
    </div>';

            $dataToMail = array();
            $dataToMail["To"] = $email_id . ',autorefunds@mylescars.com';
            $dataToMail["Subject"] = "Security Refund";
            $dataToMail["MailBody"] = $html;

            $Myle->_MYLESSendMail('SendMail', $dataToMail);
            unset($dataToMail);


            $refArray['pg_destination'] = 'Wallet';
            $refArray['platform'] = $platform;
            $refArray['coric'] = $coric;
            $refArray['payment_source_id'] = 2;
            $refArray['order_id'] = $walletresultArray['wallet_order_id'];
            $refArray['amount'] = $walletresultArray['wallet_amount'];
            $refArray['status'] = 'REFUNDED';
            $refArray['created'] = date("Y-m-d H:i:s");

            $insert = $db->insert("myles_refund_logs", $refArray);

            unset($refArray);
            unset($insert);

            if ($walletresultArray['wallet_order_id'] != '') {
                $dataToSave = array();
                $where = array();
                $where["order_id"] = $walletresultArray['wallet_order_id'];
                $dataToSave["booking_status"] = "2";
                $dataToSave["amount"] = $walletresultArray['wallet_amount'];
                $dataToSave["trans_id"] = $walletresultArray['wallet_ref_id'];
                $up = $db->update("myles_payment_logs", $dataToSave, $where);
            }
        }



        //////////////////////////////
    }



    /////////////////////////////End Wallet Refund //////////////////////////////







    $db->close();
}

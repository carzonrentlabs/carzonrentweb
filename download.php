<?php

$file = $_GET['file'];
$uId  = $_GET['uId'];



$Path  = 'https://s3-ap-southeast-1.amazonaws.com/uploadmyles/'.$uId.'/'.$file;

download_file($Path,$file);

function download_file($fullPath,$file){
  // Must be fresh start
 // echo $fullPath;
  if( !empty($file) ){

    // Parse Info / Get Extension
    $farray =  explode(".",$file);
    $ext = strtolower($farray[1]);
   
    // Determine Content Type
    switch ($ext) {
      case "pdf": $ctype="application/pdf"; break;
      case "exe": $ctype="application/octet-stream"; break;
      case "zip": $ctype="application/zip"; break;
      case "doc": $ctype="application/msword"; break;
      case "xls": $ctype="application/vnd.ms-excel"; break;
      case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
      case "gif": $ctype="image/gif"; break;
      case "png": $ctype="image/png"; break;
      case "jpeg":
      case "jpg": $ctype="image/jpg"; break;
      default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: $ctype");
    header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
    header("Content-Transfer-Encoding: binary");
    ob_clean();
    flush();
    readfile( $fullPath );

  } else
    die('File Not Found');

}
?>
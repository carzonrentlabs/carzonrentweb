<?php
    if($_POST["tokenID"]){
        include_once('./classes/cor.ws.class.php');
        include_once('./classes/cor.xmlparser.class.php');
        
        $dataToSend= array();
        $dataToSend["password"] = $_POST["txtPassword"];
        $dataToSend["tokenId"] = $_POST["tokenID"];
        
        $cor = new COR();
         if($_POST['tourtype']=='1'){
           $res = $cor->_CORSetSelfDriveNewPassword($dataToSend);
        }else{
            $res = $cor->_CORSetNewPassword($dataToSend);
        }
       
        $myXML = new CORXMLList();
        $arrUserId = $myXML->xml2ary($res->{'SetPasswordResult'}->{'any'});
        if(key_exists("result", $arrUserId[0]) && intval($arrUserId[0]["result"]) == 1){
            $url = "./login.php?resp=s";
        }
        else{
            $url = "./new-password.php?ID=" . $_POST["tokenID"] . "&resp=e";
        }
        
        header('Location: ' . $url);
    }
?>
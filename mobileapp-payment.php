<!DOCTYPE html>
<?php
error_reporting(0);
session_destroy();
session_start();
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
include_once('./classes/cor.gp.class.php');
include_once('./classes/cor.mysql.class.php');
include_once("./includes/cache-func.php");
$tab = 3;
if (isset($_POST["tab"]))
$tab = $_POST["tab"];
$bookingID = "";
if (isset($_GET["bookingid"]) && $_GET["bookingid"] != "")
$bookingID = $_GET["bookingid"];
$ref = "";
if (isset($_GET["ref"]) && $_GET["ref"] != "")
$ref = $_GET["ref"];
if ($bookingID == "")
header("Location: http://www.carzonrent.com/");
$cor = new COR();
if ($ref != "") 
{
	$data = array();
	$data["data"] = $ref;
	$res = $cor->_CORDecrypt($data);
	$ref = $res->{'DecryptResult'};
	unset($data);
}
if (strtolower($ref) == "honeywell") 
{
	$data = array();
	$data["data"] = $bookingID;
	$res = $cor->_CORDecrypt($data);
	$bookingID = $res->{'DecryptResult'};
	unset($data);
}

if ($bookingID == "")
header("Location: http://www.carzonrent.com/");
$res = $cor->_CORGetBookingsDetail(array("BookingID" => $bookingID));
$bookingDetails = array();
if ($res->{'GetBookingDetailResult'}->{'any'} != "") {
    $myXML = new CORXMLList();
    $bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});
	
 }
//echo  "<pre>";
//print_r($bookingDetails);
//die;

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>   
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Carzonrent booking</title>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/mobileapp/css/carzonrent.css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/jquery.min.js"></script>

<script>
$(function () {
//$('select.time').customSelect();
//$('select.area').customSelect();
})

function resgiter_cont() {
document.getElementById("register").style.display = 'none';
document.getElementById("travel_details").style.display = 'block';
}

function _submit2()
{
var chk;
chk = true;
if (chk)
chk = isFilledText(document.getElementById("txtcardnopb"), "", "PayBack Loyality Card number can't be left blank.");
if (chk)
chk = isFilledText(document.getElementById("txtpbpoints"), "", "Points to Redeem can't be left blank.");
if (chk && document.getElementById('txtcardnopb').value != "" && document.getElementById('txtpbpoints').value != "") {
var availablepoints = parseInt(document.getElementById('txtpointsincard').value);
var pointstoredeem = parseInt(document.getElementById('txtpbpoints').value);
if (availablepoints >= pointstoredeem) {
document.getElementById('formPaymentOptions1').action = webRoot + "/intermediate1.php";
//alert(document.getElementById('formPaymentOptions1').action);
document.getElementById('formPaymentOptions1').submit();
}
else {
alert("Insufficient points in your PayBack card.");
return false;
}
}
}
</script>

</head>
<body>
<?php //include("./mobileapp/header.php")?>
<div class="clr"> </div>
<?php
$isFleetAvail = true;
if ($bookingDetails[0]["TourType"] == "Self Drive") 
{
	$isFleetAvail = false;
	$pDate = date_create($bookingDetails[0]["PickUpDate"]);
	$dDate = date_create($bookingDetails[0]["DropOffDate"]);
	$res = $cor->_COR_SelfDrive_ISAvailability(array("PkgID" => $bookingDetails[0]["IndicatedPkgID"], "fromDate" => $pDate->format("Y-m-d"), "DateIn" => $dDate->format("Y-m-d"), "PickupTime" => $bookingDetails[0]["PickUpTime"], "DropOffTime" => $bookingDetails[0]["DropOffTime"], "SubLocationID" => $bookingDetails[0]["subLocationID"]));
	$myXML = new CORXMLList();
	$fleetStatus = $myXML->xml2ary($res);
	$isFleetAvail = (bool) $fleetStatus[0]["IsAvailable"];
}

unset($cor);
$ll = "";
$dll = "";
$travelData = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
$points = count($travelData->{'routes'}[0]->{'legs'});
$ll = $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} . "," . $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'};
$dll = ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lat'}) . "," . ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lng'});
$totalKM = 0;
for ($i = 0; $i < $points; $i++) {
$dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
if ($i == 0)
$dwstate = $dispOrg;
$dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
if ($dwstate == "")
$dwstate = $dispDest;
else
$dwstate .= " to:" . $dispDest;
}
if ($tab == 3) 
{
if (isset($_COOKIE["tcciid"]))
$cciid = $_COOKIE["tcciid"];
if (isset($_COOKIE["cciid"]))
$cciid = $_COOKIE["cciid"];
?>
<div id="country3" class="tabcontent">		
<?php
$_SESSION["rurl"] = urlencode("http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
$mtime = round(microtime(true) * 1000);

$_SESSION["hdBookingID"] = $_REQUEST["bookingid"];
$_SESSION["hdCarModel"] = $bookingDetails[0]["CarCatName"];
$_SESSION["hdOriginName"] = $bookingDetails[0]["OriginName"];
if ($bookingDetails[0]["OutstationYN"]) 
{
$_SESSION["hdTourtype"] = "Outstation";
} else 
{
$_SESSION["hdTourtype"] = "Local";
}
$_SESSION["hdDestinationName"] = $bookingDetails[0]["destination"];
$_SESSION["hdPickdate"] = $bookingDetails[0]["PickUpDate"];
$_SESSION["picktime"] = $bookingDetails[0]["PickUpTime"];
$_SESSION["hdDropdate"] = $bookingDetails[0]["DropDate"];
$_SESSION["address"] = $bookingDetails[0]["PickUpAdd"];
$_SESSION["totFare"] = $bookingDetails[0]["PaymentAmount"];
if($bookingDetails[0]["Lname"]=='Array')
{
$lname="";
}
else
{
$lname=$bookingDetails[0]["Lname"];
}
$_SESSION["name"] = $bookingDetails[0]["Fname"] . " " .$lname;

$_SESSION["monumber"] = $bookingDetails[0]["mobile"];
$_SESSION["email"] = $bookingDetails[0]["EmailID"];
$_SESSION["cciid"] = $bookingDetails[0]["userid"];
$_SESSION["PickUpDate"] = $bookingDetails[0]["PickUpDate"];
$_SESSION["DropOffDate"] = $bookingDetails[0]["DropOffDate"];
$_SESSION["originId"] = $bookingDetails[0]["originId"];
$_SESSION["hdTourtype"] = $bookingDetails[0]["TourType"];
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack") {
$_SESSION["empcode"] = $_POST["empcode"];
$_SESSION["discAmt"] = $_POST["discountAmt"];
$_SESSION["DiscTxnID"] = $_POST["pagetransactionId"];
}




    set_include_path('./lib' . PATH_SEPARATOR . get_include_path());	
    require_once('./lib/CitrusPay.php');
    require_once 'Zend/Crypt/Hmac.php';
   /* $basicAmt="";
    $addSrvAmt=$bookingDetails[0]["serviceAmount"];
	$corAS = new COR();
	$data = array();
	$data["subLoctionID"] = $bookingDetails[0]["subLocationID"];
	$data["basicAmount"] = ($basicAmt + $addSrvAmt);
	$res = $corAS->_CORGetSubLocCost($data); */
	
    function generateHmacKey($data, $apiKey = null) 
	{
        $hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
        return $hmackey;
    }
$action = "";
$flag = "";
CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d", 'production');
//CitrusPay::setApiKey("a06521adb14788d10713f08b6885e62d409d507f", 'sandbox');
//require_once("./includes/libfuncs.php");
	
if (isset($bookingDetails[0]["trackID"]) && $bookingDetails[0]["trackID"] != "")
$Order_Id = $bookingDetails[0]["trackID"];
$fname = $bookingDetails[0]["Fname"];
if($bookingDetails[0]["Lname"]=='Array')
{
$lname = "";
}
else
{
$lname = "";
}

//$vanityUrl = "ziq69qd1e9";
$vanityUrl = "carzonrent";
$currency = "INR";
$merchantTxnId = $Order_Id;
$addressState = "";
$addressCity = "";
$addressStreet1 = "";
$addressCountry = "";
$addressZip = "";
$firstName = $fname;
$lastName = $lname;
$phoneNumber = $bookingDetails[0]["mobile"];
$email = $bookingDetails[0]["EmailID"];
$paymentMode = "";
$issuerCode = "";
$cardHolderName = "";
$cardNumber = "";
$expiryMonth = "";
$cardType = "";
$cvvNumber = "";
$expiryYear = "";
$returnUrl = corWebRoot . "/mobile-cp-thanks.php";
$orderAmount = $bookingDetails[0]["PaymentAmount"];
$flag = "post";
$data = "$vanityUrl$orderAmount$merchantTxnId$currency";
$secSignature = generateHmacKey($data, CitrusPay::getApiKey());
$action = CitrusPay::getCPBase() . "$vanityUrl";
$time = time() * 1000;
$time = number_format($time, 0, '.', '');

require_once("./includes/libfuncs.php");
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack")
$Amount = $bookingDetails[0]["PaymentAmount"] - $_POST["discountAmt"];
else
$Amount = $bookingDetails[0]["PaymentAmount"];

if (intval($bookingDetails[0]["PaymentStatus"] == 0)) 
{
	
$_SESSION["orderid"] = $Order_Id;
if ($Order_Id != "") {
$db = new MySqlConnection(CONNSTRING);
$db->open();
$sql = "SELECT uid FROM cor_booking_new WHERE coric = '" . $bookingDetails[0]["trackID"] . "'";
$r = $db->callcenterquery("query", $sql);
if (array_key_exists("response", $r)) 
{
	$dataToSave = array();
	$dataToSave["source"] 					= "CORMOBILEAPP";
	if ($bookingDetails[0]["OutstationYN"] == 'true') 
	{
	$dataToSave["tour_type"] = "Outstation";
	} else {
	$dataToSave["tour_type"] = "Local";
	}
	
	$dataToSave["package_id"] 				= $bookingDetails[0]["IndicatedPkgID"];
	$dataToSave["car_cat"] 					= $bookingDetails[0]["CarCatName"];
	$dataToSave["origin_name"] 				= $bookingDetails[0]["OriginName"];
	$dataToSave["origin_id"] 				= $bookingDetails[0]["originId"];
	$dataToSave["destinations_name"] 		= $bookingDetails[0]["destination"];
	$dataToSave["destinations_id"] 			= $bookingDetails[0]["destinationid"];
	$dataToSave["model_id"]				    = $bookingDetails[0]["Modelid"];
	$dataToSave["model_name"]			    = $bookingDetails[0]["ModelName"];
	$dataToSave["pkg_type"]			    	= "";//$bookingDetails[0]["PackageType"];
	$dataToSave["BasicAmt"]			    	= "";//$bookingDetails[0]["BasicAmount"];
	$dataToSave["OriginalAmt"]				= $bookingDetails[0]["OriginalAmount"];
	$dataToSave["WeekDayDuration"]			= "";
	$dataToSave["WeekEndDuration"]			= "";
	$dataToSave["FreeDuration"]				= "";
	$dataToSave["subLoc"]					= $bookingDetails[0]["subLocationID"];
	$dataToSave["subLocName"]				= "";//$bookingDetails[0]["sublocationName"];
	$dataToSave["airportCharges"]			= "";//$bookingDetails[0]["AirportCharges"];
	$dataToSave["vat"]						= $bookingDetails[0]["VatRate"];
	$dataToSave["pickupdrop_address"]		= "";
	$picDate = date_create($bookingDetails[0]["PickUpDate"]);
	$dataToSave["pickup_date"] 				= $picDate->format('Y-m-d');
	$dataToSave["pickup_time"]				 = $bookingDetails[0]["PickUpTime"];
	$drpDate = date_create($bookingDetails[0]["DropOffDate"]);
	$dataToSave["drop_date"] 				= $drpDate->format('Y-m-d');
	$dataToSave["drop_time"] 				= $bookingDetails[0]["DropOffTime"];
	
	if (isset($bookingDetails[0]["TotalKm"]) && $bookingDetails[0]["TotalKm"] != "")
	$dataToSave["distance"] 				= $bookingDetails[0]["TotalKm"];
	else
	$dataToSave["distance"] 				= "0";
	$dataToSave["tot_fare"] 				= $bookingDetails[0]["PaymentAmount"];
	$dataToSave["tot_gross_amount"] 		= $bookingDetails[0]["OriginalAmount"];
	$dataToSave["address"] 					= $bookingDetails[0]["PickUpAdd"];
	$dataToSave["remarks"] 					= $bookingDetails[0]["Remarks"];
	$dataToSave["full_name"] 				= $bookingDetails[0]["Fname"];
	$dataToSave["email_id"] 				= $bookingDetails[0]["EmailID"];
	$dataToSave["mobile"] 					= $bookingDetails[0]["mobile"];
	$dataToSave["discount"] 				= $bookingDetails[0]["IndicatedDiscPC"];
	if ($bookingDetails[0]["DiscountAmount"] != "") {
	$dataToSave["discount_amt"] 			= $bookingDetails[0]["DiscountAmount"];
	} else {
	$dataToSave["discount_amt"] 			= ""; 
	}
	 
	if($bookingDetails[0]["DiscountCode"]=='Array')
	{
		$dataToSave["promotion_code"] 			= ""; 
	}
	else
	{
		$dataToSave["promotion_code"] 		= $bookingDetails[0]["DiscountCode"];
	}
	$dataToSave["discount_coupon"] 			= ""; 
	$dataToSave["additional_srv"] 			= "";
	$dataToSave["additional_srv_amt"]		= $bookingDetails[0]["serviceAmount"];
	$dataToSave["chauffeur_charges"] 		= $bookingDetails[0]["ChauffeurCharges"];
	$dataToSave["cciid"] 					= $bookingDetails[0]["userid"];
	$dataToSave["coric"] 					= $bookingDetails[0]["trackID"];
	$dataToSave["ip"] 						= $_SERVER["REMOTE_ADDR"];
	$dataToSave["ua"] 						= $_SERVER["HTTP_USER_AGENT"];
	$dataToSave["entry_date"] 				= date('Y-m-d H:i:s');
	$dataToSave["User_billable_amount"] 	= $bookingDetails[0]["PaymentAmount"];   
	$dataToSave['payment_mode']				= 1 ;
	$dataToSave['payment_status']			= 2;
	$r2 = $db->callcenterinsert("cor_booking_new", $dataToSave);
	unset($dataToSave);
}
 $db->close();
}
}
}


?>	
<div class="content">
<div class="cor_left f_l">
<h1>Payment</h1>
<div class="p30">
<div class="pay_online">Pay Online</div>
<ul>
<li>Using VISA, MasterCard, AMEX and Debit Cards</li>
<li>Using cash cards</li>
</ul>
<div class="button_bg">

<?php

if (!$isFleetAvail) 
{
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Car is not available anymore.</label>
</div>
</fieldset>
<?php
} elseif (intval($bookingDetails[0]["PaymentStatus"] == 1)) {
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Payment has already been made for this booking.</label>
</div>
</fieldset>
<?php
/*** expire link*/
} elseif ($bookingDetails[0]["ExpiryStatus"] == "Expired") {
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Payment Link Has Expired..</label>
</div>
</fieldset>
<?php
} else {
	//echo '$bookingDetails[0]["Status"] -'. $bookingDetails[0]["Status"];
//if($bookingDetails[0]["Status"]=="Open" || $bookingDetails[0]["Status"]=="New") {
		
?>
<input type="hidden" name="cciid" id="cciid<?php echo $i; ?>" value="<?php echo $cciid; ?>" />
<form id="formPaymentOptions1" name="formPaymentOptions1" method="POST" action="<?php echo $action; ?>">
<input name="merchantTxnId" id="Order_Id" type="hidden" value="<?php echo $merchantTxnId; ?>" />
<input name="addressState" type="hidden" value="<?php echo $addressState; ?>" />
<input name="addressCity" type="hidden" value="<?php echo $addressCity; ?>" />
<input name="addressStreet1" type="hidden" value="<?php echo $addressStreet1; ?>" />
<input name="addressCountry" type="hidden" value="<?php echo $addressCountry; ?>" />
<input name="addressZip" type="hidden" value="<?php echo $addressZip; ?>" />
<input name="firstName" type="hidden" value="<?php echo $firstName; ?>" />
<input name="lastName" type="hidden" value="<?php echo $lastName; ?>" />
<input name="phoneNumber" type="hidden" value="<?php echo $phoneNumber; ?>" />
<input name="email" type="hidden" value="<?php echo $email; ?>" />
<input name="paymentMode" type="hidden" value="<?php echo $paymentMode; ?>" />
<input name="issuerCode" type="hidden" value="<?php echo $issuerCode; ?>" />
<input name="cardHolderName" type="hidden" value="<?php echo $cardHolderName; ?>" />
<input name="cardNumber" type="hidden" value="<?php echo $cardNumber; ?>" />
<input name="expiryMonth" type="hidden" value="<?php echo $expiryMonth; ?>" />
<input name="cardType" type="hidden" value="<?php echo $cardType; ?>" />
<input name="cvvNumber" type="hidden" value="<?php echo $cvvNumber; ?>" />
<input name="expiryYear" type="hidden" value="<?php echo $expiryYear; ?>" />
<input name="returnUrl" type="hidden" value="<?php echo $returnUrl; ?>" />
<input name="orderAmount" type="hidden" value="<?php echo $orderAmount; ?>" />
<input type="hidden" name="reqtime" value="<?php echo $time; ?>" />
<input type="hidden" name="secSignature" value="<?php echo $secSignature; ?>" />
<input type="hidden" name="currency" value="<?php echo $currency; ?>" />
<input class="payandbook" id="cnfbooking" type="submit" name="" Value="" id="">
</form>
<?php
//}

}
?>
</div>
</div>
<div class="h50"> </div>
<div class="border_b"></div>
<div class="p30">
<strong>NOTE</strong>
<ul class="note">
<li>Final amount will be as per actual</li>

</ul>
<div class="h50"> </div>
</div>
</div>
<div class="cor_right f_l">
<h1>Booking Summary</h1>

<?php
if ($bookingDetails[0]["CarCatName"] == 'Intermediate')
$carCatgName = "Budget";
else if ($bookingDetails[0]["CarCatName"] == 'Van')
$carCatgName = "Family";
else if ($bookingDetails[0]["CarCatName"] == 'Superior')
$carCatgName = "Business";
else if ($bookingDetails[0]["CarCatName"] == 'Premium')
$carCatgName = "Premium";
else if ($bookingDetails[0]["CarCatName"] == 'Luxury')
$carCatgName = "Luxury";
else if ($bookingDetails[0]["CarCatName"] == 'Special')
$carCatgName = "Super Luxury";
else
$carCatgName = $bookingDetails[0]["CarCatName"];

?>
<div class="p30">
<ul>
<li><strong>Car Type:-</strong> <?php echo $carCatgName; ?> - <?php echo  $bookingDetails[0]["ModelName"]; ?></li>
<li><strong>From:-</strong> <?php echo $bookingDetails[0]["OriginName"] ?></li>




<?php

if ($bookingDetails[0]["OutstationYN"] == 'true') 
	{
	$tour_type = "Outstation";
	} else {
	$tour_type = "Local";
	}



?>    
<li><strong>Service:-</strong> <?php echo  $tour_type; ?></li>

<?php
$pickDate = strtotime($bookingDetails[0]["PickUpDate"]);
$dropDate = strtotime($bookingDetails[0]["DropOffDate"]);
?>
<li><strong>Pickup Date:-</strong> <?php echo date('d M, Y', $pickDate); ?></li>
<!--<li><strong>Drop Date:-</strong><?php echo date('d M, Y', $dropDate); ?></li>-->
<li><strong>Pickup Time:-</strong> <?php echo $bookingDetails[0]["PickUpTime"];?></li>

<!--<li><strong>Pickup Location:-</strong> <?php echo $bookingDetails[0]["sublocationName"] ?></li>-->
<li><strong>Pickup Address:-</strong> <?php echo $bookingDetails[0]["PickUpAdd"] ?></li>


</ul>
<div class="pay_amount">Payable Amount</div>

<?php
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack") 
{
?>
<ul>
<span>Rs. <?php echo intval($bookingDetails[0]["PaymentAmount"] - $_POST["discountAmt"]); ?>/-</span></h3>
<li>Original Amount:</span> <?php echo $bookingDetails[0]["PaymentAmount"]; ?></li>
<li>PayBack Discount Amount:</span> <?php echo $_POST["discountAmt"]; ?></li>
<li>PayBack Points Redeem:</span> <?php echo $_POST["redeemedPts"]; ?></li>
</ul>
<?php
}
else
{
?>
<h2>Rs. <?php echo intval($bookingDetails[0]["PaymentAmount"]); ?>/-</h2>
<?php
}
?>
</div>
</div>
<div class="clr"> </div>
</div>
<!--footer-->
<?php //include("./mobileapp/footer.php")?>
<!--footer.php-->
</body>
</html>
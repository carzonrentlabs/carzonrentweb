<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Watts Up | Carzonrent</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers cab or car rental services for Delhi, Mumbai, Bangalore, Hyderabad, Gurgaon, Noida and  Pune locations." />
<meta name="keywords" content="car rental, book a cab, car hire, rent a car" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>


<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/expand.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
$(function() {
    $("h3.expand").toggler();
    $("div.other").expandAll({
      expTxt : "[Show]", 
      cllpsTxt : "[Hide]",
      ref : "ul.collapse",
      showMethod : "show",
      hideMethod : "hide"
    });
    $("div.post").expandAll({
      expTxt : "[Read this entry]", 
      cllpsTxt : "[Hide this entry]",
      ref : "div.collapse", 
      localLinks: "p.top a"    
    });    
});
//--><!]]>
</script>
<style>
	.whatsup{
	width:100%;
	height:158px;
	background-image:url('<?php echo corWebRoot; ?>/img_e2o/header.jpg');
	background-position:center center;
	background-repeat:no-repeat;
	background-color:#ededed;
	float:left;
	border-bottom:solid 1px #e2572d;
	}
	.liner
	{
	color: #414142;
	float: left;
	font-size: 15px;
	font-weight: normal;
	padding: 10px 0px;
	width: 100%;
	}
	.mainblock
	{float:left;width:100%;margin:10px 0px 20px 0px;}
	.formblock
	{
		float:left;
		width:950px;border: solid 1px #e2572d;
		-moz-border-radius: 5px;
		border-radius: 5px;
		-moz-box-shadow: 5px 5px 5px #8c8c8c;
		-webkit-box-shadow: 5px 5px 5px #8c8c8c;
		box-shadow: 5px 5px 5px #8c8c8c;
		padding:20px;
	}
	.e2o
	{
		float:right;width:309px;
	}
	.greybandcol
	{
		background-color: #e2572d;
		float: left;
		height: 25px;
		/*-moz-border-radius: 3px;
		border-radius: 3px;*/
		width: 100%;
	}
	#tandc tr{border:solid 1px #E2572D; }
	#tandc td{border:solid 1px #E2572D; }
.bdr
{border-bottom:solid 1px #e2572d;}

.heading {
    font-family: Arial;
    font-size: 20px;
    line-height: 44px;
}

</style>

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
<script type="text/javascript">
	$(function(){
		$('select.styled').customSelect();
	});
</script>
</head>
<body>
	<!--Header Start Here-->
	<?php include_once("./includes/header.php"); ?>
		    <div style="margin: 0 auto;position: relative;width: 100%;">
			<div class="whatsup"></div>
		    </div>
		    <div class="nbd_center">
			<div id="nbd">
				<div class="heading">Terms & Conditions</div>
				<div class="mainblock">
					<div class="formblock">
		<table cellspacing="0" border="1" id="tandc" width="100%" style="border-collapse:collapse;margin-bottom:10px;border-color:#E2572D;">
			<tr>
				<td style="width:25%;padding:5px;"><b>Criteria</b></td>
				<td style="padding:5px;"><b>Description</b></td>
			</tr>
			<tr>
				<td style="padding:5px;">Eligibility Age</td>
				<td style="padding:5px;">25 Years or above</td>
			</tr>
			<tr>
				<td style="padding:5px;">Documents</td>
				<td style="padding:5px;">Passport , Driving license , Credit Card with desired limit of Security deposit</td>
			</tr>
			<tr>
				<td style="padding:5px;">Rental Period</td>
				<td style="padding:5px;">Rental period will be 24 Hrs. from customer booking time , included in this 24 Hour is the time where customer collect and return the rental vehicle</td>
			</tr>
			<tr>
				<td style="padding:5px;">Other terms</td>
				<td style="padding:5px;">As per agreement</td>
			</tr>
			<tr>
				<td style="padding:5px;">VAT</td>
				<td style="padding:5px;">As  per state</td>
			</tr>
			<tr>
				<td style="padding:5px;">Pre authorization from credit card</td>
				<td style="padding:5px;">Will be done at the time of vehicle delivery , this will be treated as security deposit , in case of any damage in the car , amount will be deducted from the pre authorized amount.</td>
			</tr>
			<tr>
				<td style="padding:5px;">Delivery / Collection</td>
				<td style="padding:5px;">Car has to be picked up from our Hub / Airport and returned to same Hub / airport it was hired from</td>
			</tr>
			<tr>
				<td style="padding:5px;">Extra day</td>
				<td style="padding:5px;">In case customer want to extend his travel , there will the additional charge of 30% on per day rental.</td>
			</tr>
			<tr>
				<td style="padding:5px;">Documents and Accessories</td>
				<td style="padding:5px;">Missing documents or lost accessories and tools would be charged extra.</td>
			</tr>
			<tr>
				<td style="padding:5px;">GPS Navigation system</td>
				<td style="padding:5px;">Available on request with additional charges (Subject to availability)</td>
			</tr>
			<tr>
				<td style="padding:5px;">Child Safety Seat</td>
				<td style="padding:5px;">Availability of child seat is on request with additional charges. The age or weight of the child must be advised at the time of booking (Subject to availability)</td>
			</tr>
			<tr>
				<td style="padding:5px;">Interstate Tax and Toll Charges</td>
				<td style="padding:5px;">All Interstate taxes and toll will be paid by customer</td>
			</tr>
		</table>
	</div>
			
					</div>
				</div>
				<div class="tc_dis">
					<b>AVAILABLE IN DELHI, MUMBAI, AND BANGALORE <br />
					<sup>*</sup> T &amp; C apply</b>: Service tax extra. <b>Unlimited driving. Free charging facility available</b>
				</div>
			</div>
		</div>
		
	<!--footer Starts Here-->
	<?php include_once("./includes/footer.php"); ?>
	<!--footer Ends Here-->
</body>
</html>

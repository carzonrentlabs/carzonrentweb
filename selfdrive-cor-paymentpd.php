<?php
error_reporting(0);
session_destroy();
session_start();
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
include_once('./classes/cor.gp.class.php');
include_once('./classes/cor.mysql.class.php');
include_once("./includes/cache-func.php");

$tab = 3;
if (isset($_POST["tab"]))
$tab = $_POST["tab"];

$bookingID = "";
if (isset($_GET["bookingid"]) && $_GET["bookingid"] != "")
$bookingID = $_GET["bookingid"];
$ref = "";
if (isset($_GET["ref"]) && $_GET["ref"] != "")
$ref = $_GET["ref"];
if ($bookingID == "")
header("Location: http://www.carzonrent.com/");
$cor = new COR();

if ($ref != "") 
{
	$data = array();
	$data["data"] = $ref;
	$res = $cor->_CORDecrypt($data);
	$ref = $res->{'DecryptResult'};
	unset($data);
}

if (strtolower($ref) == "honeywell") 
{
	$data = array();
	$data["data"] = $bookingID;
	$res = $cor->_CORDecrypt($data);
	$bookingID = $res->{'DecryptResult'};
	unset($data);
}

if ($bookingID == "")
header("Location: http://www.carzonrent.com/");
$res = $cor->_CORGetBookingsSelfdriveDetail(array("BookingID" => $bookingID));
$bookingDetails = array();
if ($res->{'GetBookingDetailResult'}->{'any'} != "") 
{
	$myXML = new CORXMLList();
	$bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="googlebot" content="noindex, nofollow">
<meta name="googlebot" content="noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="js/customSelect.jquery.js"></script>
<script type="text/javascript" src="js/_bb_general_v3.js"></script>
<script type="text/javascript" src="js/validation.js?v=<?php echo mktime(); ?>"></script>
<script type="text/javascript" src="js/funtion_init.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/secure.js"></script>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

<?php 


if ($bookingDetails[0]["TourType"] == "Self Drive") {
?>
<style>
.confirmbooking a {background: url("<?php echo corWebRoot; ?>/images/confirmbooking-myles.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
}
.qm{background:url("<?php echo corWebRoot; ?>/images/red.png") no-repeat  scroll 0 0 rgba(0, 0, 0, 0) !important;color:#fff !important;}
.innerpages .rightside .tpdiv li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll left 8px rgba(0, 0, 0, 0) !important;}
.confirmbookingredeem a {background: url("<?php echo corWebRoot; ?>/images/confirmbookingredeem-r.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
}
.innerpages .rightside .tpdiv h3 {color: #db4626 !important;}
#feedbackpopup {background-image: url("<?php echo corWebRoot; ?>/images/feedback-r.jpg") !important;}
ul.listarrow li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg")  no-repeat 30px 12px; !important;}
.payandbook a {background: url("<?php echo corWebRoot; ?>/images/payandbook-r.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;}
.middiv ul li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll 0 8px rgba(0, 0, 0, 0) !important;}
#registration{border: 10px solid #db4626 !important;}
.details_wrapper ul li{background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat 0px 5px rgba(0, 0, 0, 0) !important;}
ul.shadetabst {background: url("<?php echo corWebRoot; ?>/images/myles-tb.jpg") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
</style>
<?php
}
?>
<script>
$(function () {
$('select.time').customSelect();
$('select.area').customSelect();
})

function resgiter_cont() {
document.getElementById("register").style.display = 'none';
document.getElementById("travel_details").style.display = 'block';
}

function _submit2()
{
var chk;
chk = true;
if (chk)
chk = isFilledText(document.getElementById("txtcardnopb"), "", "PayBack Loyality Card number can't be left blank.");
if (chk)
chk = isFilledText(document.getElementById("txtpbpoints"), "", "Points to Redeem can't be left blank.");
if (chk && document.getElementById('txtcardnopb').value != "" && document.getElementById('txtpbpoints').value != "") {
var availablepoints = parseInt(document.getElementById('txtpointsincard').value);
var pointstoredeem = parseInt(document.getElementById('txtpbpoints').value);
if (availablepoints >= pointstoredeem) {
document.getElementById('formPaymentOptions1').action = webRoot + "/intermediate1.php";
//alert(document.getElementById('formPaymentOptions1').action);
document.getElementById('formPaymentOptions1').submit();
}
else {
alert("Insufficient points in your PayBack card.");
return false;
}
}
}


field.addEventListener("change", function() {
  // And save the results into the session storage object
  sessionStorage.setItem("autosave", field.value);
});


</script>
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>

</head>
<body>
<?php include_once("./includes/header.php"); ?>
<div class="tbbingouter">
<div class="main">
</div>
</div>
<?php
$isFleetAvail = true;
if ($bookingDetails[0]["TourType"] == "Self Drive") {
$isFleetAvail = false;
$pDate = date_create($bookingDetails[0]["PickUpDate"]);
$dDate = date_create($bookingDetails[0]["DropOffDate"]);
$res = $cor->_COR_SelfDrive_ISAvailability(array("PkgID" => $bookingDetails[0]["IndicatedPkgID"], "fromDate" => $pDate->format("Y-m-d"), "DateIn" => $dDate->format("Y-m-d"), "PickupTime" => $bookingDetails[0]["PickUpTime"], "DropOffTime" => $bookingDetails[0]["DropOffTime"], "SubLocationID" => $bookingDetails[0]["subLocationID"]));
$myXML = new CORXMLList();
$fleetStatus = $myXML->xml2ary($res);
$isFleetAvail = (bool) $fleetStatus[0]["IsAvailable"];
}
/*$bookingDetails = array();
   if($res->{'GetBookingDetailResult'}->{'any'} != "")
   {
    $myXML = new CORXMLList();
    $bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});
   } 
   print_r($res); */
unset($cor);
$ll = "";
$dll = "";
$travelData = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
$points = count($travelData->{'routes'}[0]->{'legs'});
$ll = $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} . "," . $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'};
$dll = ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lat'}) . "," . ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lng'});
$totalKM = 0;
for ($i = 0; $i < $points; $i++) {
$dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
if ($i == 0)
$dwstate = $dispOrg;
$dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
if ($dwstate == "")
$dwstate = $dispDest;
else
$dwstate .= " to:" . $dispDest;
}
if ($tab == 3) 
{
	
if (isset($_COOKIE["tcciid"]))
$cciid = $_COOKIE["tcciid"];
if (isset($_COOKIE["cciid"]))
$cciid = $_COOKIE["cciid"];


?>
<div id="country3" class="tabcontent">		
<?php
$_SESSION["rurl"] = urlencode("http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
$mtime = round(microtime(true) * 1000);

$_SESSION["hdBookingID"] = $_GET["bookingid"];
$_SESSION["hdCarModel"] = $bookingDetails[0]["CarCatName"];
$_SESSION["hdOriginName"] = $bookingDetails[0]["OriginName"];
$_SESSION["hdDestinationName"] = $bookingDetails[0]["destination"];
$_SESSION["hdPickdate"] = $bookingDetails[0]["PickUpDate"];
$_SESSION["picktime"] = $bookingDetails[0]["PickUpTime"];
$_SESSION["hdDropdate"] = $bookingDetails[0]["DropDate"];
$_SESSION["address"] = $bookingDetails[0]["PickUpAdd"];
$_SESSION["totFare"] = $bookingDetails[0]["PaymentAmount"];
$_SESSION["name"] = $bookingDetails[0]["Fname"] . " " . $bookingDetails[0]["Lname"];
$_SESSION["monumber"] = $bookingDetails[0]["mobile"];
$_SESSION["email"] = $bookingDetails[0]["EmailID"];
$_SESSION["cciid"] = $bookingDetails[0]["userid"];
$_SESSION["PickUpDate"] = $bookingDetails[0]["PickUpDate"];
$_SESSION["DropOffDate"] = $bookingDetails[0]["DropOffDate"];
$_SESSION["originId"] = $bookingDetails[0]["originId"];
$_SESSION["hdTourtype"] = $bookingDetails[0]["TourType"];
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack") {
$_SESSION["empcode"] = $_POST["empcode"];
$_SESSION["discAmt"] = $_POST["discountAmt"];
$_SESSION["DiscTxnID"] = $_POST["pagetransactionId"];
}
if ($bookingDetails[0]["DiscountCode"] == "FLATDISCOUNT") {
	$_SESSION["empcode"] = $bookingDetails[0]["DiscountCode"];
	$_SESSION["discAmt"] = $bookingDetails[0]["DiscountAmount"];
}

require_once("./includes/libfuncs.php");
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack")
$Amount = $bookingDetails[0]["PaymentAmount"] - $_POST["discountAmt"];
else
$Amount = $bookingDetails[0]["PaymentAmount"];

$post_url = "https://www.ccavenue.com/shopzone/cc_details.jsp";
$Merchant_Id = "M_gau22831_22831"; //This id(also User Id)  available at "Generate Working Key" of "Settings & Options" 
//$Order_Id = $bookingDetails[0]["trackID"];
$Redirect_Url = corWebRoot."/selfdrive-cor-thanks.php"; //your redirect URL where your customer will be redirected after authorisation from CCAvenue
$Order_Id = $bookingDetails[0]["trackID"]; //your script should substitute the order description in the quotes provided here
//$Order_Id = "CORIC-" . $mtime;
$WorkingKey = "72dxgbita13wdybvv9ar8du7bzizgqhq"; //put in the 32 bit alphanumeric key in the quotes provided here.Please note that get this key ,login to your CCAvenue merchant account and visit the "Generate Working Key" section at the "Settings & Options" page. 
$Checksum = getCheckSum($Merchant_Id, $Amount, $Order_Id, $Redirect_Url, $WorkingKey);

if (intval($bookingDetails[0]["PaymentStatus"] == 0)) {

$_SESSION["orderid"] = $Order_Id;
if ($Order_Id != "") {
$db = new MySqlConnection(CONNSTRING);
$db->open();
$sql = "SELECT uid FROM cor_booking_new WHERE coric = '" . $bookingDetails[0]["trackID"] . "'";
$r = $db->query("query", $sql);
if (array_key_exists("response", $r)) 
{
$dataToSave = array();
$dataToSave["source"] 					= "COR2";
$dataToSave["tour_type"] 				= $bookingDetails[0]["TourType"];
$dataToSave["package_id"] 				= $bookingDetails[0]["IndicatedPkgID"];
$dataToSave["car_cat"] 					= $bookingDetails[0]["CarCatName"];
$dataToSave["origin_name"] 				= $bookingDetails[0]["OriginName"];
$dataToSave["origin_id"] 				= $bookingDetails[0]["originId"];
$dataToSave["destinations_name"] 		= $bookingDetails[0]["destination"];

$dataToSave["destinations_id"] 			= $bookingDetails[0]["destinationid"];
$dataToSave["model_id"]				    = $bookingDetails[0]["Modelid"];
$dataToSave["model_name"]			    = $bookingDetails[0]["ModelName"];
$dataToSave["pkg_type"]			    	= $bookingDetails[0]["PackageType"];
$dataToSave["BasicAmt"]			    	= $bookingDetails[0]["BasicAmount"];
$dataToSave["OriginalAmt"]				= $bookingDetails[0]["OriginalAmount"];
$dataToSave["WeekDayDuration"]			= $bookingDetails[0]["Weekdayduration"];
$dataToSave["WeekEndDuration"]			= $bookingDetails[0]["WeekEndduration"];
$dataToSave["FreeDuration"]				= $bookingDetails[0]["Freeduration"];
$dataToSave["subLoc"]					= $bookingDetails[0]["subLocationID"];
$dataToSave["subLocName"]				= $bookingDetails[0]["sublocationName"];
$dataToSave["airportCharges"]			= $bookingDetails[0]["AirportCharges"];
$dataToSave["vat"]						= $bookingDetails[0]["VatRate"];
$dataToSave["pickupdrop_address"]		= $bookingDetails[0]["Pickup_dropoff_Address"];

$picDate = date_create($bookingDetails[0]["PickUpDate"]);
$dataToSave["pickup_date"] 				= $picDate->format('Y-m-d');
$dataToSave["pickup_time"]				 = $bookingDetails[0]["PickUpTime"];
$drpDate = date_create($bookingDetails[0]["DropOffDate"]);
$dataToSave["drop_date"] 				= $drpDate->format('Y-m-d');
$dataToSave["drop_time"] 				= $bookingDetails[0]["DropOffTime"];

if ($bookingDetails[0]["hdTourtype"] == "Self Drive")
$dataToSave["duration"] 				= $bookingDetails[0]["duration"];

if (isset($bookingDetails[0]["TotalKm"]) && $bookingDetails[0]["TotalKm"] != "")
$dataToSave["distance"] 				= $bookingDetails[0]["TotalKm"];
else
$dataToSave["distance"] 				= "0";
$dataToSave["tot_fare"] 				= $bookingDetails[0]["PaymentAmount"];
$dataToSave["tot_gross_amount"] 		= $bookingDetails[0]["OriginalAmount"];
$dataToSave["address"] 					= $bookingDetails[0]["PickUpAdd"];
$dataToSave["remarks"] 					= $bookingDetails[0]["Remarks"];
$dataToSave["full_name"] 				= $bookingDetails[0]["Fname"]; //. " " . $bookingDetails[0]["Lname"];
$dataToSave["email_id"] 				= $bookingDetails[0]["EmailID"];
$dataToSave["mobile"] 					= $bookingDetails[0]["mobile"];
$dataToSave["discount"] 				= $bookingDetails[0]["IndicatedDiscPC"];
if ($bookingDetails[0]["DiscountAmount"] != "") {
$dataToSave["discount_amt"] 			= $bookingDetails[0]["DiscountAmount"];
} else {
$dataToSave["discount_amt"] 			= ""; 
}
if ($bookingDetails[0]["DiscountCode"] == "FLATDISCOUNT") 
{
	$dataToSave["promotion_code"] 			= $bookingDetails[0]["DiscountCode"];
} 
else if($bookingDetails[0]["DiscountCode"]=='Array')
{
	$dataToSave["promotion_code"] 			= ""; 
}
else
{
	$dataToSave["promotion_code"] 			= $bookingDetails[0]["DiscountCode"];
}
$dataToSave["discount_coupon"] 			= ""; 
$dataToSave["additional_srv"] 			= $bookingDetails[0]["AddtionalService"];
$dataToSave["additional_srv_amt"]		= $bookingDetails[0]["serviceAmount"];
$dataToSave["chauffeur_charges"] 		= $bookingDetails[0]["ChauffeurCharges"];
$dataToSave["cciid"] 					= $bookingDetails[0]["userid"];
$dataToSave["coric"] 					= $bookingDetails[0]["trackID"];

//$dataToSave["ip"] 						 = $_SESSION['ipaddress'];
//$dataToSave["ua"] 			             = $_SESSION['browser'];
$dataToSave["ip"] 						=  $_COOKIE['ipaddress'];
$dataToSave["ua"] 						= $_COOKIE['browser'];
$dataToSave["entry_date"] 				= date('Y-m-d H:i:s');
$dataToSave["User_billable_amount"] 	= $bookingDetails[0]["PaymentAmount"];   

$r = $db->insert("cor_offline_booking", $dataToSave);
$r2 = $db->insert("cor_booking_new", $dataToSave);
unset($dataToSave);
$xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
$xmlToSave .= "<ccvtrans>";
$xmlToSave .= "<hdCarModel>NA</hdCarModel>";
$xmlToSave .= "<hdOriginName>" . $bookingDetails[0]["originId"] . "</hdOriginName>";
$xmlToSave .= "<hdOriginID>NA</hdOriginID>";
$xmlToSave .= "<hdDestinationName>" . $bookingDetails[0]["destination"] . "</hdDestinationName>";
$xmlToSave .= "<hdDestinationID>NA</hdDestinationID>";
$xmlToSave .= "<hdPkgID>NA</hdPkgID>";
$xmlToSave .= "<hdCat>" . $bookingDetails[0]["CarCatName"] . "</hdCat>";
$xmlToSave .= "<hdCarCatID></hdCarCatID>";
$xmlToSave .= "<PkgRate>NA</PkgRate>";
$xmlToSave .= "<duration>NA</duration>";
$xmlToSave .= "<totFare>" . $bookingDetails[0]["PaymentAmount"] . "</totFare>";
$xmlToSave .= "<tab>3</tab>";
$xmlToSave .= "<hdPickdate>" . $bookingDetails[0]["PickUpDate"] . "</hdPickdate>";
$xmlToSave .= "<hdDropdate>" . $bookingDetails[0]["DropOffDate"] . "</hdDropdate>";
$xmlToSave .= "<hdTourtype>" . $bookingDetails[0]["TourType"] . "</hdTourtype>";
$xmlToSave .= "<monumber>" . $bookingDetails[0]["mobile"] . "</monumber>";
$xmlToSave .= "<name>" . $bookingDetails[0]["Fname"] . " " . $bookingDetails[0]["Lname"] . "</name>";
$xmlToSave .= "<email>" . $bookingDetails[0]["EmailID"] . "</email>";
$xmlToSave .= "<cciid>" . $bookingDetails[0]["userid"] . "</cciid>";
$xmlToSave .= "<bookTime>NA</bookTime>";
$xmlToSave .= "<userTime>" . $bookingDetails[0]["PickUpTime"] . "</userTime>";
$xmlToSave .= "<bookDays>NA</bookDays>";
$xmlToSave .= "<tHourP>NA</tHourP>";
$xmlToSave .= "<tMinP>Na</tMinP>";
$xmlToSave .= "<tHourD>Na</tHourD>";
$xmlToSave .= "<tMinD>NA</tMinD>";
$xmlToSave .= "<totAmount>" . $bookingDetails[0]["PaymentAmount"] . "</totAmount>";
$xmlToSave .= "<vat>NA</vat>";
$xmlToSave .= "<secDeposit>NA</secDeposit>";
$xmlToSave .= "<discount>" . $bookingDetails[0]["IndicatedDiscPC"] . "</discount>";
$xmlToSave .= "<discountAmt>NA</discountAmt>";
$xmlToSave .= "<address>" . $bookingDetails[0]["PickUpAdd"] . "</address>";
$xmlToSave .= "<remarks>" . $bookingDetails[0]["Remarks"] . "</remarks>";
$xmlToSave .= "<empcode>Promotion code</empcode>";
$xmlToSave .= "<disccode>Discount coupon number</disccode>";
$xmlToSave .= "<source>COR-PAY</source>";
$xmlToSave .= "</ccvtrans>";
if (!is_dir("./xml/pre-trans/" . date('Y')))
mkdir("./xml/pre-trans/" . date('Y'), 0775);
if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m')))
mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m'), 0775);
if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
createcache($xmlToSave, "./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $Order_Id . ".xml");
}
 $db->close();
}
}
?>	




<div class="main">
<div class="innerpages">
<div class="leftside" style="min-height:379px!important">
<h1>Payment</h1>
<!--<input type="radio" name="rdoPayment" id="rdo1" value="1" onclick="javascript: _paymentCheck(this.id);" />-->
<?php
if (!$isFleetAvail) {
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Car is not available anymore.</label>
</div>
</fieldset>
<?php
} elseif (intval($bookingDetails[0]["PaymentStatus"] == 1)) {
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Payment has been done for this booking.</label>
</div>
</fieldset>
<?php
/*         * * exprying link *** */
} elseif (strtolower($bookingDetails[0]["ExpiryStatus"] == "Expired")) {
        ?>
                                                <fieldset class="payment">
                                                    <div class="leftfieldset">
                                                        <label style="width:500px !important;">Payment Link Has Expired..</label>
                                                    </div>
                                                </fieldset>
        <?php
    } else {
if (strtolower($bookingDetails[0]["Status"]) != "open") {
?>
<form id="formPaymentOptions1" name="formPaymentOptions1" method="POST" action="https://www.ccavenue.com/shopzone/cc_details.jsp" 
<?PHP if($bookingDetails[0]["destinationid"]!='69'){ ?> 
onsubmit="return troggleMail(document.getElementById('formPaymentOptions1'));"
<?php } ?>><!---change----->
<input type="hidden" name="Merchant_Id" value="M_gau22831_22831">
<input type="hidden" name="Merchant_Param" value="<?php echo $bookingID; ?>"> 
<input type="hidden" name="Order_Id" value="<?php echo $Order_Id; ?>">
<input type="hidden" name="Redirect_Url" value="<?php echo corWebRoot;?>/selfdrive-cor-thanks.php">
<input type="hidden" name="Checksum" value="<?php echo $Checksum ?>">
<input type="hidden" id="txtAmount" name="Amount" value="<?php echo $Amount; ?>">
<input type="hidden" name="billing_cust_name" value="<?php echo $bookingDetails[0]["Fname"] . " " . $bookingDetails[0]["Lname"]; ?>"> 
<input type="hidden" name="billing_cust_tel" value="<?php echo $bookingDetails[0]["mobile"]; ?>"> 
<input type="hidden" name="billing_cust_email" value="<?php echo $bookingDetails[0]["EmailID"]; ?>">
<input type="hidden" name="bookingid" id="bookingid" value="<?php echo $bookingID; ?>">	
<input type="hidden" name="ipaddress" id="ipaddress" value="">	
<input type="hidden" name="browser" id="browser" value="">				
<fieldset class="payment">
<div class="leftfieldset">
<label>Pay Online</label>
<ul class="listarrow">
<li>Using VISA, MasterCard, AMEX and Debit Cards</li>
<li>Using cash cards</li>
</ul>
<!---change----->
<?PHP
$css =''; 
if($bookingDetails[0]["destinationid"]=='69')
{ 
    $css_checkbox ='margin-left:24px;margin-top: 8px;display:none;';
	$css_button ='display:block;';
} 
else 
{
	$css_checkbox ='margin-left:24px;margin-top: 8px;';
	$css_button ='display:none;';
}
?>
<p style='<?php echo $css_checkbox;?>'><!---change----->
To make the pick-up process quicker and smoother, simply tick on the Terms & Conditions below and proceed to payment.</p>
<p style='<?php echo $css_checkbox;?>'><!---change----->
Please note a pre-authorisation of only Rs. 5000 will be charged at the time of pick-up from you and will be released to you at the time of drop-off subject to deductions due to mis-use or damage to the car.
</p>
<p style='<?php echo $css_checkbox;?>'><!---change----->
<input type="checkbox" name="tc" id="tc" style="" onclick="checkbox_action();" />&nbsp;<span>I agree to the <a  href="javascript:void(0);" onclick="tnclink_action();" title="Click to show Terms And Conditions">Terms & Conditions.</a></span>
</p>
<!------tnc agreement start---->
<?php include("./callcenter-tnc/termandcondition.php");?>  <!---change----->
<!------tnc agreement end---->
<?php
if (!isset($_POST["empcode"]) && !isset($_GET["ref"])) {
?>

<div style="float:left;margin-top:25px">
<div class="heading" style="display:none" id="pblc">PayBack Loyalty Discount</div>
<div class="middiv" style="display:none" id="pbpd">
<fieldset class="register">							
<div class="fieldrow">
<div class="row_left" style="width:181px;position:relative;right:10px;">
<label>PayBack Loyalty Card No.</label>
</div>
<div class="row_right">
<input type="text" name="txtcardnopb" id="txtcardnopb" maxlength="16" value="" onkeypress="javascript: return _allowNumeric(event);" onblur="javascript: if (this.value != '') {
_redeemPoints2();
}" />
</div>
</div>
<div class="fieldrow">
<div class="row_left">
<label>Points In Card</label>
</div>
<div class="row_right">
<input type="text" name="txtpointsincard" id="txtpointsincard" maxlength="16" value="" readonly="readonly"/>
</div>
</div>
<div class="fieldrow">
<div class="row_left">
<label>Points to Redeem</label>
</div>
<div class="row_right">
<input type="text" name="txtpbpoints" id="txtpbpoints" maxlength="10" value="" onkeypress="javascript: return _allowNumeric(event);" onblur="javascript: document.getElementById('txtamounttoredeem').readOnly = false;
document.getElementById('txtamounttoredeem').value = parseInt(this.value / 4);
document.getElementById('txtamounttoredeem').readOnly = true;" />&nbsp;<!-- <a href="javascript: void(0);" onclick="javascript: _getPBRedeem();">Redeem</a> --><br />4 Payback Points = 1 INR
</div>
</div>
<div class="fieldrow">
<div class="row_left">
<label>Amount to Redeem</label>
</div>
<div class="row_right">
<input type="text" name="txtamounttoredeem" id="txtamounttoredeem" maxlength="6" value="" readonly="readonly"/>
</div>
</div>
</fieldset>
</div>
<fieldset class="register">
<div class="fieldrow">
<div class="row_left">&nbsp;</div>
<div class="row_right">
<style type="text/css">
.styled-button-1 {
	-webkit-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	-moz-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	color:#333;
	background-color:#FA2;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:none;
	font-family:'Helvetica Neue',Arial,sans-serif;
	font-size:16px;
	font-weight:700;
	height:36px;
	padding:4px 16px;
	text-shadow:#FE6 0 1px 0
}
</style>
<!----<div class="payandbook" id="cnfbooking"><a onclick="javascript: document.forms[0].submit();" href="javascript: void(0);"></a></div>-->
<input type="submit" value="Pay And Book" id="subbyabhi" class="styled-button-1" style='<?php echo $css_button;?>'/><!---change----->
<div class="confirmbookingredeem" id="cnfbookingredeem"><a onclick="javascript: _submit2();" href="javascript: void(0);"></a></div>
</div>

</div>
</fieldset>
<?php
} else {
?>
<fieldset class="register">
<div class="fieldrow">
<div class="row_left">&nbsp;</div>
<div class="row_right">
<div class="payandbook" id="cnfbooking"><a onclick="javascript: document.forms[0].submit();" href="javascript: void(0);"></a></div>
</div>

</div>
</fieldset>
<?php
}
?>
</div>			
</fieldset>
</form>
<?php
//$AMEXAmount  = intval($Amount - ($Amount * 0.1));
//$Checksum = getCheckSum($Merchant_Id,$AMEXAmount,$Order_Id ,$Redirect_Url,$WorkingKey);
?>
<!--<form id="formPaymentOptionsAMEX" name="formPaymentOptionsAMEX" method="POST" action="https://www.ccavenue.com/shopzone/cc_details.jsp">
<input type="hidden" name="Merchant_Id" value="M_gau22831_22831">
<input type="hidden" name="Merchant_Param" value="<?php //echo $_GET["bookingid"];  ?>"> 
<input type="hidden" name="Order_Id" value="<?php //echo $Order_Id;  ?>">
<input type="hidden" name="Redirect_Url" value="http://www.carzonrent.com/cor-thanks.php">
<input type="hidden" name="Checksum" value="<?php //echo $Checksum  ?>">
<input type="hidden" name="Amount" value="<?php //echo $AMEXAmount; ?>">
<input type="hidden" name="billing_cust_name" value="<?php //echo $bookingDetails[0]["Fname"] . " " . $bookingDetails[0]["Lname"];?>"> 
<input type="hidden" name="billing_cust_tel" value="<?php //echo $bookingDetails[0]["mobile"];?>"> 
<input type="hidden" name="billing_cust_email" value="<?php //echo $bookingDetails[0]["EmailID"]; ?>">
<fieldset class="payment">
<div class="leftfieldset">
<label>Online Payment by American Express</label>
<ul class="listarrow">
<li>Get 10% off</li>
</ul>
</div>
<div class="rightfieldset">
<div class="payandbook"><a onclick="javascript: document.forms[1].submit();" href="javascript: void(0);"></a></div>
</div>
</fieldset>
</form>-->
<?php
} else {
?>
<fieldset class="payment">
<div class="leftfieldset">
<label style="width:500px !important;">Payment could not be made for this booking now.</label>
</div>
</fieldset>
<?php
}
}
?>

<?php
if (intval($bookingDetails[0]["PaymentStatus"] != 1) && strtolower($bookingDetails[0]["Status"]) != "open") {
?>
<!--<div class="border_b"></div>
<div class="middiv"> <strong>NOTE</strong>
<ul>
<!--<li>Final amount will be as per actual</li>-->
<!--<li>Pending amount after advance adjustment is to be paid to driver as cash at the end of journey</li>-->
<!--<li>0% cancellation charges on amount paid as advance</li>-->
<!---</ul>
</div>-->
<?php
}
?>
</div>
<div class="rightside">
<h1>Booking Summary</h1>
<div class="tpdiv">
<div class="clr"></div>
<?php
if ($bookingDetails[0]["CarCatName"] == 'Intermediate')
$carCatgName = "Budget";
else if ($bookingDetails[0]["CarCatName"] == 'Van')
$carCatgName = "Family";
else if ($bookingDetails[0]["CarCatName"] == 'Superior')
$carCatgName = "Business";
else if ($bookingDetails[0]["CarCatName"] == 'Premium')
$carCatgName = "Premium";
else if ($bookingDetails[0]["CarCatName"] == 'Luxury')
$carCatgName = "Luxury";
else if ($bookingDetails[0]["CarCatName"] == 'Special')
$carCatgName = "Super Luxury";
else
$carCatgName = $bookingDetails[0]["CarCatName"];
?>
<p><span>Car Type:</span> <?php echo $carCatgName; ?> - <?php $bookingDetails[0]["CarModal"]; ?></p>
<p><span>From:</span> <?php echo $bookingDetails[0]["OriginName"] ?></p>
<?php
if ($bookingDetails[0]["TourType"] == "Self Drive") {
?>    
<p><span>Service:</span> <?php echo "Self Drive"; ?> </p>
<?php
}
?> 
<?php

$pickDate = strtotime($bookingDetails[0]["PickUpDate"]);
$dropDate = strtotime($bookingDetails[0]["DropOffDate"]);
?>
<p><span>Pickup Date:</span> <?php echo date('d M, Y', $pickDate); ?></p>
<p><span>Drop Date:</span> <?php echo date('d M, Y', $dropDate); ?></p>
<p><span>Pickup Time:</span> <?php echo $bookingDetails[0]["PickUpTime"];
} ?> hrs</p>
<?php
if ($bookingDetails[0]["TourType"] == "Self Drive") {
?>
<p><span>Pickup Location:</span> <?php echo $bookingDetails[0]["sublocationName"] ?></p>
<p><span>Pickup Address:</span> <?php echo $bookingDetails[0]["sublocationAdd"] ?></p>
<?php
} else {
?>
<p><span>Pickup Address:</span> <?php echo $bookingDetails[0]["PickUpAdd"] ?></p>
<?php
}
?>

<h3>Payable Amount<br />
<?php
if (isset($_POST["empcode"]) && $_POST["empcode"] == "PayBack") {
?>
<span>Rs <?php echo intval($bookingDetails[0]["PaymentAmount"] - $_POST["discountAmt"]); ?>/-</span></h3>
<p><span>Original Amount:</span> <?php echo $bookingDetails[0]["PaymentAmount"]; ?></p>
<p><span>PayBack Discount Amount:</span> <?php echo $_POST["discountAmt"]; ?></p>
<p><span>PayBack Points Redeem:</span> <?php echo $_POST["redeemedPts"]; ?></p>
<?php
} else {
?>
<span>Rs <?php echo intval($bookingDetails[0]["PaymentAmount"]); ?>/-</span></h3>
<?php
}
?>
</div>
</div>
</div>
</div>
</div>
<?php //} ?>
</div>

<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/securebrowser.js"></script>
<script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
<script>document.getElementById('ipaddress').value=myip;</script>
<script>

var ipaddressval=document.getElementById('ipaddress').value;
var browserval=document.getElementById('browser').value;

expires = "Mon, 5 Oct 2015 01:05:11 UTC";
document.cookie = "ipaddress="+ipaddressval+';'+expires+";path=/";
document.cookie = 'browser='+browserval+';'+expires+";path=/";
//var b = moment(a).zone(0).format(YYYY-MM-DD HH:mm:ss);
</script>
<?php



include_once("./includes/footer.php");
unset($bookingDetails);
?>
</body>
</html>

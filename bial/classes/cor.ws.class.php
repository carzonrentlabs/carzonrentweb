<?php
    include_once("./includes/cache-func.php");
    class COR{
        private $soapClient;
        function __construct(){
            $this->soapClient = new SoapClient("http://insta.carzonrent.com/WsIntercityRetail_New/service.asmx?wsdl", array('trace' => 1));
        }
        function _CORGetCities(){
            try{
                //if(!$r = getcache("./xml/cache/origins.xml", INDEXPERIOD)){
                //    $r = $this->soapClient->getCities(array());
                //    $xmlContent = $r->{'getCitiesResult'}->{'any'};
                //    createcache($xmlContent, "./xml/cache/origins.xml");
                //    $r = $xmlContent;
                //}
		if(file_exists("../xml/cache/origins.xml")){
		    $r = file_get_contents("../xml/cache/origins.xml");
		}
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORTestCities(){
	    try{
		$r = $this->soapClient->getCities(array());
		return $r;
	    }
	    catch(Exception $e){
		
	    }
	}
	
        function _CORGetDestinations(){
            try{
                if(!$r = getcache("../xml/cache/destinations.xml", INDEXPERIOD)){
                    $r = $this->soapClient->getDestinations(array());
                    $xmlContent = $r->{'getDestinationsResult'}->{'any'};
                    createcache($xmlContent, "../xml/cache/destinations.xml");
                    $r = $xmlContent;
                }
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORGetPackages($arg){
            try{
                if(!$r = getcache("../xml/cache/packages-" . $arg["cityId"] . ".xml", INDEXPERIOD)){
                    $r = $this->soapClient->GetPackages($arg);
                    $xmlContent = $r->{'GetPackagesResult'}->{'any'};
                    createcache($xmlContent, "../xml/cache/packages-" . $arg["cityId"] . ".xml");
                    $r = $xmlContent;
                }
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        function _CORGetCarModel($arg){
            try{
                $k = array_keys($arg);
                $v = array_values($arg);                
                for($i = 0; $i < count($arg); $i++)
                {
                   $param[$k[$i]] = $v[$i];
                }
                $r = $this->soapClient->getCarModel($param);
                return $r;
            }
            catch(Exception $e){
                //die($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
                print $e->getMessage();
            }
        }

	function _CORGetCarCategories($arg){
            try{
                $k = array_keys($arg);
                $v = array_values($arg);                
                for($i = 0; $i < count($arg); $i++)
                {
                   $param[$k[$i]] = $v[$i];
                }
                $r = $this->soapClient->getCarCategories($param);
                return $r;
            }
            catch(Exception $e){
                //die($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
                print $e->getMessage();
            }
        }
		
	function _CORGetCabs($arg){
            try{
                $r = $this->soapClient->GetCabs($arg);
                $xmlContent = $r->{'GetCabsResult'}->{'any'};
                return $xmlContent;
            }               
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCheckPhone($arg){
            try{
                $r = $this->soapClient->VerifyPhone($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCheckLogin($arg){
            try{
                $r = $this->soapClient->verifyLogin($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORRegister($arg){
            try{
                $r = $this->soapClient->Registration($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORMakeBooking($arg){
            try{
                $r = $this->soapClient->SetTravelDetails($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        function _CORCheckSoldOut($arg){
            try{
                $r = $this->soapClient->CheckSoldOut($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORSetNewPassword($arg){
            try{
                $r = $this->soapClient->SetPassword($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        function _CORForgotPassword($arg){
            try{
                $r = $this->soapClient->ForgetPassword($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        function _CORGetInvoice($arg){
            try{
                $r = $this->soapClient->ValidateInvoice($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
        
        function _CORCancelBooking($arg){
            try{
                $r = $this->soapClient->CancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORGetBookings($arg){
            try{
                $r = $this->soapClient->RetrieveBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORGetBookingsDetail($arg){
            try{
                $r = $this->soapClient->GetBookingDetail($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORUpdatePayment($arg){
            try{
                $r = $this->soapClient->UpdatePayment($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSendMail($arg){
            try{
                $r = $this->soapClient->SenMail($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveUpdatePayment($arg){
            try{
                $r = $this->soapClient->SelfDrive_UpdatePayment($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveCreateBooking($arg){
            try{
                $r = $this->soapClient->SelfDrive_CreateBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveCancelBooking($arg){
            try{
                $r = $this->soapClient->SelfDrive_cancelBooking($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORSelfDriveGetCabs($arg){
            try{
                //$r = $this->soapClient->SelfDrive_GetCab($arg);
		$r = $this->soapClient->SelfDrive_GetCab_Temp($arg);
                $xmlContent = $r->{'SelfDrive_GetCab_TempResult'}->{'any'};
                return $xmlContent;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORVerifyDiscount($arg){
            try{
                $r = $this->soapClient->CheckValidDiscount($arg);
				$disContent = $r->{'CheckValidDiscountResult'}->{'any'};
                return $disContent;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	
	function _CORGetDiscount($arg){
            try{
                $r = $this->soapClient->DiscountCode($arg);
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	function _CORAdditionalServices($arg){
            try{
                $r = $this->soapClient->SelfDrive_AdditionalService ($arg);
                return $r;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
        }
	function _COR_SelfDrive_NextAvailibility($arg){
			try{
                $r = $this->soapClient->SelfDrive_NextAvailibility ($arg);
				$xmlContent = $r->{'SelfDrive_NextAvailibilityResult'}->{'any'};
				return $xmlContent;
            }
            catch(Exception $e){
                print $e->getMessage();
            }
	}
		function encode_base64($sData)
		{
			$sBase64 = base64_encode($sData);
			return strtr($sBase64, '+/', '-_');
		}
		function decode_base64($sData)
		{
			$sBase64 = strtr($sData, '-_', '+/');
			return base64_decode($sBase64);
		}
		function _encrypt($sData, $sKey='cor_encryption_key')
		{
			$sResult = '';
			for($i=0;$i<strlen($sData);$i++)
			{
				$sChar    = substr($sData, $i, 1);
				$sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
				$sChar    = chr(ord($sChar) + ord($sKeyChar));
				$sResult .= $sChar;
			}
			return $this->encode_base64($sResult);
		}

		function _decrypt($sData, $sKey='cor_encryption_key')
		{
			$sResult = '';
			$sData   = $this->decode_base64($sData);
			for($i=0;$i<strlen($sData);$i++){
				$sChar    = substr($sData, $i, 1);
				$sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
				$sChar    = chr(ord($sChar) - ord($sKeyChar));
				$sResult .= $sChar;
			}
			return $sResult;
		}
    };
?>
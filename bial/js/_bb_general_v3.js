String.prototype.trim=function(){var a;a=this.replace(/^\s*(.*)/,"$1");return a=a.replace(/(.*?)\s*$/,"$1")};function isEmailAddr(a,b,c){var d=!1,e=new String(a.value),f=e.indexOf("@");if(0<f){var g=e.indexOf(".",f);g>f+1&&e.length>g+1&&(d=!0)}!1==d&&(""!=c&&null!=c&&document.getElementById(c)?document.getElementById(c).innerHTML=b:alert(b),a.focus());return d} function isNumeric(a,b){var c;sText=a.value;var d=!0;for(i=0;i<sText.length;i++)c=sText.charAt(i),-1=="0123456789.-".indexOf(c)&&(d=!1);!1==d&&""!=b&&(alert(b),a.focus());return d}function isAnyNumeric(a,b,c){var d;sText=a.value;var e=!1;for(i=0;i<sText.length;i++)d=sText.charAt(i),-1<"0123456789.-".indexOf(d)&&(e=!0);return e==b?(""!=c&&(alert(c),a.focus()),!1):!0}function removeAllOptions(a){var b;for(b=a.options.length-1;0<=b;b--)a.remove(b)} function addOption(a,b,c){var d=document.createElement("OPTION");d.text=c;d.value=b;a.options.add(d)}function isFilledText(a,b,c,d){var e;e=a.value;e=e.trim();if(""==e||e==b){if(""!=c){""!=d&&null!=d&&document.getElementById(d)?document.getElementById(d).innerHTML=c:alert(c);try{a.focus()}catch(f){}}return!1}return!0} function isFilledSelect(a,b,c,d,e){if(0==d)return d=a.selectedIndex,d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus(),!1):!0;if(1==d)return d=a.options[a.selectedIndex].text,b=b.trim(),d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus(),!1):!0;if(2==d)return d=a.options[a.selectedIndex].value,b=b.trim(),d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML= c:alert(c),a.focus(),!1):!0}function isCBoxChecked(a,b,c,d,e){var f,g;if(1==d)return!1==a.checked?(""!=c&&(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus()),!1):!0;f=!1;g=0;d=a.length;for(i=0;i<d;i++)!0==a[i].checked&&(g++,g==b&&(f=!0));!1==f&&""!=c&&(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c));return f} function compareString(a,b,c){var d=!1;"Text"==c?(a=a.value,a=a.trim(),b=b.value,b=b.trim(),a==b&&(d=!0)):"Select"==c&&(a=a.options[a.selectedIndex].text,a=a.trim(),b=b.options[b.selectedIndex].text,b=b.trim(),a==b&&(d=!0));return d} function createStrSubmit(a){var b="",c,d;d="";for(i=0;i<a.elements.length;i++){c=a.elements[i];switch(c.type){case "text":case "select-one":case "hidden":case "password":case "textarea":b+=c.name+"="+escape(c.value)+"&";break;case "checkbox":!0==c.checked&&(d!=c.name?b+=c.name+"="+escape(c.value)+"&":("&"==b.substring(b.length-1,b.length)&&(b=b.substring(0,b.length-1)),b+=","+escape(c.value)),d=c.name)}"&"!=b.substring(b.length-1,b.length)&&(b+="&")}"&"==b.substring(b.length-1,b.length)&&(b=b.substring(0, b.length-1));return b}function checkFormAllFields(a){var b=!0,c,d=-1;for(i=0;i<a.elements.length;i++)switch(currElement=a.elements[i],currElement.type){case "text":case "select-one":case "password":case "textarea":case "checkbox":c=currElement.value,c=c.trim(),""==escape(c)&&(!0==b&&(d=i),b=!1)}!1==b&&(alert("No field can be left blank."),currElement=a.elements[d],currElement.focus());return b} function _allowNumeric(a){a=getKeyCode(a);return 48<=a&&57>=a?!0:null==a?!0:8==a||0==a||46==a||9==a?!0:!1}function _allowAlpha(a){a=getKeyCode(a);return 65<=a&&90>=a||97<=a&&122>=a?!0:null==a?!0:8==a||0==a||46==a||32==a||9==a?!0:!1}function getKeyCode(a){return window.event?window.event.keyCode:a?a.which:null}function _setCookie(a,b,c){var d=new Date;d.setDate(d.getDate()+c);document.cookie=a+"="+escape(b)+(null==c?"":";expires="+d.toGMTString())} function _getCookie(a){return 0<document.cookie.length&&(c_start=document.cookie.indexOf(a+"="),-1!=c_start)?(c_start=c_start+a.length+1,c_end=document.cookie.indexOf(";",c_start),-1==c_end&&(c_end=document.cookie.length),unescape(document.cookie.substring(c_start,c_end))):""}function _getDDLValue(a){var b="";document.getElementById(a)&&(b=document.getElementById(a).options[document.getElementById(a).selectedIndex].value);return b} function _getDDLText(a){var b="";document.getElementById(a)&&(b=document.getElementById(a).options[document.getElementById(a).selectedIndex].text);return b}_fillNextDDL=function(a,b){if(b){removeAllOptions(b);for(i=0;i<a.options.length;i++)addOption(b,a.options[i].value,a.options[i].text)}};_disableFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)cElement=document.getElementById(a).elements[i],cElement.disabled=!0}; _enableFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)cElement=document.getElementById(a).elements[i],cElement.disabled=!1}; _clearFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)switch(cElement=document.getElementById(a).elements[i],cElement.type){case "text":case "password":case "hidden":case "textarea":cElement.value="";break;case "select-one":cElement.selectedIndex=0;break;case "checkbox":case "radio":cElement.checked=!1}};_selAllUsers=function(){for(i=0;i<document.getElementsByName("chkUsers[]").length;i++)document.getElementsByName("chkUsers[]")[i].checked=document.getElementById("chkAllUsers").checked};
function _$(id){
    if(typeof(id) == "string"){
        if(document.getElementById(id))
            return document.getElementById(id);
        else{
            if(document.getElementsByName(id))
                return document.getElementsByName(id);
            else {
                if(document.getElementsByTagName(id))
                    return document.getElementsByTagName(id);
                else
                    return null;
            }
        }
    }
    else {
        if(id)
            return id;
        else
            return null;
    }
};
function _disablePage(){
    if(_$('disableLayer').length <= 0){
        var d;
        try{
            d = document.createElement("<div id='disableLayer' style='background-color:#000;position:fixed;opacity:0.2;z-index:99;top:0;left:0;position:fixed;cursor:pointer;'></div>");
        }
        catch(e){
            d = document.createElement("div");
            d.id = "disableLayer";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.backgroundColor = "#000";
            d.style.opacity = 0.2;
            d.style.zIndex = 99;
            d.style.cursor = 'pointer';
        }
        document.body.appendChild(d);
    }
};
function _showPopUp(mode, urlhtml, divid, w, h, isModal, param){
    _disablePage();
    if(!isModal){
        _$('disableLayer').onclick = (function(){
            document.body.removeChild(_$('disableLayer'));
            document.body.removeChild(_$('popupContainer'));
        });
    }
    
    var postop = ((screen.availHeight) - h)/3;;
    var posleft = ((screen.availWidth) - w)/2;
    var d;
    try{
        d = document.createElement("<div id='popupContainer' style='background-color:#fff;opacity:1;position:relative;z-index:100;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupContainer";
        d.style.position = "fixed";
        d.style.backgroundColor = "#fff";
        d.style.top = postop + "px";
        d.style.left = posleft + "px";
    }
    document.body.appendChild(d);
    
    try{
        d = document.createElement("<div id='popupPage' style='background-color:#fff;opacity:1;position:absolute;z-index:100;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupPage";
        d.style.position = "absolute";
        d.style.height = h + "px";
        d.style.width = w + "px";
        d.style.backgroundColor = "#fff";
        d.style.borderRadius = "5px";
        d.style.padding = "10px";
        d.style.boxShadow = "2px 5px 10px #333333";       
        d.style.opacity = 1;
        d.style.zIndex = "100";
    }
    _$('popupContainer').appendChild(d);
    
    if(mode.toLowerCase() == "iframe"){
        try{
            d = document.createElement("<iframe id=\"" + divid + "\" border=\"0\" frameborder=\"0\" framespacing=\"0\" style=\"z-index:100;top:0;left:0;position:absolute;\" src=\"./" + urlhtml + "\" ></iframe>");
        }
        catch(e)
        {
            d = document.createElement("IFRAME"); 
            d.id = divid;
            d.frameBorder = 0;
            d.frameSpacing = 0;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "100";
            d.src = urlhtml;
        }
        _$('popupPage').appendChild(d);
    }
    else if(mode.toLowerCase() == "ajax"){
        try{
            d = document.createElement("<div id=\"" + divid + "\" style=\"z-index:100;top:0;left:0;position:absolute;\" ></div>");
        }
        catch(e)
        {
            d = document.createElement("div"); 
            d.id = divid;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "100";
        }
	
        _$('popupPage').appendChild(d);
	
	_waitGetData = function(){
            _$(divid).innerHTML = "Please wait";
        };
        _responseGetData = function(str){
            _$(divid).innerHTML = str;
            _$(divid).style.top = postop + "px";
            _$(divid).style.left = posleft + "px";
        };
	
        var aj = new _ajax(urlhtml, "GET", param, function(){_waitGetData()}, function(r){_responseGetData(r)});
        aj._query();
               
    }
    else if(mode.toLowerCase() == "html"){
        try{
            d = document.createElement("<div id=\"" + divid + "\" style=\"z-index:100;top:0;left:0;position:absolute;\" ></div>");
        }
        catch(e)
        {
            d = document.createElement("div"); 
            d.id = divid;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "100";
        }
        _$('popupPage').appendChild(d);
        _$(divid).innerHTML = urlhtml;
    }
};
_closePopUp = function(){
    document.body.removeChild(_$('disableLayer'));
    document.body.removeChild(_$('popupContainer'));
};
 function _setDropDatePromo(pi, di, dy){
    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    m = _getMonthInt(pd[1].replace(",",""));
    //alert(eval(parseInt(pd[0])+1));
    p.setFullYear(pd[2], m, eval(parseInt(pd[0])+1));
    var d = new Date();
    m = _getMonthInt(dd[1].replace(",",""));
    d.setFullYear(dd[2], m, dd[0]);
    if(d < p)
	document.getElementById(di).value = document.getElementById(pi).value;
    $("#" + di).datepicker("option", "minDate", p);
    odd = document.getElementById(di).value;
 };
 
 function _setCheckPromo(pi, di, dy){
   pd = document.getElementById(pi).value.trim().split(" ");
   dd = document.getElementById(di).value.trim().split(" ");
   var p = new Date();
   m = _getMonthInt(pd[1].replace(",",""));
   p.setFullYear(pd[2], m, pd[0]);
   var d = new Date();
   m = _getMonthInt(dd[1].replace(",",""));
   d.setFullYear(dd[2], m, dd[0]);
   var df = (d - p) / (60*60*24*1000);
   var cnf = false;
   if(df > 1)
      cnf = confirm("The discount is valid for 2 days trip only.\nDo you still want to change drop date?");
   if(!cnf){
      var ndp = new Date();
      ndp.setFullYear(pd[2], m, eval(parseInt(pd[0])+1));
      $("#" + di).datepicker("option", "minDate", ndp);
      document.getElementById(di).value = odd;
   }
 }
$(document).ready(function() {
	
	//$('select.styled').customSelect();
	//$('select.time').customSelect();
	$('select.area').customSelect();
	$('select.modifymyb').customSelect();		
				
// For Travel multi city popup

	
	$("a.multi").click(function(){
		$(this).next(".modyfybook_wrapper").show();
		$(this).next(".modyfybook_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();
		})
	})
		$(".closedpop").click(function(){
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();			
		})		
		
	
// For fae details tooltips

	var row;
	_pass = function(id)
	{
		//alert(id);
		var parts = id.split("_");
		row = parts[1];
	}
	$(".faredetails").mouseover(function(){
		var tp = row * 30;
		if(tp == 0)
			tp = 30;
		else
			tp = 30 + parseInt(row) * 155;
		document.getElementById('popupbox' + row).style.top = tp + "px"; 
		$(this).next(".details_wrapper").show();
		$(this).next(".details_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".details_wrapper").hide();
			$(".overlay").remove();
		})
	})
	
	$(".closedpop").click(function(){
		$(".details_wrapper").hide();
		$(".tcpopup").hide();
		$(".overlay").remove();			
	})
		
	
	$(".atcpopup").click(function(){
		$(this).next(".tcpopup").show();
		$(this).next(".tcpopup").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".tcpopup").hide();
			$(".overlay").remove();
		})
	})	
	
	
// For map tooltips
	$(".map").click(function(){
		$(this).next(".map_wrapper").show();
		$(this).next(".map_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".map_wrapper").hide();
			$(".overlay").remove();
		})
	})
		$(".closedpop").click(function(){
			$(".map_wrapper").hide();
			$(".overlay").remove();			
		})
		
		
// For modify my Booking

	$(".modify a").click(function(){
		$(this).next(".modyfybook_wrapper").show();
		$(this).next(".modyfybook_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();
		})
	})
		$(".closedpop").click(function(){
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();			
		})		
	
// For Feedback popup
	$(".feedback_panel").delay(5000).show(500);
	$(".feedback_panel .heading a").click(function(){
		$(".feed_wrapper").hide(500);
		$(".feed_minimize").delay(200).show(500);
	});
	$(".feed_minimize").click(function(){
		$(".feed_minimize").hide(500);
		$(".feed_wrapper").delay(200).show(500);

	});

// For Book Now popup
	$(".booknow").click(function(){
		document.getElementById('frmID').value = $(this).attr('id');
		var postop = (($(window).height()) - 150)/2;
		var posleft = (($(window).width()) - 150)/2;
		$(".overlay").show();
		$(".confirmbookingpanel").show();
		$(".confirmbookingpanel").css({'top': postop});
		$(".confirmbookingpanel").css({'left': posleft});
		$(".overlay").click(function(){
			$(".confirmbookingpanel").hide();
			$(".overlay").fadeOut(500);		
		})
	})
	
	$(".closedcp").click(function(){
		$(".confirmbookingpanel").hide();
		$(".overlay").fadeOut(500);		
	})

});

function _unlockPage()
{
	document.getElementById('overlay123').style.display = 'none';
}
function _lockPage()
{
	document.getElementById('overlay123').style.display = 'block';
}
//var webRoot = "http://localhost:8080/cor-live/bial";
//var webRoot = "http://www.carzonrent.com/beta";
var webRoot = "http://www.carzonrent.com";
_getMonthInt = function(ms){
    var m = 0;
    switch(ms.toLowerCase()){
        case "jan":
            m = 0;
        break;
        case "feb":
            m = 1;
        break;
        case "mar":
            m = 2;
        break;
        case "apr":
            m = 3;
        break;
        case "may":
            m = 4;
        break;
        case "jun":
            m = 5;
        break;
        case "jul":
            m = 6;
        break;
        case "aug":
            m = 7;
        break;
        case "sep":
            m = 8;
        break;
        case "oct":
            m = 9;
        break;
        case "nov":
            m = 10;
        break;
        case "dec":
            m = 11;
        break;
    }
    return m;
};
_validate = function()
{
	var pd;
	var dd;
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("hdOriginName"), "", "Origin can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("hdDestinationName"), "", "Destination can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputField"), "", "Pickup date can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputField2"), "", "Drop date can't be left blank.");
	if(chk){
		pd = document.getElementById("inputField").value.trim().split(" ");
		if(pd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		dd = document.getElementById("inputField2").value.trim().split(" ");
		if(dd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		td = document.getElementById("hdToday").value.trim().split(" ");
		var t = new Date();
                var m = _getMonthInt(td[1].replace(",",""));
		t.setFullYear(td[2], m, td[0]);
		var p = new Date();
                m = _getMonthInt(pd[1].replace(",",""));
		p.setFullYear(pd[2], m, pd[0]);
		var d = new Date();
                m = _getMonthInt(dd[1].replace(",",""));
		d.setFullYear(dd[2], m, dd[0]);
		if(t > p)
		{
			alert("Please enter an appropriate pickup date");
			chk = false;
		}
		if(chk){
			if(p > d)
			{
				alert("Please enter an appropriate drop date");
				chk = false;
			}
		}
	}
	
	if(chk == true){
	    document.getElementById('form1').action = webRoot + "/outstation/search/" + document.getElementById("hdOriginName").value.replace(/ /g, "-").toLowerCase() + "-to-" + document.getElementById("hdDestinationName").value.replace(/,/g, "-to-").replace(/ /g, "-").toLowerCase() + "/";
	    document.getElementById('form1').submit();
	} else {
	    return chk;
	}
	//{
	//	document.getElementById('hdOriginName').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].text;
	//	document.getElementById('hdOriginID').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].value;
	//}
	//return chk;
}

_validateSF = function()
{
	var pd;
	var dd;
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("hdOriginNameSF"), "", "Origin can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputFieldSF1"), "", "Pickup date can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputFieldSF2"), "", "Drop date can't be left blank.");
	if(chk){
		pd = document.getElementById("inputFieldSF1").value.trim().split(" ");
		if(pd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		dd = document.getElementById("inputFieldSF2").value.trim().split(" ");
		if(dd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		td = document.getElementById("hdToday").value.trim().split(" ");
		var t = new Date();
                var m = _getMonthInt(td[1].replace(",",""));
		t.setFullYear(td[2], m, td[0]);
		var p = new Date();
                m = _getMonthInt(pd[1].replace(",",""));
		p.setFullYear(pd[2], m, pd[0]);
		p.setHours(document.getElementById('tHourP').options[document.getElementById('tHourP').selectedIndex].value);
		p.setMinutes(document.getElementById('tMinP').options[document.getElementById('tMinP').selectedIndex].value);
		var d = new Date();
                m = _getMonthInt(dd[1].replace(",",""));
		d.setFullYear(dd[2], m, dd[0]);
		d.setHours(document.getElementById('tHourD').options[document.getElementById('tHourD').selectedIndex].value);
		d.setMinutes(document.getElementById('tMinD').options[document.getElementById('tMinD').selectedIndex].value);
		if(t >= p)
		{
			alert("Please enter an appropriate pickup date & time");
			chk = false;
		}
		if(chk){
			if(p >= d)
			{
				alert("Please enter an appropriate drop date & time");
				chk = false;
			}
		}
		//alert(Math.ceil(eval(d - p) / (60 * 60 * 24 * 1000)));
	}
	if(chk == true)
	{
		//document.getElementById('hdOriginNameSF').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].text;
		//document.getElementById('hdOriginIDSF').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].value;
		document.getElementById('hdDestinationNameSF').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].text;
		document.getElementById('hdDestinationIDSF').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].value;
	}
	if(chk == true){
	    document.getElementById('form3').action = webRoot + "/self-drive/search/" + document.getElementById("hdOriginNameSF").value.replace(/ /g, "-").toLowerCase() + "/";
	    document.getElementById('form3').submit();
	} else {
	    return chk;
	}
	//{
	//	document.getElementById('hdOriginName').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].text;
	//	document.getElementById('hdOriginID').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].value;
	//}
	//return chk;
}
_validateLocal = function()
{
	var chk;
	chk = true;
	//chk = isFilledSelect(document.getElementById("ddlOriginL"), 0, "Origin can't be left blank.", 0, "0");
	//if(chk == true)
		chk = isFilledText(document.getElementById("inputField5"), "", "Pickup date can't be left blank.");
	if(chk){
		pd = document.getElementById("inputField5").value.trim().split(" ");
		if(pd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		td = document.getElementById("hdToday").value.trim().split(" ");
		var t = new Date();
                var m = _getMonthInt(td[1].replace(",",""));
		t.setFullYear(td[2], m, td[0]);
		var p = new Date();
                m = _getMonthInt(pd[1].replace(",",""));
		p.setFullYear(pd[2], m, pd[0]);
		if(t > p)
		{
			alert("Please enter an appropriate pickup date");
			chk = false;
		}
	}
	if(chk == true)
	{
		//document.getElementById('hdOriginNameL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].text;
		//document.getElementById('hdOriginIDL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].value;
		//document.getElementById('hdDestinationNameL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].text;
		//document.getElementById('hdDestinationIDL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].value;
	}
	if(chk == true){
	    document.getElementById('form2').action = webRoot + "/local/search/" + document.getElementById("hdOriginNameL").value.replace(" ", "-").toLowerCase() + "/";
	    document.getElementById('form2').submit();
	} else {
	    return chk;
	}
	//return chk;
}
_checkLogin = function(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("monumber"), "10 digit mobile number", "Mobile number can't be left blank.");
	if(chk)
		chk = isFilledText(document.getElementById("monumber"), "", "Mobile number can't be left blank.");
	if(chk)
	{
		if(document.getElementById("monumber").value.length < 10)
		{
			alert("Please enter a valid 10 digit mobile number.");
			chk = false;
		}
	}
	if(chk){
		if(document.getElementById('isExistUser').checked){
			chk = isFilledText(document.getElementById("corpassword"), "", "Password can't be left blank.");
		}
	}
	if(chk)
	document.getElementById('formLogin').submit();
};
_setactive = function()
{
	document.getElementById('country2').style.display = 'block';
	document.getElementById('country1').style.display = 'none';
	document.getElementById('country3').style.display = 'none';
}
function _bookMCity(frm){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("hdOriginName"), "", "Origin can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("hdDestinationName"), "", "Destination can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputField3"), "", "Pickup date can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputField4"), "", "Drop date can't be left blank.");
	if(chk){
		pd = document.getElementById("inputField3").value.trim().split(" ");
		if(pd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		dd = document.getElementById("inputField4").value.trim().split(" ");
		if(dd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		td = document.getElementById("hdToday").value.trim().split(" ");
		var t = new Date();
                var m = _getMonthInt(td[1].replace(",",""));
		t.setFullYear(td[2], m, td[0]);
		var p = new Date();
                m = _getMonthInt(pd[1].replace(",",""));
		p.setFullYear(pd[2], m, pd[0]);
		var d = new Date();
                m = _getMonthInt(dd[1].replace(",",""));
		d.setFullYear(dd[2], m, dd[0]);
		if(t > p)
		{
			alert("Please enter an appropriate pickup date");
			chk = false;
		}
		if(chk){
			if(p > d)
			{
				alert("Please enter an appropriate drop date");
				chk = false;
			}
		}
	}
	if(chk){
		if(document.getElementById('inputField'))
		document.getElementById('inputField').value = document.getElementById('inputField3').value;
		if(document.getElementById('inputField2'))
		document.getElementById('inputField2').value = document.getElementById('inputField4').value;
		document.getElementById(frm).action = webRoot + "/outstation/search/" + document.getElementById("hdOriginName").value.replace(/ /g, "-").toLowerCase() + "-to-" + document.getElementById("hdDestinationName").value.replace(/,/g, "-to-").replace(/ /g, "-").toLowerCase() + "/";
		document.getElementById(frm).submit();
	}
}

function _setSubmitBookin(){
	
	document.getElementById('formPaymentOptions').submit();	
}
function _checkRegister(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("txtName"), "", "Please fill in your full name.");
	if(chk)
		chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
	if(chk)
		chk = isFilledText(document.getElementById("txtEmail"), "", "Please fill in your email id.");
	if(chk)
		chk = isEmailAddr(document.getElementById("txtEmail"), "Please fill in a valid email id.");
	if(chk)
		document.getElementById('formRegister').submit();
};

function _checkNewPassword(){
	var chk;
	chk = true;
	if(chk)
		chk = isFilledText(document.getElementById("tokenID"), "", "Invalid token ID.");
	if(chk)
		chk = isFilledText(document.getElementById("txtPassword"), "", "Please fill in your password.");
	if(chk){
		if(document.getElementById("txtPassword").value.trim() != document.getElementById("txtCPassword").value.trim()){
			alert("Your passwords are not matching.");
			chk = false;
		}
	}
	if(chk)
		document.getElementById('frmNewPassword').submit();
}
function _doLogin(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
	if(chk)
		chk = isFilledText(document.getElementById("txtPassword"), "", "Please fill in your password.");
	if(chk)
		document.getElementById('frmLogin').submit();
};

function _setTime(hc, mc, da, ta, hf){
	if(document.getElementById(hc).selectedIndex > 0){
		//if(document.getElementById(mc).selectedIndex <= 0)
		//	document.getElementById(mc).selectedIndex = 1;
		var h = document.getElementById(hc).options[document.getElementById(hc).selectedIndex].value;
		var m = document.getElementById(mc).options[document.getElementById(mc).selectedIndex].value;
		document.getElementById(da).style.display = "none";
		var ampm = "AM";
		if(parseInt(h) >= 12){
			ampm = "PM";
			if(parseInt(h) > 12)
				h = parseInt(h) - 12;
		}
                if(document.getElementById(ta))
                    document.getElementById(ta).innerHTML = "You Selected: " + h + ":" + m + " " + ampm;
                if(document.getElementById(hf))
                    document.getElementById(hf).value = h + ":" + m + " " + ampm;
	}
	else{
		document.getElementById(mc).selectedIndex = 0;
                if(document.getElementById(ta))
                    document.getElementById(ta).innerHTML = "";
		document.getElementById(da).style.display = "none";
	}
};


function _makeBooking(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("userTime"), "", "Please choose pick time.");
	if(chk){
		var bookDays = document.getElementById('bookDays').value;
		if(bookDays <= 0){
			var diff = new Date('01/01/2012 ' + document.getElementById("userTime").value) - new Date( '01/01/2012 ' + document.getElementById("bookTime").value);
			diff = (diff / (1000 * 60 * 60));
			if(diff < 3)
			{
				alert('You can book at least 3 hours before pickup time.');
				chk = false;	
			}
		}
	}
	if(chk)
	    chk = isFilledText(document.getElementById("address"), "", "Address can't be left blank.");
	if(chk)
	    chk = isFilledText(document.getElementById("monumberX"), "", "Mobile can't be left blank.");
	if(chk)
	    chk = isFilledText(document.getElementById("monumberX"), "10 digit mobile number", "Mobile can't be left blank.");
	var mNo = document.getElementById("monumberX").value;
	//if(chk){
	//    if(mNo.substr(0,1) != "9" && mNo.substr(0,1) != "8" && mNo.substr(0,1) != "7"){
	//	alert("Invalid mobile number. Please enter correct mobile number.");
	//	chk = false;
	//    }
	//}
	if(chk)
	    chk = isFilledText(document.getElementById("txtname"), "Name", "Name can't be left blank.");
	if(chk)
	    chk = isFilledText(document.getElementById("txtname"), "", "Name can't be left blank.");
	if(chk)
	    chk = isFilledText(document.getElementById("email"), "Email", "Email can't be left blank.");
	if(chk)
	    chk = isEmailAddr(document.getElementById("email"), "Please fill in a valid email id.");
	if(chk){
		var adr = document.getElementById('address').value;
		adr = adr.replace('\n','<br />');
		adr = adr.replace('\r','<br />');
		adr = adr.replace('\n\r','<br />');
		document.getElementById('address').value = adr;
		document.getElementById('formTravelDetail').submit();
	}
};

function _paymentCheck(r){
	if(r == 'rdo1'){
		if(document.getElementById('rdo2'))
		document.getElementById('rdo2').checked  = false;
		if(document.getElementById('rdo3'))
		document.getElementById('rdo3').checked  = false;
	}
	if(r == 'rdo2'){
		if(document.getElementById('rdo1'))
		document.getElementById('rdo1').checked  = false;
		if(document.getElementById('rdo3'))
		document.getElementById('rdo3').checked  = false;
	}
	if(r == 'rdo3'){
		if(document.getElementById('rdo1'))
		document.getElementById('rdo1').checked  = false;
		if(document.getElementById('rdo2'))
		document.getElementById('rdo2').checked  = false;
	}
	_showGPDetails();
}
function _checkPaymentSubmit(frm){
	var chk;
	chk = true;
	if(chk){
	    _disablePage();
	    _showPopUp('html', '<br /><div style="text-align:center;"><img src="./images/loader.gif" border="0" /><br /><br />Your booking is being processed.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
	    frm.submit();
	}
};

function _checkForgotPassword(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
	if(chk)
		document.getElementById('frmForgotPassword').submit();
}

function _sendBusinessEnquiries(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("txtName"), "", "Please fill in your full name.");
	if(chk)
		chk = isFilledText(document.getElementById("txtCompany"), "", "Please fill in your company name.");
	if(chk)
		chk = isFilledText(document.getElementById("txtContactNum"), "", "Please fill in your contact number.");
	if(chk)
		chk = isFilledText(document.getElementById("txtDesignation"), "", "Please fill in your designation.");
	//if(chk)
	//	chk = isFilledText(document.getElementById("txtOrganization"), "", "Please fill in your organization name.");
	if(chk)
		chk = isFilledText(document.getElementById("txtEmailID"), "", "Please fill in your email id.");
	if(chk)
		chk = isEmailAddr(document.getElementById("txtEmailID"), "Please fill a valid email id.");
	if(chk)
		chk = isFilledText(document.getElementById("txtRequirement"), "", "Please write your requirement.");
	if(chk)
		document.getElementById('frmBusinessEnquiries').submit();
};

function _checkInvoice(){
	var chk;
	chk = true;
	chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
	if(chk)
		chk = isFilledText(document.getElementById("txtBookingID"), "", "Please fill in your COR booking ID.");
	if(chk){
		var aj;
		if (window.XMLHttpRequest)
		    aj = new XMLHttpRequest;
		else if (window.ActiveXObject)
		    aj = new ActiveXObject("Microsoft.XMLHTTP");
		var param = "txtBookingID=" + document.getElementById("txtBookingID").value + "&txtMobile=" + document.getElementById("txtMobile").value;
		
		aj.open("GET", "./get-invoice.php?" + param, true);
		aj.send(null);
		aj.onreadystatechange = function() {
			if (aj.readyState == 1) {
				document.getElementById('msg').innerHTML = "Please wait while we check for mobile number and COR ID.<br /><br />";
			}
			else if (aj.readyState == 4) {
			    var r = "";
			    if(aj.status == 200){
				r = aj.responseText;
				if(r != ""){
					//alert(r);
					document.getElementById('msg').innerHTML = "<a href='" + r + "' target='_blank'>Click here</a> to get invoice print out.<br /><br />";
					window.open(r, "invoice", "width=600,height=600");
				}
				else {
					document.getElementById('msg').innerHTML = "No invoice found. Please check your mobile number and COR ID and try again.<br /><br />";					
				}
				aj = null;
			    }
			}
		}		
	}
}

function _cancelBooking(b, u){
	if(b.trim() != "" && u.trim() != ""){
		var cnf;
		cnf = confirm("Are you sure that you want to cancel this booking?");
		if(cnf){
			_disablePage();
			_showPopUp('html', '<br /><div style="text-align:center;"><img src="./images/loader.gif" border="0" /><br /><br />Your booking is being cancelled.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
			var aj;
			if (window.XMLHttpRequest)
			    aj = new XMLHttpRequest;
			else if (window.ActiveXObject)
			    aj = new ActiveXObject("Microsoft.XMLHTTP");
			var param = "txtBookingID=" + b + "&CCID=" + u;
			aj.open("GET", "./cancel-booking.php?" + param, true);
			aj.send(null);
			aj.onreadystatechange = function() {
				if (aj.readyState == 1) {
					document.getElementById('msg').innerHTML = "Please wait while we cancel your booking.<br /><br />";
				}
				else if (aj.readyState == 4) {
				    var r = "";
				    if(aj.status == 200){
					r = aj.responseText;
					window.location = "./cancel-confirm.php";
					//if(parseInt(r) == 1){
					//	window.location = "./cancel-confirm.php";
					//}
					//else {
					//	//window.location = "./cancel-error.php";	
					//}
					aj = null;
				    }
				}
			}
			
		}
	}
	else
		alert("Please check your Booking ID or login status.");
};
var ca = -1; 
function _getCities(startChar, e, d, hdi, hdn, a, arr, x, y, di, w, nd){	
	var kc = getKeyCode(e);
	//alert(kc);
	//38 - Up Key
	//40 - Down Key
	//13 - Enter Key
	var ar = arr;
	if(startChar != "" && ((kc >= 65 && kc <= 90)  || (kc >= 97 && kc <= 122) || (kc == 8 || kc == 46 || kc == 32 || kc == 9))){
		document.getElementById(a).style.display = "none";
		document.getElementById(a).innerHTML = "";
		var isF = 0;
		ca = -1;
		for(i = 0; i < ar.length; i++){
			if(ar[i][0].toLowerCase().indexOf(startChar.toLowerCase()) > -1)
			{
			    cname = ar[i][0].split(",")[0].replace('*', '');
			    if(nd != undefined)
			    document.getElementById(a).innerHTML += "<a href='javascript: void(0);' id='" + i +"' onclick=\"javascript: _setCity(" + ar[i][1] + ",'" + cname + "','" + hdi + "','" + hdn + "','" + a + "','" + d + "'," + di + ",'" + nd + "');\" name='ac'>" + ar[i][0].replace('*', '') + "</a>";
			    else
			    document.getElementById(a).innerHTML += "<a href='javascript: void(0);' onclick=\"javascript: _setCity(" + ar[i][1] + ",'" + cname + "','" + hdi + "','" + hdn + "','" + a + "','" + d + "'," + di + ");\" id='" + i +"' name='ac'>" + ar[i][0].replace('*', '') + "</a>";
			    isF++;
			    if(isF >= 3)
			    break;
			}
		}
	
		if(isF >= 1){
			document.getElementById(a).style.display = "block";
			window.onclick = function(){document.getElementById(a).style.display = "none";}
			document.onclick = function(){document.getElementById(a).style.display = "none";}
		}
		
	}
	else if(startChar != "" && kc == 40){
	    _setBGWhite();
	    ca++;
	    if(document.getElementsByName('ac')[ca])
	    document.getElementsByName('ac')[ca].style.backgroundColor = "#f2f2f2";
	    if(ca >= document.getElementsByName('ac').length)
		ca = -1;
	}
	else if(startChar != "" && kc == 38){
	    _setBGWhite();
	    ca--;
	    if(document.getElementsByName('ac')[ca])
	    document.getElementsByName('ac')[ca].style.backgroundColor = "#f2f2f2";
	    if(ca < 0)
		ca = -1;
	}
	else if(startChar != "" && kc == 13){	
	    if(document.getElementsByName('ac')[ca]){
		var cid = document.getElementsByName('ac')[ca].id;
		cname = ar[cid][0].split(",")[0].replace('*', '');
		_setCity(ar[cid][1], cname, hdi, hdn, a, d, di, nd);
		ca = -1;
	    }
	}
};
function _setBGWhite(){
    var d = document.getElementsByName('ac');
    for(i = 0; i < d.length; i++){
	if(document.getElementsByName('ac')[i])
	document.getElementsByName('ac')[i].style.backgroundColor = "#fff";
    }
};

var arrCityID = new Array();
var arrCityName = new Array();
function _setCity(cid, cname, hdi, hdn, a, d, i, nd){
	arrCityID[i] = cid;
	arrCityName[i] = cname;
	if(document.getElementById(hdi))
		document.getElementById(hdi).value = arrCityID.join(",");
	if(document.getElementById(hdn))
		document.getElementById(hdn).value = arrCityName.join(",");
	if(document.getElementById(d))
		document.getElementById(d).value = cname;
	if(nd != undefined){
		document.getElementById(nd).value = cname;
	}
	document.getElementById(a).style.display = "none";
};

function _setOrigin(i, t){
	var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
	var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
	if(document.getElementById('hdOriginID'))
	document.getElementById('hdOriginID').value = cid;
	if(document.getElementById('hdOriginName'))
	document.getElementById('hdOriginName').value = cname;
	if(document.getElementById('hdOriginIDL'))
	document.getElementById('hdOriginIDL').value = cid;
	if(document.getElementById('hdOriginNameL'))
	document.getElementById('hdOriginNameL').value = cname;
	if(document.getElementById('hdOriginIDSF'))
	document.getElementById('hdOriginIDSF').value = cid;
	if(document.getElementById('hdOriginNameSF'))
	document.getElementById('hdOriginNameSF').value = cname;
	//window.location = "./?c=" + cname + "&ci=" + cid;
//	if(cname.trim() != "Select Origin"){
//                if(t != undefined)
//                    window.location = webRoot + "/car-rental-" + cname + "-" + cid + "/#" + t;
//                else
//                    window.location = webRoot + "/car-rental-" + cname + "-" + cid + "/";
//	}
};

function _setOriginCP(i){
	var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
	var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
	document.getElementById('hdOriginID').value = cid;
	document.getElementById('hdOriginName').value = cname;
	window.location = "./packages.php?c=" + cname + "&ci=" + cid;
};

var mc = 2;
function _addMore(){
	if(mc < 5){
		document.getElementById('mc-tab').rows[mc].style.display = "block";
		document.getElementById('mc-origin' + (mc + 1)).value = document.getElementById('mc-destination' + mc).value;
		mc++;
	}
	if(mc >= 5)
		document.getElementById('addmore').style.display = "none";
};

function _setHour(h){
    document.getElementById('selCabHour').innerHTML = (h * 10) + " kms included";
    document.getElementById('selCH').style.display = "block";
};

_sendimageentry = function(){
	 var chk;
	chk = true;
	if(chk)	
	{
		if(document.getElementById("txtsector").value == "Choose sector")
		{
			alert("Please enter your sector.");
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtsector"), "", "Please choose your sector.");
	if(chk)	
	{
		if(document.getElementById("txtcompany").value == "Enter company name")
		{
			alert("Please enter your company name.");
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtcompany"), "", "Please enter your company name.");
	if(chk)	
	{
		if(document.getElementById("txtstrength").value == "Enter employee strength")
		{
			alert("Please enter your employee strength.");
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtstrength"), "", "Please fill in your mobile number.");
	if(chk)
		_sendtomail();
	return chk;
 }
 var aj;
 _sendleasingenquiry = function(){	
	var chk;
	chk = true;
	if(chk)	
	{
		if(document.getElementById("txtorg").value == "Organization")
		{
			document.getElementById("msgsendmail").innerHTML = "Please enter your organization name.";
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtorg"), "", "Please enter your organization name.", "msgsendmail");	
	if(chk)	
	{
		if(document.getElementById("txtname").value == "Name")
		{
			document.getElementById("msgsendmail").innerHTML = "Please fill in your name.";
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtname"), "", "Please fill in your name.", "msgsendmail");
	if(chk)	
	{
		if(document.getElementById("txtemailid").value == "Email")
		{
			document.getElementById("msgsendmail").innerHTML = "Please fill in your emailid.";
			chk = false;
		}
	}
	if(chk)
		chk = isEmailAddr(document.getElementById("txtemailid"), "Please fill a valid email id.", "msgsendmail");
	if(chk)	
	{
		if(document.getElementById("txtmobumber").value == "Mobile")
		{
			document.getElementById("msgsendmail").innerHTML = "Please fill in your mobile number.";
			chk = false;
		}
	}
	if(chk)	
	{
		var mNo = document.getElementById("txtmobumber").value; 
		if(mNo.length < 10)
		{
			document.getElementById("msgsendmail").innerHTML = "Mobile number must be of 10 digit.";
			chk = false;
		}
	}
	if(chk)
		chk = isFilledSelect(document.getElementById("ddlemployees"), 0, "Plesae select employee strength", 0, "msgsendmail");
	if(chk)	
	{
		if(document.getElementById("txtlocation").value == "City")
		{
			document.getElementById("msgsendmail").innerHTML = "Please enter your city.";
			chk = false;
		}
	}
	if(chk)	
		chk = isFilledText(document.getElementById("txtlocation"), "", "Please enter your city.", "msgsendmail");
	if(chk)
		_sendleasingmail();
	return chk;
 }
  var aj;
_sendleasingmail = function(){
	var o = document.getElementById("txtorg").value;
	var n = document.getElementById("txtname").value;
	var e = document.getElementById("txtemailid").value;
	var m = document.getElementById("txtmobumber").value;
	var em = document.getElementById("ddlemployees").value;
	var l = document.getElementById("txtlocation").value;
	if(window.XMLHttpRequest)
		aj = new XMLHttpRequest;
	else if(window.ActiveXObject)
		aj = new ActiveXObject("Microsoft.XMLHTTP");
	aj.open("POST","sendmail.php", true);
	aj.onreadystatechange = _httpsendleasingmail;
	aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	var values = "o="+ o + "&n="+ n + "&e="+ e + "&m="+ m + "&em="+ em + "&l="+ l;
	aj.send(values);
};
 _httpsendleasingmail = function(){
    if(aj.readyState == 4)
    { 			
	var disp = aj.responseText;			
	if(disp != "" && disp != undefined)
	{
	    if(disp == 1){
		document.getElementById('msgsendmail').innerHTML = "Thank you. Your enquiry has been<br />sent to our marketing representative.";
		document.getElementById("txtorg").value = "";
		document.getElementById("txtname").value = "";
		document.getElementById("txtemailid").value = "";
		document.getElementById("txtmobumber").value = "";
		document.getElementById("ddlemployees").selectedIndex = 0;
		document.getElementById("txtlocation").value = "";
	    }
	    else{
		document.getElementById('msgsendmail').innerHTML = "Your enquiry could not saved.";		
	    }
	}
    }
    else
	document.getElementById('msgsendmail').innerHTML = "Please wait...";		
 };
 
 function _setDropDate(pi, di){
    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    m = _getMonthInt(pd[1].replace(",",""));
    p.setFullYear(pd[2], m, pd[0]);
    var d = new Date();
    m = _getMonthInt(dd[1].replace(",",""));
    d.setFullYear(dd[2], m, dd[0]);
    if(d < p)
	document.getElementById(di).value = document.getElementById(pi).value;
    $("#" + di).datepicker("option", "minDate", p);
 };
 
function _getDiscount(){
    var dAJ;
    var e = "";
    if(document.getElementById("dpcEmailID"))
	e = document.getElementById("dpcEmailID").value;
    if(e == "" || e == undefined){
	alert("Please fill in your E-mail ID to get the discount coupon.");
	return;
    }
    else {
	if(!isEmailAddr(document.getElementById("dpcEmailID"), "Please fill in a valid email id.")){
	    alert("Please fill in a valid E-mail ID.");
	    return;
	}
    }
    var c = "";
    if(document.getElementById("hdOriginID0"))
	c = document.getElementById("hdOriginID0").value;
    if(window.XMLHttpRequest)
	    dAJ = new XMLHttpRequest;
    else if(window.ActiveXObject)
	    dAJ = new ActiveXObject("Microsoft.XMLHTTP");
    dAJ.open("POST", webRoot + "/getdiscount.php", true);
    dAJ.onreadystatechange = function(){
	if(dAJ.readyState == 4){ 			
	    var disp = dAJ.responseText;			
	    if(disp != "" && disp != undefined){
		if(parseInt(disp) == 1)
		    document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Discount offer code has been sent to your Email ID. Please check your spam if you don't receive a mail within few minutes.<br /><br />We only offer a unique discount code per email ID. Book now as the code is valid only for today! <br /><br /><span id='clsid' style='cursor:pointer;text-decoration:underline;width:100% !important;text-align:right;'>Close</span></div>";
		else if(parseInt(disp) == 2)
		    document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Sorry. We only offer a unique discount code per email ID. A discount code has already been sent to you earlier.<br /><br /><span id='clsid' style='cursor:pointer;text-decoration:underline;width:100% !important;text-align:right;'>Close</span></div>";
		jQuery("#clsid").click(function() { //we usually include a "Close" link within the sliding box so user could close this box if he/she wanted to do so.
		jQuery("#popupslider").animate({opacity: "0", bottom: "-50"}, 1000).show(); //when user clicks "Close" it reverse the sliding animation to make the box disappear sliding from right to left.
		});
		dAJ = null;
	    }
	}
	else
	    document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Please wait...</div>";
    };
    dAJ.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var values = "eml="+ e + "&ct="+ c;
    dAJ.send(values);
}
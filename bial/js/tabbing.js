_setActive = function(tab)
{
	
                   if(document.getElementById('corp'))
		   document.getElementById('corp').className = 'deactive';
                   if(document.getElementById('miss'))
		   document.getElementById('miss').className = 'deactive';
                   if(document.getElementById('mana'))
		   document.getElementById('mana').className = 'deactive';
                   if(document.getElementById('event'))
		   document.getElementById('event').className = 'deactive';
                   if(document.getElementById('divcorp'))
                   document.getElementById('divcorp').style.display = 'none';
                   if(document.getElementById('divmiss'))
                   document.getElementById('divmiss').style.display = 'none';
                   if(document.getElementById('divmana'))
                   document.getElementById('divmana').style.display = 'none';
                   if(document.getElementById('divevent'))
                   document.getElementById('divevent').style.display = 'none';
		   var tabName = tab;
                   
                   if(document.getElementById(tab))
                   document.getElementById(tab).className = 'active';
                   if(document.getElementById('div' + tab))
                   document.getElementById('div' + tab).style.display = 'block';
                           
		   if(tabName == 'corp')
			   document.getElementById('arrowmoment').className = "";
		   else if(tabName == 'miss')
			   document.getElementById('arrowmoment').className = "arrowmomentprint";
		   else if(tabName == 'mana')
			   document.getElementById('arrowmoment').className = "arrowmomentvideo";
		   else if(tabName == 'event')
			   document.getElementById('arrowmoment').className = "arrowmomentevent";
}

_setActiveservice = function(tab)
{
	
		   document.getElementById('airp').className = '';
		   document.getElementById('chau').className = '';
		   document.getElementById('self').className = '';
		   document.getElementById('limo').className = '';
		   document.getElementById('oper').className = '';
		   document.getElementById('driv').className = '';
		   document.getElementById('corp').className = '';
		   var tabName = tab;
		   if(tabName == 'airp')
		   {
			   document.getElementById('airp').className = 'active';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'block';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "";
		   }
		   else if(tabName == 'chau')
		   {
				
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'active';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'block';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentchau";
		   }
		   else if(tabName == 'self')
		   {
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'active';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'block';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentself";
		   }
		   else if(tabName == 'limo')
		   {
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'active';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'block';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentlimo";
		   }
		   else if(tabName == 'oper')
		   {
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'active';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'block';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentoper";
		   }
		   else if(tabName == 'driv')
		   {
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'active';
			   document.getElementById('corp').className = 'deactive';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'block';
			   document.getElementById('divcorp').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentdriv";
		   }
		   else if(tabName == 'corp')
		   {
			   document.getElementById('airp').className = 'deactive';
			   document.getElementById('chau').className = 'deactive';
			   document.getElementById('self').className = 'deactive';
			   document.getElementById('limo').className = 'deactive';
			   document.getElementById('oper').className = 'deactive';
			   document.getElementById('driv').className = 'deactive';
			   document.getElementById('corp').className = 'active';
			   document.getElementById('divairp').style.display = 'none';
			   document.getElementById('divchau').style.display = 'none';
			   document.getElementById('divself').style.display = 'none';
			   document.getElementById('divlimo').style.display = 'none';
			   document.getElementById('divoper').style.display = 'none';
			   document.getElementById('divdriv').style.display = 'none';
			   document.getElementById('divcorp').style.display = 'block';
			   document.getElementById('arrowmoment').className = "arrowmomentcorp";
		   }
}

_setActivecareers = function(tab)
{
                   if(document.getElementById('opera'))
                                      document.getElementById('opera').className = '';
		   document.getElementById('fleet').className = '';
		   document.getElementById('qual').className = '';
		   document.getElementById('sales').className = '';
		   document.getElementById('finan').className = '';
		   var tabName = tab;
		   if(tabName == 'opera')
		   {
			   document.getElementById('opera').className = 'active';
			   document.getElementById('fleet').className = 'deactive';
			   document.getElementById('qual').className = 'deactive';
			   document.getElementById('sales').className = 'deactive';
			   document.getElementById('finan').className = 'deactive';
			   document.getElementById('divopera').style.display = 'block';
			   document.getElementById('divfleet').style.display = 'none';
			   document.getElementById('divqual').style.display = 'none';
			   document.getElementById('divsales').style.display = 'none';
			   document.getElementById('divfinan').style.display = 'none';
			   document.getElementById('arrowmoment').className = "";
		   }
		   else if(tabName == 'fleet')
		   {
				
			   document.getElementById('opera').className = 'deactive';
			   document.getElementById('fleet').className = 'active';
			   document.getElementById('qual').className = 'deactive';
			   document.getElementById('sales').className = 'deactive';
			   document.getElementById('finan').className = 'deactive';
			   document.getElementById('divopera').style.display = 'none';
			   document.getElementById('divfleet').style.display = 'block';
			   document.getElementById('divqual').style.display = 'none';
			   document.getElementById('divsales').style.display = 'none';
			   document.getElementById('divfinan').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentfleet";
		   }
		   else if(tabName == 'qual')
		   {
			   document.getElementById('opera').className = 'deactive';
			   document.getElementById('fleet').className = 'deactive';
			   document.getElementById('qual').className = 'active';
			   document.getElementById('sales').className = 'deactive';
			   document.getElementById('finan').className = 'deactive';
			   document.getElementById('divopera').style.display = 'none';
			   document.getElementById('divfleet').style.display = 'none';
			   document.getElementById('divqual').style.display = 'block';
			   document.getElementById('divsales').style.display = 'none';
			   document.getElementById('divfinan').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentqual";
		   }
		   else if(tabName == 'sales')
		   {
			   document.getElementById('opera').className = 'deactive';
			   document.getElementById('fleet').className = 'deactive';
			   document.getElementById('qual').className = 'deactive';
			   document.getElementById('sales').className = 'active';
			   document.getElementById('finan').className = 'deactive';
			   document.getElementById('divopera').style.display = 'none';
			   document.getElementById('divfleet').style.display = 'none';
			   document.getElementById('divqual').style.display = 'none';
			   document.getElementById('divsales').style.display = 'block';
			   document.getElementById('divfinan').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentsales";
		   }
		   else if(tabName == 'finan')
		   {
			   document.getElementById('opera').className = 'deactive';
			   document.getElementById('fleet').className = 'deactive';
			   document.getElementById('qual').className = 'deactive';
			   document.getElementById('sales').className = 'deactive';
			   document.getElementById('finan').className = 'active';
			   document.getElementById('divopera').style.display = 'none';
			   document.getElementById('divfleet').style.display = 'none';
			   document.getElementById('divqual').style.display = 'none';
			   document.getElementById('divsales').style.display = 'none';
			   document.getElementById('divfinan').style.display = 'block';
			   document.getElementById('arrowmoment').className = "arrowmomentfinan";
		   }
}
_setActiveoffering = function(tab)
{
	
		   document.getElementById('servi').className = '';
		   document.getElementById('value').className = '';
		   var tabName = tab;
		   if(tabName == 'servi')
		   {
			   document.getElementById('servi').className = 'active';
			   document.getElementById('value').className = 'deactive';
			   document.getElementById('divservi').style.display = 'block';
			   document.getElementById('divvalue').style.display = 'none';
			   document.getElementById('arrowmoment').className = "";
		   }
		   else if(tabName == 'value')
		   {
				
			   document.getElementById('value').className = 'active';
			   document.getElementById('servi').className = 'deactive';
			   document.getElementById('divvalue').style.display = 'block';
			   document.getElementById('divservi').style.display = 'none';
			   document.getElementById('arrowmoment').className = "arrowmomentservi";
		   }
}

_setMediaActive = function(tab)
{
		   document.getElementById('corp').className = 'deactive';
		   document.getElementById('miss').className = 'deactive';
		   document.getElementById('mana').className = 'deactive';
		   document.getElementById('event').className = 'deactive';
                   document.getElementById('divcorp').style.display = 'none';
                   document.getElementById('divmiss').style.display = 'none';
                   document.getElementById('divmana').style.display = 'none';
                   document.getElementById('divevent').style.display = 'none';
		   var tabName = tab;
                   
                   document.getElementById(tab).className = 'active';
                   document.getElementById('div' + tab).style.display = 'block';
                           
		   if(tabName == 'corp')
			   document.getElementById('arrowmoment').className = "";
		   else if(tabName == 'miss')
			   document.getElementById('arrowmoment').className = "arrowmomentprint";
		   else if(tabName == 'mana')
			   document.getElementById('arrowmoment').className = "arrowmomentvideo";
		   else if(tabName == 'event')
			   document.getElementById('arrowmoment').className = "arrowmomentevent";
}
function _getBooking(bid){
	document.getElementById('hdCheckfeedback').value = "false";
    if(bid != ""){
        var aj;
        var strParam;
        if (window.XMLHttpRequest)
            aj = new XMLHttpRequest;
        else if (window.ActiveXObject)
            aj = new ActiveXObject("Microsoft.XMLHTTP");
        strParam = "bookid=" + bid;
        aj.open("POST", webRoot + "/getbooking.php", true);
        aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        aj.onreadystatechange = function() {
            if (aj.readyState == 1){
                document.getElementById('spDiscount').innerHTML = "Fetching...";
            }
            else if (aj.readyState == 4) {
                var r = "";
                if(aj.status == 200){
                    r = aj.responseText;
                    aj = null;
                    //document.getElementById('spDiscount').innerHTML = r;
                    if(r != "" && r.indexOf("|#|") > -1){
                        var arrR = r.split("|#|");
			if(arrR[0] != ""){
			    //document.getElementById('spDiscount').innerHTML = "<a href=\"javascript: void(0);\" onclick=\"javascript: _getBooking(document.getElementById('bookingid').value);\">Get details</a>";
			    document.getElementById('spDiscount').innerHTML = "";
			    document.getElementById('hdmonumber').value = arrR[0];
			    document.getElementById('hdtxtname').value = arrR[1];
			    document.getElementById('hdemail').value = arrR[2];
			    document.getElementById('hdIsSelfDrive').value = arrR[3];
			    if(parseInt(arrR[3]) == "1")
				document.getElementById('divIsNotForSelfDrive').style.display = "none";
			    else
				document.getElementById('divIsNotForSelfDrive').style.display = "block";
			    document.getElementById('hdCheckfeedback').value = "true";
			} else {
			    document.getElementById('spDiscount').innerHTML = "";
			    document.getElementById('bookingid').value = "";
			    alert("Booking ID does not exist. \nPlease choose 'NO' if you dont have a valid Booking ID.");
			}
		    }
                }
                else
                    r = "ERROR " + aj.readyState + ": " + aj.statusText;
            }
        };
        aj.send(strParam);
    }
};

function _validateFeedback(bid){
    var cnf = false;
    if(document.getElementById('opt1').checked == true)
	{
        cnf = isFilledText(document.getElementById('bookingid'), "", "Please fill in your booking id.\nIn case you do not have booking id, please choose NO.");
    } 
	if(document.getElementById('opt2').checked == true)
	{
		cnf = isFilledText(document.getElementById('monumber'), "", "Please fill in your mobile number.");
		if(cnf){
			var m = document.getElementById('monumber').value.trim();
			if(m.length != 10){
				alert("Please fill in a valid mobile number");
				cnf = false;
			}
		}
		if(cnf)
			cnf = isFilledText(document.getElementById('txtname'), "", "Please fill in your name.");
		if(cnf)
			cnf = isFilledText(document.getElementById('email'), "", "Please fill in your email id.");
		if(cnf)
			cnf = isEmailAddr(document.getElementById('email'), "Please fill in a valid email id.");
	}
    if(cnf){
        if(document.getElementById('rdo1').checked == false && document.getElementById('rdo2').checked == false){
            alert("Were you satisfied with the booking procedure?");
            cnf = false;
        }
    }
    if(cnf){
        if(document.getElementById('hdIsSelfDrive').value == "0" && document.getElementById('rdo3').checked == false && document.getElementById('rdo4').checked == false){
            alert("Did the cab report on time?");
            cnf = false;
        }
    }
    if(cnf){
        if(document.getElementById('hdIsSelfDrive').value == "0" && document.getElementById('rdo5').checked == false && document.getElementById('rdo6').checked == false){
            alert("Were you happy with your experience with the chauffeur?");
            cnf = false;
        }
    }
    if(cnf){
        if(document.getElementById('rdo7').checked == false && document.getElementById('rdo8').checked == false){
            alert("Were you satisfied with the condition of the cab?");
            cnf = false;
        }
    }
    if(cnf){
        if(document.getElementById('rdo9').checked == false && document.getElementById('rdo10').checked == false){
            alert("Were you satisfied with your overall experience with us?");
            cnf = false;
        }
    }
    if(cnf){
		if((document.getElementById('hdCheckfeedback').value == "true" && document.getElementById('opt1').checked) || document.getElementById('opt2').checked)
		_feedbackpopup();
	    else
			alert("Invalid Booking Id");
    }
}

_chkBookingID = function()
{
	var cnf = false;
	if(document.getElementById('bookingid').value == "")
	{
		alert("Please fill in your booking id.\nIn case you do not have booking id, please choose NO.");
		document.getElementById('bookingid').focus();
		cnf = false;
	}
}
_chkMobile = function()
{
	var cnf = false;
	if(document.getElementById('monumber').value == "")
	{
		alert("Please fill in your mobile number.");
		document.getElementById('monumber').focus();
		cnf = false;
	}
	if(cnf){
        var m = document.getElementById('monumber').value.trim();
        if(m.length != 10){
            alert("Please fill in a valid mobile number");
            cnf = false;
        }
    }
}
_chkName = function()
{
	var cnf = false;
	if(document.getElementById('monumber').value == "")
	{
		document.getElementById('monumber').focus();
		cnf = false;
	}
	if(document.getElementById('txtname').value == "")
	{
		alert("Please fill in your name.");
		document.getElementById('txtname').focus();
		cnf = false;
	}
}
_chkEmailid = function()
{
	var cnf = false;
	if(document.getElementById('email').value == "")
	{
		alert("Please fill in your email id.");
		document.getElementById('email').focus();
		cnf = false;
	}
	if(cnf)
        cnf = isEmailAddr(document.getElementById('email'), "Please fill in a valid email id.");
}

_frmClear = function(){
    document.getElementById("bookingid").value = "";
    document.getElementById("monumber").value = "";
    document.getElementById("txtname").value = "";
    document.getElementById("email").value = "";
    document.getElementById("rdo1").checked = false;
    document.getElementById("rdo2").checked = false;
    document.getElementById("rdo3").checked = false;
    document.getElementById("rdo4").checked = false;
    document.getElementById("rdo5").checked = false;
    document.getElementById("rdo6").checked = false;
    document.getElementById("rdo7").checked = false;
    document.getElementById("rdo8").checked = false;
    document.getElementById("rdo9").checked = false;
    document.getElementById("rdo10").checked = false;
    document.getElementById("experience").value = "";
};

_frmDisable = function(t){
    document.getElementById("bookingid").disabled = t;
    document.getElementById("monumber").disabled = t;
    document.getElementById("txtname").disabled = t;
    document.getElementById("email").disabled = t;
    document.getElementById("rdo1").disabled = t;
    document.getElementById("rdo2").disabled = t;
    document.getElementById("rdo3").disabled = t;
    document.getElementById("rdo4").disabled = t;
    document.getElementById("rdo5").disabled = t;
    document.getElementById("rdo6").disabled = t;
    document.getElementById("rdo7").disabled = t;
    document.getElementById("rdo8").disabled = t;
    document.getElementById("rdo9").disabled = t;
    document.getElementById("rdo10").disabled = t;
    document.getElementById("experience").disabled = t;
};

_feedbackpopup = function()
{
	var bookingid = "";
    var mobno = "";
    var name = "";
    var email = "";

	if(document.getElementById('opt1').checked)
	{
		//Valid BookingID
		bookingid = document.getElementById("bookingid").value;
		mobno = document.getElementById("hdmonumber").value;
		name = document.getElementById("hdtxtname").value;
		email = document.getElementById("hdemail").value;
	}
	else
	{
		//No BookingID
		mobno = document.getElementById("monumber").value;
		name = document.getElementById("txtname").value;
		email = document.getElementById("email").value;

	}
    var statisfactionWBP = document.getElementsByName("rdo1");
    swbp = "";
    if(document.getElementById("rdo1").checked == true)
      swbp = 1;
    else if(document.getElementById("rdo2").checked == true)
      swbp = 2;

    cr = 1;
    if(document.getElementById("rdo3").checked == true)
      cr = 1;
    else if(document.getElementById("rdo4").checked == true)
      cr = 2;

    ewc = 1;
    if(document.getElementById("rdo5").checked == true)
      ewc = 1;
    else if(document.getElementById("rdo6").checked == true)
      ewc = 2;
	
    swcc = 1;
    if(document.getElementById("rdo7").checked == true)
      swcc = 1;
    else if(document.getElementById("rdo8").checked == true)
      swcc = 2;

    swoa = 1;
    if(document.getElementById("rdo9").checked == true)
      swoa = 1;
    else if(document.getElementById("rdo10").checked == true)
      swoa = 2;
	
    var txtExperienceWithUs = document.getElementById("experience").value;    
    if(txtExperienceWithUs !="")
	ExperienceWithUs = txtExperienceWithUs;
    else
	ExperienceWithUs = "";

    if(mobno != ""){
        var aj;
        var strParam;
        if (window.XMLHttpRequest)
            aj = new XMLHttpRequest;
        else if (window.ActiveXObject)
            aj = new ActiveXObject("Microsoft.XMLHTTP");
	var ienc = "0";
	if(document.getElementById('opt1').checked)
	    ienc = "1";
	var isd = "0";
	if(document.getElementById('hdIsSelfDrive'))
	    isd = document.getElementById('hdIsSelfDrive').value;
        strParam = "monumber=" + mobno + "&bookingid=" + bookingid + "&txtname=" + name + "&email=" + email + "&rdo1=" + swbp + "&rdo2=" + cr +"&rdo3=" + ewc + "&rdo4=" + swcc + "&rdo5=" + swoa + "&experience=" + ExperienceWithUs + "&enc=" +  ienc + "&isd=" +  isd;
	aj.onreadystatechange = function() {
            if (aj.readyState < 4){
		_frmDisable(true);
                document.getElementById('finishmess').innerHTML = "<b style='color:#00f;'>Wait...</b>";
            }
            else if (aj.readyState == 4) {
                var r = "";
                if(aj.status == 200){
		    _frmDisable(false);
		    _frmClear();
		    document.getElementById("finishmess").innerHTML = "<b style='color:#f00;'>Thanks for your feedback.</b>";
                }
                else
                    r = "ERROR " + aj.readyState + ": " + aj.statusText;
            }
        };
        aj.open("POST", webRoot + "/send-feedback.php", true);
        aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");        
        aj.send(strParam);
    }   
};

function _checkIsSelfDrive(){
    if(document.getElementById('selfdriveDiv')){
	if(document.getElementById('selfdriveDiv').style.display == "block"){
	    document.getElementById('hdIsSelfDrive').value = "1";
	    document.getElementById('divIsNotForSelfDrive').style.display = "none";
	} else {
	    document.getElementById('hdIsSelfDrive').value = "0";
	    document.getElementById('divIsNotForSelfDrive').style.display = "block";
	}
    }
}

/*Aamir 6th May*/
function _checkIsRedeemFilled()
{
	var cnf = false;
	cnf = isFilledText(document.getElementById('txtnamepb'), "", "Please fill in your name.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtemailpb'), "", "Please fill in your email id.");
	if(cnf)
		cnf = isEmailAddr(document.getElementById('txtemailpb'), "Please fill in a valid email id.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtcardnopb'), "", "Please fill in loyalty card number.");
	if(cnf)
	{
		_disableThisPage();_setDivPos('redeempopup');
	}
}

function _validateLoyalty()
{
	var cnf = false;
	cnf = isFilledText(document.getElementById('txtnamepbpop'), "", "Please fill in your name.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtemailpbpop'), "", "Please fill in your email id.");
	if(cnf)
		cnf = isEmailAddr(document.getElementById('txtemailpbpop'), "Please fill in a valid email id.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtmobilepbpop'), "", "Please fill in mobile number.");
	if(cnf)
	{
		var m = document.getElementById('txtmobilepbpop').value.trim();
			if(m.length != 10){
			alert("Please fill in a valid mobile number");
			cnf = false;
		}
	}
	if(cnf)
		cnf = isFilledText(document.getElementById('txtcardnopbpop'), "", "Please fill in loyalty card number.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtredeempoints'), "", "Please fill in the points you wish to redeem.");
	if(cnf)
		cnf = isFilledText(document.getElementById('txtpaybackpin'), "", "Please fill in your PIN.");
}

function _copyFromTopUp()
{
	if(document.getElementById('txtname').value != "" && document.getElementById('txtname').value != "Name" && document.getElementById('email').value != "" && document.getElementById('email').value != "Email")
	{
		if(document.getElementById('chkCopyFromTop').checked == true)
		{
			document.getElementById('txtnamepb').value = document.getElementById('txtname').value;
			document.getElementById('txtemailpb').value = document.getElementById('email').value;
		}
		else
		{
			document.getElementById('txtnamepb').value = "";
			document.getElementById('txtemailpb').value = "";
		}
	}
	else
	{
		alert("Please specify your Name and Email.");
		document.getElementById('chkCopyFromTop').checked = false;
	}
}
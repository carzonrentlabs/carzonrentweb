<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<script>
	$(function(){
		$('select.styled').customSelect();		
		
		$('#slides').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesLocal').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
		$('#slidesInt').slides({
			preload: false,
			preloadImage: 'img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
				$('.caption').animate({
					bottom:-35
				},100);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationStart on slide: ', current);
				};
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log('animationComplete on slide: ', current);
				};
			},
			slidesLoaded: function() {
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
	});

	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
	arrDestination = new Array();
<?php
	for($i = 0; $i < count($des); $i++){
?>
		arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
</script>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css">

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
<script type="text/javascript">
function _setIsOL(id)
{
	document.getElementById('outstationDiv').style.display = 'none';
	document.getElementById('localDiv').style.display = 'none';

	if(document.getElementById('rdOut').checked == true)
		document.getElementById('outstationDiv').style.display = 'block';
	if(document.getElementById('rdL').checked == true)
		document.getElementById('localDiv').style.display = 'block';
}
function _setActive(tab)
{
	document.getElementById('OL').style.display = 'none';
	document.getElementById('L').style.display = 'none';

    document.getElementById('TripleTabs').className = '';
	
    document.getElementById('outstation').className = '';
    document.getElementById('local').className = '';
    //document.getElementById('selfdrive').className = '';
    
    document.getElementById('outstationDiv').style.display = 'none';
    document.getElementById('localDiv').style.display = 'none';
    //document.getElementById('selfdriveDiv').style.display = 'none';
    
    document.getElementById(tab).className = 'activeTab';
    document.getElementById(tab + 'Div').style.display = 'block';
	if(tab == "outstation")
		document.getElementById('OL').style.display = 'block';
	if(tab == "local")
		document.getElementById('L').style.display = 'block';
};
</script>
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js" async="true"></script>
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>	
<!--Header Start Here-->
<!--Header Start Here-->
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/wz_tooltip.js"></script>
<?php
    $corH = new COR();
    $res = $corH->_CORGetCities();	
    $myXML = new CORXMLList();
    $orgH = $myXML->xml2ary($res);
    $orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    $orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    unset($corH);
    
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    $refURL = $_SERVER['HTTP_REFERER'];
    $refURLParts = split("/", $refURL);
    $domain = $refURLParts[2];
    if(strpos($refURLParts[3], "q=") != false)
	    $qstr = split("q=", $refURLParts[3]);
    else if(strpos($refURLParts[3], "p=") != false)
	    $qstr = split("p=", $refURLParts[3]);
    $qterm = split("&", $qstr[1]);
    $qterm = urldecode($qterm[0]);
    if($qterm != ""){
	    $searchquery = $qterm;
	    $arrIllegalChars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','`','{','}','[',']',':',';','?',',','.','<','>','/','|','\\');
	    $searchquery = str_replace($arrIllegalChars, '', $searchquery);
	    $searchquery = str_replace("'", '', $searchquery);
	    $searchquery = str_replace('"', '', $searchquery);
	    $searchquery = str_replace('  ', ' ', $searchquery);
	    $searchquery = str_replace(' ', '-', $searchquery);
	    
	    //$srvName = "119.82.70.66";
	    //$dbName = "cor_by_bytesbrick";
	    //$UID = "root";
	    //$PWD = "Cor@1234";
	    $srvName = "localhost";
	    $dbName = "cor_by_bytesbrick";
	    $UID = "root";
	    $PWD = "";
	    $con = mysql_connect($srvName, $UID, $PWD);
	    if(!$con)
		    die("Unable to connect the database");
	    mysql_select_db($dbName, $con);
	
	    $sql = "insert into cor_searchkeywords (search_string, entry_date, search_url, search_engine, page_url) values ('" . mysql_real_escape_string($qterm) . "', ".date('Y-m-d H:i:s').",'" . mysql_real_escape_string($searchquery) . "','" . $domain . "','" . $pageURL . "')";
	    mysql_query($sql);
	    
	    mysql_close($con);
    }
?>
		<?php
			$pgName = corWebRoot;
			$defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
			$defCityID = array(2, 4, 7, 6);
		?>		 
<!--Header Ends Here-->



<!--Banner Start Here-->
<div class="banner">
    	<div class="lfts">        
        <!--TripleTabs Start Here-->
			<ul id="typeTab">
				<li>
					<a id="outstation" class="activeTab" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">From Airport</a>
				</li>
				<li>
					<a id="local" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">To Airport</a>
				</li>
				<!-- <li>
					<a id="selfdrive" href="javascript:void(0)" onclick="javascript: _setActive(this.id)">SELF DRIVE</a>
				</li> -->
			</ul>
        <div id="TripleTabs">

		<div class="newbar">
			<div id="OL" style="display:block">
				<input style="margin-left:5px" type="radio" name="rdOutstationOrLocal" id="rdOut" checked="checked" onclick="javascipt: _setIsOL(this.id);"> Outstation
				<input style="margin-left:30px" type="radio" name="rdOutstationOrLocal" id="rdL" onclick="javascipt: _setIsOL(this.id);"> Local
			</div>
			<div id="L" style="margin-left:5px">Local</div>
		</div>

		<div id="outstationDiv" class="tber" style="display:block;">
		<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
        <form id="form1" name="form1" action="<?php echo corWebRoot; ?>/search-result.php" method="POST" target="_blank">
		<input type="hidden" name="hdOriginName" id="hdOriginName" value="Bangalore"/>
		<input type="hidden" name="hdOriginID" id="hdOriginID" value="7" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationName" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationID" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Outstation" />
                    <div class="bdrbtm">
<?php
		$selCityId = 2;
		$selCityName = "Delhi";
		for($i = 0; $i < count($org); $i++)
		{
			if($org[$i]["CityName"] == $cCity){
				$selCityId = $org[$i]["CityID"];
				$selCityName = $org[$i]["CityName"];
?>
			<script type="text/javascript" language="javascript">
				document.getElementById('hdOriginID').value = '<?php echo $org[$i]["CityID"]; ?>';
				document.getElementById('hdOriginName').value = '<?php echo $org[$i]["CityName"]; ?>';
			</script>
<?php
			}
		}
?>
                        <p style="margin-bottom:2px;">
				<span class="cities">
					<input type="text" name="txtDestination" id="txtDestination" value="I want to go to" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest', arrDestination, 0, 0, 0);" onfocus="javascript: if(this.value=='I want to go to'){this.value='';}" onblur="javascript: if(this.value==''){this.value='I want to go to';}"/>
					<div id="autosuggest" class="autosuggest floatingDiv"></div>
				</span>
                        </p>
                    </div>
                    <p style="width:100px;float:left">
                        <label>From</label>
                        <span class="datepick">
				<input type="text" type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField" class="datepicker" onchange="javascript: _setDropDate('inputField', 'inputField2');" />
                        </span>	
                    </p>
                    <p style="width:100px;float:left">
                        <label>To</label>
	                    <span class="datepick">
	                        <input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo date('d M, Y'); ?>" id="inputField2" class="datepicker" />
	                    </span>
                    </p>
					<p>
                        <label>&nbsp;</label>
                        <span class="mkebtn" style="float:left;">
							<input type="button" id="btnMakePayment" name="btnMakePayment" value="Submit" onclick="javascript: return _validate();" />
							<img style="float:right;width:120px" src ="<?php echo corWebRoot; ?>/images/carzonrent.gif">
                        </span>
                    </p>
                </form>
            </div>
		<div id="localDiv" class="tber">
                <form id="form2" name="form2" action="<?php echo corWebRoot; ?>/search-result.php" method="POST" onsubmit="javascript: return _validateLocal();" target="_blank">
		<input type="hidden" name="hdOriginName" id="hdOriginNameL" value="Bangalore" />
		<input type="hidden" name="hdOriginID" id="hdOriginIDL" value="7" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationNameL" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationIDL" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Local" />
                    <div class="bdrbtmL">
<?php
		$selCityId = 2;
		$selCityName = "Delhi";
		for($i = 0; $i < count($org); $i++)
		{
			if($org[$i]["CityName"] == $cCity){
				$selCityId = $org[$i]["CityID"];
				$selCityName = $org[$i]["CityName"];
?>
			<option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
			<script type="text/javascript" language="javascript">
				document.getElementById('hdOriginIDL').value = '<?php echo $org[$i]["CityID"]; ?>';
				document.getElementById('hdOriginNameL').value = '<?php echo $org[$i]["CityName"]; ?>';
			</script>
<?php
			}
		}
?>

				<p style="margin-top:4px">
                        <!--<label>I want a cab for</label>-->
                        <span class="cabhours"><select class="styled wdth60" style="width:100px;" name="cabHour" id="cabHour" onchange="javascript: _setHour(this.value);">
    	                <?php
				for($i = 4; $i <= 20; $i++){
			?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?> hours</option>
			<?php
				}
			?>
                        </select></span>
			<span id="selCH" style="margin-left:6px;float:left;" class="showhour"><span id="selCabHour">40 kms included</span></span>
                    </p>
		    </div>
                    <p style="float:left;width:110px;">
                        <span style="float:left;padding-top:2px;display:block">On</span><span class="datepick">
                            <input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField5" class="datepicker" />
                        </span>
                    </p>
                    <p style="float:left;width:90px;">
	                    <span class="pickuphour">
	                        <select class="styled wdth60" name="tHour" id="tHour" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
    	                        <!--<option>Hours</option> -->
					<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 10){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
						<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
					<?php } ?>
                            </select>
                        </span>
                        <span class="pickupmin ml5">
	                        <select class="styled wdth60" name="tMin" id="tMin" onchange="javascript: _setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');">
    	                        <!--<option>Min</option>  -->
								<option value="00">00</option> 
								<option value="15">15</option> 
								<option value="30">30</option>
								<option value="45">45</option>
                            </select>
                        </span>
			<span id="selT" style="margin-left:6px" class="showtime"><span id="seltime"></span></span>
			<script type="text/javascript">
				_setTime('tHour', 'tMin', 'selT', 'seltime', 'userTime');
			</script>
                    </p>
                    <p>
                        <label>&nbsp;</label>
                        <span class="mkebtn" style="float:left;">
							<input type="submit" id="btnMakePayment" name="btnMakePayment" value="Submit" />
							<img style="float:right;width:120px" src ="<?php echo corWebRoot; ?>/images/carzonrent.gif">
                        </span>
                    </p>
                </form>  
        </div>
        </div>
        </div>    
</div>
<!--Middle Start Here-->
<?php
if(trim($cCity) != ""){
	$xmlDoc = new DOMDocument();
	$xmlDoc->load("./xml/city-desc/" . strtolower($cCity) . ".xml");
	$itemArray = $xmlDoc->getElementsByTagName("city");
	$optText = "";
	foreach($itemArray as $item){
		$txt = $item->getElementsByTagName("citydesc");
		$optText = $txt->item(0)->nodeValue;
	}
	$xmlDoc = null;
?>
	<div class="about-city" style="float:left;width:100%" >
		<div class="main">
			<div class="city-desc">			
				<h2>About <?php echo $cCity; ?></h2><br />
			<?php if($optText != "") { ?>
				<?php echo $optText ?>
			<?php } ?>
			</div>
		</div>
	</div>
<?php
}
?>
</body>
</html>

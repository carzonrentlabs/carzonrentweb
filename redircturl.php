<?php
	session_start();
	include_once("./includes/cache-func.php");
		include_once('./classes/cor.mysql.class.php');	
	$failUrl = corWebRoot."/booking-fail.php";
	$url = 'https://npg.payback.in/nextgen-pgateway/PGTransaction';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$post_items[] = 'transactionId='.$_POST["transactionId"];
	$post_items[] = 'transactionStatus='.$_POST["transactionStatus"];
	$post_items[] = 'txnDigest='.$_POST["txnDigest"];
	$post_items[] = 'redeemedPts='. $_POST["redeemedPts"];
	$post_items[] = 'orderid='.$_POST["orderid"];

	 
	//format the $post_items into a string
	$post_string = implode ('&', $post_items);
	 
	//send the $_POST data to the new page
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

	$response = curl_exec($ch);
	curl_close($ch);
	//echo $response;
	if($response == 'Approved')
	{
		$transID = str_ireplace("CORIC", "CORIC-", $_POST["transactionId"]);
			
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$dataWhere = array();
		$dataWhere["coric"] = $transID;
		
		$dataToSave = array();
		$dataToSave["disc_txn_id"] = $_POST["transactionId"];
		if(isset($_POST["redeemedPts"])){
			$dataToSave["disc_value"] = $_POST["redeemedPts"];
			$dataToSave["discount_amt"] = .25* $_POST["redeemedPts"];
		}
		if(isset($_POST["transactionStatus"]))
		$dataToSave["disc_txn_status"] = $_POST["transactionStatus"];
		if(isset($_POST["orderid"]))
		$dataToSave["disc_orderid"] = $_POST["orderid"];
		$r = $db->update("cor_booking_new", $dataToSave, $dataWhere);			
		unset($dataToSave);
		unset($dataWhere);
		
		$sql = "SELECT * FROM cor_booking_new WHERE coric = '" . $transID . "' AND booking_id IS NULL";
		$booking = $db->query("query", $sql);
		
		$db->close();
		$discountPB = .25* $_POST["redeemedPts"];
		$totFare = $booking[0]["tot_fare"] + $booking[0]["additional_srv_amt"] + ceil(($booking[0]["additional_srv_amt"] * $booking[0]["vat"]) / 100);
		if($discountPB >= intval($totFare)){
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			$dataWhere = array();
			$dataWhere["coric"] = $transID;
			
			$dataToSave = array();
			$dataToSave["nb_order_no"] = $transID;
			$dataToSave["Checksum"] = $_POST["orderid"];
			$dataToSave["AuthDesc"] = "Y";
			$dataToSave["pb_status"] = 2;
			
			$r = $db->update("cor_booking_new", $dataToSave, $dataWhere);
			unset($dataToSave);
			unset($dataWhere);
?>
<form id="formTravelDetail" name="formTravelDetail" method="POST" action="<?php echo corWebRoot;?>/payment-confirmation.php">
			<input type="hidden" name="TxId" id="TxId" value="<?php echo $transID; ?>" />
			</form>
			<script>
				document.getElementById("formTravelDetail").submit();
			</script>
<?php
		} else {
			if($booking[0]["tour_type"] == 'Outstation')
				{
?>
                        <form id="formTravelDetail" name="formTravelDetail" method="POST" action="<?php echo corWebRoot; ?>/outstation/payment/<?php echo trim(str_ireplace(" ", "-", strtolower($booking[0]["origin_name"]))); ?>-to-<?php echo trim(str_ireplace(",", "-to-",str_ireplace(" ", "-", strtolower($booking[0]["destinations_name"])))); ?>/">;
<?php
				}
				elseif($booking[0]["tour_type"] == 'Local'){
?>
                            <form id="formTravelDetail" name="formTravelDetail" method="POST" action="<?php echo corWebRoot; ?>/local/payment/<?php echo trim(str_ireplace(" ", "-", strtolower($booking[0]["origin_name"]))); ?>/">
<?php
				}
				
				elseif($booking[0]["tour_type"] == 'airport'){
?>
                            <form id="formTravelDetail" name="formTravelDetail" method="POST" action="<?php echo corWebRoot; ?>/airport/payment/<?php echo trim(str_ireplace(" ", "-", strtolower($booking[0]["origin_name"]))); ?>/">
<?php
				}
				
				
				
				else{
?>
                                <form id="formTravelDetail" name="formTravelDetail" method="POST" action="<?php echo corWebRoot; ?>/self-drive/payment/<?php echo trim(str_ireplace(" ", "-", strtolower($booking[0]["origin_name"]))); ?>/">
<?php
				}
?>				<input type="hidden" name="hdCORIC" id="hdCORIC" value="<?php echo $booking[0]["coric"]; ?>" />
				<input type="hidden" name="hdCarModel" id="hdCarModel" value="<?php echo $booking[0]["model_name"]; ?>" />
				<input type="hidden" name="hdOriginName" id="hdOriginName" value="<?php echo $booking[0]["origin_name"]; ?>" />
				<input type="hidden" name="hdOriginID" id="hdOriginID" value="<?php echo $booking[0]["origin_id"]; ?>" />
				<input type="hidden" name="hdDestinationName" id="hdDestinationName" value="<?php echo $booking[0]["destinations_name"]; ?>" />
				<input type="hidden" name="hdDestinationID" id="hdDestinationID" value="<?php echo $booking[0]["destinations_id"]; ?>" />
				<input type="hidden" name="hdPkgID" id="hdPkgID" value="<?php echo $booking[0]["package_id"]; ?>" />
				<input type="hidden" name="hdCat" id="hdCat" value="<?php echo $booking[0]["car_cat"]; ?>" />
				
				<input type="hidden" name="hdCarCatID" id="hdCarCatID" value="<?php echo $booking[0]["car_cat_id"]; ?>" />
				<input type="hidden" name="duration" id="duration" value="<?php echo $booking[0]["duration"]; ?>" />
				<input type="hidden" name="totFare" id="totFare" value="<?php echo $booking[0]["tot_fare"]; ?>" />
				<input type="hidden" name="hdPickdate" id="hdPickdate" value="<?php echo $booking[0]["pickup_date"]; ?>" />
				<input type="hidden" name="hdDropdate" id="hdDropdate" value="<?php echo $booking[0]["drop_date"]; ?>" />
				<input type="hidden" name="hdTourtype" id="hdTourtype" value="<?php echo $booking[0]["tour_type"]; ?>" />
				<input type="hidden" name="monumber" id="monumber" value="<?php echo $booking[0]["mobile"]; ?>" />
			<?php
				$pickDate = date_create($booking[0]["pickup_date"]);
				$pickTime = date_create($booking[0]["pickup_time"]);
				$dropDate = date_create($booking[0]["drop_date"]);
				if(!is_null($booking[0]["drop_time"]))
				$dropTime = date_create($booking[0]["drop_time"]);
			?>
				<input type="hidden" name="name" id="name" value="<?php echo $booking[0]["full_name"]; ?>" />
				<input type="hidden" name="email" id="email" value="<?php echo $booking[0]["email_id"]; ?>" />
				<input type="hidden" name="cciid" id="cciid" value="<?php echo $booking[0]["cciid"]; ?>" />
				<input type="hidden" name="bookDays" id="bookDays" value="<?php echo $booking[0]["duration"]; ?>" />
				<input type="hidden" name="tHourP" id="tHourP" value="<?php echo $pickTime->format("i"); ?>" />
				<input type="hidden" name="tMinP" id="tMinP" value="<?php echo $pickTime->format("s"); ?>" />
				
				<input type="hidden" name="tHourD" id="tHourD" value="<?php if(!is_null($booking[0]["drop_time"])) { echo $dropTime->format("i"); } ?>" />
				<input type="hidden" name="tMinD" id="tMinD" value="<?php if(!is_null($booking[0]["drop_time"])) { echo $dropTime->format("s"); } ?>" />
				<input type="hidden" name="totAmount" id="totAmount" value="<?php echo $booking[0]["base_fare"]; ?>" />
				<input type="hidden" name="vat" id="vat" value="<?php echo $booking[0]["vat"]; ?>" />
				<input type="hidden" name="secDeposit" id="secDeposit" value="<?php echo $booking[0]["sec_deposit"]; ?>" />
				<input type="hidden" name="discount" id="discount" value="<?php echo $booking[0]["discount"]; ?>" />
				<input type="hidden" name="discountAmt" id="discountAmt" value="<?php echo $discountPB; ?>" />
				<input type="hidden" name="addServ" id="addServ" value="<?php echo $booking[0]["additional_srv"]; ?>" />
				
				<input type="hidden" name="addServAmt" id="addServAmt" value="<?php echo $booking[0]["additional_srv_amt"]; ?>" />
				<input type="hidden" name="dayRate" id="dayRate" value="<?php echo $booking[0]["dayRate"]; ?>" />
				<input type="hidden" name="kmRate" id="kmRate" value="<?php echo $booking[0]["km_rate"]; ?>" />
				<input type="hidden" name="tab" id="tab" value="3" />
				<input type="hidden" name="hdDistance" id="hdDistance" value="<?php echo $booking[0]["distance"]; ?>" />
				<input type="hidden" name="tHour" id="tHour" value="<?php echo $booking[0]["tHour"]; ?>" />
				<input type="hidden" name="tMin" id="tMin" value="<?php echo $booking[0]["tMin"]; ?>" />
				<input type="hidden" name="address" id="address" value="<?php echo $booking[0]["address"]; ?>" />
				
				<input type="hidden" name="remarks" id="remarks" value="<?php echo $booking[0]["remarks"]; ?>" />
				<input type="hidden" name="empcode" id="empcode" value="PayBack" />
				<input type="hidden" name="disccode" id="disccode" value="<?php echo $booking[0]["discount_coupon"]; ?>" />
				<input type="hidden" name="ddlSubLoc" id="ddlSubLoc" value="<?php echo $booking[0]["subLoc"]; ?>" />
				<input type="hidden" name="ddlSubLocName" id="ddlSubLocName" value="<?php echo $booking[0]["subLocName"]; ?>" />
				<input type="hidden" name="subLocCost" id="subLocCost<?php echo $i; ?>" value="<?php echo $booking[0]["subLocCost"]; ?>" />
				<input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo $booking[0]["OriginalAmt"]; ?>" />
				<input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $booking[0]["BasicAmt"]; ?>" />
				<input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $booking[0]["KMIncluded"]; ?>" />
				<input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $booking[0]["ExtraKMRate"]; ?>" />
				<input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $booking[0]["pkg_type"]?>" />
				
				<!-- added vinod-->
				<input type="hidden" name="airportCharges" id="airportCharges<?php echo $i; ?>" value="<?php echo $booking[0]["airportCharges"]?>" />
				<!-- added vinod-->
				
				<input type="hidden" name="addServiceCostAll" id="addServiceCostAll<?php echo $i; ?>" value="<?php echo $booking[0]["add_service_cost_all"]?>" />
				
				<input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $booking[0]["WeekDayDuration"]?>" />
				<input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $booking[0]["WeekEndDuration"]?>" />
				<input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $booking[0]["FreeDuration"]?>" />
				
			</form>
			<script>
				document.getElementById("formTravelDetail").submit();
			</script>
<?php
		}
	}
	else{
		$transID = "";
		if(isset($_REQUEST["transactionId"]) && $_REQUEST["transactionId"] != ""){
		    $transID = str_ireplace("CORIC", "CORIC-", $_REQUEST["transactionId"]);
		   			
		    $db = new MySqlConnection(CONNSTRING);
		    $db->open();
		    $dataWhere = array();
		    $dataWhere["coric"] = $transID;
		    $dataToSave = array();
		    $dataToSave["pb_status"] = 3;
		    $r = $db->update("cor_booking_new", $dataToSave, $dataWhere);
		    unset($dataToSave);
		    unset($dataWhere);
		    $db->close();
		}
?>
		<form id="formBooking" name="formBooking" method="POST" action="<?php echo $failUrl; ?>">
		<input type="hidden" name="TxId" id="TxId" value="<?php echo $transID; ?>" />
		</form>
		<script>
			document.getElementById("formBooking").submit();
		</script>
<?php
	}
?>
<!DOCTYPE HTML>
<?php

     error_reporting(0);
	 header('Location:http://www.carzonrent.com/');
	die;
     //error_reporting(E_ALL);
     ini_set("display_errors", 1);
     include_once('../classes/cor.ws.class.php');
     include_once('../classes/cor.xmlparser.class.php');
     include_once("../includes/cache-func.php");
     include_once('../classes/cor.mysql.class.php');
     $cor = new COR();
     $res = $cor->_CORGetCities();	
     $myXML = new CORXMLList();
     $isHrAvail = true;
	 unset($arr);
     $arr = array();
     $arr["CityID"] = 2;
     $arr["PkgType"] = "Daily";
     $res = $cor->_CORGetPackageDetails_Ladakh($arr);	
     $pkgsDaily = $myXML->xml2ary($res->{"GetPackageDetails_LadakhResult"}->{"any"});
     //echo "<pre>";
     //print_r($pkgsDaily);
     //echo "</pre>";
	
	
	
	
?>

<html>
     <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Myles Franchise</title>
	<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/franchise/css/mylesfranchise.css">
	
	<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/franchise/css/tab.css">
	<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
	<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/myles-campaign/style.css">
	<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo corWebRoot; ?>/slider/slidesjs.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/franchise/css/drivetowin.css">
	<link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'>
	  <script>
	  document.createElement('article');
	  document.createElement('section');
	  document.createElement('aside');
	  document.createElement('hgroup');
	  document.createElement('nav');
	  document.createElement('header'); 
	  document.createElement('footer');
	  document.createElement('figure');
	  document.createElement('figcaption'); 
	  </script>
	  <!-- Internet Explorer HTML5 enabling code: -->
	  <!--[if IE]>
	  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	  <style type="text/css">
	  .clear {
	    zoom: 1;
	    display: block;
	  }
	  </style>
	  <![endif]-->
	  <!-- Tab start -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <?php include_once("./includes/header-css.php"); ?>
	  <?php include_once("./includes/header-js.php");
?>
	  <link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
	  <!-- Slider image start -->
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/slider/slidesjs.css" type="text/css" media="screen" />
	  <!-- Slider image end -->
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>

	  <!-- Tab end -->
	  <!-- OnClick Slider start -->
	  <link href="css/jcarousel.css" rel="stylesheet" type="text/css">
	  <!-- OnClick Slider end -->
	  <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
	  <script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
	  <style type="text/css">
body {
	background: #fff;
}
</style>
	  </head>
     <body>
	  <?php include_once("../includes/header.php"); ?>
	  <div class="clr"> </div>
	  <section class="main">
	  <br>
	  <br>
<div class="content_design">
<strong>What are the minimum financial requirements?</strong>
<p>Car rental is a capital-intensive business. The actual financial requirement would vary from location, fleet size, types of cars, market size etc. The initial investment would vary from INR 14 – 50 lakhs.</p>
<strong>What all is included in the initial investment?</strong>
<p>Initial investment includes down payment for new car purchase, center set-up cost, technology, branding and franchise fees.</p>
<strong>How long is the approval process?</strong>
<p>After getting the approval on proposed location and signing the agreement, it takes around 25 to 60 days to launch the location and start the operations.</p>
<strong>What kind of background do I need to be a successful franchisee?</strong>
<p>Carzonrent is looking for energetic entrepreneurs who enjoy working within the travel and/or automotive field. Interested people must have a service attitude, be tech savy and have solid selling skills. Prior experience in car rental or travel is not mandatory.</p>
<strong>What is the franchise term? Is it renewable?</strong>
<p>The license agreement is a 5-year renewable term and it is renewable.</p>
<br>
<br>
<br>
<br>
</div> 
</section>
	  
	<br>
	<br>
	  <div class="clr"> </div>
<footer>
<?php
include("includes/footer.php");
?>
</footer>
   
	 
	 
     </body>
</html>
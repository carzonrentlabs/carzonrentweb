<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Corporate Rental Services - Carzonrent (India) Pvt. Ltd.</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. provides corporate rental services with the best in Class standards of service and impeccable quality." />
<meta name="keywords" content="corporate rental, corporate rental service, corporate rental services" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" ></script>
</head>
<body>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:2000, autoplay_slideshow:false});
});
</script> 
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<div class="tbul">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">Corporate Rental</a></li>
    
  </ul>
</div>
<div class="lfcnt">
<div id="country1" class="tabcontent">
  <h1>Corporate Travel</h1>
  <p><strong>Our Corporate Travel product has been designed for the discerning taste of its clients where they get the best in Class standards of service and impeccable quality. </strong></p>
  <p>For the business travelers time is money and we understand that. It is our aim to make business travel simple, hassle free and cost effective. We understand today's competitive business environment that puts a premium on value and high quality service delivered at a reasonable price</p>

<p>For partnering with us for your corporate travel please get in touch with<br />
Landline: 91-11-43083000<br />
E-mail: <a href="mailto:reserve@carzonrent.com">reserve@carzonrent.com</a></p> 

  <div class="clr"></div>
</div>



</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

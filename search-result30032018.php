<script type="text/javascript">
//function to display Popup
    function div_show(val) {

        document.getElementById('flaxi_option' + val).style.display = "block";
        document.getElementById('popup_flaxi' + val).style.display = "block";
        $('.flexibleplanclickid' + val).attr('id', 'flexibleplanclickid');
    }
//function to hide Popup
    function div_hide(val) {
        document.getElementById('flaxi_option' + val).style.display = "none";
        document.getElementById('popup_flaxi' + val).style.display = "none";
        $('.flexibleplanclickid' + val).attr('id', '');
    }
//**** url encryption code added by abhishek********/
    function subform(formid)
    {
        //alert('formid = ' + formid);
        var d = new Date();
        urldatetime = new Date($("#hdUrlcurdate" + formid).val());
        difftime = ((d - urldatetime) / (1000));

        if (difftime > 600)
        {

            pickupdate = $("#hdpickupdateforsearch").val();

            pickupatejs = new Date(+pickupdate);
            pickupdatejsdiff = (pickupatejs - d);

            changehour = 3 * 60 * 60 * 1000;

            if (pickupdatejsdiff < changehour)
            {

                changedpickupdate = parseInt(pickupdate) + (3 * 60 * 60 * 1000);

                changedpickupatejs = new Date(+changedpickupdate);
                months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                curMonth = months[changedpickupatejs.getMonth()];

                pickdate = changedpickupatejs.getDate()

                pickfullyear = changedpickupatejs.getFullYear()

                pickhours = changedpickupatejs.getHours();

                pickhours = parseInt(pickhours) < 10 ? '0' + pickhours : pickhours;
                pickminutes = changedpickupatejs.getMinutes();
                pickminutes = parseInt(pickminutes) < 10 ? '0' + pickminutes : pickminutes;

                pickupchangeddate = pickdate + " " + curMonth + ", " + pickfullyear + " " + pickhours + ":" + pickminutes;

                $("#inputFieldSF1").val(pickupchangeddate);
                $("#pickdateMS").val(pickdate + " " + curMonth + ", " + pickfullyear);
                $("#tHourPMS").val(pickhours);
                $("#tMinPMS").val(pickminutes);
                dropoffdate = $("#hddropoffforsearch").val();
                changeddropoffdate = parseInt(dropoffdate) + (3 * 60 * 60 * 1000);

                changeddropoffdatejs = new Date(+changeddropoffdate);

                dropoffdate = changeddropoffdatejs.getDate();
                dropoffcurMonth = months[changeddropoffdatejs.getMonth()];
                dropofffullyear = changeddropoffdatejs.getFullYear();

                dropoffhours = changeddropoffdatejs.getHours();

                dropoffhours = parseInt(dropoffhours) < 10 ? '0' + dropoffhours : dropoffhours;
                dropoffminutes = changeddropoffdatejs.getMinutes();
                dropoffminutes = parseInt(dropoffminutes) < 10 ? '0' + dropoffminutes : dropoffminutes;

                dropoffchangeddate = dropoffdate + " " + dropoffcurMonth + ", " + dropofffullyear + " " + dropoffhours + ":" + dropoffminutes;
                $("#inputFieldSF2").val(dropoffchangeddate);
                $("#dropdateMS").val(dropoffdate + " " + dropoffcurMonth + ", " + dropofffullyear);
                $("#tHourDMS").val(dropoffhours);
                $("#tMinDMS").val(dropoffminutes);

            }
            else
            {
                ///alert("abc = " + (pickupdatejsdiff < changehour));
            }

            //   _showPopUp("html", '<br /><div style="text-align:center;"><img src="' + webRoot + '/images/loader.gif" border="0" /><br /><br />Please wait, while we re-check the availability of our cars.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', "t", 400, 150, true, "");
//            $("#btnModifysrch").click();
//            /// _validateSFMS();
//            return false;
        }

        var dataString = $("#formBook" + formid).serialize();

        $.ajax({
            type: "POST",
            url: "urlencript.php",
            data: dataString,
            success: function (response) {

                $("#formBook" + formid).attr('action', 'search-result.php?cityid=' + ($("#hdOriginID" + formid).val()) + '&city=' + ($("#hdOriginName" + formid).val()) + '&car=' + ($("#hdCarModel" + formid).val()) + '&carid=' + ($("#hdCarCatID" + formid).val()) + '&p=' + response);
                $("#formBook" + formid).submit();
            }});


        return false;
    }


</script>
<?php
//session_start();
error_reporting(0);
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
include_once('./classes/cor.mysql.class.php');
include_once('./classes/cor.gp.class.php');
include_once("./includes/cache-func.php");
/* * * added abhishek for encryption** */
$base64decodedparameter = base64_decode($_REQUEST['p']);
$base64decodedparameter_amp = explode("&", $base64decodedparameter);
foreach ($base64decodedparameter_amp as $key => $val) {
    if ($key != 'p') {
        $eqval = explode("=", $val);
        $_REQUEST[$eqval[0]] = $eqval[1];
    }
}
/* * * end  abhishek for encryption** */
$cor = new COR();
$res = $cor->_CORGetCities();
$myXML = new CORXMLList();
$org = $myXML->xml2ary($res);

$res = $cor->_CORGetDestinations();
$des = $myXML->xml2ary($res);
unset($res);
unset($cor);
$tCCIID = 0;
if (isset($_REQUEST["hdOriginName"])) {
    $orgName = $_REQUEST["hdOriginName"];
    $orgNames = explode(",", $orgName);
}
if (isset($_REQUEST["hdOriginID"])) {
    $orgID = $_REQUEST["hdOriginID"];
    $orgIDs = explode(",", $orgID);
}
if (isset($_REQUEST["hdDestinationName"])) {
    $destName = $_REQUEST["hdDestinationName"];
    $destNames = explode(",", $destName);
}
if (isset($_REQUEST["hdDestinationID"])) {
    $destID = $_REQUEST["hdDestinationID"];
    $destIDs = explode(",", $destID);
}
if (isset($_REQUEST["pickdate"])) {
    $pDate = str_ireplace(",", "", $_REQUEST["pickdate"]);
    $pickDate = date_create($pDate);
}
if (isset($_REQUEST["dropdate"])) {
    if (trim($_REQUEST["hdTourtype"]) == "Selfdrive") {
        if (isset($_REQUEST["chkPkgType"]) && trim($_REQUEST["chkPkgType"]) == "Hourly") {
            $dDate = str_ireplace(",", "", $_REQUEST["pickdate"]);
            $dropDate = date_create($dDate . " " . $_REQUEST["tHourP"] . ":" . $_REQUEST["tMinP"] . ":00");
            $dropDate->modify("+" . intval($_REQUEST["nHours"]) . " hours");
            //echo "Drop Date (Hourly): " . $dropDate->format('H');
            //print_r($dropDate);
        } else {
            $dDate = str_ireplace(",", "", $_REQUEST["dropdate"]);
            $dropDate = date_create($dDate . " " . $_REQUEST["tHourD"] . ":" . $_REQUEST["tMinD"] . ":00");
            //echo "Drop Date (Daily): " . $dropDate->format('d M, Y H:i');
        }
    } else {
        $dDate = str_ireplace(",", "", $_REQUEST["dropdate"]);
        $dropDate = date_create($dDate);
    }
}
$tab = 1;
if (isset($_REQUEST["tab"])) {
    $tab = $_REQUEST["tab"];
}
if ($tab == 1) {
   
    $ll = "";
    $dll = "";
    if ($_REQUEST["hdTourtype"] == "Outstation") {
        if ($destName == 'Asola') {
            $destName = 'Asola-Delhi';
        }

        $travelData = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
        //$travelData = json_decode(file_get_contents('http://www.bytesbrick.com/app-test/cor/t.php?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
        $points = count($travelData->{'routes'}[0]->{'legs'});
        $ll = $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} . "," . $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'};
        $dll = ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lat'}) . "," . ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lng'});
        $totalKM = 0;
        for ($i = 0; $i < $points; $i++) {
            $dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
            if ($i == 0)
                $dwstate = $dispOrg;
            $dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
            if ($dwstate == "")
                $dwstate = $dispDest;
            else
                $dwstate .= " to:" . $dispDest;
            $totalKM += $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"value"};
        }
    }
    //$pickDate = date_create(date('Y-m-d', strtotime($pDate)));
    //$dropDate = date_create(date('Y-m-d', strtotime($dDate)));
    if ($_REQUEST["hdTourtype"] == "Outstation") {
        $interval = strtotime($dDate) - strtotime($pDate);
        $interval = ($interval / (60 * 60 * 24)) + 1;

        $intervalnight = strtotime($dDate) - strtotime($pDate);
        $intervalnight = ($intervalnight / (60 * 60 * 24));
    } else if ($_REQUEST["hdTourtype"] == "Selfdrive") {

        if ($_REQUEST['chkPkgType'] == 'Hourly') {
            if ($_REQUEST['nHoursJS']) {
                $interval = $_REQUEST['nHoursJS'];
            } else {
                $interval = $_REQUEST['nHours'];
            }
        } else {
            $isInHrs = 0;
            $cor = new COR();
            $pckDte = $pickDate->format('Y') . "-" . $pickDate->format('m') . "-" . $pickDate->format('d');
            $drpDte = $dropDate->format('Y') . "-" . $dropDate->format('m') . "-" . $dropDate->format('d');
            $drpTym = $_REQUEST["tHourD"] . $_REQUEST["tMinD"];
            $pckTym = $_REQUEST["tHourP"] . $_REQUEST["tMinP"];
            $res = $cor->_CORGetInterval(array("PickUpDate" => $pckDte, "DropOffDate" => $drpDte, "PickupTime" => $pckTym, "DropOffTime" => $drpTym, "CityId" => $orgID, "PkgType" => $_REQUEST["chkPkgType"]));
            $myXML = new CORXMLList();
            $arrUserId = $myXML->xml2ary($res->{'SelfDrive_GetTotalDurationResult'}->{'any'});
            $interval = $arrUserId[0]['Duration'];


        }
    } else {
        
       ////************ airport************/ 
           $airportddlOrigin=$_POST['airportddlOrigin'];
           $hdDestinationName=$_REQUEST['hdDestinationName'];
           $ddltermainal=$_POST['ddltermainal'];
           $picuploactionRecord=explode("|",$_POST['picuploactionid']);
           $picuploactionid=$picuploactionid[0];
        
        $dropDate = date_create(date('Y-m-d', strtotime($pDate)));
        $interval = 1;
        $interval = $_REQUEST["cabHour"];
    }
    if ($interval <= 0 && $intervalHrs < 2)
        header("Location: " . corWebRoot);
    // Search data capture by Iqbal 28Aug2013 STARTS
    $db = new MySqlConnection(CONNSTRING);
    $db->open();

    $isDiscountGiven = 0;
    $ip = $_SERVER["REMOTE_ADDR"];
    $ua = $_SERVER["HTTP_USER_AGENT"];
    $tType = $_REQUEST["hdTourtype"];
    $isSearched = 0;
    if (!isset($_COOKIE["CORCustID"])) {
        $CORCustID = strtoupper(uniqid("CCID-"));
        setcookie("CORCustID", $CORCustID, time() + 3600 * 24, "/");
    } else {
        $CORCustID = $_COOKIE["CORCustID"];
        $rs = $db->query("stored procedure", "cor_check_search_data('" . $CORCustID . "', '" . $orgID . "', '" . $destID . "', '" . $pickDate->format('Y-m-d') . "', '" . $interval . "', '" . $tType . "', 'COR')");
        $isSearched = $rs[0]["Total"];
    }
    if ($isSearched == 0) {
        $eTime = date("H:i:s");
        $searchData = array();
        $searchData["customer_id"] = $CORCustID;
        $searchData["origin_name"] = $orgName;
        $searchData["origin_code"] = $orgID;
        $searchData["destination_name"] = $destName;
        $searchData["destination_code"] = $destID;
        $searchData["pickup_date"] = $pickDate->format('Y-m-d');
        $searchData["duration"] = $interval;
        $searchData["distance"] = ceil(($totalKM / 1000));
        $searchData["ip"] = $ip;
        $searchData["user_agent"] = $ua;
        $searchData["entry_date"] = "CURDATE()";
        $searchData["entry_time"] = $eTime;
        $searchData["tour_type"] = $tType;
        $searchData["website"] = "COR";
        $searchData["rental_type"] = $_REQUEST["chkPkgType"];
        if (isset($_SESSION['last_search_id']) && isset($_SESSION['utm_source']) && isset($_SESSION['utm_medium']) && isset($_SESSION['utm_campaign'])) {
			$searchData["user_track"] = 'searchlist';
            $whereData["unique_id"] = $_SESSION['last_search_id'];
            $rs = $db->update("customer_search", $searchData, $whereData);
        } else {
            $searchData["user_track"] = 'searchlist';
            $rs = $db->insert("customer_search", $searchData);
            $last_search_id = $rs['lastinsertId'];
            $_SESSION['searchId'] = $rs['lastinsertId'];
        }
        unset($searchData);
    }
    $db->close();
    /* $eTime = date("H:i:s");
      $searchData = array();
      $searchData["customer_id"] = $CORCustID;
      $searchData["origin_name"] = $orgName;
      $searchData["origin_code"] = $orgID;
      $searchData["destination_name"] = $destName;
      $searchData["destination_code"] = $destID;
      $searchData["pickup_date"] = $pickDate->format('Y-m-d');
      $searchData["duration"] = $interval;
      $searchData["distance"] = ceil(($totalKM/1000));
      $searchData["ip"] = $ip;
      $searchData["user_agent"] = $ua;
      $searchData["entry_date"] = "CURDATE()";
      $searchData["entry_time"] = $eTime;
      $searchData["tour_type"] = $tType;
      $searchData["website"] = "COR";
      $searchData["rental_type"] = $_REQUEST["chkPkgType"];
      if($_REQUEST['userremark'] =="searchlist" && $isSearched == 0 && file_exists('searchxml/'.$_SESSION['xmlfilename'].'.xml') && isset($_SESSION['xmlfilename']))
      {
      $xml = simplexml_load_file('searchxml/'.$_SESSION['xmlfilename'].'.xml');
      $employee = $xml->addChild('searchlistdata');
      $employee->addChild('customer_id',$CORCustID);
      $employee->addChild('origin_name',$orgName);
      $employee->addChild('origin_code', $orgID);
      $employee->addChild('destination_name',$destName );
      $employee->addChild('destination_code',$destID );
      $employee->addChild('pickup_date', $pickDate->format('Y-m-d'));
      $employee->addChild('duration', $interval);
      $employee->addChild('distance',ceil(($totalKM/1000)));
      $employee->addChild('tour_type',$tType );
      $employee->addChild('rental_type',$_REQUEST["chkPkgType"]);
      file_put_contents('searchxml/'.$_SESSION['xmlfilename'].'.xml', $xml->asXML());
      }
      else if($isSearched == 0 && !file_exists('searchxml/'.$_SESSION['xmlfilename'].'.xml') && isset($_SESSION['xmlfilename']))
      {
      $rs = $db->insert("customer_search", $searchData);
      $last_search_id = $rs['lastinsertId'];
      $_SESSION['searchId'] = $rs['lastinsertId'];
      }
      else if($isSearched == 0 && !file_exists('searchxml/'.$_SESSION['xmlfilename'].'.xml') && !isset($_SESSION['xmlfilename']))
      {
      $rs = $db->insert("customer_search", $searchData);
      $last_search_id = $rs['lastinsertId'];
      $_SESSION['searchId'] = $rs['lastinsertId'];
      }
      unset($searchData);


      $db->close(); */
    // Search data capture by Iqbal 28Aug2013 ENDS
    $isDiscountShow = 0;
    if ($_REQUEST["hdTourtype"] == "Outstation") {
        if (ceil(($totalKM / 1000) / $interval) >= 150 && ceil(($totalKM / 1000) / $interval) <= 500) {
            $isDiscountShow = 1;
        }
    }
    //echo "KM: " . ($totalKM/1000) . "<br />Duration:" . $interval . "<br />Avg KM: " . ceil(($totalKM/1000) / $interval);
}

$totalKM = 0;
if ($tab == 4) {
   
    $fullname = explode(" ", trim($_REQUEST["name"]));
    if (count($fullname) > 1) {
        $firstName = $fullname[0];
        $lastName = $fullname[1];
    } else {
        $firstName = $fullname[0];
        $lastName = "";
    }
    $cor = new COR();
   
    $res = $cor->_CORCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
    
    

    $myXML = new CORXMLList();
    $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
    
    if (intval($arrUserId[0]["ClientCoIndivID"]) > 0) {

        $mtime = round(microtime(true) * 1000);
        $pkgId = $_REQUEST["hdPkgID"];
        $destination = $_REQUEST["hdDestinationName"];
        $TimeOfPickup = $_REQUEST["picktime"];
        $address = $_REQUEST["address"];
        $phone = $_REQUEST["monumber"];
        $emailId = $_REQUEST["email"];
        $userId = $_REQUEST["cciid"];
        $paymentAmount = $cor->decrypt($_REQUEST["totFare"]);
       
        $paymentType = "5";
        $paymentStatus = "0";
        $trackId = $_REQUEST["hdTrackID"];
        $transactionId = $_REQUEST["hdTransactionID"];
        $remarks = $_REQUEST["hdRemarks"];
        if($cor->decrypt($_REQUEST["hdDistance"])=="")
        {
         $totalKM=0;
        }
        else
        {
        $totalKM = $cor->decrypt($_REQUEST["hdDistance"]);
        }
        $visitedCities = $_REQUEST["hdDestinationName"];
        $pincode = $_REQUEST["pincode"];
        $gpAddress = $_REQUEST["gpaddress"];
        $dispc = $_REQUEST["discount"];
		
        $discountAmt = $cor->decrypt($_REQUEST["discountAmt"]);
        $empcode = $_REQUEST["empcode"];
		$empcode = !empty($_POST["empcode"])?$_POST["empcode"] : 0;
        if (trim($empcode) == "Promotion code")
            $empcode = "";
        $disccode = $_REQUEST["disccode"];
		$disccode = !empty($_POST["disccode"])?$_POST["disccode"] : 0;
        if (trim($disccode) == "Discount coupon number")
            $disccode = "";
        $interval = $_REQUEST["duration"];
        $outStationYN = "true";
        if (isset($_COOKIE["corsrc"]))
            $srcCookie = $_COOKIE["corsrc"]; //Aamir 1-Aug-2012
        if (isset($_COOKIE["gclid"]))
            $srcCookie = $_COOKIE["gclid"]; //Iqbal 10-Oct-2012
        $tourType = $_REQUEST["hdTourtype"];
        if ($_REQUEST["hdTourtype"] == "Outstation")
            $outStationYN = "true";
        else
            $outStationYN = "false";
        if (isset($_REQUEST["hdPickdate"])) {
            $pDate = $_REQUEST["hdPickdate"];
            $pickDate = date_create($pDate);
        }
        if (isset($_REQUEST["hdDropdate"])) {
            $dDate = $_REQUEST["hdDropdate"];
            $dropDate = date_create($dDate);
        }
        $dateOut = $pickDate->format('m/d/Y');
        $dateIn = $dropDate->format('m/d/Y');

        session_start();
        $corArrSTD = array();
        $corArrSTD["pkgId"] = $pkgId;
        $corArrSTD["destination"] = $destination;
        $corArrSTD["dateOut"] = $dateOut;
        $corArrSTD["dateIn"] = $dateIn;
        $corArrSTD["TimeOfPickup"] = $TimeOfPickup;
        $corArrSTD["address"] = $address;
        $corArrSTD["firstName"] = $firstName;
        $corArrSTD["lastName"] = $lastName;
        $corArrSTD["phone"] = $phone;
        $corArrSTD["emailId"] = $emailId;
        $corArrSTD["userId"] = $userId;
        $corArrSTD["paymentAmount"] = $paymentAmount;
        $corArrSTD["paymentType"] = $paymentType;
        $corArrSTD["paymentStatus"] = $paymentStatus;
        $corArrSTD["trackId"] = $trackId;
        $corArrSTD["transactionId"] = $transactionId;
        $corArrSTD["discountPc"] = $dispc;
        $corArrSTD["discountAmount"] = $discountAmt;
        $corArrSTD["remarks"] = $remarks;
        $corArrSTD["totalKM"] = intval($totalKM);
        $corArrSTD["visitedCities"] = $visitedCities;
        $corArrSTD["outStationYN"] = $outStationYN;
         if($tourType == 'airport')
        {
           $corArrSTD["CustomPkgYN"] = "true";   
        }
        else
        {
         $corArrSTD["CustomPkgYN"] = "false";
        }
		/******* new change *****/
		if($_POST["refclient"]!='')
		{
		$corArrSTD["OriginCode"] = $cor->decrypt($_POST["refclient"]);
		}
		else
		{
		$corArrSTD["OriginCode"] = "COR";
	    }     
		
		
		
        $corArrSTD["chkSum"] = "0";
        $corArrSTD["DisCountCode"] = $disccode; 
        $corArrSTD["PromotionCode"] = $empcode; 
        $corArrSTD["IsPayBack"] = $_SESSION["IsPayBack"];
        $corArrSTD["discountTrancastionID"] = $disccode;
        $corArrSTD["latitude"] = $_REQUEST["city_location_lat"];
        $corArrSTD["longitude"] = $_REQUEST["city_location_long"];
		
        $myXML = new CORXMLList();
        if($tourType == 'airport')
        {
            list($terminalid,$cityid) = explode("|",$_POST["terminalid"]);
           
            $corArrSTD["TerminalID"] =  $terminalid;
            $corArrSTD["TerminalLocationID"] = $_REQUEST["hdDestinationID"];
            
            
            $res = $cor->_CORMakeBookingWithLatLongAirport($corArrSTD);
            $valResult=$res->{'CreateAirport_BookingResult'}->{'any'};
  
  
        }
        else
        {
			
		$corArrSTD["IndicatedCGSTTaxPercent"] = $_REQUEST['CGSTPercent'];
		$corArrSTD["IndicatedSGSTTaxPercent"] = $_REQUEST['SGSTPercent'];
		$corArrSTD["IndicatedIGSTTaxPercent"] = $_REQUEST['IGSTPercent'];
		$corArrSTD["GSTEnabledYN"] = $_REQUEST['GSTEnabledYN'];
		$corArrSTD["IndicatedClientGSTId"] = $_REQUEST['ClientGSTId'];
		
            // echo '<pre>';print_r($corArrSTD);die;
            $res = $cor->_CORMakeBookingWithLatLong($corArrSTD);
            $valResult=$res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'};
        }
          
		 
		  
            if ($valResult != "") {
            $arrUserId = $myXML->xml2ary($valResult);
            if ($arrUserId[0]["Column1"] > 0) {
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $whereData = array();
                $dataToSave = array();
                $whereData["coric"] = $transactionId;
                $dataToSave["payment_mode"] = "2";
                $dataToSave["payment_status"] = "1";
                $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
                $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                unset($whereData);
                unset($dataToSave);
                if ($disccode != "") {
                    $whereData = array();
                    $whereData["voucher_number"] = $disccode;
                    $dataToSave = array();
                    $dataToSave["is_booked"] = "1";
                    $r = $db->update("dicount_vouchers_master", $dataToSave, $whereData);
                    unset($whereData);
                    unset($dataToSave);
                }
                $db->close();

                if ($_REQUEST['hdTourtype'] != "Local" )
                    $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_REQUEST["hdOriginName"])) . "-to-" . str_ireplace(" ", "-", strtolower($_REQUEST["hdDestinationName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                else
                    $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_REQUEST["hdOriginName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                $xmlToSave .= "<cortrans>";
                $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                $xmlToSave .= "</cortrans>";
                if (!is_dir("./xml/trans/" . date('Y')))
                    mkdir("./xml/trans/" . date('Y'), 0775);
                if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
                    mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
                if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
                    mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
                if (isset($transactionId))
                    createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $transactionId . "-B.xml");
            }
        } else
            $tab = 4;
    } else
        $tab = 4;

    
    ?>
    
    <script type="text/javascript">
        location.href = "<?php echo $url; ?>";
    </script>

    <?php
    
}
if ($tab == 3) {
    if (isset($_COOKIE["cciid"]) && isset($_COOKIE["emailid"])) {
        if ($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != "") {
            $tab = 3;
        }
    } else {
        $cor = new COR();
        if ($_REQUEST["corpassword"] == "") {

            if ($_REQUEST['hdTourtype'] == "Selfdrive") {
                $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
            } else {
                $res = $cor->_CORCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
            }

            //$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_REQUEST["monumber"],"clientid"=>2205));
            $myXML = new CORXMLList();
            $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
            if (key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0) {
                //$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
                $tab = 2;
            } else {
                //echo $arrUserId[0]["ClientCoIndivID"];
                setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                $tab = 3;
                //$url = "./search-result.php?" . ereg_replace( ' +', '%20', $query_string);
            }
            unset($myXML);
        } else {
            if (isset($_REQUEST["isExistUser"])) {
                if ($_REQUEST["isExistUser"] == "1") {

                    if ($_REQUEST['hdTourtype'] == "Selfdrive") {
                        $res = $cor->_CORSelfDriveCheckLogin(array("PhoneNo" => $_REQUEST["monumber"], "password" => $_REQUEST["corpassword"], "clientid" => 2205));
                    } else {
                        $res = $cor->_CORCheckLogin(array("PhoneNo" => $_REQUEST["monumber"], "password" => $_REQUEST["corpassword"], "clientid" => 2205));
                    }

                    //$res = $cor->_CORCheckLogin(array("PhoneNo"=>$_REQUEST["monumber"],"password"=>$_REQUEST["corpassword"],"clientid"=>2205));
                    $myXML = new CORXMLList();
                    if ($res->{'verifyLoginResult'}->{'any'} != "") {
                        $arrUserId = $myXML->xml2ary($res->{'verifyLoginResult'}->{'any'});
                        if ($arrUserId[0]["ClientCoIndivID"] > 0) {
                            setcookie("cciid", $arrUserId[0]["ClientCoIndivID"]);
                            setcookie("emailid", $arrUserId[0]["EmailID"]);
                            setcookie("phone1", $arrUserId[0]["Phone1"]);
                            
                            $tab = 3;
                        }
                    } else {
                       
                        $tab = 2;
                    }
                    unset($myXML);
                }
            } else {
                if ($_REQUEST['hdTourtype'] == "Selfdrive") {
                    $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
                } else {
                    $res = $cor->_CORCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
                }
              
                $myXML = new CORXMLList();
                $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
                if (key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0) {
                    
                    $tab = 2;
                } else {
                    setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                    $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                   
                    $tab = 3;
                }
                unset($myXML);
            }
        }
        session_start();
      
        $fullname = explode(" ", $_REQUEST["name"]);
        if (count($fullname) > 1) {
            $fname = $fullname[0];
            $lname = $fullname[1];
        } else {
            $fname = $fullname[0];
            $lname = "";
        }
        //$cor = new COR();

        if ($_REQUEST['hdTourtype'] == "Selfdrive") {
            $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
        } else {
            $res = $cor->_CORCheckPhone(array("PhoneNo" => $_REQUEST["monumber"], "clientid" => 2205));
        }

        //$res = $cor->_CORCheckPhone(array("PhoneNo"=>$_REQUEST["monumber"],"clientid"=>2205));
        $myXML = new CORXMLList();
        $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
        //print_r($res);
        if (key_exists("Column1", $arrUserId[0]) && intval($arrUserId[0]["Column1"]) == 0) {
            if (isset($_REQUEST["corpassword"]))
                $pword = $_REQUEST["corpassword"];
            else
                $pword = "retail@2012";
            //echo '<pre>';
            //print_r($_REQUEST);
            //echo '$tourType = '.$tourType
            if ($_REQUEST['hdTourtype'] == "Selfdrive") {
                $res = $cor->_CORSelfDriveRegister(array("fname" => $fname, "lname" => $lname, "PhoneNumber" => $_REQUEST["monumber"], "emailId" => $_REQUEST["email"], "password" => $pword));
            } else {
                $res = $cor->_CORRegister(array("fname" => strip_tags($fname), "lname" => strip_tags($lname), "PhoneNumber" => strip_tags($_REQUEST["monumber"]), "emailId" => strip_tags($_REQUEST["email"]), "password" => strip_tags($pword)));
            }
            //$res = $cor->_CORRegister(array("fname"=>$fname, "lname"=>$lname, "PhoneNumber"=>$_REQUEST["monumber"], "emailId"=>$_REQUEST["email"], "password"=>$pword));
            $arrUserId = $myXML->xml2ary($res->{'RegistrationResult'}->{'any'});
            if (key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0) {
                setcookie("tcciid", $arrUserId[0]["Column1"]);
                $tCCIID = trim($arrUserId[0]["Column1"]);
                //$url = "search-result.php?" . $query_string;
                $tab = 3;
            }
        } else {
            if (intval($arrUserId[0]["ClientCoIndivID"]) > 0) {
                setcookie("tcciid", $arrUserId[0]["ClientCoIndivID"]);
                $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                $tab = 3;
            }
        }
        unset($cor);
    }
}
if ($_REQUEST["hdTourtype"] != "Selfdrive" && $_REQUEST["hdTourtype"] != "Outstation" && $_REQUEST["hdTourtype"] != "Local" && $_REQUEST["hdTourtype"] != "airport")
    header("Location: " . corWebRoot);
else
    $tourType = $_REQUEST["hdTourtype"];
//echo $tab;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if (trim($orgName) != "") {
    ?>
            <title>Car Rental <?php echo $orgName; ?>, Car Hire <?php echo $orgName; ?> at Carzonrent.com</title>
            <meta name="description" content="Carzonrent.com - No. 1 car rental company <?php echo $orgName; ?> provides car hire, cab booking services for <?php echo $orgName; ?> and other various cities of India." />
            <meta name="keywords" content="car rental <?php echo strtolower($orgName); ?>, car hire <?php echo strtolower($orgName); ?>" />
    <?php
} else {
    ?>
            <title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
            <meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers book a cab or rent a car for airport transfer car rentals, online economy car rentals/car hiring and hertz car rental solutions." />
            <meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental india, car rental company india, budget/executive car rentals, economy car rental services, car hire services india, airport transfer car rental services, luxury cars on rent" />	
    <?php
}
?>
        <script type="text/javascript">
            var arrAddrs = new Array();
        </script>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<?php
if ($tab == 3) {
    ?>
            <script src="http://www.s2d6.com/js/globalpixel.js?x=sp&a=298&h=67761&o=<?php echo $_REQUEST["monumber"]; ?>&g=registration&s=0.00&q=1"></script>
    <?php
}
?>
        <style>
            .float{
                position:absolute;right:0px;bottom:0px;width:200px;height:200px;background-Color:red;
            }
            .qm{
                background:url(<?php echo corWebRoot; ?>/images/yellow.png) no-repeat;
                width:25px;height:22px;color:#615000;font-weight:bold;display:block;float:left;text-align:center;text-decoration:none;
                padding:3px 0px 0px 0px;
                margin:-4px 0px 0px 5px;
                font-size:15px;
            }
            .qm:hover{
                text-decoration:none;
            }
            .currentTab {
                background-color: #fff !important;
                border-bottom: 1px solid;
                color: #000 !important;
            }

            #divMS #hourlyBlock select.fromdd2{
                min-width:320px \0/IE9 !important;
            }
            .tbbinginr .picktime {
                line-height:18px \0/IE9 !important;
            }
        </style>
<?php if ($tourType == "Outstation" || $tourType == "Local") { ?>
            <style>
                /*.menu ul li.tfn strong {color: #F2C900 !important;}*/
                #feedbackpopup {background-image: url("<?php echo corWebRoot; ?>/images/feedback.png") !important;}
                .showtime, .showhour {
                    background: none repeat scroll 0 0 #FAEAA3 !important;
                    border: 1px solid #EFC14D !important;
                    border-radius: 4px 4px 4px 4px;
                    color: #D68300 !important;
                }
                #registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.jpg") no-repeat scroll 0 0 transparent !important;}
            </style>
<?php } elseif ($tourType == "Selfdrive") {
    ?>
            <style>
                .confirmbooking a {background: url("<?php echo corWebRoot; ?>/images/confirmbooking-myles.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
                }
                .qm{background:url("<?php echo corWebRoot; ?>/images/red.png") no-repeat  scroll 0 0 rgba(0, 0, 0, 0) !important;color:#fff !important;}
                .innerpages .rightside .tpdiv li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll left 8px rgba(0, 0, 0, 0) !important;}
                .confirmbookingredeem a {background: url("<?php echo corWebRoot; ?>/images/confirmbookingredeem-r.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
                }
                .confirmbookingredeem a:hover {background: url("<?php echo corWebRoot; ?>/images/confirmbookingredeem-r.png") no-repeat scroll 0 -52px rgba(0, 0, 0, 0);
                }
                .innerpages .rightside .tpdiv h3 {color: #db4626 !important;}
                #feedbackpopup {background-image: url("<?php echo corWebRoot; ?>/images/feedback-r.jpg") !important;}
                ul.listarrow li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg")  no-repeat 30px 12px; !important;}
                .payandbook a {background: url("<?php echo corWebRoot; ?>/images/payandbook-r.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;}
                .middiv ul li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll 0 8px rgba(0, 0, 0, 0) !important;}
                #registration{border: 10px solid #db4626 !important;}
                .details_wrapper ul li{background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat 0px 5px rgba(0, 0, 0, 0) !important;}
                ul.shadetabst {background: url("<?php echo corWebRoot; ?>/images/myles-tb.jpg") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
                </style>
    <?php
}
?>
            <script type="text/javascript">
            arrDestination = new Array();
<?php
for ($i = 0; $i < count($des); $i++) {
    ?>
                arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
            <?php
        }
        ?>
                mc = 1;
                if (typeof String.prototype.startsWith != 'function') {
                    String.prototype.startsWith = function (str) {
                        return this.indexOf(str) == 0;
                    };
                }
            </script>
            <script type="text/javascript">
                $(function () {
                    $('select.time').customSelect();
                    $('select.area').customSelect();
                    $('select.styled').customSelect();
                })

                function resgiter_cont() {
                    document.getElementById("register").style.display = 'none';
                    document.getElementById("travel_details").style.display = 'block';
                }
            </script>
            <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/newjs/jquery.datetimepicker.css"/>
            <script type="text/javascript" src="<?php echo corWebRoot; ?>/newjs/jquery.js"></script>
            <script type="text/javascript" src="<?php echo corWebRoot; ?>/newjs/jquery.datetimepicker.js"></script>
            <script type="text/javascript">
                jQuery.noConflict();
                jQuery(document).ready(function () {
                    jQuery('#inputFieldSF1').datetimepicker({
                        format: 'd M, Y H:i',
                        minDate: '<?php echo date('d M, Y'); ?>',
                        step: 30,
                        roundTime: 'ceil'
                    });
                    jQuery('#inputFieldSF2').datetimepicker({
                        format: 'd M, Y H:i',
                        minDate: '<?php echo date('d M, Y'); ?>',
                        step: 30
                    });
                });
            </script>
            <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/validateSFMS.js?v=<?php echo mktime(); ?>"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css?v=<?php echo mktime(); ?>" />
        </head>
        <body>
<?php include_once("./includes/header-with-service.php"); ?>
            <!--Three tab header starts -->
            <div class="tbbingouter">
                <div class="main">
<?php if ($tab == 1) { ?>
                        <ul id="countrytabs" class="shadetabst" style="background-position:0 -33px;">
<?php } else if ($tab == 2) { ?>
                            <ul id="countrytabs" class="shadetabst" style="background-position:0 -66px;">
<?php } else if ($tab == 3) { ?>
                                <ul id="countrytabs" class="shadetabst" style="background-position:0 -99px;">
        <?php } else { ?>
                                    <ul id="countrytabs" class="shadetabst">
<?php } ?>
                                    <li class="step1"><a href="javascript:void(0);" id="menuTab1" rel="country1" style="cursor:default;" class="selected">&nbsp;</a></li>
                                    <li class="step2"><a href="javascript:void(0);" id="menuTab2" rel="country2" style="cursor:default;">&nbsp;</a></li>
                                    <li class="step3"><a href="javascript:void(0);" id="menuTab3" rel="country3" style="cursor:default;">&nbsp;</a></li>
                                </ul>
<?php
if ($tourType == "Selfdrive" && $tab == 1) {
    ?>
                                    <div class="wd136" style="width:auto;margin: 0 21px 0 0;float: right;"><a href="javascript:void(0);" onclick="javascript: _modifySrch('divMS');"><img src="<?php echo corWebRoot; ?>/images/modify-search.jpg" alt="Modify Search" title="Modify Search" /></a></div>
            <?php
        }
        ?>
                                </div>
                                </div>
                                <!--Three tab header ends -->

<?php
if ($tab == 1) {
    if ($tourType == "Selfdrive") {
        include_once("./includes/selfdrive-list.php");
    } elseif ($tourType == "Outstation") {
        include_once("./includes/outstation-list.php");
    } elseif ($tourType == "Local") {
        include_once("./includes/local-list.php");
	} elseif ($tourType == "airport") {
		include_once("./includes/airport-list.php");
    }
	
}
if ($tab == 2) {
    if ($tourType == "Selfdrive") {
        include_once("./includes/selfdrive-contact.php");
    } elseif ($tourType == "Outstation") {
        include_once("./includes/outstation-contact.php");
    } elseif ($tourType == "Local") {
        include_once("./includes/local-contact.php");
    }
	 elseif ($tourType == "airport") {
        include_once("./includes/airport-contact.php");
    }
	
	
	
}
if ($tab == 3) {
    if ($tourType == "Selfdrive") {
        include_once("./includes/selfdrive-payment.php");
    } elseif ($tourType == "Outstation") {
        include_once("./includes/outstation-payment.php");
    } elseif ($tourType == "Local") {
        include_once("./includes/local-payment.php");
    }
	 elseif ($tourType == "airport") {
        include_once("./includes/airport-payment.php");
    }
}


if ($tourType == "Selfdrive") {
    ?>

                                    <script>
                                        function troggleopenhelpNew()
                                        {

                                            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                            var regexpch = /^[a-zA-Z]+$/;
                                            var txtname = $("#txthelpname").val();
                                            var txtnameval = document.getElementById("txthelpname");
                                            var querytype = $("#querytype option:selected").val();
                                            var monumber = $("#mohelpnumber").val();
                                            var query = $("#queryhelp").val();
                                            var email = $("#helpemail").val();
                                            if ($("#txthelpname").val().trim() == "")
                                            {
                                                alert("Please Enter name");
                                                $("#txthelpname").focus();
                                                return  false;
                                            }
                                            if (!txtnameval.value.match(regexpch))
                                            {
                                                alert("Please Enter character only");
                                                $("#txtname").focus();
                                                return false

                                            }

                                            if (monumber == "")
                                            {
                                                alert("Please Enter mobiile no");
                                                $("#mohelpnumber").focus();
                                                return  false;
                                            }

                                            if (monumber.substr(0, 1) != "9" && monumber.substr(0, 1) != "8" && monumber.substr(0, 1) != "7")
                                            {
                                                alert("Invalid mobile number. Please enter correct mobile number.");
                                                $("#mohelpnumber").focus();
                                                return  false;
                                            }
                                            if (monumber)
                                            {
                                                if (monumber.length != 10)
                                                {
                                                    alert("Mobile number must be of 10 digit.");
                                                    $("#mohelpnumber").focus();
                                                    return  false;
                                                }
                                            }


                                            var dataString = 'txtname=' + txtname + '&email=' + email + '&monumber=' + monumber + '&query=' + query + '&querytype=' + querytype;

                                            $.ajax({type: "POST", url: "<?php echo corWebRoot; ?>/helpenquiry.php",
                                                data: dataString, success: function (response) {
                                                    var txt = response;
                                                    $('#loader4').hide();
                                                    var result = txt;
                                                    var data = 1;
                                                    if (result == 1)
                                                    {
                                                        $(".helppopup").css("display", "none")

                                                        //$("div#animatediv").hide();
                                                        $(".closeA").hide();
                                                        alert("Thank you for your valued enquiry, will get back to you shortly");
                                                    }

                                                }});


                                        }


                                        $(document).ready(function () {
                                            setTimeout(popupDataAnimate, 25000);
                                            function popupDataAnimate() {
                                                $("#radiusBox").css("display", "block").animate({bottom: '0px'});
                                                $(".closeA").show();
                                                $('.closeA').click(function () {
                                                    $("#radiusBox").hide();
                                                    $(".closeA").hide();
                                                });
                                            }
                                        });
                                    </script>
                                    <script type="text/javascript">
                                        _changeSFPkgTypeMS = function (t)
                                        {   //alert(t);
                                            if (t == "Hourly")
                                            {
                                                //document.getElementById('hourly').className = 'sel';
                                                document.getElementById('chkPkgTypeMS').value = 'Hourly';
                                                document.getElementById('hourlyBlock').style.display = 'block';
                                                document.getElementById('dailyBlock').style.display = 'none';
                                                document.getElementById('weeklyBlock').style.display = 'none';
                                                document.getElementById('monthlyBlock').style.display = 'none';
                                                _setDropDateMylesMS('inputFieldSF1', 'inputFieldSF2');
                                            }
                                            if (t == "Daily")
                                            {
                                                document.getElementById('chkPkgTypeMS').value = 'Daily';
                                                document.getElementById('dailyBlock').style.display = 'block';
                                                //document.getElementById('inputFieldSF2').value = ''; 
                                                document.getElementById('hourlyBlock').style.display = 'none';
                                                document.getElementById('weeklyBlock').style.display = 'none';
                                                document.getElementById('monthlyBlock').style.display = 'none';
                                                _setDropDateMylesMS('inputFieldSF1', 'inputFieldSF2');

                                            }
                                            if (t == "Monthly")
                                            {
                                                document.getElementById('chkPkgTypeMS').value = 'Monthly';
                                                document.getElementById('dailyBlock').style.display = 'none';
                                                document.getElementById('hourlyBlock').style.display = 'none';
                                                document.getElementById('weeklyBlock').style.display = 'none';
                                                document.getElementById('monthlyBlock').style.display = 'block';
                                                _setdropdatesmodify('month', 'inputFieldSF1', 'inputFieldSF2');
                                            }
                                            if (t == "Weekly")
                                            {
                                                document.getElementById('chkPkgTypeMS').value = 'Weekly';
                                                document.getElementById('dailyBlock').style.display = 'none';
                                                document.getElementById('hourlyBlock').style.display = 'none';
                                                document.getElementById('weeklyBlock').style.display = 'block';
                                                document.getElementById('monthlyBlock').style.display = 'none';
                                                _setdropdatesmodify('week', 'inputFieldSF1', 'inputFieldSF2');
                                            }

                                        }
                                    </script>
                                    <!-- 
                                    <div id="radiusBox" class='helppopup' style="display:none;">
                                    <h1 class="helpAnimation">Should we help you?</h1>
                                    <h1 class="bgh1">Share your details below for us to call you back.</h1>
                                    <div id='animatediv'>
                                    <div class="closeA" style="display:none;"><span>X</span></div>
                                    <form name="from1" method="post" action="">
                                     <label for="name" class="mr2">Name :</label>
                                     <input type="text" name="txtname" id='txthelpname' value="">
                                     <div class="clr"> </div>
                                     <label for="mobile" class="mr2">Mobile :</label>
                                     <input type="text" name="monumber" id='mohelpnumber' value="" maxlength="10">
                                     <div class="clr"> </div>
                                     <label for="email" class="mr2">Emal ID :</label>
                                     <input type="text" name="textemailData" id='helpemail' value="">
                                     <div class="clr"> </div>
                                    
                                     <label for="query" class="mr2 query" style="vertical-align:top;">Query :</label>
                                     <textarea name="query" id="queryhelp"></textarea>
                                     
                                     <label for="name" class="mr2 hidden">hidden</label>
                                     
                                     <button class="radiusB" type="button" onclick="return troggleopenhelpNew();" >Submit</button>
                                     
                                     <img class="help_right" src="<?php echo corWebRoot; ?>/myles-campaign/images/enqButton.png" border="0" onclick="return troggleopenhelpNew();"/>
                                     </form>
                                    </div>
                                    </div>
                                    -->
    <?php
}
?>
<?php include_once("./includes/footer.php"); ?>
<?php
if (isset($_REQUEST['hdCarModel']) && !empty($_REQUEST['hdCarModel'])) {
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $sql = "select * from cor_seoitem where city = " . $_REQUEST['hdOriginID'] . " and model = " . $_REQUEST['hdCarModelID'];
    $data = '';
    $r = $db->query("query", $sql);
    if (count($r) > 0 && (!array_key_exists("response", $r))) {
        foreach ($r as $key => $val) {
            ?>
                                            <script>
                                                var google_tag_params = {
                                                    dynx_itemid: '<?php echo $_REQUEST['hdOriginName']; ?>',
                                                    dynx_itemid2: '<?php echo $_REQUEST['hdCarModel']; ?>',
                                                    dynx_pagetype: '<?php echo $val['pagetype']; ?>',
                                                    dynx_totalvalue: '<?php echo $val['totalvalue']; ?>',
                                                };
                                            </script>
            <?php
        }
    }
}
?>
                                </body>
                                </html>
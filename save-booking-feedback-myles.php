<?php
	session_start();
	error_reporting(0);
	include_once('./classes/cor.mysql.class.php'); 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once('./classes/cor.gp.class.php');
	include_once("./includes/cache-func.php");
	$rootpath=corWebRoot;
	ini_set("display_errors", 1);
	if($_SESSION["capcha"] == $_POST["capcha"])
	{

	$invoker = $_SERVER['HTTP_REFERER'];
	$isExist = strrpos($invoker,$rootpath);
	
	$allowed = "no";
	if(isset($_POST['bookingid']) && $_POST['bookingid'] != "" && $_POST['hdAllowFormSubmit'] == 'no')
		$allowed = "yes";
	if(isset($_POST["txtname"]) && $_POST["txtname"] != "" && isset($_POST["monumber"]) && $_POST["monumber"] != "" && isset($_POST["email"]) && $_POST["email"] != ""  && $_POST['hdAllowFormSubmit'] == 'no')
		$allowed = "yes";

	//echo $allowed;
	//if($isExist > 0 && $allowed == "yes") old
	if($isExist > 0 && $allowed == "yes")
	//if(isset($_POST['feedbacksubmit']) && $isExist > 0 && $allowed == "yes")
	{
	
		$db = new MySqlConnection(CONNSTRING);
		$dataToSave = array();
		$dataToMail= array();
		$db->open();
		$cor = new COR();
		$dataToSave["purpose_of_trip"] = $_POST['ddlpurpose'];
		$dataToSave["other_purpose"] = $_POST['txtOtherPurpose'];
		$dataToSave["reason_1"] = $_POST['satisfied1'];
		$dataToSave["reason_2"] = $_POST['satisfied2'];
		$dataToSave["reason_3"] = $_POST['satisfied3'];
		$dataToSave["reason_4"] = $_POST['satisfied4'];
		$dataToSave["reason_5"] = $_POST['satisfied5'];
		$dataToSave["reason_6"] = $_POST['satisfied6'];
		$dataToSave["reason_7"] = $_POST['satisfied7'];
		$dataToSave["other_reason"] = $_POST['txtOtherReason'];
		$dataToSave["experience"] = $_POST['rd'];
		$dataToSave["experience_1"] = $_POST['chkwwr1'];
		$dataToSave["experience_2"] = $_POST['chkwwr2'];
		$dataToSave["experience_3"] = $_POST['chkwwr3'];
		$dataToSave["experience_4"] = $_POST['chkwwr4'];
		$dataToSave["experience_5"] = $_POST['chkwwr5'];
		$dataToSave["experience_6"] = $_POST['chkwwr6'];
		$dataToSave["experience_7"] = $_POST['chkwwr7'];
		$dataToSave["other_experience"] = $_POST['txtWrong'];
		$dataToSave["feedback"] = $_POST['txtfeedback'];
		$dataToSave["entry_date"] = date('Y-m-d H:i:s');
		$dataToSave["ip"] = $_SERVER["REMOTE_ADDR"];
		$dataToSave["ua"] = $_SERVER["HTTP_USER_AGENT"];

		$tourtype = $_POST['hdIsSelfDrive'];
		if(isset($_POST['bookingid']) && $_POST['bookingid'] != "")
		{
			$bookingID = $_POST['bookingid'];
			$myXML = new CORXMLList();

			$res = $cor->_CORGetBookingsDetail(array("BookingID"=>$bookingID));
			$bookingDetails = array();
			if($res->{'GetBookingDetailResult'}->{'any'} != "")
			{
				$myXML = new CORXMLList();
				$bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});

				$pickDate = date_create($bookingDetails[0]["PickUpDate"]);
				$pickDate->format("Y-m-d");
				$dropDate = date_create($bookingDetails[0]["DropOffDate"]);
				$dropDate->format("Y-m-d");
				$dataToSave["booking_id"] = $bookingID;

				$dataToSave["CarCatName"] = $bookingDetails[0]["CarCatName"];
				$dataToSave["destination"] = $bookingDetails[0]["destination"];
				$dataToSave["PickUpTime"] = $bookingDetails[0]["PickUpTime"]; 
				$dataToSave["PickUpAdd"] = $bookingDetails[0]["PickUpAdd"]; 
				$dataToSave["Fname"] = $bookingDetails[0]["Fname"];
				$dataToSave["Lname"] = $bookingDetails[0]["Lname"];
				$dataToSave["mobile"] = $bookingDetails[0]["mobile"];
				$dataToSave["EmailID"] = $bookingDetails[0]["EmailID"];
				$dataToSave["BookingSource"] = $bookingDetails[0]["BookingSource"];
				$dataToSave["userid"] = $bookingDetails[0]["userid"];
				$dataToSave["PaymentStatus"] = $bookingDetails[0]["PaymentStatus"];
				$dataToSave["PaymentAmount"] = $bookingDetails[0]["PaymentAmount"];
				$dataToSave["trackID"] = $bookingDetails[0]["trackID"];
				$dataToSave["VisitedCities"] = $bookingDetails[0]["VisitedCities"];
				$dataToSave["OutstationYN"] = $bookingDetails[0]["OutstationYN"];
				$dataToSave["Remarks"] = $bookingDetails[0]["Remarks"];
				$dataToSave["IndicatedDiscPC"] = $bookingDetails[0]["IndicatedDiscPC"];
				$dataToSave["PickUpDate"] = $pickDate->format("Y-m-d");
				$dataToSave["DropOffDate"] = $dropDate->format("Y-m-d");
				$dataToSave["OriginName"] = $bookingDetails[0]["OriginName"];
				$dataToSave["originId"] = $bookingDetails[0]["originId"];
				$dataToSave["Status"] = $bookingDetails[0]["Status"];
				$dataToSave["TourType"] = $bookingDetails[0]["TourType"];
				$dataToSave["IndicatedPkgID"] = $bookingDetails[0]["IndicatedPkgID"];
				$dataToSave["DiscountAmount"] = $bookingDetails[0]["DiscountAmount"];
				$dataToSave["ChauffeurCharges"] = $bookingDetails[0]["ChauffeurCharges"];
				$dataToSave["DropOffTime"] = $bookingDetails[0]["DropOffTime"];
				$dataToSave["serviceAmount"] = $bookingDetails[0]["serviceAmount"];
				$dataToSave["PayBackMember"] = $bookingDetails[0]["PayBackMember"];
				$dataToSave["subLocationID"] = $bookingDetails[0]["subLocationID"];
			}
		}
		else
		{
			$dataToSave["Fname"] = $_POST["txtname"];
			$dataToSave["Lname"] = "";
			$dataToSave["mobile"] = $_POST["monumber"];
			$dataToSave["EmailID"] = $_POST["email"];
		}
		if($tourtype == 0)
		{
			$dataToSave["convenient_booking_process"] = $_POST['rdo1'];
			$dataToSave["cab_report_time"] = $_POST['rdo2'];
			$dataToSave["chauffer_experience"] = $_POST['rdo3'];
			$dataToSave["expectation_met"] = $_POST['rdo4'];
			$dataToSave["overall_experience"] = $_POST['rdo5'];
		}

		
	
		
		$r = $db->insert("myles_feedback", $dataToSave);

		
		$sql = "SELECT * FROM cor_booking_new WHERE booking_id= " . $_POST['bookingid'];
		$r = $db->query("query", $sql);
		if(!array_key_exists("response", $r)){
			$html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
			$html .= "<tr>";
			$html .= "<td>Name</td>";
			$html .= "<td>" . $r[0]["full_name"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Email</td>";
			$html .= "<td>" . $r[0]["email_id"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Mobile</td>";
			$html .= "<td>" . $r[0]["mobile"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Tour Type</td>";
			$html .= "<td>" . $_POST["TourType"] . "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Origin</td>";
			$html .= "<td>" . $_POST["OriginName"] . "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Destination</td>";
			$html .= "<td>" . $_POST["destination"] . "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>IP</td>";
			$html .= "<td>" . $r[0]["ip"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Pickup Date</td>";
			$html .= "<td>" . $_POST["PickUpDate"] . "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Source Website</td>";
			$html .= "<td>www.carzonrent.com</td>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		else
		{
			$html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
			$html .= "<tr>";
			$html .= "<td>Name</td>";
			$html .= "<td>" . $_POST["txtname"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Email</td>";
			$html .= "<td>" . $_POST["email"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Mobile</td>";
			$html .= "<td>" . $_POST["monumber"]. "</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td>Source Website</td>";
			$html .= "<td>www.carzonrent.com</td>";
			$html .= "</tr>";
			$html .= "</table>";
		}
		//echo $html;
		$dataToMail["To"] = "trips@carzonrent.com;ajay.anand@carzonrent.com;abhishek.bhaskar@carzonrent.com;vinod.maurya@carzonrent.com;vijay.sharma@carzonrent.com";
		//$dataToMail["To"] = "vinod.maurya@carzonrent.com";
		$dataToMail["Subject"] = "Booking Feedback";
		$dataToMail["MailBody"] = $html;
		$res = $cor->_CORSendMail($dataToMail);

		unset($cor);
		unset($dataToMail);
		$db->close();
		session_destroy();
		unset($dataToSave);
		$root=corWebRoot;
		header("Location:".$rootpath."/booking-feedback-thanks.php");
	}
	}
	else
		header("Location: ".$rootpath."/myles");
?>
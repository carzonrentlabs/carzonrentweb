<?php
   /******
    Added: vinod K Maurya
	Desc: MObile app payment gateway integration
	Dated:  17-10-2015
    ***/
	session_start();
	set_include_path('./lib'.PATH_SEPARATOR.get_include_path());
	require_once 'Zend/Crypt/Hmac.php';
	include_once("./includes/cache-func.php");
	include_once('./classes/cor.mysql.class.php');
	include_once('./classes/cor.ws.class.php');
	include_once('.classes/cor.xmlparser.class.php');
	function generateHmacKey($data, $apiKey=null)
	{
		$hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
		return $hmackey;
	}
	$txnid = "";
	$txnrefno = "";
	$txnstatus = "";
	$txnmsg = "";
	$firstName = "";
	$lastName = "";
	$email = "";
	$street1 = "";
	$city = "";
	$state = "";
	$country = "";
	$pincode = "";
	$mobileNo = "";
	$signature = "";
	$reqsignature = "";
	$data = "";
	$txnGateway = "";
	$paymentMode = "";
	$maskedCardNumber = "";
	$cardType = "";
	$flag = "dataValid";
	if(isset($_POST['TxId']))
	{
	$txnid = $_POST['TxId'];
	$data .= $txnid;
	}
	if(isset($_POST['TxStatus']))
	{
	$txnstatus = $_POST['TxStatus'];
	$data .= $txnstatus;
	}
	if(isset($_POST['amount']))
	{
	$amount = $_POST['amount'];
	$data .= $amount;
	}
	if(isset($_POST['pgTxnNo']))
	{
	$pgtxnno = $_POST['pgTxnNo'];
	$data .= $pgtxnno;
	}
	if(isset($_POST['issuerRefNo']))
	{
	$issuerrefno = $_POST['issuerRefNo'];
	$data .= $issuerrefno;
	}
	if(isset($_POST['authIdCode']))
	{
	$authidcode = $_POST['authIdCode'];
	$data .= $authidcode;
	}
	if(isset($_POST['firstName']))
	{
	$firstName = $_POST['firstName'];
	$data .= $firstName;
	}
	if(isset($_POST['lastName']))
	{
	$lastName = $_POST['lastName'];
	$data .= $lastName;
	}
	if(isset($_POST['pgRespCode']))
	{
	$pgrespcode = $_POST['pgRespCode'];
	$data .= $pgrespcode;
	}
	if(isset($_POST['addressZip']))
	{
	$pincode = $_POST['addressZip'];
	$data .= $pincode;
	}
	if(isset($_POST['signature']))
	{
	$signature = $_POST['signature'];
	}

	if(isset($_POST['TxRefNo']))
	{
	$txnrefno = $_POST['TxRefNo'];
	}
	if(isset($_POST['TxMsg']))
	{
	$txnmsg = $_POST['TxMsg'];
	}
	if(isset($_POST['email']))
	{
	$email = $_POST['email'];
	}
	if(isset($_POST['addressStreet1']))
	{
	$street1 = $_POST['addressStreet1'];
	}
	if(isset($_POST['addressStreet2']))
	{
	$street2 = $_POST['addressStreet2'];
	}
	if(isset($_POST['addressCity']))
	{
	$city = $_POST['addressCity'];
	}
	if(isset($_POST['addressState']))
	{
	$state = $_POST['addressState'];
	}
	if(isset($_POST['addressCountry']))
	{
	$country = $_POST['addressCountry'];
	}

	if(isset($_POST['mandatoryErrorMsg']))
	{
	$mandatoryerrmsg = $_POST['mandatoryErrorMsg'];
	}
	if(isset($_POST['successTxn']))
	{
	$successtxn = $_POST['successTxn'];
	}
	if(isset($_POST['mobileNo']))
	{
	$mobileNo = $_POST['mobileNo'];
	}
	if(isset($_POST['txnGateway']))
	{
	$txnGateway = $_POST['txnGateway'];
	}
	if(isset($_POST['paymentMode']))
	{
	$paymentMode = $_POST['paymentMode'];
	}
	if(isset($_POST['maskedCardNumber']))
	{
	$maskedCardNumber = $_POST['maskedCardNumber'];
	}
	if(isset($_POST['cardType']))
	{
	$cardType = $_POST['cardType'];
	}
	$bookingid = $_SESSION['hdBookingID'];
	
     $respSignature = generateHmacKey($data,"01ead54113da1cb978b39c1af588cf83e16c519d");
	//$respSignature = generateHmacKey($data,"a06521adb14788d10713f08b6885e62d409d507f");
	if($signature != "" && strcmp($signature, $respSignature) != 0)
	{
		$flag = "dataTampered";
		if(isset($_POST["TxId"])){
		include_once("./includes/cache-func.php");
		include_once('./classes/cor.mysql.class.php');
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "5";//4 tempered transaction at PG		    
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];	
		if(isset($_POST["TxId"]) && $_POST["TxId"]!='0')
		{	
		  $r = $db->callcenterupdate("cor_booking_new", $dataToSave, $whereData);
		}
		unset($whereData);
		unset($dataToSave);
		$db->close();
		$url = "mobileapp-bookingfail.php";
		}
	} 
	else
	{
	 if($_POST["TxStatus"] == "SUCCESS")
	 {
	 
	 
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "3";//3 successful transaction at PG		    
		$dataToSave["billing_cust_name"] = $_POST["firstName"] . " " . $_POST["lastName"];
		$dataToSave["billing_cust_address"] = $_POST["addressStreet1"] . " " . $_POST["addressStreet2"];
		$dataToSave["billing_cust_state"] = $_POST["addressState"];
		$dataToSave["billing_zip_code"] = $_POST["addressZip"];
		$dataToSave["billing_cust_city"] = $_POST["addressCity"];
		$dataToSave["billing_cust_country"] = $_POST["addressCountry"];
		$dataToSave["billing_cust_tel"] = $_POST["mobileNo"];
		$dataToSave["billing_cust_email"] = $_POST["email"];
		$dataToSave["AuthDesc"] = "Y";
		$dataToSave["Checksum"] = $_POST["authIdCode"];
		$dataToSave["Merchant_Param"] = $_POST["TxRefNo"];
		$dataToSave["nb_bid"] = $_POST["pgTxnNo"];
		$dataToSave["nb_order_no"] = $_POST['transactionId'];
		$dataToSave["card_category"] = $_POST["paymentMode"];
		$dataToSave["bank_name"] = "NA";
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		$dataToSave["signature"] = $_POST["signature"];
		$dataToSave["booking_id"] = $_SESSION['hdBookingID'];
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];
        if(isset($_POST["TxId"]) && $_POST["TxId"]!='0')
		{			
		$r = $db->callcenterupdate("cor_booking_new", $dataToSave, $whereData);
		}
		unset($whereData);
		unset($dataToSave);
		
		/************** after payment success ******************************/
		$dataToSave1["payment_mode"] = "1";
		$dataToSave1["payment_status"] = "1";
		$dataToSave1["booking_id"] = $_SESSION['hdBookingID'];
		$whereData1["coric"] = $_POST["TxId"];
		if(isset($_POST["TxId"]) && $_POST["TxId"]!='0')
		{	
		 $r1 = $db->callcenterupdate("cor_booking_new", $dataToSave1, $whereData1);
		}
		unset($whereData1);
		unset($dataToSave1);
		/********************After booking id updation we will update server********/
		
		$dataAPPToSave = array();
		$dataAPPToSave["BookingID"] = $_SESSION['hdBookingID'];
		$dataAPPToSave["PaymentStatus"] = 1;     
		$dataAPPToSave["PaymentAmount"] = $amount;
		$dataAPPToSave["TransactionId"] = $_POST['transactionId'];
		$dataAPPToSave["ckhSum"] = $_POST["authIdCode"];
		$dataAPPToSave["Trackid"] = $_POST['TxId'];
		$dataAPPToSave["discountPc"] = "0";
		$dataAPPToSave["discountAmount"] = "0";
		$dataAPPToSave["DisCountCode"] = "";
		$dataAPPToSave["DiscountTransactionId"] = "";
		
		$cor = new COR();
		if(isset($_POST["TxId"]) && $_POST["TxId"]!='0')
		{	
		 $res = $cor->_CORUpdatePayment($dataAPPToSave);
		}
	    unset($dataAPPToSave);
		/********************After booking id updation we will update server********/
		$db->close();
		$url = "mobileapp-trip-details.php";
		$firstName = $_POST["firstName"];
		$lastName =  $_POST["lastName"];
		$url .= "?resp=booksucc&bookid=" . $_SESSION["hdBookingID"];
		unset($cor);
	 }
	else
	 {
		$dataToSave = array();
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$dataToSave["payment_mode"] = "1";
		$dataToSave["payment_status"] = "4";//4 cancelled transaction at PG		    
		$dataToSave["billing_cust_name"] = $_POST["firstName"] . " " . $_POST["lastName"];
		$dataToSave["billing_cust_address"] = $_POST["addressStreet1"] . " " . $_POST["addressStreet2"];
		$dataToSave["billing_cust_state"] = $_POST["addressState"];
		$dataToSave["billing_zip_code"] = $_POST["addressZip"];
		$dataToSave["billing_cust_city"] = $_POST["addressCity"];
		$dataToSave["billing_cust_country"] = $_POST["addressCountry"];
		$dataToSave["billing_cust_tel"] = $_POST["mobileNo"];
		$dataToSave["billing_cust_email"] = $_POST["email"];
		$dataToSave["AuthDesc"] = "N";
		$dataToSave["Checksum"] = $_POST["authIdCode"];
		$dataToSave["Merchant_Param"] = $_POST["TxRefNo"];
		$dataToSave["nb_bid"] = $_POST["pgTxnNo"];
		$dataToSave["nb_order_no"] = $_POST['transactionId'];
		$dataToSave["card_category"] = $_POST["paymentMode"];
		$dataToSave["bank_name"] = "NA";
		$dataToSave["payment_update_date"] = date('Y-m-d H:i:s');
		$dataToSave["signature"] = $_POST["signature"];
		$whereData = array();
		$whereData["coric"] = $_POST["TxId"];	
        if(isset($_POST["TxId"]) && $_POST["TxId"]!='0')
		{			
		  $r = $db->callcenterupdate("cor_booking_new", $dataToSave, $whereData);
		}
		unset($whereData);
		unset($dataToSave);
		$db->close();
		$url = "mobileapp-bookingfail.php";
		$url .= "?resp=bookfail&bookid=" . $bookingid;
	  }
	}
    header('Location: ' . urldecode($url));
	?>
<?php    
	/* 
		API Class
 	*/	
	require_once("Rest.inc.php");
	include_once('./class/S3_class.php');
	include_once('./class/S3_config.php');
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "carzonrent-db.cwhd81c19z15.ap-southeast-1.rds.amazonaws.com";
		const DB_USER = "root";
		const DB_PASSWORD = "N2o13delhimY";
		const DB = "cor_by_bytesbrick";
		const SITEURL = "http://www.carzonrent.com/rest/";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();	
				// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['request'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/*
		* getPrivateKey method
		* Description: get private key 
		*/
		private function uploaddocument()
		{
		if (!empty($_FILES)) {		
		$name = $_FILES['FileName']['name'];
		$size = $_FILES['FileName']['size'];
		$tmp = $_FILES['FileName']['tmp_name'];
		$ext = getExtension($name);
		
		$rand_number = mt_rand(10000, 99999);
	
		if(strlen($name) > 0)
		{
	       $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
			if(in_array($ext,$valid_formats))
			{ 
				if($size<(1000000))
				{	
						$actual_image_name = $_POST["GuestID"]."_".$rand_number."_".$val.".".$ext;
						$bucket='uploadmyles';
						$targetPath = $_POST["GuestID"].'/'.$actual_image_name;
						$dataToSave= array();
						$resval = 0;
						$resval =  S3::putObjectFile($tmp, $bucket , $targetPath , S3::ACL_PUBLIC_READ);
						if($resval  == 1){
							$obj = new ResponseData();
							$obj->status = 1;
							$obj->msg = "successfully uploaded";
							$obj->errNo = 0;
							$this->response(json_encode($obj), 200);
							
						}else {
							$obj = new BaseResponse();
							$obj->status = 0;
							$obj->msg = "not uploaded";
							$obj->errNo = 5002;
							$this->response(json_encode($obj), 200);
							
						}
						
				}else{
				$obj = new BaseResponse();
				$obj->status = 0;
				$obj->msg ="Please couldn't upload more than 1MB file size";
				$obj->errNo = 5002;
				$this->response(json_encode($obj), 200);
					
				}
		
			}else{
				$obj = new BaseResponse();
				$obj->status = 0;
				$obj->msg = "Please upload only(jpeg ,jpg or png) file";
				$obj->errNo = 5002;
				$this->response(json_encode($obj), 200);
				
			}     
		}else{
			$obj = new BaseResponse();
			$obj->status = 0;
			$obj->msg = "Invalid file, please upload image file.";
			$obj->errNo = 5002;
			$this->response(json_encode($obj), 200);
		} 	
      }  
    }
		
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	class BaseResponse{
		public $status;
		public $errNo;
		public $msg;
	}
	
	class ResponseData extends BaseResponse {	
		public $data;
	}
	
	class ResponseItem extends BaseResponse {	
		public $item;
	}
	class commentList{
		public $TotalRecords;
		public $SearchData;
	}
	// Initiate Library
	
	$api = new API;
	$api->processApi();
?>
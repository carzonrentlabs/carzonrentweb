
//------FOR MODIFY SEARCH------

function _setOriginMS(i, t){
	var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
	var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
	cname = cname.replace(" (excluding NCR regions)", "");
	if(document.getElementById('hdOriginIDMS'))
	document.getElementById('hdOriginIDMS').value = cid;
	if(document.getElementById('hdOriginNameMS'))
	document.getElementById('hdOriginNameMS').value = cname;
	if(document.getElementById('hdOriginIDLMS'))
	document.getElementById('hdOriginIDLMS').value = cid;
	if(document.getElementById('hdOriginNameLMS'))
	document.getElementById('hdOriginNameLMS').value = cname;
	if(document.getElementById('hdOriginIDSFMS'))
	document.getElementById('hdOriginIDSFMS').value = cid;
	if(document.getElementById('hdOriginNameSFMS'))
	document.getElementById('hdOriginNameSFMS').value = cname;
};

function booknowsubmit(formid)
{
	//for track flexi search
	var str2 = formid.replace ( /[^\d.]/g, '' ); 
	var res = formid.replace(str2, "");
    if(res=="formSrchFlexi")
	{
		$('.flexibookclicked').attr('id','flexibookclicked');
	}
	if(res=="formSrchINA")
	{
	    $('.nxtavlclicked'+str2).attr('id','nxtavlclicked');
	}
        document.getElementById(formid).action = webRoot+"/search-result.php";
	    document.getElementById(formid).submit();
	/*var dataString = $("#"+formid).serialize();
		
					$.ajax({type:"POST", url: webRoot+"/modifiysearchurlencoder.php",
					data:dataString, success: function(response) {
					var txt=response;
					if(txt)
					{
	    document.getElementById(formid).action = webRoot+"/search-result.php?p="+txt;
	    document.getElementById(formid).submit();
		}
					}}); */
}
Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}

_validateSFMS = function()
{
	var pd;
	var dd;
	var chk;
	chk = true;
	var cityId=document.getElementById("ddlOriginSF").value;
	chk = isFilledText(document.getElementById("hdOriginNameSFMS"), "", "Origin can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputFieldSF1"), "", "Pickup date can't be left blank.");
	if(chk == true)
		chk = isFilledText(document.getElementById("inputFieldSF2"), "", "Drop date can't be left blank.");
	if(chk){
		pd = document.getElementById("inputFieldSF1").value.trim().split(" ");
		if(pd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(pd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	
	if(chk && document.getElementById("inputFieldSF2") && ($('#dailyBlock').css('display') == 'block')){
		
		dd = document.getElementById("inputFieldSF2").value.trim().split(" ");
		if(dd.length < 3){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[2].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[1].length < 4 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
		if(dd[0].length < 1 && chk){
			alert("Invalid pickup date.");
			chk = false;
		}
	}
	if(chk){
		td = document.getElementById("hdTodayMS").value.trim().split(" ");
		var t = new Date();
        var m = _getMonthInt(td[1].replace(",",""));
		t.setFullYear(td[2], m, td[0]);
		var p = new Date();
        m = _getMonthInt(pd[1].replace(",",""));
		p.setFullYear(pd[2], m, pd[0]);
		p.setHours(document.getElementById('tHourPMS').value);
		p.setMinutes(document.getElementById('tMinPMS').value);
		
		var p1 = new Date();
        m1 = _getMonthInt(pd[1].replace(",",""));
		p1.setFullYear(pd[2], m1, pd[0]);
		p1.setHours(document.getElementById('tHourPMS').value);
		p1.setMinutes(document.getElementById('tMinPMS').value);
		
		
		var d = new Date();
		if($('#dailyBlock').css('display') == 'block')
		{
			m = _getMonthInt(dd[1].replace(",",""));
			d.setFullYear(dd[2], m, dd[0]);
			d.setHours(document.getElementById('tHourDMS').value);
			d.setMinutes(document.getElementById('tMinDMS').value);
		}
		
	
		//for hourly updated on 31-july-2015
		if($('#hourlyBlock').css('display') == 'block')
		{
			var hrs = document.getElementById('nHours').value;
			
		
		 d = p;
		 d = d.addHours(parseInt(hrs));
		 p = p1;
		 
		
		}
	
		
		if(t >= p)
		{
		    alert("Please enter an appropriate pickup date & time");
		    chk = false;
		}
		if (chk) {
		    var diff = p.getTime() - t.getTime();
		    diff = (diff / (1000 * 60 * 60));
		    if(diff < 2 && cityId!=80)
		    {
			alert('You can book at least 2 hours before pickup time.');
			chk = false;	
		    }
			
			/*if(diff < 24 && cityId==80)
		    {
			alert('You can book at least 24 hours before pickup time.');
			chk = false;	
		    }*/
			
		}
		if (document.getElementById('chkPkgTypeMS')) {
		    if (chk && document.getElementById('chkPkgTypeMS').value == 'Hourly') {
				
		    var diff = d.getTime() - p.getTime();
			
			diff = (diff / (1000 * 60 * 60));
			
			if(diff > 24)//if(diff < 24) updated on 31-july-2015
			{
				    alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
				    chk = false;
			}
		    }
		    if(chk && document.getElementById('chkPkgTypeMS').value == 'Daily'){
			if(p >= d)
			{
			alert("Please enter an appropriate drop date & time");
			chk = false;
			}
			
			/*if (cityId==80 && diff < 24)
			{
			alert('You can book at least 24 hours before pickup time.');
			chk = false;
			}*/
			
			else
			{

			var diff = d.getTime() - p.getTime();
			diff = (diff / (1000 * 60 * 60));
			if(diff < 24 && cityId!=80)
			{
			alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
			chk = false;
			}
					
								
			}
		    }
		  
		}
		if (document.getElementById('chkPkgType2')) {
		    if (chk && document.getElementById('chkPkgType2').checked == true) {
			var diff = d.getTime() - p.getTime();
			diff = (diff / (1000 * 60 * 60));
			if(diff < 24)
			{
			    alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
			    chk = false;
			}
		    }
		    if(chk && document.getElementById('chkPkgType2').checked == true){
			if(p >= d)
			{
			    alert("Please enter an appropriate drop date & time");
			    chk = false;
			}
		    }
		}
	}
	if(chk == true)
	{
		document.getElementById('hdOriginNameSFMS').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].text.replace(" (excluding NCR regions)", "");
		document.getElementById('hdOriginIDSFMS').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].value;
		document.getElementById('hdDestinationNameSFMS').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].text.replace(" (excluding NCR regions)", "");
		document.getElementById('hdDestinationIDSFMS').value = document.getElementById('ddlOriginSF').options[document.getElementById('ddlOriginSF').selectedIndex].value;
	}
	
	if(chk == true){
	    document.getElementById('frmModifySrch').action = "search-result.php";
	    document.getElementById('frmModifySrch').submit();
	} else {
	    return chk;
	}
	
}


 function _setDropDateMylesMS(pi, di){
	pd = document.getElementById(pi).value.trim().split(" ");
	dd = document.getElementById(di).value.trim().split(" ");
	var p = new Date();
	var d = new Date();
	m = _getMonthInt(pd[1].replace(",",""));
	if (document.getElementById('chkPkgTypeMS')) {
	   if (document.getElementById('chkPkgTypeMS').value == 'Monthly') {
		var nxtD = eval(parseInt(pd[0]) + 30);
	   }
	   else if(document.getElementById('chkPkgTypeMS').value == 'Weekly'){
		var nxtD = eval(parseInt(pd[0]) + 7);
	   }
	   else{
		var nxtD = eval(parseInt(pd[0]) + 1);
	   }
	}	
	document.getElementById('nWeeks').selectedIndex=0
        document.getElementById('nMonths').selectedIndex=0
	p.setFullYear(pd[2], m, pd[0]);
	d.setFullYear(pd[2], m, nxtD);
	document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
	try {
	    $('#inputFieldSF2').datetimepicker({
	    format:'d M, Y H:i',
	    minDate:d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
	    step:30
	    });
	} catch(e){
	}
	
	document.getElementById('pickdateMS').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();
	if(document.getElementById('dropdateMS'))
	document.getElementById('dropdateMS').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
	var tm = pd[3].split(":");

	document.getElementById('tHourPMS').value = tm[0];
	document.getElementById('tMinPMS').value = tm[1];
	
	if(document.getElementById('dropdateMS')){
		document.getElementById('tHourDMS').value = tm[0];
		document.getElementById('tMinDMS').value = tm[1];
	}
};
 
 
  _setDropOffDateMS = function(di)
{
	dd = document.getElementById(di).value.trim().split(" ");
	var d = new Date();
	m = _getMonthInt(dd[1].replace(",",""));
	d.setFullYear(dd[2], m, dd[0]);
	document.getElementById('dropdateMS').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
	var tm = dd[3].split(":");
	document.getElementById('tHourDMS').value = tm[0];
	document.getElementById('tMinDMS').value = tm[1];
}
function _setOriginMSmodify(i, t){
	var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
	 if(cid=='69'){
                document.getElementById('goanote').innerHTML = 'Note: No change in pre-authorization charges for Goa';
            }else{
               document.getElementById('goanote').innerHTML = ''; 
            }
	
	
	
	var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
	cname = cname.replace(" (excluding NCR regions)", "");
	if(document.getElementById('hdOriginIDMS'))
	document.getElementById('hdOriginIDMS').value = cid;
	if(document.getElementById('hdOriginNameMS'))
	document.getElementById('hdOriginNameMS').value = cname;
	if(document.getElementById('hdOriginIDLMS'))
	document.getElementById('hdOriginIDLMS').value = cid;
	if(document.getElementById('hdOriginNameLMS'))
	document.getElementById('hdOriginNameLMS').value = cname;
	if(document.getElementById('hdOriginIDSFMS'))
	document.getElementById('hdOriginIDSFMS').value = cid;
	if(document.getElementById('hdOriginNameSFMS'))
	document.getElementById('hdOriginNameSFMS').value = cname;
    
    
        var clientid=2205;
        var webrt = document.getElementById('webroot').value;
        var dataString = 'Cityid='+ cid+'&clientid='+clientid+'&rentaltype=';
        $.ajax({type:"POST", url: webrt+"/getavailablityoption.php",
        data:dataString, success: function(response) {
        var txt=response.split("++++++");//alert(txt);
        
        if(txt[0])
        {
                $("#package").html(txt[0]);
        }
        if (txt[1] == "Hourly")
    {
        //document.getElementById('hourly').className = 'sel';
        document.getElementById('chkPkgTypeMS').value = 'Hourly';
        document.getElementById('hourlyBlock').style.display = 'block';
	document.getElementById('dailyBlock').style.display = 'none';
        document.getElementById('weeklyBlock').style.display = 'none';
        document.getElementById('monthlyBlock').style.display = 'none';
    }
    if (txt[1] == "Daily")
    {
        document.getElementById('chkPkgTypeMS').value = 'Daily';
        document.getElementById('dailyBlock').style.display = 'block';
	document.getElementById('hourlyBlock').style.display = 'none';
        document.getElementById('weeklyBlock').style.display = 'none';
        document.getElementById('monthlyBlock').style.display = 'none';
	
    }
    if (txt[1] == "Monthly")
    {
        
        document.getElementById('chkPkgTypeMS').value = 'Monthly';
        document.getElementById('dailyBlock').style.display = 'none';
	document.getElementById('hourlyBlock').style.display = 'none';
        document.getElementById('weeklyBlock').style.display = 'none';
        document.getElementById('monthlyBlock').style.display = 'block';
        //_setdropdatesmodify('week', 'inputFieldSF1', 'inputFieldSF2');
    }
    if (txt[1] == "Weekly")
    {
        document.getElementById('chkPkgTypeMS').value = 'Weekly';
        document.getElementById('dailyBlock').style.display = 'none';
	document.getElementById('hourlyBlock').style.display = 'none';
        document.getElementById('weeklyBlock').style.display = 'block';
        document.getElementById('monthlyBlock').style.display = 'none';
        //setdropdatesmodify('month', 'inputFieldSF1', 'inputFieldSF2');
    }
			}});
                    
    
};
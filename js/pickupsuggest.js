 $.fn.example = function(select, type) {
        var div = $("<div></div>").addClass(type)
          .append(select.clone().attr({ id: select.attr("id") + "_" + type, "class": type }));
        this.append(div);
      };

      $(document).ready(function() {
        var president = $("select#president");
		
		
		$("#orignNameVal").example(president, "dull");
        $("#orignNameVal").example(president, "flexselect-optgroup");
		
        $("select.flexselect").flexselect({ showDisabledOptions: true });
        $("select.flexselect-optgroup").flexselect({ indexOptgroupLabels: true });
		$("input#president_flexselect-optgroup_flexselect").val("")
        $("select").attr("tabindex", -1);
        $("input.flexselect").each(function(index) { $(this).attr("tabindex", index+1) });
      });
function _checkDisValidity(xi){
    var chk = true;
    var pc = document.getElementById('empcode').value;
    if(pc != "" && pc != "Discount coupon number")
    _checkDiscount(xi);
    else
    alert("Please enter promotion code");
}
function _checkDiscount(xi){
    
    var aj;
    var strParam;
    if (window.XMLHttpRequest)
        aj = new XMLHttpRequest;
    else if (window.ActiveXObject)
        aj = new ActiveXObject("Microsoft.XMLHTTP");
    strParam = "ccid=" + document.getElementById('hdCarModelID'+xi).value;
    strParam += "&ctid=" + document.getElementById('hdOriginID'+xi).value;
    strParam += "&pc=" + document.getElementById('empcode').value;
    strParam += "&pd=" + document.getElementById('pickdate'+xi).value;
    strParam += "&amt=" + parseInt(document.getElementById('totAmount'+xi).value);
    /*strParam = "ccid=1";
    strParam += "&ctid=2";
    strParam += "&pc=" + document.getElementById('disccode').value;
    strParam += "&pd=" + document.getElementById('pickdate2').value;*/
    aj.open("POST", webRoot + "/verify-discount.php", true);
    aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    aj.onreadystatechange = function() {
        if (aj.readyState == 1){
            document.getElementById('spDiscount').innerHTML = "Checking...";
        }
        else if (aj.readyState == 4) {
            var r = "";
            if(aj.status == 200){
                r = aj.responseText;
                aj = null;
                //document.getElementById('spDiscount').innerHTML = r;
                if(r != "" && r.indexOf("|#|") > -1){
                    var arrR = r.split("|#|");
                    var totPay = document.getElementById('totFare' + xi).value;
                    if(totPay > 0){
                        var dp = parseFloat(arrR[1]);
                        if(dp > 0 && dp != undefined){
                            var dcPay = totPay - ((totPay * dp) / 100);
                            if (document.getElementById('spDiscount'))
                            document.getElementById('spDiscount').innerHTML = "Now amount payable: Rs. " + parseInt(Math.ceil(dcPay)) +"/-";
                            document.getElementById('spPay').innerHTML = "Rs. " + parseInt(Math.ceil(dcPay)) + "/-";
                            //document.getElementById('totFare' + xi).value = parseInt(Math.ceil(dcPay));
                            document.getElementById('discount' + xi).value = dp;
                            document.getElementById('discountAmt' + xi).value = parseInt(Math.ceil(((totPay * dp) / 100)));
                        }
                        else{
                            document.getElementById('disccode').value = "Discount coupon number";
                            document.getElementById('spDiscount').innerHTML = "No discount.";
                        }
                    }
                    else {
                        document.getElementById('disccode').value = "Discount coupon number";
                        document.getElementById('spDiscount').innerHTML = "Discount is not applicable due to following reason:<br />" + arrR[3];
                    }
                }
            }
            else
                r = "ERROR " + aj.readyState + ": " + aj.statusText;
        }
    };
    aj.send(strParam);
};

function _applyDiscount(dp){
        
};
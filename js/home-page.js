String.prototype.trim=function(){var a;a=this.replace(/^\s*(.*)/,"$1");return a=a.replace(/(.*?)\s*$/,"$1")};function isEmailAddr(a,b,c){var d=!1,e=new String(a.value),f=e.indexOf("@");if(0<f){var g=e.indexOf(".",f);g>f+1&&e.length>g+1&&(d=!0)}!1==d&&(""!=c&&null!=c&&document.getElementById(c)?document.getElementById(c).innerHTML=b:alert(b),a.focus());return d} function isNumeric(a,b){var c;sText=a.value;var d=!0;for(i=0;i<sText.length;i++)c=sText.charAt(i),-1=="0123456789.-".indexOf(c)&&(d=!1);!1==d&&""!=b&&(alert(b),a.focus());return d}function isAnyNumeric(a,b,c){var d;sText=a.value;var e=!1;for(i=0;i<sText.length;i++)d=sText.charAt(i),-1<"0123456789.-".indexOf(d)&&(e=!0);return e==b?(""!=c&&(alert(c),a.focus()),!1):!0}function removeAllOptions(a){var b;for(b=a.options.length-1;0<=b;b--)a.remove(b)} function addOption(a,b,c){var d=document.createElement("OPTION");d.text=c;d.value=b;a.options.add(d)}function isFilledText(a,b,c,d){var e;e=a.value;e=e.trim();if(""==e||e==b){if(""!=c){""!=d&&null!=d&&document.getElementById(d)?document.getElementById(d).innerHTML=c:alert(c);try{a.focus()}catch(f){}}return!1}return!0} function isFilledSelect(a,b,c,d,e){if(0==d)return d=a.selectedIndex,d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus(),!1):!0;if(1==d)return d=a.options[a.selectedIndex].text,b=b.trim(),d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus(),!1):!0;if(2==d)return d=a.options[a.selectedIndex].value,b=b.trim(),d==b?(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML= c:alert(c),a.focus(),!1):!0}function isCBoxChecked(a,b,c,d,e){var f,g;if(1==d)return!1==a.checked?(""!=c&&(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c),a.focus()),!1):!0;f=!1;g=0;d=a.length;for(i=0;i<d;i++)!0==a[i].checked&&(g++,g==b&&(f=!0));!1==f&&""!=c&&(""!=e&&null!=e&&document.getElementById(e)?document.getElementById(e).innerHTML=c:alert(c));return f} function compareString(a,b,c){var d=!1;"Text"==c?(a=a.value,a=a.trim(),b=b.value,b=b.trim(),a==b&&(d=!0)):"Select"==c&&(a=a.options[a.selectedIndex].text,a=a.trim(),b=b.options[b.selectedIndex].text,b=b.trim(),a==b&&(d=!0));return d} function createStrSubmit(a){var b="",c,d;d="";for(i=0;i<a.elements.length;i++){c=a.elements[i];switch(c.type){case "text":case "select-one":case "hidden":case "password":case "textarea":b+=c.name+"="+escape(c.value)+"&";break;case "checkbox":!0==c.checked&&(d!=c.name?b+=c.name+"="+escape(c.value)+"&":("&"==b.substring(b.length-1,b.length)&&(b=b.substring(0,b.length-1)),b+=","+escape(c.value)),d=c.name)}"&"!=b.substring(b.length-1,b.length)&&(b+="&")}"&"==b.substring(b.length-1,b.length)&&(b=b.substring(0, b.length-1));return b}function checkFormAllFields(a){var b=!0,c,d=-1;for(i=0;i<a.elements.length;i++)switch(currElement=a.elements[i],currElement.type){case "text":case "select-one":case "password":case "textarea":case "checkbox":c=currElement.value,c=c.trim(),""==escape(c)&&(!0==b&&(d=i),b=!1)}!1==b&&(alert("No field can be left blank."),currElement=a.elements[d],currElement.focus());return b} function _allowNumeric(a){a=getKeyCode(a);return 48<=a&&57>=a?!0:null==a?!0:8==a||0==a||46==a||9==a?!0:!1}function _allowAlpha(a){a=getKeyCode(a);return 65<=a&&90>=a||97<=a&&122>=a?!0:null==a?!0:8==a||0==a||46==a||32==a||9==a?!0:!1}function getKeyCode(a){return window.event?window.event.keyCode:a?a.which:null}function _setCookie(a,b,c){var d=new Date;d.setDate(d.getDate()+c);document.cookie=a+"="+escape(b)+(null==c?"":";expires="+d.toGMTString())} function _getCookie(a){return 0<document.cookie.length&&(c_start=document.cookie.indexOf(a+"="),-1!=c_start)?(c_start=c_start+a.length+1,c_end=document.cookie.indexOf(";",c_start),-1==c_end&&(c_end=document.cookie.length),unescape(document.cookie.substring(c_start,c_end))):""}function _getDDLValue(a){var b="";document.getElementById(a)&&(b=document.getElementById(a).options[document.getElementById(a).selectedIndex].value);return b} function _getDDLText(a){var b="";document.getElementById(a)&&(b=document.getElementById(a).options[document.getElementById(a).selectedIndex].text);return b}_fillNextDDL=function(a,b){if(b){removeAllOptions(b);for(i=0;i<a.options.length;i++)addOption(b,a.options[i].value,a.options[i].text)}};_disableFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)cElement=document.getElementById(a).elements[i],cElement.disabled=!0}; _enableFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)cElement=document.getElementById(a).elements[i],cElement.disabled=!1}; _clearFormElements=function(a){var b=document.getElementById(a).elements.length;for(i=0;i<b;i++)switch(cElement=document.getElementById(a).elements[i],cElement.type){case "text":case "password":case "hidden":case "textarea":cElement.value="";break;case "select-one":cElement.selectedIndex=0;break;case "checkbox":case "radio":cElement.checked=!1}};_selAllUsers=function(){for(i=0;i<document.getElementsByName("chkUsers[]").length;i++)document.getElementsByName("chkUsers[]")[i].checked=document.getElementById("chkAllUsers").checked};
function _$(id){
    if(typeof(id) == "string"){
        if(document.getElementById(id))
            return document.getElementById(id);
        else{
            if(document.getElementsByName(id))
                return document.getElementsByName(id);
            else {
                if(document.getElementsByTagName(id))
                    return document.getElementsByTagName(id);
                else
                    return null;
            }
        }
    }
    else {
        if(id)
            return id;
        else
            return null;
    }
};
function _disablePage(){
    if(_$('disableLayer').length <= 0){
        var d;
        try{
            d = document.createElement("<div id='disableLayer' style='background-color:#000;position:fixed;opacity:0.2;z-index:99;top:0;left:0;position:fixed;cursor:pointer;'></div>");
        }
        catch(e){
            d = document.createElement("div");
            d.id = "disableLayer";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.backgroundColor = "#000";
            d.style.opacity = 0.2;
            d.style.zIndex = 99;
            d.style.cursor = 'pointer';
            d.style.top = 0 + "px";
            d.style.left = 0 + "px";
        }
        document.body.appendChild(d);
    }
};
function _showPopUp(mode, urlhtml, divid, w, h, isModal, param){
    _disablePage();
    if(!isModal){
        _$('disableLayer').onclick = (function(){
            document.body.removeChild(_$('disableLayer'));
            document.body.removeChild(_$('popupContainer'));
        });
    }
    
    var postop = ((screen.availHeight) - h)/3;;
    var posleft = ((screen.availWidth) - w)/2;
    var d;
    try{
        d = document.createElement("<div id='popupContainer' style='background-color:#fff;opacity:1;position:relative;z-index:1000;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupContainer";
        d.style.position = "fixed";
        d.style.backgroundColor = "#fff";
        d.style.top = postop + "px";
        d.style.left = posleft + "px";
        d.style.zIndex = "1000";
    }
    document.body.appendChild(d);
    
    try{
        d = document.createElement("<div id='popupPage' style='background-color:#fff;opacity:1;position:absolute;z-index:1000;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupPage";
        d.style.position = "absolute";
        d.style.height = h + "px";
        d.style.width = w + "px";
        d.style.backgroundColor = "#fff";
        d.style.borderRadius = "5px";
        d.style.padding = "10px";
        d.style.boxShadow = "2px 5px 10px #333333";       
        d.style.opacity = 1;
        d.style.zIndex = "1000";
    }
    _$('popupContainer').appendChild(d);
    
    if(mode.toLowerCase() == "iframe"){
        try{
            d = document.createElement("<iframe id=\"" + divid + "\" border=\"0\" frameborder=\"0\" framespacing=\"0\" style=\"z-index:1000;top:0;left:0;position:absolute;\" src=\"./" + urlhtml + "\" ></iframe>");
        }
        catch(e)
        {
            d = document.createElement("IFRAME"); 
            d.id = divid;
            d.frameBorder = 0;
            d.frameSpacing = 0;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "1000";
            d.src = urlhtml;
        }
        _$('popupPage').appendChild(d);
    }
    else if(mode.toLowerCase() == "ajax"){
        try{
            d = document.createElement("<div id=\"" + divid + "\" style=\"z-index:1000;top:0;left:0;position:absolute;\" ></div>");
        }
        catch(e)
        {
            d = document.createElement("div"); 
            d.id = divid;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "1000";
        }
	
        _$('popupPage').appendChild(d);
	
	_waitGetData = function(){
            _$(divid).innerHTML = "Please wait";
        };
        _responseGetData = function(str){
            _$(divid).innerHTML = str;
            _$(divid).style.top = postop + "px";
            _$(divid).style.left = posleft + "px";
        };
	
        var aj = new _ajax(urlhtml, "GET", param, function(){_waitGetData()}, function(r){_responseGetData(r)});
        aj._query();
               
    }
    else if(mode.toLowerCase() == "html"){
        try{
            d = document.createElement("<div id=\"" + divid + "\" style=\"z-index:1000;top:0;left:0;position:absolute;\" ></div>");
        }
        catch(e)
        {
            d = document.createElement("div"); 
            d.id = divid;
            d.style.height = (h - 5) + "px";
            d.style.width = (w - 5) + "px";
            d.style.zIndex = "1000";
        }
        _$('popupPage').appendChild(d);
        _$(divid).innerHTML = urlhtml;
    }
};
_closePopUp = function(){
    document.body.removeChild(_$('disableLayer'));
    document.body.removeChild(_$('popupContainer'));
};

function _disableThisPage()
{
	var d;
        try{
            d = document.createElement("<div id='_bbdl' style='background-color:#000;position:fixed;opacity:0.2;z-index:99;top:0;left:0;position:fixed;cursor:default;'></div>");
        }
        catch(e){
            d = document.createElement("div");
            d.id = "_bbdl";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.backgroundColor = "#000";
            d.style.opacity = 0.2;
	    d.style.left = 0;
	    d.style.top = 0;
            d.style.zIndex = 99;
            d.style.cursor = 'default';
        }
        document.body.appendChild(d);
};

function _enableThisPage()
{
	try{
		var d = document.getElementById("_bbdl");
		document.body.removeChild(d);
	}
	catch(e){}
};

function _disableThisPageRed()
{
	var d;
        try{
            d = document.createElement("<div id='_bbdl' style='background-color:#e25b33;position:fixed;opacity:0.9;z-index:99;top:0;left:0;position:fixed;cursor:default;'></div>");
        }
        catch(e){
            d = document.createElement("div");
            d.id = "_bbdl";
            d.style.position = "fixed";
            d.style.height = "100%";
            d.style.width = "100%";
            d.style.backgroundColor = "#e25b33";
            d.style.opacity = 0.9;
			d.style.left = 0;
			d.style.top = 0;
            d.style.zIndex = 99;
            d.style.cursor = 'default';
        }
        document.body.appendChild(d);
};

function _enableThisPage()
{
	try{
		var d = document.getElementById("_bbdl");
		document.body.removeChild(d);
	}
	catch(e){}
};

var doObjectID = "";
var moObjectID = "";	
	function _setDivPos(divElem)
	{
		var divX, divY;
		var scrWidth, scrHeight;
		var scrollX, scrollY;
		divX = divY = 0;
		scrollX = scrollY = 0;
		var brsName;
		var brsVer;
		var brsDetails = navigator.userAgent;
		var cssItem;
		if(brsDetails.toLowerCase().indexOf("msie") >= 0)
		{
			brsName = "msie";
			brsVers = brsDetails.split(";");
			brsShortName = brsVers[1];
			brsShortName = brsShortName.split(" ");
			brsVer = parseInt(brsShortName[2]);
		}
		else if(brsDetails.toLowerCase().indexOf("firefox") >= 0)
		{
			brsName = "firefox";
			brsVers = brsDetails.split(" ");
			brsShortName = brsVers[brsVers.length - 1];
			brsShortName = brsShortName.split("/");
			brsVer = parseInt(brsShortName[1]);
		}
		if(brsName == "firefox")
		{
			scrollX = window.pageXOffset;
			scrollY = window.pageYOffset;
		}
		else if(brsName == "msie" && brsVer>6)
		{
			scrollX = document.documentElement.scrollLeft;
			scrollY = document.documentElement.scrollTop;
		}
		else
		{
			scrollX = document.body.scrollLeft;
			scrollY = document.body.scrollTop;
		}
		scrWidth = screen.availWidth;
		scrHeight =  screen.availHeight;
		
		if(typeof (divElem) == "string")
        {
            if(document.getElementById(divElem))
                divElem = document.getElementById(divElem);
        }
        else
            divElem = divElem;

		divElem.style.display = "block";
		var divW = parseInt(divElem.style.width);
		if(isNaN(divW) == true)
			divW = parseInt(divElem.offsetWidth);
		if(isNaN(divW) == true)
			divW = 0;
	
		var divH = parseInt(divElem.style.height);
		if(isNaN(divH) == true)
			divH = parseInt(divElem.offsetHeight);
		if(isNaN(divH) == true)
			divH = 0;

		divX = (parseInt(scrWidth/2) + scrollX) - parseInt(divW/2);
		divY = (parseInt(scrHeight/3) + scrollY) - parseInt(divH/2);
		if(divX < scrollX)
			divX = scrollX + 10;
		if(divY < scrollY)
			divY = scrollY + 10;

		divElem.style.left = divX + "px";
		divElem.style.top =	divY + "px";
	};
	
	_findXYPosInBody = function(objID)
    {
        var objTagName = "";
        var objectID;
        //alert(objID);
        if(typeof (objID) == "string")
        {
            if(document.getElementById(objID))
            {
                objectID = document.getElementById(objID);
                objTagName = objectID.tagName;
            }
        }
        else
        {
            objectID = objID;
            objTagName = objID.tagName;
        }
        var posX = posY = 0
        while(objTagName.toString().toLowerCase() != "body" && objTagName.toString().toLowerCase() != "html")
        {
            posX += objectID.offsetLeft;
            posY += objectID.offsetTop;
            objectID = objectID.offsetParent;
            objTagName = objectID.tagName;
        }
        return (posX + "," + posY);
    };

    _showFloatingObject = function(refObjId, dispObjId, addX, addY, calMode){
        doObjectID = dispObjId;
        var dispObj = document.getElementById(dispObjId);
        dispObj.style.display = dispObj.style.display == "block" ? "none" : "block";
        if(dispObj.style.display == "block")
        {
            var coord = _findXYPosInBody(refObjId);
            var coords = coord.split(",");
            var xA = parseInt(coords[0]);
            var yA = parseInt(coords[1]);
            dispObj.style.left = (xA + (addX)) + "px";
            dispObj.style.top = (yA + (addY)) + "px";
        }
        if(calMode == 1)
        {
            document.onmousedown = _hideFloatingObject;
            document.onmouseup = _hideFloatingObject;
            window.onmousedown = _hideFloatingObject;
            window.onmouseup = _hideFloatingObject;
        }
    };

	_hideFloatingObject = function(ev1){
        if(!ev1) var ev1 = window.event;
	    var sender = (typeof( window.event ) != "undefined" ) ? ev1.srcElement : ev1.target;
	    var senderTagName = sender.tagName.toString().toLowerCase();
        var isClickable = false;
        while(senderTagName.toString().toLowerCase() != "body" && senderTagName.toString().toLowerCase() != "html")
        {
            if(sender.id.toString().toLowerCase() == doObjectID.toString().toLowerCase())
                isClickable = true;
            sender = sender.offsetParent;
            senderTagName = sender.tagName;
        }
	    if(!isClickable)
	    {
	        try
	        {
                var dispObj = document.getElementById(doObjectID);
                dispObj.style.display = "none";
            }
            catch(e){}
        }
    };

	_showMouseOverObject = function(refObjId, dispObjId, addX, addY, calMode){
		if(doObjectID != "" && doObjectID != undefined)
			_hideFloatingObjectWithID(doObjectID);
		doObjectID = dispObjId;
        moObjectID = refObjId;
		if(document.getElementById(moObjectID).className == "")
			document.getElementById(moObjectID).className = "moverMenu";
        var dispObj = document.getElementById(dispObjId);
        dispObj.style.display = dispObj.style.display == "block" ? "none" : "block";
        if(dispObj.style.display == "block")
        {
            var coord = _findXYPosInBody(refObjId);
            var coords = coord.split(",");
            var xA = parseInt(coords[0]);
            var yA = parseInt(coords[1]);
            dispObj.style.left = (xA + (addX)) + "px";
            dispObj.style.top = (yA + (addY)) + "px";
        }
        if(calMode == 1)
        {
            document.onmousedown = _hideMouseOverObject;
            document.onmouseup = _hideMouseOverObject;
			document.onmouseover = _hideMouseOverObject;
            window.onmousedown = _hideMouseOverObject;
            window.onmouseup = _hideMouseOverObject;
			window.onmouseover = _hideMouseOverObject;
        }
	 };

	_hideMouseOverObject = function(ev1){
        if(!ev1) var ev1 = window.event;
	    var sender = (typeof( window.event ) != "undefined" ) ? ev1.srcElement : ev1.target;
	    var senderTagName = sender.tagName.toString().toLowerCase();
        var isMouseOver = false;
        while(senderTagName.toString().toLowerCase() != "body" && senderTagName.toString().toLowerCase() != "html")
        {
            if(sender.id.toString().toLowerCase() == moObjectID.toString().toLowerCase() || sender.id.toString().toLowerCase() == doObjectID.toString().toLowerCase())
                isMouseOver = true;
            sender = sender.offsetParent;
            senderTagName = sender.tagName;
        }
	    if(!isMouseOver)
	    {
	        try
	        {
                var dispObj = document.getElementById(doObjectID);
                dispObj.style.display = "none";
				if(document.getElementById(moObjectID).className == "moverMenu")
					document.getElementById(moObjectID).className = "";
            }
            catch(e){}
        }
    };
    
    _hideFloatingObjectWithID = function(objID){
        try
        {
            var dispObj = document.getElementById(objID);
            dispObj.style.display = "none";
            document.onmousedown = null;
            document.onmouseup = null;
            window.onmousedown = null;
            window.onmouseup = null;
			if(document.getElementById(moObjectID).className == "moverMenu")
				document.getElementById(moObjectID).className = "";
        }
        catch(e){}
    };







var webRoot = "http://localhost/carzonrentweb";
//var webRoot = "http://www.carzonrent.com";
_getMonthInt = function (ms) {
    var m = 0;
    switch (ms.toLowerCase()) {
        case "jan":
            m = 0;
            break;
        case "feb":
            m = 1;
            break;
        case "mar":
            m = 2;
            break;
        case "apr":
            m = 3;
            break;
        case "may":
            m = 4;
            break;
        case "jun":
            m = 5;
            break;
        case "jul":
            m = 6;
            break;
        case "aug":
            m = 7;
            break;
        case "sep":
            m = 8;
            break;
        case "oct":
            m = 9;
            break;
        case "nov":
            m = 10;
            break;
        case "dec":
            m = 11;
            break;
    }
    return m;
};
_validate = function ()
{
    var pd;
    var dd;
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("ddlOrigin"), "", "Origin can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("hdDestinationName"), "", "Destination can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField"), "", "Pickup date can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField2"), "", "Drop date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputField").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        dd = document.getElementById("inputField2").value.trim().split(" ");
        if (dd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        var d = new Date();
        m = _getMonthInt(dd[1].replace(",", ""));
        d.setFullYear(dd[2], m, dd[0]);
        if (t > p)
        {
            alert("Please enter an appropriate pickup date");
            chk = false;
        }
        if (chk) {
            if (p > d)
            {
                alert("Please enter an appropriate drop date");
                chk = false;
            }
        }
    }

    if (chk == true) {
        document.getElementById('form1').action = webRoot + "/outstation/search/" + document.getElementById("hdOriginName").value.replace(/ /g, "-").toLowerCase() + "-to-" + document.getElementById("hdDestinationName").value.replace(/,/g, "-to-").replace(/ /g, "-").toLowerCase() + "/";
        document.getElementById('form1').submit();
    } else {
        return chk;
    }
    //{
    //	document.getElementById('hdOriginName').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].text;
    //	document.getElementById('hdOriginID').value = document.getElementById('ddlOrigin').options[document.getElementById('ddlOrigin').selectedIndex].value;
    //}
    //return chk;
}


// lead page

_validatelead = function ()
{
    var pd;
    var dd;
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("ddlOrigin"), "", "Origin can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("hdDestinationName"), "", "Destination can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField"), "", "Pickup date can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField2"), "", "Drop date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputField").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        dd = document.getElementById("inputField2").value.trim().split(" ");
        if (dd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        var d = new Date();
        m = _getMonthInt(dd[1].replace(",", ""));
        d.setFullYear(dd[2], m, dd[0]);
        if (t > p)
        {
            alert("Please enter an appropriate pickup date");
            chk = false;
        }
        if (chk) {
            if (p > d)
            {
                alert("Please enter an appropriate drop date");
                chk = false;
            }
        }
    }
	
	  
	  
		if (chk)
		{
		mobile = document.getElementById("mobile").value;

		if (mobile=='10 digit mobile number')
		{

		alert("Mobile no can not blank.");
		chk = false;
		}
		}

	 
	  
	 if (chk)
    {
		
		
		
		
        if (document.getElementById("mobile").value == "10 digit mobile number")
        {
            alert("Please fill in your mobile number");
            chk = false;
        }
    }
    if (chk)
    {
        var mNo = document.getElementById("mobile").value;
        if (mNo.length < 10)
        {
            
			alert("Mobile number must be of 10 digit.");
            chk = false;
        }
    }
	
	  var email = document.getElementById("email").value;
	 if (email!='Email') {
		if (chk == true)
	   {
		
		chk = isEmailAddr(document.getElementById("email"), "Please fill a valid email id.", "msgsendmail");
	   }
		
		
	 }
	 
	 if (chk == true)
	 {
        chk = isFilledText(document.getElementById("captcha_code"), "", "Validation code can't be left blank.");
	 }
	 
	    
    if (chk == true) {
       
        document.getElementById('form1').submit();
    } else {
        return chk;
    }
   
}

// end  




_validateSF = function ()
{
    var pd;
    var dd;
    var chk;
    chk = true;
    cityId = document.getElementById("hdOriginIDSF").value;

    chk = isFilledText(document.getElementById("hdOriginNameSF"), "", "Origin can't be left blank.");

    if (chk == true)
        chk = isFilledText(document.getElementById("inputFieldSF1"), "", "Pickup date can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputFieldSF2"), "", "Drop date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputFieldSF1").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk && document.getElementById("inputFieldSF2")) {
        dd = document.getElementById("inputFieldSF2").value.trim().split(" ");
        if (dd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {

        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        p.setHours(document.getElementById('tHourP').value);
        p.setMinutes(document.getElementById('tMinP').value);
        var d = new Date();
        m = _getMonthInt(dd[1].replace(",", ""));
        d.setFullYear(dd[2], m, dd[0]);
        d.setHours(document.getElementById('tHourD').value);
        d.setMinutes(document.getElementById('tMinD').value);

        if (t >= p)
        {
            alert("Please enter an appropriate pickup date & time");
            chk = false;
        }
        if (chk) {
            var diff = p.getTime() - t.getTime();
            diff = (diff / (1000 * 60 * 60));
            if (diff < 2 && cityId != '80')
            {
                alert('You can book at least 2 hours before pickup time.');
                chk = false;
            }

            /*if (cityId == '80' && diff < 24)
            {
                alert('You can book at least 24 hours before pickup time.');
                chk = false;
            }*/


        }
        if (document.getElementById('chkPkgType')) {
            if (chk && document.getElementById('chkPkgType').value == 'Hourly') {
                var diff = d.getTime() - p.getTime();
                diff = (diff / (1000 * 60 * 60));
                if (diff < 24 && cityId != '80')
                {
                    alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
                    chk = false;
                }

                /*if (cityId == '80' && diff < 24)
                {
                    alert('You can book at least 24 hours before pickup time.');
                    chk = false;
                }*/



            }
            if (chk && document.getElementById('chkPkgType').value == 'Daily') {

                if (p >= d)
                {
                    alert("Please enter an appropriate drop date & time");
                    chk = false;
                } else {
                    var diff = d.getTime() - p.getTime();
                    diff = (diff / (1000 * 60 * 60));
                    if (diff < 24 && cityId != '80')
                    {
                        alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
                        chk = false;
                    }

                    if (cityId == '80' && diff < 24)
                    {
                        alert('You can book at least 24 hours before pickup time.');
                        chk = false;
                    }


                }
            }

        }
        if (document.getElementById('chkPkgType2')) {
            if (chk && document.getElementById('chkPkgType2').checked == true) {
                var diff = d.getTime() - p.getTime();
                diff = (diff / (1000 * 60 * 60));
                if (diff < 24 && cityId != '80')
                {
                    alert('You can book at least for one day.\nElse try booking for hourly rental basis.');
                    chk = false;
                }

               /* if (cityId == '80' && diff < 24)
                {
                    alert('You can book at least 24 hours before pickup time.');
                    chk = false;
                }*/


            }
            if (chk && document.getElementById('chkPkgType2').checked == true) {
                if (p >= d)
                {
                    alert("Please enter an appropriate drop date & time");
                    chk = false;
                }
            }
        }
    }
    if (chk == true)
    {


        document.getElementById('hdOriginNameSF').value = document.getElementById('hdDestinationNameSF').value;
        document.getElementById('hdOriginIDSF').value = document.getElementById('hdDestinationIDSF').value;
    }

    if (chk == true) {
        document.getElementById('form3').action = webRoot + "/search-result.php";
        document.getElementById('form3').submit();
    } else {
        return chk;
    }

}

_validateLocal = function ()
{
    var chk;
    chk = true;
    chk = isFilledSelect(document.getElementById("ddlOriginL"), 0, "Origin can't be left blank.", 0, "0");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField5"), "", "Pickup date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputField5").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        var tm = pd[3];
        p.setHours(document.getElementById('tHour').options[document.getElementById('tHour').selectedIndex].value);
        p.setMinutes(document.getElementById('tMin').options[document.getElementById('tMin').selectedIndex].value);
        if (t > p)
        {
            alert("Please enter an appropriate pickup date & time.");
            chk = false;
        }
    }
    if (chk) {
        var diff = p.getTime() - t.getTime();
        diff = (diff / (1000 * 60 * 60));
        if (diff < 2)
        {
            alert('You can book at least 2 hours before pickup time.');
            chk = false;
        }
    }
    if (chk == true)
    {
        document.getElementById('hdOriginNameL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].text;
        document.getElementById('hdOriginIDL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].value;
        document.getElementById('hdDestinationNameL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].text;
        document.getElementById('hdDestinationIDL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].value;
    }
    if (chk == true) {
        document.getElementById('form2').action = webRoot + "/local/search/" + document.getElementById("hdOriginNameL").value.replace(" ", "-").toLowerCase() + "/";
        document.getElementById('form2').submit();
    } else {
        return chk;
    }
    //return chk;
}
_checkLogin = function () {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("monumber"), "10 digit mobile number", "Mobile number can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumber"), "", "Mobile number can't be left blank.");
    if (chk)
    {
        if (document.getElementById("monumber").value.length < 10)
        {
            alert("Please enter a valid 10 digit mobile number.");
            chk = false;
        }
    }
    if (chk) {
        if (document.getElementById('isExistUser').checked) {
            chk = isFilledText(document.getElementById("corpassword"), "", "Password can't be left blank.");
        }
    }
    if (chk)
        document.getElementById('formLogin').submit();
};
_setactive = function ()
{
    document.getElementById('country2').style.display = 'block';
    document.getElementById('country1').style.display = 'none';
    document.getElementById('country3').style.display = 'none';
}
function _bookMCity(frm) {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("hdOriginName"), "", "Origin can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("hdDestinationName"), "", "Destination can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField3"), "", "Pickup date can't be left blank.");
    if (chk == true)
        chk = isFilledText(document.getElementById("inputField4"), "", "Drop date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputField3").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        dd = document.getElementById("inputField4").value.trim().split(" ");
        if (dd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (dd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        var d = new Date();
        m = _getMonthInt(dd[1].replace(",", ""));
        d.setFullYear(dd[2], m, dd[0]);
        if (t > p)
        {
            alert("Please enter an appropriate pickup date");
            chk = false;
        }
        if (chk) {
            if (p > d)
            {
                alert("Please enter an appropriate drop date");
                chk = false;
            }
        }
    }
    if (chk) {
        if (document.getElementById('inputField'))
            document.getElementById('inputField').value = document.getElementById('inputField3').value;
        if (document.getElementById('inputField2'))
            document.getElementById('inputField2').value = document.getElementById('inputField4').value;
        document.getElementById(frm).action = webRoot + "/outstation/search/" + document.getElementById("hdOriginName").value.replace(/ /g, "-").toLowerCase() + "-to-" + document.getElementById("hdDestinationName").value.replace(/,/g, "-to-").replace(/ /g, "-").toLowerCase() + "/";
        document.getElementById(frm).submit();
    }
}

function _setSubmitBookin() {

    document.getElementById('formPaymentOptions').submit();
}
function _checkRegister() {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtName"), "", "Please fill in your full name.");
    if (chk)
        chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
    if (chk)
        chk = isFilledText(document.getElementById("txtEmail"), "", "Please fill in your email id.");
    if (chk)
        chk = isEmailAddr(document.getElementById("txtEmail"), "Please fill in a valid email id.");
    if (chk)
        document.getElementById('formRegister').submit();
}
;

function _checkNewPassword() {
    var chk;
    chk = true;
    if (chk)
        chk = isFilledText(document.getElementById("tokenID"), "", "Invalid token ID.");
    if (chk)
        chk = isFilledText(document.getElementById("txtPassword"), "", "Please fill in your password.");
    if (chk) {
        if (document.getElementById("txtPassword").value.trim() != document.getElementById("txtCPassword").value.trim()) {
            alert("Your passwords are not matching.");
            chk = false;
        }
    }
    if (chk)
        document.getElementById('frmNewPassword').submit();
}
function _doLogin() {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
    if (chk)
        chk = isFilledText(document.getElementById("txtPassword"), "", "Please fill in your password.");
    if (chk)
        document.getElementById('frmLogin').submit();
}
;



function _setTimeairport(hc, mc, da, ta, hf) {

    if (document.getElementById(hc).selectedIndex > 0) {
        //if(document.getElementById(mc).selectedIndex <= 0)
        //	document.getElementById(mc).selectedIndex = 1;
        var h = document.getElementById(hc).options[document.getElementById(hc).selectedIndex].value;
        var m = document.getElementById(mc).options[document.getElementById(mc).selectedIndex].value;
        document.getElementById(da).style.display = "block";
        var ampm = "AM";
        if (parseInt(h) >= 12) {
            ampm = "PM";
            if (parseInt(h) > 12)
                h = parseInt(h) - 12;
        }
		
        tminvar = 'tMin'+""+document.getElementById('ival').value ;
        thourvar = 'tHour'+""+document.getElementById('ival').value ;
        document.getElementById(tminvar).value = "" + m + "";
        document.getElementById(thourvar).value = " " + h + "";
       
	   if (document.getElementById('pickd'))
        {
            document.getElementById('pickd').innerHTML = " " + h + ":" + m + " " + ampm;
        }
		
        if (document.getElementById(ta))
            document.getElementById(ta).innerHTML = "You Selected: " + h + ":" + m + " " + ampm;
        if (document.getElementById(hf))
            document.getElementById(hf).value = h + ":" + m + " " + ampm;
    }
    else {
	
        document.getElementById(mc).selectedIndex = 0;
        if (document.getElementById(ta))
            document.getElementById(ta).innerHTML = "";
        document.getElementById(da).style.display = "none";
    }
}


function _setcabpurpose(cabid)
{
    var cabidval = cabid.value;
    document.getElementById('cabid').value = cabidval;
    if (cabidval > 0 || cabidval == 0)
    {

        var dataString = 'cabidval=' + cabidval;
        $.ajax({type: "POST", url: webRoot + "/city_list.php",
            beforeSend: function () {
                $("#ddlOriginL").html('Select city');
                $("#travelorigenL").html('<option value="0">Select travel location</option>');
                $("#ddltermainal").html('<option value="0">Select pickup location</option>');

            },
            data: dataString, success: function (response) {

                var txt = response;

                $("#ddlOriginL").html(txt);

            }});


    }
}


function _setTravellingTo(pickid)
{
    var pickcityid = pickid.value;
    var cname = pickid.options[pickid.selectedIndex].text;
    document.getElementById('hdDestinationIDL').value = pickcityid;
    document.getElementById('hdDestinationNameL').value = cname;
	//alert(document.getElementById('hdDestinationNameL').value);
	//alert(document.getElementById('hdDestinationIDL').value);


}



function _settrevelRecord(pickid)
{

    cabid = document.getElementById('cabid').value;
    var pickcityid = pickid.value;
	
    var cname = pickid.options[pickid.selectedIndex].text;
    document.getElementById('picuploactionid').value = pickcityid;
    document.getElementById('picuploactionName').value = cname;
    $('#loader6').html('<img src="'+ webRoot + '/images/loading.gif" />');
    var dataString = 'pickcityid=' + pickcityid + '&cabid=' + cabid;
    if ($("#ddltermainal").val() == 0)
    {
        $("#travelorigenL").html('<option value="0">Select travel location</option>');
    }
    else
    {
        $.ajax({type: "POST", url: webRoot + "/travel_list.php",
            beforeSend: function () {

                $("#travelorigenL").html('<option value="0">Select travel location</option>');


            },
            data: dataString, success: function (response) {

                var txt = response;

                $("#travelorigenL").html(txt);
				$('#loader6').hide();

            }});
    }


}


function  _setairportOrigin(cityId)
{
    cabid = document.getElementById('cabid').value;
    var cityIdval = cityId.value;
    var cname = cityId.options[cityId.selectedIndex].text;
    var cid = cityIdval;
    cname = cname.replace(" (excluding NCR regions)", "");
    if (document.getElementById('hdOriginID'))
        document.getElementById('hdOriginID').value = cid;
    if (document.getElementById('hdOriginName'))
        document.getElementById('hdOriginName').value = cname;
    if (document.getElementById('hdOriginIDL'))
        document.getElementById('hdOriginIDL').value = cid;
    if (document.getElementById('hdOriginNameL'))
        document.getElementById('hdOriginNameL').value = cname;
    $('#loader5').html('<img src="'+ webRoot + '/images/loading.gif" />');
    var dataString = 'cityid=' + cityIdval + '&cabpurposeid=' + cabid;
    $.ajax({type: "POST", url: webRoot + "/terminal_list.php",
        beforeSend: function () {
            $("#ddltermainal").html('<option value="0">Select pickup location</option>');
        },
        data: dataString, success: function (response) {

            var txt = response;

            $("#ddltermainal").html(txt);
			$('#loader5').hide();

        }});


}



_validateAirport = function ()
{
    var chk;
    chk = true;


    chk = isFilledSelect(document.getElementById("airportddlOrigin"), 0, "Cab purpose can't be left blank.", 0, "0");
    if (chk == true)
        chk = isFilledSelect(document.getElementById("ddlOriginL"), 0, "Origin can't be left blank.", 0, "0");
    if (chk == true)
        chk = isFilledSelect(document.getElementById("ddltermainal"), 0, "Pickup location can't be left blank.", 0, "0");

    if (chk == true)
        chk = isFilledSelect(document.getElementById("travelorigenL"), 0, "Travel location can't be left blank.", 0, "0");


    if (chk == true)
        chk = isFilledText(document.getElementById("inputField5"), "", "Pickup date can't be left blank.");
    if (chk) {
        pd = document.getElementById("inputField5").value.trim().split(" ");
        if (pd.length < 3) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[2].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[1].length < 4 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
        if (pd[0].length < 1 && chk) {
            alert("Invalid pickup date.");
            chk = false;
        }
    }
    if (chk) {
        td = document.getElementById("hdToday").value.trim().split(" ");
        var t = new Date();
        var m = _getMonthInt(td[1].replace(",", ""));
        t.setFullYear(td[2], m, td[0]);
        var p = new Date();
        m = _getMonthInt(pd[1].replace(",", ""));
        p.setFullYear(pd[2], m, pd[0]);
        var tm = pd[3];
        p.setHours(document.getElementById('tHour').options[document.getElementById('tHour').selectedIndex].value);
        p.setMinutes(document.getElementById('tMin').options[document.getElementById('tMin').selectedIndex].value);
        if (t > p)
        {
            alert("Please enter an appropriate pickup date & time.");
            chk = false;
        }
    }
    if (chk) {
        var diff = p.getTime() - t.getTime();
        diff = (diff / (1000 * 60 * 60));
        if (diff < 2)
        {
            alert('You can book at least 2 hours before pickup time.');
            chk = false;
        }
    }
    if (chk == true)
    {
        document.getElementById('hdOriginNameL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].text;
        document.getElementById('hdOriginIDL').value = document.getElementById('ddlOriginL').options[document.getElementById('ddlOriginL').selectedIndex].value;
    }
    if (chk == true) {
        document.getElementById('form2').action = webRoot + "/airport/search/" + document.getElementById("hdOriginNameL").value.replace(" ", "-").toLowerCase() + "/";
        document.getElementById('form2').submit();
    } else {
        return chk;
    }
    //return chk;
}




function _makeBookingairport() {
    setTimeout(
            function ()
            {

            }, 15000);

    var chk;
    chk = true;
	
    if (chk) {
        var bookDays = document.getElementById('bookDays').value;
        if (bookDays <= 0) {
            var diff = new Date('01/01/2012 ' + document.getElementById("userTime").value) - new Date('01/01/2012 ' + document.getElementById("bookTime").value);
            diff = (diff / (1000 * 60 * 60));
            if (diff < 2)
            {
                alert('You can book at least 2 hours before pickup time.');
                chk = false;
            }
        }
    }
	
	var thours =document.getElementById("tHourSL").selectedIndex;
	var tminute =document.getElementById("tMinSL").selectedIndex;
	if (chk) {
		if(thours==0)
		{
				alert("Pickup and dropoff address can not blank");
				chk = false;
		}
	}
	
    if (chk)
        chk = isFilledText(document.getElementById("address"), "", "Address can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "", "Mobile can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "10 digit mobile number", "Mobile can't be left blank.");
    var mNo = document.getElementById("monumberX").value;
    if (chk) {
        if (mNo.length > 10 || mNo.length < 10) {
            alert("Mobile number should be minimum of 10 digits.\nPlease check your mobile number");
            document.getElementById("monumberX").focus();
            chk = false;
        }
    }
	
	if(chk){
	    if(mNo.substr(0,1) != "9" && mNo.substr(0,1) != "8" && mNo.substr(0,1) != "7"){
		alert("Invalid mobile number. Please enter correct mobile number.");
		chk = false;
	    }
	}
	
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "Name", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtemail"), "Email", "Email can't be left blank.");
    if (chk)
        chk = isEmailAddr(document.getElementById("txtemail"), "Please fill in a valid email id.");
    if (chk) {
        var adr = document.getElementById('address').value;
        adr = adr.replace('\n', '<br />');
        adr = adr.replace('\r', '<br />');
        adr = adr.replace('\n\r', '<br />');
        document.getElementById('address').value = adr;
        /*if ($("#firstTimeLogin").val() != '1')
         {
         if (document.getElementById('empcode').value != '' && document.getElementById('empcode').value != 'Promotion code') {
         
         if ( document.getElementById('retrycode')) {
         alert("Please apply promotion code.");
         chk = false;
         }
         }
         } */

        if (chk) {

            /*if ($("#hdSchemeCoupon").length > 0)
             {
             $("#empcode").val($("#hdSchemeCoupon").val());
             
             $("input[name=discountAmt]").val($("#hdSchemeDiscount").val());
             
             
             extra_tot = parseInt($("#asc").html()) + parseInt($("#cnvc").html()) + parseInt($("#slc").html());
             
             extravat = (parseInt(extra_tot) * parseFloat($("#vatafterfinal").val()) / 100);
             
             extra_tot = extra_tot + extravat;
             
             totFareGrossflat = $("#totFareGrossflat").val();
             $("#totFareGrossflat").val(parseInt(extra_tot) + parseInt(totFareGrossflat));
             }
             else
             {
             
             } 
             if ($("#firstTimeLogin").val() == '1' && ($("#empcode").val() != ''))
             {
             ival = $("#ival").val();
             
             hdpickdate = $("#pickdate" + ival).val();
             hdpickdateArr = hdpickdate.split("/");
             newpickdate = hdpickdateArr[2] + "-" + hdpickdateArr[0] + "-" + hdpickdateArr[1];
             hddropdate = $("#dropdate" + ival).val();
             hddroparray = hddropdate.split("/");
             newdrop = hddroparray[2] + "-" + hddroparray[0] + "-" + hddroparray[1];
             var newpickdate = new Date(newpickdate);
             var newdrop = new Date(newdrop);
             empcodebyabhi = $("#empcode").val();
             empcodebyabhi = empcodebyabhi.toUpperCase();
             
             if (empcodebyabhi != 'MYLESSECRET')
             {
             _checkDiscount(ival);
             
             _showPopUp('html', '<br /><div style="text-align:center;"><img src="http://www.carzonrent.com/images/loader.gif" border="0" /><br /><br />Your booking is in Process..<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
             setTimeout(
             function ()
             {
             document.getElementById('formTravelDetail').submit();
             }, 15000);
             }
             else
             {
             alert("Please enter coupon code");
             }
 
             } 
             else
             { */
            document.getElementById('formTravelDetail').submit();
            //}

        }

    }
}







function _setTime(hc, mc, da, ta, hf) {
    if (document.getElementById(hc).selectedIndex > 0) {
        //if(document.getElementById(mc).selectedIndex <= 0)
        //	document.getElementById(mc).selectedIndex = 1;
        var h = document.getElementById(hc).options[document.getElementById(hc).selectedIndex].value;
        var m = document.getElementById(mc).options[document.getElementById(mc).selectedIndex].value;
        document.getElementById(da).style.display = "block";
        var ampm = "AM";
        if (parseInt(h) >= 12) {
            ampm = "PM";
            if (parseInt(h) > 12)
                h = parseInt(h) - 12;
        }
        if (document.getElementById(ta))
            document.getElementById(ta).innerHTML = "You Selected: " + h + ":" + m + " " + ampm;
        if (document.getElementById(hf))
            document.getElementById(hf).value = h + ":" + m + " " + ampm;
    }
    else {
        document.getElementById(mc).selectedIndex = 0;
        if (document.getElementById(ta))
            document.getElementById(ta).innerHTML = "";
        document.getElementById(da).style.display = "none";
    }
}
;


function _makeBooking() {

    setTimeout(
            function ()
            {

            }, 15000);

    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("userTime"), "", "Please choose pick time.");
    if (chk) {
        var bookDays = document.getElementById('bookDays').value;
        if (bookDays <= 0) {
            var diff = new Date('01/01/2012 ' + document.getElementById("userTime").value) - new Date('01/01/2012 ' + document.getElementById("bookTime").value);
            diff = (diff / (1000 * 60 * 60));
            if (diff < 2)
            {
                alert('You can book at least 2 hours before pickup time.');
                chk = false;
            }
        }
    }
	
	var thours =document.getElementById("tHourSL").selectedIndex;
	var tminute =document.getElementById("tMinSL").selectedIndex;
	if (chk) {
		if(thours==0)
		{
				alert("Pickup and dropoff address can not blank");
				chk = false;
		}
	}
	

    if (chk) {
        if ($("#3").is(":checked"))
        {
            chk = isFilledText(document.getElementById("pickdrop"), "", "Pickup and dropoff address can not blank.");
        }

    }


    if (chk) {
        var TType = document.getElementsByName("hdTourtype")[0].value;
        if (TType == "Selfdrive") {
            chk = isFilledSelect(document.getElementById("ddlSubLoc"), "Select Pickup Location", "Choose a pickup location.", 1, "0");
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("address"), "", "Address can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "", "Mobile can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "10 digit mobile number", "Mobile can't be left blank.");
    var mNo = document.getElementById("monumberX").value;
    if (chk) {
        if (mNo.length > 10 || mNo.length < 10) {
            alert("Mobile number should be minimum of 10 digits.\nPlease check your mobile number");
            document.getElementById("monumberX").focus();
            chk = false;
        }
    }
    if(chk){
       if(mNo.substr(0,1) != "9" && mNo.substr(0,1) != "8" && mNo.substr(0,1) != "7"){
    	alert("Invalid mobile number. Please enter correct mobile number.");
    	chk = false;
    }
    }

    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "Name", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtemail"), "Email", "Email can't be left blank.");
    if (chk)
        chk = isEmailAddr(document.getElementById("txtemail"), "Please fill in a valid email id.");
    if (chk) {
        var adr = document.getElementById('address').value;
        adr = adr.replace('\n', '<br />');
        adr = adr.replace('\r', '<br />');
        adr = adr.replace('\n\r', '<br />');
        document.getElementById('address').value = adr;
        if ($("#firstTimeLogin").val() != '1')
        {
            if (document.getElementById('empcode').value != '' && document.getElementById('empcode').value != 'Promotion code') {
                //alert(document.getElementById('empcode').value);       
                if ( document.getElementById('retrycode')) {
                    alert("Please apply promotion code.");
                    chk = false;
                }
            }
        }

        if (chk) {

            if ($("#hdSchemeCoupon").length > 0)
            {
                $("#empcode").val($("#hdSchemeCoupon").val());

                $("input[name=discountAmt]").val($("#hdSchemeDiscount").val());


                extra_tot = parseInt($("#asc").html()) + parseInt($("#cnvc").html()) + parseInt($("#slc").html());

                extravat = (parseInt(extra_tot) * parseFloat($("#vatafterfinal").val()) / 100);

                extra_tot = extra_tot + extravat;

                totFareGrossflat = $("#totFareGrossflat").val();
                $("#totFareGrossflat").val(parseInt(extra_tot) + parseInt(totFareGrossflat));
            }
            else
            {

            }
            if ($("#firstTimeLogin").val() == '1' && ($("#empcode").val() != ''))
            {
                ival = $("#ival").val();

                hdpickdate = $("#pickdate" + ival).val();
                hdpickdateArr = hdpickdate.split("/");
                newpickdate = hdpickdateArr[2] + "-" + hdpickdateArr[0] + "-" + hdpickdateArr[1];
                hddropdate = $("#dropdate" + ival).val();
                hddroparray = hddropdate.split("/");
                newdrop = hddroparray[2] + "-" + hddroparray[0] + "-" + hddroparray[1];
                var newpickdate = new Date(newpickdate);
                var newdrop = new Date(newdrop);
                empcodebyabhi = $("#empcode").val();
                empcodebyabhi = empcodebyabhi.toUpperCase();

                if (empcodebyabhi != 'MYLESSECRET')
                {
                    _checkDiscount(ival);

                    _showPopUp('html', '<br /><div style="text-align:center;"><img src="http://www.carzonrent.com/images/loader.gif" border="0" /><br /><br />Your booking is in Process..<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
                    setTimeout(
                            function ()
                            {
                                document.getElementById('formTravelDetail').submit();
                            }, 15000);
                }
                else
                {
                    alert("Please enter coupon code");
                }





            }
            else
            {
                document.getElementById('formTravelDetail').submit();
            }

        }

    }
}
;

function _getPBRedeem(xi) {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("userTime"), "", "Please choose pick time.");
    if (chk) {
        var bookDays = document.getElementById('bookDays').value;
        if (bookDays <= 0) {
            var diff = new Date('01/01/2012 ' + document.getElementById("userTime").value) - new Date('01/01/2012 ' + document.getElementById("bookTime").value);
            diff = (diff / (1000 * 60 * 60));
            if (diff < 2)
            {
                alert('You can book at least 2 hours before pickup time.');
                chk = false;
            }
        }
    }
    // added vinod  pick
    if (chk) {
        if ($("#3").is(":checked"))
        {

            chk = isFilledText(document.getElementById("pickdrop"), "", "Pickup and dropoff address can not blank.");
        }

    }

    if (chk) {
        var TType = document.getElementsByName("hdTourtype")[0].value;
        if (TType == "Selfdrive") {
            chk = isFilledSelect(document.getElementById("ddlSubLoc"), "Select Pickup Location", "Choose a pickup location.", 1, "0");
        }
    }
    // added vinod pick


    if (chk)
        chk = isFilledText(document.getElementById("address"), "", "Address can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "", "Mobile can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("monumberX"), "10 digit mobile number", "Mobile can't be left blank.");
    var mNo = document.getElementById("monumberX").value;
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "Name", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "", "Name can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtemail"), "Email", "Email can't be left blank.");
    if (chk)
        chk = isEmailAddr(document.getElementById("txtemail"), "Please fill in a valid email id.");
    if (chk) {
        var adr = document.getElementById('address').value;
        adr = adr.replace('\n', '<br />');
        adr = adr.replace('\r', '<br />');
        adr = adr.replace('\n\r', '<br />');
        document.getElementById('address').value = adr;
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtcardnopb"), "", "PayBack Loyality Card number can't be left blank.");
    if (chk)
        chk = isFilledText(document.getElementById("txtpbpoints"), "", "Points to Redeem can't be left blank.");
    if (chk && document.getElementById('txtcardnopb').value != "" && document.getElementById('txtpbpoints').value != "") {




        var availablepoints = parseInt(document.getElementById('txtpointsincard').value);
        var pointstoredeem = parseInt(document.getElementById('txtpbpoints').value);
        var txtamounttoredeem = parseInt(document.getElementById('txtamounttoredeem').value);
        var txttotalfareval =  parseInt(document.getElementById('spPay').innerHTML);
        if(txttotalfareval == txtamounttoredeem){
            alert("Total redeem amount should be less than total fare");
            return false;
        }else if ((availablepoints >= pointstoredeem) ) {
            document.getElementById('formTravelDetail').action = webRoot + "/intermediate.php";
            document.getElementById('formTravelDetail').submit();
        }
        else {
            alert("Insufficient points in your PayBack card.");
            return false;
        }
    }
}

/*Made by Aamir starts*/
function _redeemPoints()
{
    var aj;
    var cardNo = document.getElementById('txtcardnopb').value;
    //var points = document.getElementById('txtpbpoints').value;
    document.getElementById('txtpointsincard').readOnly = false;
    document.getElementById('txtpointsincard').value = "Please wait...";
    document.getElementById('txtpointsincard').readOnly = true;
    if (window.XMLHttpRequest)
        aj = new XMLHttpRequest;
    else if (window.ActiveXObject)
        aj = new ActiveXObject("Microsoft.XMLHTTP");
    aj.open("POST", webRoot + "/check-pb-balance.php", true);
    aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    aj.onreadystatechange = function ()
    {
        if (aj.readyState < 4)
        {
            document.getElementById('txtpbpoints').readOnly = true;
        }
        if (aj.readyState == 4)
        {
            var d = aj.responseText;
            if (isNaN(d))
            {
                alert("Please check your PayBack Loyality Card number and try again");
                document.getElementById('txtpointsincard').readOnly = false;
                document.getElementById('txtpointsincard').value = "";
                document.getElementById('txtpointsincard').readOnly = true;
            } else
            {
                var tFare = document.getElementById('totFarePB').value;
                var pointsRq = tFare * 4;
                if (pointsRq <= d) {
                    document.getElementById('txtpointsincard').value = pointsRq;
                    document.getElementById('txtpbpoints').value = pointsRq;
                    document.getElementById('txtamounttoredeem').value = tFare;
                } else {
                    document.getElementById('txtpointsincard').value = d;
                    document.getElementById('txtpbpoints').value = d;
                    document.getElementById('txtamounttoredeem').value = parseInt(d / 4);
                }

                document.getElementById('txtpbpoints').readOnly = false;
                document.getElementById('txtpointsincard').value = d;
                //if (d > 0 && d >= parseInt(points))
                //{
                //	document.getElementById('formTravelDetail').action = webRoot + "/intermediate.php";
                //	document.getElementById('formTravelDetail').submit();
                //} else
                //{
                //	alert("Insufficient points in your PayBack card.");
                //}
            }
        }
    }
    var values = "lcNo=" + cardNo;
    aj.send(values);
}
function _redeemPoints2()
{
    var aj;
    var cardNo = document.getElementById('txtcardnopb').value;
    //var points = document.getElementById('txtpbpoints').value;
    document.getElementById('txtpointsincard').readOnly = false;
    document.getElementById('txtpointsincard').value = "Please wait...";
    document.getElementById('txtpointsincard').readOnly = true;
    if (window.XMLHttpRequest)
        aj = new XMLHttpRequest;
    else if (window.ActiveXObject)
        aj = new ActiveXObject("Microsoft.XMLHTTP");
    aj.open("POST", webRoot + "/check-pb-balance.php", true);
    aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    aj.onreadystatechange = function ()
    {
        if (aj.readyState < 4)
        {
            document.getElementById('txtpbpoints').readOnly = true;
        }
        if (aj.readyState == 4)
        {
            var d = aj.responseText;
            if (isNaN(d))
            {
                alert("Please check your PayBack Loyality Card number and try again");
                document.getElementById('txtpointsincard').readOnly = false;
                document.getElementById('txtpointsincard').value = "";
                document.getElementById('txtpointsincard').readOnly = true;
            } else
            {
                var tFare = document.getElementById('txtAmount').value;
                var pointsRq = tFare * 4;
                if (pointsRq <= d) {
                    document.getElementById('txtpointsincard').value = pointsRq;
                    document.getElementById('txtpbpoints').value = pointsRq;
                    document.getElementById('txtamounttoredeem').value = tFare;
                } else {
                    document.getElementById('txtpointsincard').value = d;
                    document.getElementById('txtpbpoints').value = d;
                    document.getElementById('txtamounttoredeem').value = parseInt(d / 4);
                }

                document.getElementById('txtpbpoints').readOnly = false;
                document.getElementById('txtpointsincard').value = d;
                //if (d > 0 && d >= parseInt(points))
                //{
                //	document.getElementById('formTravelDetail').action = webRoot + "/intermediate.php";
                //	document.getElementById('formTravelDetail').submit();
                //} else
                //{
                //	alert("Insufficient points in your PayBack card.");
                //}
            }
        }
    }
    var values = "lcNo=" + cardNo;
    aj.send(values);
}
/*Made by Aamir ends*/


function _paymentCheck(r) {
    if (r == 'rdo1') {
        if (document.getElementById('rdo2'))
            document.getElementById('rdo2').checked = false;
        if (document.getElementById('rdo3'))
            document.getElementById('rdo3').checked = false;
    }
    if (r == 'rdo2') {
        if (document.getElementById('rdo1'))
            document.getElementById('rdo1').checked = false;
        if (document.getElementById('rdo3'))
            document.getElementById('rdo3').checked = false;
    }
    if (r == 'rdo3') {
        if (document.getElementById('rdo1'))
            document.getElementById('rdo1').checked = false;
        if (document.getElementById('rdo2'))
            document.getElementById('rdo2').checked = false;
    }
    _showGPDetails();
}
function _checkPaymentSubmit(frm) {
    var chk;
    chk = true;
    if (document.getElementById('tc')) {
        if (document.getElementById('tc').checked == false) {
            chk = false;
            alert("You need to read and agree to the Carzonrent (India) Terms & Conditions.");
        }
    }
    if (chk) {
        _disablePage();
        _showPopUp('html', '<br /><div style="text-align:center;"><img src="http://www.carzonrent.com/images/loader.gif" border="0" /><br /><br />Your booking is being processed.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
        /*26th August Aamir - to set the transaction on Payment Gateway starts here*/
        var aj;
        var order_id = document.getElementById("Order_Id").value;

        if (window.XMLHttpRequest)
            aj = new XMLHttpRequest;
        else if (window.ActiveXObject)
            aj = new ActiveXObject("Microsoft.XMLHTTP");
        aj.open("POST", webRoot + "/set-on-payment-gateway.php", true);
        aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        aj.onreadystatechange = function () {
            if (aj.readyState == 4) {
                frm.submit();
            }
        }
        var values = "order_id=" + order_id;
        aj.send(values);
        /*26th August Aamir - to set the transaction on Payment Gateway starts here*/
    }
}
;

function _checkForgotPassword() {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
    if (chk)
        document.getElementById('frmForgotPassword').submit();
}

function _sendBusinessEnquiries() {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtName"), "", "Please fill in your full name.");
    if (chk)
        chk = isFilledText(document.getElementById("txtCompany"), "", "Please fill in your company name.");
    if (chk)
        chk = isFilledText(document.getElementById("txtContactNum"), "", "Please fill in your contact number.");
    if (chk)
        chk = isFilledText(document.getElementById("txtDesignation"), "", "Please fill in your designation.");
    //if(chk)
    //	chk = isFilledText(document.getElementById("txtOrganization"), "", "Please fill in your organization name.");
    if (chk)
        chk = isFilledText(document.getElementById("txtEmailID"), "", "Please fill in your email id.");
    if (chk)
        chk = isEmailAddr(document.getElementById("txtEmailID"), "Please fill a valid email id.");
    if (chk)
        chk = isFilledText(document.getElementById("txtRequirement"), "", "Please write your requirement.");
    if (chk)
        document.getElementById('frmBusinessEnquiries').submit();
}
;

function _checkInvoice() {
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
    if (chk)
        chk = isFilledText(document.getElementById("txtBookingID"), "", "Please fill in your COR booking ID.");
    if (chk) {
        var aj;
        if (window.XMLHttpRequest)
            aj = new XMLHttpRequest;
        else if (window.ActiveXObject)
            aj = new ActiveXObject("Microsoft.XMLHTTP");
        var param = "txtBookingID=" + document.getElementById("txtBookingID").value + "&txtMobile=" + document.getElementById("txtMobile").value + "&tourtype=" + document.getElementById("tourtype").value;

        aj.open("GET", "get-invoice.php?" + param, true);
        aj.send(null);
        aj.onreadystatechange = function () {
            if (aj.readyState == 1) {
                document.getElementById('msg').innerHTML = "Please wait while we check for mobile number and COR ID.<br /><br />";
            }
            else if (aj.readyState == 4) {
                var r = "";
                if (aj.status == 200) {
                    r = aj.responseText;
                    if (r != "") {

                        //arr = r.split("/");
                        //alert(arr);
                        //document.getElementById('msg').innerHTML = "<a href='" + arr[1] + "' target='_blank'>Click here</a> to get invoice print out.<br /><br />";
                        //window.open(arr[1] , "invoice", "width=600,height=600");
                        //alert(arr[1]);
						//alert(r);
                        document.getElementById('msg').innerHTML = "<a href='" + r + "' target='_blank'>Click here</a> to get invoice print out.<br /><br />";
                        window.open(r, "invoice", "width=600,height=600");
                    }
                    else {
                        document.getElementById('msg').innerHTML = "No invoice found. Please check your mobile number and COR ID and try again.<br /><br />";
                    }
                    aj = null;
                }
            }
        }
    }
}

/*function _checkInvoice() {
 var chk;
 chk = true;
 chk = isFilledText(document.getElementById("txtMobile"), "", "Please fill in your mobile number.");
 if (chk)
 chk = isFilledText(document.getElementById("txtBookingID"), "", "Please fill in your COR booking ID.");
 if (chk) {
 var aj;
 if (window.XMLHttpRequest)
 aj = new XMLHttpRequest;
 else if (window.ActiveXObject)
 aj = new ActiveXObject("Microsoft.XMLHTTP");
 var param = "txtBookingID=" + document.getElementById("txtBookingID").value + "&txtMobile=" + document.getElementById("txtMobile").value;
 
 aj.open("GET", "get-invoice.php?" + param, true);
 aj.send(null);
 aj.onreadystatechange = function () {
 if (aj.readyState == 1) {
 document.getElementById('msg').innerHTML = "Please wait while we check for mobile number and COR ID.<br /><br />";
 }
 else if (aj.readyState == 4) {
 var r = "";
 if (aj.status == 200) {
 r = aj.responseText;
 if (r != "") {
 
 //arr = r.split("/");
 //alert(arr);
 //document.getElementById('msg').innerHTML = "<a href='" + arr[1] + "' target='_blank'>Click here</a> to get invoice print out.<br /><br />";
 //window.open(arr[1] , "invoice", "width=600,height=600");
 //alert(arr[1]);
 document.getElementById('msg').innerHTML = "<a href='" + r + "' target='_blank'>Click here</a> to get invoice print out.<br /><br />";
 window.open(r, "invoice", "width=600,height=600");
 }
 else {
 document.getElementById('msg').innerHTML = "No invoice found. Please check your mobile number and COR ID and try again.<br /><br />";
 }
 aj = null;
 }
 }
 }
 }
 }*/

function _cancelBooking(b, u, r) {

    if (b.trim() != "" && u.trim() != "") {
        var cnf = 'Cancel';

        if (cnf == 'Cancel') {

            _disablePage();
            _showPopUp('html', '<br /><div style="text-align:center;">' +
                    '<img src="http://www.carzonrent.com/images/loader.gif" border="0" />' +
                    '<br /><br />Your booking is being cancelled.<br /><br />' +
                    'Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');

            var param = "txtBookingID=" + b + "&CCID=" + u +"&cancelReason=" + r;


            $.ajax({
                url: "cancel-booking.php",
                type: "POST",
                data: param,
                success: function (data) {//alert(data);return false;
                    window.location = "./cancel-confirm.php";

                },
                error: function () {
                    alert("failure");

                }
            });

        }
    }
    else
        alert("Please check your Booking ID or login status.");
}
;
var ca = -1;
function _getCities(startChar, e, d, hdi, hdn, a, arr, x, y, di, w, nd) {
    var kc = getKeyCode(e);
    if (startChar.length >= 3) {
        //alert(kc);
        //38 - Up Key
        //40 - Down Key
        //13 - Enter Key
        var ar = arr;
        if (startChar != "" && ((kc >= 65 && kc <= 90) || (kc >= 97 && kc <= 122) || (kc == 8 || kc == 46 || kc == 32 || kc == 9))) {
            document.getElementById(a).style.display = "none";
            document.getElementById(a).innerHTML = "";
            var isF = 0;
            ca = -1;
            for (i = 0; i < ar.length; i++) {
                if (ar[i][0].toLowerCase().indexOf(startChar.toLowerCase()) > -1)
                {
                    cname = ar[i][0].split(",")[0].replace('*', '');
                    if (nd != undefined)
                        document.getElementById(a).innerHTML += "<a href='javascript: void(0);' id='" + i + "' onclick=\"javascript: _setCity(" + ar[i][1] + ",'" + cname + "','" + hdi + "','" + hdn + "','" + a + "','" + d + "'," + di + ",'" + nd + "');\" name='ac'>" + ar[i][0].replace('*', '') + "</a>";
                    else
                        document.getElementById(a).innerHTML += "<a href='javascript: void(0);' onclick=\"javascript: _setCity(" + ar[i][1] + ",'" + cname + "','" + hdi + "','" + hdn + "','" + a + "','" + d + "'," + di + ");\" id='" + i + "' name='ac'>" + ar[i][0].replace('*', '') + "</a>";
                    isF++;
                    if (isF >= 10)
                        break;
                }
            }

            if (isF >= 1) {
                document.getElementById(a).style.display = "block";
                window.onclick = function () {
                    document.getElementById(a).style.display = "none";
                }
                document.onclick = function () {
                    document.getElementById(a).style.display = "none";
                }
            }

        }
        else if (startChar != "" && kc == 40) {
            _setBGWhite();
            ca++;
            if (document.getElementsByName('ac')[ca])
                document.getElementsByName('ac')[ca].style.backgroundColor = "#f2f2f2";
            if (ca >= document.getElementsByName('ac').length)
                ca = -1;
        }
        else if (startChar != "" && kc == 38) {
            _setBGWhite();
            ca--;
            if (document.getElementsByName('ac')[ca])
                document.getElementsByName('ac')[ca].style.backgroundColor = "#f2f2f2";
            if (ca < 0)
                ca = -1;
        }
        else if (startChar != "" && kc == 13) {
            if (document.getElementsByName('ac')[ca]) {
                var cid = document.getElementsByName('ac')[ca].id;
                cname = ar[cid][0].split(",")[0].replace('*', '');
                _setCity(ar[cid][1], cname, hdi, hdn, a, d, di, nd);
                ca = -1;
            }
        }
    } else {
        document.getElementById(a).style.display = "none";
        document.getElementById(a).innerHTML = "";
        if (document.getElementById(hdi))
            document.getElementById(hdi).value = "";
        if (document.getElementById(hdn))
            document.getElementById(hdn).value = "";
        if (nd != undefined) {
            document.getElementById(nd).value = "";
        }
    }
}
;
function _setBGWhite() {
    var d = document.getElementsByName('ac');
    for (i = 0; i < d.length; i++) {
        if (document.getElementsByName('ac')[i])
            document.getElementsByName('ac')[i].style.backgroundColor = "#fff";
    }
}
;

var arrCityID = new Array();
var arrCityName = new Array();
function _setCity(cid, cname, hdi, hdn, a, d, i, nd) {
    arrCityID[i] = cid;
    arrCityName[i] = cname;
    if (document.getElementById(hdi))
        document.getElementById(hdi).value = arrCityID.join(",");
    if (document.getElementById('tripadvisordestinationid'))
    {
        document.getElementById('tripadvisordestinationid').value = arrCityID.join(",");  //trip advisor
        document.getElementById('tripadvisor_msg1').style.display = "block";//trip advisor
        document.getElementById('tripadvisor_msg').style.display = "none";//trip advisor
    }
    if (document.getElementById(hdn))
        document.getElementById(hdn).value = arrCityName.join(",");
    if (document.getElementById(d))
        document.getElementById(d).value = cname;
    if (nd != undefined) {
        document.getElementById(nd).value = cname;
    }
    document.getElementById(a).style.display = "none";
}
;

function _setOrigin(i, t) {
    var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
    var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
    cname = cname.replace(" (excluding NCR regions)", "");
    if (document.getElementById('hdOriginID'))
        document.getElementById('hdOriginID').value = cid;
    if (document.getElementById('hdOriginName'))
        document.getElementById('hdOriginName').value = cname;
    if (document.getElementById('hdOriginIDL'))
        document.getElementById('hdOriginIDL').value = cid;
    if (document.getElementById('hdOriginNameL'))
        document.getElementById('hdOriginNameL').value = cname;
    if (document.getElementById('hdOriginIDSF'))
        document.getElementById('hdOriginIDSF').value = cid;
    if (document.getElementById('hdOriginNameSF'))
        document.getElementById('hdOriginNameSF').value = cname;
    //window.location = "./?c=" + cname + "&ci=" + cid;
//	if(cname.trim() != "Select Origin"){
//                if(t != undefined)
//                    window.location = webRoot + "/car-rental-" + cname + "-" + cid + "/#" + t;
//                else
//                    window.location = webRoot + "/car-rental-" + cname + "-" + cid + "/";
//	}
}
;

function _setOriginCP(i) {
    var cid = document.getElementById(i).options[document.getElementById(i).selectedIndex].value;
    var cname = document.getElementById(i).options[document.getElementById(i).selectedIndex].text;
    document.getElementById('hdOriginID').value = cid;
    document.getElementById('hdOriginName').value = cname;
    window.location = "./packages.php?c=" + cname + "&ci=" + cid;
}
;

var mc = 2;
function _addMore() {
    if (mc < 5) {
        document.getElementById('mc-tab').rows[mc].style.display = "block";
        document.getElementById('mc-origin' + (mc + 1)).value = document.getElementById('mc-destination' + mc).value;
        mc++;
    }
    if (mc >= 5)
        document.getElementById('addmore').style.display = "none";
}
;

function _setHour(h) {
    document.getElementById('selCabHour').innerHTML = (h * 10) + " kms included";
    document.getElementById('selCH').style.display = "block";
}
;

_sendimageentry = function () {
    var chk;
    chk = true;
    if (chk)
    {
        if (document.getElementById("txtsector").value == "Choose sector")
        {
            alert("Please enter your sector.");
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtsector"), "", "Please choose your sector.");
    if (chk)
    {
        if (document.getElementById("txtcompany").value == "Enter company name")
        {
            alert("Please enter your company name.");
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtcompany"), "", "Please enter your company name.");
    if (chk)
    {
        if (document.getElementById("txtstrength").value == "Enter employee strength")
        {
            alert("Please enter your employee strength.");
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtstrength"), "", "Please fill in your mobile number.");
    if (chk)
        _sendtomail();
    return chk;
}
var aj;
_sendleasingenquiry = function () {
    var chk;
    chk = true;
    if (chk)
    {
        if (document.getElementById("txtorg").value == "Organization")
        {
            document.getElementById("msgsendmail").innerHTML = "Please enter your organization name.";
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtorg"), "", "Please enter your organization name.", "msgsendmail");
    if (chk)
    {
        if (document.getElementById("txtname").value == "Name")
        {
            document.getElementById("msgsendmail").innerHTML = "Please fill in your name.";
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtname"), "", "Please fill in your name.", "msgsendmail");
    if (chk)
    {
        if (document.getElementById("txtemailid").value == "Email")
        {
            document.getElementById("msgsendmail").innerHTML = "Please fill in your emailid.";
            chk = false;
        }
    }
    if (chk)
        chk = isEmailAddr(document.getElementById("txtemailid"), "Please fill a valid email id.", "msgsendmail");
    if (chk)
    {
        if (document.getElementById("txtmobumber").value == "Mobile")
        {
            document.getElementById("msgsendmail").innerHTML = "Please fill in your mobile number.";
            chk = false;
        }
    }
    if (chk)
    {
        var mNo = document.getElementById("txtmobumber").value;
        if (mNo.length < 10)
        {
            document.getElementById("msgsendmail").innerHTML = "Mobile number must be of 10 digit.";
            chk = false;
        }
    }
    if (chk)
        chk = isFilledSelect(document.getElementById("ddlemployees"), 0, "Plesae select employee strength", 0, "msgsendmail");
    if (chk)
    {
        if (document.getElementById("txtlocation").value == "City")
        {
            document.getElementById("msgsendmail").innerHTML = "Please enter your city.";
            chk = false;
        }
    }
    if (chk)
        chk = isFilledText(document.getElementById("txtlocation"), "", "Please enter your city.", "msgsendmail");
    if (chk)
        _sendleasingmail();
    return chk;
}
var aj;
_sendleasingmail = function () {
    var o = document.getElementById("txtorg").value;
    var n = document.getElementById("txtname").value;
    var e = document.getElementById("txtemailid").value;
    var m = document.getElementById("txtmobumber").value;
    var em = document.getElementById("ddlemployees").value;
    var l = document.getElementById("txtlocation").value;
    if (window.XMLHttpRequest)
        aj = new XMLHttpRequest;
    else if (window.ActiveXObject)
        aj = new ActiveXObject("Microsoft.XMLHTTP");
    aj.open("POST", "sendmail.php", true);
    aj.onreadystatechange = _httpsendleasingmail;
    aj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var values = "o=" + o + "&n=" + n + "&e=" + e + "&m=" + m + "&em=" + em + "&l=" + l;
    aj.send(values);
};
_httpsendleasingmail = function () {
    if (aj.readyState == 4)
    {
        var disp = aj.responseText;
        if (disp != "" && disp != undefined)
        {
            if (disp == 1) {
                document.getElementById('msgsendmail').innerHTML = "Thank you. Your enquiry has been<br />sent to our marketing representative.";
                document.getElementById("txtorg").value = "";
                document.getElementById("txtname").value = "";
                document.getElementById("txtemailid").value = "";
                document.getElementById("txtmobumber").value = "";
                document.getElementById("ddlemployees").selectedIndex = 0;
                document.getElementById("txtlocation").value = "";
            }
            else {
                document.getElementById('msgsendmail').innerHTML = "Your enquiry could not saved.";
            }
        }
    }
    else
        document.getElementById('msgsendmail').innerHTML = "Please wait...";
};

function _setDropDate(pi, di) {
    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));
    p.setFullYear(pd[2], m, pd[0]);
    var d = new Date();
    m = _getMonthInt(dd[1].replace(",", ""));
    d.setFullYear(dd[2], m, dd[0]);
    if (d < p)
        document.getElementById(di).value = document.getElementById(pi).value;
    $("#" + di).datepicker("option", "minDate", p);
}
;

var month = new Array();
month[0] = "Jan";
month[1] = "Feb";
month[2] = "Mar";
month[3] = "Apr";
month[4] = "May";
month[5] = "Jun";
month[6] = "Jul";
month[7] = "Aug";
month[8] = "Sep";
month[9] = "Oct";
month[10] = "Nov";
month[11] = "Dec";

function _setDropDateMyles(pi, di) {
    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    var d = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));
    if (document.getElementById('chkPkgType')) {

        if (document.getElementById('chkPkgType').value == 'Weekly') {
            var nxtD = eval(parseInt(pd[0]) + 7);
        } else if (document.getElementById('chkPkgType').value == 'Monthly') {
            var nxtD = eval(parseInt(pd[0]) + 30);
        } else {
            var nxtD = eval(parseInt(pd[0]) + 1);
        }

    }
    // alert(nxtD)
    document.getElementById('nWeeks').selectedIndex = 0
    document.getElementById('nMonths').selectedIndex = 0
    p.setFullYear(pd[2], m, pd[0]);
    d.setFullYear(pd[2], m, nxtD);
    document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
    try {
        $('#inputFieldSF2').datetimepicker({
            format: 'd M, Y H:i',
            minDate: d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
            step: 30
        });
    } catch (e) {
    }

    document.getElementById('pickdate').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();
    if (document.getElementById('dropdate'))
        document.getElementById('dropdate').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    var tm = pd[3].split(":");

    document.getElementById('tHourP').value = tm[0];
    document.getElementById('tMinP').value = tm[1];

    if (document.getElementById('dropdate')) {
        document.getElementById('tHourD').value = tm[0];
        document.getElementById('tMinD').value = tm[1];
    }

}
;

_setDropOffDate = function (di)
{

    var d = new Date();
    if (document.getElementById(di) != null)
    {
        dd = document.getElementById(di).value.trim().split(" ");
        if (dd instanceof Array)
        {
            if (dd.length > 0)
            {
                if (typeof dd[1] != 'undefined')
                {
                    m = _getMonthInt(dd[1].replace(",", ""));
                    d.setFullYear(dd[2], m, dd[0]);
                }
            }
        }
    }

    document.getElementById('dropdate').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    if (typeof dd[3] != 'undefined')
    {
        var tm = dd[3].split(":");
    }

    if (tm instanceof Array)
    {
        if (typeof tm[0] != 'undefined')
        {
            document.getElementById('tHourD').value = tm[0];
        }
        if (typeof tm[1] != 'undefined')
        {
            document.getElementById('tMinD').value = tm[1];
        }
    }


}

function _getDiscount() {
    var dAJ;
    var e = "";
    if (document.getElementById("dpcEmailID"))
        e = document.getElementById("dpcEmailID").value;
    if (e == "" || e == undefined) {
        alert("Please fill in your E-mail ID to get the discount coupon.");
        return;
    }
    else {
        if (!isEmailAddr(document.getElementById("dpcEmailID"), "Please fill in a valid email id.")) {
            alert("Please fill in a valid E-mail ID.");
            return;
        }
    }
    var c = "";
    if (document.getElementById("hdOriginID0"))
        c = document.getElementById("hdOriginID0").value;
    if (window.XMLHttpRequest)
        dAJ = new XMLHttpRequest;
    else if (window.ActiveXObject)
        dAJ = new ActiveXObject("Microsoft.XMLHTTP");
    dAJ.open("POST", webRoot + "/getdiscount.php", true);
    dAJ.onreadystatechange = function () {
        if (dAJ.readyState == 4) {
            var disp = dAJ.responseText;
            if (disp != "" && disp != undefined) {
                if (parseInt(disp) == 1)
                    document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Discount offer code has been sent to your Email ID. Please check your spam if you don't receive a mail within few minutes.<br /><br />We only offer a unique discount code per email ID. Book now as the code is valid only for today! <br /><br /><span id='clsid' style='cursor:pointer;text-decoration:underline;width:100% !important;text-align:right;'>Close</span></div>";
                else if (parseInt(disp) == 2)
                    document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Sorry. We only offer a unique discount code per email ID. A discount code has already been sent to you earlier.<br /><br /><span id='clsid' style='cursor:pointer;text-decoration:underline;width:100% !important;text-align:right;'>Close</span></div>";
                jQuery("#clsid").click(function () { //we usually include a "Close" link within the sliding box so user could close this box if he/she wanted to do so.
                    jQuery("#popupslider").animate({opacity: "0", bottom: "-50"}, 1000).show(); //when user clicks "Close" it reverse the sliding animation to make the box disappear sliding from right to left.
                });
                dAJ = null;
            }
        }
        else
            document.getElementById('sbmt').innerHTML = "<div style='padding:20% 4%;'>Please wait...</div>";
    };
    dAJ.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var values = "eml=" + e + "&ct=" + c;
    dAJ.send(values);
}

var row;
_pass = function (id)
{
    //alert(id);
    var parts = id.split("_");
    row = parts[1];
}

function _getSRSFSubLoc(oid, oname, pd, dd, ptH, ptM, dtH, dtM, slid, pkgT, nh, cpkg, pt, curdate) {
    var chkL = document.getElementsByName('chkAllSLoc');
    var og = "";
    var sl = "";
    var xx = 0;
    for (c = 0; c < chkL.length; c++) {
        if (chkL[c].checked)
            og += og == "" ? chkL[c].value : "," + chkL[c].value;
    }
    var chkSL = document.getElementsByName('chkSLoc[]');
    for (c = 0; c < chkSL.length; c++) {
        if (chkSL[c].checked) {
            sl += sl == "" ? chkSL[c].value : "," + chkSL[c].value;
            var aa = chkSL[c].id.split('-');
            if (og.indexOf(aa[1]) < 0)
                og += og == "" ? aa[1] : "," + aa[1];
            xx++;
        }
    }
    if (sl == "")
        sl = "0";

    if (slid.checked == false) {
        document.getElementById('chkAllSLoc' + oid).checked = false;
    } else {
        if (xx == chkSL.length)
            document.getElementById('chkAllSLoc' + oid).checked = true;
    }
    var ajSF;
    if (window.XMLHttpRequest)
        ajSF = new XMLHttpRequest;
    else if (window.ActiveXObject)
        ajSF = new ActiveXObject("Microsoft.XMLHTTP");
    ajSF.onreadystatechange = function () {
        if (ajSF.readyState == 4) {
            var disp = ajSF.responseText;
            if (disp != "" && disp != undefined) {
                document.getElementById('srsf').innerHTML = disp;
                ajSF = null;
                document.body.removeChild(document.getElementById('popupContainer'));
                document.body.removeChild(document.getElementById('disableLayer'));
                $(".faredetails").mouseover(function () {

                    if (document.getElementById('link_' + row))
                    {
                        var tp = document.getElementById('link_' + row).offsetTop;
                        var tl = document.getElementById('link_' + row).offsetLeft;
                        document.getElementById('popupbox' + row).style.top = eval(tp - 241) + "px";
                        document.getElementById('popupbox' + row).style.left = eval(tl - 315) + "px";
                    }


                    $(this).next(".details_wrapper").show();
                    $(this).next(".details_wrapper").after('<div class="overlay"></div>');
                    $('.overlay').click(function () {
                        $(".details_wrapper").hide();
                        $(".overlay").remove();
                    })
                });
                $(".closedpop").click(function () {
                    $(".details_wrapper").hide();
                    $(".tcpopup").hide();
                    $(".overlay").remove();
                });
                //$(".overlay").remove();
            }
            /*JQUERY popup*/
            var jqy = jQuery.noConflict();
            jqy(function () {
                if ($(".js__p_start").length > 0)
                {
                    jqy(".js__p_start, .js__p_another_start").simplePopup();
                }

            });

            var auto_refresh = setInterval(
                    function ()
                    {
                        $(document).ready(function () {
                            var d = new Date();
                            var n = d.getTime();

                            $("#curdatetime").val(d);
                            if ($("#nextcurdatetime").length > 0)
                                $("#nextcurdatetime").val(d);
                        });
                        $('#load').load('reload.php').fadeIn("slow");
                    }, 10000);



            $(function () {

// The height of the content block when it's not expanded
                var adjustheight = 31;
// The "more" link text
                var moreText = "+  More";
// The "less" link text
                var lessText = "- Less";

// Sets the .more-block div to the specified height and hides any content that overflows
                $(".more-less .more-block").css('height', adjustheight).css('overflow', 'hidden');

// The section added to the bottom of the "more-less" div


                $("div.adjust").text(moreText);

                $("div.adjust").toggle(function () {
                    $(this).parents("div:first").find(".more-block").css('height', 'auto').css('overflow', 'visible');
                    // Hide the [...] when expanded
                    $(this).parents("div:first").find("p.continued").css('display', 'none');
                    $(this).text(lessText);
                }, function () {
                    $(this).parents("div:first").find(".more-block").css('height', adjustheight).css('overflow', 'hidden');
                    $(this).parents("div:first").find("p.continued").css('display', 'block');
                    $(this).text(moreText);
                });
            });


        }
    };
    _showPopUp('html', '<br /><div style="text-align:center;"><img src="http://www.carzonrent.com/images/loader.gif" border="0" /><br /><br />Your search filter is being processed.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', 't', 400, 150, true, '');
    ajSF.open("POST", webRoot + "/get-srsf-sub-loc.php", true);
    ajSF.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var values = "oid=" + og + "&oname=" + oname + "&pd=" + pd + "&dd=" + dd + "&ptH=" + ptH + "&ptM=" + ptM + "&dtH=" + dtH + "&dtM=" + dtM + "&slid=" + sl + "&pkgt=" + pkgT + "&nh=" + nh + "&cpkg=" + cpkg + "&curdatetime=" + encodeURIComponent(curdate);
    ajSF.send(values);
}
;

//function _allSubLoc(ck) {
//    var x = true;
//    if (ck.checked == false)
//	x = false;
//
//    var chkSL = document.getElementsByName('chkSLoc[]');
//    var sl = "";
//    for(c = 0; c < chkSL.length; c++){
//	chkSL[c].checked = x;
//    }
//};

function _allSubLoc(ck, l) {
    var x = true;
    if (ck.checked == false)
        x = false;

    var chkSL = document.getElementsByName('chkSLoc[]');
    var sl = "";
    for (c = 0; c < chkSL.length; c++) {
        var aa = chkSL[c].id.split('-');
        if (aa[1] == l)
            chkSL[c].checked = x;
    }
}

function _createBooking() {
    var ajBook;
    var coric = "";
    if (document.getElementById("hdCORIC"))
        coric = document.getElementById("hdCORIC").value;
    if (coric != "" || coric != undefined) {
        if (window.XMLHttpRequest)
            ajBook = new XMLHttpRequest;
        else if (window.ActiveXObject)
            ajBook = new ActiveXObject("Microsoft.XMLHTTP");
        ajBook.onreadystatechange = function () {
            if (ajBook.readyState == 4) {
                var disp = ajBook.responseText;
                if (disp != "" && disp != undefined) {
                    ajBook = null;
                    if (isNaN(disp)) {
                        window.location = disp;
                    } else {
                        window.location = webRoot + "/booking-fail.php";
                    }
                }
            }
        };
        ajBook.open("POST", webRoot + "/createbooking.php", true);
        ajBook.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var values = "coric=" + coric;
        ajBook.send(values);
    }
}
;

function _setSFPkgType(pkgType) {
    if (pkgType == "Hourly") {
        document.getElementById('divHourly').style.display = 'block';
        document.getElementById('divDaily').style.display = 'none';
        document.getElementById('selHrs').style.display = 'block';
        document.getElementById('selTD').style.display = 'none';
        _setHours('2', 'selHrs', 'seltimeH');
    } else if (pkgType == "Daily") {
        document.getElementById('divDaily').style.display = 'block';
        document.getElementById('divHourly').style.display = 'none';
        document.getElementById('selHrs').style.display = 'none';
        document.getElementById('selTD').style.display = 'block';
    }
}
;

function _setHours(h, s1, s2) {
    document.getElementById(s1).style.display = "block";
    document.getElementById(s2).innerHTML = "You Selected: " + h + " Hours";
}
;

function _setHoursJS(h) {
    document.getElementById('nHoursJS').value = h;
}
;

_checkTxtLen = function (ev, ta, mxc) {
    if (ta) {
        var kc;
        if (window.event)
            kc = window.event.keyCode;
        else if (ev)
            kc = ev.which;
        else
            kc = null;
        if (kc != null) {
            if (kc != 8 && kc != 0) {
                var ch = ta.value;
                if (ch.length >= mxc)
                    return false;
                else {
                    ta.onkeyup = function () {
                        ch = this.value;
                        if (ch.length >= mxc)
                            ta.value = ta.value.substring(0, mxc);
                    }
                    return true;
                }
            }
        }
    } else
        return false;
};

_setTab = function (srv) {
    if (document.getElementById('selfdriveDiv'))
        document.getElementById('selfdriveDiv').style.display = "none";
    if (document.getElementById('outstationDiv'))
        document.getElementById('outstationDiv').style.display = "none";
    if (document.getElementById('localDiv'))
        document.getElementById('localDiv').style.display = "none";
    if (document.getElementById('internationalDiv'))
        document.getElementById('internationalDiv').style.display = "none";
		
	  if (document.getElementById('airportDiv'))
        document.getElementById('airportDiv').style.display = "none";

    if (document.getElementById(srv + 'Div'))
        document.getElementById(srv + 'Div').style.display = "block";

    if (document.getElementById('selfdriveLI'))
        document.getElementById('selfdriveLI').className = "";
    if (document.getElementById('outstationLI'))
        document.getElementById('outstationLI').className = "";
    if (document.getElementById('localLI'))
        document.getElementById('localLI').className = "";
    if (document.getElementById('internationalLI'))
        document.getElementById('internationalLI').className = "";
	 if (document.getElementById('airportLI'))
        document.getElementById('airportLI').className = "";

    if (document.getElementById(srv + 'LI'))
        document.getElementById(srv + 'LI').className = "currentTab";
    if (srv != "selfdrive") {
        document.getElementById('othSrv').style.display = 'block';
    }
    else {
        document.getElementById('othSrv').style.display = 'none';
    }

    if (srv == "local" || srv == "outstation" || srv == "airport") {
        document.getElementById('enredbuttonID').style.display = "none";
        document.getElementById('enyellowbuttonID').style.display = "block";
        document.getElementById('feedbackpopupRED').style.display = "none";
        document.getElementById('feedbackpopupYELLOW').style.display = "block";
    } else {
        document.getElementById('enredbuttonID').style.display = "block";
        document.getElementById('enyellowbuttonID').style.display = "none";
        document.getElementById('feedbackpopupRED').style.display = "block";
        document.getElementById('feedbackpopupYELLOW').style.display = "none";
    }
}

_setOSPickDate = function (dt, di) {
    pd = document.getElementById(dt).value.trim().split(" ");
    var p = new Date();
    var d = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));
    var nxtD = eval(parseInt(pd[0]));
    p.setFullYear(pd[2], m, pd[0]);
    d.setFullYear(pd[2], m, nxtD);
    document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    try {
        $('#inputField2').datetimepicker({
            format: 'd M, Y',
            minDate: d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear(),
            step: 30
        });
    } catch (e) {
    }

    document.getElementById('ospickdate').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();
    if (document.getElementById('osdropdate'))
        document.getElementById('osdropdate').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
};
_setOSDropDate = function (dt) {
    dd = document.getElementById(dt).value.trim().split(" ");
    var d = new Date();
    m = _getMonthInt(dd[1].replace(",", ""));
    d.setFullYear(dd[2], m, dd[0]);
    document.getElementById(dt).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    if (document.getElementById('osdropdate'))
        document.getElementById('osdropdate').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
};

_setLCPickDate = function (dt) {
    pd = document.getElementById(dt).value.trim().split(" ");
    var p = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));
    p.setFullYear(pd[2], m, pd[0]);
    document.getElementById(dt).value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear() + " " + pd[3];
    if (document.getElementById('lcpickdate'))
        document.getElementById('lcpickdate').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();
    var tm = pd[3].split(":");
    if (parseInt(tm[0]) >= 12)
        document.getElementById('userTime').value = tm[0] + ":" + tm[1] + " PM";
    else
        document.getElementById('userTime').value = tm[0] + ":" + tm[1] + " AM";
};

_showLadakhPopUp = function (idx, t) {
    var allPopUp = document.getElementsByName('ladakhPopUp');
    for (i = 0; i < allPopUp.length; i++)
        allPopUp[i].style.display = 'none';
    if (document.getElementById('ladakhPopUp-' + idx)) {
        document.getElementById('ladakhPopUp-' + idx).style.display = t;
        //if (t == 'block')
        //$(".ladakhPopUp").after('<div class="overlay"></div>');
    }
}
function _setdropdates(type, pi, di) {

    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    var d = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));
    if (document.getElementById('chkPkgType')) {

        if (type == 'week') {
            var countweek = document.getElementById('nWeeks').value;
            var days = 7 * countweek;
        } else {
            var countmonth = document.getElementById('nMonths').value;
            var days = 30 * countmonth;
        }
        var nxtD = eval(parseInt(pd[0]) + days);
    }
    //alert(nxtD);
    p.setFullYear(pd[2], m, pd[0]);
    d.setFullYear(pd[2], m, nxtD);
    document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
    try {
        $('#inputFieldSF2').datetimepicker({
            format: 'd M, Y H:i',
            minDate: d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
            step: 30
        });
    } catch (e) {
    }

    document.getElementById('pickdate').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();
    if (document.getElementById('dropdate'))
        document.getElementById('dropdate').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    var tm = pd[3].split(":");

    document.getElementById('tHourP').value = tm[0];
    document.getElementById('tMinP').value = tm[1];

    if (document.getElementById('dropdate')) {
        document.getElementById('tHourD').value = tm[0];
        document.getElementById('tMinD').value = tm[1];
    }
//        alert(document.getElementById('pickdate').value);
//        alert(document.getElementById('dropdate').value);
//        return false;
}
;
function _setdropdatesmodify(type, pi, di) {

    pd = document.getElementById(pi).value.trim().split(" ");
    dd = document.getElementById(di).value.trim().split(" ");
    var p = new Date();
    var d = new Date();
    m = _getMonthInt(pd[1].replace(",", ""));

    if (document.getElementById('chkPkgTypeMS')) {

        if (type == 'week') {
            var countweek = document.getElementById('nWeeks').value;
            var days = 7 * countweek;
        } else {
            var countmonth = document.getElementById('nMonths').value;
            var days = 30 * countmonth;
        }
        var nxtD = eval(parseInt(pd[0]) + days);
    }
    //alert(nxtD);
    p.setFullYear(pd[2], m, pd[0]);
    d.setFullYear(pd[2], m, nxtD);
    //alert(d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3]);
    document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
    try {
        $('#inputFieldSF2').datetimepicker({
            format: 'd M, Y H:i',
            minDate: d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
            step: 30
        });
    } catch (e) {
    }

    document.getElementById('pickdateMS').value = p.getDate() + " " + month[p.getMonth()] + ", " + p.getFullYear();

    if (document.getElementById('dropdateMS'))
        document.getElementById('dropdateMS').value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    var tm = pd[3].split(":");

    document.getElementById('tHourPMS').value = tm[0];
    document.getElementById('tMinPMS').value = tm[1];
    //alert(document.getElementById('dropdateMS').value);
    if (document.getElementById('dropdateMS')) {
        document.getElementById('tHourDMS').value = tm[0];
        document.getElementById('tMinDMS').value = tm[1];
    }
//        alert(document.getElementById('pickdateMS').value);
//        alert(document.getElementById('dropdateMS').value);
//        return false;
}
;
function datapopulate()
{
  
    var mobileno = $('#monumberX').val();
    var txtemail = $("#txtemail").val();
    var hdTourtype = $("input[name=hdTourtype]").val();
    hdTourtype = hdTourtype.toUpperCase();
    $("#empcode").val("");
    $("#empcode").attr("readonly", false);
    $("#payAmountVal").html("");
    $("#spDiscount").html("");
    $(".loaderbooking").show();

////////////////////////
    xi = $("#ival").val();

    $("#tfare").html($("#intact_fare").val());
    $("#spPay").html($("#intact_spPay").val());
    $("#totFare" + xi).val($("#intact_totFare").val());
    $("#totFarePB").val($("#intact_totFarePB").val());
    $("#discountAmt" + xi).val($("#intact_discountAmt").val());
    $("#disA").html($("#intact_disA").val());
    $("#vatt").html($("#intact_vatt").val());
    $("#vatAmt" + xi).val($("#intact_vatAmt").val());
    $("#subt").html($("#intact_subt").val());
    $("#subtAmount" + xi).val($("#intact_subtAmount").val());


///////////////////////////




    var href = $('#addlink').attr('href');
    if (status == 'Registered') {
        $('#addlink').attr('href', href);
    } else {
        $('#addlink').removeAttr('href');
    }

    if (mobileno != "" && mobileno.length == 10 && txtemail != '')
    {
        $.ajax({
            type: 'POST',
            url: webRoot + "/ajaxdata.php",
            beforeSend: function (msg) {
                $("#populate").show();
            },
            data: {mobileno: mobileno, txtemail: txtemail, action: 'populatedata'},
            success: function (response) {
                if (response == '' && hdTourtype != 'OUTSTATION' && hdTourtype != 'LOCAL')
                {
                    $("#populate").hide();
                    $("#firstTimeLogin").val("1");
                }
                else
                {
                    var parts = response.split('&');

                    //$('#txtname').val('');
                    $("#populate").hide();
                    $("#firstTimeLogin").val("");

                    // $("#empcode").attr("readonly", false);
                    $("#empcode").val("");
                    $("#promocode").show();
                }
                $(".loaderbooking").hide();
            }
        });
    }
    else
    {
        // $('#txtname').val("");
        /// $('#txtemail').val("");
        $("#populate").hide();
        $("#firstTimeLogin").val("");
        $("#empcode").val("");
        $(".loaderbooking").hide();

        // $("#empcode").attr("readonly", false);

    }
     $("#intact_fare").val($("#tfare").html());
    $("#intact_spPay").val($("#spPay").html());
    $("#intact_totFare").val($("#totFare" + xi).val());
    $("#intact_totFarePB").val($("#totFarePB").val());
    $("#intact_discountAmt").val($("#discountAmt" + xi).val());
    $("#intact_disA").val($("#disA").html());
    $("#intact_vatt").val($("#vatt").html());
    $("#intact_vatAmt").val($("#vatAmt" + xi).val());
    $("#intact_subt").val($("#subt").html());
    $("#intact_subtAmount").val($("#subtAmount" + xi).val());
     $("#promodis").show();
}

$(document).ready(function() {
	if($('select.area').length>0)
	{
	$('select.area').customSelect();
	}
	if($('select.modifymyb').length>0)
	{
		$('select.modifymyb').customSelect();		
	}
				
// For Travel multi city popup

	
	$("a.multi").click(function(){
		$(".modyfybook_wrapper").show();
		$(".modyfybook_wrapper").after('<div class="overlay"></div>');
		window.top.document.getElementById('iFrameOS').style.height = '556px';
		$('.overlay').click(function() {
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();
			window.top.document.getElementById('iFrameOS').style.height = '477px';
		})
	})
		$(".closedpop").click(function(){
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();
			if (window.top.document.getElementById('iFrameOS'))
			window.top.document.getElementById('iFrameOS').style.height = '477px';
		})		
		
	

	_pass = function(id)
	{
		
		var parts = id.split("_");
		row = parts[1];
	}
	$(".faredetails").mouseover(function(){
		var tp = document.getElementById('link_' + row).offsetTop;
		var tl = document.getElementById('link_' + row).offsetLeft;
		document.getElementById('popupbox' + row).style.top = eval(tp - 241) + "px";
		document.getElementById('popupbox' + row).style.left = eval(tl - 315) + "px";
		$(this).next(".details_wrapper").show();
		$(this).next(".details_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".details_wrapper").hide();
			$(".overlay").remove();
		})
	})
	
	$(".closedpop").click(function(){
		$(".details_wrapper").hide();
		$(".tcpopup").hide();
		$(".overlay").remove();			
	})
		
	
	$(".atcpopup").click(function(){
		$(this).next(".tcpopup").show();
		$(this).next(".tcpopup").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".tcpopup").hide();
			$(".overlay").remove();
		})
	})	
	
	$(".paytc").click(function(){
		$(".tcpopup").show();
		$(".tcpopup").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".tcpopup").hide();
			$(".overlay").remove();
		})
	})
	
// For map tooltips
	$(".map").click(function(){
		$(this).next(".map_wrapper").show();
		$(this).next(".map_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".map_wrapper").hide();
			$(".overlay").remove();
		})
	})
		$(".closedpop").click(function(){
			$(".map_wrapper").hide();
			$(".overlay").remove();			
		})
		
		
// For modify my Booking

	$(".modify a").click(function(){
		$(this).next(".modyfybook_wrapper").show();
		$(this).next(".modyfybook_wrapper").after('<div class="overlay"></div>');
		$('.overlay').click(function() {
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();
		})
	})
		$(".closedpop").click(function(){
			$(".modyfybook_wrapper").hide();
			$(".overlay").remove();			
		})		
	
// For Feedback popup
	$(".feedback_panel").delay(5000).show(500);
	$(".feedback_panel .heading a").click(function(){
		$(".feed_wrapper").hide(500);
		$(".feed_minimize").delay(200).show(500);
	});
	$(".feed_minimize").click(function(){
		$(".feed_minimize").hide(500);
		$(".feed_wrapper").delay(200).show(500);

	});

// For Book Now popup
	$(".booknow").click(function(){
		document.getElementById('frmID').value = $(this).attr('id');
		var postop = (($(window).height()) - 150)/2;
		var posleft = (($(window).width()) - 150)/2;
		$(".overlay").show();
		$(".confirmbookingpanel").show();
		$(".confirmbookingpanel").css({'top': postop});
		$(".confirmbookingpanel").css({'left': posleft});
		$(".overlay").click(function(){
			$(".confirmbookingpanel").hide();
			$(".overlay").fadeOut(500);		
		})
	})
	
	$(".closedcp").click(function(){
		$(".confirmbookingpanel").hide();
		$(".overlay").fadeOut(500);		
	})

});

function _unlockPage()
{
	document.getElementById('overlay123').style.display = 'none';
}
function _lockPage()
{
	document.getElementById('overlay123').style.display = 'block';
}
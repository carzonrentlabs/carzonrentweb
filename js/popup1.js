//parameters :1.background disable Div class/id name
//parameters :2.Popup Div class/id name
//parameters :3.clickable Div class/id name which will display the name;
//parameters :4.clickable closing Div class/id name which will display the name;
function popupShow(cssOfDiv,popclass,clickDiv,closepop){
//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;
//loading popup with jQuery magic!
function loadPopup(){
//loads popup only if it is disabled
if(popupStatus==0){
$(cssOfDiv).css({
"opacity": "0.7"
});
$(cssOfDiv).fadeIn("slow");
$(popclass).fadeIn("slow");
popupStatus = 1;
}
}

//disabling popup with jQuery magic!
function disablePopup(){
//disables popup only if it is enabled
if(popupStatus==1){
$(cssOfDiv).fadeOut("slow");
$(popclass).fadeOut("slow");
popupStatus = 0;
}
}

//centering popup
function centerPopup(){
//request data for centering
var windowWidth = document.documentElement.clientWidth;
var windowHeight = document.documentElement.clientHeight;
var popupHeight = $(popclass).height();
var popupWidth = $(popclass).width();
//centering
$(popclass).css({
"position": "absolute",
"top": windowHeight/4-popupHeight/4,
"left": windowWidth/2-popupWidth/2
});
//only need force for IE6

$(cssOfDiv).css({
"height": windowHeight
});

}
//LOADING POPUP
//Click the button event!
$(popclass).hide();
$(clickDiv).click(function(){
//centering with css
centerPopup();
//load popup
loadPopup();
});

//CLOSING POPUP
//Click the x event!
$(closepop).click(function(){
disablePopup();
});

//Click out event!
	$(cssOfDiv).click(function(){
		disablePopup();
	});
//Press Escape event!
$(document).keydown(function(e){

if(e.keyCode == 27 && popupStatus == 1){
disablePopup();
}
});
}

//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
popupShow("#backgroundPopup","#popuprajive","#rajive","#popuprajiveclose");
popupShow("#backgroundPopup","#popupram","#ram","#popupramclose");
popupShow("#backgroundPopup","#popupsakshi","#sakshi","#popupsakshiclose");
popupShow("#backgroundPopup","#popupgaurav","#gaurav","#popupgauravclose");
popupShow("#backgroundPopup","#popupreshav","#reshav","#popupreshavclose");
popupShow("#backgroundPopup","#popupsanjiv","#sanjiv","#popupsanjivclose");
popupShow("#backgroundPopup","#popuprakesh","#rakesh","#popuprakeshclose");
popupShow("#backgroundPopup","#popupanirudh","#anirudh","#popupanirudhclose");

});
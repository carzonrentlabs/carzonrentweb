<?php 
	if(isset($_POST['merchantTxnId']) && $_POST['merchantTxnId'] != ""){
		set_include_path('../lib'.PATH_SEPARATOR.get_include_path());
		require_once('../lib/CitrusPay.php');
		require_once 'Zend/Crypt/Hmac.php';

		function generateHmacKey($data, $apiKey=null){
			$hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
			return $hmackey;
		}

		CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d",'production');

		$vanityUrl = $_POST['vanityUrl'];
		$currency = $_POST['currency'];
		$merchantTxnId = $_POST['merchantTxnId'];
		$orderAmount = $_POST['orderAmount'];
		$data = "$vanityUrl$orderAmount$merchantTxnId$currency";
		$secSignature = generateHmacKey($data,CitrusPay::getApiKey());

		echo $secSignature;
	}
?>
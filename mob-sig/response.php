<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
set_include_path('../lib'.PATH_SEPARATOR.get_include_path());
require_once 'Zend/Crypt/Hmac.php';

function generateHmacKey($data, $apiKey=null){
	$hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
	return $hmackey;
}

$txnid = "";
$txnrefno = "";
$txnstatus = "";
$txnmsg = "";
$firstName = "";
$lastName = "";
$email = "";
$street1 = "";
$city = "";
$state = "";
$country = "";
$pincode = "";
$mobileNo = "";
$signature = "";
$reqsignature = "";
$data = "";
$txnGateway = "";
$paymentMode = "";
$maskedCardNumber = "";
$cardType = "";
$flag = "dataValid";

if(isset($_POST['TxId']))
{
	$txnid = $_POST['TxId'];
	$data .= $txnid;
}
if(isset($_POST['TxStatus']))
{
	$txnstatus = $_POST['TxStatus'];
	$data .= $txnstatus;
}
if(isset($_POST['amount']))
{
	$amount = $_POST['amount'];
	$data .= $amount;
}
if(isset($_POST['pgTxnNo']))
{
	$pgtxnno = $_POST['pgTxnNo'];
	$data .= $pgtxnno;
}
if(isset($_POST['issuerRefNo']))
{
	$issuerrefno = $_POST['issuerRefNo'];
	$data .= $issuerrefno;
}
if(isset($_POST['authIdCode']))
{
	$authidcode = $_POST['authIdCode'];
	$data .= $authidcode;
}
if(isset($_POST['firstName']))
{
	$firstName = $_POST['firstName'];
	$data .= $firstName;
}
if(isset($_POST['lastName']))
{
	$lastName = $_POST['lastName'];
	$data .= $lastName;
}
if(isset($_POST['pgRespCode']))
{
	$pgrespcode = $_POST['pgRespCode'];
	$data .= $pgrespcode;
}
if(isset($_POST['addressZip']))
{
	$pincode = $_POST['addressZip'];
	$data .= $pincode;
}
if(isset($_POST['signature']))
{
	$signature = $_POST['signature'];
}
/*signature data end*/

if(isset($_POST['TxRefNo']))
{
	$txnrefno = $_POST['TxRefNo'];
}
if(isset($_POST['TxMsg']))
{
	$txnmsg = $_POST['TxMsg'];
}
if(isset($_POST['email']))
{
	$email = $_POST['email'];
}
if(isset($_POST['addressStreet1']))
{
	$street1 = $_POST['addressStreet1'];
}
if(isset($_POST['addressStreet2']))
{
	$street2 = $_POST['addressStreet2'];
}
if(isset($_POST['addressCity']))
{
	$city = $_POST['addressCity'];
}
if(isset($_POST['addressState']))
{
	$state = $_POST['addressState'];
}
if(isset($_POST['addressCountry']))
{
	$country = $_POST['addressCountry'];
}

if(isset($_POST['mandatoryErrorMsg']))
{
	$mandatoryerrmsg = $_POST['mandatoryErrorMsg'];
}
if(isset($_POST['successTxn']))
{
	$successtxn = $_POST['successTxn'];
}
if(isset($_POST['mobileNo']))
{
	$mobileNo = $_POST['mobileNo'];
}
if(isset($_POST['txnGateway']))
{
	$txnGateway = $_POST['txnGateway'];
}
if(isset($_POST['paymentMode']))
{
	$paymentMode = $_POST['paymentMode'];
}
if(isset($_POST['maskedCardNumber']))
{
	$maskedCardNumber = $_POST['maskedCardNumber'];
}
if(isset($_POST['cardType']))
{
	$cardType = $_POST['cardType'];
}

$respSignature = generateHmacKey($data,"01ead54113da1cb978b39c1af588cf83e16c519d");

if($signature != "" && strcmp($signature, $respSignature) != 0)
{
	$json = json_encode("dataTampered");
} else {
	$json = json_encode($_POST);
?>
	<!DOCTYPE html>
	<html>
		<head>
			<script type='text/javascript' language="javascript"> 
				function callTojavaFn(){
					var msg = "Calling from Java Script";
					<?php
					  //print_r($json . "\n");
					  print_r("JsHandler.sendResponse('" . $json . "');\n");
					  //print_r("JsHandler.sendResponse('" . $json . "');\n");
					?>
				}
			</script>
		</head>
		<body onload="callTojavaFn()">
			<!--<input type="button" onClick="javascript: callTojavaFn()" value="Call Java Fn">-->
		</body>
	</html>
<?php
}
?>

<?php    
	/* 
		API Class
 	*/	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "carzonrent-db.cwhd81c19z15.ap-southeast-1.rds.amazonaws.com";
		const DB_USER = "root";
		const DB_PASSWORD = "N2o13delhimY";
		const DB = "cor_by_bytesbrick";
		const SITEURL = "http://localhost/rest/";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();	
				// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['request'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/*
		* getPrivateKey method
		* Description: get private key 
		*/
		private function getCustomerSearch()
		{
				$sql="Select * from customer_search where refsite!='' limit 0,10";
				$prj = array();
				$res = mysql_query($sql, $this->db);
				if(mysql_num_rows($res) > 0){								
					while($arr = mysql_fetch_array($res,MYSQL_ASSOC)){
					  $prj[] = $arr;
					}								
				}
				/* for total Comment   */
				$sql1 = "SELECT FOUND_ROWS() AS `found_rows`";
				$result = mysql_query($sql1, $this->db);
				if(mysql_num_rows($result) > 0){
					$ct = mysql_fetch_array($result,MYSQL_ASSOC);			
					$count = $ct['found_rows'];
				}
				$prjData = new CommentList();
				$prjData->TotalRecords = $count;
				$prjData->Items = array_values($prj);
				$obj = new ResponseData();
				$obj->status = 1;
				$obj->msg = "Search Result";
				$obj->errNo =0;
				$obj->data= json_encode($prjData);
				$this->response(json_encode($obj), 200);					
		}
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	class BaseResponse{
		public $status;
		public $errNo;
		public $msg;
	}
	
	class ResponseData extends BaseResponse {	
		public $data;
	}
	
	class ResponseItem extends BaseResponse {	
		public $item;
	}
	
	
	class getFilteredResultResponse{	
		public $projects;
		public $properties;
	}
	
	class listData{
		public $TotalRecords;
		public $Data;
	}
	
	class commentList{
		public $TotalRecords;
		public $Items;
	}
	
	class getCityRelatedData{
		public $projects;
		public $localities;
	}
	
	class getConfigData{
		public $configVersion; 
		public $apiURL; 
		public $criteria; 
		public $sortingOptions; 
		public $cityList; 
		public $termsConditionsLink; 
		public $aboutUsLink; 
		public $blogLink; 
		public $projectCatType; 
		public $buildUpAreaUnit; 
		public $CustomerCareNumber; 
	}
	
	class getProjectData{
		public $projectID; 
		public $projectName; 
		public $projectStatus; 
		public $location; 
		public $CurrentBSP; 
		public $PricePortal; 
		public $DefaultImageUrl; 
		public $imageCount; 
		public $propertyType; 
		public $projectArea; 
		public $projectCategory; 
		public $lat; 
		public $lng; 
		public $priceList; 
		public $propertyTypes; 
		public $images; 
		public $rating;
		public $FavouriteFlag;
	}
	
	class getPropertyData{
		public $propertyID; 
		public $propertyName; 
		public $projectID; 
		public $projectName; 
		public $location; 
		public $propertyStatus; 
		public $price; 
		public $DefaultImageUrl; 
		public $imageCount; 
		public $bedRooms; 
		public $bathRooms; 
		public $ExpectedPossessionDate; 
		public $lat; 
		public $lng; 
		public $images; 
		public $rating;
		public $FavouriteFlag;
	}
	
	// Initiate Library
	
	$api = new API;
	$api->processApi();
?>
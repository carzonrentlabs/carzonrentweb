<?php

error_reporting(0);
session_start();
if (isset($_POST["coric"]) && $_POST["coric"] != "") {
    include_once("./includes/cache-func.php");
    include_once('./classes/cor.mysql.class.php');
    include_once('./classes/cor.ws.class.php');
    include_once('./classes/cor.xmlparser.class.php');
    $resp = 0;
    //$sql = "SELECT * FROM cor_booking_new WHERE coric = '" . $_POST["coric"] . "' AND booking_id IS NULL AND NOT nb_order_no IS NULL AND NOT Checksum IS NULL AND AuthDesc = 'Y'";
    $sql = "SELECT * FROM cor_booking_new WHERE coric = '" . $_POST["coric"] . "' AND booking_id IS NULL AND  nb_order_no IS NOT NULL AND  Checksum IS NOT NULL AND AuthDesc = 'Y'";
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $booking = $db->query("query", $sql);
    $db->close();
    if (!array_key_exists("response", $booking)) {
        $fullname = explode(" ", $booking[0]["full_name"]);
        if (count($fullname) > 1) {
            $firstName = $fullname[0];
            $lastName = $fullname[1];
        } else {
            $firstName = $fullname[0];
            $lastName = "";
        }
        $cor = new COR();
        if ($booking[0]['tour_type'] == 'airport') {



            $totFare = $booking[0]["tot_fare"] - $booking[0]["discount_amt"];

            $corArrSTD = array();
            $corArrSTD["pkgId"] = $booking[0]["package_id"];
            $corArrSTD["destination"] = $booking[0]["destinations_name"];
            $pDate = date_create($booking[0]["pickup_date"]);
            $corArrSTD["dateOut"] = $pDate->format("Y-m-d");
            $dDate = date_create($booking[0]["drop_date"]);
            $corArrSTD["dateIn"] = $dDate->format("Y-m-d");
            $pTime = date_create($booking[0]["pickup_time"]);
            $corArrSTD["TimeOfPickup"] = $pTime->format("is");
            $corArrSTD["address"] = str_ireplace(array("<br>", "<br />"), "", nl2br($booking[0]["address"]));
            $corArrSTD["firstName"] = $firstName;
            $corArrSTD["lastName"] = $lastName;
            $corArrSTD["phone"] = $booking[0]["mobile"];
            $corArrSTD["emailId"] = $booking[0]["email_id"];
            $corArrSTD["userId"] = $booking[0]["cciid"];
            $corArrSTD["paymentAmount"] = $booking[0]["tot_fare"];
            $corArrSTD["paymentType"] = "1";
            $corArrSTD["paymentStatus"] = "1";
            $corArrSTD["trackId"] = $_POST["coric"];
            $corArrSTD["transactionId"] = $booking[0]["nb_order_no"];
            $corArrSTD["discountPc"] = $booking[0]["discount"];
            $corArrSTD["discountAmount"] = $booking[0]["discount_amt"];
            $corArrSTD["remarks"] = $booking[0]["remarks"];
            $corArrSTD["totalKM"] = $booking[0]["distance"];
            $corArrSTD["visitedCities"] = $booking[0]["destinations_name"];

            $outStationYN = "false";
            $corArrSTD["outStationYN"] = $outStationYN;
            $corArrSTD["CustomPkgYN"] = 'true';
            $corArrSTD["OriginCode"] = $booking[0]["source"]; //Aamir 1-Aug-2012
            $corArrSTD["chkSum"] = $booking[0]["Checksum"];
            $corArrSTD["DisCountCode"] = $booking[0]["promotion_code"];
            $corArrSTD["PromotionCode"] = $booking[0]["discount_coupon"];
            $corArrSTD["IsPayBack"] = $booking[0]["isPayBack"];
            $corArrSTD["discountTrancastionID"] = $booking[0]["disc_txn_id"];

            $corArrSTD["latitude"] = $booking[0]["latitude"];
            $corArrSTD["longitude"] = $booking[0]["longitude"];
            $corArrSTD["TerminalID"] = $booking[0]["terminalid"];
            $corArrSTD["TerminalLocationID"] = $booking[0]["destinations_id"];
			$corArrSTD["IndicatedCGSTTaxPercent"] = $booking[0]["CGSTPercent"];
			$corArrSTD["IndicatedSGSTTaxPercent"] = $booking[0]["SGSTPercent"];
			$corArrSTD["IndicatedIGSTTaxPercent"] = $booking[0]["IGSTPercent"];
			$corArrSTD["GSTEnabledYN"] = $booking[0]["GSTEnabledYN"];
			$corArrSTD["IndicatedClientGSTId"] = $booking[0]["ClientGSTId"];

		  
            $res = $cor->_CORMakeBookingWithLatLongAirport($corArrSTD);



            $myXML = new CORXMLList();
            if ($res->{'CreateAirport_BookingResult'}->{'any'} != "") {
                $arrUserId = $myXML->xml2ary($res->{'CreateAirport_BookingResult'}->{'any'});
                if (key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0) {
                    if ($booking[0]["tour_type"] != "Local")
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($booking[0]["tour_type"])) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($booking[0]["origin_name"])) . "-to-" . str_ireplace(" ", "-", strtolower($booking[0]["destinations_name"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                    else
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($booking[0]["tour_type"])) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($booking[0]["origin_name"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                    $xmlToSave .= "<cortrans>";
                    $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                    $xmlToSave .= "</cortrans>";
                    if (!is_dir("./xml/trans/" . date('Y')))
                        mkdir("./xml/trans/" . date('Y'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
                    if (1)
                        createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_POST["coric"] . "-B.xml");

                    $db = new MySqlConnection(CONNSTRING);
                    $db->open();
                    $dataToSave = array();
                    $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
                    $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
                    $dataToSave["payment_mode"] = "1";
                    $whereData = array();
                    $whereData["coric"] = $_POST["coric"];
                    $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                    unset($whereData);
                    unset($dataToSave);

                    /*                     * ****** for search record booking updated ****** */
                    $dataToSaveSearch = array();
                    $dataToSaveSearch["booking_id"] = $arrUserId[0]["Column1"];
                    $dataToSaveSearch["emailid"] = $booking[0]["email_id"];
                    $whereDatasearch = array();
                    $whereDatasearch["unique_id"] = $_SESSION['SEARCH_CUSTOMER_ID'];
                    $r = $db->update("customer_search", $dataToSaveSearch, $whereDatasearch);
                    unset($whereDatasearch);
                    unset($dataToSaveSearch);
                    /*                     * ****** for search record ****** */


                    $db->close();
                    $resp = $url;
                }
            }
            else {
                $resp = 2;
            }
        } else {

            $totFare = $booking[0]["tot_fare"] - $booking[0]["discount_amt"];

            $corArrSTD = array();
            $corArrSTD["pkgId"] = $booking[0]["package_id"];
            $corArrSTD["destination"] = $booking[0]["destinations_name"];
            $pDate = date_create($booking[0]["pickup_date"]);
            $corArrSTD["dateOut"] = $pDate->format("Y-m-d");
            $dDate = date_create($booking[0]["drop_date"]);
            $corArrSTD["dateIn"] = $dDate->format("Y-m-d");
            $pTime = date_create($booking[0]["pickup_time"]);
            $corArrSTD["TimeOfPickup"] = $pTime->format("is");
            $corArrSTD["address"] = str_ireplace(array("<br>", "<br />"), "", nl2br($booking[0]["address"]));
            $corArrSTD["firstName"] = $firstName;
            $corArrSTD["lastName"] = $lastName;
            $corArrSTD["phone"] = $booking[0]["mobile"];
            $corArrSTD["emailId"] = $booking[0]["email_id"];
            $corArrSTD["userId"] = $booking[0]["cciid"];
            $corArrSTD["paymentAmount"] = $totFare;
            $corArrSTD["paymentType"] = "1";
            $corArrSTD["paymentStatus"] = "1";
            $corArrSTD["trackId"] = $_POST["coric"];
            $corArrSTD["transactionId"] = $booking[0]["nb_order_no"];
            $corArrSTD["discountPc"] = $booking[0]["discount"];
            $corArrSTD["discountAmount"] = $booking[0]["discount_amt"];
            $corArrSTD["remarks"] = $booking[0]["remarks"];
            $corArrSTD["totalKM"] = $booking[0]["distance"];
            $corArrSTD["visitedCities"] = $booking[0]["destinations_name"];
            if ($booking[0]["tour_type"] != "Local")
                $outStationYN = "true";
            else
                $outStationYN = "false";
            $corArrSTD["outStationYN"] = $outStationYN;
            $corArrSTD["OriginCode"] = $booking[0]["source"]; //Aamir 1-Aug-2012
            $corArrSTD["chkSum"] = $booking[0]["Checksum"];
            $corArrSTD["DisCountCode"] = $booking[0]["promotion_code"];
            $corArrSTD["PromotionCode"] = $booking[0]["discount_coupon"];
            $corArrSTD["IsPayBack"] = $booking[0]["isPayBack"];
            $corArrSTD["discountTrancastionID"] = $booking[0]["disc_txn_id"];

            $corArrSTD["latitude"] = $booking[0]["latitude"];
            $corArrSTD["longitude"] = $booking[0]["longitude"];
			
			$corArrSTD["IndicatedCGSTTaxPercent"] = $booking[0]["CGSTPercent"];
			$corArrSTD["IndicatedSGSTTaxPercent"] = $booking[0]["SGSTPercent"];
			$corArrSTD["IndicatedIGSTTaxPercent"] = $booking[0]["IGSTPercent"];
			$corArrSTD["GSTEnabledYN"] = $booking[0]["GSTEnabledYN"];
			$corArrSTD["IndicatedClientGSTId"] = $booking[0]["ClientGSTId"];
			$corArrSTD["CustomPkgYN"] = 'false';

            $res = $cor->_CORMakeBookingWithLatLong($corArrSTD);


            // $res = $cor->_CORMakeBooking($corArrSTD);
            $myXML = new CORXMLList();
            if ($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'} != "") {
                $arrUserId = $myXML->xml2ary($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'});
                if (key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0) {
                    if ($booking[0]["tour_type"] != "Local")
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($booking[0]["tour_type"])) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($booking[0]["origin_name"])) . "-to-" . str_ireplace(" ", "-", strtolower($booking[0]["destinations_name"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                    else
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($booking[0]["tour_type"])) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($booking[0]["origin_name"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                    $xmlToSave .= "<cortrans>";
                    $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                    $xmlToSave .= "</cortrans>";
                    if (!is_dir("./xml/trans/" . date('Y')))
                        mkdir("./xml/trans/" . date('Y'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
                    if (1)
                        createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $_POST["coric"] . "-B.xml");

                    $db = new MySqlConnection(CONNSTRING);
                    $db->open();
                    $dataToSave = array();
                    $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
                    $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
                    $dataToSave["payment_mode"] = "1";
                    $whereData = array();
                    $whereData["coric"] = $_POST["coric"];
                    $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                    unset($whereData);
                    unset($dataToSave);

                    /*                     * ****** for search record booking updated ****** */
                    $dataToSaveSearch = array();
                    $dataToSaveSearch["booking_id"] = $arrUserId[0]["Column1"];
                    $dataToSaveSearch["emailid"] = $booking[0]["email_id"];
                    $whereDatasearch = array();
                    $whereDatasearch["unique_id"] = $_SESSION['SEARCH_CUSTOMER_ID'];
                    $r = $db->update("customer_search", $dataToSaveSearch, $whereDatasearch);
                    unset($whereDatasearch);
                    unset($dataToSaveSearch);
                    /*                     * ****** for search record ****** */


                    $db->close();
                    $resp = $url;
                }
            }




            else {
                $resp = 2;
            }
        }
    } else {
        $resp = 0;
    }
    echo $resp;
}
?>
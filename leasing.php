<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<script type="text/javascript" src="js/tabcontent.js"></script>
</head>
<body>
<?php 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >	
	<div style="margin:0 auto;width:948px;">
		<div class="leaseformpanal">
			<h3>CORPORATE LEASING</h3>
			<div class="corportaeinfo">
			<form action="" method="post" name="frmcorportaeinfo" id="frmcorportaeinfo">
				<table width="200px" style="margin:10px 0px 0px 15px;" cellpadding="0" cellspacing="10" border="0">
					 <tr>						
						<td align="left" valign="top" ><input type="text" name="txtOrganization" id="txtOrganization" onfocus="javascript: if(this.value=='Organization'){this.value='';}" onblur="javascript: if(this.value==''){this.value='Organization';}" value="Organization"/></td>	     
					</tr>
					<tr>						
						<td align="left" valign="top" ><input type="text" name="txtName" id="txtName" maxlength="10" onfocus="javascript: if(this.value=='Name'){this.value='';}" onblur="javascript: if(this.value==''){this.value='Name';}" value="Name"/></td>    
					</tr>
					<tr>						
						<td align="left" valign="top" ><input type="text" name="txtEmail" id="txtEmail" onfocus="javascript: if(this.value=='Email'){this.value='';}" onblur="javascript: if(this.value==''){this.value='Email';}" value="Email"/></td>
					</tr>
					<tr>						
						<td align="left" valign="top" ><input type="text" name="txtMobile" id="txtMobile" maxlength="10" onfocus="javascript: if(this.value=='Mobile'){this.value='';}" onblur="javascript: if(this.value==''){this.value='Mobile';}" value="Mobile" /></td>    
					</tr>
					<tr>						
						<td align="left" valign="top" ><input type="text" name="txtCity" id="txtCity" onfocus="javascript: if(this.value=='City'){this.value='';}" onblur="javascript: if(this.value==''){this.value='City';}" value="City"/></td>
					</tr>
					<tr>						
						<td align="left" valign="top" ><input type="text" name="txtemployees" id="txtemployees" onfocus="javascript: if(this.value=='Number of employees'){this.value='';}" onblur="javascript: if(this.value==''){this.value='Number of employees';}" value="Number of employees"/></td>
					</tr>
					<tr>						
						<td align="left" valign="top" ><div class="submit" style="margin:20px 0px 20px 0px"><a href="javascript:void(0);" onclick="javascript: _checkRegister();"></a></div></td>	     
					</tr>
				</table>
			</form>
			</div><!-- end of corportaeinfo -->
		</div><!-- end of leaseformpanal -->
		<div class="" style="margin:55px 0px 30px 3px;float:left;">
			<img src="./images/leasingbigimg.png" width="687px" height="367px" border="0"/>
		</div>
	</div>	
</div>
<script type="text/javascript">
var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	//$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	//$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Watts Up | Carzonrent</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers cab or car rental services for Delhi, Mumbai, Bangalore, Hyderabad, Gurgaon, Noida and  Pune locations." />
<meta name="keywords" content="car rental, book a cab, car hire, rent a car" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>


<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/expand.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
$(function() {
    $("h3.expand").toggler();
    $("div.other").expandAll({
      expTxt : "[Show]", 
      cllpsTxt : "[Hide]",
      ref : "ul.collapse",
      showMethod : "show",
      hideMethod : "hide"
    });
    $("div.post").expandAll({
      expTxt : "[Read this entry]", 
      cllpsTxt : "[Hide this entry]",
      ref : "div.collapse", 
      localLinks: "p.top a"    
    });    
});
//--><!]]>
</script>
<style>
	.whatsup{
	width:100%;
	height:158px;
	background-image:url('<?php echo corWebRoot; ?>/img_e2o/header.jpg');
	background-position:center center;
	background-repeat:no-repeat;
	background-color:#ededed;
	float:left;
	border-bottom:solid 1px #e2572d;
	}
	.liner
	{
	color: #414142;
	float: left;
	font-size: 15px;
	font-weight: normal;
	padding: 10px 0px;
	width: 100%;
	}
	.mainblock
	{float:left;width:100%;margin:10px 0px 20px 0px;}
	.formblock
	{
		float:left;
		width:550px;height:300px;border: solid 1px #e2572d;
		-moz-border-radius: 5px;
		border-radius: 5px;
		-moz-box-shadow: 5px 5px 5px #8c8c8c;
		-webkit-box-shadow: 5px 5px 5px #8c8c8c;
		box-shadow: 5px 5px 5px #8c8c8c;
	}
	.e2o
	{
		float:right;width:309px;
	}
	.greybandcol
	{
		background-color: #e2572d;
		float: left;
		height: 25px;
		/*-moz-border-radius: 3px;
		border-radius: 3px;*/
		width: 100%;
	}
	.expand a:link, .expand a:visited {
	background-image:none !important;
	background-repeat: no-repeat;
	height: 20px;
      }
      .expand {
	margin-bottom: 0px !important;
      }
      .expand a:link, .expand a:visited {
	background-image:none;
	background-repeat: no-repeat;
      }
      .expand a:link, .expand a:visited {
	border: 0 dotted #CCCCCC;
	color: #555555;
	text-decoration: none;
      }
      .expand a {
	display: block;
	padding: 1px 10px !important;
      }
      .expand a {
	display: block;
	padding: 3px 10px;
      }
      .greybandcol span {
	color: #FFFFFF;
	float: left;
	font-size: 14px;
	font-weight:normal !important;
	padding: 4px 0px 2px 20px;
	width: 90%;
      }
      .expand a.open:link, .expand a.open:visited {
    background: none !important;
    height: 20px;
}
.expand a.open:link, .expand a.open:visited {
    background: none !important;
}
.collapse {
    float: left;
    width: 100%;
}
.wrapacc {
    float: left;border-left:solid 1px #e2572d !important;border-right:solid 1px #e2572d !important;border-bottom:solid 1px #e2572d !important; 
    width: 100%;
    margin-bottom:6px;
}
.bigbox
{
	float:left;margin: 15px 0px 10px 25px;
	width:850px;
}
.box
{
	float:left;margin: 5px 0px 10px 20px;
	width:200px;
}
.box p
{
	float:left;width:150px;border-bottom:dotted 1px #e2572d;padding:8px 0px 8px 0px;color:#404041;
}
.box p .tp
{
	float:right;
}
.box p .rs
{
	float:right;
	background-image:url('<?php echo corWebRoot; ?>/img_e2o/rs.jpg');
	background-position:left 3px;
	background-repeat:no-repeat;
	padding-left:12px;
}
.tc_dis
{
	float:left;width:100%;font-size:13px;
}

.showtime, .showhour {
    background: none repeat scroll 0 0 #e2572d;
    border: 1px solid #e2572d;
    border-radius: 3px;
    color: #fff;
    float: left;
    font-size: 12px;
    margin-top: 5px;
    padding: 5px 10px;
}
.iwan
{
	display:block;color:#404041;padding:25px 0px 25px 15px;font-size:15px;
}

.big select {
	border: 1px solid #e2572d !important;
    border-radius: 3px 3px 3px 3px !important;
    color: #999999 !important;
    font-size: 14px;
    height: 32px;
    line-height: 32px;
    margin-bottom: 10px;
    padding: 2px 5px;
    width: 190px !important;
    box-shadow: none;
}

select {
	border: 1px solid #e2572d !important;
    border-radius: 3px 3px 3px 3px !important;
    color: #999999 !important;
    font-size: 14px;
    height: 32px;
    line-height: 32px;
    margin-bottom: 10px;
    padding: 2px 5px;
    width: 50px !important;
    box-shadow: none;
}
select option{
	padding-left:12px;
	font-size:14px;
	font-weight:normal;
	color:#666666;
	box-shadow: none !important;
}
.mkebtn input {
    background: url("<?php echo corWebRoot; ?>/img_e2o/button.jpg") no-repeat scroll left top transparent;
    border: 0 none;
    cursor: pointer;
    float: left;
    height: 52px;
    line-height: 0;
    overflow: hidden;
    text-indent: -9999px;
    width: 213px;
    margin:15px 0px 0px 170px;
}
.bdr
{border-bottom:solid 1px #e2572d;}
span.customStyleSelectBox {
	border: 1px solid #e2572d !important;
	border-radius: 3px 3px 3px 3px;
	color: #999999;
	font-size: 14px;
	font-weight: bold;
	height: 32px;
	line-height: 32px;
	margin-bottom: 0px;
	padding: 2px 5px;
	width:180px !important;
	background:#FFF;
	box-shadow: none !important;
}
span.customStyleSelectBox.changed {

}
customStyleSelectBoxInner {
	background:url('<?php echo corWebRoot; ?>/img_e2o/arrow.png') no-repeat center right !important;
	width:290px !important;
}
select option {
	width:180px !important;
}
span.customStyleSelectBox.changed {
}
.customStyleSelectBoxInner {
	background:url('<?php echo corWebRoot; ?>/img_e2o/arrow.png') no-repeat center right !important;
	width:180px!important;
}
span.datepick{
	border: 1px solid #e2572d !important;
	border-radius: 3px 3px 3px 3px;
	color: #999999;
	font-size: 16px;
	font-weight: bold;
	height: 32px;
	line-height: 32px;
	padding: 2px 5px;
	width: 140px;
	background:#FFFFFF;	
	float:left;
	display:block;
	box-shadow:none !important;
}
span.datepick input[type="text"] {
	width:135px;
	float:left;
	background:#fff;
	border:none;
	font-size:14px;
	font-weight:bold;
	color:#9f9f9f;
	line-height:28px;
	height:28px;
	margin:3px 0px 0px 0px;
	background:url('<?php echo corWebRoot; ?>/images/datepick.png') no-repeat right 0px!important;
	cursor:pointer;
	box-shadow:none !important;
}

.pickuphour span.customStyleSelectBox {
	border: 1px solid #CCCCCC;
	border-radius: 3px 3px 3px 3px;
	box-shadow:none !important;
	color: #999999;
	font-size: 14px;
	font-weight: bold;
	height: 32px;
	line-height: 32px;
	margin-bottom: 0px;
	padding: 2px 5px;
	width:30px !important;
	background:#FFF;
}
.pickuphour span.customStyleSelectBox.changed {

}
.pickuphour .customStyleSelectBoxInner {
	background:url('<?php echo corWebRoot; ?>/img_e2o/arrow.png') no-repeat center right;
	width:30px !important;
}
.pickuphour select option {
	width:30px !important;
}
.pickupmin span.customStyleSelectBox {
	border: 1px solid #e2572d;
	border-radius: 3px 3px 3px 3px;
	color: #999999;
	font-size: 14px;
	font-weight: bold;
	height: 32px;
	line-height: 32px;
	margin-bottom: 0px;
	padding: 2px 5px;
	width:30px !important;
	background:#FFF;
	box-shadow:none !important;
}
.pickupmin span.customStyleSelectBox.changed {

}
.pickupmin .customStyleSelectBoxInner {
	background:url('<?php echo corWebRoot; ?>/images/arrow.png') no-repeat center right;
	width:30px !important;
}
.pickupmin select option {
	width:30px !important;
}
.bigbox p
{
	float:left;width:100% !important;
}
.bigbox p.faq .q
{
	float:left;width:2%;
	display:block;
}
.bigbox p.faq .a
{
	float:left;width:98%;display:block;
}
</style>

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo (date('d') + 1); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
<script type="text/javascript">
	$(function(){
		$('select.styled').customSelect();
	});
</script>
</head>
<body>
	<!--Header Start Here-->
	<?php include_once("./includes/header.php"); ?>
		    <div style="margin: 0 auto;position: relative;width: 100%;">
			<div class="whatsup"></div>
		    </div>
		    <div class="nbd_center">
			<div id="nbd">
				<p class="liner">Carzonrent introduces the electric Mahindra e2o in its self-drive service. A next-generation electric vehicle, the most convenient and greenest way to get around town. <i>Wattever</i> your destination, just self-drive it.
				</p>
				<div class="mainblock">
					<div class="formblock">
						
						<form id="form3" name="form3" action="<?php echo corWebRoot; ?>/search-result.php" method="post">
							<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
							<input type="hidden" name="hdOriginName" id="hdOriginNameSF" />
							<input type="hidden" name="hdOriginID" id="hdOriginIDSF" />
							<input type="hidden" name="hdDestinationName" id="hdDestinationNameSF" />
							<input type="hidden" name="hdDestinationID" id="hdDestinationIDSF" />
							<input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" />
							<input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo date('h:i A');?>" />
							<input type="hidden" name="userTime" id="userTimeSF" value="" />
							<input type="hidden" name="onlyE2C" id="onlyE2C" value="1" />
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td class="bdr"><span class="iwan">I want a self drive car in</span></td>
								<td class="bdr big">
									<select name="ddlOrigin" id="ddlOriginSF" class="styled" onchange="javascript: _setOrigin(this.id, 's');">
														    <option>Select Origin</option>
	<?php
			$tday = date("Y") . date("m") . date("d") + 1;
			$dt = date_create($tday);
			$selCityId = 2;
			$selCityName = "Delhi";
			$sdOrigin = array(2, 7);
			//$org[] = array("CityID" => 69, "CityName" => "Goa");
			$arCityName = array();
			foreach($org as $k => $v){
				$arCityName[$k] = $v['CityName'];
			}
			array_multisort($arCityName, SORT_ASC, $org);
			
			for($i = 0; $i < count($org); $i++)
			{
				if(in_array($org[$i]["CityID"], $sdOrigin)){
					if($org[$i]["CityName"] == $cCity){
						$selCityId = $org[$i]["CityID"];
						$selCityName = $org[$i]["CityName"];
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
					<script type="text/javascript" language="javascript">
						document.getElementById('hdOriginIDSF').value = '<?php echo $org[$i]["CityID"]; ?>';
						document.getElementById('hdOriginNameSF').value = '<?php echo $org[$i]["CityName"]; ?>';
					</script>
	<?php
					}
					else {
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
	<?php
					}
				}
			}
	?>
									</select>
								</td>
							</tr>
							<tr>
								<td><span class="iwan">Pickup Date &amp; Time</span></td>
								<td><span class="iwan">Drop Date &amp; Time</span></td>
							</tr>
							<tr>
								<td style="padding-left:10px;">
			<p>
				<span class="datepick">
				    <input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo $dt->format("d M, Y"); ?>" id="inputFieldSF1" class="datepicker" onchange="javascript: _setDropDate('inputFieldSF1', 'inputFieldSF2');" readonly="readonly" />
				</span>
				<span class="pickuphour ml5">
					<select class="styled wdth60" name="tHourP" id="tHourP" onchange="javascript: _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTimeSF');">
						<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 8){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
							<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
						<?php } ?>
					</select>
				</span>
				<span class="pickupmin ml5">
					<select class="styled wdth60" name="tMinP" id="tMinP" onchange="javascript: _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTimeSF');">
						<!--<option>Min</option>  -->
						<option value="00">00</option> 
						<option value="15">15</option> 
						<option value="30">30</option>
						<option value="45">45</option>
					</select>
				</span>
			</p>
			<span id="selTP" style="margin:10px 0px 10px 0px; float:left;" class="showtime"><span id="seltimeP"></span></span>
			<script type="text/javascript">
				 _setTime('tHourP', 'tMinP', 'selTP', 'seltimeP', 'userTimeSF');
			</script>
								</td>
								<td>
			<p>
					<span class="datepick">
					<input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo $dt->format("d M, Y"); ?>" id="inputFieldSF2" class="datepicker" readonly="readonly" />
					</span>
					<span class="pickuphour ml5">
						<select class="styled wdth60" name="tHourD" id="tHourD" onchange="javascript: _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTimeNA');">
							<!--<option>Hours</option> -->
							<?php for($i = 0; $i <= 23; $i++) { $sel = ""; if($i == 8){$sel = "selected='selected'";} if($i < 10) { $t = "0" . $i; } else { $t = $i;}?>
								<option value="<?php echo $t; ?>" <?php echo $sel; ?>><?php echo $t; ?></option> 
							<?php } ?>
						</select>
					</span>
					<span class="pickupmin ml5">
						<select class="styled wdth60" name="tMinD" id="tMinD" onchange="javascript: _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTimeNA');">
							<option value="00">00</option> 
							<option value="15">15</option> 
							<option value="30">30</option>
							<option value="45">45</option>
						</select>
					</span>
			</p>
				<span id="selTD" style="margin:10px 0px 10px 0px; float:left;" class="showtime"><span id="seltimeD"></span></span>
				<script type="text/javascript">
					 _setTime('tHourD', 'tMinD', 'selTD', 'seltimeD', 'userTimeNA');
				</script>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<span class="mkebtn" style="float:left;">
									<input type="button" id="btnMakePaymentSF" name="btnMakePayment" value="Make Your Booking Now" onclick="javascript: return _validateSF();" />
								      </span>
								</td>
							</tr>
							
						</table>
			
					</div>
					<div class="e2o">
						<img src="<?php echo corWebRoot; ?>/img_e2o/e2o.jpg" alt="" title="" />
					</div>
				</div>
					</form>
				<div class="os">
					<div class="wrapacc">
						<h3 class="greybandcol"><span> Prices</span></h3>
						
						<!--<div class="collapse">-->
						<div class="collapse">
							<div class="box">
								<p>Duration <span class="tp">Price<sup>*</sup></span></p>
								<p>2 hours <span class="rs">200</span></p>
								<p>4 hours <span class="rs">300</span></p>
								<p>6 hours <span class="rs">400</span></p>
								<p>12 hours <span class="rs">500</span></p>
								<p>24 hours <span class="rs">800</span></p>
							</div>
						</div>
					</div>
					<div class="wrapacc">
						<h3 class="greybandcol expand"><span>FAQ's</span></h3>
						
						<div class="collapse">
							<div class="bigbox">
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">The e2o Self-drive service is currently available in which cities?</span>
									<span class="q">A. </span>
									<span class="a">Mahindra e2o is currently available in Delhi, Mumbai & Bangalore. However, other cars for self-drive are available in Delhi, Mumbai, Bangalore, Hyderabad & Goa.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">How much does the car run in one charge?</span>
									<span class="q">A. </span>
									<span class="a">The car can run upto 80kms on a full charge.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">How much time does it takes to charge the car?</span>							
									<span class="q">A. </span>
									<span class="a">A full charge takes only 5 hours. </span>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Where can I charge my car?</span>
									<span class="q">A. </span>
									<span class="a">Apart from the regular charge points at your homes & offices, the car can be charged for free at the below mentioned locations. Delhi Mumbai Bangalore</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Is there a pick up and drop facility?</span>
									<span class="q">A. </span>
									<span class="a">Currently, we provide the car from the airports & select city points. They can also be delivered at your preferred location on an extra cost.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">What is the security deposit and how it is done?</span>
									<span class="q">A. </span>
									<span class="a">The security deposit is Rs.5,000/- and it is pre-authorized on a credit card.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Is there a GPS or tracking system?</span>
									<span class="q">A. </span>
									<span class="a">Yes, the cars can be tracked real-time & a GPS device can be made available on request.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">What if the car battery runs out in between?</span>
									<span class="q">A. </span>
									<span class="a">The car has a display unit which informs about the percentage of battery left & the expected number of kms. In case of an assistance, you may please contact us at our 24*7 contact center -  0888 222 2222 </span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">How much does the full charge costs?</span>
									<span class="q">A. </span>
									<span class="a">A full charge only costs Rs.50/- at a domestic electricity consumption unit.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Can the car be charged at any plug point?</span>
									<span class="q">A. </span>
									<span class="a">The specifications for the charge point are the regular 230v, 50hz, 3kw, 16A points available at our home & offices.</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Is there an alternate fuel option in the car?</span>
									<span class="q">A. </span>
									<span class="a">No, the car only runs on battery</span>
								</p>
								<p class="faq">
									<span class="q">Q. </span>
									<span class="a">Is this car insured like all other cars?</span>
									<span class="q">A. </span>
									<span class="a">Yes, the car has a full insurance cover.</span>
								</p>
								<!-- Please <a href="<?php echo corWebRoot; ?>/e2o-terms-and-conditions.php">Click here</a> for terms &amp; conditions on the service. -->

								Please <a id="tandclink" onclick="javascript:_disableThisPage();_setDivPos('tc-sd');" href="javascript: void(0);">Click here</a> for terms &amp; conditions on the service.
							</div>
						</div>
					</div>
					<div class="wrapacc">
						<h3 class="greybandcol expand"><span>List of car charging points</span></h3>
						
						<div class="collapse">
							<div class="bigbox">
								<a href="http://www.carzonrent.com/beta/img_e2o/Delhi-NCR.pdf" target="_blank">Delhi NCR</a>&nbsp;|&nbsp;<!--<a href="http://www.carzonrent.com/beta/img_e2o/Mumbai.pdf" target="_blank">Mumbai</a>&nbsp;|&nbsp;--><a href="http://www.carzonrent.com/beta/img_e2o/Bangalore.pdf" target="_blank">Bangalore</a>
							</div>
						</div>
					</div>
					<div class="wrapacc">
						<h3 class="greybandcol expand"><span>To know more about the car</span></h3>
						
						<div class="collapse">
							<div class="bigbox">
								<p class="faq">
									Carzonrent introduces the electric Mahindra e2o in its self-drive service. A next-generation electric vehicle, the most convenient and greenest way to get around town. Wattever your destination, just self-drive it.<br /><br />With an automatic transition, the e2o makes city driving enjoyable. With the smart phone app, you can lock / unlock and operate AC remotely. The electric e2o has zero emission making it best for congested city driving. You never have to visit a petrol pump again or worry about scratched or dents!<br /><br />

									<b>Automatic Drive: </b><br/>Driving through start-stop city traffic can wear you down. But driving an automatic car takes a lot of pressure off you! No Gears, No Clutch, No Worries!<br /><br /> 
									<b>Dent Resistant Body:</b><br/>
									The body is made of dent and scratch resistant, color impregnated composites which is lighter and tougher than metal bodies. <br /><br />

									<b>The most silent drive: </b><br/>
									The e2o is a relief from constant deafening city sounds. The car does not has a noisy IC engine which ensure you have a peaceful and calm drive. <br /><br />

									<b>Turning Radius: </b><br/>
									With just 3.9 mm turning radius, it makes it even more convenient to maneuver in city traffic.<br /><br />

									<b>Hill Hold: </b><br/>
									Prevents your e2o from rolling backwards on inclines.<br /><br />

									<b>Reverse Camera: </b><br/>
									Now no need to turn the heads all around while reversing. The infotainment system becomes your reverse camera once put in that mode!<br /><br />

									<b>Bye Bye Petrol: </b><br/>
									No need to worry on waiting in long queues in petrol stations or burning a hole in your pocket. E2o simply uses electricity!<br />
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="tc_dis">
					<b>AVAILABLE IN BANGALORE AND DELHI. COMING SOON IN MUMBAI.<br />
					<sup>*</sup> T &amp; C apply</b>: Service tax extra. <b>Unlimited driving. Free charging facility available</b>
				</div>
			</div>
		</div>
		
	<!--footer Starts Here-->
	<?php include_once("./includes/footer.php"); ?>
	<!--footer Ends Here-->
	<div class="tcpopup" id="tc-sd">
		<div class="heading">Terms &amp; Conditions <span class="closedpop" onclick="_hideFloatingObjectWithID('tc-sd'); _enableThisPage();">X</span></div>
		<table cellspacing="0" border="1" width="100%" style="border-collapse:collapse;margin-bottom:10px;">
			<tr>
				<td style="width:25%;padding:5px;"><b>Criteria</b></td>
				<td style="padding:5px;"><b>Description</b></td>
			</tr>
			<tr>
				<td style="padding:5px;">Eligibility Age</td>
				<td style="padding:5px;">25 Years or above</td>
			</tr>
			<tr>
				<td style="padding:5px;">Documents</td>
				<td style="padding:5px;">Passport , Driving license , Credit Card with desired limit of Security deposit</td>
			</tr>
			<tr>
				<td style="padding:5px;">Rental Period</td>
				<td style="padding:5px;">Rental period will be 24 Hrs. from customer booking time , included in this 24 Hour is the time where customer collect and return the rental vehicle</td>
			</tr>
			<tr>
				<td style="padding:5px;">Other terms</td>
				<td style="padding:5px;">As per agreement</td>
			</tr>
			<tr>
				<td style="padding:5px;">VAT</td>
				<td style="padding:5px;">As  per state</td>
			</tr>
			<tr>
				<td style="padding:5px;">Pre authorization from credit card</td>
				<td style="padding:5px;">Will be done at the time of vehicle delivery , this will be treated as security deposit , in case of any damage in the car , amount will be deducted from the pre authorized amount.</td>
			</tr>
			<tr>
				<td style="padding:5px;">Delivery / Collection</td>
				<td style="padding:5px;">Car has to be picked up from our Hub / Airport and returned to same Hub / airport it was hired from</td>
			</tr>	
			<tr>
				<td style="padding:5px;">Extra day</td>
				<td style="padding:5px;">In case customer want to extend his travel , there will the additional charge of 30% on per day rental.</td>
			</tr>
			<tr>
				<td style="padding:5px;">Documents and Accessories</td>
				<td style="padding:5px;">Missing documents or lost accessories and tools would be charged extra.</td>
			</tr>
			<tr>
				<td style="padding:5px;">GPS Navigation system</td>
				<td style="padding:5px;">Available on request with additional charges (Subject to availability)</td>
			</tr>
			<tr>
				<td style="padding:5px;">Child Safety Seat</td>
				<td style="padding:5px;">Availability of child seat is on request with additional charges. The age or weight of the child must be advised at the time of booking (Subject to availability)</td>
			</tr>
			<tr>
				<td style="padding:5px;">Interstate Tax and Toll Charges</td>
				<td style="padding:5px;">All Interstate taxes and toll will be paid by customer</td>
			</tr>
		</table>
</div>
</div>
</body>
</html>

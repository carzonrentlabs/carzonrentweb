<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrnet Offers- Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />

</head>
<body>
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder" style="margin-top: 20px;">
		<div class="magindiv" style="padding:7px 0px 7px 0px;width:1130px;">
			<h2>Carzonrent Offers</h2>
		</div>
	</div>	
	<div style="margin:0 auto;width:1130px;">
			<div class="divcenter" style="width:100%">
			<div class="contactleftdiv">
			
			    
				<div  style="height:120px;">
					<h3>Terms and conditions:</h3></br>
					<ul>
					<li>Offer – Flat Rs 350 Off</li>
					<li>Use Code – <b>CORGL350</b></li>
					<li>Applicable for outstation bookings only.</li>
					<li> Validity till 31 Dec -2016</li>
					<li> Offer not applicable in case the booking is cancelled.</li>
					<li>The discount amount for all codes is applicable on the base fare only.</li>
					<li>Offers cannot be clubbed with any other promotion.</li>
					<li>Discounts can be redeemed in full & not in parts.</li><br/><br/>

</p>
					
					
					
				</div>
				
				
			</div>
		
		</div>
	</div>	
</div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

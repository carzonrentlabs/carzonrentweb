<?php
    session_start();
	error_reporting(0);
	include_once('./classes/cor.mysql.class.php'); 
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once('./classes/cor.gp.class.php');
	include_once("./includes/cache-func.php");
	$rootpath=corWebRoot;
	//ini_set("display_errors", 1);
	$invoker = $_SERVER['HTTP_REFERER'];
	$isExist = strrpos($invoker,$rootpath);
	
	
	$allowed = "no";
	if(isset($_POST['bookingid']) && $_POST['bookingid'] != "" && $_POST['hdAllowFormSubmit'] == 'no')
		$allowed = "yes";
	if(isset($_POST["txtname"]) && $_POST["txtname"] != "" && isset($_POST["monumber"]) && $_POST["monumber"] != "" && isset($_POST["email"]) && $_POST["email"] != ""  && $_POST['hdAllowFormSubmit'] == 'no')
		$allowed = "yes";
	
	//if($isExist > 0 && $allowed == "yes") old
	  if($isExist == 0 && $allowed == "yes")
	//if(isset($_POST['feedbacksubmit']) && $isExist > 0 && $allowed == "yes")
	{
		$db = new MySqlConnection(CONNSTRING);
		$dataToSave = array();
		$dataToMail= array();
		$db->open();
		$cor = new COR();
		
		$dataToSave["purpose_of_trip"] = $_POST['ddlpurpose'];
		$dataToSave["other_purpose"] = $_POST['txtOtherPurpose'];
		$dataToSave["reason_1"] = $_POST['satisfied1'];
		$dataToSave["reason_2"] = $_POST['satisfied2'];
		$dataToSave["reason_3"] = $_POST['satisfied3'];
		$dataToSave["reason_4"] = $_POST['satisfied4'];
		$dataToSave["reason_5"] = $_POST['satisfied5'];
		$dataToSave["reason_6"] = $_POST['satisfied6'];
		$dataToSave["reason_7"] = $_POST['satisfied7'];
		$dataToSave["other_reason"] = $_POST['txtOtherReason'];
		$dataToSave["experience"] = $_POST['rd'];
		$dataToSave["experience_1"] = $_POST['chkwwr1'];
		$dataToSave["experience_2"] = $_POST['chkwwr2'];
		$dataToSave["experience_3"] = $_POST['chkwwr3'];
		$dataToSave["experience_4"] = $_POST['chkwwr4'];
		$dataToSave["experience_5"] = $_POST['chkwwr5'];
		$dataToSave["experience_6"] = $_POST['chkwwr6'];
		$dataToSave["experience_7"] = $_POST['chkwwr7'];
		$dataToSave["other_experience"] = $_POST['txtWrong'];
		$dataToSave["feedback"] = $_POST['txtfeedback'];
		$dataToSave["entry_date"] = date('Y-m-d H:i:s');
		$dataToSave["ip"] = $_SERVER["REMOTE_ADDR"];
		$dataToSave["ua"] = $_SERVER["HTTP_USER_AGENT"];

		$tourtype = $_POST['hdIsSelfDrive'];


		if(isset($_POST['bookingid']) && $_POST['bookingid'] != "")
		{
			$bookingID = $_POST['bookingid'];
			$myXML = new CORXMLList();

			$res = $cor->_CORGetBookingsDetail(array("BookingID"=>$bookingID));
			$bookingDetails = array();
			if($res->{'GetBookingDetailResult'}->{'any'} != "")
			{
				$myXML = new CORXMLList();
				$bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});


				$pickDate = date_create($bookingDetails[0]["PickUpDate"]);
				$pickDate->format("Y-m-d");
				$dropDate = date_create($bookingDetails[0]["DropOffDate"]);
				$dropDate->format("Y-m-d");
				$dataToSave["booking_id"] = $bookingID;

				$dataToSave["CarCatName"] = $bookingDetails[0]["CarCatName"];
				$dataToSave["destination"] = $bookingDetails[0]["destination"];
				$dataToSave["PickUpTime"] = $bookingDetails[0]["PickUpTime"]; 
				$dataToSave["PickUpAdd"] = $bookingDetails[0]["PickUpAdd"]; 
				$dataToSave["Fname"] = $bookingDetails[0]["Fname"];
				$dataToSave["Lname"] = $bookingDetails[0]["Lname"];
				$dataToSave["mobile"] = $bookingDetails[0]["mobile"];
				$dataToSave["EmailID"] = $bookingDetails[0]["EmailID"];
				$dataToSave["BookingSource"] = $bookingDetails[0]["BookingSource"];
				$dataToSave["userid"] = $bookingDetails[0]["userid"];
				$dataToSave["PaymentStatus"] = $bookingDetails[0]["PaymentStatus"];
				$dataToSave["PaymentAmount"] = $bookingDetails[0]["PaymentAmount"];
				$dataToSave["trackID"] = $bookingDetails[0]["trackID"];
				$dataToSave["VisitedCities"] = $bookingDetails[0]["VisitedCities"];
				$dataToSave["OutstationYN"] = $bookingDetails[0]["OutstationYN"];
				$dataToSave["Remarks"] = $bookingDetails[0]["Remarks"];
				$dataToSave["IndicatedDiscPC"] = $bookingDetails[0]["IndicatedDiscPC"];
				$dataToSave["PickUpDate"] = $pickDate->format("Y-m-d");
				$dataToSave["DropOffDate"] = $dropDate->format("Y-m-d");
				$dataToSave["OriginName"] = $bookingDetails[0]["OriginName"];
				$dataToSave["originId"] = $bookingDetails[0]["originId"];
				$dataToSave["Status"] = $bookingDetails[0]["Status"];
				$dataToSave["TourType"] = $bookingDetails[0]["TourType"];
				$dataToSave["IndicatedPkgID"] = $bookingDetails[0]["IndicatedPkgID"];
				$dataToSave["DiscountAmount"] = $bookingDetails[0]["DiscountAmount"];
				$dataToSave["ChauffeurCharges"] = $bookingDetails[0]["ChauffeurCharges"];
				$dataToSave["DropOffTime"] = $bookingDetails[0]["DropOffTime"];
				$dataToSave["serviceAmount"] = $bookingDetails[0]["serviceAmount"];
				$dataToSave["PayBackMember"] = $bookingDetails[0]["PayBackMember"];
				$dataToSave["subLocationID"] = $bookingDetails[0]["subLocationID"];
			}
		}
		else
		{
			$dataToSave["Fname"] = $_POST["txtname"];
			$dataToSave["Lname"] = "";
			$dataToSave["mobile"] = $_POST["monumber"];
			$dataToSave["EmailID"] = $_POST["email"];
		}
		if($tourtype == 0)
		{
			$dataToSave["convenient_booking_process"] = $_POST['rdo1'];
			$dataToSave["cab_report_time"] = $_POST['rdo2'];
			$dataToSave["chauffer_experience"] = $_POST['rdo3'];
			$dataToSave["expectation_met"] = $_POST['rdo4'];
			$dataToSave["overall_experience"] = $_POST['rdo5'];
		}

		$r = $db->insert("myles_feedback", $dataToSave);
		
		$sql = "SELECT * FROM cor_booking_new WHERE booking_id= " . $_POST['bookingid'];
		$r = $db->query("query", $sql);
		$subject='Selfdrive';
		$href='http://www.mylescars.com/';
		$logo='http://www.mylescars.com/img/myles-logo.png';
		$alt='Mylescars logo';
		$title='Mylescars.com';
		if(!array_key_exists("response", $r)){
			$msg_representative='<table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:#666 solid 1px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal;"><tr><td colspan="2" bgcolor="#666666"></td></tr><tr><td style="border-bottom:#666 solid 1px; background:#999999"><a href="'.$href.'" target="_blank"> <img src="'.$logo.'" width="150" height="60" border="0" alt="'.$alt.'" title="'.$title.'"/> </a></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p> <strong>Myles FeedBack</strong></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Mobile no</b>: '. $r[0]["mobile"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Name</b>: '.ucfirst($r[0]["full_name"]).'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Email</b>: '.$r[0]["email_id"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Tour Type</b>: '.$bookingDetails[0]["TourType"] .'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Origin Name</b>: '.$bookingDetails[0]["OriginName"].'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Destination</b>: '.$bookingDetails[0]["destination"] .'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>IP Address</b>: '.$r[0]["ip"] .'</b></p><p style="padding-left:20px;"></p></td><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Source Website</b>: www.carzonrent.com</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Feedback</b>: '. $_POST['txtfeedback'] .'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><br/><br/><strong>Thanks,<br /> '.ucfirst($r[0]["full_name"]).'</strong></td></tr><tr><td colspan="2" bgcolor="#666666"></td></tr></table>';	
		}
		else
		{
			$msg_representative='<table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:#666 solid 1px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal;"><tr><td colspan="2" bgcolor="#666666"></td></tr><tr><td style="border-bottom:#666 solid 1px; background:#999999"><a href="'.$href.'" target="_blank"> <img src="'.$logo.'" width="150" height="60" border="0" alt="'.$alt.'" title="'.$title.'"/> </a></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p> <strong>FeedBack Details</strong></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Mobile no</b>: '.$_POST["monumber"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Name</b>: '.ucfirst($_POST["txtname"]).'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Email</b>: '.$_POST["email"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Query</b>: '.$_POST["txtfeedback"].'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><br/><br/><strong>Thanks,<br /> '.ucfirst($_POST["txtname"]).'</strong></td></tr><tr><td colspan="2" bgcolor="#666666"></td></tr></table>';
		}
		$sql = "select email from EmailManagement as em join email_page_master as epm on em.p_id= epm.p_id where epm.page_name='Carzonrent(myles) Feedabck' and em.status='1'";
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
        $resemail= $db->query("query", $sql);
		$a='';
		for($i=0;$i<=count($resemail)-1;$i++)
		{
			$a.= $resemail[$i]['email'].';';
			$email=trim($a,';');
		}
        $db->close();
		$dataToMail["To"] = $email;
		$dataToMail["Subject"] = "Booking Feedback";
		$dataToMail["MailBody"] = $msg_representative;
		$res = $cor->_CORSendMail($dataToMail);

		unset($cor);
		unset($dataToMail);
		$db->close();
		session_destroy();
		unset($dataToSave);
		$root=corWebRoot;
		header("Location:".$rootpath."/booking-feedback-thanks.php");
	}
	else
		header("Location: ".$rootpath."/");
?>
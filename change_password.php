<?php
	
	if(!isset($_COOKIE["cciid"]) || !isset($_COOKIE["emailid"]))
		header("Location: ./login.php?rurl=./myaccount.php");
	error_reporting(0);
	include_once('classes/cor.ws.class.php');
	include_once('classes/cor.xmlparser.class.php');
	if($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != ""){
		$userId = $_COOKIE["cciid"];
	}
		
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("includes/seo-metas.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/enfeedback.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_disablepage.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_elmpos.js"></script>
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/_bb_general_v3.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" ></script>
<link rel="stylesheet" type="text/css" href="./css/global.css?v=<?php echo mktime(); ?>" />
</head>
<body>
<script language="javascript" type="text/javascript">
var id;
$(document).ready(function(eOuter) {
    $('input').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });
	$('#txtPassword').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
			
			_changePassword();
		}
	});
	
});

function _changePassword(){
	var chk;
	chk = true;
	if(chk)
		chk = isFilledText(document.getElementById("txtPassword"), "", "Please fill in your password.");
	if(chk){
		if(document.getElementById("txtPassword").value.trim() != document.getElementById("txtCPassword").value.trim()){
			alert("Your passwords are not matching.");
			chk = false;
		}
	}
	if(chk)
		document.getElementById('frmCHGPassword').submit();
}
</script>
<!--Header Start Here-->
<?php include_once("includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<!--<div class="tbul">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">Set New Password </a></li>
    
  </ul>
</div>-->
<div class="lfcnt">
<div id="country1" class="tabcontent">
<h1>Set New Password</h1>
  
<div class="businessEnquiry">
<form action="password-change.php" method="post" name="frmCHGPassword" id="frmCHGPassword">
<table width="75%" cellpadding="0" cellspacing="10" border="0" align="left">
	<tr>
	    <td align="right" valign="middle"><label>New password</label></td>
	    <td align="left" valign="top" ><input type="password" name="txtPassword" id="txtPassword" tabindex="1"  /></td>
			 
	</tr>
	<tr>
	    <td align="right" valign="middle"><label>Confirm new password</label></td>
	    <td align="left" valign="top" ><input type="password" name="txtCPassword" id="txtCPassword" tabindex="2" /></td>
		<input type='hidden'  name="GuestID"  value='<?php echo $userId; ?>'/>
		<input type='hidden'  name="UserID"  value='1'/>	 
	</tr>
	<tr>
	    <td align="right" valign="middle"></td>
	    <td align="left" valign="top" ><div class="submit"><a href="javascript: void(0);" onclick="javascript: _changePassword();" tabindex="3"></a></div></td>			 
	</tr>
</table>
</form></div>

  <div class="clr"></div>
</div>
  <p>&nbsp;</p>


</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

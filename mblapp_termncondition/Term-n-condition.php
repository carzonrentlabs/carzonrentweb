<?php
include_once("../includes/cache-func.php");
include_once('../classes/cor.mysql.class.php');
?>
<style type="text/css">
.term_mobile{
width:100%;
}
.term_mobile input,
.term_mobile button{
display:block;
margin-bottom:10px;
width:60%;
}
.term_mobile input{
border-radius: 2px;
-moz-border-radius: 2px;
-webkit-border-radius: 2px;
-o-border-radius: 2px;
padding: 0px 0 0 5px;
border: 1px solid #909090;
line-height: 26px;
height: 26px;
}
.term_mobile button[type="button"]{
border-radius:2px;
-moz-border-radius:2px;
-webkit-border-radius:2px;
-o-border-radius:2px;
padding:0px 0 0 5px;
border:1px solid #909090;
line-height:32px;
height:32px;
width:100px;
color:#fff;
background:#000;
cursor:pointer;
}
.term_mobile button[type="button"]:hover{
background:#e1471b;
}
</style>
</head>

<body>
<div class="term_mobile">
<p>
CARZONRENT may send booking confirmation, itinerary information, cancellation, payment confirmation, refund status, schedule change or any such other information relevant for the transaction, via SMS or by voice call on the contact number given by the User at the time of booking; CARZONRENT may also contact the User by voice call, SMS or email in case the User couldn't or hasn't concluded the booking, for any reason what so ever or to know the preference of the User for concluding the booking and also to help the User for the booking. The User hereby unconditionally consents that such communications via SMS and/ or voice call by CARZONRENT is (a) upon the request and authorization of the User, (b) 'transactional' and not an 'unsolicited commercial communication' as per the guidelines of Telecom Regulation Authority of India (TRAI) and (c) in compliance with the relevant guidelines of TRAI or such other authority in India and abroad.
</p>
<img style="float: right; height: 36px; margin-right: 503px; margin-top: -6px;display:none;" src="<?php echo corWebRoot; ?>/images/32.gif" id="loading_image"/>
<input placeholder="Please Enter Name." name="chatemail" id='chatemail' type="text">
<input class="tc_input" placeholder="" name="entrydate" id='entrydate' type="text" value="<?PHP echo date("Y-m-d h:i:sa");?>" disabled>

<button type="button" id="btnfrst" name='submit' onclick="troggleMail();">Submit</button>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
function troggleMail()
{
var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var emailId= document.getElementById('chatemail').value;
var entrydate= document.getElementById('entrydate').value;
//var coric =$('#coricid').val();
if(emailId=="")
{
alert('Please Enter Full Name.');
$("#chatemail").attr('placeholder','Please Full Name.').css('background','rgb(240, 223, 205)').focus();
return  false;
}
var dataString = 'emailId='+ emailId+'&entrydate='+entrydate;
$.ajax({type:"POST",
url: "<?php echo corWebRoot; ?>/mblapp_termncondition/sendemailuser.php",
data:dataString,
beforeSend: function() {
$('#loading_image').show();
$('#btnfrst').attr('disabled','disabled');
},
success: function(response) { 
var txt=response;
if(txt==1)
{
$('#loading_image').hide();
}
}});

}
</script>

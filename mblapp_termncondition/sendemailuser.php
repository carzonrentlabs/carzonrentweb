<?php
if(isset($_POST["emailId"])){
session_start();
include_once("../includes/cache-func.php");
include_once('../classes/cor.mysql.class.php');
require('../mblapp_termncondition/pdf/fpdf.php');
$dataToSave = array();
$db = new MySqlConnection(CONNSTRING);
$db->open();
$dataToSave["name"] = $_REQUEST['emailId'];
$dataToSave["entrydate"] = $_REQUEST['entrydate'];
$dataToSave["source"] = 'Mobile App';
//$dataToSave["coric"] = $_REQUEST['coric'];
$r = $db->insert("cor_accept_tnc", $dataToSave);
$_SESSION['lastinsertId']=$r['lastinsertId'];
unset($dataToSave);
$db->close();
class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('../mblapp_termncondition/pdf/COR-logo.gif',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(60,10,'User Agreement',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
$text1='The API is easy to use and fully supports HTML/CSS2/JavaScript.';
$text2='The API is easy to use and fully supports HTML/CSS2/JavaScript.';
$text3='The API is easy to use and fully supports HTML/CSS2/JavaScript.';
$pdf->Cell(50,5,$text1,0,1);
$pdf->Cell(50,5,$text2,0,1);
$pdf->Cell(50,5,$text3,0,1);
$pdf->Cell(50,10,ucfirst($_REQUEST['emailId']),0,1);
$pdf->Cell(0,4,$_REQUEST['entrydate'],0,1);
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/mblapp_termncondition/agreementdoc/'.$_REQUEST['emailId'].'-'.date("d-m-Y-h-m-s").'.pdf','F');
$db = new MySqlConnection(CONNSTRING);
$db->open();
$dataToSave = array();
$dataToSave["tncpdf"] = $_REQUEST['emailId'].'-'.date("d-m-Y-h-m-s").'.pdf';  // Added by Iqbal on 28Aug2013
$whereData = array();
$whereData["id"] =$_SESSION['lastinsertId'];
$r = $db->update("cor_accept_tnc", $dataToSave, $whereData);
$db->close();
unset($whereData);
unset($dataToSave);
unset($_SESSION['lastinsertId']);
echo '1';
}
?>
<?php
	error_reporting(0);
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/cache-func.php");
	include_once('./classes/cor.mysql.class.php');
	include_once("./includes/dbconnect.php"); 
	
	if(!isset($_GET["c"]) && !isset($_GET["ci"])){
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$sql = "SELECT visitor_count FROM cor_visitors";
		$r = $db->query("query", $sql);
		$vCount = $r[0]["visitor_count"];
		unset($r);
		$sql = "UPDATE cor_visitors SET visitor_count = visitor_count + 1";
		$r = $db->query("query", $sql);
		unset($r);
		//if($vCount % 4 != 0 || $vCount % 5 == 0){
		/*if($vCount % 2 == 0){
		    header("Location: http://www.carzonrent.com/home.php");
		}*/
		$db->close();	
	}
	
	$useragent=$_SERVER['HTTP_USER_AGENT'];  
	if(preg_match('/android|avantgo|samsung|HTC|nokia|iphone|Opera Mini|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))  
	header('Location: http://m.carzonrent.com/');
	
	$refCode = "";
	if(isset($_GET["ref"]) && trim($_GET["ref"]) != "")
	$refCode = $_GET["ref"];
	$refURL = $_SERVER['HTTP_REFERER'];
	
	if($refCode != "" || $refURL != ""){
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$campaign_code = "MYLES";
		$trackData = array();
		$trackData["campaign_code"] = $campaign_code;
		$trackData["entry_date"] = date('Y-m-d H:i:s');
		$trackData["ref_code"] = $refCode;
		$trackData["ref_url"] = $refURL;
		$trackData["curr_url"] = $_SERVER['REQUEST_URI'];
		$trackData["ip"] = $_SERVER["REMOTE_ADDR"];
		$trackData["ua"] = $_SERVER["HTTP_USER_AGENT"];
		$r = $db->insert("cor_camaign_tracking", $trackData);
		
		unset($trackData);
		$db->close();
		
		if($refCode != "")
		setcookie("refCode", $refCode, time() + (24 * 60 * 60) , "/");
		if($refURL != "")
		setcookie("refURL", $refURL, time() + (24 * 60 * 60) , "/");
	}
	
	$isHrAvail = true;
	$pkt = "Hourly";
	if(isset($_GET["pkt"]) && $_GET["pkt"] != "")
	$pkt = $_GET["pkt"];
	
	$arr = array();
	$arr["CityID"] = $cCityID;
	$arr["PkgType"] = $pkt;
	$cor = new COR();
	$res = $cor->_CORGetPackageDetails($arr);	
	$myXML = new CORXMLList();
	$pkgs = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	
	unset($arr);
	$arr = array();
	$arr["CityID"] = $cCityID;
	$arr["PkgType"] = "Daily";
	$res = $cor->_CORGetPackageDetails($arr);	
	$pkgsDaily = $myXML->xml2ary($res->{"GetPackageDetailsResult"}->{"any"});
	unset($myXML);
	unset($cor);
	
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	$orgH = $org;
	$orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	$orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
	$orgH[] = array("CityID" => 39, "CityName" => "Manesar");
	$orgH[] = array("CityID" => 41, "CityName" => "Bhubaneswar");
	$orgH[] = array("CityID" => 69, "CityName" => "Goa");
	$orgH[] = array("CityID" => 9, "CityName" => "Chandigarh");
	$orgH[] = array("CityID" => 43, "CityName" => "Amritsar");
	
	if($cCityID != "0" && $cCity != ""){
	    $iOrgFound = false;
	    for($o = 0; $o < count($orgH); $o++){
		if($cCityID == $orgH[$o]["CityID"] && strtolower($cCity) == strtolower($orgH[$o]["CityName"]))
		$iOrgFound = true;
	    }
	    if(!$iOrgFound)
	    header('Location: http://www.carzonrent.com/page-not-found/'); 
	}
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

$mainData=$_REQUEST['q'];
$selectQueryData="select * from cor_static_page where web_url='".$mainData."'";

$querydata=mysql_query($selectQueryData);
$result=mysql_fetch_array($querydata);
$descriptionVal=$result['meta_description'];
$metakeyword=$result['meta_keyword'];
$metatitle=$result['title'];

?>
<title><?php echo $metatitle; ?></title>
<meta name="description" content="<?php echo $descriptionVal; ?>" />
<meta name="keywords" content="<?php echo $metakeyword; ?>" />
<?php  ?>

<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<!--<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />-->
<!-- Slider image start -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/slider/slidesjs.css" type="text/css" media="screen" />
<!-- Slider image end -->



<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>
<script type="text/javascript">
  $j = jQuery.noConflict();
  $j(document).ready(function(){
    $j("div.item").tooltip();
    
  });
</script>  
<script>
    var mode = "Hourly";
    var iCount = 0;
<?php
	if(count($pkgsDaily) > 0){
?>
    var iPkgName = "<?php echo trim(strtolower($pkgsDaily[0]["PkgName"])); ?>";
    var iPkgID = <?php echo $pkgsDaily[0]["PkgID"]; ?>;
<?php	
	}
?>
    var hPkgID = new Array();
    var dPkgID = new Array();
    var hCarImg = new Array();
    var dCarImg = new Array();
    var hCarName = new Array();
    var dCarName = new Array();
    var hCarDec = new Array();
    var dCarDec = new Array();
    var hPkgRate = new Array();
    var dPkgRate = new Array();
    var hSecDeposit = new Array();
    var dSecDeposit = new Array();
    _setCount = function(c){
		if (mode == "Hourly") {
			for(p = 0; p < hPkgID.length; p++){
				if (hPkgID[p] == c) {
					iCount = p;
					iPkgName = hCarName[p];
					break;
				}
			}
		} else {
			for(p = 0; p < dPkgID.length; p++){
				if (dPkgID[p] == c) {
					iCount = p;
					iPkgName = dCarName[p];
					break;
				}
			}
		}
	}
    _setMode = function(md){
		mode = md;
		if (md == "Hourly") {
			for(p = 0; p < hPkgID.length; p++){
				if (hCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = hPkgID[p];
					break;
				}
			}
			
			document.getElementById('aHr').className = "selected";
			document.getElementById('aDl').className = "";
			document.getElementById('mycarousel-1').style.display = "none";
			document.getElementById('mycarousel').style.display = "block";
			jQuery('#mycarousel').jcarousel({
				start: parseInt(eval(iCount + 1))
			});
		} else {
			for(p = 0; p < dPkgID.length; p++){
				if (dCarName[p].toLowerCase() == iPkgName.toLowerCase()) {
					iCount = p;
					iPkgID = dPkgID[p];
					break;
				}
			}
			document.getElementById('aHr').className = "";
			document.getElementById('aDl').className = "selected";
			document.getElementById('mycarousel-1').style.display = "block";
			document.getElementById('mycarousel').style.display = "none";
			jQuery('#mycarousel-1').jcarousel({
				start: parseInt(eval(iCount + 1))
			});
		}
		_sePrice();
	}
	_sePrice = function(){
		if (mode == "Hourly") {
			<?php
				if(count($pkgs) > 0){
					for($pk = 0; $pk < count($pkgs); $pk++){
			?>
				document.getElementById('cars-<?php echo $pkgs[$pk]["PkgID"]; ?>').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>0.png';
			<?php
					}
				}
			?>
			document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[iCount] + '1.png';
			document.getElementById('carbrandcost').innerHTML = 'Rs. ' + hPkgRate[iCount] + '/Hr. <span style=\"font-size:12px;font-weight:bold;\">(Fuel Included)</span>';
			document.getElementById('carbrandline').innerHTML = hCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs " + hPkgRate[iCount] + " / Hr.<span style=\"font-size:12px;font-weight:bold;\">(Fuel Included)</span></span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs " + hPkgRate[iCount] + "</li><li>VAT (@12.5%): Rs " + parseInt(eval((hPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: Rs " + parseInt(eval(hPkgRate[iCount] + (hPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): Rs " + hSecDeposit[iCount] + " (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else {
			<?php
				for($pk = 0; $pk < count($pkgsDaily); $pk++){
			?>
				document.getElementById('cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>0.png';
			<?php
				}
			?>
			document.getElementById('cars-' + iPkgID).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[iCount] + '1.png';
			document.getElementById('carbrandcost').innerHTML = 'Rs. ' + dPkgRate[iCount] + '/Day</span>';
			document.getElementById('carbrandline').innerHTML = dCarDec[iCount];
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs " + dPkgRate[iCount] + " / Day</span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs " + dPkgRate[iCount] + "</li><li>VAT (@12.5%): Rs " + parseInt(eval((dPkgRate[iCount] * 12.5) / 100)) + "</li><li>Total Fare: Rs " + parseInt(eval(dPkgRate[iCount] + (dPkgRate[iCount] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): Rs " + hSecDeposit[iCount] + " (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
	};
	<?php
		if(count($pkgs) > 0){
			for($pk = 0; $pk < count($pkgs); $pk++){
	?>
			hPkgID.push('<?php echo $pkgs[$pk]["PkgID"]; ?>');
			hCarImg.push('<?php echo trim(strtolower($pkgs[$pk]["PkgName"])); ?>');
			hCarName.push('<?php echo $pkgs[$pk]["PkgName"]; ?>');
			hCarDec.push('<?php echo $pkgs[$pk]["PkgDesc"]; ?>');
			hPkgRate.push(<?php echo intval($pkgs[$pk]["PkgRate"]); ?>);
			hSecDeposit.push('<?php echo intval($pkgs[$pk]["DepositAmt"]); ?>');
	<?php
			}
		} else {
			$isHrAvail = false;
	?>
		mode = "Daily";
	<?php
		}
		if(count($pkgsDaily) > 0) {
			for($pk = 0; $pk < count($pkgsDaily); $pk++){
	?>
			dPkgID.push('<?php echo $pkgsDaily[$pk]["PkgID"]; ?>');
			dCarImg.push('<?php echo trim(strtolower($pkgsDaily[$pk]["PkgName"])); ?>');
			dCarName.push('<?php echo $pkgsDaily[$pk]["PkgName"]; ?>');
			dCarDec.push('<?php echo $pkgsDaily[$pk]["PkgDesc"]; ?>');
			dPkgRate.push(<?php echo intval($pkgsDaily[$pk]["PkgRate"]); ?>);
			dSecDeposit.push('<?php echo intval($pkgsDaily[$pk]["DepositAmt"]); ?>');
	<?php
			}
		}
	?>
	_changeFrame = function(id)
	{
		var idx = 0;
		iPkgID = id;
		if (mode == "Hourly") {
			for(p = 0; p < hPkgID.length; p++){
				if (hPkgID[p] == id) {
					idx = p;
					iPkgName = hCarName[p];
					break;
				}
			}
		} else {
			for(p = 0; p < dPkgID.length; p++){
				if (dPkgID[p] == id) {
					idx = p;
					iPkgName = dCarName[p];
					break;
				}
			}
		}
		if (mode == "Hourly") {
			<?php
				for($pk = 0; $pk < count($pkgs); $pk++){
			?>
				document.getElementById('cars-<?php echo $pkgs[$pk]["PkgID"]; ?>').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>0.png';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[idx] + '.png';
			document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + hCarImg[idx] + '1.png';
			document.getElementById('carbrandname').innerHTML = hCarName[idx];
			document.getElementById('carbrandcost').innerHTML = 'Rs. ' + hPkgRate[idx] + '/Hr. <span style="font-size:12px;font-weight:bold;">(Fuel Included)</span>';
			document.getElementById('carbrandline').innerHTML = hCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs " + hPkgRate[idx] + " / Hr.<span style=\"font-size:12px;font-weight:bold;\">(Fuel Included)</span></span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs " + hPkgRate[idx] + "</li><li>VAT (@12.5%): Rs " + parseInt(eval((hPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: Rs " + parseInt(eval(hPkgRate[idx] + (hPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): Rs " + hSecDeposit[idx] + " (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		} else {
			<?php
				for($pk = 0; $pk < count($pkgsDaily); $pk++){
			?>
				document.getElementById('cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>0.png';
			<?php
				}
			?>
			document.getElementById('carbrandimage').src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '.png';
			document.getElementById('cars-' + id).src = '<?php echo corWebRoot; ?>/myles-campaign/car-images/' + dCarImg[idx] + '1.png';
			document.getElementById('carbrandname').innerHTML = dCarName[idx];
			document.getElementById('carbrandcost').innerHTML = 'Rs. ' + dPkgRate[idx] + '/Day';
			document.getElementById('carbrandline').innerHTML = dCarDec[idx];
			document.getElementById('popupFDetails').innerHTML = "<div class=\"pophold\"><span class=\"headliners\">Rate (Delhi)</span><br /><span class=\"carrate\">Rs " + dPkgRate[idx] + " / Hr.<span style=\"font-size:12px;font-weight:bold;\">(Fuel Included)</span></span><br /><span class=\"headliners\">Fare Details</span><br /><ul><li>Minimum Billing: Rs " + dPkgRate[idx] + "</li><li>VAT (@12.5%): Rs " + parseInt(eval((dPkgRate[idx] * 12.5) / 100)) + "</li><li>Total Fare: Rs " + parseInt(eval(dPkgRate[idx] + (dPkgRate[idx] * 12.5) / 100)) + "</li><li>Refundable Security Deposit (Pre Auth from card): Rs " + dSecDeposit[idx] + " (Mastercard and Visa only)</li></ul><span class=\"headliners\">Mandatory Documents</span><br /><ul><li>Passport/ Voter ID Card</li><li>Driving License</li><li>Credit Card</li></ul></div>";
		}
		
	}
</script>

<script type="text/javascript">
_frmClearPP = function(){

    document.getElementById("txtNamePP").value = "Name";
    document.getElementById("txtContactNoPP").value = "Mobile No";
    //document.getElementById("txtEmailPP").value = "Emal Id";
	document.getElementById("txtQuery").value = "Query";
};
_frmDisablePP = function(t){
    document.getElementById("txtNamePP").disabled = t;
    document.getElementById("txtContactNoPP").disabled = t;
    //document.getElementById("txtEmailPP").disabled = t;
	document.getElementById("txtQuery").value = t;
};
function _validateMyles()
{
    var chk;
    chk = true;
    chk = isFilledText(document.getElementById("txtNamePP"), "Name", "Please fill in your full name.","ppresponse");
    if(chk)
	chk = isFilledText(document.getElementById("txtContactNoPP"), "Mobile No", "Please fill in your mobile number.","ppresponse");
//    if(chk)
//	chk = isFilledText(document.getElementById("txtEmailPP"), "Email Id", "Please fill in your email id.","ppresponse");
//    if(chk)
//	chk = isEmailAddr(document.getElementById("txtEmailPP"), "Please fill in a valid email id.","ppresponse");
    if(chk)
      chk = isFilledText(document.getElementById("txtQuery"), "Query", "Please fill in your query.","ppresponse");
    if (chk) {
	var ajx;
	var strParam;
	if (window.XMLHttpRequest)
	    ajx = new XMLHttpRequest;
	else if (window.ActiveXObject)
	    ajx = new ActiveXObject("Microsoft.XMLHTTP");
	 strParam = "txtname=" + document.getElementById("txtNamePP").value + "&email=NA&monumber=" + document.getElementById("txtContactNoPP").value + "&query=" + document.getElementById("txtQuery").value + "&curr_url=" + escape(document.getElementById("hdCURL").value);
	 
	ajx.onreadystatechange = function()
	{
	    if (ajx.readyState < 4){
	    
		document.getElementById('ppresponse').innerHTML = "<b style='color:#DB4A28;'>Please Wait...</b>";
	    }
	    else if (ajx.readyState == 4) {
		var r = "";
		if(ajx.status == 200){
		    var url = webRoot + "/myles-thanks.php?op=" + document.getElementById("txtContactNoPP").value;
		    _frmDisablePP(false);
		    _frmClearPP();
		    window.location = url;
		    //document.getElementById("ppresponse").innerHTML = "<b style='color:#DB4A28;'>Thank you for your valued enquiry, will get back to you shortly</b>";
		}
	    }
	};
	ajx.open("POST", webRoot + "/myles-enquiry.php", true);
	ajx.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");        
	ajx.send(strParam);
    }   
};

wx = 0;
function _moveScreen() {
  if(wx <= 600){
    wx += 3;
    window.scrollTo(0, wx);
    setTimeout("_moveScreen()", wx * 0.00012);
  }
}
</script>
<script type="text/javascript">

_changeSFPkgType = function(t)
{
    document.getElementById('hourly').className = 'unsel';
    document.getElementById('daily').className = 'unsel';
    document.getElementById('hourlyBlock').style.display = 'none';
    document.getElementById('dailyBlock').style.display = 'none';
    if (t == "hourly")
    {
        document.getElementById('hourly').className = 'sel';
        document.getElementById('chkPkgType').value = 'Hourly';
        document.getElementById('hourlyBlock').style.display = 'block';
    }
    if (t == "daily")
    {
        document.getElementById('daily').className = 'sel';
        document.getElementById('chkPkgType').value = 'Daily';
        document.getElementById('dailyBlock').style.display = 'block';
    }
}
</script>

<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/newjs/jquery.datetimepicker.css"/>

	<style>
	  #registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.png") no-repeat scroll 0 0 transparent !important;}
	  #registration{border: 10px solid #db4626 !important;}
#mycarousel
{
	overflow:hidden !important;
}
#mycarousel li{
	list-style: none outside none;
	width:105px;
	margin-right:10px;
}
.jcarousel-skin-tango .jcarousel-container {
	float:left;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-direction-rtl {
	direction: rtl;
}

.jcarousel-skin-tango .jcarousel-container-horizontal {
    width:  1090px;
    padding: 0px 20px;
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-clip {
    overflow: hidden;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width:  1050px;
	margin-left:17px;	
	overflow:hidden;
}

.jcarousel-skin-tango .jcarousel-item {
    width: 105px;
}

.jcarousel-skin-tango .jcarousel-item-horizontal {
	margin-left: 0;
    /*margin-right: 10px;*/
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-item-horizontal {
	margin-left: 13px;
    margin-right: 0;
}

.jcarousel-skin-tango .jcarousel-item-placeholder {
    background: #fff;
    color: #000;
}

/**
 *  Horizontal Buttons
 */
.jcarousel-skin-tango .jcarousel-next-horizontal {
    position: absolute;
    top: 0px;
    right: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png) no-repeat;
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-next-horizontal {
    left: 8px;
    right: auto;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
	background-position:-20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:focus {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-horizontal:active {
    background-position: -20px 0px;
}

.jcarousel-skin-tango .jcarousel-next-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:active {
    cursor: default;
    background-position: -40px 0;
	background-color:transparent;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal {
    position: absolute;
    top: 0px;
    left: 5px;
    width: 20px;
    height: 79px;
    cursor: pointer;
    background: transparent url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-left.png) no-repeat -40px 0;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-prev-horizontal {
    left: auto;
    right: 15px;
    background-image: url(<?php echo corWebRoot; ?>/images/Non-3D-Arrow-buttons-right.png);
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:focus {
    background-position: -0px 0;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal:active {
    background-position: -40px 0;
}

.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:focus,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:active {
    cursor: default;
    background-position: 0px 0;
	width:20px;
}
.body
{
	background-color: #FFF !important;
}

#slider img {
    display: none;
    height: 250px !important;
    left: 0;
    position: absolute;
    top: 0;
    width: 950px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css?v=<?php echo mktime(); ?>" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style-new.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style-new.css?v=<?php echo mktime(); ?>" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/newjs/jquery.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/newjs/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />

<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
</head>
<body class="body">
<div class="d100p">
<?php include_once("./includes/header-with-service.php"); ?>

<div class="d100p">
<div id="main-container">
    <!--starts aamir-->
		<div class="main">
        <div class="lfts" id="lfts">
        <div class="clr"></div>
        <div id="TripleTabs" style="min-height:207px;border:none">
		<!-- class="tber" -->
		<div id="selfdriveDiv"  style="display:block;">
			<!-- new starts-->
			<form id="form3" name="form3" action="<?php echo corWebRoot; ?>/search-myles.php" method="post">
			<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
			<input type="hidden" name="hdOriginName" id="hdOriginNameSF" />
			<input type="hidden" name="hdOriginID" id="hdOriginIDSF" />
			<input type="hidden" name="hdDestinationName" id="hdDestinationNameSF" />
			<input type="hidden" name="hdDestinationID" id="hdDestinationIDSF" />
			<input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" />
			<input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo date('h:i A');?>" />
			<input type="hidden" name="userTime" id="userTimeSF" value="" />
			<input type="hidden" name="onlyE2C" id="onlyE2C" value="0" />
			<input type="hidden" name="chkPkgType" id="chkPkgType" value="Daily" />
                        <div class="derive_service fma">
		        <div id="typewrap">
			  <span id="daily" class="sel" onclick="javascript: _changeSFPkgType(this.id)">Book Daily Rental</span>
			  <span id="hourly" class="unsel" onclick="javascript: _changeSFPkgType(this.id)">Book Hourly Rental</span>
			  <!--<span style="float:left;display:block;width:100%;"><input type="radio" name="chkPkgType" id="chkPkgType1" value="Hourly" onclick="javascript: _setSFPkgType('Hourly');" />&nbsp;<span style='top: -3px;position:relative;'>Hourly Rental</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="chkPkgType" id="chkPkgType2" value="Daily" checked="checked" onclick="javascript: _setSFPkgType('Daily');" />&nbsp;<span style='top: -3px;position:relative;'>Daily Rental</span>
			  </span>-->
		        </div>
		        <div class="oneliner">
			  <span style="float:left;border: 1px solid #a2a2a2;">
			      <!--<input type="text" id="" class="from" name="" placeholder="From" />-->
			      <select name="ddlOrigin" id="ddlOriginSF" class="fromdd" onchange="javascript: _setOrigin(this.id, 's');">
			      <option>Select City</option>
	<?php
			$selCityId = 2;
			$selCityName = "Delhi";
			$sdOrigin = array(1, 2, 3, 7, 4, 5, 6, 8, 9, 10, 11, 39, 41, 43, 49, 69);
			$org[] = array("CityID" => 69, "CityName" => "Goa");
			$org[] = array("CityID" => 9, "CityName" => "Chandigarh");
			$org[] = array("CityID" => 43, "CityName" => "Amritsar");
			$org[] = array("CityID" => 39, "CityName" => "Manesar");
			$org[] = array("CityID" => 41, "CityName" => "Bhubaneswar");
			$arCityName = array();
			foreach($org as $k => $v){
				$arCityName[$k] = $v['CityName'];
			}
			array_multisort($arCityName, SORT_ASC, $org);
			
			for($i = 0; $i < count($org); $i++)
			{
				if(in_array($org[$i]["CityID"], $sdOrigin)){
					if($org[$i]["CityID"] == $cCityID){
						$selCityId = $org[$i]["CityID"];
						$selCityName = $org[$i]["CityName"];
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
					<script type="text/javascript" language="javascript">
						document.getElementById('hdOriginIDSF').value = '<?php echo $org[$i]["CityID"]; ?>';
						document.getElementById('hdOriginNameSF').value = '<?php echo $org[$i]["CityName"]; ?>';
					</script>
	<?php
					}
					else {
	?>
					<option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
	<?php
					}
				}
			}
	?>
					</select>
			  </span>
			  <span style="float:left;border: 1px solid #a2a2a2;">
			      <input type="text" class="from picktime" id="inputFieldSF1" onchange="javascript: _setDropDateMyles('inputFieldSF1', 'inputFieldSF2');" placeholder="Pick Up Date and Time" readonly="readonly" />
			      <!--<input type="text" id="picktime" name="picktime" class="from picktime" placeholder="Pick Up Date and Time" />-->
			  </span>
			  <span style="float:left;border: 1px solid #a2a2a2;" id="dailyBlock">
			      <input type="text" class="from picktime" id="inputFieldSF2" placeholder="Drop Off Date and Time" readonly="readonly" onchange="javascript: _setDropOffDate(this.id);" />
			      <!--<input type="text" value="" class="from picktime" id="inputFieldSF2" placeholder="Drop Off Date and Time"/>-->
			  </span>
			  <span style="float:left;display: none;border: 1px solid #a2a2a2;" id="hourlyBlock">
			      <!--<input type="text" id="" name="" class="from" placeholder="No of Hours" />-->
			      <select class="fromdd2" name="nHours" id="nHours" onchange="javascript: _setHours(this.value, 'selHrs', 'seltimeH');">
			      <?php for($i = 2; $i <= 23; $i++){ ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?> HRS</option> 
			      <?php } ?>
			      </select>
			  </span>
			  <span style="float:left;">
			      <input id="btnMakePaymentSF" class="btn" type="button" onclick="javascript: return _validateSF();" value="NEXT" name="btnMakePayment">
			  </span>
		        </div>
		        <input type="hidden" name="tHourP" id="tHourP" value="" />
		        <input type="hidden" name="tMinP" id="tMinP" value="" />
		        <input type="hidden" name="tHourD" id="tHourD" value="" />
		        <input type="hidden" name="tMinD" id="tMinD" value="" />
		        <input type="hidden" name="pickdate" id="pickdate" value="" />
		        <input type="hidden" name="dropdate" id="dropdate" value="" />
		        </form>
		        <!-- new ends -->
		</div>
		<div class="banner_animation">
			<!--<div id="slider" class="nivoSlider">
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide0.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide1.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide2.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide3.jpg" alt="" title="" />
			</div>-->
			<div id="slides">
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide0.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide1.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide2.jpg" alt="" title="" />
				<img src="<?php echo corWebRoot; ?>/myles-campaign/images/140618_myles_16cities_DEL_slide3.jpg" alt="" title="" />
				<a href="#" class="slidesjs-previous slidesjs-navigation"><i class="icon-chevron-left icon-large"></i></a>
				<a href="#" class="slidesjs-next slidesjs-navigation"><i class="icon-chevron-right icon-large"></i></a>
			</div>
			
			
			
		</div>
		    <div class="car-container" id="range-of-cars">
		<div class="car-heading" onclick="javascript: _moveScreen();">
			<div class="text">Pick from a wide range of cars</div>
			  <a class="know-more" href="javascript: void(0)" id="bkn">Available in 16 cities</a>
		  </div>
            <div class="show-car">
            	<div class="car-img">
		    <?php
			if($isHrAvail){
		    ?>
			<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[0]["PkgName"]); ?>.png" id="carbrandimage" alt="" title=""/>
		    <?php
			} else {
		    ?>
			<img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[0]["PkgName"]); ?>.png" id="carbrandimage" alt="" title=""/>
		    <?php
			}
		    ?>			
		</div>
		<div class="holddata">
                <div class="holdall" style="width:600px !important;">
			<?php
			    if($isHrAvail){
			?>
				<span id="carbrandname"><?php echo $pkgs[0]["PkgName"]; ?></span>
				<span id="carbrandcost" style="width:400px !important;">Rs. <?php echo intval($pkgs[0]["PkgRate"]); ?>/Hr. <span style="font-size:12px;font-weight:bold;">(Fuel Included)</span></span>
				<span id="carbrandline"><?php echo $pkgs[0]["PkgDesc"]; ?></span>
				<span class="smdetails">
				<div id="item_1" class="item">
				Details +
				<div class="tooltip_description" style="display:none" id="popupFDetails">
					<div class="pophold">
						<span class="headliners">Rate (<?php echo $cCity; ?>)</span><br />
						<span class="carrate">Rs <?php echo $pkgs[0]["PkgRate"]; ?> / Hr. <span style="font-size:12px;">(Fuel Included)</span></span><br />
						<span class="headliners">Fare Details</span><br />
						<ul>
							<li>Minimum Billing: Rs <?php echo intval($pkgs[0]["PkgRate"]); ?></li>
							<li>VAT (@12.5%): Rs <?php echo intval(($pkgs[0]["PkgRate"] * 12.5) / 100); ?></li>
							<li>Total Fare: Rs <?php echo ($pkgs[0]["PkgRate"] + intval(($pkgs[0]["PkgRate"] * 12.5) / 100)); ?></li>
							<li>Refundable Security Deposit (Pre Auth from card): Rs <?php echo $pkgs[0]["DepositAmt"]; ?> (Mastercard and Visa only)</li>
						</ul>
						<span class="headliners">Mandatory Documents</span><br />
						<ul>
							<li>Passport/ Voter ID Card</li>
							<li>Driving License</li>
							<li>Credit Card</li>
						</ul>
					    </div>
				    </div>
				  </div>
				</span>
			<?php
			    } else {
			?>
				<span id="carbrandname"><?php echo $pkgsDaily[0]["PkgName"]; ?></span>
				<span id="carbrandcost" style="width:400px !important;">Rs. <?php echo intval($pkgsDaily[0]["PkgRate"]); ?>/Day</span><br />
				<span id="carbrandline"><?php echo $pkgsDaily[0]["PkgDesc"]; ?></span>
				<span class="smdetails">
				<div id="item_1" class="item">
				Details +
				<div class="tooltip_description" style="display:none" id="popupFDetails">
					<div class="pophold">
						<span class="headliners">Rate (<?php echo $cCity; ?>)</span><br />
						<span class="carrate">Rs <?php echo $pkgsDaily[0]["PkgRate"]; ?> / Day <span style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><br />
						<span class="headliners">Fare Details</span><br />
						<ul>
							<li>Minimum Billing: Rs <?php echo intval($pkgsDaily[0]["PkgRate"]); ?></li>
							<li>VAT (@12.5%): Rs <?php echo intval(($pkgsDaily[0]["PkgRate"] * 12.5) / 100); ?></li>
							<li>Total Fare: Rs <?php echo ($pkgsDaily[0]["PkgRate"] + intval(($pkgsDaily[0]["PkgRate"] * 12.5) / 100)); ?></li>
							<li>Refundable Security Deposit (Pre Auth from card): Rs <?php echo $pkgsDaily[0]["DepositAmt"]; ?> (Mastercard and Visa only)</li>
						</ul>
						<span class="headliners">Mandatory Documents</span><br />
						<ul>
							<li>Passport/ Voter ID Card</li>
							<li>Driving License</li>
							<li>Credit Card</li>
						</ul>
					    </div>
				    </div>
				  </div>
				</span>
			<?php
			    }
			?>
			    </div>
			<div class="holdallbottom">
			   <div style="float:left;width:140px;">
			    <span class="avin">Select Price:-</span>
		<?php
			if($isHrAvail){
		?>
				<ul>
					<li><a href="javascript: void(0);" id="aHr" onclick="javascript: _setMode('Hourly');" class="selected">Hourly</a></li>
					<li><a href="javascript: void(0);" id="aDl" onclick="javascript: _setMode('Daily');">Daily</a></li>
				</ul>
		<?php
			} else{
		?>
				<ul>
					<li><a href="javascript: void(0);" id="aHr" onclick="javascript: alert('Hourly rental is not available in <?php echo $cCity; ?>.');">Hourly</a></li>
					<li><a href="javascript: void(0);" id="aDl" class="selected">Daily</a></li>
				</ul>
		<?php
			}
		?>
					</div>
				<div style="float:left;width:650px;">
					<span class="avin">Select Your City:-</span>
					<div class="linkbar">
					<?php
						if($cCityID == 2){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Delhi-2/" class="first selected">DELHI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Delhi-2/" class="first">DELHI</a><span class="spanner">|</span>
					<?php
						}
						if($cCityID == 4){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Mumbai-4/" class="selected">MUMBAI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Mumbai-4/">MUMBAI</a><span class="spanner">|</span>
					<?php
						}
						if($cCityID == 7){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Bangalore-7/" class="selected">BANGALORE</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Bangalore-7/">BANGALORE</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 6){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Hyderabad-6/" class="selected">HYDERABAD</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Hyderabad-6/">HYDERABAD</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 69){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Goa-69/" class="selected">GOA</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Goa-69/">GOA</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 10){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Jaipur-10/" class="selected">JAIPUR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Jaipur-10/">JAIPUR</a><span class="spanner">|</span>
					<?php
						}
					?>	
					<!--<div style="clear: both"></div>-->
					
					<?php
						if($cCityID == 8){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Chennai-8/" class="selected">CHENNAI</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Chennai-8/">CHENNAI</a><span class="spanner">|</span>
					<?php
						}
					?>
					<!--<div style="clear: both"></div>-->
					<?php
						if($cCityID == 5){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Pune-5/" class="selected">PUNE</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Pune-5/">PUNE</a><span class="spanner">|</span>
					<?php
						}
					?>	
					<?php
						if($cCityID == 1){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Ahmedabad-1/" class="selected">AHMEDABAD</a>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Ahmedabad-1/">AHMEDABAD</a>
					<?php
						}
					?>	
					<?php
						if($cCityID == 9){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Chandigarh-9/" class="first selected">CHANDIGARH</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Chandigarh-9/" class="first">CHANDIGARH</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 49){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Visakhapatnam-49/" class="selected">VISAKHAPATNAM</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Visakhapatnam-49/">VISAKHAPATNAM</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 43){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Amritsar-43/" class="selected">AMRITSAR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Amritsar-43/">AMRITSAR</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 39){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Manesar-39/" class="selected">MANESAR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Manesar-39/">MANESAR</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 41){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Bhubaneswar-41/" class="selected">BHUBANESWAR</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Bhubaneswar-41/">BHUBANESWAR</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 11){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Noida-11/" class="selected">NOIDA</a><span class="spanner">|</span>
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Noida-11/">NOIDA</a><span class="spanner">|</span>
					<?php
						}
					?>
					<?php
						if($cCityID == 3){
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Gurgaon-3/" class="selected">GURGAON</a> 
					<?php	
						}else{
					?>
						<a href="<?php echo corWebRoot; ?>/car-rental-Gurgaon-3/">GURGAON</a> 
					<?php
						}
					?>
					</div>
					<a href="javascript:void(0);" class="bkn" onclick="javascript: window.scrollTo(0,0);_setTab('selfdrive');"><img src="<?php echo corWebRoot; ?>/myles-campaign/images/booknow.png" alt="Book Now" title="Book Now"/></a>
					</div>
					<div  class="clearfix"></div>
               </div>
	    </div>
                <div  class="clearfix"></div>
				<div style="float:left;width:1130px;margin:25px 0px 20px 0px">
		<?php
			if(count($pkgs) > 0){
		?>
				<ul id="mycarousel" class="jcarousel-skin-tango">
				<?php
					for($pk = 0; $pk < count($pkgs); $pk++){
						if($pk == 0){
				?>
						<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>1.png" id="cars-<?php echo $pkgs[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgs[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgs[$pk]["PkgID"]; ?>)"/></li>
				<?php
						} else {
				?>
						<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>0.png" id="cars-<?php echo $pkgs[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgs[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgs[$pk]["PkgID"]; ?>)"/></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
			if(count($pkgsDaily) > 0) {
			    if($isHrAvail){
		?>
				<ul id="mycarousel-1" class="jcarousel-skin-tango" style="display: none;">
		<?php
			    } else {
		?>
				<ul id="mycarousel-1" class="jcarousel-skin-tango">
		<?php
			    }
		
					for($pk = 0; $pk < count($pkgsDaily); $pk++){
						if($pk == 0){
				?>
						<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>1.png" id="cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>)"/></li>
				<?php
						} else {
				?>
						<li><img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgsDaily[$pk]["PkgName"]); ?>0.png" id="cars-<?php echo $pkgsDaily[$pk]["PkgID"]; ?>" onclick="javascript: _setCount(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>);_changeFrame(<?php echo $pkgsDaily[$pk]["PkgID"]; ?>)"/></li>
				<?php
						}
					}
				?>
				</ul>
		<?php
			}
		?>
				</div>
            </div>
           
            
	</div>
		</div><!--Selfdrive-->
	    <div id="othSrv" style="float:left;">
		
		<div id="outstationDiv" class="tber">
		    <iframe src="<?php echo corWebRoot; ?>/outstation.php" width="1128px" height="477px" frameborder="0" id="iFrameOS" name="iFrameOS"></iframe>
		</div>
		<div id="localDiv" class="tber">
		    <iframe src="<?php echo corWebRoot; ?>/local.php" width="1128px" height="477px" frameborder="0"></iframe>
		</div>
		<div id="internationalDiv" class="tber">
			<iframe src="http://international.carzonrent.com/?module=2808&language=en&countrycode=IN&currency=INR" width="1128px" height="475px" frameborder="0"></iframe>
		</div>
		</div>
	
        <!--TripleTabs Ends Here-->
        </div>
	</div>
		</div>
	<script type="text/javascript">
	$('#inputFieldSF1').datetimepicker({
	format:'d M, Y H:i',
	minDate:'<?php echo date('d M, Y'); ?>',
	step:30,
	roundTime:'ceil'
	});
	$('#inputFieldSF2').datetimepicker({
	format:'d M, Y H:i',
	minDate:'<?php echo date('d M, Y'); ?>',
	step:30
	});
	
	</script>
    <!--ends aamir-->
    
	
<div class="clearfix"></div>
<?php include_once("./includes/middle.php"); ?>

<?php
if(isset($_REQUEST['q']) != ""){

$weburl=$_REQUEST['q'];
$selectQuery="select * from cor_static_page where web_url='".$weburl."'";
$queryData=mysql_query($selectQuery);
$recordVal=mysql_fetch_array($queryData);
$descritionData=$recordVal['page_description'];
	
	
?>
	<div class="about-city" style="float:left;width:100%; padding:10px;">
		<div class="main">
			<div class="city-desc">			
				<!--<h2>About <?php //echo $cCity; ?></h2><br />-->
			<?php if($descritionData != "") { ?>
				<?php echo $descritionData ?>
			<?php } ?>
			</div>
		</div>
	</div>
<?php
}
?>
<?php
if(isset($_GET["c"]) && isset($_GET["c"])){
if(trim($cCity) != ""){
	$xmlDoc = new DOMDocument();
	$xmlDoc->load("./xml/city-desc/" . strtolower($cCity) . ".xml");
	$itemArray = $xmlDoc->getElementsByTagName("city");
	$optText = "";
	foreach($itemArray as $item){
		$txt = $item->getElementsByTagName("citydesc");
		$optText = $txt->item(0)->nodeValue;
	}
	$xmlDoc = null;
?>





	<div class="about-city" style="float:left;width:100%;margin-top:50px;">
		<div class="main">
			<div class="city-desc">			
				<!--<h2>About <?php //echo $cCity; ?></h2><br />-->
			<?php if($optText != "") { ?>
				<?php echo $optText ?>
			<?php } ?>
			</div>
		</div>
	</div>
<?php
}
}
?>
</div>
</div> <!-- d100p ends -->
<?php include_once("./includes/footer.php"); ?>



	<!-- Slider image start -->
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo corWebRoot; ?>/slider/jquery.slides.min.js"></script>
	<script>
	var j = jQuery.noConflict();
	$(function() {
	j('#slides').slidesjs({
	width:940,
	play: {
	auto: true,
	interval: 4000
	}
	});
	});
	</script>
	<!-- Slider image end -->

   
	<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/scripts/jquery-1.4.3.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/jquery.nivo.slider.pack.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
	    $('#slider').nivoSlider();
	});
	</script>-->
      
	    <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.jcarousel.min.js"></script>
	    <script type="text/javascript">
		var jQuery = jQuery.noConflict();
		jQuery(document).ready(function() {
		<?php
		    if($isHrAvail){
		?>
			jQuery('#mycarousel').jcarousel({
			    start: 1
			});
		<?php
		    } else {
		?>
			jQuery('#mycarousel-1').jcarousel({
			    start: 1
			});
		<?php
		    }
		?>		
		    var ddURL = document.URL;
		    if(ddURL.indexOf('#') > -1){
			var dURLPart = ddURL.split('#');
			var tab = "outstation";
			switch(dURLPart[1]){
			    case "o":
			    case "outstation":
			    case "multicity":
				tab = "outstation";
			    break;
			    case "i":
			    case "international":
				    tab = "international";
			    break;
			    case "l":
			    case "local":
				    tab = "local";
			    break;
			    case "s":
			    case "selfdrive":
			    case "range-of-cars":
				    tab = "selfdrive";
			    break;
			    default:
				    tab = "outstation";
			    break;
			}
			_setTab(tab);
		    }
		});
	    </script> 
      <?php
	for($pk = 0; $pk < count($pkgs); $pk++){
      ?>
	  <img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>.png" width="1px" class="abs" />
	  <img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>0.png" width="1px" class="abs" />
	  <img src="<?php echo corWebRoot; ?>/myles-campaign/car-images/<?php echo strtolower($pkgs[$pk]["PkgName"]); ?>1.png" width="1px" class="abs" />
      <?php
	}
      ?>
      <a id="utility" class="scrollup" title="Back to top" href="javascript:void(0);"></a>
      <script type="text/javascript">
	
	$(document).ready(function(){
	$('.scrollup').fadeOut();
	$(window).scroll(function(){
	if ($(this).scrollTop() > 400) {
	$('.scrollup').fadeIn();
	} else {
	$('.scrollup').fadeOut();
	}
	});
	$('.scrollup').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 1000);
	return false;
	});
	});
      </script>
</body>
</html>

<?php
	error_reporting(0);
	if(isset($_GET["resp"])) {
		if($_GET["resp"] == "e"){
			session_start();
		}
		if($_GET["resp"] == "f"){
			session_start();
		}
	}
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	$rurl = corWebRoot;
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers book a cab or rent a car for airport transfer car rentals, online economy car rentals/car hiring and hertz car rental solutions." />
<meta name="keywords" content="car rental, book a cab, rent a car, car hire, car rentals, car rental india, car rental company india, budget/executive car rentals, economy car rental services, car hire services india, airport transfer car rental services, luxury cars on rent" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<script language="javascript" type="text/javascript">
var id;
$(document).ready(function(eOuter) {
    $('input').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });
	$('#txtPassword').bind('keypress', function(eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
			
			_checkRegister();
		}
	});
	
});
</script>
</head>
<body>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
	<div class="yellwborder" style="margin-top: 20px;">
		<div class="yellodivtxt">
			<h2>Register</h2>
		</div>
	</div>
<div class="main">
<br /><br />
<div class="businessEnquiry">
	
	
<form action="./create-registration.php" method="post" id="formRegister" name="formRegister">
<table width="75%" cellpadding="0" cellspacing="10" border="0" align="left" class="border-radius">
	<tr>
	<td colspan="2">
	<?php if(isset($_GET["resp"])) {
		  if($_GET["resp"] == "e"){
			  session_start();
	?>
	<p style="padding-left:3.5%; color:red">Mobile number already exists with us. <a href="forgot-password.php">Forgot password?</a></p>
	<?php
		}elseif($_GET["resp"] == "f"){ ?>
	<p style="padding-left:3.5%; color:red">Please enter a valid referral code or left it blank.</p>		
			
	<?php	}
	}
	?>
	</td>
	</tr>
	
	<tr>
	<td valign="middle" align="left" colspan="2">	
	<span class="regi_l"><a href="<?php echo $rurl; ?>"><img style="height:50px;" src="<?php echo $rurl; ?>/images/512_512_logo.png"></a>
	</span>
	<span class="regi_r">
	<span class="heading">Register online and start now.</span>
		</span>
	</td>
	</tr>
            
    
        <!--<tr>
		<td align="right" valign="middle"><label>Tour Type</label></td>
                <td align="left" valign="top" >   
                    <select name="tourtype" class="service_type">
                        <option value="1">Selfdrive</option>
                        <option value="2">Outstation/Local</option>
                    </select>
                </td>	     
	</tr>-->
    
    
	<tr>
		<td align="right" valign="middle"><label>Name</label></td>
		<td align="left" valign="top" ><input type="text" name="txtName" id="txtName" tabindex="1" onKeyPress="javascript: return _allowAlpha(event);" value="<?php if($_SESSION["fname"] != "") { echo $_SESSION["fname"] . " " . $_SESSION["lname"]; } ?>"  /></td>	     
	</tr>
	<tr>
		<td align="right" valign="middle"><label>Mobile Number</label></td>
		<td align="left" valign="top" ><input type="text" name="txtMobile" id="txtMobile" tabindex="2" maxlength="10" onKeyPress="javascript: return _allowNumeric(event);" value="<?php echo $_SESSION["PhoneNumber"]; ?>" /></td>    
	</tr>
	<tr>
		<td align="right" valign="middle"><label>Email ID</label></td>
		<td align="left" valign="top" ><input type="text" name="txtEmail" id="txtEmail" tabindex="3" value="<?php echo $_SESSION["emailId"]; ?>" /></td>
	</tr>
	<tr>
		<td align="right" valign="middle"><label>Referral Code(Optional)</label></td>
		<td align="left" valign="top" ><input type="text" name="txtRefferralCode" id="txtRefferralCode" tabindex="4" /></td>
	</tr>
	<tr>
		<td align="right" valign="middle"></td>
		<td align="left" valign="top" ><div class="submit"><a href="javascript:void(0);" onclick="javascript: _checkRegister();" tabindex="5"></a></div></td>	     
	</tr>
</table>
</form></div>

  <div class="clr"></div>
</div>
  <p>&nbsp;</p>


</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

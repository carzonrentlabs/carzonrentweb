<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Carzonrent Vehicle Guide - Premium, Compact, Intermediate</title>
<meta name="description" content="Carzonrent provides modern fleet range of cars including premium, superior, intermediate and economy. Read here vehicle guide at Carzonrent." />
<meta name="keywords" content="Intermediate Car Hire, Premium Car rental in india, vehicle guide" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
<style>
	.aboutusdv{
		float: left;
		width: 1000px;
		background: #fff;
	}
</style>
</head>
<body>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<div class="tbul" style="float: left;">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">Economy</a></li>
    <li><a href="#" rel="country2">Compact </a></li>
    <li><a href="#" rel="country3">Intermediate </a></li>
    <li><a href="#" rel="country4">Van</a></li>
	<li><a href="#" rel="country5">Standard</a></li>
    <li><a href="#" rel="country6">Superior</a></li>
    <li><a href="#" rel="country7">Premium</a></li>
     <li><a href="#" rel="country8">Luxury</a></li>
	<li><a href="#" rel="country9">4 Wheel Drive </a></li>
  </ul>
</div>
<div class="lfcnt">

<div id="country1" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Economy</h2>
<p>Economy car rentals in India come with the distinct Carzonrent advantage. You get comfort, and safety in this category which comprises of car models like Tata Indica, Wagon R, Hyundai Santro, Maruti Zen & Maruti Alto. These are good for both short as well as long journeys and ideally suited for a small family.
</p>
<p><span class="txt14px">Alto VX</span><br />
<strong>Cubic Capacity (cc) </strong>: 1061<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 35</p>

<p><span class="txt14px">Hyundai Santro</span><br />
<strong>Cubic Capacity (cc) :</strong> 1086<br />
<strong>Seats :</strong> 5<br />
<strong>Fuel Tank Capacity (lit) :</strong> 35</p>

<p><span class="txt14px">Maruti Zen</span><br />
<strong>Cubic Capacity (cc)</strong> : 993<br />
<strong>Seats :</strong> 5<br />
<strong>Fuel Tank Capacity (lit) </strong>: 35</p>

<p><span class="txt14px">Indica V2</span><br />
<strong>Cubic Capacity (cc)</strong> : 1405<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 37</p>

<p><span class="txt14px">Wagon R</span><br />
<strong>Cubic Capacity (cc)</strong> : 1086<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 35</p>

<p><span class="txt14px">Hyundai Santro</span><br />
<strong>Cubic Capacity (cc)</strong> : 1061<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 35</p>

  <div class="clr"></div>
</div>


<div id="country2" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Compact</h2>
<p>The Compact category has a wider range of options and includes models such as the Hyundai Getz and Maruti Swift. In this category you have available a range of midsized cars such as the Tata Indigo and Suzuki Esteem. </p>
<p><span class="txt14px">Hyundai Getz</span><br />
<strong>Cubic Capacity (cc)</strong> : 1341<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 45</p>

<p><span class="txt14px">Suzuki Esteem</span><br />
<strong>Cubic Capacity (cc)</strong> : 1298<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 40</p>

<p><span class="txt14px">Suzuki Swift</span><br />
<strong>Cubic Capacity (cc)</strong> : 1298<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 43</p>

<p><span class="txt14px">Tata Indigo</span><br />
<strong>Cubic Capacity (cc)</strong> : 1405<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 42 </p>
<div class="clr"></div>
</div>


<div id="country3" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Intermediate </h2>
<p>This category which comprises primarily of car models such as the Ford Ikon, Hyundai Accent and Opel Corsa is the most popular for Middle level Executive Travel both intercity and within the city. Being reasonably priced it also caters to family travel for both self and chauffeur drive. The recently launched Ford Fusion is also now available in this category.  </p>
<p><span class="txt14px">Ford Fusion</span><br />
<strong>Cubic Capacity (cc)</strong> : 1596<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 40</p>

<p><span class="txt14px">Ford Ikon</span><br />
<strong>Cubic Capacity (cc) </strong>: 1597<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 40</p>

<p><span class="txt14px">Hyundai Accent</span><br />
<strong>Cubic Capacity (cc) </strong>: 1493<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit) </strong>: 45</p>

<p><span class="txt14px">Indigo Limited</span><br />
<strong>Cubic Capacity (cc)</strong> : 1405<br />
<strong>Seats</strong> : 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 42<br />

<p><span class="txt14px">Opel Corsa</span><br />
<strong>Cubic Capacity (cc)</strong> : 1598<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit) </strong>: 46 <br />
<div class="clr"></div>
</div>

<div id="country4" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Van </h2>

<p><span class="txt14px">Toyota Innova</span><br />
<strong>Cubic Capacity (cc)</strong> : 2494<br />
<strong>Seats </strong>: 7<br />
<strong>Fuel Tank Capacity (lit)</strong> : 55 </p>
<div class="clr"></div>
</div>

 <div id="country5" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Standard</h2>
  <p>The standard car rental product has on offer a range of vehicles comprising the Maruti Baleno, Mitsubishi Lancer, Chevrolet Optra, Honda City & Opel Astra. Here again this category caters to a large segment of Executive Car Rentals especially in the Senior Management segment. These cars are also very popular for airport transfers for senior managers. </p>

<p><span class="txt14px">Chevrolet Optra</span><br />
<strong>Cubic Capacity (cc)</strong> : 1599<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 60</p>


<p><span class="txt14px">Honda City</span><br />
<strong>Cubic Capacity (cc)</strong> : 1497<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 42</p>


<p><span class="txt14px">Maruti Baleno</span><br />
<strong>Cubic Capacity (cc)</strong> : 1590<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 51</p>


<p><span class="txt14px">Mitsubishi Lancer</span><br />
<strong>Cubic Capacity (cc)</strong> : 1468<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 50</p>

<p><span class="txt14px">Opel Astra</span><br />
<strong>Cubic Capacity (cc)</strong> : 1700<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 52</p>

<div class="clr"></div>
</div> 


<div id="country6" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Superior</h2>
  <p>Our range of Superior Cars include top of the range models like the Toyota Corolla, Skoda Octavia & Hyundai Elantra. Prominent amongst users of this category are very senior executive management and Heads of Companies. This category is preferred by Administration Heads when opting for Executive and Luxury car rental services for their companies. </p>

<p><span class="txt14px">Hyundai Elantra</span><br />
<strong>Cubic Capacity (cc)</strong> : 1975<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 55</p>

<p><span class="txt14px">Skoda Octavia</span><br />
<strong>Cubic Capacity (cc)</strong> : 1984<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 55</p>

<p><span class="txt14px">Toyota Corolla</span><br />
<strong>Cubic Capacity (cc)</strong> : 1794<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 50</p>
<div class="clr"></div>
</div> 


<div id="country7" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Premium</h2>
  <p>The Premium Car rentals segment in essence begins with the range available in this category - Hyundai Sonata, Honda Accord, Toyota Camry and the Mercedes C Class. Foreign dignitaries, visiting heads of companies from overseas are frequent users of this option of Luxury Car Hire. Carzonrent has the single largest range of Mercedes Benz cars in India and this gives it a premium positioning in the Luxury Car Hire segment.</p>

<p><span class="txt14px">Hyundai Sonata</span><br />
<strong>Cubic Capacity (cc)</strong> : 1997<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 65</p>

<p><span class="txt14px">Honda Accord</span><br />
<strong>Cubic Capacity (cc)</strong> : 2354<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 65</p>

<p><span class="txt14px">Mercedes C-Class</span><br />
<strong>Cubic Capacity (cc)</strong> : 1796<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 62</p>

<p><span class="txt14px">Toyota Camry</span><br />
<strong>Cubic Capacity (cc)</strong> : 2362<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 70</p>

<div class="clr"></div>
</div> 





<div id="country8" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>Luxury</h2>
  
<p><span class="txt14px">Mercedes E-Class</span><br />
<strong>Cubic Capacity (cc)</strong> : 2597<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 80</p>
<div class="clr"></div>
</div> 

<div id="country9" class="tabcontent">
  <h1 style="width:790px;">Vehicle Guide</h1>
  <h2>4 Wheel Drive</h2>
  <p>Carzonrent offers Tata Safari to those who prefer a strong vehicle to travel on the roughest of terrains. This vehicle is ideally suited for a group of 4 - 5 people for long journeys. </p>

<p><span class="txt14px">Ford Endeavour</span><br />
<strong>Cubic Capacity (cc)</strong> : 2499<br />
<strong>Seats </strong>: 7<br />
<strong>Fuel Tank Capacity (lit)</strong> : 71</p>

<p><span class="txt14px">Honda CRV</span><br />
<strong>Cubic Capacity (cc)</strong> : 2400<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 58</p>

<p><span class="txt14px">Hyundai Tuscan</span><br />
<strong>Cubic Capacity (cc)</strong> : 2000<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 58</p>

<p><span class="txt14px">Tata Safari</span><br />
<strong>Cubic Capacity (cc)</strong> : 1948<br />
<strong>Seats </strong>: 5<br />
<strong>Fuel Tank Capacity (lit)</strong> : 60 </p>


<div class="clr"></div>
</div> 




</div>
</div>
</div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>
</div>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
</body>
</html>
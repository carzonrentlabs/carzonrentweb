<!DOCTYPE HTML>
<?php
//header('location:http://www.mylescars.com/referralProgram');
 error_reporting(0);
 //error_reporting(E_ALL);
 //ini_set("display_errors", 1);
 include_once('../classes/cor.ws.class.php');
 include_once('../classes/cor.xmlparser.class.php');
 include_once("../includes/cache-func.php");
 include_once('../classes/cor.mysql.class.php');
 ?>
<html>
     <head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 <title>Myles Referral Program</title>
	 <meta name="description" content="Myles Referral Program" />
	 <meta name="keywords" content="Myles, Referral Program, self drive" />	  
	 
	 
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/myles-campaign/style.css">
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js?v=<?php echo mkdir(); ?>"></script>
	  <link rel="stylesheet" type="text/css" href="css/ReferralProgram.css">
	  <link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'>
	  
	  
	  <!-- Tab start -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	  
	  <!-- Tab end -->
	  <!-- OnClick Slider start -->
	  <link href="css/jcarousel.css" rel="stylesheet" type="text/css">
	  <!-- OnClick Slider end -->
	  <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
	  <script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
	  <style>
	  .nivo-controlNav .nivo-control{display:none !important;}
	  </style>
     </head>
     <body>
	  <?php include_once("../includes/header.php"); ?>
	  <div class="clr"> </div>
	  <section class="main">
	       <div class="center">
		   <div class="sliderMain">
		   <!--  -->
			 <div id="slider" class="nivoSlider" style="width:1080px;">
			 <img src="<?php echo corWebRoot; ?>/referralProgram/images/referral_1.jpg" alt="" title="" />
			 </div>
		    </div>
<h1 class="bg">How to participate in referral campaign</h1>
<div class="row">
<div class="w31 mr3 f_l">
<img src="<?php echo corWebRoot; ?>/referralProgram/images/img01.jpg" alt="Refer your friends through Email, Facebook, Twitter, Google+, and LinkedIn" title="Refer your friends through Email, Facebook, Twitter, Google+, and LinkedIn"/>
</div>
<div class="w31 mr3 f_l">
<img src="<?php echo corWebRoot; ?>/referralProgram/images/img02.jpg" alt="Your friend books a Myles car through the referral link" title="Your friend books a Myles car through the referral link"/>
</div>
<div class="w31 f_l">
<img src="<?php echo corWebRoot; ?>/referralProgram/images/img03.jpg" alt="You & your friend get Rs. 500 off on your next booking" title="You & your friend get Rs. 500 off on your next booking" />
</div>
<div class="clr"> </div>
</div>
<div class="f_r">
<div class="termsCod">
More details
</div>
<div class="clr"> </div>
</div>
<div class="t_condition">
<h3>Process</h3>
<ul>
<li>Refer a friend through email/facebook/twitter/linkedin/Google+</li>
<li>Your friend gets a link which opens to the Myles car booking engine</li>
<li>Your friend books a Myles car</li>
<li>Both you & your friend gets a Myles coupon worth Rs.500/- on the next booking</li>
</ul>

<h3>T&C</h3>
<ul>
<li>No two offers can be clubbed together</li>
<li>Offer valid on a booking of Rs.2000 & above</li>
<li>Valid on all cars & cities</li>
<li>Coupon valid till 6 months from the date of issue</li>
<li>Carzonrent has the right to refusal in case of disputes, if any.</li>
<li>Program/offer valid on self-drive service only.</li>
</ul>


</div>

	       </div>	

<script type="text/javascript">
function invitereferrals_2623(){ 
var params = { bid: 2926, cid: 2623 };
invite_referrals.widget.inlineBtn(params);
}
</script>
<button class="refral" type="button" onclick="invitereferrals_2623()">
<div class="img_ref"><img src="http://cdn.invitereferrals.com/images/site/general/ecommerce/reward.png" text-align="center"></div>
<div class="txt_ref">Click here to Refer</div>
</button>
		   
     </section>
	 
	  <div class="clr"> </div>
     <footer>
	  <div class="main">
	  <div class="f_l w50">
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/aboutus.php">About Us</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/services.php">Our Service</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
		    <!--<li><a href="#">Media</a></li>-->
		    <li><a href="http://careers.carzonrent.com/">Careers</a></li>
	       </ul>
	  </div>
	  <div class="f_r w50 t_a_r">
	       <!--<a href="https://www.facebook.com/carzonrent" target="_blank"><img src="images/facebook.png" border="0" class="mr2"/></a>
	       <a href="https://plus.google.com/+carzonrent" target="_blank"><img src="images/google+.png" border="0" class="mr2"/></a>
	       <a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="images/twitter.png" border="0"/></a>-->
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/login.php">Account Summary</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
	       </ul>
	       <div class="copyright">
		    Copyright © 2015 Carzonrent India Pvt Ltd. All Rights Reserved.
	       </div>
	  </div>
	  <div class="clr"> </div>
     </div>
     </footer>
	 
	 <script type='text/javascript' src='js/tooglesilde.js'></script>
     <!-- slider start --> 	
     <script type='text/javascript' src='js/slider/jquery.min.js'></script>
     <script type='text/javascript' src='js/slider/jquery.mobile.customized.min.js'></script>
     <script type='text/javascript' src='js/slider/jquery.easing.1.3.js'></script> 
     
     <!-- slider end --> 
     <!-- date picker start --> 
     <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
     <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
     <script type="text/javascript" src="js/jquery.ui.core.js"></script>
     <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
     <script type="text/javascript">
     var j=jQuery.noConflict();
	     j('#inputFieldSF1').datetimepicker({
	     format:'d M, Y H:i',
	     minDate:'d M, Y',
	     step:30,
	     roundTime:'ceil'
	     });
	     j('#inputFieldSF2').datetimepicker({
	     format:'d M, Y H:i',
	     minDate:'d M, Y',
	     step:30
	     });
     </script>
     <!-- date picker end -->
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/scripts/jquery-1.4.3.min.js"></script>
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/jquery.nivo.slider.pack.js"></script>
     <script type="text/javascript">
     $(window).load(function() {
	 $('#slider').nivoSlider();
     });
     </script>
     <script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.jcarousel.min.js"></script>
     <script type="text/javascript">
	  jQuery(document).ready(function() {
	  
	       jQuery('#mycarousel-1').jcarousel({
		    start: 1
	       });
	  
	
	  });
     </script> 
	 <!-- Google analytics -->
		<?php include_once("../includes/analytic.php"); ?>
		<!--  Google analytics end -->
	  <!-- Google Code for Remarketing tag -->
		<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide:google.com/ads/remarketingsetup -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 957885192;
		var google_conversion_label = "FhZ6COil-gUQiNbgyAM";
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957885192/?value=0&amp;label=FhZ6COil-gUQiNbgyAM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		
		<div id='invtrflfloatbtn'></div>
<script>        
var invite_referrals = window.invite_referrals || {}; (function() {
       invite_referrals.auth = { bid_e : '1B3596545F3AF427193DA8B36E523D4F', bid : '2926', t : '420', orderID : '', email : '' };        
var script = document.createElement('script');script.async = true;
script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
</script>
		
		
     </body>
</html>
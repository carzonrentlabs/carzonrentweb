<div id="cor_header">
<div class="top_head">
<div class="w_78 f_l">
<a href="<?php echo corWebRoot; ?>"><img class="logo" src="<?php echo corWebRoot; ?>/images/COR-logo.gif"></a>
</div>
<div class="w_22 f_l">
<div class="social_icon f_r">
<ul class="w_100">
<li><a href="https://www.facebook.com/carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/FB-icon.gif" border="0" alt="Facebook" title="Facebook"></a></li>
<li><a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="<?php echo corWebRoot; ?>/images/Twitter.gif" border="0" alt="Twitter" title="Twitter"></a></li>
<li><a href="https://www.linkedin.com/company/carzonrent-india-pvt-ltd" target="_blank"><img src="<?php echo corWebRoot; ?>/images/linkdin.png" border="0" alt="Linkedin" title="Linkedin"></a></li>
<li><a href="https://plus.google.com/+carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/googleplus.gif" border="0" alt="Google Plus" title="Google Plus"></a></li>
<li><a href="http://www.youtube.com/carzonrenttv" target="_blank"><img src="<?php echo corWebRoot; ?>/images/You-tube.gif" border="0" alt="YouTube" title="YouTube"></a></li>
<li><a target="_blank" href="<?php echo corWebRoot; ?>"><img border="0" src="<?php echo corWebRoot; ?>/images/blog.gif" alt="Blog" title="blog"></a></li>
</ul>
</div>
<div class="login f_r">
<ul class="w_100">
<li><a href="http://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp" target="_blank">Corporate User</a></li>
<li><a href="<?php echo corWebRoot; ?>/login.php">Login</a></li>
<li><a href="<?php echo corWebRoot; ?>/register.php">Register</a></li>
<li><a href="<?php echo corWebRoot; ?>/faq.php" class="btn-special" target="_blank">Help</a></li>
</ul>
</div>
<div class="top-number bg_color f_r">
<span class="top-number2">Call 24 hours</span>
0888 222 2222
</div>
</div>
<div class="clr"> </div>
</div>
<div class="menu">
<div class="menu_mid">
<div class="w_78 f_l">
<ul>
<li><a class="selected" href="<?php echo corWebRoot; ?>/#o" target="_blank">Outstation</a></li>
<li><a href="<?php echo corWebRoot; ?>#l" target="_blank">Local</a></li>
<li><a href="<?php echo corWebRoot; ?>rdurl.php" target="_blank">EasyCabs</a></li>
<li><a href="<?php echo corWebRoot; ?>" target="_blank">Self-Drive</a></li>
<li><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>
</ul>
</div>
<div class="f_l w_22">
<div class="top-number number_dt f_r">
<span class="top-number2">Call 24 hours</span>
0888 222 2222
</div>
</div>
</div>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
<div id="cor_footer">
<div class="cor_footer">
<div class="w_25 f_l">
<h1>Company</h1>
<ul>
<li><a href="#">About</a></li>
<li><a href="#">Services</a></li>
<li><a href="#">Career</a></li>
<li><a href="#">Blog</a></li>
</ul>
</div>
<div class="w_25 f_l">
<h1>Book a Cab</h1>
<ul>
<li><a href="#">Outstation</a></li>
<li><a href="#">Local</a></li>
<li><a href="#">EasyCabs</a></li>
</ul>
</div>
<div class="w_25 f_l">
<h1>EasyCabs</h1>
<ul>
<li><a href="#">Advertise with Us</a></li>
<li><a href="#">Fleet</a></li>
<li><a href="#">Tariff</a></li>
<li><a href="#">Partners</a></li>
</ul>
</div>
<div class="w_25 f_l">
<h1>Quick Links</h1>
<ul>
<li><a href="#">My Account</a></li>
<li><a href="#">Fleet</a></li>
<li><a href="#">Tariff</a></li>
<li><a href="#">Partners</a></li>
</ul>
</div>
<div class="car_name">
<ul>
<li><a href="#">Car Rental Delhi</a></li>
<li><a href="#">Car Rental Bangalore</a></li>
<li><a href="#">Car Rental Mumbai</a></li>
<li><a href="#">Car Rental Hyderabad</a></li>
<li><a href="#">Car Rental Chennai</a></li>
<li><a href="#">Car Rental Pune</a></li>
<li><a href="#">Car Rental Ahmedabad</a></li>
<li><a href="#">Car Rental Jaipur</a></li>
<li><a href="#">Car Rental Chandigarh</a></li>
<li><a href="#">Car Rental Visakhapatnam</a></li>
<li><a href="#">Car Rental Noida</a></li>
<li><a href="#">Car Rental Gurgaon</a></li>
</ul>
</div>
<div class="copyright">
Copyright @ 2014 <strong>Carzonrent India Pvt Ltd.</strong> All Rights Reserved.
</div>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
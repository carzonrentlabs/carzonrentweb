<?php
include_once('../classes/cor.ws.class.php');
include_once('../classes/cor.xmlparser.class.php');
include_once("../includes/cache-func.php");
?>
<div  class="clearfix"></div>
    <style>
     #map-canvas {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
	  .help{
position:absolute;
top:50px;
left:50%;
z-index:1;
}
.help .text{
position: absolute;
left: -220px;
color: #fff;
font-weight: bold;
font-size: 10px;
padding: 5px 0;
width: 170px;
text-align: center;
background:rgba(0,0,0,0.8);
/*background: url("images/bg.png");*/
}
#HomepageMapZoom .zoom-box {
position: absolute;
right: 10px;
top: 50px;
color: #fff;
font-weight: bold;
font-size: 16px;
width: 30px;
height: 80px;
text-align: center;
font-size: 30px;
line-height: 30px;
}
#HomepageMapZoom .zoom-box .box-active {
width: 30px;
height: 30px;
background: #323a45;
margin-bottom: 5px;
cursor: pointer;
opacity: .9;
}
.rela_new{
position:relative;
top:-333px;
}
    </style>
<link href="<?php echo corWebRoot; ?>/map/map.css" media="screen" rel="stylesheet"/>

<noscript>
<iframe height='0' src='//www.googletagmanager.com/ns.html?id=GTM-T24CHT' style='display:none;visibility:hidden' width='0'></iframe>
</noscript>

	
<script>
var mainbaseurl='<?php echo corWebRoot; ?>';
  
function initialize() 
{
  var mapOptions = {
	mapTypeId: google.maps.MapTypeId.ROADMAP, 
	scrollwheel: true, 
	streetViewControl: false,
	mapTypeControl: false,
	zoomControl: true,
	panControl: true,
	panControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
  // zoom: 8,
    //center: new google.maps.LatLng(21.1289956, 82.7792201)
  }
    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	map.set('zoomControl', false);
	map.set('scrollwheel', false);
	map.set('panControl', false);
	  bounds = new google.maps.LatLngBounds ();
var infowindow82 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'> Newdelhi)</div><div class='p-5'><b>Car rental at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Mahindra Scorpio LX</div></div>",
	maxWidth: 400
});
	
var myLatlng82 = new google.maps.LatLng(28.459497 ,77.026638);
bounds.extend (myLatlng82);
var marker82 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/View_1_1.png",
	position: myLatlng82,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "F.C. Road (Shreenath Plaza)"
});

google.maps.event.addListener(marker82, 'click', function() {
	infowindow82.open(map,marker82);
	pushEvent('Locations Selected', '', 'Home Page Map');
});

//first

var infowindow80 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Nirvana, Gurgaon.</div><div class='p-5'>A-12, Courtyard Market, Nirvana,<br>Sector-50, Gurgaon 122102,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng80 = new google.maps.LatLng(28.423078,77.057886);
bounds.extend (myLatlng80);
var marker80 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng80,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker80, 'click', function() {
	infowindow80.open(map,marker80);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

// second

var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - DLF Phase 3, Gurgaon.</div><div class='p-5'>Plot-2, Siris Road,<br/> DLF Phase-III, Gurgaon, 122002,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng78 = new google.maps.LatLng(28.471604 ,77.098938);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//3
var infowindow70 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - DLF Phase 1, Gurgaon.</div><div class='p-5'>E-2/1, DLF Phase-1, ,<br>Behind Arjun Marg Shopping Complex<br>Gurgaon 122002<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng70 = new google.maps.LatLng(28.465045 ,77.100712);
bounds.extend (myLatlng70);
var marker70 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng70,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker70, 'click', function() {
	infowindow70.open(map,marker70);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//4
var infowindow71 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Sector-31, Gurgaon.</div><div class='p-5'>Opposite 32 Mile Stone, <br> NH 8,Sector-31,Gurgaon ,122002<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng71 = new google.maps.LatLng(28.453807,77.04925);
bounds.extend (myLatlng71);
var marker71 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng71,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker71, 'click', function() {
	infowindow71.open(map,marker71);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//5

var infowindow72 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - The Plaza Mall (MG Road), Gurgaon).</div><div class='p-5'>The Plaza Mall, MG Road,<br>Near Westin Hotel, Gurgaon ,122001<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng72 = new google.maps.LatLng(28.455473 ,77.021902);
bounds.extend (myLatlng72);
var marker72 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng72,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker72, 'click', function() {
	infowindow72.open(map,marker72);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//6

var infowindow73 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Palam Vihar, Gurgaon.</div><div class='p-5'>E-2228, Near Ansal Plaza,<br>Palam Vihar, Gurgaon 122017<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng73 = new google.maps.LatLng(28.512783 ,77.036893);
bounds.extend (myLatlng73);
var marker73 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng73,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker73, 'click', function() {
	infowindow73.open(map,marker73);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//7
var infowindow74 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - DLF Cyber City, Gurgaon.</div><div class='p-5'>Near DLF Phase 2,<br>Rapid Metro Station, Gurgaon<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng74 = new google.maps.LatLng(28.487725 ,77.092814);
bounds.extend (myLatlng74);
var marker74 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng74,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker74, 'click', function() {
	infowindow74.open(map,marker74);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//8

var infowindow75 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Galleria Market, Gurgaon.</div><div class='p-5'>Opposite Galleria Market,<br>Near Costa Coffee, Gurgaon,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng75 = new google.maps.LatLng(28.459497 ,77.026638);
bounds.extend (myLatlng75);
var marker75 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng75,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker75, 'click', function() {
	infowindow75.open(map,marker75);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//9
var infowindow76 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - DT Mega Mall, Gurgaon.</div><div class='p-5'>The DT Mega Mall, Golf Course Road,<br> Near Bristol Hotel, Gurgaon<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng76 = new google.maps.LatLng(28.453992 ,77.098043);
bounds.extend (myLatlng76);
var marker76 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng76,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker76, 'click', function() {
	infowindow76.open(map,marker76);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//10
var infowindow77 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Hotel Formule1, Gurgaon.</div><div class='p-5'>Hotel Formule1 Gurgaon Good Earth City Centre,<br>Opposite Malibu Commercial Complex,<br> Sector 50, Gurgaon- 122018, Haryana</br><a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng77 = new google.maps.LatLng(28.432249,77.1137595);
bounds.extend (myLatlng77);
var marker77 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng77,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker77, 'click', function() {
	infowindow77.open(map,marker77);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//11
     var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Huda City, Gurgaon.</div><div class='p-5'> Huda City Metro Station, Sector 29,<br> Gurgaon, Haryana 122009,</br><a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng78 = new google.maps.LatLng(28.4833734,77.0332034);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});   

google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.addListener(map, 'click', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			 map.set('styles', '');
		});
		google.maps.event.addListenerOnce(map, 'click', function() {
			pushEvent('Maps','First Click', 'Home Page');
		});
		google.maps.event.addListener(map, 'dragend', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
		});
		google.maps.event.addListener(map, 'dblclick', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#HomepageMap").show();
		google.maps.event.trigger(map, 'resize');
		map.fitBounds(bounds);
		curVal = parseInt(map.getZoom());
		$("#ZoomPlus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$j("#ZoomMinus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal-1;
			mapZoom(map,newVal);
		});
	});	  
		  
}



function checkJquery() {
  if (window.jQuery) {
  var scriptaa = document.createElement('script');
  scriptaa.type = 'text/javascript';
  scriptaa.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA1gLNE5MymygzyWc0ikPQ8iuikOte_MRU&sensor=false&'+'callback=initialize';
  document.body.appendChild(scriptaa);
  } else {
		window.setTimeout(checkJquery, 1000);
	}
}

checkJquery();

    </script>

	<div id="map-canvas" class='map'></div>
	<div class="rela_new">
	<div class='help ts-0' id='HomepageMapHelp'>
	<div class='text r-5'>Myles Gurgaon Locations</div>
	</div>
	<div class='ts-0' id='HomepageMapZoom' style='display:none;' >
	<div class='zoom-box'>
	<div class='box-active r-2' id='ZoomPlus'>+</div>
	<div class='box-active r-2' id='ZoomMinus'>-</div>
	</div>
	</div>
	</div>
	<script>
  function downloadJSAtOnloadnew() {
  	var element = document.createElement("script");
  	element.src = mainbaseurl+"/map/map.js";
  	document.body.appendChild(element);
  }
  if (window.addEventListener)
  	window.addEventListener("load", downloadJSAtOnloadnew, false);
  else if (window.attachEvent)
  	window.attachEvent("onload", downloadJSAtOnloadnew);
  else window.onload = downloadJSAtOnloadnew;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38347989-1']);
  _gaq.push(['_setDomainName', 'carzonrent.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
  	var ga = document.createElement('script'); 
	ga.type = 'text/javascript'; ga.async = true;
  	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
	
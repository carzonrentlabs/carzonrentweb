<?php
include_once('../classes/cor.ws.class.php');
include_once('../classes/cor.xmlparser.class.php');

?>
<link href="<?php echo corWebRoot; ?>/map/map.css" media="screen" rel="stylesheet" />
	<style>
	 #map-canvas {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
	  .help{
position:absolute;
top:50px;
left:50%;
z-index:1;
}
.help .text{
position: absolute;
left: -220px;
color: #fff;
font-weight: bold;
font-size: 12px;
padding: 5px 0;
width: 170px;
text-align: center;
background:rgba(0,0,0,0.8);
/*background: url("images/bg.png");*/
}
#HomepageMapZoom .zoom-box {
position: absolute;
right: 10px;
top: 50px;
color: #fff;
font-weight: bold;
font-size: 16px;
width: 30px;
height: 80px;
text-align: center;
font-size: 30px;
line-height: 30px;
}
#HomepageMapZoom .zoom-box .box-active {
width: 30px;
height: 30px;
background: #323a45;
margin-bottom: 5px;
cursor: pointer;
opacity: .9;
}
.rela_new{
position:relative;
top:-333px;
}

	</style>
<noscript>
<iframe height='0' src='//www.googletagmanager.com/ns.html?id=GTM-T24CHT' style='display:none;visibility:hidden' width='0'></iframe>
</noscript>
<script>
var mainbaseurl='<?php echo corWebRoot; ?>';
function initialize() 
{
  var mapOptions = {
	mapTypeId: google.maps.MapTypeId.ROADMAP, 
	scrollwheel: true, 
	streetViewControl: false,
	mapTypeControl: false,
	zoomControl: true,
	panControl: true,
	panControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
    // zoom: 8,
    //center: new google.maps.LatLng(21.1289956, 82.7792201)
  }
    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	map.set('zoomControl', false);
	map.set('scrollwheel', false);
	map.set('panControl', false);
	  bounds = new google.maps.LatLngBounds ();
	var infowindow82 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'></div><div class='p-5'></div></div>",
	maxWidth: 400
});
	
var myLatlng82 = new google.maps.LatLng(28.5356816,77.3914352);
bounds.extend (myLatlng82);
var marker82 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/View_1_1.png",
	position: myLatlng82,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});

google.maps.event.addListener(marker82, 'click', function() {
	infowindow82.open(map,marker82);
	pushEvent('Locations Selected', '', 'Home Page Map');
});

//1
var infowindow80 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Sector-31, Noida.</div><div class='p-5'>C1/5 Sector 31,<br> Near Jaipuriya Mall Noida<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});

var myLatlng80 = new google.maps.LatLng(28.535516,77.391026);
bounds.extend (myLatlng80);
var marker80 = new google.maps.Marker({
	flat: true,
	icon:  mainbaseurl+"/map/map/view.png",
	position: myLatlng80,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Yerwada (IBM Daksh)"
});
google.maps.event.addListener(marker80, 'click', function() {
	infowindow80.open(map,marker80);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//2 
var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Sector 93, Noida.</div><div class='p-5'>Silver City Apartment, Silver City, Sector 93,<br>Noida, Uttar Pradesh 201304, India<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
	
});
var myLatlng78 = new google.maps.LatLng(28.51932,77.387349);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//3

var infowindow70 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Sector -51, Noida.</div><div class='p-5'>B1A/6,commercial Complex,<br>Sector -51,Noida-201301<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
	 
});
var myLatlng70 = new google.maps.LatLng(28.58212,77.326699);
bounds.extend (myLatlng70);
var marker70 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng70,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker70, 'click', function() {
	infowindow70.open(map,marker70);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});


var infowindow68 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Jaypee Greens - Greater Noida.</div><div class='p-5'>Block B, Jaypee Greens, Pari Chowk, <br/>Greater Noida, Uttar Pradesh 201310<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng68 = new google.maps.LatLng(28.465243 , 77.511058);
bounds.extend (myLatlng68);
var marker68 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng68,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker68, 'click', function() {
	infowindow68.open(map,marker68);
	pushEvent('Locations Selected', 'Carzonrent India PVt Ltd.', 'Home Page Map');
});


google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.addListener(map, 'click', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			 map.set('styles', '');
		});
		google.maps.event.addListenerOnce(map, 'click', function() {
			pushEvent('Maps','First Click', 'Home Page');
		});
		google.maps.event.addListener(map, 'dragend', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
		});
		google.maps.event.addListener(map, 'dblclick', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#HomepageMap").show();
		google.maps.event.trigger(map, 'resize');
		map.fitBounds(bounds);
		curVal = parseInt(map.getZoom());
		$("#ZoomPlus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#ZoomMinus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal-1;
			mapZoom(map,newVal);
		});
	});	  
		  
}



function checkJquery() {
  if (window.jQuery) {
  var scriptmum = document.createElement('script');
  scriptmum.type = 'text/javascript';
  scriptmum.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA1gLNE5MymygzyWc0ikPQ8iuikOte_MRU&'+'callback=initialize';
  document.body.appendChild(scriptmum);
  } else {
		window.setTimeout(checkJquery, 1000);
	}
}

checkJquery();

    </script>
    
	<div   id="map-canvas" class='map' ></div>
	<div class="rela_new">
	<div class='help ts-0' id='HomepageMapHelp'>
	<div class='text r-5'>Myles Noida Locations</div>
	</div>
	<div class='ts-0' id='HomepageMapZoom' style='display:none;' >
	<div class='zoom-box'>
	<div class='box-active r-2' id='ZoomPlus'>+</div>
	<div class='box-active r-2' id='ZoomMinus'>-</div>
	</div>
	</div>
	</div>
 
 
 <script>
  function downloadJSAtOnloadmum() {
  	var element = document.createElement("script");
  	element.src = mainbaseurl+"/map/map.js";
  	document.body.appendChild(element);
  }
  if (window.addEventListener)
  	window.addEventListener("load", downloadJSAtOnloadmum, false);
  else if (window.attachEvent)
  	window.attachEvent("onload", downloadJSAtOnloadmum);
  else window.onload = downloadJSAtOnloadmum;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38347989-1']);
  _gaq.push(['_setDomainName', 'carzonrent.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
  	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
 
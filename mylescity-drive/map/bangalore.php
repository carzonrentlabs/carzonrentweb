<?php
include_once('../classes/cor.ws.class.php');
include_once('../classes/cor.xmlparser.class.php');
?>
<link href="<?php echo corWebRoot; ?>/map/map.css" media="screen" rel="stylesheet" />
<style>
       #map-canvas {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
	  .help{
position:absolute;
top:50px;
left:50%;
z-index:1;
}
.help .text{
position: absolute;
left: -220px;
color: #fff;
font-weight: bold;
font-size: 11px;
padding: 5px 0;
width: 170px;
text-align: center;
background:rgba(0,0,0,0.8);
/*background: url("images/bg.png");*/
}
#HomepageMapZoom .zoom-box {
position: absolute;
right: 10px;
top: 50px;
color: #fff;
font-weight: bold;
font-size: 16px;
width: 30px;
height: 80px;
text-align: center;
font-size: 30px;
line-height: 30px;
}
#HomepageMapZoom .zoom-box .box-active {
width: 30px;
height: 30px;
background: #323a45;
margin-bottom: 5px;
cursor: pointer;
opacity: .9;
}
.rela_new{
position:relative;
top:-333px;
}

    </style>
	
	

<noscript>
<iframe height='0' src='//www.googletagmanager.com/ns.html?id=GTM-T24CHT' style='display:none;visibility:hidden' width='0'></iframe>
</noscript>
<script>
var mainbaseurl='<?php echo corWebRoot; ?>';
 
function initialize() 
{
  var mapOptions = {
	mapTypeId: google.maps.MapTypeId.ROADMAP, 
	scrollwheel: true, 
	streetViewControl: false,
	mapTypeControl: false,
	zoomControl: true,
	panControl: true,
	panControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
  // zoom: 8,
    //center: new google.maps.LatLng(21.1289956, 82.7792201)
  }
    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	map.set('zoomControl', false);
	map.set('scrollwheel', false);
	map.set('panControl', false);
	  bounds = new google.maps.LatLngBounds ();
var infowindow82 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'></div><div class='p-5'></div></div>",
	maxWidth: 400
});
	
var myLatlng82 = new google.maps.LatLng(12.971599,77.594563);
bounds.extend (myLatlng82);
var marker82 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/View_1_1.png",
	position: myLatlng82,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});

google.maps.event.addListener(marker82, 'click', function() {
	infowindow82.open(map,marker82);
	pushEvent('Locations Selected', '', 'Home Page Map');
});



// First 

var infowindow80 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Ulsoor, Bengaluru.</div><div class='p-5'>No-16, Behind BSNL Office, Ulsoor Cross Road,<br/>Ulsoor Cross Road, Ulsoor, Bangalore<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng80 = new google.maps.LatLng(12.974638,77.621584);
bounds.extend (myLatlng80);
var marker80 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng80,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker80, 'click', function() {
	infowindow80.open(map,marker80);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});


// second 

var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Electronic City,Bangalore.</div><div class='p-5'>#63, Chinnegowda building, first floor, next to old RTO office,<br/>central jail road, Electronic City,  <br/>Bengaluru, Karnataka 560100,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng78 = new google.maps.LatLng(12.942745,77.509101);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

// third
var infowindow75 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kempegowda International Airport, Bangalore .</div><div class='p-5'>Devanahalli, KIAL Road,<br/>Bangalore, Karnataka 560300<a href='http://carzonrent.com' target='_blank'>www.carzorent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng75 = new google.maps.LatLng(13.199839,77.703037);
bounds.extend (myLatlng75);
var marker75 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng75,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker75, 'click', function() {
	infowindow75.open(map,marker75);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//forth

var infowindow70 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Itbp Ascendas, Bangalore .</div><div class='p-5'>Businees Centre, Underground Floor<br/>Innovator Building, ITPB, Whitefield,<br/> Banaglore-560066, <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng70 = new google.maps.LatLng(12.986055,77.735923);
bounds.extend (myLatlng70);
var marker70 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng70,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker70, 'click', function() {
	infowindow70.open(map,marker70);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

// fifth

var infowindow68 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Prestige Shantiniketan, Bangalore.</div><div class='p-5'>Cresent-4, Upper Basement,<br/> Whitefield Main Road,Mahadevapura Post,<br/>Bangalore-560048, <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng68 = new google.maps.LatLng(12.991605,77.730868);
bounds.extend (myLatlng68);
var marker68 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng68,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker68, 'click', function() {
	infowindow68.open(map,marker68);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//six
var infowindow65 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Nagawara, Bangalore.</div><div class='p-5'>#34/35, First Floor, Patel Munivenkatappa Layout<br/>Opp.Manyata Techpark, Nagavara Ring Road, Hebbal,<br/>,Bangalore-560045, <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng65 = new google.maps.LatLng(13.040918,77.6218548417091);
bounds.extend (myLatlng65);
var marker65 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng65,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker65, 'click', function() {
	infowindow65.open(map,marker65);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

// seventh

var infowindow61 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kanakapura Road – Magnum Honda, Bangalore.</div><div class='p-5'>Survey #13,11 k.m. Kanakapura Road,<br/><br/>Bangalore- 560062, <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng61 = new google.maps.LatLng(12.892678,77.567714);
bounds.extend (myLatlng61);
var marker61 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng61,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker61, 'click', function() {
	infowindow61.open(map,marker61);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

// eight 

var infowindow60 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Malleshwaram – Garuda Autocraft, Bangalore .</div><div class='p-5'>191/1, 11th Cross, <br/>Malleswaram, Bangalore - 560003<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng60 = new google.maps.LatLng(13.002174,77.570512);
bounds.extend (myLatlng60);
var marker60 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng60,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker60, 'click', function() {
	infowindow60.open(map,marker60);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//9

var infowindow105 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - K R Puram-Trident Hyundai/Trident Renault,</div><div class='p-5'>#111, 124/125, B Narayanapura, KR Puram, </br>Hobli, Bangalore-560016, <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng105 = new google.maps.LatLng(12.999843,77.682057);
bounds.extend (myLatlng105);
var marker105 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng105,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker105, 'click', function() {
	infowindow105.open(map,marker105);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//10



//11

var infowindow43 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Domlur, Trident Hyundai , Bangalore.</div><div class='p-5'>#540, Amarjyothi Layout, Inner Ring Road,<br/>Domlur, Bangalore-560071,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng43 = new google.maps.LatLng(12.952757,77.640357);
bounds.extend (myLatlng43);
var marker43 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng43,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker43, 'click', function() {
	infowindow43.open(map,marker43);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//12

var infowindow39 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Hosur Road-Trident Hyundai, Bangalore.</div><div class='p-5'># 46/4, Garvebhavi palya Begur <br/>Hobli, Bangalore-560068,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng39 = new google.maps.LatLng(12.890378,77.639533);
bounds.extend (myLatlng39);
var marker39 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng39,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker39, 'click', function() {
	infowindow39.open(map,marker39);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//13
var infowindow38 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Mysore Road – Trident Renault, Bangalore.</div><br/>Sy.No.18/1B (OLD NO.18/1C) ,Nayandahalli Grama,<br/>Kengeri Hobli, Bangalore <a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng38 = new google.maps.LatLng(12.936082,77.5162);
bounds.extend (myLatlng38);
var marker38 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng38,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker38, 'click', function() {
	infowindow38.open(map,marker38);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//14
var infowindow37 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kalyan Nagar – Trident Chevrolet/Trident Hyundai</div><div class='p-5'>No. 122/1, C. Shankar Reddy Layout, Kalyan Nagar Ring Road,<br/>Kalyan Nagar, Bangalore- 560043,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng37 = new google.maps.LatLng(13.025388,77.641735);
bounds.extend (myLatlng37);
var marker37 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng37,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker37, 'click', function() {
	infowindow37.open(map,marker37);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//15


//16

var infowindow24 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - J P Nagar – Trident Hyundai, Bangalore.</div><div class='p-5'># 10th KM, (Anthony's Industrial Area), Kanakpura Main Road,<br/>JP.Nagar, Bangalore - 560078 ,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng24 = new google.maps.LatLng(12.898566, 77.572886);
bounds.extend (myLatlng24);
var marker24 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng24,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker24, 'click', function() {
	infowindow24.open(map,marker24);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//17
var infowindow23 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Yashwanthpur – Trident Hyundai, Bangalore.</div><div class='p-5'>#99/A, Industrial Suburb,Tumkur Road, <br/>Yeshwanthpur Bangalore - 560022,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng23 = new google.maps.LatLng(13.027973, 77.540689);
bounds.extend (myLatlng23);
var marker23 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng23,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker23, 'click', function() {
	infowindow23.open(map,marker23);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//18
var infowindow72 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kanakapura Road – Magnum Honda, Bangalore.</div><div class='p-5'>Survey #13,11 k.m. Kanakapura Road<br/> Bangalore- 560062,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng72 = new google.maps.LatLng(12.893076, 77.56794);
bounds.extend (myLatlng72);
var marker72 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng72,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker72, 'click', function() {
	infowindow72.open(map,marker72);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//19

var infowindow21 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Prestige Tech Park-Outer Ring Road, Bangalore.</div><div class='p-5'>Kariyammana Agrahara Road, Yemalur, Marathahalli,<br/> Bengaluru, Karnataka 560037,<a href='http://carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng21 = new google.maps.LatLng(12.942204,77.692888);
bounds.extend (myLatlng21);
var marker21 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng21,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker21, 'click', function() {
	infowindow21.open(map,marker21);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});




<!-- data start here -->

<!-- data end here-->




google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.addListener(map, 'click', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			 map.set('styles', '');
		});
		google.maps.event.addListenerOnce(map, 'click', function() {
			pushEvent('Maps','First Click', 'Home Page');
		});
		google.maps.event.addListener(map, 'dragend', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
		});
		google.maps.event.addListener(map, 'dblclick', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#HomepageMap").show();
		google.maps.event.trigger(map, 'resize');
		map.fitBounds(bounds);
		curVal = parseInt(map.getZoom());
		$("#ZoomPlus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#ZoomMinus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal-1;
			mapZoom(map,newVal);
		});
	});	  
		  
}



function checkJquery() {
  if (window.jQuery) {
  var scriptmum = document.createElement('script');
  scriptmum.type = 'text/javascript';
  scriptmum.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA1gLNE5MymygzyWc0ikPQ8iuikOte_MRU&'+'callback=initialize';
  document.body.appendChild(scriptmum);
  } else {
		window.setTimeout(checkJquery, 1000);
	}
}

checkJquery();

    </script>
    
	<div   id="map-canvas" class='map' ></div>
	<div class="rela_new">
	<div class='help ts-0' id='HomepageMapHelp'>
	<div class='text r-5'>Myles Bangalore Locations</div>
	</div>
	<div class='ts-0' id='HomepageMapZoom' style='display:none;' >
	<div class='zoom-box'>
	<div class='box-active r-2' id='ZoomPlus'>+</div>
	<div class='box-active r-2' id='ZoomMinus'>-</div>
	</div>
	</div>
	</div>
 
 
 <script>
  function downloadJSAtOnloadmum() {
  	var element = document.createElement("script");
  	element.src =  mainbaseurl+"/map/map.js";
  	document.body.appendChild(element);
  }
  if (window.addEventListener)
  	window.addEventListener("load", downloadJSAtOnloadmum, false);
  else if (window.attachEvent)
  	window.attachEvent("onload", downloadJSAtOnloadmum);
  else window.onload = downloadJSAtOnloadmum;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38347989-1']);
  _gaq.push(['_setDomainName', 'carzonrent.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
  	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
 
<?php
include_once('/classes/cor.ws.class.php');
include_once('/classes/cor.xmlparser.class.php');
include_once("/includes/cache-func.php");
?>
<div  class="clearfix"></div>
    <style>
      #map-canvas {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
	  .help{
position:absolute;
top:50px;
left:50%;
z-index:1;
}
.help .text{
position: absolute;
left: -220px;
color: #fff;
font-weight: bold;
font-size: 12px;
padding: 5px 0;
width: 170px;
text-align: center;
background:rgba(0,0,0,0.8);
/*background: url("images/bg.png");*/
}
#HomepageMapZoom .zoom-box {
position: absolute;
right: 10px;
top: 50px;
color: #fff;
font-weight: bold;
font-size: 16px;
width: 30px;
height: 80px;
text-align: center;
font-size: 30px;
line-height: 30px;
}
#HomepageMapZoom .zoom-box .box-active {
width: 30px;
height: 30px;
background: #323a45;
margin-bottom: 5px;
cursor: pointer;
opacity: .9;
}
.rela_new{
position:relative;
top:-333px;
}
    </style>
<link href="<?php echo corWebRoot; ?>/map/map.css" media="screen" rel="stylesheet"/>

<noscript>
<iframe height='0' src='//www.googletagmanager.com/ns.html?id=GTM-T24CHT' style='display:none;visibility:hidden' width='0'></iframe>
</noscript>

	
<script type="text/javascript">
var mainbaseurl='<?php echo corWebRoot; ?>';

function initialize() 
{
  var mapOptions = {
	mapTypeId: google.maps.MapTypeId.ROADMAP, 
	scrollwheel: true, 
	streetViewControl: false,
	mapTypeControl: false,
	zoomControl: true,
	panControl: true,
	panControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
  // zoom: 8,
    //center: new google.maps.LatLng(21.1289956, 82.7792201)
  }
    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	map.set('zoomControl', false);
	map.set('scrollwheel', false);
	map.set('panControl', false);
	  bounds = new google.maps.LatLngBounds ();
var infowindow82 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'> Newdelhi)</div><div class='p-5'><b>Car rental at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Mahindra Scorpio LX</div></div>",
	maxWidth: 400
});
	
var myLatlng82 = new google.maps.LatLng(28.6160134,77.2099439);
bounds.extend (myLatlng82);
var marker82 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/View_1_1.png",
	position: myLatlng82,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "F.C. Road (Shreenath Plaza)"
});

google.maps.event.addListener(marker82, 'click', function() {
	infowindow82.open(map,marker82);
	
});

//first

var infowindow80 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Jhandewalan, New Delhi.</div><div class='p-5'>9th Floor, Videocon Tower, Block-E1<br/>Jhandewalan Extension,New Delhi-110055,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng80 = new google.maps.LatLng(28.6450645,77.203015);
bounds.extend (myLatlng80);
var marker80 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng80,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker80, 'click', function() {
	infowindow80.open(map,marker80);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

// second

var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Sarita Vihar, Delhi.</div><div class='p-5'>AB-28, Opp Aptara Building,Mohan Co-operative,<br/>Sarita Vihar Near Sarita Vihar Metro Station, Delhi-110044,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng78 = new google.maps.LatLng(28.5006897,77.3152116);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


var infowindow75 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Mayur Vihar, New Delhi.</div><div class='p-5'> New Ashok Nagar Metro Station, Opposite<br/>Crown Plaza Hotel, Mayur Vihar, New Delhi, <br/>Delhi 110091, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng75 = new google.maps.LatLng(28.614624,77.312158);
bounds.extend (myLatlng75);
var marker75 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng75,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker75, 'click', function() {
	infowindow75.open(map,marker75);
	//pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});



var infowindow70 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Janakpuri West, New Delhi.</div><div class='p-5'>Janakpuri Metro Station (W), Opposite Hilton Hotel,<br/>Janakpuri, New Delhi, Delhi<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng70 = new google.maps.LatLng(28.621899,77.087838);
bounds.extend (myLatlng70);
var marker70 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng70,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker70, 'click', function() {
	infowindow70.open(map,marker70);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});

//
var infowindow68 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Nehru Place, New Delhi</div><div class='p-5'>B-18, Chirag Enclave, Lala Lajpat Rai Road,<br/>Nehru Place, New Delhi, Delhi 110048<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng68 = new google.maps.LatLng(28.546819,77.24946);
bounds.extend (myLatlng68);
var marker68 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng68,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker68, 'click', function() {
	infowindow68.open(map,marker68);
	//pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});


//
var infowindow65 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Lajpat Nagar, New Delhi.</div><div class='p-5'>52, Ring Road, Block H, Lajpat Nagar III,<br/>Lajpat Nagar, New Delhi, Delhi 110024,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng65 = new google.maps.LatLng(28.564669, 77.245995);
bounds.extend (myLatlng65);
var marker65 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng65,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker65, 'click', function() {
	infowindow65.open(map,marker65);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd', 'Home Page Map');
});


//7
var infowindow61 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Connaught Place, New Delhi.</div><div class='p-5'>Opposite Kake Da Hotel, Connaught Circus, Connaught Lane,<br/>Connaught Place, New Delhi, Delhi 110001, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng61 = new google.maps.LatLng(28.629848,77.22303);
bounds.extend (myLatlng61);
var marker61 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng61,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ")"
});
google.maps.event.addListener(marker61, 'click', function() {
	infowindow61.open(map,marker61);
	pushEvent('Locations Selected', 'Carzonrent Indi pvt Ltd.', 'Home Page Map');
});

//8


var infowindow60 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Dwarka, New Delhi.</div><div class='p-5'>17, Road Number 203, Block A,<br/>Sector 12 Dwarka, Dwarka, New Delhi,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng60 = new google.maps.LatLng(28.6149511,77.022712);
bounds.extend (myLatlng60);
var marker60 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng60,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker60, 'click', function() {
	infowindow60.open(map,marker60);
	//pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//9
var infowindow45 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Khan Market, New Delhi.</div><div class='p-5'>Prithviraj Lane, Khan Market, <br/>Rabindra Nagar, New Delhi, Delhi 110003, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng45 = new google.maps.LatLng(28.599866,77.224535);
bounds.extend (myLatlng45);
var marker45 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng45,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker45, 'click', function() {
	infowindow45.open(map,marker45);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//10

var infowindow44 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Okhla, New Delhi.</div><div class='p-5'>D – 12, Okhla Phase 1,<br/>Sailing Club Road, Canal Colony,<br/>Okhla, New Delhi, Delhi 110025<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng44 = new google.maps.LatLng(28.564827,77.288479);
bounds.extend (myLatlng44);
var marker44 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng44,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker44, 'click', function() {
	infowindow44.open(map,marker44);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//11
var infowindow43 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Hauz Khas, New Delhi.</div><div class='p-5'>Corner -44,<br/>Opp. NIFT, Near DDA Park, Hauz Khas, New Delhi – 110017,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng43 = new google.maps.LatLng(28.5494489,77.2001368);
bounds.extend (myLatlng43);
var marker43 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng43,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker43, 'click', function() {
	infowindow43.open(map,marker43);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//12

var infowindow39 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kailash Colony, New Delhi.</div><div class='p-5'> Kailash Colony Windshied Experts,Opp Metro Pillar No-81, A-15, Kailash Colony Road, Block A<br/>Kailash Colony, Greater Kailash, New Delhi, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng39 = new google.maps.LatLng(28.5520154,77.241892);
bounds.extend (myLatlng39);
var marker39 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng39,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker39, 'click', function() {
	infowindow39.open(map,marker39);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//13

var infowindow38 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Mahipalpur, New Delhi.</div><div class='p-5'>Indian Oil Petrol Pump,<br/>Next to Dusit Devarana, NH 8 Towards, New Delhi, ,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng38 = new google.maps.LatLng(28.613939,77.209021);
bounds.extend (myLatlng38);
var marker38 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng38,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker38, 'click', function() {
	infowindow38.open(map,marker38);
	pushEvent('Locations Selected', 'Carzonrent india Pvt Ltd.', 'Home Page Map');
});

//14

var infowindow37 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kamla Nagar, Delhi.</div><div class='p-5'>SPARK Mall, Kamla Nagar, New Delhi,<br/>DL 110007 India<br/> <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng37 = new google.maps.LatLng(28.680556, 77.203611);
bounds.extend (myLatlng37);
var marker37 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng37,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker37, 'click', function() {
	infowindow37.open(map,marker37);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//15

var infowindow29 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Karkardooma, Delhi.</div><div class='p-5'>Karkarduma, Bhartendu Harish Chandra Marg,<br/>Anand Vihar, New Delhi, DL 110092, India<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng29 = new google.maps.LatLng(28.648545, 77.305967);
bounds.extend (myLatlng29);
var marker29 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng29,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker29, 'click', function() {
	infowindow29.open(map,marker29);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//16
var infowindow24 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Karol Bagh, Delhi.</div><div class='p-5'>Karol Bagh Metro Station,<br/>Pusa Rd, Karol Bagh, New Delhi<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng24 = new google.maps.LatLng(28.64402,77.188069);
bounds.extend (myLatlng24);
var marker24 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng24,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker24, 'click', function() {
	infowindow24.open(map,marker24);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//17
var infowindow83 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Kashmere Gate, Delhi .</div><div class='p-5'>Gokhale Marg, Inter State Bus Terminal, <br/>Kashmere Gate, New Delhi, DL 110006<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng83 = new google.maps.LatLng(28.6669983,77.228898);
bounds.extend (myLatlng83);
var marker83 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng83,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker83, 'click', function() {
	infowindow83.open(map,marker83);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//18
var infowindow84 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Mathura Road, Delhi.</div><div class='p-5'>A31, Mathura Rd, Mohan Cooperative Industrial Estate,<br/>Badarpur, New Delhi, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng84 = new google.maps.LatLng(28.502541,77.300603);
bounds.extend (myLatlng84);
var marker84 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng84,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker84, 'click', function() {
	infowindow84.open(map,marker84);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//19

var infowindow85 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Moti Nagar, Delhi.</div><div class='p-5'>Shivaji Marg, 12A, Najafgarh Rd,<br/>Moti Nagar, New Delhi<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng85 = new google.maps.LatLng(28.65787,77.142674);
bounds.extend (myLatlng85);
var marker85 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng85,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker85, 'click', function() {
	infowindow85.open(map,marker85);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});


//20
var infowindow86 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Netaji Subhash Palace, Delhi.</div><div class='p-5'>Netaji Subhash Place Metro Station,<br/>New Delhi, DL <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng86 = new google.maps.LatLng(28.696092,77.15274);
bounds.extend (myLatlng86);
var marker86 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng86,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker86, 'click', function() {
	infowindow86.open(map,marker86);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});
//21


var infowindow87 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Nirman Vihar, Delhi.</div><div class='p-5'>Vikas Marg, Laxmi Nagar Commercial Complex, <br/>Preet Vihar, New Delhi,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng87 = new google.maps.LatLng(28.641627,77.295375);
bounds.extend (myLatlng87);
var marker87 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng87,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker87, 'click', function() {
	infowindow87.open(map,marker87);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//22

var infowindow88 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Rajouri Garden, Delhi.</div><div class='p-5'>City Square Mall, Vishal Enclave, Rajouri Garden,<br/> New Delhi,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng88 = new google.maps.LatLng(28.588949,77.061555);
bounds.extend (myLatlng88);
var marker88 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng88,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker88, 'click', function() {
	infowindow88.open(map,marker88);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//23
 var infowindow89 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Saket, Delhi.</div><div class='p-5'>Max Super Speciality Hospital, 2, Press Enclave Marg,<br/> Saket, New Delhi,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng89 = new google.maps.LatLng(28.527368,77.211756);
bounds.extend (myLatlng89);
var marker89 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng89,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker89, 'click', function() {
	infowindow89.open(map,marker89);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//24

 var infowindow90 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - South Ex, Delhi.</div><div class='p-5'>M-10, South Extension II,<br/>New Delhi, DL 110049, <a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng90 = new google.maps.LatLng(28.566603,77.220133);
bounds.extend (myLatlng90);
var marker90 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng90,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker90, 'click', function() {
	infowindow90.open(map,marker90);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});

//25

 var infowindow91 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Airport - Terminal -1, Delhi.</div><div class='p-5'>IGI Airport,<br/> New Delhi,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});
var myLatlng91 = new google.maps.LatLng(28.556162,77.099958);
bounds.extend (myLatlng91);
var marker91 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng91,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker91, 'click', function() {
	infowindow91.open(map,marker91);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});






<!-- data start here -->

<!-- data end here-->




google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.addListener(map, 'click', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			 map.set('styles', '');
		});
		google.maps.event.addListenerOnce(map, 'click', function() {
			pushEvent('Maps','First Click', 'Home Page');
		});
		google.maps.event.addListener(map, 'dragend', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
		});
		google.maps.event.addListener(map, 'dblclick', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#HomepageMap").show();
		google.maps.event.trigger(map, 'resize');
		map.fitBounds(bounds);
		curVal = parseInt(map.getZoom());
		$("#ZoomPlus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$j("#ZoomMinus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal-1;
			mapZoom(map,newVal);
		});
	});	  
		  
}



function checkJquery() {
  if (window.jQuery) {
  var scriptaa = document.createElement('script');
  scriptaa.type = 'text/javascript';
  scriptaa.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA1gLNE5MymygzyWc0ikPQ8iuikOte_MRU&amp;sensor=false&'+'callback=initialize';
  document.body.appendChild(scriptaa);
  } else {
		window.setTimeout(checkJquery, 1000);
	}
}

checkJquery();

    </script>

	<div id="map-canvas" class='map'></div>
	<div class="rela_new">
	<div class='help ts-0' id='HomepageMapHelp'>
	<div class='text r-5'>Myles Delhi Locations</div>
	</div>
	<div class='ts-0' id='HomepageMapZoom' style='display:none;' >
	<div class='zoom-box'>
	<div class='box-active r-2' id='ZoomPlus'>+</div>
	<div class='box-active r-2' id='ZoomMinus'>-</div>
	</div>
	</div>
	</div>
	<script type="text/javascript">
  function downloadJSAtOnloadnew() {
  	var element = document.createElement("script");
  	element.src = mainbaseurl+"/map/map.js";
  	document.body.appendChild(element);
  }
  if (window.addEventListener)
  	window.addEventListener("load", downloadJSAtOnloadnew, false);
  else if (window.attachEvent)
  	window.attachEvent("onload", downloadJSAtOnloadnew);
  else window.onload = downloadJSAtOnloadnew;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38347989-1']);
  _gaq.push(['_setDomainName', 'carzonrent.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
  	var ga = document.createElement('script'); 
	ga.type = 'text/javascript'; ga.async = true;
  	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
	
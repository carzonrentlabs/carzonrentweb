<?php
include_once('../classes/cor.ws.class.php');
include_once('../classes/cor.xmlparser.class.php');
?>
<link href="<?php echo corWebRoot; ?>/map/map.css" media="screen" rel="stylesheet" />
	<style>
	 #map-canvas {
        height: 300px;
        margin: 0px;
        padding: 0px
      }
	  .help{
position:absolute;
top:50px;
left:50%;
z-index:1;
}
.help .text{
position: absolute;
left: -220px;
color: #fff;
font-weight: bold;
font-size: 11px;
padding: 5px 0;
width: 170px;
text-align: center;
background:rgba(0,0,0,0.8);
/*background: url("images/bg.png");*/
}
#HomepageMapZoom .zoom-box {
position: absolute;
right: 10px;
top: 50px;
color: #fff;
font-weight: bold;
font-size: 16px;
width: 30px;
height: 80px;
text-align: center;
font-size: 30px;
line-height: 30px;
}
#HomepageMapZoom .zoom-box .box-active {
width: 30px;
height: 30px;
background: #323a45;
margin-bottom: 5px;
cursor: pointer;
opacity: .9;
}
.rela_new{
position:relative;
top:-333px;
}

	</style>
<noscript>
<iframe height='0' src='//www.googletagmanager.com/ns.html?id=GTM-T24CHT' style='display:none;visibility:hidden' width='0'></iframe>
</noscript>
<script>
var mainbaseurl='<?php echo corWebRoot; ?>';


function initialize() 
{
  var mapOptions = {
	mapTypeId: google.maps.MapTypeId.ROADMAP, 
	scrollwheel: true, 
	streetViewControl: false,
	mapTypeControl: false,
	zoomControl: true,
	panControl: true,
	panControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
    // zoom: 8,
    //center: new google.maps.LatLng(21.1289956, 82.7792201)
  }
    var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	map.set('zoomControl', false);
	map.set('scrollwheel', false);
	map.set('panControl', false);
	  bounds = new google.maps.LatLngBounds ();
var infowindow82 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'> Newdelhi)</div><div class='p-5'><b>Car rental at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Mahindra Scorpio LX</div></div>",
	maxWidth: 400
});
	
var myLatlng82 = new google.maps.LatLng(23.022505,72.571362);
bounds.extend (myLatlng82);
var marker82 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/View_1_1.png",
	position: myLatlng82,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "F.C. Road (Shreenath Plaza)"
});

google.maps.event.addListener(marker82, 'click', function() {
	infowindow82.open(map,marker82);
	pushEvent('Locations Selected', 'F.C. Road (Shreenath Plaza)', 'Home Page Map');
});

//1
var infowindow80 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Myles - Ahmedabad.</div><div class='p-5'>Ashmi Shopping Centre, Opp. Memnagar Fire Station,<br>27, Vijay Cross Rd, Professor's Colony, Sarvottamnagar Society,<br/>Ahmedabad, Gujarat 380009,<a href='http://www.carzonrent.com' target='_blank'>www.carzonrent.com</a></div></div>",
	maxWidth: 400
});

var myLatlng80 = new google.maps.LatLng(23.04559,72.553019);
bounds.extend (myLatlng80);
var marker80 = new google.maps.Marker({
	flat: true,
	icon:  mainbaseurl+"/map/map/view.png",
	position: myLatlng80,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: ""
});
google.maps.event.addListener(marker80, 'click', function() {
	infowindow80.open(map,marker80);
	pushEvent('Locations Selected', 'Carzonrent India Pvt Ltd.', 'Home Page Map');
});


//2
/*
var infowindow78 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Chennai University</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Mahindra Scorpio LX</div></div>",
	maxWidth: 400
});
var myLatlng78 = new google.maps.LatLng(12.9940476,80.1708019);
bounds.extend (myLatlng78);
var marker78 = new google.maps.Marker({
	flat: true,
	icon: mainbaseurl+"/map/map/view.png",
	position: myLatlng78,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Wakad (Silver Inn Hotel)"
});
google.maps.event.addListener(marker78, 'click', function() {
	infowindow78.open(map,marker78);
	pushEvent('Locations Selected', 'Chennai University', 'Home Page Map');
});


var infowindow75 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Kalyani Nagar (Chaitanya Gokul)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Mahindra Scorpio LX</div></div>",
	maxWidth: 400
});
var myLatlng75 = new google.maps.LatLng(18.545876403033954, 73.90567061724141);
bounds.extend (myLatlng75);
var marker75 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng75,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Kalyani Nagar (Chaitanya Gokul)"
});
google.maps.event.addListener(marker75, 'click', function() {
	infowindow75.open(map,marker75);
	pushEvent('Locations Selected', 'Kalyani Nagar (Chaitanya Gokul)', 'Home Page Map');
});

var infowindow70 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Dhole Patil Road (Akshay Complex)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport</div></div>",
	maxWidth: 400
});
var myLatlng70 = new google.maps.LatLng(18.53669430789797, 73.87578799040057);
bounds.extend (myLatlng70);
var marker70 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng70,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Dhole Patil Road (Akshay Complex)"
});
google.maps.event.addListener(marker70, 'click', function() {
	infowindow70.open(map,marker70);
	pushEvent('Locations Selected', 'Dhole Patil Road (Akshay Complex)', 'Home Page Map');
});

var infowindow68 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Hadapsar (Mega Center)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Ford Ecosport<br/>Mahindra Scorpio LX<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng68 = new google.maps.LatLng(18.50372357827911, 73.92599780112505);
bounds.extend (myLatlng68);
var marker68 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng68,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Hadapsar (Mega Center)"
});
google.maps.event.addListener(marker68, 'click', function() {
	infowindow68.open(map,marker68);
	pushEvent('Locations Selected', 'Hadapsar (Mega Center)', 'Home Page Map');
});

var infowindow65 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Model Colony (Central Mall)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport</div></div>",
	maxWidth: 400
});
var myLatlng65 = new google.maps.LatLng(18.53189867148601, 73.8429104257375);
bounds.extend (myLatlng65);
var marker65 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng65,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Model Colony (Central Mall)"
});
google.maps.event.addListener(marker65, 'click', function() {
	infowindow65.open(map,marker65);
	pushEvent('Locations Selected', 'Model Colony (Central Mall)', 'Home Page Map');
});

var infowindow61 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Erandwane (Central Mall)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng61 = new google.maps.LatLng(18.510299198938167, 73.83867251879565);
bounds.extend (myLatlng61);
var marker61 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng61,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Erandwane (Central Mall)"
});
google.maps.event.addListener(marker61, 'click', function() {
	infowindow61.open(map,marker61);
	pushEvent('Locations Selected', 'Erandwane (Central Mall)', 'Home Page Map');
});

var infowindow60 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Baner Pashan Link Road (Regent Plaza )</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Ford Ecosport</div></div>",
	maxWidth: 400
});
var myLatlng60 = new google.maps.LatLng(18.55232321924117, 73.79314845801218);
bounds.extend (myLatlng60);
var marker60 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng60,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Baner Pashan Link Road (Regent Plaza )"
});
google.maps.event.addListener(marker60, 'click', function() {
	infowindow60.open(map,marker60);
	pushEvent('Locations Selected', 'Baner Pashan Link Road (Regent Plaza )', 'Home Page Map');
});

var infowindow45 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Baner Road (Vrindavan lawns)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo</div></div>",
	maxWidth: 400
});
var myLatlng45 = new google.maps.LatLng(18.544976969989662, 73.81966245178774);
bounds.extend (myLatlng45);
var marker45 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng45,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Baner Road (Vrindavan lawns)"
});
google.maps.event.addListener(marker45, 'click', function() {
	infowindow45.open(map,marker45);
	pushEvent('Locations Selected', 'Baner Road (Vrindavan lawns)', 'Home Page Map');
});

var infowindow44 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Fatima Nagar</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Ford Ecosport<br/>Mercedes GLA</div></div>",
	maxWidth: 400
});
var myLatlng44 = new google.maps.LatLng(18.50380497176189, 73.90046310450998);
bounds.extend (myLatlng44);
var marker44 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng44,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Fatima Nagar"
});
google.maps.event.addListener(marker44, 'click', function() {
	infowindow44.open(map,marker44);
	pushEvent('Locations Selected', 'Fatima Nagar', 'Home Page Map');
});

var infowindow43 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Pune Airport</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport<br/>Tata Safari<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng43 = new google.maps.LatLng(18.581253653424433, 73.91943597584032);
bounds.extend (myLatlng43);
var marker43 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng43,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Pune Airport"
});
google.maps.event.addListener(marker43, 'click', function() {
	infowindow43.open(map,marker43);
	pushEvent('Locations Selected', 'Pune Airport', 'Home Page Map');
});

var infowindow39 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Maldhakka Chowk (Near Dr.Ambedkar Bhavan)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport</div></div>",
	maxWidth: 400
});
var myLatlng39 = new google.maps.LatLng(18.526598710496376, 73.86580576188862);
bounds.extend (myLatlng39);
var marker39 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng39,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Maldhakka Chowk (Near Dr.Ambedkar Bhavan)"
});
google.maps.event.addListener(marker39, 'click', function() {
	infowindow39.open(map,marker39);
	pushEvent('Locations Selected', 'Maldhakka Chowk (Near Dr.Ambedkar Bhavan)', 'Home Page Map');
});

var infowindow38 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Pimple Saudagar (Kokane Chowk)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng38 = new google.maps.LatLng(18.597483515719876, 73.79003981128335);
bounds.extend (myLatlng38);
var marker38 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng38,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Pimple Saudagar (Kokane Chowk)"
});
google.maps.event.addListener(marker38, 'click', function() {
	infowindow38.open(map,marker38);
	pushEvent('Locations Selected', 'Pimple Saudagar (Kokane Chowk)', 'Home Page Map');
});

var infowindow37 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Kothrud (Near Karishma Society)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Ford Ecosport<br/>Mahindra Scorpio LX<br/>Tata Safari<br/>Mercedes A Class</div></div>",
	maxWidth: 400
});
var myLatlng37 = new google.maps.LatLng(18.50222796614508, 73.8212607242167);
bounds.extend (myLatlng37);
var marker37 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng37,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Kothrud (Near Karishma Society)"
});
google.maps.event.addListener(marker37, 'click', function() {
	infowindow37.open(map,marker37);
	pushEvent('Locations Selected', 'Kothrud (Near Karishma Society)', 'Home Page Map');
});

var infowindow29 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Viman Nagar (Ibis Hotel)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Honda City<br/>Mahindra Scorpio LX<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng29 = new google.maps.LatLng(18.55965211632721, 73.91297634691);
bounds.extend (myLatlng29);
var marker29 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng29,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Viman Nagar (Ibis Hotel)"
});
google.maps.event.addListener(marker29, 'click', function() {
	infowindow29.open(map,marker29);
	pushEvent('Locations Selected', 'Viman Nagar (Ibis Hotel)', 'Home Page Map');
});

var infowindow24 = new google.maps.InfoWindow({
	content: "<div class='map-info'><div class='p-5 zoom size-16 f-b'>Hinjewadi (Formule 1 Hotel)</div><div class='p-5'><b>Car makes at site</b><br/>Ford Figo<br/>Honda Amaze<br/>Ford Ecosport<br/>Mahindra Scorpio LX<br/>Mahindra XUV</div></div>",
	maxWidth: 400
});
var myLatlng24 = new google.maps.LatLng(18.591595793276554, 73.74287048354745);
bounds.extend (myLatlng24);
var marker24 = new google.maps.Marker({
	flat: true,
	icon: "//assets.zoomcar.com/assets/map/zoom-400813e723fcc20871e7f303ba22ffe1.png",
	position: myLatlng24,
	map: map,
	optimized: false,
	animation: google.maps.Animation.DROP,
	title: "Hinjewadi (Formule 1 Hotel)"
});
google.maps.event.addListener(marker24, 'click', function() {
	infowindow24.open(map,marker24);
	pushEvent('Locations Selected', 'Hinjewadi (Formule 1 Hotel)', 'Home Page Map');
});*/

<!-- data start here -->

<!-- data end here-->




google.maps.event.addListenerOnce(map, 'idle', function() {
		google.maps.event.addListener(map, 'click', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			 map.set('styles', '');
		});
		google.maps.event.addListenerOnce(map, 'click', function() {
			pushEvent('Maps','First Click', 'Home Page');
		});
		google.maps.event.addListener(map, 'dragend', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
		});
		google.maps.event.addListener(map, 'dblclick', function() {
			$("#HomepageMapHelp").slideUp();
			$("#HomepageMapZoom").slideDown();
			map.set('styles', '');
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#HomepageMap").show();
		google.maps.event.trigger(map, 'resize');
		map.fitBounds(bounds);
		curVal = parseInt(map.getZoom());
		$("#ZoomPlus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal+1;
			mapZoom(map,newVal);
		});
		$("#ZoomMinus").click(function() {
			curVal = parseInt(map.getZoom());
			var newVal = curVal-1;
			mapZoom(map,newVal);
		});
	});	  
		  
}



function checkJquery() {
  if (window.jQuery) {
  var scriptmum = document.createElement('script');
  scriptmum.type = 'text/javascript';
  scriptmum.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA1gLNE5MymygzyWc0ikPQ8iuikOte_MRU&'+'callback=initialize';
  document.body.appendChild(scriptmum);
  } else {
		window.setTimeout(checkJquery, 1000);
	}
}

checkJquery();

    </script>
    
	<div   id="map-canvas" class='map' ></div>
	<div class="rela_new">
	<div class='help ts-0' id='HomepageMapHelp'>
	<div class='text r-5'>Myles Ahmedabad Locations</div>
	</div>
	<div class='ts-0' id='HomepageMapZoom' style='display:none;' >
	<div class='zoom-box'>
	<div class='box-active r-2' id='ZoomPlus'>+</div>
	<div class='box-active r-2' id='ZoomMinus'>-</div>
	</div>
	</div>
	</div>
 
 
 <script>
  function downloadJSAtOnloadmum() {
  	var element = document.createElement("script");
  	element.src =  mainbaseurl+"/map/map.js";
  	document.body.appendChild(element);
  }
  if (window.addEventListener)
  	window.addEventListener("load", downloadJSAtOnloadmum, false);
  else if (window.attachEvent)
  	window.attachEvent("onload", downloadJSAtOnloadmum);
  else window.onload = downloadJSAtOnloadmum;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38347989-1']);
  _gaq.push(['_setDomainName', 'carzonrent.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
  	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
 
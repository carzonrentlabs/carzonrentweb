<?php
  
   header("Location:http://www.mylescars.com");
   exit;

	error_reporting(0);
	include_once("../includes/cache-func.php");
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Myles City-Drive | Carzonrent</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/default-new.css" />
	<link rel="stylesheet" type="text/css" href="css/ladakh.css" />
	<link rel="stylesheet" type="text/css" href="css/jcarousel.css" />
	<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js?v=<?php echo mkdir(); ?>"></script>
	<script type="text/javascript" src="./js/jquery.js"></script>
	<script type="text/javascript">
		_setCar = function(idx){
			var dCarImg = new Array();
			dCarImg.push('purple-nano-1');
			dCarImg.push('blue-nano-1');
			dCarImg.push('silver-nano-1');
			dCarImg.push('golden-nano-1');
			document.getElementById('cars-0').src = './images/purple-nano-0.jpg';
			document.getElementById('cars-1').src = './images/blue-nano-0.jpg';
			document.getElementById('cars-2').src = './images/silver-nano-0.jpg';
			document.getElementById('cars-3').src = './images/golden-nano-0.jpg';
			document.getElementById('cars-' + idx).src = './images/' + dCarImg[idx] + '.jpg';
			document.getElementById('carbrandimage').src = './images/' + dCarImg[idx].replace("-1", "") + '.png';
	       }
	       
	       _getMonthInt = function(ms){
		    var m = 0;
		    switch(ms.toLowerCase()){
			case "jan":
			    m = 0;
			break;
			case "feb":
			    m = 1;
			break;
			case "mar":
			    m = 2;
			break;
			case "apr":
			    m = 3;
			break;
			case "may":
			    m = 4;
			break;
			case "jun":
			    m = 5;
			break;
			case "jul":
			    m = 6;
			break;
			case "aug":
			    m = 7;
			break;
			case "sep":
			    m = 8;
			break;
			case "oct":
			    m = 9;
			break;
			case "nov":
			    m = 10;
			break;
			case "dec":
			    m = 11;
			break;
		    }
		    return m;
		};
	       var month=new Array();
	       month[0]="Jan";
	       month[1]="Feb";
	       month[2]="Mar";
	       month[3]="Apr";
	       month[4]="May";
	       month[5]="Jun";
	       month[6]="Jul";
	       month[7]="Aug";
	       month[8]="Sep";
	       month[9]="Oct";
	       month[10]="Nov";
	       month[11]="Dec";
	       
		function _setDropDateMyles(pi, di){
		   pd = document.getElementById(pi).value.trim().split(" ");
		   dd = document.getElementById(di).value.trim().split(" ");
		   var p = new Date();
		       var d = new Date();
		       m = _getMonthInt(pd[1].replace(",",""));
		       var nxtD = eval(parseInt(pd[0]) + 1);
		       p.setFullYear(pd[2], m, pd[0]);
		       d.setFullYear(pd[2], m, nxtD);
		       document.getElementById(di).value = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3];
		       try {
			   $('#inputFieldSF2').datetimepicker({
			   format:'d M, Y H:i',
			   minDate:d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear() + " " + pd[3],
			   step:30
			   });
		       } catch(e) {
		       }
		       
		       document.getElementById('pickdate').value = (p.getMonth() + 1) + "/" + p.getDate() + "/" + p.getFullYear();
		       if(document.getElementById('dropdate'))
		       document.getElementById('dropdate').value = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
		       var tm = pd[3].split(":");
	       
		       document.getElementById('tHourP').value = tm[0];
		       document.getElementById('tMinP').value = tm[1];
		       
		       if(document.getElementById('dropdate')){
			       document.getElementById('tHourD').value = tm[0];
			       document.getElementById('tMinD').value = tm[1];
		       }
		};
		_setDropOffDate = function(di)
		{
		     if (document.getElementById('inputFieldSF1').value.trim() != "" && document.getElementById(di).value.trim() != "") {
			  pd = document.getElementById('inputFieldSF1').value.trim().split(" ");
			  dd = document.getElementById(di).value.trim().split(" ");
			  var p = new Date();
			  m = _getMonthInt(pd[1].replace(",",""));
			  p.setFullYear(pd[2], m, pd[0]);
			  var d = new Date();
			  m = _getMonthInt(dd[1].replace(",",""));
			  d.setFullYear(dd[2], m, dd[0]);
			  document.getElementById('dropdate').value = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
			  var tm = dd[3].split(":");
			  document.getElementById('tHourD').value = tm[0];
			  document.getElementById('tMinD').value = tm[1];
		     }
		};
		_validate = function(){
		    var chk = false;
		    chk = isFilledText(document.getElementById("inputFieldSF1"), "", "Pickup date can't be left blank.");
		    if(chk == true)
			    chk = isFilledText(document.getElementById("inputFieldSF2"), "", "Drop date can't be left blank.");
		    if(chk){
			    pd = document.getElementById("inputFieldSF1").value.trim().split(" ");
			    if(pd.length < 3){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[2].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[1].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(pd[0].length < 1 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
		    }
		    if(chk && document.getElementById("inputFieldSF2")){
			    dd = document.getElementById("inputFieldSF2").value.trim().split(" ");
			    if(dd.length < 3){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[2].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[1].length < 4 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
			    if(dd[0].length < 1 && chk){
				    alert("Invalid pickup date.");
				    chk = false;
			    }
		    }
		    if (chk)
		    document.getElementById('formBook').submit();
	       };
	       _setMode = function(md){
		if (md == "Hourly") {
			document.getElementById('aHr').className = "selected";
			document.getElementById('aDl').className = "";
		} else {
			document.getElementById('aHr').className = "";
			document.getElementById('aDl').className = "selected";
		}
		document.getElementById('chkPkgType').value = md;
	}
	</script>
</head>    
<body class="body">
	<map name="urlmap">
	<area shape="rect" coords="0,0,497,300" href="http://www.carzonrent.com/" target="_blank" alt="Myles City-Drive">
	<area shape="rect" coords="497,0,1101,300" href="http://m.carzonrent.com/myles/license/" target="_blank" alt="License To Drive">
      </map>
	<?php include_once("../includes/header.php"); ?>
<div class="d100pmain">
<div id="main-container">
	<div class="wrappern-main">
                <div class="wrapper-main">
			<span class="headtxt">INTRODUCING MYLES CITY-DRIVE AT ONLY<span class="rs"><img src="./images/rsb.png" /></span>99/HR*</span>
			<div class="clr"></div>
			<span class="ht23">YOUR LICENSE TO DRIVE YOURSELF</span>
			<div class="clr"></div>
<!--			<span id="mimg" onclick="javascript: this.innerHTML = '<iframe style=\'margin-top:5px;\' width=\'603\' height=\'339\' src=\'//www.youtube.com/embed/cEv-AZFGXVA?rel=0&amp;autoplay=1\' frameborder=\'0\' allowfullscreen></iframe>';this.style.background='none';"></span>-->
			<span id="mimg" style="background: none !important;"><iframe style='margin-top:5px;' width='603' height='339' src='//www.youtube.com/embed/cEv-AZFGXVA?rel=0&amp;autoplay=1' frameborder='0' allowfullscreen></iframe></span>
		</div>
	        <div style="float: left;width:100%">
			<span class="myledv"><img src="./images/banner-combined.jpg" border="0" usemap="#urlmap" /></span>
		</div>
		<div style="float: left;width:98%">
			<span style="float:left;">
				<a href="javascript:void(0);" class="btn bkneno" > Book your Myles City-Drive here</a>
			</span>
		</div>
		<div id='tab1' style="display:block; margin-top: 15px;">
			<div class="sl-main m-0 wd100p">
				<div class="tab1_bg">
					<div class="sl-content">
					      <div class="sl-slide visible">
						   <div class="w40 f_l">
							<div class="img_design"><img src="./images/purple-nano.png" id="carbrandimage" alt="" title=""/></div>
						   </div>
						   <div class="w40 pr3 f_l pt30 greyTxt">
							<!--<div class="xuv"><span id="pkgname">Nano</span><br>
							     <img class="rssp" src="images/rsn.png"/> <span id="carbrandcost">99</span>/hr
							     <!--<div class="hdtdv"><span class="hourtxt">HOURLY</span><span class="dailytxt">DAILY</span></div>-->
							<!--</div>-->
							<div class="holdallbottom">
							<ul>
								<li><a href="javascript: void(0);" id="aDl" onclick="javascript: _setMode('Daily');" class="selected">Daily</a></li>
								<li><a href="javascript: void(0);" id="aHr" onclick="javascript: _setMode('Hourly');">Hourly</a></li>
							</ul>
							</div>
							<div class="clr"></div>
							<div class="h10"> </div>
							<div class="w48 f_l pr2">
							     <input type="text" class="from picktime fbff" id="inputFieldSF1" onchange="javascript: _setDropDateMyles('inputFieldSF1', 'inputFieldSF2');" placeholder="Pick Up Date and Time" readonly="readonly" />
							</div>
							<div class="w50 f_l" id="dropoff">
							     <input type="text" class="from picktime fbff" id="inputFieldSF2" onchange="javascript: _setDropOffDate(this.id);" placeholder="Drop Off Date and Time" readonly="readonly" onchange="javascript: _setDropOffDate(this.id);" />
							</div>
							<div class="w50 f_l" id="nHours" style="display:none;">
							     <input type="text" class="from picktime fbff" id="inputFieldSF2" onchange="javascript: _setDropOffDate(this.id);" placeholder="Drop Off Date and Time" readonly="readonly" onchange="javascript: _setDropOffDate(this.id);" />
							</div>
							<div class="clr"></div>
							<button type="button" class="select button" onclick="javascript: _validate();" style="margin-top: 20px;">Select</button>
						   </div>
						   <div class="w15 f_l pt95 pr2 greyTxt">
							<!--<a href="javascript: void(0);" class="fareD button">Fare details</a>-->
							</div>
							
							<form id="formBook" name="formBook" method="GET" action="../search-result.php">
							<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
							<input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" />
							<input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo date('h:i A');?>" />
							<input type="hidden" name="userTime" id="userTimeSF" value="" />
							<input type="hidden" name="onlyE2C" id="onlyE2C" value="0" />
							<input type="hidden" name="chkPkgType" id="chkPkgType" value="Daily" />
							<input type="hidden" name="customPkg" id="customPkg" value="0" />
							<input type="hidden" name="tHourP" id="tHourP" value="" />
							<input type="hidden" name="tMinP" id="tMinP" value="" />
							<input type="hidden" name="tHourD" id="tHourD" value="" />
							<input type="hidden" name="tMinD" id="tMinD" value="" />
							<input type="hidden" name="pickdate" id="pickdate" value="" />
							<input type="hidden" name="dropdate" id="dropdate" value="" />
							
							<input type="hidden" name="hdOriginName" id="hdOriginName" value="Delhi" />
							<input type="hidden" name="hdOriginID" id="hdOriginID" value="2"/>
							<input type="hidden" name="hdDestinationName" id="hdDestinationName" value="Delhi"/>
							<input type="hidden" name="hdDestinationID" id="hdDestinationID" value="2"/>
							</form>	
						</div>
					</div>
				</div>
			</div>
			<div style="float:left;width:100%;margin:25px 0px 20px 0px">
			    <ul id="mycarnano-1">
				    <li><img src="./images/purple-nano-1.jpg" id="cars-0" onclick="javascript: _setCar(0);"/></li>
				    <li><img src="./images/blue-nano-0.jpg" id="cars-1" onclick="javascript: _setCar(1);"/></li>
				    <li><img src="./images/silver-nano-0.jpg" id="cars-2" onclick="javascript: _setCar(2);"/></li>
				    <li><img src="./images/golden-nano-0.jpg" id="cars-3" onclick="javascript: _setCar(3);"/></li>
			    </ul>
		       </div>
	       </div>
	  </div>
		<div class="dvchkout">
			<div class="bbfb"></div><span style="float:right;width:210px"><a href="http://www.carzonrent.com/" class="btn wd210 tal" >CHECK OUT OTHER<br /> MYLES CARS</a>
			</span>
		</div>
	</div>
	<div style="float: left;"><a href="javascript:void(0)" class="scrollup"></a></div>
</div>	
<div class="clr"></div>
<footer>
	  <div class="main">
	  <div class="f_l w50">
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/aboutus.php">About Us</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/services.php">Our Service</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
		    <!--<li><a href="#">Media</a></li>-->
		    <li><a href="http://careers.carzonrent.com/">Careers</a></li>
	       </ul>
	  </div>
	  <div class="f_r w50 t_a_r">
	       <a href="https://www.facebook.com/carzonrent" target="_blank"><img src="images/facebook.png" border="0" class="mr2"/></a>
	       <a href="https://plus.google.com/+carzonrent" target="_blank"><img src="images/google+.png" border="0" class="mr2"/></a>
	       <a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="images/twitter.png" border="0"/></a>
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/login.php">Account Summary</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
	       </ul>
	       <div class="copyright">
		    Copyright � 2014 Carzonrent India Pvt Ltd. All Rights Reserved.
	       </div>
	  </div>
	  <div class="clr"> </div>
     </div>
     </footer>
	<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
	<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript">
		var j=jQuery.noConflict();
		j('#inputFieldSF1').datetimepicker({
		format:'d M, Y H:i',
		minDate:'d M, Y',
		step:30,
		roundTime:'ceil'
		});
		j('#inputFieldSF2').datetimepicker({
		format:'d M, Y H:i',
		minDate:'d M, Y',
		step:30
		});
		
		j(document).ready(function () {
			window.scrollTo(0, 120);
		    j(window).scroll(function () {
			if (j(this).scrollTop() > 100) {
			    j('.scrollup').fadeIn();
			} else {
			    j('.scrollup').fadeOut();
			}
		    });
		    j('.scrollup').click(function () {
			j("html, body").animate({
			    scrollTop: 0
			}, 1000);
			return false;
		    });
		});
	</script>
	<!-- Facebook Conversion Code for COR FB Pixel -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6019441269186', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019441269186&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>

<?php include_once("../includes/analytic.php"); ?>
<div id='invtrflfloatbtn'></div>
<script>	
var invite_referrals = window.invite_referrals || {}; (function() { 
	invite_referrals.auth = { bid_e : '1B3596545F3AF427193DA8B36E523D4F', bid : '2926', t : '420', orderID : '', email : '' };	
var script = document.createElement('script');script.async = true;
script.src = (document.location.protocol == 'https:' ? "//d11yp7khhhspcr.cloudfront.net" : "//cdn.invitereferrals.com") + '/js/invite-referrals-1.0.js';
var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
</script>
</body>
</html>
<?php
	error_reporting(0);
//	error_reporting(E_ALL);
//	ini_set("display_errors", 1);
	session_start();
	$bookID = "";
	if(isset($_GET["bookid"]) && $_GET["bookid"] != "")
	$bookID = $_GET["bookid"];
	
	if($bookID == "")
	header("Location: http://www.carzonrent.com/");
	
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	include_once("./includes/cache-func.php");
	include_once('./classes/cor.mysql.class.php');
	
	$sql = "SELECT * FROM cor_booking_new WHERE booking_id = " . $bookID;
        $db = new MySqlConnection(CONNSTRING);
	$db->open();
        $booking = $db->query("query", $sql);
        $db->close();
        if(!array_key_exists("response", $booking)){
		$pkgId = $booking[0]["package_id"];
		$catG = $booking[0]["car_cat"];
		$orgName = $booking[0]["origin_name"];
		$ttype = $booking[0]["tour_type"];
		$destName = $booking[0]["destinations_name"];
		$pickDate = $booking[0]["pickup_date"];
		$pickTime = $booking[0]["pickup_time"];
		$dropDate = $booking[0]["drop_date"];
		$address = $booking[0]["address"];
		$fullname = $booking[0]["full_name"]; 
		$totFare = $booking[0]["tot_fare"];
		$payment = $booking[0]["payment_mode"];
		$duration = $booking[0]["duration"];
		$hdDistance = $booking[0]["distance"];
		$cciid = $booking[0]["cciid"];
		$ChauffeurCharges = $booking[0]["chauffeur_charges"];
		$nightstaycharge = $booking[0]["nightstayCharge"];
		$modelName = $booking[0]["model_name"];
		$convCharges = $booking[0]["airportCharges"];
		$pDate = date_create($booking[0]["pickup_date"] . " " . $booking[0]["pickup_time"]);
		$kmRate = $booking[0]["km_rate"];
        $paybackdiscount=$booking[0]["discount_amt"];
		$pickuplocationrecord=$booking[0]["origin_name"];
		
	    $cabpurpose         =$booking[0]["cab_purpose"]; 
		$teminalname        =$booking[0]["terminalname"];
		$desinationname    =$booking[0]["destinations_name"];
		
                
		
		
	} else {
		header("Location: http://www.carzonrent.com/");
	}
	if($booking[0]["tour_type"]=="Outstation" || $booking[0]["tour_type"]=="Local" || $booking[0]["tour_type"]=="airport")
	{
	////////////////////////////////////for insert car model \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		if (isset($_SESSION['last_search_id']) && $_SESSION['last_search_id'] != "") {
			$dataToSave["booking_id"] = $bookID;
			$dataToSave["user_track"] = "Payment Done";
			$whereData["unique_id"] = $_SESSION['last_search_id'];
			$r = $db->update("customer_search", $dataToSave, $whereData);
		}
		$db->close();
	}
	////////////////////////////////////for insert car model \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\		
?>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="googlebot" content="noindex, nofollow">
<meta name="googlebot" content="noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Booking Confirmation - Carzonrent Pvt. Ltd</title>
<?php include_once("./includes/header-css.php"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot ;?>/css/trip.css" />
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot ;?>/css/default-new.css" />
<?php include_once("./includes/header-js.php"); ?>
</head>
<body>
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js" async="true"></script>
<!-------------trip advisor trip details--------------->
<?Php
if(isset($_SESSION['tripadvisor']) && $_SESSION['tripadvisor']!='' && $booking[0]["tour_type"]=="Outstation")
{?>
	<script>
	$(document).ready(function(){
	$('#tripadvisorbtn')[0].click();
	});
	</script>
<?PHP 
} 
else if(isset($_SESSION['tripadvisor']) && $_SESSION['tripadvisor']!='' && isset($_SESSION['tripadvisordestination']) && $_SESSION['tripadvisordestination']!='' && $booking[0]["tour_type"]=="Selfdrive")
{?>
	<script>
	$(document).ready(function(){
	$('#tripadvisorbtn')[0].click();
	});
	</script>
<?PHP
}
?>
<input type="hidden"  onclick="tour_advisor();"  id="tripadvisorbtn"/>
<!-------------trip advisor end----------->
<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
	if($ttype == "Outstation" || $ttype == "Local"  || $ttype == "airport"){
?>
	<style>
		.showtime, .showhour {
			background: none repeat scroll 0 0 #FAEAA3 !important;
			border: 1px solid #EFC14D !important;
			border-radius: 4px 4px 4px 4px;
			color: #D68300 !important;
		}
		#registration .submit a{background: url("<?php echo corWebRoot; ?>/images/submit.jpg") no-repeat scroll 0 0 transparent !important;}
	</style>
<?php
	} elseif(trim($ttype) == "Selfdrive"){
?>
	<style>
		.tripdetails h3 {color: #db4626 !important;}
		.tripdetails ul li {background: url("<?php echo corWebRoot; ?>/images/libg-r.jpg") no-repeat scroll left 3px rgba(0, 0, 0, 0) !important;}
		#registration{border: 10px solid #db4626 !important;}
		.tbbingouter.yellowstrip {
		    background: none repeat scroll 0 0 #db4626;
		    border-bottom: medium none;
		}
	</style>
<?php
	}
?>
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<!--<div class="tbbingouter yellowstrip">
  <div class="main">
    <ul class="myaccount_tab">
    	<li>You booking has been confirmed</li>
    </ul>
  </div>
</div>-->
<div class="clr"></div>
<?php
	
	$bookID = $_GET["bookid"];
	
	if($orgName != "")
	$orgNames = explode(",", $orgName);
	if($destName != "")
	$destNames = explode(",", $destName);
	
	$ll = "";
	$dll = "";
	$travelData = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
	$points = count($travelData->{'routes'}[0]->{'legs'});
	$ll = $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} . "," . $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'};
	$dll = ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lat'}) . "," . ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lng'});
	for($i = 0; $i < $points; $i++){
		$dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
		if($i == 0)
			$dwstate = $dispOrg;
		$dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
		if($dwstate == "")
			$dwstate = $dispDest;
		else
			$dwstate .= " to:" . $dispDest;
		
	}
	//echo $_SESSION["rurl"];
?>
<!--Middle Start Here-->
<div class="main">

<?php if($ttype == "Outstation" || $ttype == "Local" || $ttype == "airport") { ?>
<div class="myprofile">
	<div class="heading">Trip details <span id="msg">
	<!---<a href="javascript:void(0)" onclick="javascript: _cancelBooking('<?php echo $bookID; ?>', '<?php echo $cciid; ?>');">Cancel Booking</a>---></span></div>
	<div class="tripdetails">  
	
	<p style="padding:0px !important;"><strong>Booking ID:</strong> <?php echo $bookID; ?></p>
	<p style="padding:0px !important;"><strong>Car Type:</strong> <?php echo $carG; ?> - <?php echo $modelName; ?></p>
	<?php
		if(trim($booking[0]["origin_name"]) == "Goa"){
	?>
		      <p style="padding:0px !important;">Please note that the car in Goa will be provided through a partner.</p>
	<?php
	        }
	?>
          <p style="padding:0px !important;"><strong>From:</strong> <?php echo $orgName; ?></p>
          <p style="padding:0px !important;"><strong>Service:</strong> <?php echo $ttype;?> </p>

		     <p style="padding:0px !important;"><strong>Pickup Location:</strong>  
			 
			 <?php 
			 
			 if($cabpurpose==1 || $cabpurpose==2) 
			 {
				echo $teminalname; 
			    
			 } 
			
			 else 
			  {
			  echo $_SESSION["subLocName"];
			 }
			 ?> 
			 
			 </p>
	
          <p style="padding:0px !important;"><strong>Destination:</strong> <?php echo $destName; ?></p>
          <p style="padding:0px !important;"><strong>Pickup Date:</strong> <?php echo $pDate->format('Y-m-d'); ?></p>
          <p style="padding:0px !important;"><strong>Pickup Time:</strong> <?php echo $pDate->format('is') . " HRS"; ?></p>
	<p style="padding:0px !important;"><strong>Drop Date:</strong> <?php echo $dropDate; ?></p>
          <p style="padding:0px !important;"><strong>Address:</strong> <?php echo $address; ?></p>
          
          
      <h3>Round Trip Fare<br />
        <?php
		if($ttype == "Selfdrive"){
			$totFare = intval($booking[0]["BasicAmt"] + $booking[0]["additional_srv_amt"]);
	?>
		<span style="float:left">Rs <?php echo ceil($totFare + ($totFare * $booking[0]["vat"]/100)); ?>/- </span><span style="font-size:11px;margin:10px 0px 0px 35px;display:block;width:auto;float:left;font-weight:normal;color:#666666;">(Including VAT)</span></h3>
	<?php		
		} else {
	?>
		<span style="float:left">Rs <?php echo intval($totFare); ?>/- </span><span style="font-size:11px;margin:10px 0px 0px 35px;display:block;width:auto;float:left;font-weight:normal;color:#666666;">(Including GST)</span></h3>
	<?php
		}
		$payMode = $payment;
		$payModeName = "Pay Online";
		switch($payMode){
			case 1:
			$payModeName = "Pay Online";
			break;
			case 2:
			$payModeName = "Pay to our cash collection agency";
			break;
			case 5:
			$payModeName = "Pay to the driver at the start of journey";
			break;
		}
		if($payModeName != ""){
	?>
		<p style="padding:0px !important;"><strong>Payment Mode:</strong> <?php echo $payModeName;?></p>
	<?php
		}
	?>
          <p style="padding:0px !important;"><strong>Fare Details</strong></p>
	  <div class="clr"></div>
	<?php if($ttype == "Selfdrive") {
		
		?>
	  <ul>
		<li>Minimum Billing: Rs <span id="bsamt"><?php echo intval($booking[0]["BasicAmt"]); ?></span>/-</li>
		<li>Additional Services Cost: Rs <span id="asc"><?php echo intval($booking[0]["additional_srv_amt"]); ?></span>/-</li>
		<li>Sub Total: Rs <span id="subt"><?php echo $totFare; ?></span>/-</li>
		<li>VAT (@<?php echo number_format($booking[0]["vat"], 3); ?>%): Rs <span id="vatt"><?php echo intval(ceil(($totFare * $booking[0]["vat"]) /100)); ?></span>/-</li>
		<li>Total Fare: Rs <span id="tfare"><?php echo  ceil($totFare + ($totFare * $booking[0]["vat"]/100)); ?></span>/-</li>
	<?php
		if(trim($booking[0]["origin_name"]) != "Goa"){
	?>
		<li>Refundable Security Deposit  (Pre Auth from card): Rs <?php echo intval($booking[0]["sec_deposit"]); ?> (Mastercard and Visa only)</li>
	<?php
		} else {
	?>
		<li>The billing cycle starts from 8am each day</li>
		<li>Refundable Security Deposit: Rs <?php echo intval($booking[0]["sec_deposit"]); ?> (To be paid in cash before the start of the journey)</li>
		<li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
		<li>The vehicle is to be driven within the permissible limits of Goa.</li>
	<?php
		}
	?>
	</ul>
	<p><strong>Mandatory Documents(Original)</strong></p>
	<div style="clear: both"></div>
	<?php
		if(trim($booking[0]["origin_name"]) != "Goa"){
	?>
	<ul>
		<li>Passport / Voter ID Card</li>
		<li>Driving License</li>
		<li>Credit Card</li>
	</ul>
	<?php
		} else {
	?>
	<ul>
		<li>Passport / Voter ID Card</li>
		<li>Driving License</li>
		<li>Any of the following has to be submitted in original as an identity proof.
			<ol type="1">
				<li>Adhaar card</li>
				<li>Pan card</li>
				<li>Voter ID card</li>
			</ol>
		</li>
	</ul>
	<?php
		}
	?>
	<?php } ?>
   <?php if($ttype == "Outstation") { ?>
      <ul>
	<?php if(intval($dayRate) && intval($kmRate)) { ?>
	<?php } ?>
	<?php
		$kmPerDay = intval(($hdDistance)/$duration);
		if($kmPerDay < 250)
		{
		//	$totAmount = intval(250 * $duration * $kmRate);
			$displayKMS = 250;
		}
		else
		{
	//		$totAmount = intval($kmPerDay * $duration * $kmRate);
			$displayKMS = intval($kmPerDay * $_POST["duration"]);
		}
	?>
	  </ul>
	  <p style="padding:0px !important;width:100%;float:left"><strong>Includes</strong></p>
	  <ul>
		<?php
			if($kmPerDay < 250)
			{
		?>
		<li style="float:left !important;width:100%"><?php echo $displayKMS; ?> * <?php echo $duration; ?> = <?php echo intval($displayKMS*$duration); ?> Kms</li>
		<?php
			}
			else
			{
		?>
		<li style="float:left !important;width:100%"><?php echo $hdDistance; ?> Kms</li>
		<?php
			}
		?>
		<li style="float:left !important;width:100%">Per Km charge = Rs.<?php echo intval($kmRate); ?>/-</li>
		<li style="float:left !important;width:100%">Number of days = <?php echo $duration; ?> day(s)</li>
		<li style="float:left !important;width:100%">Chauffeur per day charge = Rs.<?php echo $ChauffeurCharges; ?>/-</li>
		<li style="float:left !important;width:100%">Chauffeur charge = Rs.<?php echo $ChauffeurCharges; ?> * <?php echo $duration; ?></li>
		<li style="float:left !important;width:100%">Night Stay Allowance Charge =Rs. <?php echo number_format($nightstaycharge); ?></li>
		
		<li style="float:left !important;width:100%">Minimum billable kms per day = 250 kms</li>
		<li style="float:left !important;width:100%">GST</li>
	</ul>
	<p style="padding:0px !important;width:100%"><strong>Extra Charges</strong></p>
	<ul>
		<li style="float:left !important;width:100%">Tolls, parking and state permits as per actuals</li>
		<?php
			if($kmPerDay < 250)
			{
		?>
		<li style="float:left !important;width:100%">Extra Km beyond <?php echo intval($displayKMS * $duration); ?> =  Rs.<?php echo intval($kmRate); ?>/km</li>
		<?php
			}
			else
			{
		?>
		<li style="float:left !important;width:100%">Extra Km beyond <?php echo $hdDistance; ?> kms =  Rs.<?php echo intval($kmRate); ?>/km</li>
		<?php
			}
		?>
	</ul>
<?php   
	}
	
	if($ttype == "airport" ){
?>
	<ul>
	
		<li>Rs <?php echo intval($totFare+$paybackdiscount); ?> minimum billing</li>
		<li>GST & chauffeur charge inclusive in the fixed rate</li>
		<li>Fixed charge is applicable only for one way point to point transfer</li>
		<li>No halt or deviation allowed in between the journey.</li>
		<li>Chauffeur will decide which route to take to destination.</li>
		<li>Toll, Parking, & State tax (If any) to be paid  extra as per actual.</li>
		
	</ul>
<?php
	}
	
	
if($ttype == "Local"){
?>
	<ul>
	<?php
		if(intval($dayRate) && intval($PkgHrs)) { ?>
		  <li>Rate: Rs <?php echo intval($dayRate / $PkgHrs); ?> / hour</li>
	<?php } ?>
		<li>10 km included for every hour of travel</li>
		<li>Rs <?php echo  $kmRate;//($dayRate / ($PkgHrs * 10)); ?> / km for additional km</li>
		<li>Rs <?php echo $totFare; ?> minimum billing</li>
		<li>New hour billing starts when usage more than 30 mins</li>
		<li>Toll and parking extra</li>
		<li>Chauffeur charges and GST included</li>
		<?php if($_GET["hdOriginID"] == 2) { ?>
		<?php
			$charge = 200;
			if($_GET["hdCat"] == 'Van' || $_GET["hdCat"] == '4 Wheel Drive' || $_GET["hdCat"] == 'Full Size'){
				$charge = 300;
			}
		?>
			<li>In case of travel to Noida or Ghaziabad, UP state permit of Rs <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
		<?php }
			if($_GET["hdOriginID"] == 7){
		?>
			<li>Additional Rs <?php echo (20 * ($dayRate / ($PkgHrs * 10))); ?> to be paid in case of pick up or drop from airport.</li>
		<?php } 
			if($_GET["hdOriginID"] == 2 || $_GET["hdOriginID"] == 11 || $_GET["hdOriginID"] == 3){
		?>
			<li>Additional Rs <?php echo (20 * ($dayRate / ($PkgHrs * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
		<?php } ?>
	</ul>
<?php
	}
	setcookie('kword', "", time()-3600 , "/");
	setcookie('gclid', "", time()-3600 , "/");
?>
</div>
<input type="hidden" name="hdBookingID" id="hdBookingID" value="<?php echo $bookID; ?>" />
<input type="hidden" name="hdCCIID" id="hdCCIID" value="<?php echo $cciid; ?>" />
<div class="tripsmap">
	<iframe width="439" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=<?php echo urlencode($orgName); ?>&amp;daddr=<?php echo urlencode($dwstate); ?>&amp;hl=en&amp;aq=1&amp;oq=<?php echo urlencode($orgName); ?>&amp;sll=<?php echo $ll; ?>&amp;mra=ls&amp;ie=UTF8&amp;ll=<?php echo $ll; ?>&amp;t=m&amp;z=4&amp;iwloc=addr&amp;output=embed"></iframe>
</div>
</div>
<?php } elseif(trim($ttype) == "Selfdrive") { ?>
<div id="trip">
<div class="trip_top">
<div class="f_l"><strong>Trip details</strong></div>
<div class="f_r"><!---<a class="Cbooking" href="javascript:void(0)" onclick="javascript: _cancelBooking('<?php echo $bookID; ?>', '<?php echo $cciid; ?>');">Cancel Booking</a>---></div>
</div>
<div class="w96 ptb_lr_1_2">
<div class="w68 mr2 f_l">
<span class="nameI">Hey <?php echo $fullname; ?>,</span>
<p>Your <?php echo strtoupper($modelName); ?> is booked and ready to hit the road. Here&#8217;s your booking confirmation. Feel free to get in touch with us at anytime, day or night.</p>
<div class="bh4"> </div>
<p>
Booking ID: <?php echo $bookID; ?> <br>
Car Type: <?php echo $modelName; ?> <br>
From: <?php echo $orgName; ?> <br>
Service: <?php echo $ttype;?> <br>
Destination: <?php echo $orgName; ?> <br>
Pickup Date: <?php echo $pDate->format('Y-m-d'); ?> <br>
Pickup Time: <?php echo $pDate->format('is') . " HRS"; ?> <br>
Drop Date: <?php echo $dropDate; ?> <br>
Pickup Location: <?php echo $_SESSION["subLocName"];?>
</p>
<?php


	//$totFare = intval($booking[0]["BasicAmt"] + $booking[0]["additional_srv_amt"] + $booking[0]["subLocCost"] + $booking[0]["airportCharges"] - $booking[0]["discount_amt"]);
$totFare = $booking[0]['User_billable_amount'];
?>
<span class="redT">Fare Details</span> <br>
<span class="fs20">Rs. <?php echo $totFare;
//ceil($totFare + ($totFare * $booking[0]["vat"]/100)); ?>/-</span> (Including VAT) <br>
Payment Mode: Pay Online <br>
<strong>Fee Details</strong>
<ul class="trip_arrow">
<li>Minimum Billing: Rs. <?php echo intval($booking[0]["BasicAmt"]); ?>/-</li>
<li>Additional Serive Cost: Rs. <?php echo intval($booking[0]["additional_srv_amt"]); ?>/-</li>
<li>Sub Location Cost: Rs. <?php echo intval($booking[0]["subLocCost"]); ?>/-</li>
<li>Convenience Charge: Rs. <?php echo intval($booking[0]["airportCharges"]); ?>/-</li>
<li>Discount Amount: Rs. <?php echo intval($booking[0]["discount_amt"]); ?>/-</li>
<li>Sub Total: Rs. <?php echo ($booking[0]["BasicAmt"]+$booking[0]["additional_srv_amt"]+$booking[0]["subLocCost"]+$booking[0]["airportCharges"])-$booking[0]["discount_amt"]; ?>/-</li>
</ul>
VAT(@<?php echo number_format($booking[0]["vat"], 3); ?>%): Rs. <?php echo intval(ceil(($totFare * $booking[0]["vat"]) /100)); ?>/- <br>
Total Fare: Rs. <?php echo $totFare;

//ceil($totFare + ($totFare * $booking[0]["vat"]/100)); ?>/-
</div>
<div class="w30 f_l">
<img class="mb25" border="0" src="<?php echo corWebRoot; ?>/images/add1.jpg"/>
<div class="add2 mb25 relative">
<div class="tripF"><a href="https://www.facebook.com/Mylescars" target="_blank"><img class="mb5" border="0" src="<?php echo corWebRoot; ?>/images/facebookT.jpg"/></a></div>
<div class="tripT"><a href="https://twitter.com/MylesCars" target="_blank"><img class="mb5" border="0" src="<?php echo corWebRoot; ?>/images/twitterT.jpg"/></a></div>
</div>
<a href="http://www.carzonrent.com/referralprogram/" target="_blank"><img class="mb25" border="0" src="<?php echo corWebRoot; ?>/images/add3.jpg"/></a>
</div>
</div>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
<!-- Google Code for COR_Conversion_Nov&#39;14 Conversion Page --> <script type="text/javascript"> /* <![CDATA[ */ var google_conversion_id = 960024148; var google_conversion_language = "en"; var google_conversion_format = "3"; var google_conversion_color = "ffffff"; var google_conversion_label = "Pl07CO3A4FcQ1JzjyQM"; var google_remarketing_only = false; /* ]]> */ </script> <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> </script> <noscript> <div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/960024148/?label=Pl07CO3A4FcQ1JzjyQM&amp;guid=ON&amp;script=0"/> </div> </noscript>



<!-- cashkaro Offer Conversion: Mylescars  -->
<iframe src="http://track.techtrack.in/aff_l?offer_id=273&adv_sub=<?php echo $bookID; ?>&amount=<?php echo $totFare; ?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<!-- // End Offer Conversion --> 

<?php } ?>
</div>
<!--footer Start Here-->
<?php include_once("./includes/footer.php"); ?>
<!-- Google Code for Carzonrent (India) Pvt. Ltd (updated 22 may 2015 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957885192;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "Ad-ICKi-5QkQiNbgyAM";
var google_conversion_value = 1.00;
var google_conversion_currency = "INR";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/957885192/?value=1.00&amp;currency_code=INR&amp;label=Ad-ICKi-5QkQiNbgyAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<img src="<?php 
	$bid = '2926'; 
	$bid_e = '1B3596545F3AF427193DA8B36E523D4F';
        $t = '420';
	$orderID = $bookID; //set order id
        $purchaseValue = ceil($totFare + ($totFare * $booking[0]["vat"]/100));
        $email = $booking[0]["email_id"]; //set user email
        $mobile = $booking[0]["mobile"]; //mobile number of user
        $userParams = urlencode(json_encode(array('fname'=>'$booking[0]["full_name"]','lname'=>''))); //user information
        $userCustomParams = urlencode(json_encode(array('customValue'=>'')));
	$http_val = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
	print $http_val.'www.ref-r.com/campaign/t1/settings?bid_e='.$bid_e.'&bid='.$bid.'&t='.$t.'&email='.$email.'&orderID='.$orderID.'&purchaseValue='.$purchaseValue.'&mobile='.$mobile.'&userParams='.$userParams.'&userCustomParams='.$userCustomParams;
?>" />
<!-- Facebook Conversion Code for COR FB Pixel -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6019441269186', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019441269186&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>


<!-- Facebook Conversion Code for Myles Pixel May-June 2015 Campaign -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032900221786', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032900221786&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>
<!---------------facebook conversion code viajy- dont remove without confirmation 3agust2015>



<!-- Facebook Conversion Code for Checkouts - COR 1 intractive Avenue 20/08/15 agust -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6029509175200', {'value':'0.00','currency':'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029509175200&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" /></noscript>
<!-- Facebook Conversion Code for Checkouts - COR 1 intractive Avenue 20/08/15 agust end -->

<!-------------trip advisor trip details--------------->
 <script>
  function tour_advisor()
   {
		var emailaddress= '<?PHP echo $booking[0]["email_id"];?>';
		//var emailaddress= 'singhalok4it@gmail.com';
		<?Php
		if($booking[0]["tour_type"]=='Outstation')
		{?>
		var locationid= '<?Php echo $booking[0]["destinations_id"];?>';
		var destinations_name = '<?PHP echo $booking[0]["destinations_name"];?>';
		<?Php } else {?>
		var locationid= '<?Php echo $_SESSION['tripadvisordestinationid'];?>';
		var destinations_name = '<?PHP echo $_SESSION['tripadvisordestination'];?>';
		<?Php } ?>
		var tourtype= '<?Php echo $booking[0]["tour_type"];?>';
		var booking_id = '<?PHP echo $booking[0]["booking_id"];?>';
		var full_name = '<?PHP echo $booking[0]["full_name"];?>';
		var origin_name = '<?PHP echo $booking[0]["origin_name"];?>';
		$.ajax({
		type: 'POST',
		url: '<?PHP echo corWebRoot ;?>/tripadvisor.php',
		data: {locationid:locationid,tourtype:tourtype,emailaddress:emailaddress,booking_id:booking_id,full_name:full_name,destinations_name:destinations_name,origin_name:origin_name,action:'TripAdvisor'},
		success: function(response){
		console.log(response);
      }
    });
}
</script>
<!-------------trip advisor trip details--------------->
<?Php
if($_SESSION['utm_source']=='Affiliate-Payoom-COR' && $_SESSION['utm_medium']=='Affiliate' && $_SESSION['utm_campaign']=='Carzonrent-Affiliate-Payoom')
{?>
<!-- Offer Conversion: Carzonerent - CPS -->
<iframe src="https://payoom.go2cloud.org/aff_l?offer_id=1194&adv_sub=<?php echo $bookID;?>&amount=<?php echo $totFare;?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<!-- // End Offer Conversion -->
<?php 
} 
else if($_SESSION['utm_source']=='Optimidea-Affiliate' && $_SESSION['utm_medium']=='Optimidea-Network' && $_SESSION['utm_campaign']=='Optimidea-Affiliate-Campaign')
{ ?>
<img src="http://optimidea.go2cloud.org/aff_l?offer_id=106&adv_sub=<?php echo $bookID;?>&amount=<?php echo $totFare;?>" width="1" height="1" />
<?php
} 
else if($_SESSION['utm_source']=='Clickonik-COR' && $_SESSION['utm_medium']=='Clickonik-CPC' && $_SESSION['utm_campaign']=='Clickonik-COR-CPC')
{?>
<iframe src="https://clickonik.go2cloud.org/aff_l?offer_id=14&adv_sub=<?php echo $bookID;?>&amount=<?php echo $totFare;?>" scrolling="no" frameborder="0" width="1" height="1"></iframe> 
 <?Php
 }
 ?>
</body>
</html>
<?php
    error_reporting(0);
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    include_once('./classes/cor.ws.class.php');
    include_once('./classes/cor.xmlparser.class.php');
    include_once("./cache-func.php");
    include_once('./classes/cor.mysql.class.php');
    setcookie("tcciid", "", time()-60);
    $cor = new COR();
    $res = $cor->_CORGetCities();	
    $myXML = new CORXMLList();
    $org = $myXML->xml2ary($res);
    $orgH = $org;
    $orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    $orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    $orgH[] = array("CityID" => 39, "CityName" => "Manesar");
    $orgH[] = array("CityID" => 41, "CityName" => "Bhubaneswar");
    $res = $cor->_CORGetDestinations();
    $des = $myXML->xml2ary($res);
	
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php session_start();

if(isset($_POST['Submit']))
{
	if(empty($_SESSION['captcha_code'] ) || strcasecmp($_SESSION['captcha_code'], $_REQUEST['captcha_code']) != 0)
	{  
	
		
		$msg="<span style='color:red'>The Validation code does not match!</span>";	
	}
	else
	{	

		$msg="<span style='color:green'>The Validation code has been matched.</span>";
		$dataToSave = array();
		$dataToSave['origin_name']=strip_tags($_REQUEST['hdOriginName']);
		$dataToSave['origin_id']=strip_tags($_REQUEST['hdOriginID']);
		$dataToSave['destination_name']=strip_tags($_REQUEST['hdDestinationName']);
		$dataToSave['destination_id']=strip_tags($_REQUEST['hdDestinationID']);
		$dataToSave['tour_type']=strip_tags($_REQUEST['hdTourtype']);
		$dataToSave['package_type']=strip_tags($_REQUEST['chkPkgType']);
		$pickDate = date_create(date('Y-m-d', strtotime($_REQUEST['pickdate'])));
		$dropdate = date_create(date('Y-m-d', strtotime($_REQUEST['dropdate'])));
		
		$dataToSave['pickup_date']= $pickDate->format('Y-m-d');
		$dataToSave['drop_date']=$dropdate->format('Y-m-d');
		$dataToSave['mobile']=strip_tags($_REQUEST['mobile']);
		if($_REQUEST['email']!='Email')
		{
		$dataToSave['email']=strip_tags($_REQUEST['email']);
		}
		$dataToSave['comment']=strip_tags($_REQUEST['comment']);
		$dataToSave['entry_date']=date('Y-m-d h:m:s');

		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$r = $db->insert("cor_leading", $dataToSave);
		if($r)
		{
			
		$subject="Carzonrent Lead";
		$href='http://www.carzonrent.com/';
		$logo='http://www.carzonrent.com/images/COR-logo.gif';
		$alt='Carzonrent logo';
		$title='Carzonrent.com';
		
		$msg='<table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:#666 solid 1px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal;"><tr><td colspan="2" bgcolor="#666666"></td></tr><tr><td style="border-bottom:#666 solid 1px; background:#999999"><a href="'.$href.'" target="_blank"> <img src="'.$logo.'" width="150" height="60" border="0" alt="'.$alt.'" title="'.$title.'"/> </a></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p> <strong>Enquiry Details Website - Carzonrent</strong></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Starting City</b>: '.$_POST["hdOriginName"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Destination City</b>: '.$_POST["hdDestinationName"].'</b></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Date of Travel</b>: '.$dataToSave['pickup_date'].'</b></p></td></tr>
		
		<tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Date of Return</b>: '.$dataToSave['drop_date'].'</b></p></td></tr>
		<tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Mobile</b>: '.$_REQUEST['mobile'].'</b></p></td></tr>
		 <tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Email</b>: '.$_POST["email"].'</b></p><p style="padding-left:20px;"></p></td></tr>
		 <tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><p style="padding-left:20px;"><b>Comment</b>: '.$_POST["comment"].'</b></p><p style="padding-left:20px;"></p></td></tr><tr><td bgcolor="#F1F1F1" style="padding-left:20px;"><br/><br/></td></tr></table>';
		
		/********* Email code**********/
		$dataToMail["To"] = "vinod.maurya@carzonrent.com;vijay.sharma@carzonrent.com;ashutosh.singh@mylescars.com;khyati.mehta@carzonrent.com;trips@carzonrent.com";
		//$dataToMail["To"] = "ashutosh.singh@mylescars.com;khyati.mehta@carzonrent.com;trips@carzonrent.com";
        $dataToMail["MailBody"] = $msg;
        $dataToMail["Subject"] = "Carzonrent Lead";
        $res = $cor->_CORSendMail($dataToMail);
		
		unset($dataToMail);
			
			
			
			

		$result= "Thank you for your query. We will respond to your query as soon as possible.";
		$_SESSION['msgthnk']=$result;
		header("Location:leadthanks.php");
		

		}
		
		
        
		unset($dataToSave);
		
	}
	
	
	
	
}	
?>
<script type='text/javascript'>
function refreshCaptcha(){
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>


<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js-no-ga.php"); ?>
<script type="text/javascript">
	$(function(){
		$('select.styled').customSelect();
		$('#slidesOutstation').slides({
			preload: false,
			preloadImage: '/images/loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(current){
			
				$('.caption').animate({
					bottom:-35
				},100);
			},
			animationComplete: function(current){
			
				$('.caption').animate({
					bottom:0
				},200);
			},
			slidesLoaded: function() {
			
				$('.caption').animate({
					bottom:0
				},200);
			}
		});
	});

	arrOrigin = new Array();
<?php
	for($i = 0; $i < count($org); $i++){
?>
		arrOrigin[<?php echo $i; ?>] = new Array("<?php echo $org[$i]['CityName'] ?>", <?php echo $org[$i]['CityID'] ?>);
<?php
	}
?>
	arrDestination = new Array();
<?php
	for($i = 0; $i < count($des); $i++){
?>
		arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
<?php
	}
?>
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}
</script>

<!--  For Calender -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/captchstyle.css" />


<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

$(function() {
	var dateToday = new Date();
	dateToday.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
	$( ".datepicker" ).datepicker({minDate: dateToday});
});
</script>
</head>
<body>
<?php include_once("./includes/leadheader.php"); ?>
<script type="text/javascript">var _kiq = _kiq || [];</script>

<?php
	if(isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != ""){
?>
<script type="text/javascript" charset="utf-8">
_kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
</script>
<?php
	}
?>

		<div class="banner marginTop >
    	<div class="lfts">  
        <div id="TripleTabs">
		<div class=" leadout">
		<div  class="bookout">Book a Cab for Outstation</div>
		</div>
		
		<div id="outstationFrm" class="tber NewFormDesign">
		<input type="hidden" name="hdToday" id="hdToday" value="<?php echo date('d M, Y');?>" />
        <form id="form1" name="form1" action="" method="post" target="_top">
		<input type="hidden" name="hdOriginName" id="hdOriginName" />
		<input type="hidden" name="hdOriginID" id="hdOriginID" />
		<input type="hidden" name="hdDestinationName" id="hdDestinationName" />
		<input type="hidden" name="hdDestinationID" id="hdDestinationID" />
		<input type="hidden" name="hdTourtype" id="hdTourtype" value="Outstation" />
		<input type="hidden" name="chkPkgType" id="chkPkgType" value="Daily" />
		<div class="bdrbtm">
		<?php include_once("./includes/m-city-popup.php"); ?> 

						
						
					
						
                        <p>
                            <label>Starting From <span class="red">*</span></label>
	                        <span class="location">
                                <select name="ddlOrigin" id="ddlOrigin" class="styled" onchange="javascript: _setOrigin(this.id, 'o');">
                                    <option value="">Select City</option>
                                    <?php
                                    $selCityId = 2;
                                    $selCityName = "Delhi";
                                    for($i = 0; $i < count($org); $i++)
                                    {
                                        if($org[$i]["CityName"] == $cCity){
                                            $selCityId = $org[$i]["CityID"];
                                            $selCityName = $org[$i]["CityName"];
                                            ?>
                                            <option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
                                            <script type="text/javascript" language="javascript">
                                            document.getElementById('hdOriginID').value = '<?php echo $org[$i]["CityID"]; ?>';
                                            document.getElementById('hdOriginName').value = '<?php echo $org[$i]["CityName"]; ?>';
                                            </script>
                                            <?php
                                        }
                                        else {
                                            ?>
                                            <option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </span>
                        </p>
                        <p>
				<label>Travelling to <span class="red">*</span></label>
				<span class="cities">
					<input type="text" name="txtDestination" id="txtDestination" value="" autocomplete="off" onkeyup="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest', arrDestination, 0, 0, 0);" />
					<div id="autosuggest" class="autosuggest autosuggestNew floatingDiv"></div>
				</span>
                        </p>
                    </div>
                    <p>
                        <label>Date of Travel <span class="red">*</span></label>
                        <span class="datepick">
				<input type="text" size="12" autocomplete="off" name="pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField" class="datepicker" onchange="javascript: _setDropDate('inputField', 'inputField2');" />
                        </span>	
                    </p>
                    <p>
                        <label>Date of Return <span class="red">*</span></label>
	                    <span class="datepick">
	                        <input type="text" size="12" autocomplete="off" name="dropdate" value="<?php echo date('d M, Y'); ?>" id="inputField2" class="datepicker" />
	                    </span>
                    </p>
					
					 <p>
                        <label>Mobile <span class="red">*</span></label>
	                    <span class="cities">
	                        <input type="text" size="12" autocomplete="off" name="mobile"  onkeypress="javascript: return _allowNumeric(event);" value="10 digit mobile number" onblur="if (value == '')
                                                    value = '10 digit mobile number'" onfocus="if (value == '10 digit mobile number')
                                                                value = ''" value="<?php echo $_REQUEST['mobile'];?>" id="mobile"  />
	                    </span>
                    </p>
					
					
					 <p>
                        <label>Email  </label>
	                    <span class="cities">
	                        <input type="text" size="12" autocomplete="off" name="email"  value="Email" onblur="if (value == '')
                                                    value = 'Email'" onfocus="if (value == 'Email')
                                                                value = ''" value="<?php echo $_REQUEST['email'];?>" id="email"     />
	                    </span>
                    </p>
					
					
					 <p>
                        <label>Comment</label>
	                    <span class="resp resp">
	                        <textarea id="comment" name="comment" rows="3" value="" cols="60"><?php echo $_REQUEST['comment'];?></textarea>
	                    </span>
                    </p>
				
			
					
					  <p>
						  <label>Validation code:</label> 
						  <span class="paddingTop captcha_re">						  
							<img src="captchamain.php?rand=<?php echo rand();?>" id='captchaimg'> 
							<a href='javascript: refreshCaptcha();'><img src="<?php echo corWebRoot; ?>/images/refresh.png"></a>
						  </span>
						 


							
					  </p>
					  <!--<p>
						<label></label>
						
						</p>-->
					  <p>
					  <label></label>
					  
					  <?php if(isset($msg)){?>

							<span  colspan="2" align="center" valign="top"><?php echo $msg;?></span>

							<?php } ?>
							
							</p>
					  <p>
						<label for='message'>Enter the code above here :</label>
					   
						<span class="cities">
						<input id="captcha_code" name="captcha_code" type="text" class="cities">
						</span>
						</p>
						
					
					  <p>
						<label></label>
					  <span class="mkebtnNew"><input name="Submit" type="submit" onclick="return _validatelead();" value="Submit" class="button1"></span>
					
				  </p>
					  
				
			
			
                </form>
				<div class="clr"></div>
            </div>
        </div>
        <!--TripleTabs Ends Here-->
        </div>
    
</div>
<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQCJKN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-WQCJKN');</script>

<!-- End Google Tag Manager -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-22195130-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
<?php
    error_reporting(0);
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache"); 
?> 
<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
	setcookie("tcciid", "", time()-60);
	$cor = new COR();
	$res = $cor->_CORGetCities();	
	$myXML = new CORXMLList();
	$org = $myXML->xml2ary($res);
	//$org[] = array("CityID" => 11, "CityName" => "Ghaziabad");
	//$org[] = array("CityID" => 3, "CityName" => "Faridabad");
	//print_r($org);
	$res = $cor->_CORGetDestinations();
	$des = $myXML->xml2ary($res);
	
	if(isset($_GET["gclid"]) && $_GET["gclid"] != "") 
		setcookie('gclid',$_GET["gclid"], time()+3600*24 , "/");

	$bookingID = "";
	if(isset($_GET["bookingid"]) && $_GET["bookingid"] != "")
	    $bookingID =  $_GET["bookingid"];
	$ref = "";
	if(isset($_GET["ref"]) && $_GET["ref"] != "")
	    $ref =  $_GET["ref"];
	
	$cor = new COR();
	
	/* Decrypt REF parameter */
	if($ref != ""){
		$data = array();
		$data["data"] = $ref;
		$res = $cor->_CORDecrypt($data);
		$ref = $res->{'DecryptResult'};
		unset($data);
	}
	
	/* Decrypt REF parameter */
	
	/* Decrypt BOOKING ID parameter */
	if(strtolower($ref) == "honeywell"){
		$data = array();
		$data["data"] = $bookingID;
		$res = $cor->_CORDecrypt($data);
		$bookingID = $res->{'DecryptResult'};
		unset($data);
	}
	$res = $cor->_CORGetBookingsDetail(array("BookingID"=>$bookingID));
	$bookingDetails = array();
	if($res->{'GetBookingDetailResult'}->{'any'} != ""){
		$myXML = new CORXMLList();
		$bookingDetails = $myXML->xml2ary($res->{'GetBookingDetailResult'}->{'any'});
	}
	/*echo "<pre>";
	print_r($bookingDetails);*/

	$pickDate = date_create($bookingDetails[0]["PickUpDate"]);
	$pickDate->format("Y-m-d");
	$dropDate = date_create($bookingDetails[0]["DropOffDate"]);
	$dropDate->format("Y-m-d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="dc4es0xCEm_yWgOSy8l4x8VOx5SN8J0jw8RTbOkKf-g" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if(trim($cCity) != "") { ?>
	<?php if(trim($cCity) == "Delhi") { ?>
		<title>Car Hire Company | Car Rental Services in <?php echo ucwords($cCity); ?> - Carzonrent</title>
		<meta name="description" content="Carzonrent provides India's best and cheapest car rental & hire services for travel in Delhi. Book a cab in Delhi online on carzonrent.com. call now 0888 222 2222." />
	<?php } elseif(trim($cCity) == "Mumbai") { ?>
		<title>Best Car Rental | Car Hire Service Company in Mumbai - Carzonrent</title>
		<meta name="description" content="Carzonerent.com is well known  destination of car rental for Mumbai. To hire a cab at reasonable price  call now at 0888 222 2222." />
	<?php } elseif(trim($cCity) == "Bangalore") { ?>
		<title>Car Hire Service | Car Rental in Bangalore - Carzonrent</title>
		<meta name="description" content="Carzonrent is one of the  India's  most reliable and leading  Car Rental/Hire services Company. It offers quick & cozy cab on rent in Bangalore. Call now 0888 222 2222" />
	<?php } else { ?>
		<title>Car Hire Company | Car Rental Services in <?php echo ucwords($cCity); ?> - Carzonrent</title>
		<meta name="description" content="Carzonrent provides India's best and cheapest car rental & hire services for travel in <?php echo ucwords($cCity); ?>. Book a cab in <?php echo ucwords($cCity); ?> online on carzonrent.com. call now 0888 222 2222." />
	<?php } ?>
<meta name="keywords" content="car rental <?php echo strtolower($cCity); ?>, car hire <?php echo strtolower($cCity); ?>" />
<?php } else { ?>
<title>Car Rental | Hire Company | Book a Cab in India -  Carzonrent</title>
<meta name="description" content="Carzonrent - India's largest Car Rental/Hire Company offers cab or car rental services for Delhi, Mumbai, Bangalore, Hyderabad, Gurgaon, Noida and  Pune locations." />
<meta name="keywords" content="car rental, book a cab, car hire, rent a car" />
<?php } ?>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>

<style type="text/css">
#err
{
	margin-left: 10px;float:left;height:25px;color:#DB4626;
}
.mylesfeedbackfrom
{
	width:900px;
	border:solid 1px #333;
          float:left;
	background-color:#fff;
	padding:0px 0px 25px 25px;
}
.mylesfeedbackfrombtn
{
	width:900px;
	border-left:solid 1px #333;
	border-right:solid 1px #333;
	border-bottom:solid 1px #333;
          float:left;
	background-color:#F2F2F2;
	padding:25px 0px 25px 25px;
}
.top-left {
  float:left;
  margin:0px;
  width:900px;
}
.top-left .box {
  float:left;
  margin-right:15px;
  padding:10px 0 0 11px;
  width:150px;
}
.col
{
	width:280px;
	float:left;
	margin:0px 0px 10px 0px;
}
.experience
{
	width:900px;
	float:left;
	margin:25px 0px 0px 0px;
}
.row
{
	float:left;margin:25px 0px 0px 0px;
}
.resp
{
	float:left;margin:15px 0px 0px 0px;
}
.experience input[type=radio]{
	border:solid 1px #a2a2a2;
	margin:2px 2px 0px 20px;
	float:left;
}
.experience input[type=checkbox]{
	border:solid 1px #a2a2a2;
	margin:2px 2px 0px 20px;
	float:left;
}
input[type=checkbox]{
	margin:2px 2px 0px 20px;
	float:left;
}
label{
	float:left;
	margin:1px 0px 0px 2px;
}
.experience label{
	float:left;
	margin:1px 0px 0px 2px;
}
.ddl
{
	float:left;width:200px;padding:5px;
}
.tbox
{
	float:left;width:200px;padding:5px;
}
#wwr
{
	float:left;display:none;margin-top:20px;
}
</style>
<script>
$( document ).ready(function() {
 $('.visitorPopUp').hide();	
});
</script>
</head>
<body>	
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<div class="banner">
	<div class="main">
		<div class="mylesfeedbackfrom">		
			<div class="top-left">
			    <div class="row">
				<br />
				<div style="clear:both"></div>
				<br /><br />Thank you for sharing your feedback. This will help us serve you better.<br /><br /><br /><br />
			    </div>
			    <div style="clear:both"></div>
			</div>
		</div>
	</div>
</div>
<!--Middle Start Here-->
</div>	

<!--footer Starts Here-->
<?php include_once("./includes/footer.php"); ?>
<!--added by kamal-->
<style>
.open_close {
background: url(http://www.carzonrent.com/cms/cms_images/down_plus.png);
width: 24px;
height: 24px;

float: left;
}
.show {
background: url(http://www.carzonrent.com/cms/cms_images/up_minus.png);
width: 24px;
height: 24px;

float: left;
}
.cities-footer li a {
font-size: 11px;
color: rgb(102, 102, 102);
font-weight: normal;
text-decoration: none;
}
.cities-footer li {padding: 5px 0px;}
</style>
<!--end--->


<!--footer Ends Here-->
<div id="brandshow">
	<a href="http://www.worldtravelawards.com/vote-for-carzonrent-2013" target="_blank"><img src="<?php echo corWebRoot; ?>/images/popimg.gif" alt="Carzonrent.com" title="Carzonrent.com" /></a>
</div>
<script>
//added by kamal
$(document).ready(function(){
 $('#slide_click').bind('click',function(){
    $('.open_close').toggleClass('show');
  $('#slide_up_down').slideToggle();
 });
});
</script>
</body>
</html>
<?php
session_destroy();
?>
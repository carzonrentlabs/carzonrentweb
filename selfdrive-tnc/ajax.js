var webRoot = "http://www.carzonrent.com";
function checkbox_action()
{
    if ($('#tc').is(':checked') && $('.aboutus').is(":not(:visible)"))
    {
        $(".styled-button-1").show();
		 alert("I am 23 years and above, will present all the required original documents. I have read and accepted all the terms and conditions.");
		
    }
    else if ($('#tc').is(":not(:checked)"))
    {
        $(".styled-button-1").hide();
        $('.aboutus').hide();
    }
    else if ($('#tc').is(":checked") && $('.aboutus').is(':visible'))
    {
	    alert("I am 23 years and above, will present all the required original documents. I have read and accepted all the terms and conditions.");
        $('.aboutus').show();
        $(".styled-button-1").show();
        $("html, body").delay(50).animate({scrollTop:
                    $('.term_head').offset().top}, 500);
    }
}
function tnclink_action()
{
    $('.aboutus').toggle();
}
function troggleMail(frm)
{
	
    var cityId = $('#cityId').val();
    var ClientId = $('#ClientId').val();
    var pickupDate = $('#pickupDate').val();
    var droffDate = $('#droffDate').val();
    var PickUpTime = $('#PickUpTime').val();
    var DropOffTime = $('#DropOffTime').val();
    var SubLocationId = $('#SubLocationId').val();
    var PkgType = $('#PkgType').val();
    var CarModelId = $('#CarModelId').val();

    var dataString1 = "cityId=" + cityId + "&ClientId=" + ClientId + "&pickupDate=" + pickupDate +
            "&droffDate=" + droffDate + "&PickUpTime=" + PickUpTime + "&DropOffTime=" + DropOffTime +
            "&SubLocationId=" + SubLocationId + "&PkgType=" + PkgType + "&CarModelId=" + CarModelId;
    
    $.ajax({type: "POST",
        url:webRoot + "/carcheckbeforepayment.php",
        data:dataString1,
        success: function (response) {

            var txt1 = response;
            if (txt1 == 1)
            {
				
                /////////////////////////////////////
				
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                var emailId = document.getElementById('chatemail').value;
                var entrydate = document.getElementById('entrydate').value;
                var fname = $('#hidefanme').val();
                var lname = $('#hidelname').val();
                var email = $('#hideemail').val();
                var mobile = $('#hidemobile').val();
                var coric = $('#coricid').val();
                var bookingid = $('#hidebookingid').val();
                var destination = $('#hidedestination').val();
                var dataString = 'emailId=' + emailId + '&entrydate=' + entrydate + '&coric=' + coric + '&fname=' + fname + '&lname=' + lname + '&email=' + email + '&mobile=' + mobile + '&bookingid=' + bookingid + '&destination=' + destination;


                var ajBook;
                if (window.XMLHttpRequest)
                    ajBook = new XMLHttpRequest;
                else if (window.ActiveXObject)
                    ajBook = new ActiveXObject("Microsoft.XMLHTTP");
                ajBook.onreadystatechange = function () {
                    if (ajBook.readyState == 4) {
                        frm.submit();
                    }
                };
                ajBook.open("POST", webRoot + "/selfdrive-tnc/sendpdf_ajax.php", true);
                ajBook.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                var values = dataString;
                ajBook.send(values);



				
				/////////////////////////////////////
            }
            else
            {
                alert("Car Not Available");
				location.href= webRoot;
                return false;
            }
        }});



    return false;
}
function validate_checkbox(frm)
{
    if ($('#tc').is(":not(:checked)"))
    {
        alert("Please Select Terms And Conditions");
        return false;
    }
    else if ($('#chatemail').val() == "" && $('.aboutus').is(':visible'))
    {
        alert('Please Enter Full Name.');
        $("#chatemail").attr('placeholder', 'Please Full Name.').css('background', 'rgb(240, 223, 205)').focus();
        $("html, body").delay(50).animate({scrollTop:
                    $('.aboutus').offset().top}, 500);
        return  false;
    }
    else if ($('#checkbtn').val() == '0' && $('.aboutus').is(':visible'))
    {
        alert("Please submit argeement contract before processing of booking");
        $("html, body").delay(50).animate({scrollTop:
                    $('.aboutus').offset().top}, 500);
        $('#btnfrst').css('background', '#000').fadeTo(1500, 0.1).fadeTo(1500, 1.0);
        return false;
    }
    else if ($('.aboutus').is(":not(:visible)") && $('#chatemail').val() == "")
    {
        alert("Please click I agree button.");
        $('#agree_btn').fadeTo(500, 0.1).fadeTo(500, 1.0);
        return false;
    }
    else if ($('.aboutus').is(":not(:visible)") && $('#chatemail').val() != "" && $('#checkbtn').val() == '0')
    {
        alert("Please submit argeement contract before processing of booking");
        $('.aboutus').show();
        $("html, body").delay(50).animate({scrollTop:
                    $('.aboutus').offset().top}, 500);
        $('#btnfrst').css('background', '#000').fadeTo(1500, 0.1).fadeTo(1500, 1.0);
        return false;
    }
    else
    {

        frm.submit();
    }
}
function popupshpw()
{
    if ($('#tc').is(':checked'))
    {
        $(".visitorPopUp").fadeIn();
        $("#additempopup1").show();
        $(".visitorPopUp").animate({top: '4%'});
    }
}
function close_popup()
{
    $('.aboutus').slideUp(200);
}	
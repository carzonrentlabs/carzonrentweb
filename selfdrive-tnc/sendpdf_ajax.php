<?php
session_start();
include_once("../includes/cache-func.php");
include_once('../classes/cor.mysql.class.php');
require('../selfdrive-tnc/pdf/fpdf.php');
if(isset($_POST["emailId"])){

$dataToSave = array();
$db = new MySqlConnection(CONNSTRING);
$db->open();
$dataToSave["name"] = $_REQUEST['emailId'];
$dataToSave["entrydate"] = $_REQUEST['entrydate'];
$dataToSave["source"] = 'Website';
$dataToSave["coric"] = $_REQUEST['coric'];
$r = $db->insert("cor_accept_tnc", $dataToSave);
$_SESSION['lastinsertId']=$r['lastinsertId'];
unset($dataToSave);
$db->close();
class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('../selfdrive-tnc/pdf/COR-logo.gif',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(60,10,'User Agreement',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}


// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',8);
$text1='This CAR RENTAL AGREEMENT ("Agreement") is made and entered on this '.date("l jS \of F").','.date('Y').' ("Effective Date") at '.$_POST['destination'].'';
$text2='By and between';
$text3='CARZONRENT (INDIA) PRIVATE LIMITED, a private limited company incorporated under the provisions of the Companies Act 1956, having its Registered Office at 9th Floor Videocon Tower, E-1 Block, Jhandewalan Extension, New Delhi 110055,(hereinafter referred as the "Company" and which term shall unless excluded by or repugnant to the context shall mean and include its successors, administrators and assignees etc. ) of the FIRST PART;';
$text4='AND';
$text5=''.ucfirst($_POST['fname']).',Phone no : '.$_POST['mobile'].' ,Email Address: '.$_POST['email'].' (hereinafter referred as the "User" and which term shall unless excluded by or repugnant to the context shall mean and include his/her legal heirs and representatives etc. ) of the SECOND PART;
The Company and the User may be collectively referred to as the "Parties" and individually as "Party".';
$pdf->Write(5,$text1,0,2); 
$pdf->ln();
$pdf->Write(5,$text2,0,2); 
$pdf->ln();
$pdf->Write(5,$text3,0,2); 
$pdf->ln();
$pdf->Write(5,$text4,0,2); 
$pdf->ln();
$pdf->Write(5,$text5,0,2); 
$pdf->ln();
$pdf->Cell(0,2,ucfirst($_REQUEST['emailId']),0,1);
$pdf->ln(); 
$pdf->Cell(0,2,'Coric Id-'.$_POST['coric'],0,1);
$pdf->ln(); 
$pdf->Cell(0,2,'Accept Date And time-'.$_REQUEST['entrydate'],0,1);
$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/selfdrive-tnc/agreementdoc/'.$_POST['coric'].'.pdf','F');
//$pdf->Output('../selfdrive-tnc/agreementdoc/'.$_REQUEST['emailId'].'-'.date("d-m-Y-h-m-s").'-'.$_POST['coric'].'.pdf','F');
$db = new MySqlConnection(CONNSTRING);
$db->open();
$dataToSave = array();
$dataToSave["tncpdf"] = $_POST['coric'].'.pdf';  // Added by Iqbal on 28Aug2013
$whereData = array();
$whereData["id"] =$_SESSION['lastinsertId'];
$r = $db->update("cor_accept_tnc", $dataToSave, $whereData);
//for payment status//
$dataToSave1["payment_status"] = "2"; //2 means the user has gone to the payment gateway
$dataToSave1["payment_mode"] = "1";
$dataToSave1["payment_start_dt"] = date('Y-m-d H:i:s');
$whereData1 = array();
$whereData1["coric"] = $_POST['coric'];		
$r2 = $db->update("cor_booking_new", $dataToSave1, $whereData1);
$db->close();
unset($whereData1);
unset($dataToSave1);
//for payment status//
unset($whereData);
unset($dataToSave);
unset($_SESSION['lastinsertId']);
echo '1';
}
?>
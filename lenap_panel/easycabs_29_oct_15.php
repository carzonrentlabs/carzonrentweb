<?php
    include_once("./includes/check-user.php");
    include_once("./includes/encrypt.php");
    $lgUserName = _decrypt($_COOKIE["loggedinuser"], $bbEncryptionKey);
    if($lgUserName != "admin@carzonrent.com"){
	$fURL1 = "./logout.php";
	header('Location: ' . $fURL1);	
    }
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>EasyCabs Live Bookings</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
		<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
		<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/ajax.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/admin/js/admin.js" type="text/javascript"></script>
		<!-- <script src="./js/admin.js" type="text/javascript"></script> -->
		
	    <script>
	    $(function() {
		    var dateToday = new Date();
		    dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    var dateNextDay = new Date();
		    dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    $( ".datepicker" ).datepicker({minDate: dateToday, maxDate: dateNextDay});
	    });
	    </script>
	    <style type="text/css">
		.wordwrap {
			    white-space: pre-wrap;
			    word-wrap: break-word;
			    }	
	    </style>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/admin/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
    <?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="./dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> EasyCabs Live Bookings</h2>
		</div>
	    </div>
	     <?php		
		$url = "./easycabs.php";
                $dlndURL = "./easycabs-download.php";
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                
                $recPerPage = 100;
                $maxPageNum = 7;
                $cPage = 1;
                $sDate = date_create(date("Y-m-d"));
                $eDate = date_create(date("Y-m-d"));
                
                if(isset($_GET["pg"]) && $_GET["pg"] != "")
                $cPage = $_GET["pg"];
                
                if(isset($_GET["sd"]) && $_GET["sd"] != ""){
                    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
                    $isFiltered = true;
                    $isDtInc = true;
                }
                    
                if(isset($_GET["ed"]) && $_GET["ed"] != ""){
                    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
                    $isFiltered = true;
                    $isDtInc = true;
                }
                $lStart = ($cPage - 1) * $recPerPage;
                
                $totRec = 0;
                if($fld != "" && $fVal != ""){
                    $sql = "SELECT COUNT(unique_id) as TotalRecord FROM easycabs_booking WHERE " . $fld . " = '" . $fVal . "'";
                    if($isDtInc){
                        $sql .= " AND entry_datetime BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
                    }
                } else {
                    // $sql = "SELECT COUNT(unique_id) as TotalRecord FROM easycabs_booking WHERE entry_datetime BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
					  $sql = "select count(unique_id) as TotalRecord from easycabs_booking where isValid = 1 AND booking_id IS NOT NULL AND (entry_datetime BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59') ORDER by unique_id DESC"; 
					
                }
                $r = $db->query("query", $sql);
                if(!array_key_exists("response", $r)){
                    $totRec = $r[0]["TotalRecord"];
                }
                unset($r);
                
                $numOfPage = ceil($totRec / $recPerPage);
                
                $sql = "select booking_id,keyword,cust_mobile, guest_name, pickup_address, pickup_datetime, pickup_location,
                pickup_landmark,pickup_latlon,entry_datetime,dest_address, drop_landmark, drop_latlon, tokenid,ip,ua, userid, username,
                useremail, userphone from easycabs_booking where isValid = 1 AND booking_id IS NOT NULL
                AND (entry_datetime BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59') ORDER by unique_id DESC LIMIT " . $lStart . ", " . $recPerPage;
                $r = $db->query("query", $sql);
                $db->close();
                
                $pagingHTML = "";
                if($numOfPage > 1){
                    $nURL = "";
                    $nURL .= $nURL == "" ? "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) : "&" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));			    
                    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
                        $pagingHTML .= "<tr>";
                            $pagingHTML .= "<td>";
                            if($cPage % $maxPageNum != 0)
                                $pgStart = $cPage - ($cPage % $maxPageNum);
                            else
                                $pgStart = $cPage - $maxPageNum;
                            $pgStart++;
                            if($cPage > $maxPageNum){
                                $pgURL = $nURL;
                                $pgPrev = $pgStart -  1;
                                $pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
                                $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
                            }
                            for($p = $pgStart; $p <= $numOfPage; $p++){
                                $pgURL = $nURL;
                                if($p != $cPage){
                                    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
                                    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
                                }
                                else
                                    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
                                if($p % $maxPageNum == 0)
                                break;
                            }
                            if($numOfPage > $p){
                                $p++;
                                $pgURL = $nURL;
                                $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
                                $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
                            }
                            $pgURL = $nURL;
                            $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
                    $pagingHTML .= "Go to page number <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
                            for($p = 1; $p <= $numOfPage; $p++){
                                if($p != $cPage){
                                    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
                                } else {
                                    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
                                }
                            }
                                $pagingHTML .= "</select>";
                            $pagingHTML .= "</td>";
                            if(($lStart + $recPerPage) >= $totRec)
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
                            else
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
                        $pagingHTML .= "</tr>";
                    $pagingHTML .= "</table>";
                } else {
                    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
                        $pagingHTML .= "<tr>";
                            if(($lStart + $recPerPage) >= $totRec)
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . $totRec . " of <b>" . $totRec . "</b></td>";
                            else
                            $pagingHTML .= "<td>&nbsp;Showing " . ($lStart + 1) . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b></td>";
                        $pagingHTML .= "</tr>";
                    $pagingHTML .= "</table>";
                }
	    ?>
	    <div class="main" style="width:100%;">
		<div style="margin:0 auto;width:99%;">			
			<form id="frmSearch" name="frmSearch" action="easycabs.php" method="get">
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('sds').value = this.value;}" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if(document.getElementById('inc').checked){document.getElementById('eds').value = this.value;}" />						    
							</span>
					    </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
					    </td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right">&nbsp;</td>
			    </tr>
			</table>
			</form>
                        <table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="width:100%;"><?php echo $pagingHTML; ?></td>
			    </tr>
			</table>
			<table width="100%" cellspacing="1" class="adm" cellpadding="5" border="0" align="left" style="color:#323232;table-layout: fixed;margin-top:30px;">
			    <tbody id="searchData">			
                <tr>
				    <th align="left" width="2%">SrNo.</th>
				    <th align="left">Book Date</th>
				    <th align="left">Booking ID</th>
				    <th align="left">Keyword</th>
				    <th align="left">Mobile</th>				    
				    <th align="left">Name</th>
				    <th align="left">Pickup Address</th>
				    <th align="left">Pickup Datetime</th>
				    <th align="left">Location</th>
				    <th align="left">Drop Location</th>
				    <th align="left">IP<br />Browser</th>
				    <th align="left">LoggedIn User</th>
                </tr>			    
            <?php
				//print_r($r);
				if(!array_key_exists("response", $r)){
                                    $srno = $lStart + 1;
				    for($i=0 ; $i < count($r); $i++)
				    {				   
					$style = "";
                                        $cls = "od";
					if($i%2 == 0)
					    $cls = "ev";
                        ?>
				    <tr class="<?php echo $cls ?>">
					<td style="padding:5px !important;word-wrap:break-word;" name = "seqno"><?php echo $srno; ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('d M, Y', strtotime($r[$i]["entry_datetime"])) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["booking_id"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["keyword"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["cust_mobile"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["guest_name"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["pickup_landmark"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('d M, Y H:iA', strtotime($r[$i]["pickup_datetime"])) ?></td>
					<?php
					    $dispLocation = "";
					    switch ($r[$i]["pickup_location"]) {
						    case 1:
							    $dispLocation = "Delhi";
							    break;
						    case 2:
							    $dispLocation = "Mumbai";
							    break;
						    case 3:
							    $dispLocation = "Bangalore";
							    break;
						    case 4:
							    $dispLocation = "Hyderabad";
							    break;
					    }
					    $ua = browser_info($r[$i]["ua"]);
					    $b = array_keys($ua);
					    $v = array_values($ua);
					?>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $dispLocation ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["drop_landmark"] ?><br /><?php echo $r[$i]["dest_address"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["ip"] . "<br /><br />"; ?><?php echo ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]); ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><br /><?php echo $r[$i]["userid"] ?><br /><?php echo $r[$i]["username"] ?><br /><?php echo $r[$i]["useremail"] ?><br /><?php echo $r[$i]["userphone"] ?></td>
				    </tr>
            <?php
                                        $srno++;
				    }
				    unset($r);
				}
				else {
			?>
				    <tr style="background-color:#f1f1f1;">
					<td align="center" colspan="10">No data found.</td>
				    </tr>
			<?php
				}
                        ?>
			    </tbody>
			</table>
			<div style="clear:both"></div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="width:100%;"><?php echo $pagingHTML; ?></td>
			    </tr>
			</table>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<?php
include_once("./includes/check-user.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Carzonrent Admin</title>
        <?php
        include_once("./includes/cache-func.php");
        include_once("./includes/header-css.php");
        include_once("./includes/header-js.php");
        include_once("./includes/config.php");
        include_once("./classes/bb-mysql.class.php");
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo __WEBROOT__ ?>/css/style.css?v=<?php echo mktime(); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="<?php echo __WEBROOT__ ?>/js/ajax.js" type="text/javascript"></script>
        <script src="<?php echo __WEBROOT__ ?>/js/searchajax.js" type="text/javascript"></script>
        <script src="<?php echo __WEBROOT__ ?>/js/admin.js" type="text/javascript"></script>	
        <script src="<?php echo __WEBROOT__ ?>/js/new-admin.js" type="text/javascript"></script>	  
        <script>
            $(function () {
                var dateToday = new Date();
                dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                var dateNextDay = new Date();
                dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: dateToday, maxDate: dateNextDay});
            });
        </script>     
    </head>
    <body>
        <?php include_once("./includes/header.php"); ?>
        <div id="middle">	
            <div class="yellwborder">
                <div style="padding:7px 0px 7px 10px; width: 98% !important;" class="magindiv">
                    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Campaign Summary</h2>
                </div>
            </div>
            <div class="main" style="width:100%;">
                <br><br>
                        <div style="margin:0 auto;width:99%;">
                            <form id="frmSearch" name="frmSearch" action="campaign-summary.php" method="get">
                                <table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
                                    <tbody>
                                        <tr>
                                            <?php
                                            $db = new MySqlConnection(CONNSTRING);
                                            $db->open();

                                            $sDate = "";
                                            if (isset($_GET["startdate"]) && $_GET["startdate"] != "") {
                                                $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
                                            } else {
                                                $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
                                                //print_r($maxDate);
                                                $sDate = date_create($maxDate[0]["max_date"]);
                                            }

                                            $eDate = "";
                                            if (isset($_GET["enddate"]) && $_GET["enddate"] != "") {
                                                $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
                                            } else {
                                                $eDate = $sDate;
                                            }
                                            if (isset($_REQUEST["campaign"]) && $_REQUEST["campaign"] != "") {
                                                $location = $_REQUEST["campaign"];
                                            } else {
                                                $location = '';
                                            }
                                            ?>
                                            <td><label>Start Date</label></td>
                                            <td>
                                                <span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            <td><label>End Date </label>
                                            </td>
                                            <td><span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            <td><label>Campaign</label></td>
                                            <td>
                                                <?php
                                                $db = new MySqlConnection(CONNSTRING);
                                                $db->open();
                                                $sql = "SELECT campaign FROM customer_search where refsite IS NOT NULL group by campaign";
                                                $r = $db->query("query", $sql);
                                                if (!array_key_exists("response", $r)) {
                                                    ?>
                                                    <select class="ddlS" name="campaign" id="campaign" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <option value="">Select One....</option>   <?php
                                                        for ($i = 0; $i < count($r); $i++) {
                                                           if($_REQUEST['campaign']==$r[$i]["campaign"]){
                                                               $sel = 'selected="selected"';
                                                           }else{
                                                               $sel = '';
                                                           } ?>
                                                        
                                                            <option <?php echo $sel;?> value="<?php echo $r[$i]["campaign"]; ?>"><?php echo $r[$i]["campaign"]; ?></option>
                                                            <?php ?>

                                                        <?php } ?>

                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
                                            </td>
<!--                                            <td valign="middle">					
                                                <a href="download-campaign-summary.php?sdate=<?php echo $sDate->format('Y-m-d'); ?>&edate=<?php echo $eDate->format('Y-m-d'); ?>&campaign=<?php echo $_REQUEST['campaign']?>">Download</a>
                                            </td>-->
                                            
                                            
                                        </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </form>
                            <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
                                <tbody id="searchData">			
                                    <tr>
                                        <td align="center" width="5%" class="search_table">SrNo.</td>
                                        <td align="center" width="10%" class="search_table">Campaign</td>
                                        <td align="center" width="10%" class="search_table">Site</td>
                                        <td align="center" width="10%" class="search_table">Hits</td>
                                        <td align="center" width="10%" class="search_table">Booking</td>
                                    </tr>			    
                                    <?php
                                    $db = new MySqlConnection(CONNSTRING);
                                    $db->open();
                                    
                                    if (isset($_REQUEST["campaign"]) && $_REQUEST["campaign"] != "") {
                                        $r = $db->query("stored procedure", "cor_search_campaign_report_location('" . $sDate->format('Y-m-d') . "','" . $eDate->format('Y-m-d') . "','".$_REQUEST['campaign']."')");
                                        $sql = "SELECT (count(cs.refsite)) AS hits,cs.*,(count(bn.booking_id)) AS booking FROM customer_search AS cs LEFT JOIN cor_booking_new as bn on cs.coric=bn.coric where cs.refsite IS NOT NULL AND cs.campaign = '".$_REQUEST['campaign']."' and (cs.entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."') group by cs.refsite ORDER by cs.unique_id DESC";
                                        //$sql = "select count(refsite) AS hits,cs.* from customer_search AS cs where (cs.entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."') and cs.refsite IS NOT NULL AND cs.campaign='".$_REQUEST['campaign']."' group by cs.unique_id ORDER by cs.unique_id DESC";
                                        $qst = $db->query("query", $sql);
                                    } else {
                                        $r = $db->query("stored procedure", "cor_search_campaign_report('" . $sDate->format('Y-m-d') . "','" . $eDate->format('Y-m-d') . "')");
                                        $sql = "select count(refsite) AS hits,cs.* from customer_search AS cs where (cs.entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."') and cs.refsite IS NOT NULL group by cs.refsite ORDER by cs.unique_id DESC";
                                        $qst = $db->query("query", $sql);
                                     }
                                   //echo "<pre>"; print_r($r);
                                    if (!array_key_exists("response", $r)) {
                                        for ($i = 0; $i < count($r); $i++) {
                                            $style = "";
                                            if ($i % 2 == 0)
                                                $style = "background-color:#f1f1f1;";
                                            else
                                                $style = "background-color:#dddada;";
                                            
                                            ?>
                                            <tr style="<?php echo $style ?>">
                                            <td style="padding:5px !important;" name = "seqno"><?php echo ($i + 1) ?></td>
                                            <td style="padding:5px !important;"><?php echo $r[$i]["campaign"] ?></td>
                                            <td style="padding:5px !important;"><?php echo $r[$i]["refsite"] ?></td>
                                            <td style="padding:5px !important;"><?php echo $r[$i]["hits"] ?></td>
                                            <td style="padding:5px !important;"><?php echo $r[$i]["booking"] ?></td>    
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else {
                                        ?>
                                        <tr style="background-color:#f1f1f1;">
                                            <td align="center" colspan="10">No data found.</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>

                            <?php if (count($qst) > 50) { ?>
                                <table width="100%" cellspacing="1" cellpadding="5" border="0"  style="float: left;margin:15px 0 0 0px;">
                                    <tbody id = "loadmore">
                                        <tr>
                                            <td align="right">
                                                <?php if($_REQUEST['campaign']!=''){ ?>
                                                <a href="javascript:void(0);" onclick="javascript: _loadMoreSearchWithcity('<?php echo $sDate->format('Y-m-d') ?>', '<?php echo $eDate->format('Y-m-d') ?>',<?php echo $i+1 ?>,'<?php echo $_REQUEST['campaign'] ?>')">LOAD MORE</a>
                                                <?php }else{
                                                    ?>
                                                        <a href="javascript:void(0);" onclick="javascript: _loadMoreSearch('<?php echo $sDate->format('Y-m-d') ?>', '<?php echo $eDate->format('Y-m-d') ?>',<?php echo $i+1 ?>)">LOAD MORE</a>
                                                            <?php
                                                } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            unset($r);
                            ?>
                        </div>		
                        <div class="clr"></div>
                        </div>
                        <p>&nbsp;</p>	    	
                        </div>
                        <?php
                        include_once("./includes/footer.php");
                        ?>
                        </body>
                        </html>
<?php
    include_once("./includes/check-user.php");
    include_once("./classes/cor.ws.class.php");
    include_once("./classes/cor.xmlparser.class.php");
    
    $fld = "";
    $val = "";
    $isDtInc = false;
    $isFiltered = false;
    $sortFld = "uid";
    $sortMode = "desc";
    
    $date = date_create(date('Y-m-d'));
    date_sub($date, date_interval_create_from_date_string('30 days'));
    $fromdate=date_format($date, 'Y-m-d');
    


    $maintodate=date('Y-m-d');
    
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
    $cPage = $_GET["pg"];
    
    
    
    if(isset($_GET["f"]) && $_GET["f"] != ""){
        $fld = $_GET["f"];
        $isFiltered = true;
    }
    
    //print_r($sDate);die;
    if(isset($_GET["q"]) && $_GET["q"] != ""){
        
        $fVal = $_GET["q"];
        $isFiltered = true;
        if($fld != ""){
            if($fld == "payment_mode"){
                if(trim(strtolower($fVal)) == "online")
                    $fVal = 1;
                elseif(trim(strtolower($fVal)) == "pay to driver")
                    $fVal = 2;
            } elseif($fld == "payment_status"){
                if(trim(strtolower($fVal)) == "success")
                    $fVal = 1;
                elseif(trim(strtolower($fVal)) == "gateway")
                    $fVal = 2;
                elseif(trim(strtolower($fVal)) == "payment")
                    $fVal = 3;
                elseif(trim(strtolower($fVal)) == "failure")
                    $fVal = 4;
            }				
        }
    }
    
    if(isset($_GET["inc"]) && $_GET["inc"] != ""){
        $isDtInc = true;
        $isFiltered = true;
    }
    if(isset($_GET["sf"]) && $_GET["sf"] != "")
    $sortFld = $_GET["sf"];
    
    if(isset($_GET["sm"]) && $_GET["sm"] != "")
    $sortMode = $_GET["sm"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "" && isset($_GET["ed"]) && $_GET["ed"] != "")                   
        $filename = "bookings_rentalcars_" . $fromdate . "_" . $maintodate;
    else
        $filename = "bookings_overall_rentalcars_" . $fromdate;
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
        $filename .= "_p" . $cPage;
    
    $filename .= ".csv";
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");
    
    if(isset($_GET["sd"]) && $_GET["sd"] != ""){
    $fdate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
    $fromdate=date_format($fdate, 'Y-m-d');}
    //echo $fromdate;die;
    
    if(isset($_GET["ed"]) && $_GET["ed"] != ""){
    $sdate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
    $maintodate=date_format($sdate, 'Y-m-d');}
    
     
    
    $WebRequestData1 = array();
    $WebRequestData1["ClientID"] = '2713';//2219++++2713;
    $WebRequestData1["FromDate"] = $fromdate;
    $WebRequestData1["ToDate"] = $maintodate;
    $WebRequestData1["DateFilterYN"] = 'Y';
    $soapClient = new COR();
    $myXML = new CORXMLList();
    //print_r($WebRequestData1);die;
    $RecordResult = $soapClient->_CORDashBoardSummary($WebRequestData1);
    //echo "<pre>";print_r($RecordResult);die;



    if ($RecordResult->{'DashBoardSummaryResult'}) {
        $result = $RecordResult->{'DashBoardSummaryResult'}->{'any'};

        $resultany = $myXML->xml2ary($result);
        $countArray = count($resultany);
        if (is_array($resultany)) {
            $i=1;
            
        $fieldNames = array('S No','Source','BookingId','Destination','Fare','Travel On','Booked On','Client Id','Phone No','EmailId');
        fputcsv($output, $fieldNames);
        unset($fieldNames);
            foreach ($resultany as $valarray) {
                
            $fieldValues = array($i,$valarray['trackid'],$valarray['bookingid'],$valarray['pickupadd'],$valarray['indicatedprice'],$valarray['pickupdate'],$valarray['createdate'],$valarray['clientcoid'],$valarray['phone1'],$valarray['emailid']);
            fputcsv($output, $fieldValues);
            unset($fieldValues);
        
               //echo "<pre>";print_r($valarray);
                ?>

                <?php 
            $i++;}
           
                }
            }
 
?>
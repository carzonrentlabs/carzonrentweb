<?php
    include_once("./includes/check-user.php");
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
    $currentMonth = date("m");
    $curyear = date("Y");
    if(isset($_REQUEST['month'])){
        $mnt = $_REQUEST['month'];
    }else{
        $mnt = $currentMonth;
    }
    if(isset($_REQUEST['year'])){
        $yr = $_REQUEST['year'];
    }else{
        $yr = $curyear;
    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/chart.js" type="text/javascript"></script>
   
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/highcharts.js"></script>
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/exporting.js"></script>

<script>
$(function (){
    
    var bid1 = [<?php 
          $dtl = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '2' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dtl .= $prefix.$rows[0]['ttl'];
            }
            $dtl = str_replace("<br/>", " ", $dtl );
            echo $dtl;
            $i = '';
        ?>];
        var bid2 = [<?php 
          $dt2 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '11' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt2 .= $prefix.$rows[0]['ttl'];
            }
            $dt2 = str_replace("<br/>", " ", $dt2 );
            echo $dt2;
            $i = '';
        ?>];
        var bid3 = [<?php 
          $dt3 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '3' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt3 .= $prefix.$rows[0]['ttl'];
            }
            $dt3 = str_replace("<br/>", " ", $dt3 );
            echo $dt3;
            $i = '';
        ?>];
        var bid4 = [<?php 
          $dt4 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '7' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt4 .= $prefix.$rows[0]['ttl'];
            }
            $dt4 = str_replace("<br/>", " ", $dt4 );
            echo $dt4;
            $i = '';
        ?>];
        var bid5 = [<?php 
          $dt5 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '4' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt5 .= $prefix.$rows[0]['ttl'];
            }
            $dt5 = str_replace("<br/>", " ", $dt5 );
            echo $dt5;
            $i = '';
        ?>];
        var bid6 = [<?php 
          $dt6 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '8' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt6 .= $prefix.$rows[0]['ttl'];
            }
            $dt6 = str_replace("<br/>", " ", $dt6 );
            echo $dt6;
            $i = '';
        ?>];
        var bid7 = [<?php 
          $dt7 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '6' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt7 .= $prefix.$rows[0]['ttl'];
            }
            $dt7 = str_replace("<br/>", " ", $dt7 );
            echo $dt7;
            $i = '';
        ?>];
        var bid8 = [<?php 
          $dt8 = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$i." AND origin_id = '5' AND booking_id IS NOT NULL";
                $rows = $db->query("query", $sql);
                $dt8 .= $prefix.$rows[0]['ttl'];
            }
            $dt8 = str_replace("<br/>", " ", $dt8 );
            echo $dt8;
            $i = '';
        ?>];
    $('#container').highcharts({
        title: {
            text: 'Dates',
            x: 0 //center
        },
        /*subtitle: {
            text: 'Source: WorldClimate.com',
            x: 0
        },*/
        xAxis: {
            categories: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
        },
        yAxis: {
            title: {
                text: 'No Of Booking'
            },
             tickInterval: 1,
              labels: {
                formatter: function() {
                    if(this.value >= 0)
                    {
                        return Math.abs(this.value);
                    }
                 
                    
                }
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080',
            }]
        },
        tooltip: {
            valueSuffix: 'Cars'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [{
            name: 'Delhi',
            data: bid1 }
        ,{
            name: 'Noida',
            data: bid2
        }
        ,{
            name: 'Gurgaon',
            data: bid3
        }
        ,{
            name: 'Bangalore',
            data: bid4
        }
        ,{
            name: 'Mumbai',
            data: bid5
        }
        ,{
            name: 'Chennai',
            data: bid6
        }
        ,{
            name: 'Hyderabad',
            data: bid7
        }
        ,{
            name: 'Pune',
            data: bid8
        }
	]
    });
});
</script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Booking Chart For Multiple Cities</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./chart.php";
			//$dlndURL = "./selfdrive-search-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "unique_id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("source" => "Source", "coric" => "CORIC", "booking_id" => "Booking ID", "tour_type" => "Tour Type", "package_id" => "Package ID","car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			
			
		?>
	    
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Month</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>Year</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						
						    <select class="ddlS" name="month" id="month" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        $months = array("01"=>"Jan", "02"=>"Feb", "03"=>"Mar", "04"=>"Apr","05"=>"May","06"=>"June","07"=>"July","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
                                                        
                                                        $sel = '';
                                                        $mm = 0;
                                                        foreach($months as $k=>$month) {
                                                            if(isset($_REQUEST['month'])){
                                                                if(($_REQUEST['month'] == $k) && $mm==0){
                                                                $sel = 'selected = "selected"';
                                                                $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($currentMonth==$k && $mm==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $k . "\">" . $month . "</option>";
                                                        
                                                           
                                                            
                                                            
                                                            }
                                                        ?>

                                                    </select>
						
					    </td>
                                                
					    <td style="padding-left: 10px;">
                                                <select class="ddlS" name="year" id="year" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        
                                                        $sel = '';
                                                        $nn = 0;
                                                        for($i=2010;$i<=$curyear;$i++){
                                                            if(isset($_REQUEST['year'])){
                                                                if(($_REQUEST['year'] == $i) && $nn==0){
                                                                $sel = 'selected = "selected"';
                                                                $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($curyear==$i && $nn==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $i . "\">" . $i . "</option>";
                                                        }
                                                        ?>

                                                    </select>
					    </td>
                                            
					    <td style="padding-left: 10px;">
                                                <input type="image" value="Submit" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" />
					    </td>
                                            
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				
			    </tr>
			</table>
		</div>		
		<div class="clr"></div>
	    </div>
            
            <div id="container" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Booking Status | Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
		include_once("./classes/cor.ws.class.php");
		include_once("./classes/cor.xmlparser.class.php");
		$bookingDetails = array();
		if(isset($_GET["q"]) && $_GET["q"] != ""){
		    $bookingID = $_GET["q"];
		    $soapClient = new COR();
		    $bookingResponse = $soapClient->_CORGetBookingsDetail(array("BookingID" => $bookingID));
		    unset($soapClient);
		    //echo "<pre>";
		    //print_r($bookingResponse);
		    //echo "</pre>";
		    
		    $xmlToArray = new CORXMLList();
		    $bookingDetails = $xmlToArray->xml2ary($bookingResponse->{'GetBookingDetailResult'}->{'any'});
		    unset($xmlToArray);
		    //echo "<pre>";
		    //print_r($bookingDetails);
		    //echo "</pre>";
		}
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo __WEBROOT__ ?>/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo __WEBROOT__ ?>/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo __WEBROOT__ ?>/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Booking Status</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <form id="" name="" action="./booking-status.php" method="get">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td colspan="2" style="padding-left: 12px;">Enter Booking ID</td>
					    <td style="padding-left: 10px;"><input type="text" id="q" name="q" class="txtSBox" size="22" placeholder="Enter here" <?php if(isset($_GET["q"])) { echo "value=" . $_GET["q"]; } ?> /></td>
					    <td style="padding-left: 15px;"><input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" /></td>
					</tr>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
			<?php
			    if(count($bookingDetails) > 0) :
				$pDate = date_create($bookingDetails[0]["PickUpDate"]);
				$dDate = date_create($bookingDetails[0]["DropOffDate"]);
			?>
				<table cellpadding="5" cellspacing="1" class="adm" border="0" style="margin: 10px 1.5%; width: 97%;float: left;">
				    <tr class="od">
					<td>Origin</td>
					<td><?php echo $bookingDetails[0]["OriginName"]; ?></td>
					<td>Destination</td>
					<td><?php echo $bookingDetails[0]["destination"]; ?></td>
				    </tr>
				    <tr class="ev">
					<td>PickUp Date & Time</td>
					<td><?php echo $pDate->format('d M, Y') . " " . $bookingDetails[0]["PickUpTime"] . "HRS"; ?></td>
					<td>DropOff Date</td>
					<td><?php echo $dDate->format('d M, Y'); ?></td>
				    </tr>
				    <tr class="od">
					<td>Name</td>
					<td><?php echo $bookingDetails[0]["Fname"] . " " . $bookingDetails[0]["Lname"]; ?></td>
					<td>Phone</td>
					<td><?php echo $bookingDetails[0]["mobile"]; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Email</td>
					<td><?php echo $bookingDetails[0]["EmailID"]; ?></td>
					<td>Address</td>
					<td><?php echo $bookingDetails[0]["PickUpAdd"]; ?></td>
				    </tr>
				    <tr class="od">
					<td>Payment Amount</td>
					<td><?php echo $bookingDetails[0]["PaymentAmount"]; ?></td>
					<td>Payment Status</td>
					<td><?php echo $bookingDetails[0]["PaymentStatus"]; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Tour Type</td>
					<td><?php echo $bookingDetails[0]["TourType"]; ?></td>
					<td>Package ID</td>
					<td><?php echo $bookingDetails[0]["IndicatedPkgID"]; ?></td>
				    </tr>
				    <tr class="od">
					<td>Discount Amount</td>
					<td><?php echo $bookingDetails[0]["DiscountAmount"]; ?></td>
					<td>Remarks</td>
					<td><?php echo $bookingDetails[0]["Remarks"]; ?></td>
				    </tr>
				    <tr class="ev">
					<td>Track ID</td>
					<td><?php echo $bookingDetails[0]["trackID"]; ?></td>
					<td>Status</td>
					<td><?php echo $bookingDetails[0]["Status"]; ?></td>
				    </tr>
				</table>
			<?php
			    endif;
			?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p <?php if(!isset($_GET["q"])) { echo "style=\"height:300px;\""; } ?>>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	unset($bookingDetails);
	?>
    </body>
</html>
<?php
include_once("./includes/check-user.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Carzonrent Admin</title>
        <?php
        include_once("./includes/cache-func.php");
        include_once("./includes/header-css.php");
        include_once("./includes/header-js.php");
        include_once("./includes/config.php");
        include_once("./classes/bb-mysql.class.php");
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/ajax.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/searchajax.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/admin.js" type="text/javascript"></script>	
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js" type="text/javascript"></script>	  
        <script>
            $(function () {
                var dateToday = new Date();
                dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                var dateNextDay = new Date();
                dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: dateToday, maxDate: dateNextDay});
            });
        </script>     
    </head>
    <body>
        <?php include_once("./includes/header.php"); ?>
        <div id="middle">	
            <div class="yellwborder">
                <div style="padding:7px 0px 7px 10px; width: 98% !important;" class="magindiv">
                    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Search Report</h2>
                </div>
            </div>
            <div class="main" style="width:100%;">
                <br><br>
                        <div style="margin:0 auto;width:99%;">
                            <?php include_once("./includes/search-menu.php"); ?>
                            <form id="frmSearch" name="frmSearch" action="search-report.php" method="get">
                                <table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
                                    <tbody>
                                        <tr>
                                            <?php
                                            $db = new MySqlConnection(CONNSTRING);
                                            $db->open();

                                            $sDate = "";
                                            if (isset($_GET["startdate"]) && $_GET["startdate"] != "") {
                                                $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
                                            } else {
                                                $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
                                                //print_r($maxDate);
                                                $sDate = date_create($maxDate[0]["max_date"]);
                                            }

                                            $eDate = "";
                                            if (isset($_GET["enddate"]) && $_GET["enddate"] != "") {
                                                $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
                                            } else {
                                                $eDate = $sDate;
                                            }
                                            if (isset($_REQUEST["ddlOrigin"]) && $_REQUEST["ddlOrigin"] != "") {
                                                $location = $_REQUEST["ddlOrigin"];
                                            } else {
                                                $location = '';
                                            }
                                            ?>
                                            <td><label>Start Date</label></td>
                                            <td>
                                                <span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            <td><label>End Date </label>
                                            </td>
                                            <td><span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            <td><label>Location</label></td>
                                            <td>
                                                <?php
                                                $db = new MySqlConnection(CONNSTRING);
                                                $db->open();
                                                $sql = "SELECT * FROM cor_location";
                                                $r = $db->query("query", $sql);
                                                if (!array_key_exists("response", $r)) {
                                                    ?>
                                                    <select class="ddlS" name="ddlOrigin" id="ddlOriginSF" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <option value="">Select One....</option>   <?php
                                                        for ($i = 0; $i < count($r); $i++) {
                                                           if($_REQUEST['ddlOrigin']==$r[$i]["city_id"]){
                                                               $sel = 'selected="selected"';
                                                           }else{
                                                               $sel = '';
                                                           } ?>
                                                        
                                                            <option <?php echo $sel;?> value="<?php echo $r[$i]["city_id"]; ?>"><?php echo $r[$i]["CityName"]; ?></option>
                                                            <?php ?>

                                                        <?php } ?>

                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
                                            </td>
                                            <td valign="middle">					
                                                <a href="download-search-report.php?sdate=<?php echo $sDate->format('Y-m-d'); ?>&edate=<?php echo $eDate->format('Y-m-d'); ?>">Download</a>
                                            </td>
                                            <td>
                                                <?php 
                                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']!=''){
                                                        $db = new MySqlConnection(CONNSTRING);
                                                        $db->open();
                                                        $cityid = $_REQUEST['ddlOrigin'];
                                                        $sql = "SELECT * FROM customer_search where origin_code = '$_REQUEST[ddlOrigin]' and (entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."')";
                                                        $rx = $db->query("query", $sql);
                                                        if (!array_key_exists("response", $rx)) {
                                                            $hour = '';$days = '';
                                                            for ($i = 0; $i < count($rx); $i++) {

                                                                if($rx[$i]['rental_type']=='Hourly'){
                                                                    $hour +=  $rx[$i]['duration'];
                                                                    ?>
                                                                   
                                                                <?php }
                                                                else{
                                                                    
                                                                    $days +=  $rx[$i]['duration'];
                                                                    ?>
                                                        
                                                                    <?php }
                                                                   
                                                            } 
                                                            ?>
                                                        <td style="padding-left: 30px;"><span class="dataLabel" style="min-width: 100px !important;">Total Hours: <b style="color:#333;"><?php echo ($hour=='' || !$hour)?'0':$hour;?></b></span></td> 
                                                                    <td><span class="dataLabel" style="min-width: 100px !important;">Total Days: <b style="color:#a5109c;"><?php echo ($days=='' || !$days)?'0':$days;?></b></span></td>
                                                                <?php
                                                        }
                                                    }?>
                                                
                                            </td>
                                            
                                        </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </form>
                            <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
                                <tbody id="searchData">			
                                    <tr>
                                        <td align="center" width="5%" class="search_table">SrNo.</td>
                                        <td align="center" width="10%" class="search_table">Search Date</td>
                                        <td align="center" width="10%" class="search_table">Type</td>
                                        <td align="center" width="10%" class="search_table">Origin</td>
                                        <td align="center" width="10%" class="search_table">Destination</td>
                                        <td align="center" width="10%" class="search_table">Travel Date</td>
                                        <td align="center" width="10%" class="search_table">Duration</td>
                                        <td align="center" width="10%" class="search_table">Duration Type</td>
<!--                                        <td align="center" width="10%" class="search_table">Details</td>-->
                                        <td align="center" width="10%" class="search_table">Distance</td>
                                        <td align="center" width="10%" class="search_table">Avg Distance</td>
                                        <td align="center" width="5%" class="search_table">IP</td>
                                        <!--<td align="center" width="10%" class="search_table">BROWSER</td>-->
                                    </tr>			    
                                    <?php
                                    /* echo $data="select entry_date,tour_type,origin_name, destination_name, 
                                      pickup_date, duration, distance, ip, user_agent from customer_search
                                      where (entry_date BETWEEN '", $sDate->format('Y-m-d'),"' AND '", $eDate->format('Y-m-d'),"')
                                      ORDER by unique_id DESC LIMIT ",0,",",50; */
                                    //echo $data = "select cs.entry_date,cs.tour_type,cs.origin_name, cs.destination_name, cs.pickup_date, cs.duration, cs.rental_type, cs.distance, cs.ip, cs.user_agent,GROUP_CONCAT(uc.next_available_date),GROUP_CONCAT(uc.car_model) from customer_search AS cs LEFT JOIN unavailable_cars uc ON cs.unique_id = uc.customer_search_id where (entry_date BETWEEN '", $sDate->format('Y-m-d'),"' AND '", $eDate->format('Y-m-d'),"') group by cs.unique_id ORDER by cs.unique_id DESC LIMIT ",0,",",50;
                                      
                                    $db = new MySqlConnection(CONNSTRING);
                                    $db->open();
                                    
                                    
                                    if (isset($_REQUEST["ddlOrigin"]) && $_REQUEST["ddlOrigin"] != "") {
                                        $r = $db->query("stored procedure", "cor_search_report_location('" . $sDate->format('Y-m-d') . "','" . $eDate->format('Y-m-d') . "','".$_REQUEST['ddlOrigin']."')");
                                        $sql = "select cs.unique_id, cs.entry_date,cs.tour_type,cs.origin_name, cs.destination_name, cs.pickup_date, cs.duration, cs.rental_type, cs.distance, cs.ip, cs.user_agent,GROUP_CONCAT(uc.next_available_date SEPARATOR ', '),GROUP_CONCAT(uc.car_model SEPARATOR ', ') from customer_search AS cs LEFT JOIN customer_search_cars uc ON cs.unique_id = uc.customer_search_id where (cs.entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."') AND cs.origin_code=".$_REQUEST['ddlOrigin']." group by cs.unique_id ORDER by cs.unique_id DESC";
                                        $qst = $db->query("query", $sql);
                                        
                                    } else {
                                        $r = $db->query("stored procedure", "cor_search_report('" . $sDate->format('Y-m-d') . "','" . $eDate->format('Y-m-d') . "')");
                                        $sql = "select cs.unique_id, cs.entry_date,cs.tour_type,cs.origin_name, cs.destination_name, cs.pickup_date, cs.duration, cs.rental_type, cs.distance, cs.ip, cs.user_agent,GROUP_CONCAT(uc.next_available_date SEPARATOR ', '),GROUP_CONCAT(uc.car_model SEPARATOR ', ') from customer_search AS cs LEFT JOIN customer_search_cars uc ON cs.unique_id = uc.customer_search_id where (cs.entry_date BETWEEN '".$sDate->format('Y-m-d')."' AND '".$eDate->format('Y-m-d')."') group by cs.unique_id ORDER by cs.unique_id DESC";
                                        $qst = $db->query("query", $sql);
                                        
                                        
                                    }
                                   // print_r(count($qst));die;
                                    if (!array_key_exists("response", $r)) {
                                        for ($i = 0; $i < count($r); $i++) {
                                            $style = "";
                                            if ($r[$i]["duration"] != 0)
                                                $avgDist = ceil($r[$i]["distance"] / $r[$i]["duration"]);
                                            if ($i % 2 == 0)
                                                $style = "background-color:#f1f1f1;";
                                            else
                                                $style = "background-color:#dddada;";
                                            //if($avgDist >=150 && $avgDist <= 500)
                                            //    $style .= "color:#0f0;font-weight:bold;";
                                            //echo "<pre>";print_r($r[$i]);//die;
                                            ?>
                                            <tr style="<?php echo $style ?>">
                                                <td style="padding:5px !important;" name = "seqno"><?php echo ($i + 1) ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["entry_date"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["tour_type"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["origin_name"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["destination_name"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["pickup_date"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["duration"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["rental_type"] ?></td>
<!--                                                <td style="padding:5px !important;"><a href='javascript: void(0)' onclick='javascript: _searchDetails(<?php echo $r[$i]["unique_id"]; ?>)' >Details</a></td>-->
                                                <td style="padding:5px !important;"><?php echo $r[$i]["distance"] ?></td>
                                                <td style="padding:5px !important;"><?php echo $avgDist; ?></td>
                                                <td style="padding:5px !important;"><?php echo $r[$i]["ip"] ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else {
                                        ?>
                                        <tr style="background-color:#f1f1f1;">
                                            <td align="center" colspan="10">No data found.</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>

                            <?php if (count($qst) > 50) { ?>
                                <table width="100%" cellspacing="1" cellpadding="5" border="0"  style="float: left;margin:15px 0 0 0px;">
                                    <tbody id = "loadmore">
                                        <tr>
                                            <td align="right">
                                                <?php if($_REQUEST['ddlOrigin']!=''){ ?>
                                                <a href="javascript:void(0);" onclick="javascript: _loadMoreSearchWithcity('<?php echo $sDate->format('Y-m-d') ?>', '<?php echo $eDate->format('Y-m-d') ?>',<?php echo $i+1 ?>,'<?php echo $_REQUEST['ddlOrigin'] ?>')">LOAD MORE</a>
                                                <?php }else{
                                                    ?>
                                                        <a href="javascript:void(0);" onclick="javascript: _loadMoreSearch('<?php echo $sDate->format('Y-m-d') ?>', '<?php echo $eDate->format('Y-m-d') ?>',<?php echo $i+1 ?>)">LOAD MORE</a>
                                                            <?php
                                                } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            unset($r);
                            ?>
                        </div>		
                        <div class="clr"></div>
                        </div>
                        <p>&nbsp;</p>	    	
                        </div>
                        <?php
                        include_once("./includes/footer.php");
                        ?>
                        </body>
                        </html>
<?php
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
	<title>Carzonrent Admin</title>
            <?php                
		include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	<script type="text/javascript" language="javascript">
	    function _validateAdminBooking(){
		var cnf = false;
		cnf = confirm("Are you sure that you want to create this booking?\r\nThis would send an email to customer.\r\nContinue?");
		if(cnf){
		    cnf = confirm("Confirm booking?");
		    if(cnf)
		    document.getElementById('frmAdminBooking').submit();
		}
	    }
	</script>
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> Booking</h2>
		</div>
	    </div>
	    <div class="main">
		<br><br>
		<div class="businessEnquiry">
		    <form name="frmAdminBooking" id="frmAdminBooking" method="post" action="./admin-create-booking.php">
			<input type="hidden" value="<?php echo $_GET["bdir"]; ?>" name="hdRURL" id="hdRURL">
			<input type="hidden" value="<?php echo $_GET["corid"]; ?>" name="corid" id="corid">
			<table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin-left:38px;font-size:14px;color:#323232;">                
			    <tbody>                            
                                <?php
				    $corid = $_GET["corid"];
				    $base_xml_link = "../" . $_GET["bdir"];
				    $isPaid = $_GET["ispaid"];
				    $isBooked = $_GET["isbooked"];
				    $isError = $_GET["iserror"];
				    $passDetailXML = $base_xml_link . "/" . $corid . ".xml";
				    $bookDetailXML = str_ireplace("pre-trans", "trans", $base_xml_link) . "/" .  $corid . "-B.xml";
				    $errorDetailXML = str_ireplace("pre-trans", "trans", $base_xml_link) . "/" .  $corid . "-E.xml";
                                ?>
                                            <tr>
                                                <td colspan="2" style="padding:0px 0px 10px 0px;">You are here: 
				<?php
						$dirs = explode("/", $base_xml_link);
						$base_xml_link = "xml";
						for($i = 0; $i < count($dirs); $i++){
						    if($i > 1){
							$base_xml_link .= "/" . $dirs[$i];
							echo "<a href='booking.php?dir=" . $base_xml_link . "'>" . $dirs[$i] . "</a> / ";
						    }
						    else
							echo $dirs[$i] . " / ";
						}
						echo $corid;
				?>		
						</td>
                                            </tr>                               	    
			    </tbody>
			</table>
			<?php
			    if(strtolower($isPaid) != "" && file_exists($passDetailXML)){
				if(strtolower($isBooked) == "yes" && file_exists($bookDetailXML)){
				    try{
					$xmlDoc = new DOMDocument();
					$xmlDoc->load($bookDetailXML);
					$passData = $xmlDoc->getElementsByTagName("cortrans");
					foreach($passData as $data){
					    $cData = $data->childNodes;
					    for($i = 0; $i < $cData->length; $i++){
						$optText = $cData->item($i)->nodeName;
						$optValue = $cData->item($i)->nodeValue;
			?>
			    <table cellspacing="1" cellpadding="5" border="0" align="left" style="margin-left:38px;font-size:14px;color:#323232;">
                                <tr>
                                    <td style="padding:5px;font-weight:bold;"><br />Booking ID:</td>
				    <td style="padding:5px;"><input type="text" id="<?php echo $optText; ?>" name="<?php echo $optText; ?>" value="<?php echo $optValue; ?>" /></td>
                                </tr>
			    </table>
                        <?php
					    }
					}
				    }
				    catch(Exception $e){}
				    $xmlDoc = null;
				}
				if(strtolower($isError) == "yes" && file_exists($errorDetailXML)){
				    try{
					$xmlDoc = new DOMDocument();
					$xmlDoc->load($errorDetailXML);
					$passData = $xmlDoc->getElementsByTagName("cortrans");
					foreach($passData as $data){
					    $cData = $data->childNodes;
					    for($i = 0; $i < $cData->length; $i++){
						$optText = $cData->item($i)->nodeName;
						$optValue = $cData->item($i)->nodeValue;
			?>
			    <table cellspacing="1" cellpadding="5" border="0" align="left" style="margin-left:38px;font-size:14px;">
                                <tr>
                                    <td style="padding:5px;font-weight:bold;"><br /><?php echo $optText; ?></td>
				    <td style="padding:5px;"><input type="text" id="<?php echo $optText; ?>" name="<?php echo $optText; ?>" value="<?php echo $optValue; ?>" /></td>
                                </tr>
			    </table>
                        <?php
					    }
					}
				    }
				    catch(Exception $e){}
				    $xmlDoc = null;
				}
			?>
			    <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin-left:38px;font-size:14px;">
                                <tr>
                                    <td colspan="2" style="padding:0px 0px 10px 0px;font-weight:bold;"><br />Passenger Details</td>
                                </tr>
                        <?php
			    try{
                                $xmlDoc = new DOMDocument();
                                $xmlDoc->load($passDetailXML);
                                $passData = $xmlDoc->getElementsByTagName("ccvtrans");
                                foreach($passData as $data){
                                    $cData = $data->childNodes;
                                    for($i = 0; $i < $cData->length; $i++){
                                        $optText = $cData->item($i)->nodeName;
                                        $optValue = $cData->item($i)->nodeValue;
                                        if($i % 3 == 0){
                                            if($i == 0){
                         ?>
                                            <tr>
                        <?php
                                            } else {
                        ?>
                                            </tr><tr>
                        <?php
                                            }
                                        }
                        ?>
                                        <td style="padding:5px !important;"><?php echo $optText; ?></td>
                                        <td style="padding:5px !important;"><input type="text" id="<?php echo $optText; ?>" name="<?php echo $optText; ?>" value="<?php echo $optValue; ?>" /></td>
                        <?php
                                    }
                                }
                                //}
                                $xmlDoc = null;
			    }
			    catch(Exception $e){print_r($e);}
			?>
			    </table>
			<?php
			    }
			?>
                        <?php
			    if(strtolower($isBooked) != "" && file_exists(str_ireplace("pre-trans", "trans", $passDetailXML))){
			?>
			    <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin-left:38px;font-size:14px;">
                                <tr>
                                    <td colspan="2" style="padding:0px 0px 10px 0px;font-weight:bold;"><br />Booking Details</td>
                                </tr>
                        <?php
                        try{
			    //echo str_ireplace("pre-trans", "trans", $passDetailXML);
                                $xmlDoc = new DOMDocument();
                                $xmlDoc->load(str_ireplace("pre-trans", "trans", $passDetailXML));
                                $passData = $xmlDoc->getElementsByTagName("ccvtrans");
                                foreach($passData as $data){
                                    $cData = $data->childNodes;
                                    for($i = 0; $i < $cData->length; $i++){
                                        $optText = $cData->item($i)->nodeName;
                                        $optValue = $cData->item($i)->nodeValue;
                                        if($i % 3 == 0){
                                            if($i == 0){
                         ?>
                                            <tr>
                        <?php
                                            } else {
                        ?>
                                            </tr><tr>
                        <?php
                                            }
                                        }
                        ?>
                                        <td style="padding:5px !important;"><?php echo $optText; ?></td>
                                        <td style="padding:5px !important;"><input type="text" id="<?php echo $optText; ?>" name="<?php echo $optText; ?>" value="<?php echo $optValue; ?>" /></td>
                        <?php
                                    }
                                }
                                //}
                                $xmlDoc = null;
			    }
			    catch(Exception $e){print_r($e);}
			?>
			    </table>
			<?php
			    }
			    if(strtolower($isBooked) != "yes"){
			?>
			<button id="btnBookNow1" class="adminBookingBtn" type="button" onclick="javascript: _validateAdminBooking();" value="Book Now" name="btnBookNow" />
			<?php
			    }
			?>
		    </form>
		</div>
		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
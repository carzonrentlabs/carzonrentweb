<?php
error_reporting(0);


function browser_info($agent = null) {
    // Declare known browsers to look for
    $known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
        'konqueror', 'gecko');

    // Clean up agent and build regex that matches phrases for known browsers
    // (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
    // version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
    $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

    // Find all phrases (or return empty array if none found)
    if (!preg_match_all($pattern, $agent, $matches))
        return array();

    // Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
    // Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
    // in the UA).  That's usually the most correct.
    $i = count($matches['browser']) - 1;
    return array($matches['browser'][$i] => $matches['version'][$i]);
}

function filter_querystring($query_string, $arrFields) {
    if ($query_string != "") {
        $qString = "";
        if (count($arrFields) > 0) {
            $qsParams = explode("&", $query_string);
            for ($q = 0; $q < count($qsParams); $q++) {
                $qsParam = explode("=", $qsParams[$q]);
                $iF = -1;
                for ($f = 0; $f < count($arrFields); $f++) {
                    if ($qsParam[0] == $arrFields[$f]) {
                        $iF = $f;
                        break;
                    }
                }
                if ($iF == -1)
                    $qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
            }
        } else {
            $qString = $query_string;
        }
    } else
        $qString = "";
    return $qString;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Carzonrent Admin</title>
        <?php
        include_once("./includes/cache-func.php");
        include_once("./includes/header-css.php");
        include_once("./includes/header-js.php");
        include_once("./includes/config.php");
        include_once("./classes/bb-mysql.class.php");

        include_once("./classes/cor.ws.class.php");
        include_once("./classes/cor.xmlparser.class.php");
        
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
        <script type="text/javascript">
            qsArray = new Array();
        </script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/chart.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                var minSDate = new Date();
                minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                var maxEDate = new Date();
                maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: minSDate, maxDate: maxEDate});
            });
        </script>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
<?php include_once("./includes/header.php"); ?>
        <div id="middle">	
            <div class="yellwborder">
                <div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
                    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> MIS Partners</h2>
                </div>
            </div>
            <div class="main" style="width:100%;">
                <div>
                    <?php
                    include_once("./includes/mis-partners-menu.php");
                    $url = "./mis-rentalcars.php";
                    $dlndURL = "./mis-rentalcars-download.php";
                    $db = new MySqlConnection(CONNSTRING);
                    $db->open();

                    $recPerPage = 100;
                    $maxPageNum = 5;
                    $cPage = 1;
                    $sDate = date_create(date("Y-m-d"));
                    $eDate = date_create(date("Y-m-d"));
                    $fld = "";
                    $val = "";
                    $sortFld = "uid";
                    $sortMode = "desc";
                    $sortDType = "varchar";
                    $isDtInc = false;
                    $isFiltered = false;
                    $arrFields = array("source" => "Source", "coric" => "CORIC", "booking_id" => "Booking ID", "tour_type" => "Tour Type", "package_id" => "Package ID", "car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID", "promotion_code" => "Discount Code");

                    if (isset($_GET["pg"]) && $_GET["pg"] != "")
                        $cPage = $_GET["pg"];

                    if (isset($_GET["sd"]) && $_GET["sd"] != "") {
                        $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
                        $isFiltered = true;
                        $isDtInc = true;
                    }

                    if (isset($_GET["ed"]) && $_GET["ed"] != "") {
                        $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
                        $isFiltered = true;
                        $isDtInc = true;
                    }

                    if (isset($_GET["f"]) && $_GET["f"] != "") {
                        $fld = $_GET["f"];
                        $isFiltered = true;
                    }

                    if (isset($_GET["q"]) && $_GET["q"] != "") {
                        $fVal = $_GET["q"];
                        $isFiltered = true;
                        if ($fld != "") {
                            if ($fld == "payment_mode") {
                                if (trim(strtolower($fVal)) == "online")
                                    $fVal = 1;
                                elseif (trim(strtolower($fVal)) == "pay to driver")
                                    $fVal = 2;
                            } elseif ($fld == "payment_status") {
                                if (trim(strtolower($fVal)) == "success")
                                    $fVal = 1;
                                elseif (trim(strtolower($fVal)) == "gateway")
                                    $fVal = 2;
                                elseif (trim(strtolower($fVal)) == "payment")
                                    $fVal = 3;
                                elseif (trim(strtolower($fVal)) == "failure" || trim(strtolower($fVal)) == "cancelled")
                                    $fVal = 4;
                                elseif (trim(strtolower($fVal)) == "unauthorised")
                                    $fVal = 5;
                            }
                        }
                    }

                    if (isset($_GET["inc"]) && $_GET["inc"] != "") {
                        $isDtInc = true;
                        $isFiltered = true;
                    }

                    if (isset($_GET["sf"]) && $_GET["sf"] != "")
                        $sortFld = $_GET["sf"];

                    if (isset($_GET["sm"]) && $_GET["sm"] != "")
                        $sortMode = $_GET["sm"];

                    if (isset($_GET["dt"]) && $_GET["dt"] != "")
                        $sortDType = "CONVERT(" . $sortFld . ", " . $_GET["dt"] . ")";
                    else {
                        if (isset($_GET["sf"]) && $_GET["sf"] != "")
                            $sortDType = $sortFld;
                        else
                            $sortDType = "CONVERT(" . $sortFld . ", unsigned integer)";
                    }
                    //$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    
                    ?>


                    <table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
                        <tr>
                            <td>
                                <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
                                    <table cellspacing="1" cellpadding="5" border="0" align="left">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left: 10px;"><label>Start Date</label></td>
                                                <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 10px;">
                                                    <span class="datepick">
                                                        <input type="text" size="12" autocomplete="off" id="sd" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if (document.getElementById('inc').checked) {
                                                                document.getElementById('sds').value = this.value;
                                                            }" />						    
                                                    </span>
                                                </td>
                                                <td style="padding-left: 10px;"><span class="datepick">
                                                        <input type="text" size="12" autocomplete="off" id="ed" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" onchange="javascript: if (document.getElementById('inc').checked) {
                                                                        document.getElementById('eds').value = this.value;
                                                                    }" />						    
                                                    </span>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" id="frm1Btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>    
                            </td>
                            <td align="right">
                                
                                                    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
                                                        <tr>
                                                            <td><a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                            <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>" style="color:#128a30;">Download</a></td>
                                                            <?php
                                                            if ($numOfPage > 1) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
                                                                <?php
                                                            }
                                                            if ($isDtInc || $isFiltered) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
    <?php
}
?>
                                                        </tr>
                                                    </table>
                                               
                                
<!--                                <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
<?php
if (isset($_GET["sd"])) {
    if ($_GET["sd"] == "") {
        ?>
                                            <input type="hidden" id="sds" name="sd" value="" />
                                            <?php
                                        } else {
                                            ?>
                                            <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" id="sds" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" />
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (isset($_GET["ed"])) {
                                        if ($_GET["ed"] == "") {
                                            ?>
                                            <input type="hidden" id="eds" name="ed" value="" />
                                            <?php
                                        } else {
                                            ?>
                                            <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" id="eds" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" />
                                        <?php
                                    }
                                    ?> 
                                    <table cellspacing="1" cellpadding="5" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding-right: 10px;"><label>Search</label></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" size="22" autocomplete="off" name="q" id="q" class="txtSBox" placeholder="Search query" value="<?php if (isset($_GET["q"]) && $_GET["q"] != "") {
                                        echo $_GET["q"];
                                    } ?>" /></td>
                                                <td>
                                                    <select name="f" id="f" class="ddlS" style="margin-left: 10px;">
                                                        <option>Select field&nbsp;</option>
<?php
foreach ($arrFields as $key => $val) {
    $sel = "";
    if ($key == $fld)
        $sel = "selected='selected'";
    ?>
                                                            <option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                        ?>

                                                    </select>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" id="inc" name="inc" value="1" <?php if (!isset($_GET["sd"])) {
                                                            echo "checked=\"checked\"";
                                                        } elseif (isset($_GET["sd"]) && $_GET["sd"] != "") {
                                                            echo "checked=\"checked\"";
                                                        } ?> onclick="if (this.checked) {
                                                                            document.getElementById('sds').value = document.getElementById('sd').value;
                                                                            document.getElementById('eds').value = document.getElementById('ed').value;
                                                                        } else {
                                                                            document.getElementById('sds').value = '';
                                                                            document.getElementById('eds').value = '';
                                                                        }" <?php if ($isDtInc) {
                                                                    echo "checked='checked'";
                                                                } ?> />
                                                            </td>
                                                            <td>Include date filter</td>
                                                        </tr>
                                                    </table>
                                                </td> 
                                                <td colspan="2" valign="middle" align="right">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 5px;">
                                                        <tr>
                                                            <td><a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                            <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php if (filter_querystring($_SERVER["QUERY_STRING"], array('pg')) != "") {
                                                                echo "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg'));
                                                            } ?>" style="color:#128a30;">Download</a></td>
                                                            <?php
                                                            if ($numOfPage > 1) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/download.png" alt="Download" title="Download" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $dlndURL; ?><?php echo filter_querystring($_SERVER["QUERY_STRING"], array('pg')) == "" ? "?pg=" . $cPage : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('pg')) . "&pg=" . $cPage; ?>" style="color:#128a30;">Download This Page</a></td>
                                                                <?php
                                                            }
                                                            if ($isDtInc || $isFiltered) {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#ee2117;">Remove filters</a></td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td>&nbsp;&nbsp;&nbsp;<a href="<?php echo $url; ?>"><img src="<?php echo corWebRoot; ?>/lenap_panel/img/refresh.png" alt="Remove filters" title="Remove filters" width="16px" border="0" /></a></td>
                                                                <td>&nbsp;<a href="<?php echo $url; ?>" style="color:#2026a4;">Refresh</a></td>
    <?php
}
?>
                                                        </tr>
                                                    </table>


                                                </td>
                                        </tbody>
                                    </table>
                                </form>-->
                            </td>
                        </tr>
                    </table>

                    <table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
                        <thead>
                            <tr>
                                <th align="left">#</th>
                                
                                <th align="left">Source</th>
                                <th align="left">Booking ID <?php if ($sortFld == "booking_id") { ?><img src="<?php echo corWebRoot; ?>/lenap_panel/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                                <th align="left"> Destination</th>
                                <th align="left">Fare </th>
                                <th align="left">Travel On</th>
                                <th align="left">Booked On</th>
                                <th align="left">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
                    $companyname = 'RentalCars';
                    $date = date_create(date('Y-m-d'));
                    //date_sub($date, date_interval_create_from_date_string('30 days'));
                    $fromdate=date_format($date, 'Y-m-d');
                    $mainfromDate=explode("-",$fromdate);
                    //$formdate=$mainfromDate[1]."-".$mainfromDate[2]."-".$mainfromDate[0];
                    $formdate = $fromdate;


                    $todate=date('Y-m-d');
//                    $convertdate=explode("-",$todate);
//                    $maintodate=$convertdate[1]."-".$convertdate[2]."-".$convertdate[0];
                    $maintodate = $todate;
                    if(isset($_GET["sd"]) && $_GET["sd"] != ""){
                        $fdate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
                        //$formdate=date_format($fdate, 'm-d-Y');
                        $formdate=date_format($fdate, 'Y-m-d');
                    }
		       
                    if(isset($_GET["ed"]) && $_GET["ed"] != ""){
                        $edate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
                        //$maintodate=date_format($edate, 'm-d-Y');
                        $maintodate=date_format($edate, 'Y-m-d');
                    }
                    //echo $formdate.'++++++++++'.$maintodate;
                    
                    $WebRequestData1 = array();
                    $WebRequestData1["ClientID"] = '2713';//2219++++2713;
                    $WebRequestData1["FromDate"] = $formdate;
                    $WebRequestData1["ToDate"] = $maintodate;
                    $WebRequestData1["DateFilterYN"] = 'Y';
                    //$WebRequestData1["SearchText"] = '';
                    //$WebRequestData1["SearchColumnName"] = '';
                    //$WebRequestData1["Branch"] = "Bangalore";
                    $soapClient = new COR();
		    $myXML = new CORXMLList();
		    $RecordResult = $soapClient->_CORDashBoardSummary($WebRequestData1);
		    //echo "<pre>";print_r($RecordResult);
			
			
                  
                    if ($RecordResult->{'DashBoardSummaryResult'}) {
                        $result = $RecordResult->{'DashBoardSummaryResult'}->{'any'};
						
			$resultany = $myXML->xml2ary($result);
			$countArray = count($resultany);
                        if (is_array($resultany)) {
                            $i=1;
                            foreach ($resultany as $valarray) {
                               //echo "<pre>";print_r($valarray);
                                $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showRentalcarsDetails('".$valarray['trackid'] . "')\" title=\"Click on view details of " . $valarray['trackid'] . "\">Details</a>";
                                ?>
                                <tr class="ev">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ucwords($valarray['trackid']); ?></td>
                                    <td><?php echo $valarray['bookingid']; ?></td>
                                    <td><?php echo $valarray['pickupadd']; ?></td>
                                    <td><?php echo $valarray['indicatedprice']; ?></td>
                                    <td><?php echo date_format(date_create($valarray['pickupdate']),"d M Y H:i:s"); ?></td>
                                    <td><?php echo date_format(date_create($valarray['createdate']),"d M Y H:i:s");?></td>
<!--                                    <td><?php echo $valarray->Tripstatus;?></td>-->
                                    <td><?php echo $vTxt; ?></td>
                                </tr>
                                <?php 
                            $i++;}
                            ?>
                            </td>

                            </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
                        <tr>
                            <td align="right" style="margin-right: 10px;">
        <?php echo $pagingHTML; ?>
                            </td>
                        </tr>
                    </table>
<?php
unset($r);
?>
                </div>		
                <div class="clr"></div>
            </div>
            <p>&nbsp;</p>	    	
        </div>
<?php
include_once("./includes/footer.php");
?>
    </body>
</html>
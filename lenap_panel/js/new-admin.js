_showDetails = function(uid, coric){
    _showPopup();
    var ar = new Array();
    ar[0] = new Array("uid", uid);
    ar[1] = new Array("coric", coric);
    ar[2] = new Array("rnd", Math.random()*99999);
    
    var aj = new _ajax();
    aj.fileName = "./ajax/get-booking-details.php";
    aj.callMethod = "get";
    aj.callParam = ar;
    aj.isAsynchronus = true;
    aj._query(function(p){_waitBookingDetails(p)}, function(p, r){_responseBookingDetails(p, r)});
};
_showIxigoDetails = function(uid, coric){
    _showPopup();
    var ar = new Array();
    ar[0] = new Array("uid", uid);
    ar[1] = new Array("coric", coric);
    ar[2] = new Array("rnd", Math.random()*99999);
    
    var aj = new _ajax();
    aj.fileName = "./ajax/get-ixigobooking-details.php";
    aj.callMethod = "get";
    aj.callParam = ar;
    aj.isAsynchronus = true;
    aj._query(function(p){_waitBookingDetails(p)}, function(p, r){_responseBookingDetails(p, r)});
};
_showRentalcarsDetails = function(coric){
    _showPopup();
    var ar = new Array();
    ar[0] = new Array("coric", coric);
    ar[1] = new Array("rnd", Math.random()*99999);
    
    var aj = new _ajax();
    aj.fileName = "./ajax/get-rentalbooking-details.php";
    aj.callMethod = "get";
    aj.callParam = ar;
    aj.isAsynchronus = true;
    aj._query(function(p){_waitBookingDetails(p)}, function(p, r){_responseBookingDetails(p, r)});
};
_waitBookingDetails = function(ar){
    document.getElementById("cnt").innerHTML = "Wait while we fetch booking details for " + ar[1][1];
};

_responseBookingDetails = function(ar, rs){
    document.getElementById("cnt").innerHTML = rs;
};
_waitSearchDetails = function(ar){
    document.getElementById("cnt").innerHTML = "Wait while we fetch searching details for " + ar[1][1];
};
_searchDetails = function(searchId){
    
     _showPopup();
    var ar = new Array();
    ar[0] = new Array("searchId", searchId);
    ar[1] = new Array("rnd", Math.random()*99999);
    var aj = new _ajax();
    
    aj.fileName = "./ajax/get-search-details.php";
    aj.callMethod = "get";
    aj.callParam = ar;
    aj.isAsynchronus = true;
    
    aj._query(function(p){_waitSearchDetails(p)}, function(p, r){_responseBookingDetails(p, r)});
};

_showPopup = function(){
    var d;
    try{
        d = document.createElement("<div id='disableLayer' style='background-color:#000;position:fixed;opacity:0.2;z-index:99;top:0;left:0;position:fixed;cursor:pointer;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "disableLayer";
        d.style.position = "fixed";
        d.style.height = "100%";
        d.style.width = "100%";
        d.style.left = "0";
        d.style.top = "0";
        d.style.backgroundColor = "#000";
        d.style.opacity = 0.2;
        d.style.zIndex = 99;
        d.style.cursor = 'pointer';
    }
    document.body.appendChild(d);
    var h = parseInt(window.innerHeight * 0.92);
    var w = parseInt(window.innerWidth * 0.9);
    var postop = parseInt(window.innerHeight * 0.5) - (h / 2);
    var posleft = parseInt(window.innerWidth * 0.5) - (w / 2);
    var d;
    try{
        d = document.createElement("<div id='popupContainer' style='background-color:#fff;opacity:1;position:relative;z-index:100;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupContainer";
        d.style.position = "fixed";
        d.style.backgroundColor = "#fff";
        d.style.top = postop + "px";
        d.style.left = posleft + "px";
        d.style.zIndex = "100";
    }
    document.body.appendChild(d);
    
    try{
        d = document.createElement("<div id='popupPage' style='overflow: auto;background-color:#fff;opacity:1;position:absolute;z-index:101;height:" + h + "px;width:" + w + "px;'></div>");
    }
    catch(e){
        d = document.createElement("div");
        d.id = "popupPage";
        d.style.position = "absolute";
        d.style.height = h + "px";
        d.style.width = w + "px";
        d.style.backgroundColor = "#fff";
        d.style.borderRadius = "5px";
        d.style.padding = "10px";
        d.style.boxShadow = "2px 5px 10px #333333";       
        d.style.opacity = 1;
        d.style.zIndex = "101";
        d.style.overflow = "auto";
    }
    document.getElementById('popupContainer').appendChild(d);
    
    try{
        d = document.createElement("<img id='imgClose' src='./img/close.png' style='position:relative;z-index:101;top:-15px;left:-15px;cursor:pointer;' />");
    }
    catch(e){
        d = document.createElement("img");
        d.id = "imgClose";
        d.style.position = "relative";
        d.style.zIndex = "101";
        d.style.left = "-15px";
        d.style.top = "-15px";
        d.style.cursor = "pointer";
        d.src = './img/close.png';
    }
    
    document.getElementById('popupContainer').appendChild(d);
    
    try{
        d = document.createElement("<div id=\"cnt\" style=\"z-index:102;top:0;left:0;position:absolute;\" ></div>");
    }
    catch(e)
    {
        d = document.createElement("div"); 
        d.id = "cnt";
        d.style.height = (h - 5) + "px";
        d.style.width = (w - 5) + "px";
        d.style.zIndex = "102";
    }
    document.getElementById('popupPage').appendChild(d);
    document.getElementById('imgClose').onclick = (function(){
        document.body.removeChild(document.getElementById('disableLayer'));
        document.body.removeChild(document.getElementById('popupContainer'));
    });
    document.getElementById('disableLayer').onclick = (function(){
        document.body.removeChild(document.getElementById('disableLayer'));
        document.body.removeChild(document.getElementById('popupContainer'));
    });
}
_closePopup = function(){
    document.body.removeChild(document.getElementById('disableLayer'));
    document.body.removeChild(document.getElementById('popupContainer'));
};

_createBooking = function(){
    var cnf = false;
    cnf = confirm("Are you sure that you want to create this booking?\r\nThis would send an email to customer.\r\nContinue?");
    if (cnf) {
        cnf = confirm("Confirm booking?");
        if (cnf) {
            var ar = new Array();
            ar[0] = new Array("pack_id", document.getElementById('txtPackageID').value);
            ar[1] = new Array("destination", document.getElementById('txtDestination').value);
            ar[2] = new Array("tourtype", document.getElementById('txtTourType').value);
            ar[3] = new Array("pick_date", document.getElementById('txtPickupDate').value);
            ar[4] = new Array("drop_date", document.getElementById('txtDropDate').value);
            ar[5] = new Array("disc_pc", document.getElementById('txtDiscountPC').value);
            ar[6] = new Array("pick_time", document.getElementById('txtPickupTime').value);
            ar[7] = new Array("drop_time", document.getElementById('txtDropTime').value);
            ar[8] = new Array("disc_amt", document.getElementById('txtDiscountAmount').value);
            ar[9] = new Array("full_name", document.getElementById('txtFullName').value);
            ar[10] = new Array("email_id", document.getElementById('txtEmailID').value);
            ar[11] = new Array("mobile", document.getElementById('txtMobile').value);
            ar[12] = new Array("cciid", document.getElementById('txtUserID').value);
            ar[13] = new Array("track_id", document.getElementById('txtTrackID').value);
            ar[14] = new Array("trans_id", document.getElementById('txtTransactionID').value);
            ar[15] = new Array("address", document.getElementById('txtAddress').value);
            ar[16] = new Array("remarks", document.getElementById('txtRemarks').value);
            ar[17] = new Array("add_serv", document.getElementById('txtAddServices').value);
            ar[18] = new Array("add_serv_amt", document.getElementById('txtAddCharges').value);
            ar[19] = new Array("fare_amount", document.getElementById('txtFareAmount').value);
            ar[20] = new Array("pay_type", document.getElementById('txtPaymentType').value);
            ar[21] = new Array("pay_status", document.getElementById('txtPaymentStatus').value);
            ar[22] = new Array("distance", document.getElementById('txtDistance').value);
            ar[23] = new Array("visited_cities", document.getElementById('txtVisitedCities').value);
            ar[24] = new Array("source", document.getElementById('txtOriginCode').value);
            ar[25] = new Array("check_sum", document.getElementById('txtCheckSum').value);
            ar[26] = new Array("disc_code", document.getElementById('txtDiscountCode').value);
            ar[27] = new Array("promo_code", document.getElementById('txtPromotionCode').value);
            ar[28] = new Array("auth_desc", document.getElementById('txtAuthDesc').value);
            ar[29] = new Array("destination_id", document.getElementById('txtDestinationID').value);
            ar[30] = new Array("sublocation_id", document.getElementById('txtSubLocationID').value);
            ar[31] = new Array("duration", document.getElementById('txtDuration').value);
            ar[32] = new Array("add_service_cost_all", document.getElementById('add_service_cost_all').value);
            ar[33] = new Array("vat", document.getElementById('vat').value);
            ar[34] = new Array("airportCharges", document.getElementById('airportCharges').value);
            
            var aj = new _ajax();
            aj.fileName = "./ajax/create-booking.php";
            aj.callMethod = "post";
            aj.callParam = ar;
            aj.isAsynchronus = true;
            aj._query(function(p){_waitCreateBooking(p)}, function(p, r){_responseCreateBooking(p, r)});	
        }
    }
};

_waitCreateBooking = function(ar){
    document.getElementById("msg").innerHTML = "&nbsp;Wait while we create booking details for " + ar[13][1];
};

_responseCreateBooking = function(ar, rs){
    if (parseInt(rs) > 0) {
        document.getElementById("msg").innerHTML = "&nbsp;Booking has been created with Booking ID: " + rs;
    } else {
        document.getElementById("msg").innerHTML = "&nbsp;Booking could not be created. <a href='javascript: void(0);' onlick='javascript: window.location.reload();'>Click here</a> to refresh the interface.";
    }
};

_showReport = function(rpt, tp){
    //alert(rpt+'++'+tp);
    if (qsArray.length > 0) {
        _showPopup();
        var ar = new Array();
        for (a = 0; a < qsArray.length; a++) {
            ar.push(new Array(qsArray[a][0], qsArray[a][1]));
        }
        ar.push(new Array("rnd", Math.random()*99999));
        ar.push(new Array("rpt", rpt));
        ar.push(new Array("tp", tp));
        ar.push(new Array("sd", document.getElementById('sd').value));
        ar.push(new Array("ed", document.getElementById('ed').value));
        
        var aj = new _ajax();
        aj.fileName = "./ajax/summary-report.php";
        aj.callMethod = "post";
        aj.callParam = ar;
        aj.isAsynchronus = true;
        aj._query(function(p){_waitShowReport(p)}, function(p, r){_responseShowReport(p, r)});  
    }    
};

_waitShowReport = function(ar){
    document.getElementById("cnt").innerHTML = "Wait while we fetch summerized report.";
};

_responseShowReport = function(ar, rs){
    document.getElementById("cnt").innerHTML = rs;
};

_changeChart = function(ct){
    switch (ct) {
        case "bookings":
        _showChart(aL, aDB);
        break;
        case "searches":
        _showChart(aL, aDS);
        break;
        case "fare":
        _showChart(aL, aDF);
        break;
    }
};

_closeChart = function(th){
    document.getElementById("chartC").style.display = 'none';
    document.getElementById("chartOptions").style.display = 'none';
    document.getElementById("chartOptions2").style.display = 'block';
    
};

_openChart = function(th){
    document.getElementById("chartC").style.display = 'block';
    document.getElementById("chartOptions").style.display = 'block';
    document.getElementById("chartOptions2").style.display = 'none';
    document.getElementById("ddlCharts").selectedIndex = 0;
    _showChart(aL, aDB);
}
<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
	//include_once("./includes/config.php");
    include_once("./includes/cache-func.php");    
    include_once('../classes/cor.ws.class.php');
    include_once('../classes/cor.xmlparser.class.php');
    include_once("./classes/cor.mysql.class.php");  
		set_include_path('../lib'.PATH_SEPARATOR.get_include_path());
	require_once('../lib/CitrusPay.php');
    $act_count = 0;
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $r = $db->query("stored procedure", "cor_bookings_at_gateway()");
	
    if(count($r) > 0)
    {
	
	for($i = 0; $i < count($r); $i++)
	{
	$tarr = array(
	"merchantAccessKey" => "6TY836TTLCN57XI3JMHA",
	"transactionId" => $r[$i]["coric"],
	);
	CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d",'production');
    $response = Enquiry::create($tarr,CitrusPay::getApiKey());
	
    unset($tarr);
	if($response->get_resp_code() == 200 && $response->get_resp_msg() == "Enquiry successful")
	 {
	  try{
		    $enquiry = $response->get_enquiry();
		    for($enq = 0; $enq < count($enquiry); ++$enq)
		    {
			    $enqObj = $enquiry[$enq];
                            if($enqObj->get_resp_code() == 0){
                                $where = array();
                                $where["coric"] = $r[$i]["coric"];
                                $dataToSave = array();
                                $dataToSave["payment_mode"] = "1";
                                $dataToSave["payment_status"] = "3";
                                $dataToSave["AuthDesc"] = "Y";
                                $dataToSave["Checksum"] = $enqObj->get_auth_id_code();
                                $dataToSave["Merchant_Param"] = $enqObj->get_rrn();
                                $dataToSave["nb_bid"] = $enqObj->get_pg_txn_id();
                                $dataToSave["nb_order_no"] = $enqObj->get_txn_id();
                                $dataToSave["card_category"] = $enqObj->get_payment_mode();
                                $dataToSave["bank_name"] = "NA";
                                $dataToSave["payment_update_date"] = "NOW()";
                                $up = $db->update("cor_booking_new", $dataToSave, $where);
								
                                unset($dataToSave);
                                unset($where);
                            }
		    }
                }
                catch(Exception $e){}
	    }
            unset($response);
        }
	
	
	$book = $db->query("stored procedure", "cor_bookings_failed()");
	
	
	if(!array_key_exists("response",$book)){
	    for($b = 0; $b < count($book); $b++){
		$fullname = explode(" ", $book[$b]["full_name"]);
		if(count($fullname) > 1){
		    $firstName = $fullname[0];
		    $lastName = $fullname[1];
		} else {
		    $firstName = $fullname[0];
		    $lastName = "";
		}
		    $cor = new COR();
			if($book[$b]["tour_type"]!= "Selfdrive"){
			
			
			$corArrSTD = array();
		    $corArrSTD["pkgId"] = $book[$b]["package_id"];
		    $corArrSTD["destination"] = $book[$b]["destinations_name"];
		    $pDate = date_create($book[$b]["pickup_date"]);
		    $corArrSTD["dateOut"] = $pDate->format("Y-m-d");
		    $dDate = date_create($book[$b]["drop_date"]);
		    $corArrSTD["dateIn"] = $dDate->format("Y-m-d");
		    $pTime = date_create($book[$b]["pickup_time"]);
		    $corArrSTD["TimeOfPickup"] = $pTime->format("is");;
		    $corArrSTD["address"] = str_ireplace(array("<br>","<br />"), "", nl2br($book[$b]["address"]));
		    $corArrSTD["firstName"] = $firstName;
		    $corArrSTD["lastName"] = $lastName;
		    $corArrSTD["phone"] = $book[$b]["mobile"];
		    $corArrSTD["emailId"] = $book[$b]["email_id"];
		    $corArrSTD["userId"] = $book[$b]["cciid"];
		    $corArrSTD["paymentAmount"] = $book[$b]["tot_fare"];
		    $corArrSTD["paymentType"] = "1";
		    $corArrSTD["paymentStatus"] = "1";
		    $corArrSTD["trackId"] = $book[$b]["coric"];
		    $corArrSTD["transactionId"] = $book[$b]["nb_order_no"];
		    $corArrSTD["discountPc"] = $book[$b]["discount"];
		    $corArrSTD["discountAmount"] = $book[$b]["discount_amt"];
		    $corArrSTD["remarks"] = $book[$b]["remarks"];
		    $corArrSTD["totalKM"] = $book[$b]["distance"];
		    $corArrSTD["visitedCities"] = $book[$b]["destinations_name"];
		    if($book[$b]["tour_type"] != "Local")
		    $outStationYN = "true";
		    else
		    $outStationYN = "false";
		    $corArrSTD["outStationYN"] = $outStationYN;
		    $corArrSTD["OriginCode"] = $book[$b]["source"]; //Aamir 1-Aug-2012
		    $corArrSTD["chkSum"] = $book[$b]["Checksum"];
		    $corArrSTD["DiscountCode"] = $book[$b]["promotion_code"];
		    $corArrSTD["PromotionCode"] = $book[$b]["discount_coupon"];
		    $corArrSTD["IsPayBack"] = $book[$b]["isPayBack"];
		    $corArrSTD["discountTrancastionID"] = $book[$b]["disc_txn_id"];
			$corArrSTD["latitude"] = $book[$b]["latitude"];
            $corArrSTD["longitude"] = $book[$b]["longitude"];
			$res = $cor->_CORMakeBookingWithLatLong($corArrSTD);
			
			//$res = $cor->_CORMakeBooking($corArrSTD);
			$myXML = new CORXMLList();
		    if($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'} != ""){
			$arrUserId = $myXML->xml2ary($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'});
			if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){					  
			    $dataToSave = array();
			    $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
			    $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
			    $dataToSave["payment_mode"] = "1";
			    $whereData = array();
			    $whereData["coric"] = $book[$b]["coric"];		
			    $u = $db->update("cor_booking_new", $dataToSave, $whereData);
			    unset($whereData);
			    unset($dataToSave);
			    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			    $xmlToSave .= "<cortrans>";
			    $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
			    $xmlToSave .= "</cortrans>";
			    if(!is_dir("../xml/trans/" . date('Y')))
			    mkdir("../xml/trans/" . date('Y'), 0775);
			    if(!is_dir("../xml/trans/" . date('Y') . "/" . date('m')))
			    mkdir("../xml/trans/" . date('Y') . "/" . date('m'), 0775);
			    if(!is_dir("../xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
			    mkdir("../xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
			    if(1)
			    createcache($xmlToSave, "../xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $book[$b]["coric"] . "-B.xml");
			}
		   elseif($arrUserId[0]["Column1"] == "NoCar")
			{
				$db = new MySqlConnection(CONNSTRING);
				$db->open();
				$sql = "UPDATE cor_booking_new SET booking_id = '0' WHERE coric= '".$book[$b]["coric"]."'";
				$r = $db->updatenew($sql);
				
				$html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
						$html .= "<tr>";
						$html .= "<td colspan='2'>Dear Team,<br/><br/>The following booking is failed post successful payment by the customer:
						</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Cor id</td>";
						$html .= "<td>" .$book[$b]["coric"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer name</td>";
						$html .= "<td>" . $book[$b]["full_name"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer email id</td>";
						$html .= "<td>" . $book[$b]["email_id"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer mobile number</td>";
						$html .= "<td>" . $book[$b]["mobile"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Pick up city</td>";
						$html .= "<td>" . $book[$b]["origin_name"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Car model</td>";
						$html .= "<td>" . $book[$b]["model_name"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Pick up date and time</td>";
						$html .= "<td>" .$book[$b]["pickup_date"]." : ".$book[$b]["pickup_time"] . "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Drop off date and time</td>";
						$html .= "<td>" . $book[$b]["drop_date"]." : ".$book[$b]["drop_time"] . "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Fare amount</td>";
						$html .= "<td>" . $book[$b]["tot_fare"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Trip type</td>";
						$html .= "<td>" . $book[$b]["trip_type"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Booking channel</td>";
						$html .= "<td>" . $book[$b]["source"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Please speak with the customer and check if an alternate booking can be made <br>Regards,
						<br>Myles IT team
						</td>";
						$html .= "</tr>";
						$html .= "</table>";
						$dataToMail= array();
						$dataToMail["To"] = "vinod.maurya@carzonrent.com;rahul.jain@carzonrent.com";
						$dataToMail["Subject"] = "Booking failure  | Payment success";
						$dataToMail["MailBody"] = $html;
						$res = $cor->_CORSendMail($dataToMail);
						unset($dataToMail);
						// New code Added by vinod
						unset($whereData);
						unset($dataToSave);
				
			}
		    }
		}
		unset($cor);
		$act_count++;
	    }
	}
    }
	
	$dateval=date('Y-m-d H:i:s');
	if($act_count==0)
	{  
	    $exectCase='Cron executed';
		$cronfile = fopen("/var/www/html/carzonrent/lenap_panel/CRDcron.txt", "w") or die("Unable to open file!");
		$txt = $dateval.$exectCase;
		fwrite($cronfile, $txt);
		fclose($cronfile);
	}
	else
	{
	  
		$exectCase='Cron executed with fail booking';
		$cronfile = fopen("/var/www/html/carzonrent/lenap_panel/CRDcron.txt", "w") or die("Unable to open file!");
		$txt = $dateval.$exectCase;
		fwrite($cronfile, $txt);
		fclose($cronfile);
	
	
	}
	
    $dataToSave = array();
    $dataToSave["update_date_time"] = "NOW()";
    $dataToSave["action_count"] = $act_count;
	$dataToSave["tour_type"] ='Outstation/Local';
    $up = $db->insert("cor_last_schedule_job", $dataToSave);
    unset($dataToSave);
    $db->close();
?>
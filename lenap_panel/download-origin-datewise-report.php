<?php
	include_once("./includes/check-user.php");
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('origin', 'start date','end date', 'count'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");
    //mysql_connect('localhost', 'username', 'password');
    //mysql_select_db('database');
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_getOriginCountDateWise('".$_GET["sdate"]."','".$_GET["edate"]."','".$_GET["origin"]."','".$_GET["tt"]."')");
    // loop over the rows, outputting them
    for($i = 0;$i< count($rows);$i++)
    {
         fputcsv($output, array($rows[$i]["origin_name"],$rows[$i]["entry_date"],$rows[$i]["entry_date"],$rows[$i]["cnt"]));        
    }    
?>
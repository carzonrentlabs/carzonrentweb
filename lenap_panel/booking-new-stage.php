<?php
    include_once("./includes/check-user.php");
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js" type="text/javascript"></script>
    
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
    <style>
	table.adm tr th{
	    background-color: #333;
	    color: #fff;
	    padding: 6px;
	}
	table.adm tr td{
	    color: #222;
	    padding: 6px;
	}
	table.adm tr.ev{
	    background-color: #fef6cf;
	}
	table.adm tr.od{
	    background-color: #fef3bc;
	}
	table.adm tr.er{
	    background-color: #fbb1a4;
	}
	table.adm tr.er:hover{
	    background-color: #f58875;
	    color:#000;
	}
	table.adm tr.ev:hover, table.adm tr.od:hover{
	    background-color: #d2d2d2;
	    color:#000;
	}
	.pg{
	    margin-right:5px;
	    padding:5px 8px;
	    border:solid 1px #f2c900;
	    display:block;
	    float:left;
	    color:#666;
	    text-decoration: none;
	    background-color: #FEF6CF;
	}
	.pg:hover{
	    font-weight: bold;
	    text-decoration: none;
	    background-color: #FEF3BC;
	}
	.pg-sel{
	    color:#f00 !important;
	    font-weight: bold;
	    text-decoration: none;
	    background-color: #FEF3BC;
	}
	.ddl{
	    border:solid 1px #666;
	    padding: 3px;
	}
	.txtBox{
	    border:solid 1px #555;
	    padding: 3px;
	    font-size: 14px;
	}
    </style>
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> Booking</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<br><br>
		<div>
		    <?php
			$url = "./booking-new-stage.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != "")
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != "")
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    
			$lStart = ($cPage - 1) * $recPerPage;
			$totATRec = 0;
			$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_booking_new";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totATRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$totRec = 0;
			$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59'";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			$totATBooking = 0;
			$sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE booking_id IS NOT NULL";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totATBooking = $r[0]["TotalBooking"];
			unset($r);
			
			$totBooking = 0;
			$sql = "SELECT COUNT(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND booking_id IS NOT NULL";
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r))
			    $totBooking = $r[0]["TotalBooking"];
			unset($r);
			
			//$totRec = 0;
			$sql = "SELECT uid, source, coric, booking_id, origin_name, tour_type, tot_fare, additional_srv_amt, payment_mode, pickup_date, pickup_time, entry_date, ip, ua, AuthDesc FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ORDER BY uid DESC LIMIT " . $lStart . ", " . $recPerPage;
			$r = $db->query("query", $sql);
			$db->close();
			
			$pagingHTML = "";
			if($numOfPage > 1){
			    $nURL = $url;
			    if(isset($_GET["sd"]) && $_GET["sd"] != "")
			    $nURL .= "?sd=" . urlencode($_GET["sd"]) . "&ed=" . urlencode($_GET["ed"]);
			    $pagingHTML = "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
				$pagingHTML .= "<tr>";
				    $pagingHTML .= "<td>";
				    if($cPage % $maxPageNum != 0)
					$pgStart = $cPage - ($cPage % $maxPageNum);
				    else
					$pgStart = $cPage - $maxPageNum;
				    $pgStart++;
				    if($cPage > $maxPageNum){
					$pgURL = $nURL;
					$pgPrev = $pgStart -  1;
					$pgURL .= $pgURL == $url ? "?pg=" . $pgPrev : "&pg=" . $pgPrev;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Prev</a>";
				    }
				    for($p = $pgStart; $p <= $numOfPage; $p++){
					$pgURL = $nURL;
					if($p != $cPage){
					    $pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					    $pagingHTML .= "<a href='" . $pgURL . "' class='pg'>" . $p . "</a>";
					}
					else
					    $pagingHTML .= "<a href='javascript: void(0);' class='pg pg-sel'>" . $p . "</a>";
					if($p % $maxPageNum == 0)
					break;
				    }
				    if($numOfPage > $p){
					$p++;
					$pgURL = $nURL;
					$pgURL .= $pgURL == $url ? "?pg=" . $p : "&pg=" . $p;
					$pagingHTML .= "<a href='" . $pgURL . "' class='pg'>Next</a>";
				    }
				    $pgURL = $nURL;
				    $pgURL .= $pgURL == $url ? "?pg=" : "&pg=";
			    $pagingHTML .= "Go to page number <select id=\"ddlPage\" name=\"ddlPage\" class=\"ddl\" onchange=\"javascript: window.location='" . $pgURL . "' + this.value;\">";
				    for($p = 1; $p <= $numOfPage; $p++){
					if($p != $cPage){
					    $pagingHTML .= "<option value=\"" . $p . "\">" . $p . "</option>";
					} else {
					    $pagingHTML .= "<option value=\"" . $p . "\" selected=\"selected\">" . $p . "</option>";
					}
				    }
					$pagingHTML .= "</select>";
				    $pagingHTML .= "</td>";
				$pagingHTML .= "</tr>";
			    $pagingHTML .= "</table>";
			}
			if(($lStart + $recPerPage) >= $totRec)
			$showingText = "Showing " . $lStart + 1 . " to " . $totRec . " of <b>" . $totRec . "</b>";
			else
			$showingText = "Showing " . $lStart + 1 . " to " . ($lStart + $recPerPage) . " of <b>" . $totRec . "</b>";
		?>
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmSearch" name="frmSearch" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Start Date</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>End Date</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						<span class="datepick">
						    <input type="text" size="12" autocomplete="off" name="sd" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
						</span>
					    </td>
					    <td style="padding-left: 10px;"><span class="datepick">
							    <input type="text" size="12" autocomplete="off" name="ed" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
							</span>
					    </td>
					    <td style="padding-left: 10px;">
						<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
					    </td>
					    <td valign="middle" style="padding-left: 10px;">					
						<a href="booking-download.php?sd=<?php echo $sDate->format('Y-m-d'); ?>&ed=<?php echo $eDate->format('Y-m-d'); ?>">Download</a>
					    </td>
					    <!--<td>
						<input type="text" size="12" autocomplete="off" name="sTxt" value="" id="sTxt" class="datepicker" onkeypress="javascript: return false;" />						    
					    </td>-->
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				<td align="right" valign="bottom">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;">Confirmed Booking: <b style='color:#f00;'><?php echo $totBooking; ?></b></td>
				<td align="right"><?php echo $showingText; ?></td>
			    </tr>
			</table>
			<table width="99%" class="adm" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">SrNo</th>
				    <th align="left">Source</th>
				    <th align="left">CORIC</th>
				    <th align="left">Booking ID</th>
				    <th align="left">Type</th>
				    <th align="left">Origin</th>
				    <th align="left">Fare</th>
				    <th align="left">Payment</th>
				    <th align="left">Travel On</th>
				    <th align="left">Booked On</th>
				    <th align="left">IP</th>
				    <th align="left">Browser</th>
				    <th align="left">&nbsp;</th>
				</tr>
			    </thead>
			    <tbody>
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    $vTxt = "<a href='javascript: void(0)' onclick=\"javascript: _showDetails(" . $r[$i]["uid"] . ", '" . $r[$i]["coric"] . "')\" title=\"Click on view details of " . $r[$i]["coric"] . "\">Details</a>";
				    if($i % 2 == 0)
					$cls = "ev";
				    if($r[$i]["payment_mode"] == 1 && is_null($r[$i]["booking_id"]) && $r[$i]["AuthDesc"] == 'Y'){
					$cls = "er";
					$vTxt .= "&nbsp;<a href='javascript: void(0)' title='Failed booking. Click on View Details to create booking of " . $r[$i]["coric"] . "'>?</a>";
				    }
				    $payMode = "-";
				    switch($r[$i]["payment_mode"]){
					case 1:
					    $payMode = "Online";
					break;
					case 2:
					    $payMode = "Pay to driver";
					break;
				    }
				    $ua = browser_info($r[$i]["ua"]);
				    //echo $r[$i]["ua"] . "<pre>";
				    $b = array_keys($ua);
				    $v = array_values($ua);
				    $pickDate = date_create($r[$i]["pickup_date"]);
				    $pickTime = date_create($r[$i]["pickup_time"]);
				    $bookDate = date_create($r[$i]["entry_date"]);
		    ?>
				    <tr class="<?php echo $cls; ?>">
					<td align="left"><?php echo $srno; ?></td>
					<td align="left"><?php echo $r[$i]["source"]; ?></td>
					<td align="left"><?php echo $r[$i]["coric"]; ?></td>
					<td align="left"><?php echo $r[$i]["booking_id"] == "" ? "-" : $r[$i]["booking_id"]; ?></td>
					<td align="left"><?php echo $r[$i]["tour_type"]; ?></td>
					<td align="left"><?php echo $r[$i]["origin_name"]; ?></td>
					<td align="left"><?php echo $r[$i]["tot_fare"] + $r[$i]["additional_srv_amt"]; ?></td>
					<td align="left"><?php echo $payMode; ?></td>
					<td align="left"><?php echo $pickDate->format('d M, Y'); ?>&nbsp;<sub><?php echo $pickTime->format("is"); ?> HRS</sub></td>
					<td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<sub>" . $bookDate->format('H:i A') . "</sub>"; ?></td>
					<td align="left"><?php echo $r[$i]["ip"]; ?></td>
					<td align="left"><?php echo ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]); ?> <a href="javascript: void(0);" title="<?php echo $r[$i]["ua"]; ?>">?</a></td>
					<td align="left"><?php echo $vTxt; ?></td>
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="right" style="margin-right: 10px;">
				    <?php echo $pagingHTML; ?>
				</td>
			    </tr>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
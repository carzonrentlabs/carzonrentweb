<?php
    include_once("./includes/check-user.php");
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
    $currentMonth = date("m");
    $curyear = date("Y");
    if(isset($_REQUEST['month'])){
        $mnt = $_REQUEST['month'];
    }else{
        $mnt = $currentMonth;
    }
    if(isset($_REQUEST['year'])){
        $yr = $_REQUEST['year'];
    }else{
        $yr = $curyear;
    }
    if(isset($_REQUEST['ddlOrigin'])){
        $orgn = $_REQUEST['ddlOrigin'];
    }else{
        $orgn = '2';
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    
    <?php 
  // echo "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = 2015 and month(pickup_date) = 08 and day(pickup_date) <= 01) && (year(drop_date) = 2015 and month(drop_date) = 08 and day(drop_date) >= 01)) and origin_id = 2";
    ?>
    
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/highcharts.js"></script>
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/exporting.js"></script>

<script>
$(function(){
    
    var bid = [<?php 
          $dtl = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$i." and origin_id = ".$orgn." ";
                $rows = $db->query("query", $sql);
                $dtl .= $prefix.$rows[0]['ttl'];
            }
            $dtl = str_replace("<br/>", " ", $dtl );
            echo $dtl;
        ?>];
                    
    var cardays = [<?php 
          $dtl = '';
           for($i=1;$i<=31;$i++){
                if($i=='1'){
                    $prefix = '';
                }else{
                    $prefix = ',';
                }
                if($i<10){
                    $i = '0'.$i;
                }
                $db = new MySqlConnection(CONNSTRING);
                $db->open();
                $sql = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$i.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$i.")) and origin_id = ".$orgn." ";
                $rows = $db->query("query", $sql);
                $dtl .= $prefix.$rows[0]['ttl'];
            }
            $dtl = str_replace("<br/>", " ", $dtl );
            echo $dtl;
        ?>];
    
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Details',
			x: 0
        },
        xAxis: {
            categories: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'No Of Bookings',
            data: bid

        }, {
            name: 'Car Days',
            data: cardays

        }
		]
    });
});
</script>
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Booking, Car-Days Chart</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./booking-cardays.php";
			//$dlndURL = "./selfdrive-search-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "unique_id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("source" => "Source", "coric" => "CORIC", "booking_id" => "Booking ID", "tour_type" => "Tour Type", "package_id" => "Package ID","car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			
			
		?>
	    
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Month</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>Year</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						
						    <select class="ddlS" name="month" id="month" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        $months = array("01"=>"Jan", "02"=>"Feb", "03"=>"Mar", "04"=>"Apr","05"=>"May","06"=>"June","07"=>"July","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
                                                        
                                                        $sel = '';
                                                        $mm = 0;
                                                        foreach($months as $k=>$month) {
                                                            if(isset($_REQUEST['month'])){
                                                                if(($_REQUEST['month'] == $k) && $mm==0){
                                                                $sel = 'selected = "selected"';
                                                                $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($currentMonth==$k && $mm==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $k . "\">" . $month . "</option>";
                                                        
                                                           
                                                            
                                                            
                                                            }
                                                        ?>

                                                    </select>
						
					    </td>
                                                
					    <td style="padding-left: 10px;">
                                                <select class="ddlS" name="year" id="year" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        
                                                        $sel = '';
                                                        $nn = 0;
                                                        for($i=2010;$i<=$curyear;$i++){
                                                            if(isset($_REQUEST['year'])){
                                                                if(($_REQUEST['year'] == $i) && $nn==0){
                                                                $sel = 'selected = "selected"';
                                                                $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($curyear==$i && $nn==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $i . "\">" . $i . "</option>";
                                                        }
                                                        ?>

                                                    </select>
					    </td>
                                            <td><label>Location</label></td>
                                            <td>
                                                <?php
                                                $db = new MySqlConnection(CONNSTRING);
                                                $db->open();
                                                $sql = "SELECT * FROM cor_location";
                                                $rloc = $db->query("query", $sql);
                                                if (!array_key_exists("response", $rloc)) {
                                                    ?>
                                                    <select class="ddlS" name="ddlOrigin" id="ddlOriginSF" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                         <?php
                                                        $sel = '';
                                                        $ll = 0;
                                                        for ($i = 0; $i < count($rloc); $i++) {
                                                               if(isset($_REQUEST['ddlOrigin'])){
                                                                if(($_REQUEST['ddlOrigin'] == $rloc[$i]["city_id"]) && $ll==0){
                                                                $sel = 'selected = "selected"';
                                                                $ll++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($rloc[$i]["city_id"] == '2' && $ll==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $ll++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }  

                                                           ?>
                                                        
                                                            <option <?php echo $sel;?> value="<?php echo $rloc[$i]["city_id"]; ?>"><?php echo $rloc[$i]["CityName"]; ?></option>
                                                            
                                                        <?php } ?>

                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </td>
					    <td style="padding-left: 10px;">
                                                <input type="image" value="Submit" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" />
					    </td>
                                            
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				
			    </tr>
			</table>
		</div>		
		<div class="clr"></div>
	    </div>
            
            <div id="container" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<div id="container" style="min-width: 310px; height: 550px; margin: 0 auto"></div>
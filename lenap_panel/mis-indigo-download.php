<?php
    include_once("./includes/check-user.php");
    
    $fld = "";
    $val = "";
    $isDtInc = false;
    $isFiltered = false;
    $sortFld = "uid";
    $sortMode = "desc";
    $sDate = date_create(date("Y-m-d"));
    $eDate = date_create(date("Y-m-d"));
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
    $cPage = $_GET["pg"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "")
        $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
        
    if(isset($_GET["ed"]) && $_GET["ed"] != "")
        $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
    
    if(isset($_GET["f"]) && $_GET["f"] != ""){
        $fld = $_GET["f"];
        $isFiltered = true;
    }
    
    //print_r($sDate);die;
    if(isset($_GET["q"]) && $_GET["q"] != ""){
        
        $fVal = $_GET["q"];
        $isFiltered = true;
        if($fld != ""){
            if($fld == "payment_mode"){
                if(trim(strtolower($fVal)) == "online")
                    $fVal = 1;
                elseif(trim(strtolower($fVal)) == "pay to driver")
                    $fVal = 2;
            } elseif($fld == "payment_status"){
                if(trim(strtolower($fVal)) == "success")
                    $fVal = 1;
                elseif(trim(strtolower($fVal)) == "gateway")
                    $fVal = 2;
                elseif(trim(strtolower($fVal)) == "payment")
                    $fVal = 3;
                elseif(trim(strtolower($fVal)) == "failure")
                    $fVal = 4;
            }				
        }
    }
    
    if(isset($_GET["inc"]) && $_GET["inc"] != ""){
        $isDtInc = true;
        $isFiltered = true;
    }
    if(isset($_GET["sf"]) && $_GET["sf"] != "")
    $sortFld = $_GET["sf"];
    
    if(isset($_GET["sm"]) && $_GET["sm"] != "")
    $sortMode = $_GET["sm"];
    
    if(isset($_GET["sd"]) && $_GET["sd"] != "" && isset($_GET["ed"]) && $_GET["ed"] != "")                   
        $filename = "bookings_" . $sDate->format('d-m-Y') . "_" . $eDate->format('d-m-Y');
    else
        $filename = "bookings_overall_" . $sDate->format('d-m-Y');
    if(isset($_GET["pg"]) && $_GET["pg"] != "")
        $filename .= "_p" . $cPage;
    
    $filename .= ".csv";
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");

    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    if($fld != "" && $fVal != ""){
        $sql = "SELECT * FROM cor_booking_new WHERE " . $fld . " = '" . $fVal . "' AND source='INDIGO'";
        if($isDtInc){
            $sql .= " AND entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' ";
        }
        $sql .= " ORDER BY " . $sortFld . " " . $sortMode;
    } else {
        $sql = "SELECT * FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND source='INDIGO' ORDER BY " . $sortFld . " " . $sortMode;
    }
    //echo $sql;
    if(isset($_GET["pg"]) && $_GET["pg"] != ""){
        $recPerPage = 100;
        $lStart = ($cPage - 1) * $recPerPage;
        $sql .= " LIMIT " . $lStart . ", " . $recPerPage;
    }
    $rows = $db->query("query", $sql);    
    // loop over the rows, outputting them
    if(!array_key_exists("response", $rows)){
        // output the column headings
        $fieldNames = array();
        $fieldNames = array_keys($rows[0]);
        fputcsv($output, $fieldNames);
        unset($fieldNames);
        for($i = 0;$i< count($rows);$i++){        
            $ua = browser_info($rows[$i]["ua"]);
            $b = array_keys($ua);
            $v = array_values($ua);
            $rows[$i]["ua"] = ucwords($b[0] == "msie" ? "MSIE" . " " . $v[0] : $b[0] . " " . $v[0]);
            $fieldValues = array();
            $fieldValues = $rows[$i];
            fputcsv($output, $fieldValues);
            unset($fieldValues);
        }
    }
?>
<?php
    include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
    
     $html = "";     
    if(isset($_POST["seq_no"]))
    {
        $sno = $_POST["seq_no"];
        
        $db = new MySqlConnection(CONNSTRING);
        $db->open();           
        $r = $db->query("stored procedure","cor_loadmore_Failed_bookings('".$_POST["seq_no"]."','".$_POST["stdate"]."','".$_POST["edate"]."')");
        //print_r($r);
        for($i = 0;$i < count($r); $i++)
        {
            $style = "";            
            if($i%2 == 0)
            {
            $style = "background-color:#f1f1f1;";
            }
            else
            {
            $style = "background-color:#dddada;";
            }            
            
            $html .= "<tr style=\"$style \">
                        <td style=\"padding:5px;word-wrap:break-word;\" name = \"seqno\">".($i+1)."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["hdTourtype"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["hdOriginName"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["hdDestinationName"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".date('F j, Y', strtotime($r[$i]["entry_date"]))."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".date('F j, Y', strtotime($r[$i]["hdPickdate"]))." - ".date('F j, Y', strtotime($r[$i]["hdDropdate"]))."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["name"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["email"]."/".$r[$i]["monumber"]."</td>
                        <td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["coric"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["ua"]."</td>
						<td style=\"padding:5px;word-wrap:break-word;\">".$r[$i]["ip"]."</td> 
                    </tr>";
                $sno++;
        }
        unset($r);
        
        $count = $db->query("stored procedure","cor_countForFailed_Booking('".$_POST["stdate"]."','".$_POST["edate"]."')");

        $hdCount = $count[0]["cnt"];
        
        unset($count);
        $db->close();
    }
    
    $c = 0;
    if($hdCount <= $sno)
    {
	$c =1;
    }
    
    echo $sno."#!#".$html."#!#".$c."#!#".$_POST["stdate"]."#!#".$_POST["edate"];
?>
<?php
	include_once("../includes/config.php");
    include_once("../classes/bb-mysql.class.php");
	$resp = "";
	$recPerPage = 200;
	$maxPageNum = 10;
	$sortFld = "uid";
	$sortMode = "desc";
	$cPage = 1;
	if(isset($_GET["pg"]) && $_GET["pg"] != "")
	$cPage = $_GET["pg"];
	$lStart = ($cPage - 1) * $recPerPage;			
	$totRec = 0;

	$db = new MySqlConnection(CONNSTRING);
	$db->open();

	$sql = "SELECT COUNT(uid) as TotalRecord FROM cor_seo_mata_tags";
	$r = $db->query("query", $sql);
	if(!array_key_exists("response", $r)){
		$totRec = $r[0]["TotalRecord"];
	}
	unset($r);
	
	$numOfPage = ceil($totRec / $recPerPage);
	
	$sql = "SELECT * FROM cor_seo_mata_tags ORDER BY " . $sortFld . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
	$r = $db->query("query", $sql);
	$db->close();

	if(!array_key_exists("response", $r)){
		$srno = $lStart + 1;
		for($i = 0; $i < count($r); $i++){
			$cls = "od";
			if($i % 2 == 0)
			$cls = "ev";
			
			$bookDate = date_create($r[$i]["entry_date"]);
			$resp .= "<tr class='" . $cls . "'>";
			$resp .= "<td align=\"left\">" . $srno . "</td>";
			$resp .= "<td align=\"left\">" . $r[$i]["page_url"] . "</td>";
			$resp .= "<td align=\"left\">" . $r[$i]["metaTitle"] . "</td>";
			$resp .= "<td align=\"left\">" . $r[$i]["metaDescription"] . "</td>";
			$resp .= "<td align=\"left\">" . $r[$i]["metaKeyword"] . "</td>";
			$resp .= "<td align=\"left\">" . $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookDate->format('H:i A') . "</small></td>";
			$resp .= "<td align=\"left\"><a href=\"javascript: _editMeta(" .  $r[$i]["uid"] . ", " .  $srno . ");\">Edit</a> | <a href=\"javascript: _deleteMeta(" .  $r[$i]["uid"] . ", " .  $srno . ");\">Delete</a></td>";
			$resp .= "</tr>";
			$srno++;
		}
	}

	echo $resp;
?>
<?php
    //error_reporting(E_ALL);
    //ini_set("display_errors", 1);
    include_once("../includes/check-user.php");
    include_once("../includes/config.php");
    include_once("../includes/cache-func.php");
    include_once("../classes/bb-mysql.class.php");
    include_once('../../classes/cor.xmlparser.class.php');
            
    $bookingId = 0;
    if(isset($_POST["mobile"]) && $_POST["mobile"] != ""){
        $clientid = 2205;
        $userID = 0;
        if((strtolower(trim($_POST["source"])) != "cor" && strtolower(trim($_POST["source"])) != "cor-mobile")){
            $clientid = 2257;
            include_once('../classes/cor.wlw.ws.class.php');
        } else {
            include_once('../classes/cor.ws.class.php');
        }
        
        $fullname = explode(" ", $_POST["full_name"]);
        if(count($fullname) > 1){
             $firstName = $fullname[0];
             $lastName = $fullname[1];
        } else {
             $firstName = $fullname[0];
             $lastName = "";
        }
            
       $cor = new COR();
		
		if(strtolower($_POST["tourtype"]) == "selfdrive"){
		

		$r = $cor->_CORSelfDriveCheckPhone(array("PhoneNo"=>$_POST["mobile"], "clientid"=>$clientid));
        //print_r($r);
        $myXML = new CORXMLList();
        $user = $myXML->xml2ary($r->{'VerifyPhoneResult'}->{'any'});

        if(key_exists("ClientCoIndivID", $user[0]))
            $userID = $user[0]["ClientCoIndivID"];
        elseif(key_exists("Column1", $user[0])){
            $userID = trim($user[0]["Column1"]);
            if($userID == 0){
                $r = $cor->_CORSelfDriveRegister(array("fname"=>$firstname, "lname"=>$lastName, "PhoneNumber"=>$_POST["mobile"], "emailId"=>$_POST["email_id"], "password"=>"retail@2012"));
                $user = $myXML->xml2ary($r->{'RegistrationResult'}->{'any'});
                if(key_exists("Column1", $user[0]) && $user[0]["Column1"] > 0){
                    $userID = trim($user[0]["Column1"]);
                }
            }
        }

		}
		else
		{
		
        $r = $cor->_CORCheckPhone(array("PhoneNo"=>$_POST["mobile"], "clientid"=>$clientid));
        //print_r($r);
        $myXML = new CORXMLList();
        $user = $myXML->xml2ary($r->{'VerifyPhoneResult'}->{'any'});

        if(key_exists("ClientCoIndivID", $user[0]))
            $userID = $user[0]["ClientCoIndivID"];
        elseif(key_exists("Column1", $user[0])){
            $userID = trim($user[0]["Column1"]);
            if($userID == 0){
                $r = $cor->_CORRegister(array("fname"=>$firstname, "lname"=>$lastName, "PhoneNumber"=>$_POST["mobile"], "emailId"=>$_POST["email_id"], "password"=>"retail@2012"));
                $user = $myXML->xml2ary($r->{'RegistrationResult'}->{'any'});
                if(key_exists("Column1", $user[0]) && $user[0]["Column1"] > 0){
                    $userID = trim($user[0]["Column1"]);
                }
            }
        }
		
		}
		
		
		
		
        if($userID != 0){
            $cciid = $userID;                
            if(strtolower($_POST["tourtype"]) == "selfdrive"){
				
				$totFare = $_POST["User_billable_amount"];
				
                $arrBooking = array();
                $arrBooking["pkgId"] = $_POST["pack_id"];
                $arrBooking["PickUpdate"] = $_POST["pick_date"];
                $arrBooking["PickUptime"] = $_POST["pick_time"];
                $arrBooking["DropOffDate"] = $_POST["drop_date"];
                $arrBooking["DropOfftime"] = $_POST["drop_time"];
                $arrBooking["FirstName"] = $firstName;
                $arrBooking["LastName"] = $lastName;
                $arrBooking["phone"] = $_POST["mobile"];
                $arrBooking["paymentMode"] = $_POST["pay_type"];
                $arrBooking["emailId"] = $_POST["email_id"];
                $arrBooking["userId"] = $cciid;
                $arrBooking["paymentAmount"] = $totFare;
                $arrBooking["paymentType"] = $_POST["pay_type"];
                $arrBooking["paymentStatus"] = "1";
                $arrBooking["visitedCities"] = $_POST["destination"];
                $arrBooking["AdditionalService"] = $_POST["add_serv"];
                $arrBooking["ServiceAmount"] = $_POST["add_serv_amt"];
                $arrBooking["trackId"] = $_POST["track_id"];
                $arrBooking["transactionId"] = $_POST["trans_id"];
                $arrBooking["remarks"] = "";
                $arrBooking["chkSum"] = $_POST["check_sum"];
                $arrBooking["OriginCode"] = $_POST["source"];
                $arrBooking["discountPc"] = $_POST["disc_pc"];
                $arrBooking["discountAmount"] = $_POST["disc_amt"];
                //if($_POST["disc_code"] != "Promotion code")
                $arrBooking["DiscountCode"] = $_POST["disc_code"];
                //else
                //$arrBooking["DiscountCode"] = "";
                
				if($_POST["promo_code"] != "Discount coupon number")
                $arrBooking["PromotionCode"] = $_POST["promo_code"];
                else
                $arrBooking["PromotionCode"] = "";
				
				
				if($arrBooking["promotion_code"] == 'FLATDISCOUNT')
				{
				 $corArrSTD["paymentAmount"] = $arrBooking['tot_gross_amount'];
				 $corArrSTD['finalAmount'] = $totFare;
				 
				}
				else
				{
				 $corArrSTD["paymentAmount"] = $totFare;
				 $corArrSTD['finalAmount'] = '';

				}
			
                $IsPB = 0;
                $arrBooking["IsPayBack"] = $IsPB;
                $DiscTxnID = "NA";
				$arrBooking["discountTrancastionID"] = $DiscTxnID;
                $arrBooking["SubLocationID"] = intval($_POST["sublocation_id"]);
				$arrBooking["pkgDuration"] = intval($_POST["duration"]);
				if($_POST["add_service_cost_all"] != "")
				$arrBooking["ServiceAmtAll"] = $_POST["add_service_cost_all"];
				else
				$arrBooking["ServiceAmtAll"] = "0";
				$arrBooking["SubAirportCost"] = $_POST["airportCharges"];
				
				/**** New added by vinod on april 08-04-2015 ***/
				$corArrSTD["WeekDayDuration"] = $_POST["WeekDayDuration"];
				$corArrSTD["WeekEndDuration"] = $_POST["WeekEndDuration"];
				$corArrSTD["FreeDuration"]    = $_POST["FreeDuration"];
				/**** New added by vinod on april 08-04-2015 ***/
		
		
		
		//print_r($arrBooking);
                $r = $cor->_CORSelfDriveCreateBooking_WeekEnd($arrBooking);
		//print_r($r);
                $myXML = new CORXMLList();
                if($r->{'SelfDrive_CreateBooking_WeekEndResult'}->{'any'} != ""){
                    $booking = $myXML->xml2ary($r->{'SelfDrive_CreateBooking_WeekEndResult'}->{'any'});
                    if(key_exists("Column1", $booking[0]) && $booking[0]["Column1"] > 0){
                        $bookingId = $booking[0]["Column1"];
                        if(isset($_POST["trans_id"]) && $_POST["trans_id"] != ""){
                            $dataToSave = array();
                            $dataToSave["BookingID"] = intval($bookingId);
                            $dataToSave["PaymentStatus"] = 1;
                            $dataToSave["PaymentAmount"] = intval($_POST["fare_amount"]);
                            $dataToSave["TransactionId"] = $_POST["trans_id"];
                            $dataToSave["ckhSum"] = $_POST["check_sum"];
                            $dataToSave["Trackid"] = $_POST["track_id"];
                            $dataToSave["discountPc"] = "0";
							$dataToSave["discountAmount"] = "0";
							$dataToSave["DisCountCode"] = "0";
							$dataToSave["DiscountTransactionId"] = $DiscTxnID;
                            $res = $cor->_CORSelfDriveUpdatePayment($dataToSave);
                            unset($dataToSave);                            
                        
                            $db = new MySqlConnection(CONNSTRING);
                            $db->open();
                            $dataToSave = array();
                            $dataToSave["booking_id"] = $bookingId;
                            $dataToSave["payment_status"] = "1";
                            $dataToSave["cciid"] = $cciid;
                            $whereData = array();
                            $whereData["coric"] = $_REQUEST['track_id'];		
                            $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                            unset($whereData);
                            unset($dataToSave);
                        }
                    }
                }
                unset($arrBooking);
            } else {
                $arrBooking = array();
                $arrBooking["pkgId"] = $_POST["pack_id"];;
                $arrBooking["destination"] = $_POST["destination"];
                $arrBooking["dateOut"] = $_POST["pick_date"];
                $arrBooking["dateIn"] = $_POST["drop_date"];
                $arrBooking["TimeOfPickup"] = $_POST["pick_time"];
                $arrBooking["address"] = str_ireplace(array("<br>","<br />"), "", nl2br($_POST["address"]));
                $arrBooking["firstName"] = $firstName;
                $arrBooking["lastName"] = $lastName;
                $arrBooking["phone"] = $_POST["mobile"];
                $arrBooking["emailId"] = $_POST["email_id"];
                $arrBooking["userId"] = $cciid;
                $arrBooking["paymentAmount"] = $_POST["fare_amount"];
                $arrBooking["paymentType"] = $_POST["pay_type"];
                $arrBooking["paymentStatus"] = "1";
                $arrBooking["trackId"] = $_POST["track_id"];
                $arrBooking["transactionId"] = $_POST["trans_id"];
                $arrBooking["discountPc"] = $_POST["disc_pc"];
                $arrBooking["discountAmount"] = $_POST["disc_amt"];
                $arrBooking["remarks"] = str_ireplace(array("<br>","<br />"), "", nl2br($_POST["remarks"]));
                $arrBooking["totalKM"] = $_POST["distance"];
                $arrBooking["visitedCities"] = $_POST["destination"];
                if(strtolower(trim($_POST["tourtype"])) == "outstation")
                $arrBooking["outStationYN"] = "true";
                else
                $arrBooking["outStationYN"] = "false";
                $arrBooking["CustomPkgYN"] = "false";
                $arrBooking["OriginCode"] = $_POST["source"]; //Aamir 1-Aug-2012
                $arrBooking["chkSum"] = $_POST["check_sum"];
                if($_POST["disc_code"] != "Promotion code")
                $arrBooking["DiscountCode"] = $_POST["disc_code"];
                else
                $arrBooking["DiscountCode"] = "";
                if($_POST["promo_code"] != "Discount coupon number")
                $arrBooking["PromotionCode"] = $_POST["promo_code"];
                else
                $arrBooking["PromotionCode"] = "";
                $IsPB = "0";
                $arrBooking["IsPayBack"] = $IsPB;
                $DiscTxnID = "0";
				$arrBooking["discountTrancastionID"] = $DiscTxnID;
                
                //echo "<pre>";
                //print_r($arrBooking);
                //echo "<br />";
                $r = $cor->_CORMakeBooking($arrBooking);
                //print_r($r);
                if($r->{'SetTravelDetailsResult'}->{'any'} != ""){
                    $booking = $myXML->xml2ary($r->{'SetTravelDetailsResult'}->{'any'});
                    if(key_exists("Column1", $booking[0]) && $booking[0]["Column1"] > 0){
                        $bookingId = $booking[0]["Column1"];
                        
                        $db = new MySqlConnection(CONNSTRING);
                        $db->open();
                        $dataToSave = array();
                        $dataToSave["booking_id"] = $bookingId;
                        $dataToSave["payment_status"] = "1";
                        $dataToSave["cciid"] = $cciid;
                        $whereData = array();
                        $whereData["coric"] = $_REQUEST['track_id'];		
                        $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                        unset($whereData);
                        unset($dataToSave);
                    }
                }
            }
        }
        echo $bookingId;
    }
?>
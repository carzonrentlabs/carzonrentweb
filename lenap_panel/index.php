<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin Console By Bytesbrick</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header.php");
                include_once("./includes/header-css.php");
            ?>

    </head>
    <body>
	<div id="middle">	
		<div class="yellwborder">
		    <div style="padding:7px 0px 7px 0px;" class="magindiv">
			<h2 class="heading2">Administrator Login</h2>
		    </div>
		</div>
	    <div class="main">
	    <br><br>
		<div class="businessEnquiry">
		<form name="frmLogin" id="frmLogin" method="post" action="./do-login.php">
		    <!--<input type="hidden" value="http://www.carzonrent.com" name="hdRURL" id="hdRURL">-->
		    <table width="75%" cellspacing="10" cellpadding="0" border="0" align="left" class="border-radius">                
			<tbody>
				<tr>
				<td></td>		     
				<td id="errmsg"></td>		     
			    </tr>
			    <tr>
				<td valign="middle" align="right"><label>Email Id</label></td>
				<td valign="top" align="left"><input type="text" id="txtuserid" name="txtuserid" onkeyup="$('#errmsg').html('');$('#txtuserid').css('background','');"></td>			     
			    </tr>
			    <tr>
				<td valign="middle" align="right"><label>Password</label></td>
				<td valign="top" align="left"><input type="password" id="txtpassword" name="txtpassword" onkeyup="$('#errmsg').html('');$('#txtpassword').css('background','');"></td>					
			    </tr>
			    <tr>
				<td valign="middle" align="right"></td>                                     
			    </tr>
			    <tr>
				<td valign="middle" align="right"></td>
				<td valign="top" align="left"> <div class="submit"><a onclick="validate();" href="javascript:void(0)"></a></div></td>                                   
			    </tr>    
			</tbody>
		    </table>		    
		</form>
		</div>
    
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>
	</div>
        <?php
            include_once("./includes/footer.php");
        ?>
    </body>
</html>
<script>
   $(document).ready(function() {
	<?PHP
	if(isset($_GET['resp']) && $_GET['resp']=='inv')
	{?>
	$('#errmsg').css('color','red').html('Either Email Id Or Password Is Wrong!.');
	return false;
	<?PHP } ?>
    });
	function validate()
	{
		var id =$('#txtuserid').val();
		var pwd =$('#txtpassword').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(id=="")
		{
			$('#errmsg').css('color','red').html('Please Enter Email id.');
			$('#txtuserid').css('background-color','pink').attr('Placeholder','Please Enter Email id.').focus();
			return false;
		}
		if (!filter.test(id)) {$('#errmsg').css('color','red').html('Please Enter Email id in correct format.');
			$('#txtuserid').css('background-color','pink').attr('Placeholder','Please Enter Email id in correct format.').focus();
			return false;
		}
		else if(pwd=="")
		{
			$('#errmsg').css('color','red').html('Please Enter Password.');
			$('#txtpassword').css('background-color','pink').attr('Placeholder','Please Enter Password.').focus();
			return false;
		}
		else 
		{
			document.forms['frmLogin'].submit();
		}	
		
	}
	</script>
	<script>
	$(document).keypress(function (e) {
		if (e.which == 13) {
			document.forms['frmLogin'].submit();
		}
	});
	</script>
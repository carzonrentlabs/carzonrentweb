<?php
    include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> Booking</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<br><br>
		<div class="businessEnquiry">
		    <form name="frmLogin" id="frmLogin" method="post" action="./do-login.php">
			<input type="hidden" value="http://www.carzonrent.com" name="hdRURL" id="hdRURL">
			<table width="99%" cellspacing="1" border="0" align="center" style="font-size:14px;color:#323232;">                
			    <tbody>
                                <?php
                                            $odir = $_GET["dir"];
                                            $dir = "../".$odir;
                                            $iDCount = 1;
                                            $iFCount = 1;
                                            if ($handle = opendir($dir))
                                            {
                                ?>
                                            <tr>
                                                <td colspan="2" style="padding:0px 0px 10px 0px;">You are here:
				<?php
						$dirs = explode("/", $dir);
						$base_xml_link = "xml";
						for($i = 0; $i < count($dirs); $i++){
						    if($i > 1){
							$base_xml_link .= "/" . $dirs[$i];
							echo "<a href='booking.php?dir=" . $base_xml_link . "'>" . $dirs[$i] . "</a> / ";
						    }
						    else
							echo $dirs[$i] . " / ";
						}				    
				?>
						</td>
                                <?php
                                                while (false !== ($entry = readdir($handle)))
                                                {        
                                                    if($entry != "." && $entry != "..")
                                                    {                                       
                                                        if(is_dir($dir . "/" . $entry))
                                                        {
                                                            if($iDCount == 1)
							    {
                                ?>
								</tr>
                                                                <tr style="background-color:#252525;color:#fff">
                                                                    <td align="center" style="padding:5px !important;"><b>Dirtectory</b></td>
                                                                    <td align="center" style="padding:5px !important;"><b>Action</b></td>
                                                                </tr>
                                <?php
                                                            }
                                                            $iDCount++;
							    $style = "";
							    if($iDCount%2 == 0)
								$style = "background-color:#f1f1f1";
							    else
								$style = "background-color:#dddada";
                                ?>
                                                            <tr style="<?php echo $style ?>">				    
                                                                <td width="50%" style="padding:5px !important;" align="center"><a class="ruline" href="booking.php?dir=<?php echo $odir . "/" . $entry ?>"><?php echo $entry; ?></a>  </td>
                                                                <td width="50%" style="padding:5px !important;" align="center"><a class="ruline" href="booking.php?dir=<?php echo $odir . "/" . $entry ?>">View</a></td>
                                                            </tr>
                                <?php
                                                        }
                                                        else
							{
                                                            if($iFCount == 1)
							    {
                                ?>
								    <td colspan="2" align="right">
									<table cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
									    <tr>
										<td style="background-color:#98f349;width:20px;">&nbsp;</td>
										<td style="padding:0px 25px 0px 5px;">Booked</td>
										<td style="background-color:#f24f34;width:20px;">&nbsp;</td>
										<td style="padding:0px 25px 0px 5px;">Paid But Not Booked</td>
										<td style="background-color:#4eaab1;width:20px;">&nbsp;</td>
										<td style="padding:0px 25px 0px 5px;">Booked But Not Paid</td>
										<td style="background-color:#f7d449;width:20px;">&nbsp;</td>
										<td style="padding:0px 25px 0px 5px;">Error</td>
									    </tr>
									</table>
								    </td>
								</tr>
                                                                <tr style="background-color:#252525;color:#fff">
                                                                    <td align="center" style="padding:5px !important;"><b>CORIC</b></td>
                                                                    <td align="center" style="padding:5px !important;"><b>Is Paid?</b></td>
								    <td align="center" style="padding:5px !important;"><b>Is Booked?</b></td>
                                                                    <td align="center" style="padding:5px !important;"><b>View</b></td>
                                                                </tr>
                                <?php
                                                            }
                                                            $iFCount++;
							    if($iFCount%2 == 0)
								$style = "background-color:#f1f1f1";
							    else
								$style = "background-color:#dddada";
							    $isPaid = "No";
							    $isBooked = "No";
							    $isError = "No";
							    $corIC = str_ireplace(".xml", "", $entry);
							    $filename = $base_xml_link."/" . $corIC . ".xml";
							    $filename = str_replace("pre-trans", "trans", $filename);
							    $filename = "../" . $filename;
							    //echo filesize($filename);
							    if(file_exists($filename) && filesize($filename) > 500)
								$isPaid = "Yes";
							    $filename = str_ireplace(".xml", "-B.xml", $filename);
							    if(file_exists($filename))
								$isBooked = "Yes";
							    $filename = str_ireplace("-B.xml", "-E.xml", $filename);
							    if(file_exists($filename))
								$isError = "Yes";
							    if($isPaid == "Yes" && $isBooked == "Yes" && $isError == "No")
								$style = "background-color:#98f349";
							    else if($isPaid == "Yes" && $isBooked == "No" && $isError == "No")
								$style = "background-color:#f24f34";
							    else if($isPaid == "Yes" && $isBooked == "No" && $isError == "Yes")
								$style = "background-color:#f7d449";
							    else if($isPaid == "No" && $isBooked == "Yes" && $isError == "No")
								$style = "background-color:#4eaab1";
                                ?>
                                                            <tr style="<?php echo $style ?>">				    
                                                                <td width="150" style="padding:5px !important;" align="center"><?php echo $corIC; ?>  </td>
                                                                
				<?php
							    if($isPaid == "Yes")
							    {							    
				?>
								<td width="150" style="padding:5px !important;" align="center"><b>YES</b></td>
				<?php
							    }
							    else
							    {							    
				?>
								<td width="150" style="padding:5px !important;" align="center"><b>NO</b></td>
                                <?php
							    }
							    if($isBooked == "Yes")
							    {							    
				?>
								<td width="150" style="padding:5px !important;" align="center"><b>YES</b></td>
				<?php
							    }
							    else
							    {							    
				?>
								<td width="150" style="padding:5px !important;" align="center"><b>NO</b></td>
                                <?php
							    }
				?>
							    <td width="150" style="padding:5px !important;" align="center"><a href="booking-details.php?bdir=<?php echo $base_xml_link; ?>&corid=<?php echo $corIC; ?>&ispaid=<?php echo $isPaid; ?>&isbooked=<?php echo $isBooked; ?>&iserror=<?php echo $isError; ?>">View Details</a></td>
							    </tr>
				<?php
                                                        }
                                                    }        
                                                }    
                                            }
                                        ?>				
			    </tbody>
			</table>		    
		    </form>
		</div>
		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<?php
    include_once("./includes/check-user.php");
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>SEO Meta Tags | Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
				include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo __WEBROOT__ ?>/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo __WEBROOT__ ?>/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
	<script src="<?php echo __WEBROOT__ ?>/js/handleTable.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
	<script type="text/javascript">
		_addMetas = function(){
			c = new Array();
			c.push(0);
			c.push(1);
			c.push(2);
			c.push(3);
			c.push(4);
			c.push(5);
			c.push(6);
			p = new Array();
			p.push(new Array("cell", "1", "element", "input", "type", "text", "name", "txtPageURL", "id", "txtPageURL", "className", "txtSBox", "placeholder", "Page URL"));
			p.push(new Array("cell", "2","element", "input", "type", "text", "maxLength", "100", "name", "txtMetaTitle", "id", "txtMetaTitle", "className", "txtSBox", "placeholder", "Title"));
			p.push(new Array("cell", "3","element", "textarea", "type", "textarea", "maxLength", "300", "rows", "4", "cols", "30", "name", "txtMetaDescription", "id", "txtMetaDescription", "className", "txtAreaBox", "placeholder", "Description"));
			p.push(new Array("cell", "4","element", "textarea", "type", "textarea", "maxLength", "200", "rows", "4", "cols", "30", "name", "txtMetaKeywords", "id", "txtMetaKeywords", "className", "txtAreaBox", "placeholder", "Keywords"));
			p.push(new Array("cell", "5","element", "input", "type", "button", "name", "btnSave", "id", "btnSave", "className", "addBtn", "value", "Save", "event", "onclick|#|javascript:_validateMeta('add');"));
			p.push(new Array("cell", "6","element", "input", "type", "button", "name", "btnCancel", "id", "btnCancel", "className", "addBtn", "value", "Cancel", "event", "onclick|#|javascript:_cancelAddMeta();"));
			_startEdit('seo-metas', c, p, 'add', 0, 'btnAddMeta');
			isWriteMode = true;
		};
		
		_editMeta = function(uid, idx){
			c = new Array();
			c.push(0);
			c.push(1);
			c.push(2);
			c.push(3);
			c.push(4);
			c.push(5);
			c.push(6);
			p = new Array();
			p.push(new Array("cell", "1", "element", "input", "type", "text", "name", "txtPageURL", "id", "txtPageURL", "className", "txtSBox", "placeholder", "Page URL", "value", ""));
			p.push(new Array("cell", "2","element", "input", "type", "text", "maxLength", "100", "name", "txtMetaTitle", "id", "txtMetaTitle", "className", "txtSBox", "placeholder", "Title", "value", ""));
			p.push(new Array("cell", "3","element", "textarea", "type", "textarea", "maxLength", "300", "rows", "4", "cols", "30", "name", "txtMetaDescription", "id", "txtMetaDescription", "className", "txtAreaBox", "placeholder", "Description", "value", ""));
			p.push(new Array("cell", "4","element", "textarea", "type", "textarea", "maxLength", "200", "rows", "4", "cols", "30", "name", "txtMetaKeywords", "id", "txtMetaKeywords", "className", "txtAreaBox", "placeholder", "Keywords", "value", ""));
			p.push(new Array("cell", "5","element", "input", "type", "button", "name", "btnSave", "id", "btnSave", "className", "addBtn", "value", "Save", "event", "onclick|#|javascript:_validateMeta('edit');"));
			p.push(new Array("cell", "6","element", "input", "type", "button", "name", "btnCancel", "id", "btnCancel", "className", "addBtn", "value", "Cancel", "event", "onclick|#|javascript:_cancelEditMeta(" + uid + ", " + idx + ");"));
			p.push(new Array("cell", "6","element", "input", "type", "hidden", "name", "hdUID", "id", "hdUID", "value", uid));
			_startEdit('seo-metas', c, p, 'edit', idx, 'btnAddMeta');
			isWriteMode = true;
		}

		_validateMeta = function(md){
			var chk = false;
			chk = isFilledText(document.getElementById("txtPageURL"), "", "Page URL can't be left blank.");
			if(chk == true)
				chk = isFilledText(document.getElementById("txtMetaTitle"), "", "Title can't be left blank.");
			if(chk == true)
				chk = isFilledText(document.getElementById("txtMetaDescription"), "", "Description can't be left blank.");
			if(chk == true)
				chk = isFilledText(document.getElementById("txtMetaKeywords"), "", "Keywords can't be left blank.");
			if(chk){
				var ar = new Array();
				if(md == "edit")
				ar.push(new Array("uid", document.getElementById('hdUID').value));

				ar.push(new Array("pgurl", document.getElementById('txtPageURL').value));
				ar.push(new Array("title", document.getElementById('txtMetaTitle').value));
				ar.push(new Array("description", document.getElementById('txtMetaDescription').value));
				ar.push(new Array("keywords", document.getElementById('txtMetaKeywords').value));
				
				var aj = new _ajax();
				aj.fileName = "./ajax/save-seo-metas.php";
				aj.callMethod = "post";
				aj.callParam = ar;
				aj.isAsynchronus = true;
				aj._query(function(p){_waitSaveMeta(p)}, function(p, r){_responseSaveMeta(p, r)});
				isWriteMode = false;
			}
		};
		_waitSaveMeta = function(p){

		};
		_responseSaveMeta = function(p, r){
			var rs = r.split("|#|");
			if(parseInt(rs[0]) == 1){
				document.getElementById('totRec').innerHTML = rs[1];
				_getMeta();
			}
			document.getElementById('btnAddMeta').style.visibility = 'visible';
		};
		_deleteMeta = function(uid, rid){
			var cnf = confirm("Are you sure to delete this meta?");
			if(cnf){
				var ar = new Array();
				ar.push(new Array("uid", uid));
				var aj = new _ajax();
				aj.fileName = "./ajax/delete-seo-metas.php";
				aj.callMethod = "post";
				aj.callParam = ar;
				aj.isAsynchronus = true;
				aj._query(function(p){_waitDelMeta(p)}, function(p, r){_responseDelMeta(p, r)});
			}
		};
		_waitDelMeta = function(p){
			
		};
		_responseDelMeta = function(p, r){
			document.getElementById('totRec').innerHTML = r;
			document.getElementById('btnAddMeta').style.visibility = 'visible';
			_getMeta();
		};
		_getMeta = function(){
			var ar = new Array();
			ar.push(new Array("rnd", Math.random()*99999));
			var aj = new _ajax();
			aj.fileName = "./ajax/get-seo-metas.php";
			aj.callMethod = "get";
			aj.callParam = ar;
			aj.isAsynchronus = true;
			aj._query(function(p){_waitGetMeta(p)}, function(p, r){_responseGetMeta(p, r)});
		};
		_waitGetMeta = function(p){
			document.getElementById('seo-metas-tbody').innerHTML = "<tr><td colspan='7' align='center'>Loading metas...</td></tr>";
		};
		_responseGetMeta = function(p, r){
			document.getElementById('seo-metas-tbody').innerHTML = r;
		};

		_cancelAddMeta = function(){
			_cancelEdit('seo-metas', 'btnAddMeta', 'add');
			isWriteMode = false;
		}
		_cancelEditMeta = function(uid, idx){
			p = new Array();
			p.push(new Array("cell", "6", "element", "a", "event", "href|#|javascript: void(0);", "event", "onclick|#|javascript: _editMeta(" + uid + ", " + idx + ");", "innerHTML", "Edit"));
			p.push(new Array("cell", "6", "element", "span", "innerHTML", " | "));
			p.push(new Array("cell", "6", "element", "a", "event", "href|#|javascript: void(0);", "event", "onclick|#|javascript: _deleteMeta(" + uid + ", " + idx + ");", "innerHTML", "Delete", "className", "btnDelete"));
			_cancelEdit('seo-metas', 'btnAddMeta', 'edit', idx, p);
			isWriteMode = false;
		}
	</script>
    <link rel="stylesheet" type="text/css" href="<?php echo __WEBROOT__ ?>/css/style.css?v=<?php echo mktime(); ?>" />
	<style>
		.addBtn{
			padding:5px 10px;
			margin: 0 10px 0 0;
			background-color:#000;
			color:#eee;
			font-weight:bold;
			text-decoration:none;
			border-radius: 5px 5px;
		}
		.addBtn:hover{
			background-color:#f2c900;
			text-decoration:none;
			color:#111;
		}
		.txtAreaBox {
			background: none repeat scroll 0 0 #ffffff;
			border: 1px solid #cccccc;
			border-radius: 3px;
			box-shadow: 0 2px 3px #dddddd inset;
			font-size: 16px;
			font-weight: bold;
			padding: 3px;
		}
	</style>
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> SEO Meta Tags</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./myles-enquiry.php";
			$dlndURL = "./myles-enquiry-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 200;
			$maxPageNum = 10;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "uid";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("name" => "Full Name", "mobile" => "Mobile", "ref_code" => "Source", "curr_url" => "Page", "cur_domain" => "Site");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			if(isset($_GET["f"]) && $_GET["f"] != ""){
			    $fld = $_GET["f"];
			    $isFiltered = true;
			}
			
			if(isset($_GET["sf"]) && $_GET["sf"] != "")
			$sortFld = $_GET["sf"];
			
			if(isset($_GET["sm"]) && $_GET["sm"] != "")
			$sortMode = $_GET["sm"];
			
			if(isset($_GET["dt"]) && $_GET["dt"] != "")
			    $sortDType = "CONVERT(" . $sortFld . ", " . $_GET["dt"] . ")";
			else {
			    if(isset($_GET["sf"]) && $_GET["sf"] != "")
			    $sortDType = $sortFld;
			    else
			    $sortDType = "CONVERT(" . $sortFld . ", unsigned integer)";		    
			}
			//$sortFld = "CONVERT(" . $sortFld . "," . $_GET["dt"] . ")";			    
			
			$lStart = ($cPage - 1) * $recPerPage;			
			$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM cor_seo_mata_tags WHERE " . $fld . " = '" . $fVal . "'";
			} else {
			    $sql = "SELECT COUNT(uid) as TotalRecord FROM cor_seo_mata_tags";
			}
			$r = $db->query("query", $sql);
			if(!array_key_exists("response", $r)){
			    $totRec = $r[0]["TotalRecord"];
			}
			unset($r);
			
			$numOfPage = ceil($totRec / $recPerPage);
			
			//$totRec = 0;
			if($fld != "" && $fVal != ""){
			    $sql = "SELECT * FROM cor_seo_mata_tags WHERE " . $fld . " = '" . $fVal . "'";
			    $sql .= " ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			} else {
			    $sql = "SELECT * FROM cor_seo_mata_tags ORDER BY " . $sortDType . " " . $sortMode . " LIMIT " . $lStart . ", " . $recPerPage;
			}
			//echo $sql;
			$r = $db->query("query", $sql);
			
			$dtDiff = strtotime($sDate->format("Y-m-d")) - strtotime($eDate->format("Y-m-d"));
			$db->close();
			//print_r($r);
		?>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
					<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
						<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td><span class="dataLabel">Total Pages: <b style='color:#00f;' id="totRec"><?php echo $totRec; ?></b></span></td>
							<td><a href="javascript: void(0);" id="btnAddMeta" onclick="javascript: _addMetas();" class="addBtn">Add Meta</a></td>
						</tr>
						</table>
					</td>
			    </tr>
			</table>
			<table width="99%" class="adm" id="seo-metas" cellspacing="1" border="0" align="left" style="font-size:14px;float: left;margin-top: 10px;">
			    <thead>
				<tr>
				    <th align="left">#</th>
				<?php
				    $sm = $sortFld == "page_url" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=page_url" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=page_url&sm=" . $sm ?>">Page URL <?php if ($sortFld == "source") { ?><img src="<?php echo __WEBROOT__ ?>/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
				<?php
				    $sm = $sortFld == "metaTitle" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=metaTitle&sm=" . $sm ?>&dt=unsigned integer">Meta Title <?php if ($sortFld == "name") { ?><img src="<?php echo __WEBROOT__ ?>/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
                    <th align="left">Meta Description</th>
					<th align="left">Meta Keywords</th>
				<?php
				    $sm = $sortFld == "entry_date" ? $sortMode == "asc" ? "desc" : "asc" : "asc";
				?>
				    <th align="left"><a href="<?php echo $url . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm')) == "" ? "?sb=source" : "?" . filter_querystring($_SERVER["QUERY_STRING"], array('sf', 'sm', 'dt')) . "&sf=entry_date&sm=" . $sm ?>&dt=datetime">Updated On <?php if ($sortFld == "entry_date" || $sortFld == "unique_id") { ?><img src="<?php echo __WEBROOT__ ?>/img/<?php echo $sortMode; ?>.png" border="0" /><?php } ?></a></th>
					<th>Action</th>
				</tr>
			    </thead>
			    <tbody id="seo-metas-tbody">
		    <?php
			    if(!array_key_exists("response", $r)){
				$srno = $lStart + 1;
				for($i = 0; $i < count($r); $i++){
				    $cls = "od";
				    if($i % 2 == 0)
					$cls = "ev";
				    
				    $bookDate = date_create($r[$i]["entry_date"]);
		    ?>
				    <tr class="<?php echo $cls; ?>">
					<td align="left"><?php echo $srno; ?></td>
					<td align="left"><?php echo $r[$i]["page_url"]; ?></td>
					<td align="left"><?php echo $r[$i]["metaTitle"]; ?></td>
					<td align="left"><?php echo $r[$i]["metaDescription"]; ?></td>
					<td align="left"><?php echo $r[$i]["metaKeyword"]; ?></td>
					<td align="left"><?php echo $bookDate->format('d M, Y') . "&nbsp;<small>" . $bookDate->format('H:i A'); ?></small></td>
					<td align="left"><a href="javascript: _editMeta(<?php echo $r[$i]["uid"]; ?>, <?php echo $srno; ?>);">Edit</a> | <a href="javascript: _deleteMeta(<?php echo $r[$i]["uid"]; ?>, <?php echo $srno; ?>);">Delete</a></td>
				    </tr>
		    <?php
				    $srno++;
				}
			    }
		    ?>
			    </tbody>
			</table>
		    <?php
			unset($r);
		    ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
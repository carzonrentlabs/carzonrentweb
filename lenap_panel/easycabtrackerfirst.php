<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    include_once("./includes/check-user.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
		<head>
		<title>Booking Status | Carzonrent Admin</title>
		<?php
		include_once("./includes/cache-func.php");
		include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
		include_once("./includes/config.php");
		include_once("./classes/bb-mysql.class.php");
		include_once("./classes/cor.ws.class.php");
		include_once("./classes/cor.xmlparser.class.php");
       ?>
	   <style>
	   .h1 {
         font-weight: bold;
       }
	   </style>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
	qsArray = new Array();
    </script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Easycab Payback Status</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
			<table cellpadding="5" cellspacing="0" border="0" style="margin-top: 10px; width: 99%;float: left;">
			    <tr>
				<td align="left" style="padding-left: 10px;font-size: 16px;width:50%;">
				    <form id="" name="" action="./easycabtrackerfirst.php" method="GET" onsubmit="return validate();">
				    <table cellpadding="0" cellspacing="0" border="0">
					<tr>
					    <td colspan="2" style="padding-left: 12px;">Enter Easycabe ID</td>
					    <td style="padding-left: 10px;">
						<input type="text" id="q" name="q" class="txtSBox" size="22" placeholder="Enter here" <?php if(isset($_GET["q"])) { echo "value=" . $_GET["q"]; } ?> /></td>
					    <td style="padding-left: 15px;"><input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm2Btn" id="frm2Btn" /></td>
					</tr>
				    </table>
				    </form>
				</td>
			    </tr>
			</table>
	<?php 
	 if(isset($_REQUEST['q']) && $_REQUEST['q']!="")
	 {
	 
		$esaycabsid=$_REQUEST['q'];
		$db = new MySqlConnection(CONNSTRING);
		$db->open();
		$sql = "SELECT * FROM easycabs_payback_burn where ec_uid='".$esaycabsid."'";
		$record = $db->query("query", $sql);
		
	 
	
		 ?>
			<table cellpadding="5" cellspacing="1" class="adm" border="0" style="margin: 10px 1.5%; width: 97%;float: left;">
				  
<?php 

foreach($record  as $val)
{




?>				  
				   <tr class="od">
					<td class="h1">UserName</td>
					<td><?php echo $val['pax_name']; ?></td>
					<td class="h1">Mobile</td>
					<td><?php echo $val['mobile']; ?></td>
				    </tr>
				    <tr class="ev">
					<td class="h1">Branch ID</td>
					<td><?php echo  $val['branchid']; ?></td>
					<td class="h1">Entry Date</td>
					<td><?php echo $val['entry_date'];  ?></td>
				    </tr>
				    <tr class="od">
					<td class="h1">Payback Transe ID </td>
					<td><?php echo $val['pb_txn_id']; ?></td>
					<td class="h1">Payback Discount Val</td>
					<td><?php echo $val['pb_disc_value']; ?></td>
				    </tr>
				    <tr class="ev">
					<td class="h1">Discount</td>
					<td><?php echo $val['ec_disc_code']; ?></td>
					<td class="h1">Order ID</td>
					<td><?php echo $val['pn_order_id'] ?></td>
				    </tr>
				    
				</table>
			
			
		  <?PHP 
		}
		}
	
	?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p <?php if(!isset($_GET["q"])) { echo "style=\"height:300px;\""; } ?>>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	unset($bookingDetails);
	?>
    </body>
</html>
<script>
function validate()
{
 var code = $('#q').val();
 if(code=="")
 {
	 alert('Please Enter Coupan code');
	 $('#q').css('background-color','pink').attr('placeholder','Please Enter Coupan code').focus();
	 return false;
 }
}
</script>
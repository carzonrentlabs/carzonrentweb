<?php
    //error_reporting(0);
    //ini_set("display_errors", 1);
	//include_once("./includes/config.php");

    include_once("includes/cache-func.php");    
    include_once('classes/cor.ws.class.php');
    include_once('classes/cor.xmlparser.class.php');
    include_once("classes/cor.mysql.class.php");    
    $act_count = 0;
    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    
	$book = $db->query("stored procedure", "cor_bookings_failedtest()");
	
	
	if(!array_key_exists("response",$book))
	{
	    for($b = 0; $b < count($book); $b++){
		$fullname = explode(" ", $book[$b]["full_name"]);
		if(count($fullname) > 1){
		    $firstName = $fullname[0];
		    $lastName = $fullname[1];
		} else {
		    $firstName = $fullname[0];
		    $lastName = "";
		}
		    $cor = new COR();
			if($book[$b]["tour_type"]!= "Selfdrive"){
			
			
			$corArrSTD = array();
		    $corArrSTD["pkgId"] = $book[$b]["package_id"];
		    $corArrSTD["destination"] = $book[$b]["destinations_name"];
		    $pDate = date_create($book[$b]["pickup_date"]);
		    $corArrSTD["dateOut"] = $pDate->format("Y-m-d");
		    $dDate = date_create($book[$b]["drop_date"]);
		    $corArrSTD["dateIn"] = $dDate->format("Y-m-d");
		    $pTime = date_create($book[$b]["pickup_time"]);
		    $corArrSTD["TimeOfPickup"] = $pTime->format("is");;
		    $corArrSTD["address"] = str_ireplace(array("<br>","<br />"), "", nl2br($book[$b]["address"]));
		    $corArrSTD["firstName"] = $firstName;
		    $corArrSTD["lastName"] = $lastName;
		    $corArrSTD["phone"] = $book[$b]["mobile"];
		    $corArrSTD["emailId"] = $book[$b]["email_id"];
		    $corArrSTD["userId"] = $book[$b]["cciid"];
		    $corArrSTD["paymentAmount"] = $book[$b]["tot_fare"];
		    $corArrSTD["paymentType"] = "1";
		    $corArrSTD["paymentStatus"] = "1";
		    $corArrSTD["trackId"] = $book[$b]["coric"];
		    $corArrSTD["transactionId"] = $book[$b]["nb_order_no"];
		    $corArrSTD["discountPc"] = $book[$b]["discount"];
		    $corArrSTD["discountAmount"] = $book[$b]["discount_amt"];
		    $corArrSTD["remarks"] = $book[$b]["remarks"];
		    $corArrSTD["totalKM"] = $book[$b]["distance"];
		    $corArrSTD["visitedCities"] = $book[$b]["destinations_name"];
		    if($book[$b]["tour_type"] != "Local")
		    $outStationYN = "true";
		    else
		    $outStationYN = "false";
		    $corArrSTD["outStationYN"] = $outStationYN;
		    $corArrSTD["OriginCode"] = $book[$b]["source"]; //Aamir 1-Aug-2012
		    $corArrSTD["chkSum"] = $book[$b]["Checksum"];
		    $corArrSTD["DiscountCode"] = $book[$b]["promotion_code"];
		    $corArrSTD["PromotionCode"] = $book[$b]["discount_coupon"];
		    $corArrSTD["IsPayBack"] = $book[$b]["isPayBack"];
		    $corArrSTD["discountTrancastionID"] = $book[$b]["disc_txn_id"];
			$corArrSTD["latitude"] = $book[$b]["latitude"];
            $corArrSTD["longitude"] = $book[$b]["longitude"];
			
			$corArrSTD["IndicatedCGSTTaxPercent"] = $book[$b]["CGSTPercent"];
			$corArrSTD["IndicatedSGSTTaxPercent"] = $book[$b]["SGSTPercent"];
			$corArrSTD["IndicatedIGSTTaxPercent"] = $book[$b]["IGSTPercent"];
			$corArrSTD["GSTEnabledYN"] = $book[$b]["GSTEnabledYN"];
			$corArrSTD["IndicatedClientGSTId"] = $book[$b]["ClientGSTId"];

			echo '<pre>';
			print_r($corArrSTD);
			
			$res = $cor->_CORMakeBookingWithLatLong($corArrSTD);
			
			//$res = $cor->_CORMakeBooking($corArrSTD);
			print_r($res);
			$myXML = new CORXMLList();
		    if($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'} != ""){
			$arrUserId = $myXML->xml2ary($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'});
			if(key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0){					  
			    $dataToSave = array();
			    $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
			    $dataToSave["payment_status"] = "1";  // Added by Iqbal on 28Aug2013
			    $dataToSave["payment_mode"] = "1";
			    $whereData = array();
			    $whereData["coric"] = $book[$b]["coric"];		
			    $u = $db->update("cor_booking_new", $dataToSave, $whereData);
					
				///////////
				
					$dataAPPToSave = array();
				$dataAPPToSave["BookingID"] = $arrUserId[0]["Column1"];
				$dataAPPToSave["PaymentStatus"] = 1;     
				$dataAPPToSave["PaymentAmount"] = $book[$b]["tot_fare"];
				$dataAPPToSave["TransactionId"] = $book[$b]["nb_order_no"];
				$dataAPPToSave["ckhSum"] = $book[$b]["Checksum"];
				$dataAPPToSave["Trackid"] = $book[$b]["coric"];
				$dataAPPToSave["discountPc"] = "0";
				$dataAPPToSave["discountAmount"] = "0";
				$dataAPPToSave["DisCountCode"] = "";
				$dataAPPToSave["DiscountTransactionId"] = "";
		
				$cor = new COR();
					
				 $res = $cor->_CORUpdatePayment($dataAPPToSave);
				
				unset($dataAPPToSave);
				
				///////////////
				
				
			    unset($whereData);
			    unset($dataToSave);
			    
			}
		   elseif($arrUserId[0]["Column1"] == "NoCar")
			{
				$db = new MySqlConnection(CONNSTRING);
				$db->open();
				$sql = "UPDATE cor_booking_new SET booking_id = '0' WHERE coric= '".$book[$b]["coric"]."'";
				$r = $db->updatenew($sql);
				
				$html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
						$html .= "<tr>";
						$html .= "<td colspan='2'>Dear Team,<br/><br/>The following booking is failed post successful payment by the customer:
						</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Cor id</td>";
						$html .= "<td>" .$book[$b]["coric"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer name</td>";
						$html .= "<td>" . $book[$b]["full_name"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer email id</td>";
						$html .= "<td>" . $book[$b]["email_id"]."</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Customer mobile number</td>";
						$html .= "<td>" . $book[$b]["mobile"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Pick up city</td>";
						$html .= "<td>" . $book[$b]["origin_name"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Car model</td>";
						$html .= "<td>" . $book[$b]["model_name"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Pick up date and time</td>";
						$html .= "<td>" .$book[$b]["pickup_date"]." : ".$book[$b]["pickup_time"] . "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Drop off date and time</td>";
						$html .= "<td>" . $book[$b]["drop_date"]." : ".$book[$b]["drop_time"] . "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Fare amount</td>";
						$html .= "<td>" . $book[$b]["tot_fare"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Trip type</td>";
						$html .= "<td>" . $book[$b]["trip_type"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Booking channel</td>";
						$html .= "<td>" . $book[$b]["source"]. "</td>";
						$html .= "</tr>";
						$html .= "<tr>";
						$html .= "<td>Please speak with the customer and check if an alternate booking can be made <br>Regards,
						<br>Myles IT team
						</td>";
						$html .= "</tr>";
						$html .= "</table>";
						$dataToMail= array();
						$dataToMail["To"] = "vinod.maurya@carzonrent.com;rahul.jain@carzonrent.com";
						$dataToMail["Subject"] = "Booking failure  | Payment success";
						$dataToMail["MailBody"] = $html;
						$res = $cor->_CORSendMail($dataToMail);
						unset($dataToMail);
						// New code Added by vinod
						unset($whereData);
						unset($dataToSave);
				
			}
		    }
		}
		unset($cor);
		$act_count++;
	    }
	}
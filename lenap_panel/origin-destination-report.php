<?php
    include_once("./includes/check-user.php");
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
            <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/admin.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/ajax.js" type="text/javascript"></script>
	    <script>
	    $(function() {
		    var dateToday = new Date();
		    dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    var dateNextDay = new Date();
		    dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    $( ".datepicker" ).datepicker({minDate: dateToday, maxDate: dateNextDay});
	    });
	    </script>     
    </head>
    <body>
    <?php     include_once("./includes/header.php"); ?>
    <div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> Search</h2>
		</div>
	    </div>
            <div class="main" style="width:100%;">
		<br><br>
		<div style="margin:0 auto;width:99%;">
			<?php     include_once("./includes/search-menu.php"); ?>
                        <?php
                                            $db = new MySqlConnection(CONNSTRING);
                                            $db->open();
                                            
                                            $sDate = "";
                                            if(isset($_GET["startdate"]) && $_GET["startdate"] != ""){
                                                $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
                                            }
                                            else {
                                                $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
                                                $sDate = date_create($maxDate[0]["max_date"]);
                                            }
                                            
                                            $eDate = "";
                                            if(isset($_GET["enddate"]) && $_GET["enddate"] != ""){
                                                $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
                                            }
                                            else {
                                                $eDate = $sDate;
                                            }
                                            $tour_type = "OutStation";
                                            if(isset($_GET["tt"]))
                                                $tour_type = $_GET["tt"];
                                            else
                                                $tour_type = "OutStation";
                                            $osCSS = "text-decoration: underline;color:#666;";
                                            $loCSS = "text-decoration: underline;color:#666;";
                                            $sdCSS = "text-decoration: underline;color:#666;";
                                            if($tour_type == "selfdrive")
                                                $sdCSS .= "font-weight:bold;";
                                            elseif($tour_type == "local")
                                                $loCSS .= "font-weight:bold;";
                                            else
                                                $osCSS .= "font-weight:bold;";
                                        ?>
                        <form id="frmSearch" name="frmSearch" action="origin-destination-report.php" method="get">
			    <input type="hidden" id="tt" name="tt" value="<?php echo $tour_type; ?>" />
                            <table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
                                <tbody>
                                    <tr>                                        
                                        <td><label>Start Date</label></td>
                                        <td>
                                            <span class="datepick">
                                                <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
                                            </span>
                                        </td>
                                        <td>
                                            <label>End Date </label>
                                        </td>
                                        <td><span class="datepick">
                                                        <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
                                                    </span>
                                        </td>
                                        <td>
                                            <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
                                        </td>
					<td valign="middle">					
					    <a href="down-load-origin-destination-report.php?sdate=<?php echo $sDate->format('Y-m-d'); ?>&edate=<?php echo $eDate->format('Y-m-d'); ?>&tt=<?php echo $tour_type?>">Download</a>
					</td>
                                    </tr>
                                    <tr>                                        
                                        <td colspan="5" style="padding:15px 0px 5px 0px;"><a style="<?php echo $osCSS; ?>" href="./origin-destination-report.php?startdate=<?php echo urlencode($sDate->format('d M, Y')); ?>&enddate=<?php echo urlencode($eDate->format('d M, Y')); ?>&tt=outstation">Outstation<a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="<?php echo $loCSS; ?>" href="./origin-destination-report.php?startdate=<?php echo urlencode($sDate->format('d M, Y')); ?>&enddate=<?php echo urlencode($eDate->format('d M, Y')); ?>&tt=local"></?php>Local</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="<?php echo $sdCSS; ?>" href="./origin-destination-report.php?startdate=<?php echo urlencode($sDate->format('d M, Y')); ?>&enddate=<?php echo urlencode($eDate->format('d M, Y')); ?>&tt=selfdrive">Self Drive</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <div style="clear: both"></div>
                        <table width="99%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
                            <tbody>
                               <tr>
                                    <td align="center" width="10%" class="search_table">SrNo.</td>
                                    <td align="center" width="30%" class="search_table">Search (Origin - Destination)</td>
                                    <td align="center" width="30%" class="search_table">Search Date</td>
				    <td align="center" width="30%" class="search_table">Count</td>                                    
                                </tr>
                             <?php
				$r = $db->query("stored procedure", "cor_OriginDestinationReport('". $sDate->format('Y-m-d') ."','".$eDate->format('Y-m-d')."','".$tour_type."')");
                               $k = 1;
				if(!array_key_exists("response", $r)){
				    $gTotal = 0;
				    for($i=0 ; $i < count($r); $i++)
				    {
					$gTotal += $r[$i]["total"];
                                        if($r[$i]["origin_name"] != "" && $r[$i]["destination_name"] !="")
                                        {
                                            $style = "";					
					if($k%2 == 0)
					{
					    $style = "background-color:#f1f1f1;";
					}
					else
					{
					    $style = "background-color:#dddada;";
					}
                            ?>
                            <tr style="<?php echo $style ?>">
                                    <td width="10%" style="padding:5px !important;"><?php echo $k; ?></td>
                                    <td align="left" style="padding:5px !important;" width="30%"><?php echo $r[$i]["origin_name"] ?> - <?php echo $r[$i]["destination_name"] ?></td>
                                    <td align="left" style="padding:5px !important;" width="30%"><a style=" cursor: pointer" onclick="javascript: _getDateWiseDetails(<?php echo $k ?>,'<?php echo $sDate->format('Y-m-d')?>','<?php echo $eDate->format('Y-m-d') ?>','<?php echo $r[$i]["origin_name"] ?>','<?php echo $r[$i]["destination_name"] ?>','<?php echo $tour_type?>');_disableThisPage();_setDivPos('divSignInUp');"><?php echo $sDate->format('Y-m-d')?> -- <?php echo $eDate->format('Y-m-d') ?></a></td>
				    <td align="left" style="padding:5px !important;" width="30%"><?php echo $r[$i]["total"] ?></td>                                    
                                </tr>				
                            <?php
                                            $k++;
                                        }
                                    }
			    ?>
				<tr style="<?php echo $style ?>">
                                    <td colspan="3" class="search_table">Grand Total</td>
				    <td align="left" class="search_table" width="30%"><?php echo $gTotal; ?></td>                                    
                                </tr>
			    <?php
                                }
                                else
                                {
                            ?>
                                <tr style="background-color:#f1f1f1;">
					<td align="center" colspan="10">No data found.</td>
				    </tr>
                            <?php
                                   
                                }
                            ?>
                            </tbody>
                        </table>
                </div>
            </div>
    </div>
    <?php
    include_once("./includes/footer.php");
    ?>
    <div class="floatingDiv2" id="divSignInUp" style="left: 340px; top: 219px;">	  
	    <a class="closepopopup" href="javascript: void(0)" onclick="_hideFloatingObjectWithID('divSignInUp'); _enableThisPage();_clearFrom();"><img border="0" title="Close" alt="Close" src="<?php echo corWebRoot; ?>/images/minus.png"></a>
	    <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left">
		<thead>
		    <tr>
			<td align="center" width="10%" class="search_table">SrNo.</td>
			<td align="center" width="30%" class="search_table">Search (Origin - Destination)</td>
			<td align="center" width="30%" class="search_table">Search Date</td>
			<td align="center" width="30%" class="search_table">Count</td>                                    
		    </tr>
		</thead>
		<tbody id = "tbdy"></tbody>
	    </table>
    </div>
    </body>
</html
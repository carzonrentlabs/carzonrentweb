<?php
	include_once("./includes/check-user.php");
  
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    
    // output the column headings
    fputcsv($output, array('entry_date', 'tour_type', 'origin_name','destination_name','pickup_date','duration','distance','ip','user_agent'));
    
    // fetch the data
    include_once("./includes/config.php");
    include_once("./classes/bb-mysql.class.php");
    //mysql_connect('localhost', 'username', 'password');
    //mysql_select_db('database');
    $db = new MySqlConnection(CONNSTRING);
    $db->open();   
    $rows = $db->query("stored procedure","cor_downloadCustomerSearchData('".$_GET["sdate"]."','".$_GET["edate"]."')");    
    // loop over the rows, outputting them
    for($i = 0;$i< count($rows);$i++)
    {
         fputcsv($output, array($rows[$i]["entry_date"],$rows[$i]["tour_type"],$rows[$i]["origin_name"],$rows[$i]["destination_name"],$rows[$i]["pickup_date"],$rows[$i]["duration"],$rows[$i]["distance"],$rows[$i]["ip"],$rows[$i]["user_agent"]));        
    }    
?>
<?php
    include_once("./includes/check-user.php");
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
				include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
		<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
		<script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/ajax.js" type="text/javascript"></script>
	    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/admin.js" type="text/javascript"></script>
		<!-- <script src="./js/admin.js" type="text/javascript"></script> -->
		
	    <script>
	    $(function() {
		    var dateToday = new Date();
		    dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    var dateNextDay = new Date();
		    dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		    $( ".datepicker" ).datepicker({minDate: dateToday, maxDate: dateNextDay});
	    });
	    </script>
	    <style type="text/css">
		.wordwrap {
			    white-space: pre-wrap;
			    word-wrap: break-word;
			    }	
	    </style>
    </head>
    <body>
    <?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#666666">Dashboard &raquo;</a> EasyCabs Failed Booking Report</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<br><br>
		<div style="margin:0 auto;width:99%;">			
			<form id="frmSearch" name="frmSearch" action="failed-bookings-ec.php" method="get">
			<table width="65%" cellspacing="1" cellpadding="5" border="0" align="center">
			    <tbody>
                <tr>
				    <?php
					$db = new MySqlConnection(CONNSTRING);
					$db->open();
					
					$sDate = "";
					if(isset($_GET["startdate"]) && $_GET["startdate"] != ""){
					    $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
					}
					else {
					    $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
					    $sDate = date_create($maxDate[0]["max_date"]);
					}
					$eDate = "";
					if(isset($_GET["enddate"]) && $_GET["enddate"] != ""){
					    $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
					}
					else {
					    $eDate = $sDate;
					}
				    ?>
                                    <td><label>Start Date</label></td>
				    <td>
					<span class="datepick">
					    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
					</span>
				    </td>
				    <td><label>End Date </label>
				    </td>
				    <td><span class="datepick">
						    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
						</span>
				    </td>
				    <td>
					<input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
				    </td>
				    <td valign="middle">					
					<a href="download-new-booking-ec-failure.php?sdate=<?php echo $sDate->format('Y-m-d 00:00:00'); ?>&edate=<?php echo $eDate->format('Y-m-d 23:59:59'); ?>">Download</a>
				    </td>
                 </tr>
				</tbody>
			</table>
			</form>			
			<table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="color:#323232;table-layout: fixed;margin-top:30px;">
			    <tbody id="searchData">			
                <tr>
				    <td align="center" width="3%" class="search_table">SrNo.</td>
					<td align="center" width="10%" class="search_table">Book Date</td>
				    <td align="center" width="10%" class="search_table">Booking ID</td>
				    <td align="center" width="6%" class="search_table">Keyword</td>
				    <td align="center" width="10%" class="search_table">Mobile</td>				    
				    <td align="center" width="10%" class="search_table">Name</td>
				    <td align="center" width="16%" class="search_table">Pickup Address</td>
				    <td align="center" width="10%" class="search_table">Location</td>
				    <td align="center" width="10%" class="search_table">Landmark</td>
				    <td align="center" width="10%" class="search_table">Pickup Datetime</td>				    
				    <td align="center" width="10%" class="search_table">Destination Address</td>
				    <td align="center" width="10%" class="search_table">Destination Landmark</td>
				    <td align="center" width="10%" class="search_table">IP<br />Browser</td>
				    <td align="center" width="10%" class="search_table">Error</td>
                </tr>			    
            <?php					     								
				$r = $db->query("stored procedure", "cor_new_easycabs_bookings_failure('".$sDate->format('Y-m-d 00:00:00')."','".$eDate->format('Y-m-d 23:59:59')."')");
				//print_r($r);
				if(!array_key_exists("response", $r)){
				    for($i=0 ; $i < count($r); $i++)
				    {				   
					$style = "";					
					if($i%2 == 0)
					    $style = "background-color:#f1f1f1;";
					else
					    $style = "background-color:#dddada;";					
                        ?>
				    <tr style="<?php echo $style ?>">
					<td style="padding:5px !important;word-wrap:break-word;" name = "seqno"><?php echo ($i+1) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('F j, Y', strtotime($r[$i]["entry_datetime"])) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["booking_id"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["keyword"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["cust_mobile"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["guest_name"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["pickup_address"] ?></td>
					<?php
					    $dispLocation = "";
					    switch ($r[$i]["pickup_location"]) {
						case 1:
						    $dispLocation = "Delhi";
						    break;
						case 2:
						    $dispLocation = "Mumbai";
						    break;
						case 3:
						    $dispLocation = "Bangalore";
						    break;
						case 4:
						    $dispLocation = "Hyderabad";
						    break;
					    }
					?>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $dispLocation ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["pickup_landmark"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo date('F j, Y', strtotime($r[$i]["pickup_datetime"])) ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["drop_landmark"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["dest_address"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["ip"] . "<br /><br />" . $r[$i]["ua"] ?></td>
					<td style="padding:5px !important;word-wrap:break-word;"><?php echo $r[$i]["err"]; ?></td>
				    </tr>
            <?php
				    }
				    unset($r);
				}
				else {
			?>
				    <tr style="background-color:#f1f1f1;">
					<td align="center" colspan="12">No data found.</td>
				    </tr>
			<?php
				}
                        ?>
			    </tbody>
			</table>
			<div style="clear:both"></div>
			<?php
			 if($i >= 50)
                        {
                            ?>
			<table width="100%" cellspacing="1" cellpadding="5" border="0" style="float: left;margin:15px 0 0 0px;">
			    <tbody id = "loadmore">
				<tr>
				    <td align="right">
					<a href="javascript:void(0);" onclick="javascript: _loadMoreNewBookingECFail(<?php echo $i ?>,'<?php echo $sDate->format('Y-m-d 00:00:00') ?>','<?php echo $eDate->format('Y-m-d 23:59:59') ?>')">LOAD MORE</a>
				    </td>
				</tr>
			    </tbody>
			</table>
			<?php
                        }
                        ?>
		</div>		
		<div class="clr"></div>
	    </div>
	    <p>&nbsp;</p>	    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
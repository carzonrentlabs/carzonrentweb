<?php

    include_once("./includes/check-user.php");
    
    function browser_info($agent=null) {
	// Declare known browsers to look for
	$known = array('msie', 'firefox', 'chrome', 'safari', 'webkit', 'opera', 'netscape',
	  'konqueror', 'gecko');
	
	// Clean up agent and build regex that matches phrases for known browsers
	// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
	// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
	$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
	$pattern = '#(?<browser>' . join('|', $known) .
	  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
	
	// Find all phrases (or return empty array if none found)
	if (!preg_match_all($pattern, $agent, $matches)) return array();
	
	// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
	// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
	// in the UA).  That's usually the most correct.
	$i = count($matches['browser'])-1;
	return array($matches['browser'][$i] => $matches['version'][$i]);
    }
    
    function filter_querystring($query_string, $arrFields){
	if($query_string != ""){
	    $qString = "";
	    if(count($arrFields) > 0){
		$qsParams = explode("&", $query_string);
		for($q = 0; $q < count($qsParams); $q++){
		    $qsParam = explode("=", $qsParams[$q]);
		    $iF = -1;
		    for($f = 0; $f < count($arrFields); $f++){
			if($qsParam[0] == $arrFields[$f]){
			    $iF = $f;
			    break;
			}
		    }
		    if($iF == -1)
			$qString .= $qString == "" ? $qsParams[$q] : "&" . $qsParams[$q];
		}
	    } else {
		$qString = $query_string;
	    }
	} else
	$qString = "";
	return $qString;
    }
    $currentMonth = date("m");
    $curyear = date("Y");
    if(isset($_REQUEST['month'])){
        $mnt = $_REQUEST['month'];
    }else{
        $mnt = $currentMonth;
    }
    if(isset($_REQUEST['year'])){
        $yr = $_REQUEST['year'];
    }else{
        $yr = $curyear;
    }
    if(isset($_REQUEST['ddlOrigin'])){
        $orgn = $_REQUEST['ddlOrigin'];
    }else{
        $orgn = '2';
    }
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    <title>Carzonrent Admin</title>
            <?php
                include_once("./includes/cache-func.php");
                include_once("./includes/header-css.php");
		include_once("./includes/header-js.php");
                include_once("./includes/config.php");
                include_once("./classes/bb-mysql.class.php");
            ?>
    
    <?php 
  //echo "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = 2015 and month(pickup_date) = 08 and day(pickup_date) <= 01) && (year(drop_date) = 2015 and month(drop_date) = 08 and day(drop_date) >= 01)) and origin_id = 2";
    ?>
    
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/bytesbrick.ajax.js" type="text/javascript"></script>
    
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js?v=<?php echo mktime(); ?>" type="text/javascript"></script>
    <script src="<?php echo corWebRoot; ?>/lenap_panel/js/chart.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		var minSDate = new Date();
		minSDate.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		var maxEDate = new Date();
		maxEDate.setFullYear(<?php echo date('Y'); ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
		$( ".datepicker" ).datepicker({minDate: minSDate, maxDate: maxEDate});
	});
    </script>
<link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
    
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/highcharts.js"></script>
<script src="<?php echo corWebRoot; ?>/lenap_panel/js/exporting.js"></script>


    </head>
    <body>
	<?php include_once("./includes/header.php"); ?>
	<div id="middle">	
	    <div class="yellwborder">
		<div style="padding:7px 0px 7px 10px;width: 98% !important;" class="magindiv">
		    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Myles Booking Details</h2>
		</div>
	    </div>
	    <div class="main" style="width:100%;">
		<div>
		    <?php
			$url = "./myles-booking-metrics.php";
			//$dlndURL = "./selfdrive-search-download.php";
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerPage = 100;
			$maxPageNum = 7;
			$cPage = 1;
			$sDate = date_create(date("Y-m-d"));
			$eDate = date_create(date("Y-m-d"));
			$fld = "";
			$val = "";
			$sortFld = "unique_id";
			$sortMode = "desc";
			$sortDType = "varchar";
			$isDtInc = false;
			$isFiltered = false;
			$arrFields = array("source" => "Source", "coric" => "CORIC", "booking_id" => "Booking ID", "tour_type" => "Tour Type", "package_id" => "Package ID","car_cat" => "Car Category", "origin_name" => "Origin", "destinations_name" => "Destination", "tot_fare" => "Fare", "payment_mode" => "Mode of Payment", "payment_status" => "Payment Status", "cciid" => "Customer ID", "full_name" => "Customer Name", "email_id" => "Customer Email ID", "mobile" => "Customer Mobile", "Checksum" => "Checksum", "nb_order_no" => "CCA Order ID");

			if(isset($_GET["pg"]) && $_GET["pg"] != "")
			$cPage = $_GET["pg"];
			
			if(isset($_GET["sd"]) && $_GET["sd"] != ""){
			    $sDate = date_create(str_ireplace(",", "", urldecode($_GET["sd"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			    
			if(isset($_GET["ed"]) && $_GET["ed"] != ""){
			    $eDate = date_create(str_ireplace(",", "", urldecode($_GET["ed"])));
			    $isFiltered = true;
			    $isDtInc = true;
			}
			
			
			
		?>
	    
			<table cellpadding="5" cellspacing="0" border="0" align="left" style="margin-top: 10px; width: 99%;">
			    <tr>
				<td>
				    <form id="frmDateFilter" name="frmDateFilter" action="<?php echo $url; ?>" method="get">
				    <table cellspacing="1" cellpadding="5" border="0" align="left">
				    <tbody>
					<tr>
					    <td style="padding-left: 10px;"><label>Month</label></td>
					    <td colspan="3" style="padding-left: 10px;"><label>Year</label></td>
					</tr>
					<tr>
					    <td style="padding-left: 10px;">
						
						    <select class="ddlS" name="month" id="month" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        $months = array("01"=>"Jan", "02"=>"Feb", "03"=>"Mar", "04"=>"Apr","05"=>"May","06"=>"June","07"=>"July","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
                                                        
                                                        $sel = '';
                                                        $mm = 0;
                                                        foreach($months as $k=>$month) {
                                                            if(isset($_REQUEST['month'])){
                                                                if(($_REQUEST['month'] == $k) && $mm==0){
                                                                $sel = 'selected = "selected"';
                                                                $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($currentMonth==$k && $mm==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $mm++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $k . "\">" . $month . "</option>";
                                                            }
                                                        ?>

                                                    </select>
						
					    </td>
                                                
					    <td style="padding-left: 10px;">
                                                <select class="ddlS" name="year" id="year" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <?php
                                                        
                                                        $sel = '';
                                                        $nn = 0;
                                                        for($i=2010;$i<=$curyear;$i++){
                                                            if(isset($_REQUEST['year'])){
                                                                if(($_REQUEST['year'] == $i) && $nn==0){
                                                                $sel = 'selected = "selected"';
                                                                $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($curyear==$i && $nn==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $nn++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }

                                                            echo "<option $sel value=\"" . $i . "\">" . $i . "</option>";
                                                        }
                                                        ?>

                                                    </select>
					    </td>
                                            <td><label>Location</label></td>
                                            <td>
                                                <?php
                                                $db = new MySqlConnection(CONNSTRING);
                                                $db->open();
                                                $sql = "SELECT * FROM cor_location";
                                                $rloc = $db->query("query", $sql);
                                                if (!array_key_exists("response", $rloc)) {
                                                    ?>
                                                    <select class="ddlS" name="ddlOrigin" id="ddlOriginSF" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                        <option value="0">Select All</option>
                                                         <?php
                                                        $sel = '';
                                                        $ll = 0;
                                                        for ($i = 0; $i < count($rloc); $i++) {
                                                               if(isset($_REQUEST['ddlOrigin'])){
                                                                if(($_REQUEST['ddlOrigin'] == $rloc[$i]["city_id"]) && $ll==0){
                                                                $sel = 'selected = "selected"';
                                                                $ll++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }else{
                                                                if($rloc[$i]["city_id"] == '2' && $ll==0){
                                                                    $sel = 'selected = "selected"';
                                                                    $ll++;
                                                                }else{
                                                                    $sel = '';
                                                                }
                                                            }  

                                                           ?>
                                                        
                                                            <option <?php echo $sel;?> value="<?php echo $rloc[$i]["city_id"]; ?>"><?php echo $rloc[$i]["CityName"]; ?></option>
                                                            
                                                        <?php } ?>

                                                    </select>
                                                    <?php
                                                }
                                                
                                                if(isset($_REQUEST['bookingtype'])){
                                                               if(($_REQUEST['bookingtype'] == 1)){
                                                               $selrec = 'selected = "selected"';
                                                               $selexe = '';
                                                               }else{
                                                               $selrec = '';
                                                               $selexe = 'selected = "selected"';
                                                               }
                                                }else{
                                                    $selrec = 'selected = "selected"';
                                                               $selexe = '';
                                                }
                                                
                                                ?>
                                            </td>
                                            
                                            <td><label>Booking</label></td>
                                            <td>
                                            <select class="ddlS" name="bookingtype" id="bookingtype" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;'>';
                                                <option value="1" <?php echo $selrec;?>>Received</option>
                                                <option value="2" <?php echo $selexe;?>>Execution</option>
                                            </select>
                                                    
                                            </td>
                                            
					    <td style="padding-left: 10px;">
                                                <input type="image" value="Submit" src="<?php echo corWebRoot; ?>/images/submit-a.png" name="frm1Btn" />
					    </td>
                                            
					</tr>
					<tr>
					    <td>&nbsp;</td>
					</tr>
					</tbody>
				    </table>
				    </form>    
				</td>
				
			    </tr>
			</table>
		</div>		
		<div class="clr"></div>
	    </div>
            
            <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
                                <tbody id="searchData">			
                                    <tr>
                                        <td align="center" width="5%" class="search_table">Date</td>
                                        <td align="center" width="10%" class="search_table">Bookings</td>
                                        <td align="center" width="10%" class="search_table">Car days sold</td>
                                        <td align="center" width="10%" class="search_table">Hourly</td>
                                        <td align="center" width="10%" class="search_table">Daily</td>
                                        <td align="center" width="10%" class="search_table">Weekly</td>
                                        <td align="center" width="10%" class="search_table">Monthly</td>
                                        
                                    </tr>
                                    <?php 
                                    $list=array();
                                    $result = strtotime("{$yr}-{$mnt}-01");
                                    $result = strtotime('-1 second', strtotime('+1 month', $result));

                                    $end_date = date('d', $result);
                                    for($d=01; $d<=$end_date; $d++)
                                    {
                                        $time=mktime(12, 0, 0, $mnt, $d, $yr);          
                                        if (date('m', $time)==$mnt){       
                                            $list=date('d-m-Y,D', $time);
                                        }else{
                                            $list='';
                                        }
                                        //booking Receive start
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                          $sqlrec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." ";  
                                        }else{
                                        $sqlrec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." ";
                                        }
                                        $rowsrec = $db->query("query", $sqlrec);
                                        $ttl_booking_rec = $rowsrec[0]['ttl'];
                                        
                                        
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                        $sqlcardaysrec = "select sum(duration) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." ";    
                                        }else{
                                        $sqlcardaysrec = "select sum(duration) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." ";
                                        }$rowsCarrec = $db->query("query", $sqlcardaysrec);
                                        $car_days_sold_rec = $rowsCarrec[0]['ttl'];
                                        
                                        /*For hour*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                           $sqlHoursrec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." AND pkg_type = 'Hourly'"; 
                                        }else{
                                            $sqlHoursrec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." AND pkg_type = 'Hourly'";
                                        }
                                        
                                        $rowsHourrec = $db->query("query", $sqlHoursrec);
                                        $hours_rec = $rowsHourrec[0]['ttl'];
                                        /*For days*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqldaysRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." AND pkg_type = 'Daily'";
                                        }else{
                                            $sqldaysRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." AND pkg_type = 'Daily'";
                                        }
                                        $rowDaysRec = $db->query("query", $sqldaysRec);
                                        $days_rec = $rowDaysRec[0]['ttl'];
                                        /*For Week*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlWeeksRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." AND pkg_type = 'Weekly'";
                                        }else{
                                            $sqlWeeksRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." AND pkg_type = 'Weekly'";
                                        }
                                        $rowWeekRec = $db->query("query", $sqlWeeksRec);
                                        $weeks_rec = $rowWeekRec[0]['ttl'];
                                        /*For Month*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlmonthRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." AND pkg_type = 'Monthly'";
                                        }else{
                                            $sqlmonthRec = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(entry_date) = ".$yr." and month(entry_date) = ".$mnt." and day(entry_date) = ".$d." and origin_id = ".$orgn." AND pkg_type = 'Monthly'";
                                        }
                                        
                                        $rowsMonthRec = $db->query("query", $sqlmonthRec);
                                        $month_rec = $rowsMonthRec[0]['ttl'];
                                        
                                        //booking Receive end
                                        
                                        //booking Execute start
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sql = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$d." ";
                                        }else{
                                            $sql = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) = ".$d." and origin_id = ".$orgn." ";
                                        }
                                        
                                        $rows = $db->query("query", $sql);
                                         $ttl_booking = $rows[0]['ttl'];
                                         
                                         
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlcardays = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) ";
                                        }else{
                                            $sqlcardays = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) and origin_id = ".$orgn." ";
                                        }
                                        
                                        $rowsCar = $db->query("query", $sqlcardays);
                                        $car_days_sold = $rowsCar[0]['ttl'];
                                        
                                        /*For hour*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlHours = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) AND pkg_type = 'Hourly'";
                                        }else{
                                            $sqlHours = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) and origin_id = ".$orgn." AND pkg_type = 'Hourly'";
                                        }
                                        
                                        $rowsHour = $db->query("query", $sqlHours);
                                        $hours = $rowsHour[0]['ttl'];
                                        /*For days*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqldays = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) AND pkg_type = 'Daily'";
                                        }else{
                                            $sqldays = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) and origin_id = ".$orgn." AND pkg_type = 'Daily'";
                                        }
                                        
                                        $rowDays = $db->query("query", $sqldays);
                                        $days = $rowDays[0]['ttl'];
                                        /*For Week*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlWeeks = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) AND pkg_type = 'Weekly'";
                                        }else{
                                            $sqlWeeks = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) and origin_id = ".$orgn." AND pkg_type = 'Weekly'";
                                        }
                                        
                                        $rowWeek = $db->query("query", $sqlWeeks);
                                        $weeks = $rowWeek[0]['ttl'];
                                        /*For Month*/
                                        if(isset($_REQUEST['ddlOrigin']) && $_REQUEST['ddlOrigin']=='0'){
                                            $sqlmonth = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) AND pkg_type = 'Monthly'";
                                        }else{
                                            $sqlmonth = "select count(uid) as ttl from cor_booking_new where booking_id IS NOT NULL AND (tour_type='selfdrive' || tour_type='Self Drive') AND ((year(pickup_date) = ".$yr." and month(pickup_date) = ".$mnt." and day(pickup_date) <= ".$d.") && (year(drop_date) = ".$yr." and month(drop_date) = ".$mnt." and day(drop_date) >= ".$d.")) and origin_id = ".$orgn." AND pkg_type = 'Monthly'";
                                        }
                                        
                                        $rowsMonth = $db->query("query", $sqlmonth);
                                        $month = $rowsMonth[0]['ttl'];
                                        //booking Execute end
                                        //print_r($_REQUEST['bookingtype']);die;
                                        if(isset($_REQUEST['bookingtype']) && $_REQUEST['bookingtype']=='2'){
                                        ?>
                                    <tr>
                                            <td style="text-align: center;padding:5px !important;" name = "seqno"><?php echo $list?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $ttl_booking; ?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $car_days_sold; ?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $hours; ?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $days; ?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $weeks; ?></td>
                                            <td style="text-align: center;padding:5px !important;"><?php echo $month; ?></td>
                                                
                                    </tr>
                                    <?php
                                    }else{?>
                                    <tr>
                                                <td style="text-align: center;padding:5px !important;" name = "seqno"><?php echo $list?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $ttl_booking_rec; ?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $car_days_sold_rec; ?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $hours_rec; ?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $days_rec; ?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $weeks_rec; ?></td>
                                                <td style="text-align: center;padding:5px !important;"><?php echo $month_rec; ?></td>
                                                
                                    </tr>
                                    <?php }
                                        }
                                    ?>
                                </tbody>
                            </table>    	
	</div>
	<?php
	include_once("./includes/footer.php");
	?>
    </body>
</html>
<div id="container" style="min-width: 310px; height: 550px; margin: 0 auto"></div>
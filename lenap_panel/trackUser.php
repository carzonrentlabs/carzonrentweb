<?php
include_once("./includes/check-user.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Carzonrent Admin</title>
        <?php
        include_once("./includes/cache-func.php");
        include_once("./includes/header-css.php");
        include_once("./includes/header-js.php");
        include_once("./includes/config.php");
        include_once("./classes/bb-mysql.class.php");
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/lenap_panel/css/style.css?v=<?php echo mktime(); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css" />
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/ajax.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/searchajax.js" type="text/javascript"></script>
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/admin.js" type="text/javascript"></script>	
        <script src="<?php echo corWebRoot; ?>/lenap_panel/js/new-admin.js" type="text/javascript"></script>	  
        <script>
            $(function () {
                var dateToday = new Date();
                dateToday.setFullYear(<?php echo date('Y') - 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                var dateNextDay = new Date();
                dateNextDay.setFullYear(<?php echo date('Y') + 1; ?>, <?php echo (date('m') - 1); ?>, <?php echo date('d'); ?>);
                $(".datepicker").datepicker({minDate: dateToday, maxDate: dateNextDay});
            });
        </script>     
    </head>
    <body>
        <?php include_once("./includes/header.php"); ?>
        <div id="middle">	
            <div class="yellwborder">
                <div style="padding:7px 0px 7px 10px; width: 98% !important;" class="magindiv">
                    <h2 class="heading2"><a href="dashboard.php" style="text-decoration:none;color:#222">Dashboard &raquo;</a> Track User</h2>
                </div>
            </div>
            <div class="main" style="width:100%;">
                <br><br>
                        <div style="margin:0 auto;width:99%;">
                            <form id="frmSearch" name="frmSearch" action="trackUser.php" method="get">
                                <table width="65%" cellspacing="1" cellpadding="5" border="0" align="left">
                                    <tbody>
                                        <tr>
                                            <?php
                                            $db = new MySqlConnection(CONNSTRING);
                                            $db->open();

                                            $sDate = "";
                                            if (isset($_GET["startdate"]) && $_GET["startdate"] != "") {
                                                $sDate = date_create(str_ireplace(",", "", $_GET["startdate"]));
                                            } else {
                                                $maxDate = $db->query("stored procedure", "cor_getMaxEntryDate()");
                                                //print_r($maxDate);
                                                $sDate = date_create($maxDate[0]["max_date"]);
                                            }

                                            $eDate = "";
                                            if (isset($_GET["enddate"]) && $_GET["enddate"] != "") {
                                                $eDate = date_create(str_ireplace(",", "", $_GET["enddate"]));
                                            } else {
                                                $eDate = $sDate;
                                            }
                                            
                                            ?>
                                            <td><label>Start Date</label></td>
                                            <td>
                                                <span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="startdate" value="<?php echo $sDate->format('d M, Y'); ?>" id="inputField1" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            <td><label>End Date </label>
                                            </td>
                                            <td><span class="datepick">
                                                    <input type="text" size="12" autocomplete="off" name="enddate" value="<?php echo $eDate->format('d M, Y'); ?>" id="inputField2" class="datepicker" onkeypress="javascript: return false;" />						    
                                                </span>
                                            </td>
                                            
                                            <td>
                                                <input type="image" src="<?php echo corWebRoot; ?>/images/submit-a.png" onclick="javascript: _searchDefiner();" />
                                            </td>
<!--                                            <td valign="middle">					
                                                <a href="download-campaign-summary.php?sdate=<?php echo $sDate->format('Y-m-d'); ?>&edate=<?php echo $eDate->format('Y-m-d'); ?>&campaign=<?php echo $_REQUEST['campaign']?>">Download</a>
                                            </td>-->
                                            
                                            
                                        </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </form>
                            <table width="100%" cellspacing="1" cellpadding="5" border="0" align="left" style="margin:30px 0 0 0px;color:#323232;">
                                <tbody id="searchData">			
                                    <tr>
                                        <td align="center" width="5%" class="search_table">SrNo.</td>
                                        <td align="center" width="10%" class="search_table">City</td>
                                        <td align="center" width="10%" class="search_table">Search List</td>
                                        <td align="center" width="10%" class="search_table">Contact Detail</td>
                                        <td align="center" width="10%" class="search_table">Payment</td>
                                        <td align="center" width="10%" class="search_table">Confirm Booking</td>
                                    </tr>			    
                                    <?php
                                    $db = new MySqlConnection(CONNSTRING);
                                    $db->open();
                                    
                                    $sql = "SELECT origin_name, user_track FROM customer_search WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'  AND (tour_type = 'Selfdrive' || tour_type = 'Self Drive') GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
                                    $r = $db->query("query", $sql);
                                     
                                   //echo "<pre>"; print_r($r);
                                    if (!array_key_exists("response", $r)) {
                                        for ($i = 0; $i < count($r); $i++) {
                                            $style = "";
                                            if ($i % 2 == 0)
                                                $style = "background-color:#f1f1f1;";
                                            else
                                                $style = "background-color:#dddada;";
                                            $ori = $r[$i]["origin_name"];
                                            $search = "SELECT COUNT(unique_id) as ttlcnt FROM customer_search WHERE user_track='searchlist' and entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'  AND (tour_type = 'Selfdrive' || tour_type = 'Self Drive') AND origin_name = '".$ori."' AND (website='COR' || website='myles') GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
											$searchr = $db->query("query", $search);
                                            if (!array_key_exists("response", $searchr)) {
                                                $searchList = $searchr[0]["ttlcnt"];
                                            }
                                            
                                            $contact =  "SELECT count(unique_id) as ttlcnt FROM customer_search WHERE user_track='contact-information' and entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'  AND (tour_type = 'Selfdrive' || tour_type = 'Self Drive') AND origin_name = '".$ori."' AND (website='COR' || website='myles') GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
                                            $contactr = $db->query("query", $contact);
                                            if (!array_key_exists("response", $contactr)) {
                                                $contactDetail = $contactr[0]["ttlcnt"];
                                            }
                                            
                                            $pay =  "SELECT count(unique_id) as ttlcnt FROM customer_search WHERE user_track='payment' and entry_date BETWEEN '" . $sDate->format("Y-m-d") . "' AND '" . $eDate->format("Y-m-d") . "'  AND (tour_type = 'Selfdrive' || tour_type = 'Self Drive') AND origin_name = '".$ori."' AND (website='COR' || website='myles') GROUP BY origin_name ORDER BY COUNT(unique_id) DESC";
                                            $payr = $db->query("query", $pay);
                                            if (!array_key_exists("response", $payr)) {
                                                $payBook = $payr[0]["ttlcnt"];
                                            }
                                            
											
											$confirm = "SELECT count(uid) as TotalBooking FROM cor_booking_new WHERE entry_date BETWEEN '" . $sDate->format("Y-m-d") . " 00:00:00' AND '" . $eDate->format("Y-m-d") . " 23:59:59' AND (tour_type = 'Selfdrive' || tour_type = 'Self Drive') AND origin_name = '" . $ori . "' AND booking_id IS NOT NULL AND source='COR'";
											$confirmr = $db->query("query", $confirm);
                                            if (!array_key_exists("response", $confirmr)) {
                                                $confirmBooking = $confirmr[0]["TotalBooking"];
                                            }
                                            
                                            
                                            
                                            
                                            
                                            ?>
                                            <tr style="<?php echo $style ?>">
                                            <td style="padding:5px !important;" name = "seqno"><?php echo ($i + 1); ?></td>
                                            <td style="padding:5px !important;"><?php echo $r[$i]["origin_name"]; ?></td>
                                            <td style="padding:5px !important;"><?php echo $searchList; ?></td>
                                            <td style="padding:5px !important;"><?php echo $contactDetail; ?></td>
                                            <td style="padding:5px !important;"><?php echo $payBook; ?></td>
                                            <td style="padding:5px !important;"><?php echo $confirmBooking; ?></td>    
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else {
                                        ?>
                                        <tr style="background-color:#f1f1f1;">
                                            <td align="center" colspan="10">No data found.</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div style="clear:both"></div>

                            
                        </div>		
                        <div class="clr"></div>
                        </div>
                        <p>&nbsp;</p>	    	
                        </div>
                        <?php
                        include_once("./includes/footer.php");
                        ?>
                        </body>
                        </html>
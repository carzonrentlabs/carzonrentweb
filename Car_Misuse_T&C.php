<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transportation Service Provider, Car Rental Solutions - Carzonrent Pvt. Ltd</title>
<meta name="description" content="Carzonrent (India) Pvt. Ltd. (CIPL) is India's # 1 transportation service provider offering car rental solutions through its fleet of 5000 cars across the country." />
<meta name="keywords" content="transportation service provider, car rental solutions" />
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
</head>
<body>
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder2" style="margin-top: 20px;">
		<div class="w1130">
			<h2>Myles - Car Misuse Terms & Conditions</h2>
		</div>
	</div>	
<div style="margin:0 auto;width:1130px;">
<div class="car_misuse">
<strong>Following conditions will be considered as Myles car misuse.  Customers will be fined if found involved in any of the following:</strong>
<ul>
<li><strong>Over speeding:</strong> Customers are allowed to drive the car up to a maximum speed of 100 km/hr (63 miles/hr), beyond which the car will be considered as over speeding. A penalty of Rs. 200 will be charged on the first instance, followed by Rs. 500 for the 2nd and Rs. 1000 on the 3rd instance. Thereafter, booking will stand cancelled and the customer must return the car.</li>
<li><strong>Traffic violation:</strong> Users are liable to pay for any traffic violation tickets received during the Rental Period. Company can charge traffic violation from the User’s credit/debit card on actual if the violations tickets are sent directly to the Company by the traffic department.</li>
<li><strong>Car spare part changed:</strong> Users should not charge or remove any car spare parts. In case of emergency, the User should inform Company and act as per advice. Users will be charged a penalty of Rs. 5000 over and above the cost of spare part.</li>
<li><strong>Tyre misuse:</strong> In case of any tyre damages resulting from driving in bad terrain and continued driving in case of tyre puncture, User will be charged for the cost of tyre on actual .</li>
<li><strong>Running vehicle in damaged conditions:</strong>  Users are not advised to drive the Vehicle if it gets damaged in an accident. Users are advised to inform the Company immediately. In such case, Users will be charged for the cost of spare parts on actual as per this Agreement.</li>
<li><strong>Engine damage:</strong>  In case of damage, either latent or patent to the engine of the car, and if same is not covered or partly covered under the existing insurance policy of the vehicle, the user shall be solely responsible towards the reimbursement, either the full or the residue, of actual cost of repair/replacement of the engine. The user shall be bound to immediately inform the company about the damage occurred and under no circumstance shall continue to driving the said car in the damaged condition. Any other consequential cost(s) shall also be borne by the user. Further, if the user attempts to conceal such damage from the company, then the company shall reserve a right to initiate appropriate legal action(s) against the said user, in accordance with the law.</li>
<li><strong>Unauthorized activity in the car:</strong> Users are not allowed to carry arms ammunitions and banned drugs. In addition, use of Vehicle for commercial activity such as product sell and promotion, and carry goods is strictly prohibited. In such cases, Users will be charged a penalty of Rs. 5000.</li>
<li><strong>External branding:</strong> Any form of external branding on the car is prohibited. Customers are not allowed to paste or paint any external brand promotion on the car. Customers will have to pay a penalty of Rs. 5000.</li>
<li><strong>Tampering with devices:</strong> Users are not allowed to tamper with the odometer, GPS device and in car technology devices. Users will have to pay a penalty of Rs. 1000 and over in additional to the actual cost of the device.</li>
<li><strong>Deliberately driving the car in water:</strong>Users will be charged for the actual cost of repair and spare parts in terms of this Agreement.</li>
<li><strong>Producing fake and tampered personal documents:</strong> Customers will be charged a penalty of Rs. 1000 if found guilty.</li>
<li><strong>Diversion:</strong> Users are not allowed to drive the car into unauthorized or government banned areas. Users are advised to inform the Company in case they change the course of their trip. All Myles Vehicles are geo-fenced and users have to pay a penalty Rs. 10,000 if the Vehicle trespasses into banned areas, naxal hit areas, international border of republic of India and extreme end of ladakh.</li>
<li><strong>Tobacco Use:</strong>Users are not allowed to consume, chew or use any type of Tobacco, Cigarettes, Drugs or any type of Narcotics and Psychotropic Substances violating which User shall be liable to penalty of Rs. 1000(Rupees One Thousand Only) and further it shall entitle the Company to take back the Vehicle and terminate this Agreement. </li>



</ul>
</div>
</div>	
</div>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

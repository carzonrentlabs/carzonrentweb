<?php
	error_reporting(0);
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Corporate Social Responsibility</title>
<meta name="description" content="Carzonrent(India) Pvt. Ltd. � Offer airport transfer, chauffeur & self-drive, limousine, operating lease, driving holidays and corporate travels services with quality services at affordable price." />
<meta name="keywords" content="Carzonrent services, airport transfer service, chauffeur & self-drive service, limousine services, operating lease services, Driving holidays service, corporate travels service." /> 
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php"); ?>
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
<link rel="stylesheet" type="text/css" href="css/custom.css" />
</head>
<body>
<?php 
	include_once("./includes/header.php"); 
?>
<!--Banner Start Here-->
<!--Middle Start Here-->
<div id="middle" >
	<div class="yellwborder" style="margin-top: 20px">
		<div class="yellodivtxt">
			<h2>Corporate Social Responsibility</h2>
		</div>
	</div>

	<div class="divcenter mainW960">
	<div id="divairp" class="tabbbox" style="display:block">
		<div class="divtabtxt" style='width:100%;'>
			<p>Overview</p>
			<span class="txtcar" style="margin-bottom:0px;">As a corporate entity, our Company "Carzonrent India Private Limited" recognizes that its business activities have wide impact on the societies in which it operates, and therefore an effective practice is required giving due consideration to the interests of its stakeholders including shareholders, customers, employees, suppliers, business partners, local communities and other organizations. The vision of the Company is "to actively contribute to the social and economic development of the communities in which it operates".</span>
			
			
			<p>CSR Policy & Initiate of the Company</p>
			<span class="txtcar">In order to fulfill the above objective, Corporate Social Responsibility (CSR) Policy of the Company has been framed under which the Company shall undertake the CSR activities through the "COR Welfare Trust" established in the year 2009 by the Promoters of the Company to initiate necessary steps/ activities for advancement and betterment of the society. </span>
			<span class="txtcar">The Trust is an irrevocable public charitable trust made for the benefit of all persons belonging to different communities irrespective of their caste, creed or religion. The trust is constituted with the object of promoting education and learning in different fields and branches through or by establishment of various schools, colleges, institutions, cultural and social service centers, industrial training centers, etc., and/ or by way of running, maintaining or assisting such education imparting institutes, medical institutions, nursing homes or clinics established to support and assist the needy and poor people in the society. The said objects are more specifically stipulated in the aforesaid Trust Deed. </span>
			
			<span class="txtcar">To attain its CSR objectives in a professional and integrated manner, activities shall be carried out by the Company/ COR Welfare Trust, in accordance with the provisions of Section 135 of the Companies Act, 2013 and Schedule VII of the Act and the Companies (Corporate Social Responsibility Policy) Rules, 2014 or as may be amended from time to time.</span>
			
			
			<span class="txtcar">Pursuant to the provisions of sub section (5) of section 135 of the Companies Act, 2013, the Company shall spend in every financial year at least 2% of the average net profits of the Company made during the three immediately preceding financial years in the activities mentioned in the CSR Policy and/or Trust Deed or as may be amended from time to time. Under the terms of CSR Policy, if the amount allocated and transferred for the purpose of compliance for CSR activities remains unutilized, it will not lapse and will be carried over to the next year which will accumulate in non-lapsable pool as provided in CSR Policy of the Company. </span>
			
			
			
			<span class="txtcar">Towards achievement of CSR objectives, the Company shall abide by the CSR Policy of the Company framed in accordance with the Companies Act, 2013 and Rules framed thereunder or as may be amended from time to time.</span>
			
<style>
table.csr {
border-collapse: collapse;
}
table.csr, th, td {
border: 1px solid black;
padding:5px;
}
table.nb, table.nb th, table.nb td {
	border:0px;
	padding:3px;
}
ul.csr_ul{
margin:0px 0 10px 10px;
}
ul.csr_ul li{
line-height:17px;
}
.abcd{
margin:10px 0 0px 10px;
}
</style>
			<span class="txtcar">For the financial year 2014-2015, Company spent more 2% of the average net profit. through COR Welfare Trust on poverty and educational support for weaker sections of the society. In future, the Company will adopt and participate in more projects to help and support weaker sections of the society.</span>
			
			
			<span class="txtcar">Through the COR Welfare Trust, employees of the Company have engaged themselves to fulfill the CSR activity for the betterment of the society. Manner in which the amount spent during the financial year 2014-15 is detailed below:</span>
		   <div class="clr"> </div>
		   <table class="csr" width="100%" border="0" style="border:1px solid #000;">
		   <tr>
		   <th height="20" align="center" valign="middle"> (1) </th>
		   <th align="center" valign="middle"> (2) </th>
		   <th align="center" valign="middle"> (3) </th>
		   <th align="center" valign="middle"> (4) </th>
		   <th align="center" valign="middle"> (5) </th>
		   <th align="center" valign="middle"> (6) </th>
		   <th align="center" valign="middle"> (7) </th>
		   <th align="center" valign="middle"> (8) </th>
		   </tr>
		   <tr>
		   <td align="left" valign="top"> S. No. </td>
		   <td align="left" valign="top"> CSR project or activity identified </td>
		   <td align="left" valign="top"> Sector in which the Project is covered </td>
		   <td align="left" valign="top"> 
		   Projects or programs <br>
			1) Local area or other <br>
			2) Specify<br>
			the State and district where projects or programs was undertaken
		</td>
		 <td align="left" valign="top"> 
		   Amount outlay (budget) project or programs wise
		</td>
		<td align="left" valign="top"> 
		   Amount spent on the projects or programs Sub-heads: <br>
l) Direct expenditure on projects or programs <br>
2) Overheads
		</td>
		<td align="left" valign="top"> 
Cumulative expenditure upto to the reporting period
		</td>
				<td align="left" valign="top"> 
Amount spent Direct or through implementing agency
		</td>
		   </tr>
		   
		   
		   
		   
		   <tr>
		   <td align="left" valign="top"> 1 </td>
		   <td align="left" valign="top"> Free distribution of book to poor children</td>
		   <td align="left" valign="top"> Educational Support</td>
		   <td align="left" valign="top"> 
			1) Local area <br>
			2) New Delhi<br>
		</td>
		 <td align="left" valign="top"> 
		   Rs. 1,90,000/-
		</td>
		<td align="left" valign="top"> 
		   Rs. 1,80,000/-
		</td>
		<td align="left" valign="top"> 
Rs. 1,80,000/-
		</td>
				<td align="left" valign="top"> 
Rs. 1,80,000/-
		</td>
		   </tr>
		   
		   <tr>
		   <td align="left" valign="top"> 2 </td>
		   <td align="left" valign="top"> Free distribution of blankets to poor people. </td>
		   <td align="left" valign="top"> Poverty </td>
		   <td align="left" valign="top"> 
		   
			 3.	Local area <br>
			 4.	New Delhi
		</td>
		 <td align="left" valign="top"> 
		   Rs. 11,65,170/-
		</td>
		<td align="left" valign="top"> 
		  Rs. 11,80,000/-
		</td>
		<td align="left" valign="top"> 
Rs. 11,80,000/-
		</td>
				<td align="left" valign="top"> 
Rs. 11,80,000/-
		</td>
		   </tr>
		   
		   <tr>
		   <td align="left" valign="top"> S. No. </td>
		   <td align="left" valign="top"> Total</td>
		   <td align="left" valign="top"> </td>
		   <td align="left" valign="top"> 
		   
		</td>
		 <td align="left" valign="top"> 
		   Rs. 13,55,170/-
		</td>
		<td align="left" valign="top"> 
		   Rs. 13,60,000/-
		</td>
		<td align="left" valign="top"> 
Rs. 13,60,000/-
		</td>
				<td align="left" valign="top"> 
Rs. 13,60,000/-
		</td>
		   </tr>
		   
		   
		   
		   
		   </table>
		   <div class="clr"> </div>
			
			<p>Objectives to be covered under CSR Policy:</p>
			<span class="txtcar">Our Company is vigilant in its enforcement towards corporate principles and is committed towards sustainable development and inclusive growth. The Company constantly strives to ensure strong corporate culture which emphasizes on integrating CSR values with business objective. It also pursues initiative related to quality management, environment preservation and social awareness. Arising from this the focus areas that have emerged are Education, Health care, Sustainable livelihood, Infrastructure development, and espousing social causes. The vision of the Company is "to actively contribute to the social and economic development of the communities in which it operates". In so doing build a better, sustainable way of life for the weaker sections of society and raise the country's human development index.</span>
			
			<span class="txtcar">To attain its CSR objectives in a professional and integrated manner, followings are the activities which may be carried out by the Company, in accordance with the provisions of Section 135 of the Companies Act, 2013, Schedule VII of the Companies Act, 2013 read with  the Companies (Corporate Social Responsibility Policy) Rules, 2014. </span>
			<div class="clr"> </div>
			<ul class="csr_ul">
			<li>Eradicating hunger, poverty and malnutrition, promoting preventive health care and sanitation and making available safe drinking water;</li>
			<li>Promoting education, including special education and employment enhancing vocational skills especially among children, women, elderly and the differently abled and livelihood enhancement projects;</li>
			<li>Promoting gender equality, empowering women, setting up homes and hostels for women and orphans; setting up old age homes, day care centers and such other facility for senior citizens and measures for reducing inequalities faced by socially and economically backward groups;</li>
			<li>Ensuring environmental sustainability ecological balance, protection of flora and fauna, animal welfare, agroforestry, conservation natural resources and maintaining quality of soil, air and water;</li>
			<li>Protection of national heritage, art and culture including restoration of buildings and sites of historical importance and works of art, setting up public libraries; promotions and development of traditional arts and handicrafts; sports and Olympics sports</li>
			
			<li>Measures for the benefit of armed forces veterans, war widows and their dependents;</li>
			
			
			<li>Training to promote rural sports, nationally recognized sports, Paralympics sports and Olympics Sports;</li>
			<li>Contribution to the Prime Minister's National Relief Fund or any other fund set up by the Central Government for socio-economic development and relief and warfare of the scheduled castes, the schedules tribes, other backward classes, minorities and women;</li>
			
			<li>Contribution to funds provided to technology incubators located within academic institutions which are approved by the Central Government;</li>
			<li>Rural development projects;</li>
			
			<li>Any other project, welfare activity in line with the aims and objectives specified above and within the ambit of the provisions of Section 135 of the Companies Act, 2013 read with (Corporate Social Responsibility Policy) Rules, 2014, and duly approved by the Board of Directors of the Company;</li>
			
			<li>As a part of CSR strategy, in line with the aims and objectives specified above, the scope of CSR activities would cover the following areas:</li>
			</ul>
			<div class="abcd">
			a.	Education; <br>
			b.	Health; <br>
			c.	Drinking Water/ Sanitation; <br>
			d.	Environment; <br>
			e.	Community Development and Social Empowerment; <br>
			f.	Generation of employment opportunities and livelihood; <br>
			g.	Any other activity as may be identified by Board of Directors.
			</div>

			
			
			
<p>Responsibility of CSR Committee:</p>
			
			<div class="clr"> </div>
			<ul class="csr_ul">
			<li>Formulation, development, amendment in policy framework and broad guidelines for selection of the projects, planning, budget execution and monitoring;</li>
			<li>Selection of a project in accordance with policy framework and guidelines, prepare a project report along with estimated cost and send for the approval of Board;</li>
			<li>Formulation of strategies for efficient implementation along with other stakeholders like civil society and implement the project as per guidelines;</li>
			<li>Supervision, coordination and implementation of CSR activities/ projects;</li>
			<li>Compilation of information and preparation of regular/ annual reports etc.;</li>
			<li>To coordinate with various other departments for exchange of information for promotion of CSR activities and ensure harmony in activities by different agencies;</li>
			<li>Submit quarterly and annual statements of physical and financial progress to the Board;</li>
			<li>To consider and approve the projects for CSR activities and submit annual budgetary allocation among various projects;</li>
			<li>To arrange workshops, training etc. to sensitize the staff for better implementation of the policy;</li>
			<li>To keep up-dated CSR policy including the changes/ clarifications suggested/issued by the Ministry of Corporate Affairs and other government agencies;
         </li>
        </ul>


<p>Implementation Procedure:</p>
			
			<div class="clr"> </div>
			<ul class="csr_ul">
			<li>Discussions and interactions with various private/ Government bodies/ Government officials may be held to identify the areas for undertaking CSR activities;</li>
			<li>While identifying the CSR activities, emphasis shall be on the areas related to the business of the Company;</li>
			<li>A survey may be carried out to find out the needs and requirements of community before planning a project;</li>
			<li>The target beneficiaries, the local authorities, institutions etc. involved in  similar activities if need be, may be consulted in the process of planning and implementation of CSR program;</li>
			<li>Based on the need analysis survey, prepare a detailed program report reflecting content, objectives, major milestones, time frame for implementation, budget thereof and implementing agency. The investment in CSR activity would be project based and for every project, time-frame and periodic targets would be finalized at the outset along with the modalities for the concurrent and final evaluation.</li>
			
			
			<li>Ensure that that the CSR activities are in accordance with the approved policy. Activities related to sustainable development will form a significant element;</li>
			<li>May assign CSR projects to NGOs/ specialized agencies under an MOU/ Agreement reflecting the mutual terms and conditions for the projected activities. The Committees should make all efforts to verify the reliability and past track record of the engaged agency and only agencies of good repute may be engaged.   </li>
</ul>


<p>Monitoring and Evaluation: </p>
			
			<div class="clr"> </div>
			<ul class="csr_ul">
			<li>The impact of the CSR activities undertaken should be quantified to the extent possible with reference to baseline data, to be created before the start of any project. Therefore, Base-line surveys would be an integral part of CSR program so that progress can be measured. Photographic record may be maintained wherever possible;</li>
			<li>For proper and periodic monitoring of CSR activities, if considered necessary, the programs undertaken under CSR policy may be evaluated through a suitable independent external agency and the evaluation should be both concurrent and final.</li>
			
</ul>
			
<div class="clr"> </div>

<table class="nb" width="100%" border="0" style="border:none;">
<tr>
<td align="left" valign="top">Sd/- </td>
<td align="center" valign="top"> Sd/- </td>
</tr>
<tr>
<td align="left" valign="top">(Managing Director)</td>
<td align="center" valign="top">(Chairman CSR Committee)</td>
</tr>
</table>

			
  </div>
			
			
		</div>
	</div><!-- end of divairp-->
	</div><!-- end of divcenter -->
</div>
<div id="backgroundPopup"></div>
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()
</script>
<?php include_once("./includes/footer.php"); ?>
</body>
</html>

<?php
    if(isset($_GET["txtMobile"]) && isset($_GET["txtBookingID"])){
        include_once('./classes/cor.ws.class.php');
        include_once('./classes/cor.xmlparser.class.php');
        
        $url = "";
        if( ! $_SESSION)
        {
          session_start();
        } 
        $dataToSave= array();
        $dataToSave["PhoneNo"] = $_GET["txtMobile"];
        $dataToSave["BookingID"] = $_GET["txtBookingID"];
        
        
        $cor = new COR();
        /*if(isset($_GET["tourtype"]) && $_GET["tourtype"]=='1'){
            $res = $cor->_CORSelfDriveGetInvoice($dataToSave);
        }else{
            $res = $cor->_CORGetInvoice($dataToSave);
        }*/
		$res = $cor->_CORGetInvoice($dataToSave);
        unset($cor);
        
        $myXML = new CORXMLList();
        $arrInvoice = $myXML->xml2ary($res->{'ValidateInvoiceResult'}->{'any'});
        
        if($arrInvoice != "")
        {
//            $myXML = new CORXMLList();
//            $arrInvoice = $myXML->xml2ary($res->{'ValidateInvoiceResult'}->{'any'});
            if(count($arrInvoice) > 0){
                $url = $arrInvoice[0]["URL"];
            }
            unset($myXML);
        }
        else{
            $url = "./print-booking.php?resp=e";
        }
        echo $url;
    }
?>
<?php
	 error_reporting(1);
     //error_reporting(E_ALL);
     //ini_set("display_errors", 1);
     include_once('../classes/cor.ws.class.php');
     include_once('../classes/cor.xmlparser.class.php');
     include_once("../includes/cache-func.php");
     include_once('../classes/cor.mysql.class.php');
    
?>
<!DOCTYPE HTML>
<html>
     <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <title>EasyCabs Mobile App TC</title>
	   <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/drivetowin/css/style.css">
	   <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/css/default-new.css">
	  
	  <link rel="stylesheet" type="text/css" href="<?php echo corWebRoot; ?>/myles-campaign/style.css">
	  <link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/nivo-slider.css" type="text/css" media="screen" />
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_general_v3.js?v=<?php echo mktime(); ?>"></script>
	  <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/validation.js?v=<?php echo mktime(); ?>"></script>
	  <link rel="stylesheet" type="text/css" href="css/drivetowin.css">
	  <link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'>
	  <script>
	  document.createElement('article');
	  document.createElement('section');
	  document.createElement('aside');
	  document.createElement('hgroup');
	  document.createElement('nav');
	  document.createElement('header'); 
	  document.createElement('footer');
	  document.createElement('figure');
	  document.createElement('figcaption'); 
	  </script>
	 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once("./includes/seo-metas.php"); ?>
<?php include_once("./includes/header-css.php"); ?>
<?php include_once("./includes/header-js.php");
?>
<link href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/style.css?v=<?php echo mktime(); ?>" type="text/css" rel="stylesheet" />
<!-- Slider image start -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/slider/slidesjs.css" type="text/css" media="screen" />
<!-- Slider image end -->
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/style.css?v=<?php echo mktime(); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/mystyle/skin.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/myles-campaign/stylesheets/jquery.tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/myles-campaign/javascripts/jquery.tooltip.js"></script>

	  
	 
	  
	  <!-- Tab end -->
	  <!-- OnClick Slider start -->
	  <link href="css/jcarousel.css" rel="stylesheet" type="text/css">
	  <!-- OnClick Slider end -->
	  <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
	  <script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
	  <style type="text/css">
	  body{
	  background:#fff;
	  }
	  </style>
     </head>
     <body>
	 <?php include_once("../includes/header.php"); ?>
	  <div class="clr"> </div>
	  <br>
	   <div class="main">
	
	
	<link href="css/term_condition.css" rel="stylesheet" type="text/css">
<!-- Term & Condition Start -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/tab.js"></script>
<link rel="stylesheet" type="text/css" href="css/tab.css">
<!-- Term & Condition End -->
<div class="app_banner">
<div class="app">
<div class="term_heading">
mobile app offer terms and conditions:
</div>



<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab1">Easycabs</a>
	</li>
	<li>
		<a href="#tab2">Outstation</a>
	</li>
	<li>
		<a href="#tab3">Myles</a>
	</li>
</ul>

<div id="tab1" class="tab-content active">
<div>
<div class="discount_h">
<img src="<?php echo corWebRoot; ?>/EasyCabsMobileApp/images/100_discount.jpg" alt="Rs. 100 Discount" title="Rs. 100 Discount"/>
</div>
<div class="ptb20_lr40">
<h1>Easycabs offer terms and conditions:</h1>
<ol>
<li>Offer is auto-applied on your 1st booking made through the Mobile application.</li>
<li>Applicable only when you pay through the Mobile application.</li>
<li>Not applicable on Call Centre & Website bookings.</li>
<li>Only one offer is applicable on a booking at a time.</li>
<li>Cabs will be provided subject to availability.</li>
<li>The Company reserves the right to change, suspend, or discontinue the offer at any time for any reason, without any prior notice.</li>
<li>Offer is valid till 31st December 2015.</li>
<li>Offer is applicable in: Delhi, Mumbai, Bangalore and Hyderabad.</li>
</ol>
<div class="clr"> </div>
</div>
</div>
</div>
<div id="tab2" class="tab-content hide">
<div>
<div class="discount_h">
<img src="<?php echo corWebRoot; ?>/EasyCabsMobileApp/images/500_discount.jpg" alt="Rs. 500 Discount" title="Rs. 500 Discount"/>
</div>
<div class="ptb20_lr40">
<h1>Outstation offer terms and conditions:</h1>
<ol>
<li>Applicable on bookings made through Carzonrent App.</li>
<li>Apply the coupon code received on your registered mobile number/e-mail id.</li>
<li>Apply the coupon code before making the reservation.</li>
<li>Applicable for a period of 1 month from the date of coupon generation</li>
<li>Applicable on all car categories and in all cities of operations</li>
<li>Only one offer is applicable on a booking at a time.</li>
<li>Cars will be provided subject to availability.</li>
<li>The Company reserves the right to change, suspend, or discontinue the offer at any time for any reason, without any prior notice.</li>
<li>Offer is valid till 31st December 2015.</li>
<li>Coupon code applicable only once.</li>
</ol>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
</div>
<div id="tab3" class="tab-content hide">
<div>
<div class="discount_h">
<img src="<?php echo corWebRoot; ?>/EasyCabsMobileApp/images/500_discount.jpg" alt="Rs. 500 Discount" title="Rs. 500 Discount"/>
</div>
<div class="ptb20_lr40">
<h1>Myles offer terms and conditions:</h1>
<ol>
<li>Applicable on bookings made through Carzonrent App.</li>
<li>Apply the coupon code received on your registered mobile number/e-mail id.</li>
<li>Apply the coupon code before making the reservation.</li>
<li>Applicable for a period of 1 month from the date of coupon generation.</li>
<li>Applicable on a minimum booking of Rs. 2000.</li>
<li>Applicable on all car categories and in all cities of operations</li>
<li>Only one offer is applicable on a booking at a time.</li>
<li>Cars will be provided subject to availability.</li>
<li>The Company reserves the right to change, suspend, or discontinue the offer at any time for any reason, without any prior notice.</li>
<li>Offer is valid till 31st December 2015.</li>
<li>Coupon code applicable only once.</li>
</ol>
<div class="clr"> </div>
</div>
<div class="clr"> </div>
</div>
</div>




</div>
<div class="clr"> </div>
</div>
        
	

  
   </div>
   </br>
   
</div>
<div class="clr"> </div>
<!--Middle Start Here-->
<?php include_once("middle.php"); ?>


<div class="clr"> </div>
<?php include_once("../includes/footertc.php"); ?>






</body>
</html>
<?php
	include_once('./classes/cor.ws.class.php');
	include_once('./classes/cor.xmlparser.class.php');
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/tabcontent.css" />
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/tabcontent.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_disablepage.js"></script>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/_bb_elmpos.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" ></script>
<?php include_once("./includes/header-js.php"); ?>
</head>
<body>

<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:2000, autoplay_slideshow:false});
});
</script> 
<!--Header Start Here-->
<?php include_once("./includes/header.php"); ?>
<!--Banner Start Here--> 

<!--Middle Start Here-->

<div id="middle">
<div class="main">
<div class="aboutus">
<div class="aboutusdv">
<!--<div class="tbul">
  <ul id="countrytabs" class="shadetabsabout">
    <li><a href="#" rel="country1" class="selected">Registration</a></li>	
  </ul>
</div>-->
<div class="lfcnt">
<div id="country1" class="tabcontent">
  <h1>Registration</h1>
  <h2>Thank You</h2>
<p class="txt14px">&nbsp;</p>
<p>Thank you for registering with Carzonrent. Please check your email account to setup your password and complete the registration.<br />
<div class="apply"><div class="submit"><a href="javascript:void(0)"></a></div></div>
<p class="txt14px">&nbsp;</p>
<div class="clearfix"></div>
</div>
</div>
</div>
<!--<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>-->
</div>
</div>
<!--footer Start Here-->
<div id="footer">
	<div class="main">
    	
        <div class="fbtm" style="width:740px;">
		<?php
			if((isset($_GET["src"]) && $_GET["src"] != "") || (isset($_COOKIE["corsrc"]) && isset($_COOKIE["corsrc"]) != ""))
			{}
			else
			{
		?>
        	<div class="cm" style="width:180px">
            	<h3>Company</h3>
                <ul>
                	<li><a href="<?php echo corWebRoot; ?>/aboutus.php">About</a></li>
			<li><a href="<?php echo corWebRoot; ?>/services.php">Services</a></li>
			<li><a href="http://careers.carzonrent.com/" target="_blank">Career</a></li>
			<li><a href="<?php echo str_ireplace("www", "blog", corWebRoot); ?>/" target="_blank">Blog</a></li>
			
                </ul>
            </div>
            <div class="cm" style="width:180px">
            	<h3>Book a Cab</h3>
                <ul>
                	<li><a href="<?php echo corWebRoot; ?>/#o">Outstation</a></li>
			<li><a href="<?php echo corWebRoot; ?>/#l">Local</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/">EasyCabs</a></li>
                </ul>
            </div>
            <div class="cm" style="width:180px">
            	<h3>EasyCabs</h3>
                <ul>
                	<li><a href="<?php echo ecWebRoot; ?>/advertise-with-us.php" target="_blank">Advertise with Us</a></li>
                        <li><a href="<?php echo ecWebRoot; ?>/fleet.php" target="_blank">Fleet</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/tariff.php" target="_blank">Tariff</a></li>
			<li><a href="<?php echo ecWebRoot; ?>/partners.php" target="_blank">Partners</a></li>
                </ul>
            </div>
	    <!-- <div class="qu" style="width:185px">
            	<h3>Corporate Leasing</h3>
                <ul>
			<li><a href="<?php echo corWebRoot; ?>/cor-lease/leasing.php" target="_blank">Why Leasing</a></li>
			<li><a href="<?php echo corWebRoot; ?>/cor-lease/lease-benefits.php" target="_blank">Benefits</a></li>
			<li><a href="<?php echo corWebRoot; ?>/cor-lease/product-and-services.php" target="_blank">Products &amp; Services</a></li>
                </ul>
            </div>-->
            <div class="qu" style="width:180px">
            	<h3>Quick Links</h3>
                <ul>
			<li><a href="<?php echo corWebRoot; ?>/myaccount.php">My Account</a></li>
			<li><a href="<?php echo corWebRoot; ?>/print-invoice.php">Print Invoice</a></li>
			<li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
			<li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
                </ul>
            </div>
        </div>
	<?php
		}
	?>
	
	
	
	
	
	<div style="clear: both;"></div>
	 <!--<div class="citilist"><a href="<?php echo corWebRoot; ?>/carrental/car-rental-delhi-to-agra/">Car Rental Delhi</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/carrental/car-rental-bangalore-to-coorg/"> Car Rental Bangalore</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Mumbai-4/">Car Rental Mumbai</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Hyderabad-6/"> Car Rental Hyderabad</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Chennai-8/">Car Rental Chennai</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Pune-5/">Car Rental Pune</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Ahmedabad-1/">Car Rental Ahmedabad</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Jaipur-10/">Car Rental Jaipur</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Chandigarh-9/">Car Rental Chandigarh</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Visakhapatnam-49/">Car Rental Visakhapatnam</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Noida-11/">Car Rental Noida</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Gurgaon-3/">Car Rental Gurgaon</a></div>-->
	<div class="citilist"><a href="<?php echo corWebRoot; ?>/carrental/car-rental-delhi-to-agra/">Car Rental Delhi</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/carrental/car-rental-bangalore-to-coorg/"> Car Rental Bangalore</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Mumbai-4/">Car Rental Mumbai</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Hyderabad-6/"> Car Rental Hyderabad</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Chennai-8/">Car Rental Chennai</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Pune-5/">Car Rental Pune</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Ahmedabad-1/">Car Rental Ahmedabad</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Jaipur-10/">Car Rental Jaipur</a>&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><br></span>  <a href="<?php echo corWebRoot; ?>/car-rental-Chandigarh-9/">Car Rental Chandigarh</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Visakhapatnam-49/">Car Rental Visakhapatnam</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Noida-11/">Car Rental Noida</a>&nbsp;|&nbsp;<a href="<?php echo corWebRoot; ?>/car-rental-Gurgaon-3/">Car Rental Gurgaon</a></div>
	<div style="clear: both;"></div>
        <div class="copurit">Copyright &copy; 2014 <b>Carzonrent India Pvt Ltd</b>. All Rights Reserved. <a href="<?php echo corWebRoot; ?>/terms-of-use.php" class="copurita">Terms of Use</a> | <a href="<?php echo corWebRoot; ?>/privacy-policy.php" class="copurita">Privacy Policy</a> | <a href="<?php echo corWebRoot; ?>/faqs.php" class="copurita">FAQs</a> | <a href="http://ecompliance.carzonrent.com" class="copurita" target="_blank">e-Compliance</a>
		<?php
			/*include_once('./classes/cor.mysql.class.php');
			$db = new MySqlConnection(CONNSTRING);
			$db->open();
			
			$recPerSS = 50000;
			$r = $db->query("query", "Select Count(unique_id) as total from cor_searchkeywords");
			$recCount = $r[0]["total"];
			$noOfpage = ceil($recCount/$recPerSS);
			unset($r);
			$db->close();
			if ($noOfpage > 1)
			{*/
		     ?>
				   <!-- <a href="javascript: void(0);" onclick="javascript:if(document.getElementById('srchsm').style.display == 'none'){document.getElementById('srchsm').style.display = 'block';this.innerHTML = '- Search Site Map';} else {document.getElementById('srchsm').style.display = 'none';this.innerHTML = '+ Search Site Map';}">+ Search Site Map</a>
				   <div id="srchsm" style="display:none;"> -->	  
		     <?php
				   //for($i = 1; $i <= $noOfpage; $i++)
				  // {
		     ?>
					  <!-- <a href="<?php //echo corWebRoot; ?>/search-sitemap-<?php //echo $i; ?>/" class="copurita">Search Site Map - <?php //echo $i; ?></a><br /> -->
		     <?php
				   //}
		     ?>
				   <!-- </div>    --> 
		     <?php
			    //}
			    //else {
		     ?>
			    <!-- <a href="<?php //echo corWebRoot; ?>/search-sitemap-1/" class="copurita">Search Site Map</a> -->
		     <?php
			    //}
		     ?><!-- </div> -->

    </div>
</body>
</html>

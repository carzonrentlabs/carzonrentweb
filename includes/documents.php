<link href="<?php echo corWebRoot; ?>/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo corWebRoot; ?>/css/datepicker.css" rel="stylesheet">
<script src="<?php echo corWebRoot; ?>/js/prettify.js"></script>
<script src="<?php echo corWebRoot; ?>/js/bootstrap-jquery.js"></script>
<script src="<?php echo corWebRoot; ?>/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
	$(function(){
		$('#Expiry').datepicker({
			format: 'yyyy-mm-dd'
		});
	});
	$(function(){
		$('#ExpiryDL').datepicker({
			format: 'yyyy-mm-dd'
		});
	});
   $("#load").hide();
   setTimeout(function(){ $("#file_msgs").fadeOut(); }, 5000);
});

function hideExpity(Idval){
	document.getElementById(Idval).style.display = 'none';
	
}

function showExpity(Idval){
	document.getElementById(Idval).style.display = 'block';
	
}

function upload_document(){
	var FileName = $("#fileId").val();
	var IdNo = $("#IDNo").val();
	var IssuCity = $("#IssuCity").val();
	
	var letters = /^[0-9a-zA-Z]+$/;  
	if(!IdNo.match(letters))   
	{  
		alert('Please input alphanumeric characters for ID Number');
		document.getElementById("IDNo").focus();
		return false;  
	}
	
	if(IdNo.length < 8){
		alert('Please input valid ID Number');
		document.getElementById("IDNo").focus();
		return false; 
	}  
	
	
	if(IssuCity.length < 3)  
	{  
		alert('Please input Issue City'); 
		document.getElementById("IssuCity").focus();
		return false;   
	}  
	

	if(FileName == ''){
		alert('Please select a file to upload.');
		document.getElementById("fileId").focus();
		return false;
	}else{		
		$("#load").show();
	}
}

function upload_document_dl(){
	var FileName = $("#fileId2").val();
	var IdNo = $("#IDNoDL").val();
	var IssuCity = $("#IssuCityDL").val();
	var letters = /^[0-9a-zA-Z]+$/;  
	if(!IdNo.match(letters))   
	{  
		alert('Please input alphanumeric characters for ID Number'); 
		document.getElementById("IDNoDL").focus();
		return false;  
	}
	
	if(IdNo.length < 8){
		alert('Please input valid ID Number'); 
		document.getElementById("IDNoDL").focus();
		return false; 
	}  

	
	if(IssuCity.length < 3)  
	{  
		alert('Please input Issue City'); 
		document.getElementById("IssuCityDL").focus();
		return false;   
	}  
	
	
	if(FileName == ''){
		alert('Please select a file to upload.');
		document.getElementById("fileId2").focus();
	return false;
	}else{		
		$("#load").show();
	}	
}	
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="booking_summary_data booking_new">
	<tr>
		<td colspan="4" align="center" valign="middle">
		<h1 class="license" id="file_msgs"><?php if(isset($_SESSION['file_msg'])){
			
			echo '<script>
					$("#load").hide();
				</script>';
			if($_SESSION['type'] == '0'){				
				echo "Passport ".$_SESSION['file_msg'];
			}else if($_SESSION['type'] == '1'){
				echo "Driving License ".$_SESSION['file_msg'];
			}else if($_SESSION['type'] == '4'){
				echo "Voter ID ".$_SESSION['file_msg'];
			}else{
				echo $_SESSION['file_msg'];
			}								
			
			unset($_SESSION['type']);
			unset($_SESSION['file_msg']);
		}  ?></h1>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle">
						 <form id ="form1" action="upload_document.php" method="post" enctype="multipart/form-data" onsubmit="return upload_document();">
								<fieldset>
									
								<h1 class="license">Valid ID</h1>		
								<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td align="left" valign="middle">
										<label for="doc1" class="license">Passport</label>
									</td>
									<td align="left" valign="middle">
										<input type='radio' id="doc1"  value='0' name='docType' checked="checked" onclick="javascript:showExpity('ExpT');"  />
									</td>
									
								</tr>														
								<tr>
									<td align="left" valign="middle">
										<label for="doc2" class="license">Voter ID</label>
									</td>
									<td align="left" valign="middle">
										<input type='radio' id="doc2"  value='4' name='docType'  onclick="javascript:hideExpity('ExpT');" />
									</td>
									
								</tr>
							
								<tr>
									<td align="left" valign="middle">								
										<label for="IDNo" class="license">ID Number</label>
									</td>
									<td align="left" valign="middle">
										<input type='text' id="IDNo"   name='IDNo' maxlength="20" />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" colspan="2">
										<table width="100%" id='ExpT' cellspacing="0" cellpadding="0" border="0">
										<tr style="background-color: #fff;">
											<td align="left" valign="middle">
												<label for="Expiry" class="license">Expiry Date</label>
											</td>
											<td align="left" valign="middle">
												<input type='text' id="Expiry"   name='Expiry' value="<?php echo date('Y-m-d'); ?>" />
											</td>
										</tr>
										</table>
									</td>
								</tr>
								
								<tr>
									<td align="left" valign="middle">
										<label for="IssuCity" class="license">Issue City</label>
									</td>
									<td align="left" valign="middle">
										<input type='text' id="IssuCity"   name='IssuCity' maxlength="20" />
									</td>
								</tr>
								<tr>
								<td colspan="2" align="left" valign="top">
									<input type="file" id="fileId" name ="FileName" />
								</td>
								</tr>
								<tr style="display:none">
									<td colspan="2" align="left" valign="top">
										<input type='hidden'  name="GuestID"  value='<?php echo $userId; ?>'/>
										<input type='hidden'  name="UserID"  value='1'/>
									</td>
								</tr>
								</table>							
								<p>
								<input class="upload" type="submit" name="Upload" value="Upload" />
								</p>
								
								</fieldset>
						  </form>
					
			</td>
			<td colspan="2" align="center" valign="top">

						 <form id ="form2" action="upload_document.php" method="post" enctype="multipart/form-data" onsubmit="return upload_document_dl();">
								<fieldset>
								<h1 class="license">Driving License</h1>
								<table cellspacing="0" cellpadding="0" border="0">														
								<tr>
									<td align="left" valign="middle">
										<label for="doc3" class="license">Driving License</label>
									</td>
									<td align="left" valign="middle">
										<input type='radio' id="doc3"  value='1' name='docType' checked onclick="javascript:showExpity('ExpDl');"/>
									</td>
								</tr>
								<tr>
									<td align="left" valign="middle">
										<label for="doc4" class="license">Aadhaar Card</label>
									</td>
									<td align="left" valign="middle">
										<input type='radio' id="doc4"  value='3' name='docType'  onclick="javascript:hideExpity('ExpDl');" />
									</td>
									
								</tr>
								<tr>
									<td align="left" valign="middle">								
										<label for="IDNo" class="license">ID Number</label>
									</td>
									<td align="left" valign="middle">
										<input type='text' id="IDNoDL"  name='IDNo'  maxlength="20" />
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" colspan="2">
										<table width="100%" id='ExpDl' cellspacing="0" cellpadding="0" border="0">
										<tr style="background-color: #fff;">
											<td align="left" valign="middle">
												<label for="Expiry" class="license">Expiry Date</label>
											</td>
											<td align="left" valign="middle">
												<input type='text' id="ExpiryDL"   name='Expiry' value="<?php echo date('Y-m-d'); ?>" />
											</td>
										</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left" valign="middle">
										<label for="IssuCity" class="license">Issue City</label>
									</td>
									<td align="left" valign="middle">
										<input type='text' id="IssuCityDL"   name='IssuCity' />
									</td>
								</tr>
								<tr>
									<td colspan="2" align="left" valign="top">
										<input type="file" id="fileId2" name ="FileName" />
									</td>
								</tr>
								<tr style="display:none">
								<td colspan="2" align="left" valign="top">
									<input type='hidden'  name="GuestID"  value='<?php echo $userId; ?>'/>
									<input type='hidden'  name="UserID"  value='1'/>
								</td>
								</tr>
								</table>							
								<p>
								<input  class="upload" type="submit" name="Upload" value="Upload" />
								</p>
								
								</fieldset>
						  </form>
					
			</td>
		</tr>
		<tr>
			<td colspan="4" align="left" valign="middle"><div style="font-size:11px">* Max upload file size limit: 1MB<br />* Upload file type : [jpeg,jpg or png]</div></td>
		</tr>
	</table>
<div id="country2" class="tabcontent">
	<div class="myprofile">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="booking_summary_data booking_new">		
		<tr>
			<th align="left" valign="middle" width="11%">Document Type</th>
			<th align="left" valign="middle" width="16%">Status</th>
			<th align="left" valign="middle" width="15%">Action</th>
		</tr>
	<?php
		$arr = array();
		$userId = "";
		if(isset($_COOKIE["cciid"]) || isset($_COOKIE["emailid"])){
			if($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != ""){
				$userId = $_COOKIE["cciid"];
				//echo $userId;
				$cor = new COR();
				$res = $cor->_CORGetUserDocumentDetails(array("ClientCoIndivID"=>$userId));				
				//echo '<pre>'; print_r($res);
				$myXML = new CORXMLList();
				$arr = $myXML->xml2ary($res->{'GetUserDocumentDetailsResult'}->{'any'});
				//echo '<pre>'; print_r($arr);
				unset($cor);
				unset($myXML);
			}
		}
	
		if(count($arr)) {
			
			
			for($i = 0; $i < count($arr); $i++){
				
		if($arr[$i]["File1"] != array())
		{ 		
	?>
		<tr>
			<td align="left" valign="top">
			<?php
				if($arr[$i]["File1"] != array())
				{ 
					echo 'Passport';
				} 
				
			?>
			</td>
			<td align="left" valign="top">
			<div class="status_can">
			<?php
				if($arr[$i]["File1ApproveYN"] != array())
				{ 
						if($arr[$i]["File1ApproveYN"] == false){ 
							echo "Unapproved";
						} else{ 
							echo "Approved"; 
						} 
				} 
				
			?>
			</div>
			</td>
			<td align="left" valign="top">
				<strong>
				<?php
				if($arr[$i]["File1"] != array())
				{ 
					echo '<a class="change_password" href="download.php?file='.$arr[$i]["File1"].'&uId='.$userId.'" target="_blank">View</a>';
				} 
				
				?>
				</strong>
			</td>
		</tr>
	<?php
			} if($arr[$i]["File2"] != array())
		{ 
			
		?>
		
		<tr>
			<td align="left" valign="top">
			<?php
				
				if($arr[$i]["File2"] != array())
				{ 
					echo 'Driving License';
				}
			?>
			</td>
			<td align="left" valign="top">
			<div class="status_can">
			<?php
				
				if($arr[$i]["File2ApproveYN"] != array())
				{ 
					if($arr[$i]["File2ApproveYN"] == false){ 
						echo "Unapproved"; 
					} else{
						echo "Approved"; 
					} 
				}
			?>
			</div>
			</td>
			<td align="left" valign="top">
				<strong>
				<?php
				
				if($arr[$i]["File2"] != array())
				{ 
					echo '<a class="change_password" href="download.php?file='.$arr[$i]["File2"].'&uId='.$userId.'" target="_blank">View</a>';
				}
				?>
				</strong>
			</td>
		</tr>
			
		
		<?php
			} if($arr[$i]["File5"] != array())
		{ 
			
		?>
		
		<tr>
			<td align="left" valign="top">
			<?php
				
				if($arr[$i]["File5"] != array())
				{ 
					echo 'Voter ID';
				}
			?>
			</td>
			<td align="left" valign="top">
			<div class="status_can">
			<?php
				
				if($arr[$i]["File5ApproveYN"] != array())
				{ 
					if($arr[$i]["File5ApproveYN"] == false){ 
						echo "Unapproved"; 
					} else{
						echo "Approved"; 
					} 
				}
			?>
			</div>
			</td>
			<td align="left" valign="top">
				<strong>
				<?php
				
				if($arr[$i]["File5"] != array())
				{ 
					echo '<a class="change_password" href="download.php?file='.$arr[$i]["File5"].'&uId='.$userId.'" target="_blank">View</a>';
				}
				?>
				</strong>
			</td>
		</tr>
		<?php
			}
			
			
			
			if($arr[$i]["File4"] != array())
		{ 
			
		?>
		
		<tr>
			<td align="left" valign="top">
			<?php
				
				if($arr[$i]["File4"] != array())
				{ 
					echo 'Aadhar Card';
				}
			?>
			</td>
			<td align="left" valign="top">
			<div class="status_can">
			<?php
				
				if($arr[$i]["File4ApproveYN"] != array())
				{ 
					if($arr[$i]["File4ApproveYN"] == false){ 
						echo "Unapproved"; 
					} else{
						echo "Approved"; 
					} 
				}
			?>
			</div>
			</td>
			<td align="left" valign="top">
				<strong>
				<?php
				
				if($arr[$i]["File4"] != array())
				{ 
					echo '<a class="change_password" href="download.php?file='.$arr[$i]["File4"].'&uId='.$userId.'" target="_blank">View</a>';
				}
				?>
				</strong>
			</td>
		</tr>
		<?php
			}
			
			
			
			
		}
		}else{
	?>
	<tr>
			<td colspan="3" align="left" valign="top"><span><?php echo "No documents details." ?></span></td>
			
	</tr>
	
	<?php 
	
		}
	?>
	</table>
	
	</div>
</div>	
<script>
function selectbox_selection(val)
{
	if(val=='Others')
	{
		$('#txtOtherPurpose').show();
	}
	else if(val=='Holiday' || val=='Business' || val=='Alternate car' || val=='Bigger car')	
	{
		$('#txtOtherPurpose').hide();
	}	
}	
function mobile_or_booking()
{	
	if($('#opt1').is(':checked'))
	{
		$('#feedback1').show();
		$('#divNOblock').hide();
	}
	else if($('#opt2').is(':checked'))
	{
		$('#divNOblock').show();
		$('#feedback1').hide();
	}						
}
function overall_experience_radio()
{	
	if($('#rd1').is(':checked') || $('#rd2').is(':checked'))
	{
		$('#wwr').show();
	}
	else if($('#rd3').is(':checked') || $('#rd4').is(':checked') || $('#rd5').is(':checked'))
	{
		$('#wwr').hide();
	}						
}
function checkbox_checked()
{
	if($("#ckReason7").is(':checked'))	
	{
		$('#txtOtherReason').show();
	}
	else 	
	{
		$('#txtOtherReason').hide();
	}		
}
function getBooking()
{
	var bookingid = $('#bookingid').val();
	$.ajax({
    url : "<?PHP echo corWebRoot;?>/getbooking.php",
    type: "POST",
    data : {bookingid:bookingid},
	beforeSend:function(){
	 $('#spDiscount').show();
	},
    success: function(r){
	if(r != "" && r.indexOf("|#|") > -1 && r!='NotFound'){
    var arrR = r.split("|#|");
	if(arrR[0] != ""){
		$('#spDiscount').css({'color':'green','margin-top':'0px'}).html('Valid Booking Id');
		document.getElementById('hdmonumber').value = arrR[0];
		document.getElementById('hdtxtname').value = arrR[1];
		document.getElementById('hdemail').value = arrR[2];
		document.getElementById('hdIsSelfDrive').value = arrR[3];
		document.getElementById('hdCheckfeedback').value = "true";
	} 
	}
	else
	{
		$('#spDiscount').css({'color':'red','margin-top':'-4px'}).html('Booking ID does not exist.Please choose NO .<br> if you dont have a valid Booking ID');
		$('#bookingid').val('');
		return false;
	}
	}	
	});
}
function validationwithsubmit()
{
 var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 if($('#bookingid').val()=='' && $('#bookingid').is(':visible'))
 {
	 alert('Please Enter booking id.');
	  $('#bookingid').focus();
	 return false;
 }
 if($('#monumber_feedback').val()=='' && $('#monumber_feedback').is(':visible'))
 {
	 alert('Please Enter Mobile no.')
	  $('#monumber_feedback').focus();
	 return false;
 } 
 if($('#txtname_feedback').val()=='' && $('#txtname_feedback').is(':visible'))
 {
	 alert('Please Enter Name.')
	 $('#txtname_feedback').focus();
	 return false;
 }
 if($('#email_feedback').val()=='' && $('#email_feedback').is(':visible'))
 {
	 alert('Please Enter Email Id.');
	 $('#email_feedback').focus();
	 return false;
 }
 if (!filter.test($('#email_feedback').val()) && $('#email_feedback').is(':visible')) 
 {	
	alert("Please fill a valid email id.");
	$("#email_feedback").focus();
	return  false;
 }
 if($('#ddlpurpose').val()=='0')
 {
	 alert('Please Select Purpose of trip.');
	 $('#ddlpurpose').focus();
	 return false;
 }
 if($('#ddlpurpose').val()=="Others" && $('#txtOtherPurpose').is(':visible') && $('#txtOtherPurpose').val()=='')
 {
	 alert('Please specify txtOtherPurpose.');
	 $('#txtOtherPurpose').focus();
	 return false;
 } 
if($('.reason:checked').length==0)
{
	 alert('Please Select At list one reason.');
	 $('#ckReason1').focus();
	 return false;
}
if($('#ckReason7').is(':checked') && $('#txtOtherReason').is(':visible') && $('#txtOtherReason').val()=='')
{
	 alert('Please specify .');
	 $('#txtOtherReason').focus();
	 return false;
}
 if($("input[name='rd']:checked").length == 0)
 {
	 alert('Please Select At list one Expireience.');
	 $('#rd1').focus();
	 return false;
 }
 if($('.resp').is(':visible') && $('.hidden1:checked').length==0 && ($('#rd1').is(':checked') || $('#rd2').is(':checked')))
 {
	 alert('Please Select At list one wrong.');
	 $('#chkwwr1').focus();
	 return false;
 }
 if($('.resp').is(':visible') && $("input[name='chkwwr7']:checked").length == 1 && $('#txtWrong').val()=='')
 {
	 alert('Please specify .');
	 $('#txtWrong').focus();
	 return false;
 }
 if($('#txtfeedback').val()=="")
 {
	 alert('Please Enter Feedback.');
	 $('#txtfeedback').focus();
	 return false;
 }
 if($('#capcha').val()=="")
 {
	 alert('Please Enter Capche Code.');
	 $('#capcha').focus();
	 return false;
 }
 else
 {
	  var dataString = $("#airportform").serialize();
	  $.ajax(
			  { 
			    type: "POST",
				url: "<?php echo ecWebRoot; ?>/booking-airport.php",
				data: dataString,
				beforeSend: function () {
					$('#loadergif').show();
				},
				success: function (response) {
					alert(data);
					if (response != '')
					{
						alert(response);
						$("#optpasstest").css('border-color', 'rgb(228, 109, 127)').focus();

					}
					else
					{
						location.href = 'thanksairport.php'
					}
					$('#loadergif').hide();

			}
		});
    }
 }
$(document).ready(function () {
  $("#bookingid").keydown(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 && $('#bookingid').is(':visible'))) {
        alert('Only Numeric value');
		$('#bookingid').val('').focus();
               return false;
    }
   });
});
$(document).ready(function () {
  $("#monumber_feedback").keydown(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && $('#monumber_feedback').is(':visible')) {
        alert('Only Numeric value');
		$('#monumber_feedback').val('').focus();
               return false;
    }
   });
});
</script>
<div id="registration" style="padding-left:25px !important;">
<div id="regclose">
	<a onclick="_hideFloatingObjectWithID('registration'); _enableThisPage();document.getElementById('finishmess').innerHTML = '';"><img style="float:right;padding-right:15px; cursor: pointer;" alt="Close" title="Close" src ="<?php echo corWebRoot; ?>/images/close.png"/></a>
</div>
<div class="divtabtxt" style="width:560px;">
				<div class="middiv" style="float:left;height:500px;	overflow-y:scroll;overflow-x:hidden;">
					<div class="register" style="margin-bottom:25px !important;width:528px !important;">
						
						<form method="post" name="frmFeedback" id="frmFeedback" action="./save-booking-feedback.php" onsubmit="return validationwithsubmit();">
						<input type="hidden" name="hdAllowFormSubmit" id="hdAllowFormSubmit" value="no" />	
						<input type="hidden" name="hdCheckfeedback" id="hdCheckfeedback" value="true" />					
						<input type="hidden" name="hdIsSelfDrive" id="hdIsSelfDrive" value="0" />
						<!-- Added By Aamir on 3May -->
						<input type="hidden" name="hdmonumber" id="hdmonumber" value="" />
						<input type="hidden" name="hdtxtname" id="hdtxtname" value="" />
						<input type="hidden" name="hdemail" id="hdemail" value="" />

						<div class="fieldrow2">
							Thank you for considering this. <br />This won't take more than a minute of yours & will help us serve you better.<br /><br /><br />
							<label>Please let us know if you have a valid booking ID?</label>
						</div>
						<div class="fieldrow">
							<table width="30%" cellspacing="2" cellpadding="2" border="0">
								<tbody>
									<tr>
										<td width="15px"><input type="radio" checked="checked" onclick="mobile_or_booking();" value="1" id="opt1" name="opt" /></td>
										<td>Yes</td>
										<td width="15px"><input type="radio" onclick="mobile_or_booking();" value="2" id="opt2" name="opt" /></td>
										<td>No</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div style="display:block;">
							<div class="fl">
								<div id="feedback1">
									<div class="fieldrow2" style="margin-bottom:14px;">
										  <label>Please provide us with your booking id</label>
									</div>
									<div class="fieldrow" style="margin-bottom:0px;">
										<input type="text"  onchange="return getBooking();" maxlength="10"  id="bookingid" name="bookingid" class="tbgen" /> <span id="spDiscount" style="display:none; float: left;padding: 2px;color: green; margin-top: 3px;margin-left: 20px;">Please Wait We Validate Your Booking Id!</span>
									</div>
								</div>
							</div>
							<div id="divNOblock" style="display:none">
								<div class="fl">
									<div class="fieldrow2">
										  <label>Mobile number</label>
									</div>
									<div class="fieldrow">
										<input type="text" maxlength="10" value="" class="tbgen" id="monumber_feedback" name="monumber" />
									</div>
								</div>
								<div class="fl">
									<div class="fieldrow2">
										  <label>Name</label>
									</div>
									<div class="fieldrow">
										<input type="text" onkeypress="javascript: return _allowAlpha(event);" class="tbgen" value="" id="txtname_feedback" name="txtname" />
									</div>
								</div>
								<div style="clear:both"></div>
								<div class="fl">
									<div class="fieldrow2">
										<label>Email</label>
									</div>
									<div class="fieldrow">
										<input type="text" value="" id="email_feedback" class="tbgen" name="email" />
									</div>
								</div>
							</div>
							<div style="clear:both"></div>
							<div class="row">
									What was the purpose of your trip?
								</div>
								<div style="clear:both"></div>
								<div class="resp">
									<select id="ddlpurpose" name="ddlpurpose" class="ddl" onchange="selectbox_selection(this.value)">
										<option value="0">Select One</option>
										<option value="Holiday">Holiday</option>
										<option value="Business">Business</option>
										<option value="Alternate car">Alternate car</option>
										<option value="Bigger car">Bigger car</option>
										<option value="Others">Others</option>
									</select>
									<textarea id="txtOtherPurpose" rows="2" cols="40" name="txtOtherPurpose" style="margin-left:15px;display: none" placeholder="Please specify"></textarea>
								</div>
								<div style="clear:both"></div>
								<div class="row">
									Reason you chose us?
								</div>
								<div style="clear:both"></div>
								<div class="resp">
								<div style="float:left;">
									<div class="col">
										<input type="checkbox" id="ckReason1" name="satisfied1" value="Multiple/nearby location (s)" class="mybox reason" /><label for="ckReason1">Multiple &#47; nearby location (s)</label>
									</div>
									<div class="col">
										<input type="checkbox" id="ckReason2" name="satisfied2" value="Variety of cars" class="mybox reason" /><label for="ckReason2">Variety of cars</label>
									</div>
								</div>
								<div>
									<div class="col">
										<input type="checkbox" id="ckReason3" name="satisfied3" value="I like the concept" class="mybox reason" /><label for="ckReason3">I like the concept</label>
									</div>
									<div class="col">
										<input type="checkbox" id="ckReason4" name="satisfied4" value="Cost effective" class="mybox reason" /><label for="ckReason4">Cost effective</label>
									</div>
								</div>
								<div>
									<div class="col">
										<input type="checkbox" id="ckReason5" name="satisfied5" value="Privacy" class="mybox reason" /><label for="ckReason5">Privacy</label>
									</div>
									<div class="col" style="width:300px">
										<input type="checkbox" id="ckReason6" name="satisfied6" value="Was referred by a friend &#47; colleague &#47; relative" class="mybox reason" /><label for="ckReason6">Was referred by a friend &#47; colleague &#47; relative</label>
									</div>
								</div>
								<div>
									<div class="col" style="width:500px;">
									
									
									
										<input type="checkbox" id="ckReason7" name="satisfied7" id="chkOtherReason" value="Yes" class="mybox reason" onclick="checkbox_checked()" />
										
										
										<label for="ckReason7">Others</label>
										<textarea  id="txtOtherReason" rows="2" cols="40" name="txtOtherReason" style="display: none;margin-left:20px;" placeholder="Please specify"></textarea>
									</div>
								</div>
								</div>
								<div style="clear:both"></div>
								<div class="experience">
									Rate your overall experience with us?<br />
									<div class="resp">
									<input type="radio" id="rd1" name="rd" value="1" class="mybox" onclick="overall_experience_radio();"/><label for="rd1">1</label>
									<input type="radio" id="rd2" name="rd" value="2" class="mybox" onclick="overall_experience_radio();"/><label for="rd2">2</label>
									<input type="radio" id="rd3" name="rd" value="3" class="mybox" onclick="overall_experience_radio();"/><label for="rd3">3</label>
									<input type="radio" id="rd4" name="rd" value="4" class="mybox" onclick="overall_experience_radio();"/><label for="rd4">4</label>
									<input type="radio" id="rd5" name="rd" value="5" class="mybox" onclick="overall_experience_radio();"/><label for="rd5">5</label>
									</div>
									<div style="clear:both"></div>
									<div id="wwr">
									What went wrong?<br />
									<div class="resp">
										<div class="col">
											<input type="checkbox" id="chkwwr1" name="chkwwr1" value="Call center executive" class="mybox hidden1"/><label for="chkwwr1">Call center executive</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr2" name="chkwwr2" value="Ground Staff" class="mybox hidden1"/><label for="chkwwr2">Ground Staff</label>
										</div>
										<div style="clear:both"></div>
										<div class="col">
											<input type="checkbox" id="chkwwr3" name="chkwwr3" value="Issues in car pick up & drop" class="mybox hidden1"/><label for="chkwwr3">Issues in car pick up & drop</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr4" name="chkwwr4" value="Website &#47; booking engine" class="mybox hidden1"/><label for="chkwwr4">Website &#47; booking engine</label>
										</div>
										<div style="clear:both"></div>
										<div class="col">
											<input type="checkbox" id="chkwwr5" name="chkwwr5" value="Condition of the car" class="mybox hidden1"/><label for="chkwwr5">Condition of the car</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr6" name="chkwwr6" value="Invoicing" class="mybox hidden1"/><label for="chkwwr6">Invoicing</label>
										</div>
										<div style="clear:both"></div>
										<div class="col" style="width:500px;">
											<input type="checkbox" id="chkwwr7" name="chkwwr7" value="1" class="mybox hidden1" onclick="javascript: _showWWROthers(this.id)"/><label for="chkwwr7">Other</label>
											<textarea  id="txtWrong" name="txtWrong" rows="2" cols="40" style="display: none;margin-left:20px;" plcaeholder="Please specify"></textarea>
										</div>
									</div>
									</div>
								</div>
							</div>							
							<div id="divIsNotForSelfDrive" style="display:none;">
								<div class="fieldrow2 row">
								<label>Was the booking procedure convenient?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo1" name="rdo1" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo2" name="rdo1" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Did the cab report on time?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo3" name="rdo2" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo4" name="rdo2" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Were you happy with your experience with the chauffeur?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo5" name="rdo3" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo6" name="rdo3" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Did the car meet your expectations?</label>
								</div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo7" name="rdo4" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo8" name="rdo4" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
								<label>Were you satisfied with your overall experience with us?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo9" name="rdo5" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo10" name="rdo5" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								</div>
								<div style="clear:both"></div>
								<div class="row">
									What can we do to keep you coming back for more?
									<div style="clear:both"></div>
									<div class="resp resp">
									<textarea id="txtfeedback" name="txtfeedback" rows="3" cols="60"></textarea>
									</div>
								</div>
								<div class="row">
									Are you human? Please type the digits<br /><br />
									<div style="float:left;width:80px;"><img src="./captcha.php" alt="Image Capture" title="Image Capture" id="imgcaptcha" /></div> <div style="float:left;width:260px;"><input class="captcha ddl" type="text" name="capcha" id="capcha" /></div>
									<span id="err"></span>
								</div>
								<div class="fieldrow">
									<!-- <div class="submit"><br /><a onclick="javascript: _validateFeedback();" id="feedbacksubmit" href="javascript:void(0)" style="float:left !important;"></a></div> -->
									<div class="submit"><br />
									<input type="submit" id="feedbacksubmit" name="feedbacksubmit" style="float:left !important;" />
									</div>
									<span id="finishmess"></span>
								</div>
								</div>
						</form>
					</div>
				</div>
			</div>
</div>
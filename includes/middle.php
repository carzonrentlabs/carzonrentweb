<div class="cabs">
	<div class="main">
    	<div class="ftop">
        	<div class="wdcar">
            <img src="<?php echo corWebRoot; ?>/images/car.png" alt="Cabs" title="Cabs" width="47" height="39" /> <label><span>8,000</span><br />Cabs</label>
            </div>
            <div class="wdtrips">
            <img src="<?php echo corWebRoot; ?>/images/trips.png" alt="Trips Daily" title="Trips Daily" width="42" height="42" /> <label><span>20,000+</span><br />Trips Daily</label>
            </div>
            <div class="wdten">
            <img src="<?php echo corWebRoot; ?>/images/lst.png" alt="Customers Annually" title="Customers Annually" width="44" height="40" /> <label><span>6,000,000+</span><br />Customers Annually</label>
            </div>
            <div class="wdten">
            <img src="<?php echo corWebRoot; ?>/images/ten.png" alt="Kilometers Daily" title="Kilometers Daily" width="40" height="47" /> <label><span>100,000+</span><br />Kilometers Daily</label>
            </div>
        </div>
    </div>
</div>
<div id="middle">
	<div class="main">
    	<div class="homepage" style="width:850px">
        	<div class="repeatdiv">
            	<img src="<?php echo corWebRoot; ?>/images/icon3.gif" alt="Quality" width="66" height="63" title="Quality" class="icon1" />
            	<h2 class="icon1">Quality</h2>
                <ul>
                	<li>Well trained chauffeurs</li>
			<li>Young, well-maintained car fleet</li>
			<li>Amenities for comfort  </li>
  

                </ul>
            </div>
            <div class="repeatdiv md">
            	<img src="<?php echo corWebRoot; ?>/images/icon2.gif" alt="Reliability" width="34" height="51" title="Reliability" class="icon2" />
            	<h2 class="icon2">Reliability</h2>
                <ul>
                    <li>India's largest car hire&#47;rental company</li>
                    <li>In car GPS devices for extra safety</li>
                    <li>Transparent pricing structure</li>
                </ul>
            </div>
            <div class="repeatdiv right" style="margin-right:0px;width:225px">
            	<img src="<?php echo corWebRoot; ?>/images/icon1.gif" alt="Convenience" title="Convenience" width="47" height="50" class="icon3" />
            	<h2 class="icon3">Convenience</h2>
                <ul>
                	<li>Available across major cities</li>
                    <li>Book through web or phone</li>
                    <li>Pay by cash or card</li>
                </ul>
            </div>
        </div>
    </div>
</div>
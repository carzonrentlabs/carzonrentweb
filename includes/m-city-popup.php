<div class="travelmulti">
    <!--<a href="javascript:void(0)" class="multi">&nbsp;</a>-->
    <div class="modyfybook_wrapper">
    <!--<div class="toolarrow"></div>-->
        <div class="heading">Travel multi-city<span class="closedpop">X</span></div>
        <table width="100%" cellpadding="0" cellspacing="5" border="0" id="mc-tab">
            <tr style="display:block;">
                <td align="right" valign="middle" width="20%">From</td>
                <td align="left" valign="top" width="35%">
                    <div class="modyfy_select">
                        <!--<input type="text" id="mc-origin" name="mc-origin" class="modifymyb" />-->
                        <select id="mc-origin1" name="mc-origin" class="modifymyb" style="width: 200px !important;" onchange="javascript: _setOrigin(this.id);">
                            <option>Select Origin</option>
        <?php
                            for($i = 0; $i < count($org); $i++){
                                if($org[$i]["CityName"] == $cCity){
	?>
                                    <option value="<?php echo $org[$i]["CityID"]; ?>" selected="selected"><?php echo $org[$i]["CityName"]; ?></option>
	<?php
				}
				else {
	?>
				    <option value="<?php echo $org[$i]["CityID"]; ?>"><?php echo $org[$i]["CityName"]; ?></option>
	<?php
                                }
                            }
	?>            
                        </select>
                    </div>
                </td>
                <td align="right" valign="middle" width="5%">To</td>
                <td align="left" valign="top" width="40%" class="inputtext">                   
                        <input type="text" id="mc-destination1" name="mc-destination" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest1', arrDestination, 0, 0, 0, 182, 'mc-origin2');" /><div class="autos"><div id="autosuggest1" class="autosuggestO floatingDiv"></div></div>
                   
                </td>                   
            </tr>
            <tr style="display:block;">
                <td align="right" valign="middle" width="20%">From</td>
                <td align="left" valign="top" width="35%" class="inputtext">
                   
                        <input type="text" id="mc-origin2" name="mc-origin" value="" readonly="readonly" />
                   
                </td>
                <td align="right" valign="middle" width="5%">To</td>
                <td align="left" valign="top" width="40%" class="inputtext">
                   
                       <input type="text" id="mc-destination2" name="mc-destination" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest2', arrDestination, 462, 670, 1, 182, 'mc-origin3');" /><div class="autos"><div id="autosuggest2" class="autosuggestO floatingDiv"></div></div>
                   
                </td>                   
            </tr>
            <tr style="display:none;">
                <td align="right" valign="middle" width="20%">From</td>
                <td align="left" valign="top" width="35%" class="inputtext">
                       <input type="text" id="mc-origin3" name="mc-origin" value="" readonly="readonly" />
                </td>
                <td align="right" valign="middle" width="5%">To</td>
                <td align="left" valign="top" width="40%" class="inputtext">                   
                       <input type="text" id="mc-destination3" name="mc-destination" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest3', arrDestination, 510, 670, 2, 182, 'mc-origin4');" /><div class="autos"><div id="autosuggest3" class="autosuggestO floatingDiv"></div></div>
                  
                </td>                   
            </tr>
            <tr style="display:none;">
                <td align="right" valign="middle" width="20%">From</td>
                <td align="left" valign="top" width="35%" class="inputtext">
                  
                        <input type="text" id="mc-origin4" name="mc-origin" value="" readonly="readonly" />
                  
                </td>
                <td align="right" valign="middle" width="5%">To</td>
                <td align="left" valign="top" width="40%" class="inputtext">
                   
                        <input type="text" id="mc-destination4" name="mc-destination" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest4', arrDestination, 558, 670, 3, 182, 'mc-origin5');" /><div class="autos"><div id="autosuggest4" class="autosuggestO floatingDiv"></div></div>
                    
                </td>                   
            </tr>
            <tr style="display:none;">
                <td align="right" valign="middle" width="20%">From</td>
                <td align="left" valign="top" width="35%" class="inputtext">
                   
                        <input type="text" id="mc-origin5" name="mc-origin" value="" readonly="readonly" />
                    
                </td>
                <td align="right" valign="middle" width="5%">To</td>
                <td align="left" valign="top" width="40%" class="inputtext">
                   
                       <input type="text" id="mc-destination5" name="mc-destination" value="" autocomplete="off" onKeyUp="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest5', arrDestination, 0, 0, 4, 182);" /><div class="autos"><div id="autosuggest5" class="autosuggestO floatingDiv"></div></div>
                   
                </td>                   
            </tr>
            <tr>
                <td colspan="4" class="addmore"><a href="javascript:void(0)" id="addmore" onclick="javascript: _addMore();" >Add more +</a></td>
            </tr>   
            <tr>
                <td colspan="4" class="m_boder">&nbsp;</td>
            </tr>
            <tr style="display:block;">
                <td align="right" valign="middle" width="20%">Pickup Date</td>
                <td align="left" valign="top" width="35%"><span class="datepick">
				<input type="text" size="12" autocomplete="off" name="m-pickdate" value="<?php echo date('d M, Y'); ?>" id="inputField3" class="datepicker" />
				<!-- <input type="text" name="pickdate" autocomplete="off" value="<?php //echo date('Y-m-d'); ?>" /> --></span></td>
                <td align="right" valign="middle" width="5%" >&nbsp;</td>
                <td align="left" valign="top" width="40%"><div class="confirmbooking"><a href="javascript:void(0);" onclick="javascript: _bookMCity('form1');"></a></div></td>                   
            </tr>
            <tr style="display:block;">
                <td align="right" valign="middle" width="20%">Drop Date</td>
                <td align="left" valign="top" width="38%"><span class="datepick">
				<!-- <input type="text" name="pickdate" autocomplete="off" value="<?php //echo date('Y-m-d'); ?>" /> -->
				<input type="text" size="12" autocomplete="off" name="m-dropdate" value="<?php echo date('d M, Y'); ?>" id="inputField4" class="datepicker" />
				</span></td>
                <td align="right" valign="middle" width="2%" >&nbsp;</td>
                <td align="left" valign="top" width="40%">&nbsp;</td>
            </tr>                       
        </table>
    </div>
</div>
<div id="midmyles">
	<div class="main">
		<div class="myles">
			<h2><span style='color:#db4626'>MYLES</span> cars now available in</h2>
			<div class="carrows">
				<div class="cell">DELHI</div>
				<div class="cell">MUMBAI</div>
				<div class="cell">BANGALORE</div>
				<div class="cell">HYDERABAD</div>
				<div class="cell">GOA</div>
				<div class="cell">JAIPUR</div>
				<div class="cell">CHENNAI</div>
				<div class="cell">PUNE</div>
				<div class="cell">AHMEDABAD</div>
				<div class="cell">CHANDIGARH</div>
			</div>
		</div>
	</div>
</div>
<div id="middle">
	<div class="main">
    	<div class="homepage">
        	<div class="repeatdiv">
            	<img src="<?php echo corWebRoot; ?>/images/icon3.gif" alt="Quality" width="66px" height="63px" title="Quality" class="icon1" />
            	<h2 class="icon1">Quality</h2>
                <ul>
                	<li>Well trained chauffeurs</li>
			<li>Young, well-maintained car fleet</li>
			<li>Amenities for comfort  </li>
  

                </ul>
            </div>
            <div class="repeatdiv md">
            	<img src="<?php echo corWebRoot; ?>/images/icon2.gif" alt="Reliability" width="34px" height="51px" title="Reliability" class="icon2" />
            	<h2 class="icon2">Reliability</h2>
                <ul>
                    <li>India's largest car hire/rental company</li>
                    <li>In car GPS devices for extra safety</li>
                    <li>Transparent pricing structure</li>
                </ul>
            </div>
            <div class="repeatdiv right">
            	<img src="<?php echo corWebRoot; ?>/images/icon1.gif" alt="Convenience" title="Convenience" width="47px" height="50px" class="icon3" />
            	<h2 class="icon3">Convenience</h2>
                <ul>
                	<li>Available across major cities</li>
                    <li>Book through web or phone</li>
                    <li>Pay by cash or card</li>
                </ul>
            </div>
        </div>
    </div>
</div>
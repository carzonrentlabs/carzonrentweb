<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/secure.js"></script>
<script>
    jQuery(document).ready(function ($) {

        if (window.history && window.history.pushState) {

            $(window).on('popstate', function () {
                var hashLocation = location.hash;
                var hashSplit = hashLocation.split("#!/");
                var hashName = hashSplit[1];

                if (hashName !== '') {
                    var hash = window.location.hash;
                    if (hash === '') {
                        // alert("Back Button pressed not allowed");
                        location.href = webRoot;
                    }
                }
            });

            window.history.pushState('forward', null, './#forward');
        }

    });

</script>
<?php
//echo "<pre>";print_r($_POST);die;
/* * ** alok code for LMS *** */
$_SESSION['SEARCH_CUSTOMER_ID'] = $_REQUEST['searchId'];
/* * ** alok code for LMS  End *** */
if ($tab == 3) {
    if ($tCCIID != 0)
        $cciid = $tCCIID;
    if (isset($_COOKIE["tcciid"]))
        $cciid = $_COOKIE["tcciid"];
    if (isset($_COOKIE["cciid"]))
        $cciid = $_COOKIE["cciid"];
    $totPayable = $basicAmt = $addSrvAmt = $subLocCost = $vatAmt = $disA = 0;
    if ($_POST["hdTourtype"] == "Selfdrive") {
        $basicAmt = $_POST["BasicAmt"];
        if ($_REQUEST['empcode'] == 'PayBack') {
            $disAPayback = $_POST["discountAmt"];
        } else {
            $disA = $_POST["discountAmt"];
        }
        $cnvCharge = $_POST["airportCharges"];
        $arrAdSrv = explode(",", $_POST["addServ"]);
        $corAS = new COR();
       /***** added by vinod K Maurya *****/
		if (count($arrAdSrv) > 0) 
		{
		   if($_REQUEST['chkPkgType']=='Hourly')
		    $durationVal=1;
		    else
		    $durationVal=$_REQUEST['duration'];
			$arrSer = array();
			$arrSer["CarModelId"] = $_REQUEST["hdCarCatID"];
			$arrSer["ClientID"] = 2205;
			if ($_REQUEST['empcode'] == 'PayBack') 
			{
				$arrSer["PickUpDate"] = $_REQUEST["hdPickdate"];
				$arrSer["DropOffDate"] = $_REQUEST["hdDropdate"];
			}
			else
			{
				$pick = explode('/',$_REQUEST["hdPickdate"]);
				$drop = explode('/',$_REQUEST["hdDropdate"]);
				$arrSer["PickUpDate"] = $pick["2"].'-'.$pick["0"].'-'.$pick["1"];
				$arrSer["DropOffDate"] = $drop["2"].'-'.$drop["0"].'-'.$drop["1"];
			}
			$arrSer["PickUpTime"] = $_REQUEST["tHourP"].$_REQUEST["tMinP"];
			$arrSer["DropOffTime"] = $_REQUEST["tHourD"].$_REQUEST["tMinD"];
			$arrSer["PickUpCityID"] = $_REQUEST["hdOriginID"];
			$resAS = $corAS->_CORSelfDriveAdditionalServiceDetails($arrSer);
			$asAmts = $myXML->xml2ary($resAS->{'GetAdditionalService_NewResult'}->{'any'});
			foreach($asAmts as $asAmt)
			{
			
				  if ($asAmt["Active"] == true && in_array($asAmt["ServiceID"], $arrAdSrv)) {	
					
					if($asAmt["ServiceID"]==3)     //pick up drop off 
					$addSrvAmt += $asAmt["Amount"];
					else
				    $addSrvAmt += $asAmt["Amount"]*$durationVal;
				}
			}
		}
		/***** added by vinod K Maurya *****/
        $data = array();
        $data["subLoctionID"] = $_POST["ddlSubLoc"];
        $data["basicAmount"] = ($basicAmt + $addSrvAmt);
        $res = $corAS->_CORGetSubLocCost($data);
        $subLocCost = ceil($res->{"SublocationPercentCostResult"});

        $vatRes = $corAS->_CORVatRate(array("CityId" => $_POST["hdOriginID"]));
        $arrVat = $myXML->xml2ary($vatRes->{'GetVatRateResult'}->{'any'});
        $vat = number_format($arrVat[0]["DSTPercent"], 3);

        unset($data);
        unset($corAS);

        if (isset($_POST['vatAmt']) && $_POST['vatAmt'] != "") {
            $vatAmt = $_POST['vatAmt'];  /* coming  from discount directly */
        } else {
            $vatAmt = ceil(($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) * $vat) / 100;  // coming from payback
        }

        $orignalBilling = ceil($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge + $vatAmt);

        if ($disAPayback != "") {
            $totPayable = ceil(($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) + $vatAmt) - $disAPayback;
        } else {
            $totPayable = $_POST["totFare"];
            $subtAmount = $_POST['subtAmount'];
        }

        //$vatAmt = ceil(((($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) - $disA) * $vat) / 100);
        //$totPayable = (((($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) - $disA) + $vatAmt));
        //$totPayable = $totPayable - intval($_POST["discountAmt"]);
    } else {
        $totPayable = $_POST["totFare"];
    }
    ?>
	<!----------------------trip Advisor payment-------------------------->
	<?PHP 
	if($_REQUEST['tripadvisorname']=='true')
	{
		$_SESSION['tripadvisor']=$_REQUEST['tripadvisorname'];
		$_SESSION['tripadvisordestination']=$_REQUEST['txtDestination'];
		$_SESSION['tripadvisordestinationid']=$_REQUEST['tripadvisordestinationid'];
	}	
	?>
	<!---------------------trip Advisor payment---------------------->
    <div id="country3" class="tabcontent">		
    <?php
    $_SESSION["rurl"] = urlencode("http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
    $mtime = round(microtime(true) * 1000);

    $_SESSION["pkgId"] = $_POST["hdPkgID"];
    $_SESSION["hdCat"] = $_POST["hdCat"];
    $_SESSION["hdCarModel"] = $_POST["hdCarModel"];
    $_SESSION["hdOriginName"] = $_POST["hdOriginName"];
    $_SESSION["hdTourtype"] = $_POST["hdTourtype"];
    $_SESSION["hdDestinationName"] = $_POST["hdDestinationName"];
    $_SESSION["hdPickdate"] = $_POST["hdPickdate"];
    $_SESSION["hdDropdate"] = $_POST["hdDropdate"];
    if ($_POST["hdTourtype"] == "Selfdrive") {
        if (isset($_POST["picktime"])) {
            $_SESSION["picktime"] = $_POST["picktime"];
        } else {
            $_SESSION["picktime"] = $_POST["tHourP"] . $_POST["tMinP"];
        }
        if (isset($_POST["droptime"])) {
            $_SESSION["droptime"] = $_POST["droptime"];
        } else {
            $_SESSION["droptime"] = $_POST["tHourD"] . $_POST["tMinD"];
        }
    } else {
        if (isset($_POST["picktime"])) {
            $_SESSION["picktime"] = $_POST["picktime"];
        } else {
            $_SESSION["picktime"] = $_POST["tHour"] . $_POST["tMin"];
        }
    }
    if (isset($_POST["hdDistance"]) && $_POST["hdDistance"] != "")
        $_SESSION["totalKM"] = $_POST["hdDistance"];
    else
        $_SESSION["totalKM"] = "0";
    $_SESSION["address"] = $_POST["address"];
    $_SESSION["remarks"] = $_POST["remarks"];
    $_SESSION["totFare"] = $_POST["totFare"];
    $_SESSION["rdoPayment"] = "1";
    $_SESSION["dayRate"] = $_POST["dayRate"];
    $_SESSION["kmRate"] = $_POST["kmRate"];
    $_SESSION["duration"] = $_POST["duration"];
    $_SESSION["hdDistance"] = $_POST["hdDistance"];
    $_SESSION["name"] = $_POST["name"];
    $_SESSION["monumber"] = $_POST["monumber"];
    $_SESSION["email"] = $_POST["email"];
    $_SESSION["PkgHrs"] = $_POST["PkgHrs"];
    if (isset($_POST["totAmount"]))
        $_SESSION["totAmount"] = $_POST["totAmount"];
    if (isset($_POST["vat"]))
        $_SESSION["vat"] = $_POST["vat"];
    $_SESSION["secDeposit"] = $_POST["secDeposit"];
    $_SESSION["dispc"] = $_POST["discount"];
    $_SESSION["discountAmt"] = $_POST["discountAmt"];
    $_SESSION["empcode"] = $_POST["empcode"];
    $_SESSION["disccode"] = $_POST["disccode"];
    $_SESSION["cciid"] = $cciid;
    $_SESSION["ChauffeurCharges"] = $_POST["ChauffeurCharges"];
    $_SESSION["orderid"] = "CORIC-" . $mtime;
    $_SESSION["addServ"] = $_POST["addServ"];
    $_SESSION["addServAmt"] = $_POST["addServAmt"];
    $isPB = '0';
    if (isset($_POST["paybackearn"]) && $_POST["paybackearn"] != "")
        $isPB = $_POST["paybackearn"];
    $_SESSION["IsPayBack"] = $isPB;
    if (isset($_POST["pagetransactionId"]))
        $_SESSION["disc_txn_id"] = $_POST["pagetransactionId"];
    if (isset($_POST["redeemedPts"]))
        $_SESSION["disc_value"] = $_POST["redeemedPts"];
    if (isset($_POST["transactionStatus"]))
        $_SESSION["disc_txn_status"] = $_POST["transactionStatus"];
    if (isset($_POST["orderid"]))
        $_SESSION["disc_orderid"] = $_POST["orderid"];
    if (isset($_POST["ddlSubLoc"]))
        $_SESSION["subLoc"] = $_POST["ddlSubLoc"];
    else
        $_SESSION["subLoc"] = "0";
    if (isset($_POST["subLocName"]))
        $_SESSION["subLocName"] = $_POST["subLocName"];
    else
        $_SESSION["subLocName"] = "NA";

    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
    $xmlToSave .= "<ccvtrans>";
    $query_string = "";
    if ($_POST) {
        $kv = array();
        foreach ($_POST as $key => $value) {
            if ($key != "corpassword") {
                $kv[] = "$key=$value";
                $xmlToSave .= "<$key>$value</$key>";
            }
        }
        $query_string = join("&", $kv);
    } else {
        $query_string = $_SERVER['QUERY_STRING'];
    }
    $xmlToSave .= "</ccvtrans>";
    if (!is_dir("./xml/pre-trans/" . date('Y')))
        mkdir("./xml/pre-trans/" . date('Y'), 0775);
    if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m')))
        mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m'), 0775);
    if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
        mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
    createcache($xmlToSave, "./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/CORIC-" . $mtime . ".xml");

    $db = new MySqlConnection(CONNSTRING);
    $db->open();
    $dataToSave = array();
    $dataToSave["source"] = "COR";

    $dataToSave["tour_type"] = $_POST["hdTourtype"];
    $dataToSave["package_id"] = $_POST["hdPkgID"];
    $dataToSave["car_cat"] = $_POST["hdCat"];
    if (isset($_POST["hdCarCatID"]) && $_POST["hdCarCatID"] != "")
        $dataToSave["car_cat_id"] = $_POST["hdCarCatID"];

    $dataToSave["origin_name"] = $_POST["hdOriginName"];
    $dataToSave["origin_id"] = $_POST["hdOriginID"];
    $dataToSave["destinations_name"] = $_POST["hdDestinationName"];
    $dataToSave["destinations_id"] = $_POST["hdDestinationID"];

    $picDate = date_create($_POST["hdPickdate"]);
    $dataToSave["pickup_date"] = $picDate->format('Y-m-d');
    if (isset($_POST["tHour"]) && $_POST["tHour"] != "" && isset($_POST["tMin"]) && $_POST["tMin"] != "")
        $dataToSave["pickup_time"] = $_POST["tHour"] . $_POST["tMin"];
    elseif (isset($_POST["tHourP"]) && isset($_POST["tMinP"]))
        $dataToSave["pickup_time"] = $_POST["tHourP"] . $_POST["tMinP"];
    $drpDate = date_create($_POST["hdDropdate"]);
    $dataToSave["drop_date"] = $drpDate->format('Y-m-d');
    if ($_POST["hdTourtype"] == "Selfdrive")
        $dataToSave["drop_time"] = $_POST["tHourD"] . $_POST["tMinD"];

    $dataToSave["duration"] = $_POST["duration"];
    if (isset($_POST["hdDistance"]) && $_POST["hdDistance"] != "")
        $dataToSave["distance"] = $_POST["hdDistance"];
    else
        $dataToSave["distance"] = "0";
    $dataToSave["tot_fare"] = $_POST["totFare"];

    $dataToSave["address"] = $_POST["address"];
    $dataToSave["remarks"] = $_POST["remarks"];

    $dataToSave["full_name"] = $_POST["name"];
    $dataToSave["email_id"] = $_POST["email"];
    $dataToSave["mobile"] = $_POST["monumber"];

    $dataToSave["discount"] = $_POST["discount"];
    //$dataToSave["discount_amt"] = $_POST["discountAmt"];



    $empcode = $_POST["empcode"];
    if ($empcode == 'FLATDISCOUNT') {
        $dataToSave["discount_amt"] = $_POST["discountAmt"] + ( $_POST["discountAmt"] * $_POST["vat"] / 100 );
    } else {
        $dataToSave["discount_amt"] = $_POST["discountAmt"];
    }

    if (trim($empcode) == "Promotion code")
        $empcode = "";
    $dataToSave["promotion_code"] = $empcode;

    $disccode = $_POST["disccode"];
    if (trim($disccode) == "Discount coupon number")
        $disccode = "";
    $dataToSave["discount_coupon"] = $disccode;

    //$dataToSave["promotion_code"] = $_POST["empcode"];
    //$dataToSave["discount_coupon"] = $_POST["disccode"];

    $dataToSave["additional_srv"] = $_POST["addServ"];
    $dataToSave["additional_srv_amt"] = $_POST["addServAmt"];

    $dataToSave["chauffeur_charges"] = $_POST["ChauffeurCharges"];

    $dataToSave["cciid"] = $cciid;
    $dataToSave["coric"] = "CORIC-" . $mtime;
    $dataToSave["ip"] = $_POST['ipaddress']; //$_SERVER["REMOTE_ADDR"];
    $dataToSave["ua"] = $_POST['browser'];//$_SERVER["HTTP_USER_AGENT"];
    $dataToSave["entry_date"] = date('Y-m-d H:i:s');
    if (isset($_POST["totAmount"]) && $_POST["totAmount"] != "")
        $dataToSave["base_fare"] = $_POST["totAmount"];
    else
        $dataToSave["base_fare"] = $_POST["totFare"];
    if (isset($_POST["vat"]) && $_POST["vat"] != "")
        $dataToSave["vat"] = $_POST["vat"];
    else
        $dataToSave["vat"] = 0.00;
    if (isset($_POST["secDeposit"]) && $_POST["secDeposit"] != "")
        $dataToSave["sec_deposit"] = $_POST["secDeposit"];
    else
        $dataToSave["sec_deposit"] = 0;
    if (isset($_POST["kmRate"]) && $_POST["kmRate"] != "")
        $dataToSave["km_rate"] = $_POST["kmRate"];
    else
        $dataToSave["km_rate"] = 0;
    $dataToSave["isPayBack"] = $isPB;
    if (isset($_POST["pagetransactionId"]))
        $dataToSave["disc_txn_id"] = $_POST["pagetransactionId"];
    if (isset($_POST["redeemedPts"]))
        $dataToSave["disc_value"] = $_POST["redeemedPts"];
    if (isset($_POST["transactionStatus"]))
        $dataToSave["disc_txn_status"] = $_POST["transactionStatus"];
    if (isset($_POST["orderid"]))
        $dataToSave["disc_orderid"] = $_POST["orderid"];
    if (isset($_POST["ddlSubLoc"]))
        $dataToSave["subLoc"] = $_POST["ddlSubLoc"];
    if (isset($_COOKIE["kword"]) && $_COOKIE["kword"] != "")
        $dataToSave["kword"] = $_COOKIE["kword"];
    if (isset($_COOKIE["gclid"]) && $_COOKIE["gclid"] != "")
        $dataToSave["gclid"] = $_COOKIE["gclid"];
    $dataToSave["pkg_type"] = $_POST["chkPkgType"];
    $dataToSave["add_service_cost_all"] = $_POST["addServiceCostAll"];
    $dataToSave["model_name"] = str_ireplace(array("<br>", "<br />"), "", nl2br($_POST["hdCarModel"]));
    if (isset($_POST["hdCarModelID"]) && $_POST["hdCarModelID"] != "")
        $dataToSave["model_id"] = $_POST["hdCarModelID"];

    if (isset($_POST["subLocName"]) && $_POST["subLocName"] != "")
        $dataToSave["subLocName"] = $_POST["subLocName"];
    if (isset($_POST["subLocCost"]) && $_POST["subLocCost"] != "")
        $dataToSave["subLocCost"] = $_POST["subLocCost"];
    if (isset($_POST["OriginalAmt"]) && $_POST["OriginalAmt"] != "")
        $dataToSave["OriginalAmt"] = $_POST["OriginalAmt"];
    if (isset($_POST["BasicAmt"]) && $_POST["BasicAmt"] != "")
        $dataToSave["BasicAmt"] = $_POST["BasicAmt"];
    if (isset($_POST["KMIncluded"]) && $_POST["KMIncluded"] != "")
        $dataToSave["KMIncluded"] = $_POST["KMIncluded"];
    if (isset($_POST["ExtraKMRate"]) && $_POST["ExtraKMRate"] != "")
        $dataToSave["ExtraKMRate"] = $_POST["ExtraKMRate"];
    if (isset($_POST["airportCharges"]) && $_POST["airportCharges"] != "")
        $dataToSave["airportCharges"] = $_POST["airportCharges"];

    if (isset($_POST["WeekDayDuration"]) && $_POST["WeekDayDuration"] != "")
        $dataToSave["WeekDayDuration"] = $_POST["WeekDayDuration"];

    if (isset($_POST["WeekEndDuration"]) && $_POST["WeekEndDuration"] != "")
        $dataToSave["WeekEndDuration"] = $_POST["WeekEndDuration"];

    if (isset($_POST["FreeDuration"]) && $_POST["FreeDuration"] != "")
        $dataToSave["FreeDuration"] = $_POST["FreeDuration"];

    if (isset($_POST["totFare"]) && $_POST["totFare"] != "")
        $dataToSave["User_billable_amount"] = $_POST["totFare"];

    if (isset($_POST["triptype"]) && $_POST["triptype"] != "")
        $dataToSave["trip_type"] = $_POST["triptype"];

    if (isset($_REQUEST["FlexiBooking"]) && $_REQUEST["FlexiBooking"] != "") {
        $dataToSave["flexi_remark"] = $_REQUEST["FlexiBooking"]; //flexi search
    }
    if (isset($_REQUEST["totFareGross"]) && $_REQUEST["totFareGross"] != "") {
        $dataToSave["tot_gross_amount"] = $_REQUEST["totFareGross"]; //Gross total without discount
    }
     if (isset($_REQUEST["pickdrop"]) && $_REQUEST["pickdrop"] != "") {
        $dataToSave["pickupdrop_address"] = $_REQUEST["pickdrop"]; //pick drop address
    }


    if (!isset($_POST["hdCORIC"]))
        $r = $db->insert("cor_booking_new", $dataToSave);
  
    setcookie('kword', "", time() - 3600, "/");
    setcookie('gclid', "", time() - 3600, "/");
    unset($dataToSave);

    /* For Tracking Multiple Booking Attempts without being booked 24th August starts */
    $sql = "Select full_name,email_id,mobile,tour_type,origin_name,destinations_name,ip,ua,pickup_date FROM cor_booking_new WHERE ip = '" . $_SERVER["REMOTE_ADDR"] . "' AND DATE(entry_date) = DATE(" . date('Y-m-d H:i:s') . ") AND booking_id is null Order By uid desc";
    $r = $db->query("query", $sql);
	
    if (!array_key_exists("response", $r)) {
        if (count($r) % 3 == 0) {
            
            $cor = new COR();
            $html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
            $html .= "<tr>";
            $html .= "<td>Name</td>";
            $html .= "<td>" . $r[0]["full_name"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Email</td>";
            $html .= "<td>" . $r[0]["email_id"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Mobile</td>";
            $html .= "<td>" . $r[0]["mobile"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Tour Type</td>";
            $html .= "<td>" . $r[0]["tour_type"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Origin</td>";
            $html .= "<td>" . $r[0]["origin_name"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Destination</td>";
            $html .= "<td>" . $r[0]["destinations_name"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>IP</td>";
            $html .= "<td>" . $r[0]["ip"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Pickup Date</td>";
            $html .= "<td>" . $r[0]["pickup_date"] . "</td>";
            $html .= "</tr>";
            $html .= "<tr>";
            $html .= "<td>Source Website</td>";
            $html .= "<td>www.carzonrent.com</td>";
            $html .= "</tr>";

            $html .= "</table>";
            $dataToMail = array();
            $sql = "select email from EmailManagement as em join email_page_master as epm on em.p_id= epm.p_id where epm.page_name='multiple attempt booking carzonrent' and em.status='1'";
            $db = new MySqlConnection(CONNSTRING);
            $db->open();
            $resemail = $db->query("query", $sql);
            $a = '';
            for ($i = 0; $i <= count($resemail) - 1; $i++) {
                $a.= $resemail[$i]['email'] . ';';
                $email = trim($a, ';');
            }
            $db->close();
            $dataToMail["To"] = $email;
            $dataToMail["Subject"] = "ALERT: Multiple Booking Attempts (" . count($r) . ") on www.carzonrent.com website";
            $dataToMail["MailBody"] = $html;

            $res = $cor->_CORSendMail($dataToMail);

            unset($cor);
            unset($dataToMail);
        }
    }
    /* For Tracking Multiple Booking Attempts without being booked 24th August ends */

    $db->close();

    set_include_path('./lib' . PATH_SEPARATOR . get_include_path());
    require_once('./lib/CitrusPay.php');
    require_once 'Zend/Crypt/Hmac.php';

    function generateHmacKey($data, $apiKey = null) {
        $hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
        return $hmackey;
    }

    $action = "";
    $flag = "";
   
    CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d", 'production');
     //CitrusPay::setApiKey("84865e1c2022ef254ae6a5523d9f234a30065fce", 'sandbox');
    //require_once("./includes/libfuncs.php");
    if (($cciid == "1400323" || $cciid == "1386289"))
        $Amount = "1";
    else {
        $Amount = $totPayable;
    }
    if (isset($_POST["hdCORIC"]) && $_POST["hdCORIC"] != "")
        $Order_Id = $_POST["hdCORIC"];
    else
        $Order_Id = "CORIC-" . $mtime;

    $fullname = $_POST['name'];
    $fNames = explode(" ", $fullname);
    if (count($fNames) > 1) {
        $fname = $fNames[0];
        $lname = $fNames[1];
    } else {
        $fname = $fullname;
        $lname = "";
    }
    $vanityUrl = "carzonrent";
    $currency = "INR";
    $merchantTxnId = $Order_Id;
    $addressState = "";
    $addressCity = "";
    $addressStreet1 = "";
    $addressCountry = "";
    $addressZip = "";
    $firstName = $fname;
    $lastName = $lname;
    $phoneNumber = $_POST['monumber'];
    $email = $_POST['email'];
    $paymentMode = "";
    $issuerCode = "";
    $cardHolderName = "";
    $cardNumber = "";
    $expiryMonth = "";
    $cardType = "";
    $cvvNumber = "";
    $expiryYear = "";
    $returnUrl = corWebRoot . "/cp-thanks.php";
    $orderAmount = $Amount;
    $flag = "post";
    $data = "$vanityUrl$orderAmount$merchantTxnId$currency";
    $secSignature = generateHmacKey($data, CitrusPay::getApiKey());
    $action = CitrusPay::getCPBase() . "$vanityUrl";
    $time = time() * 1000;
    $time = number_format($time, 0, '.', '');
    /*     * ***** ICICI  alliance  implementation for self drive  ****** */
    if ($empcode == 'MYICI500') {
        $sigkey = '01ead54113da1cb978b39c1af588cf83e16c519d';
        $discountamountVal = 500;
        $orderAmount = $orderAmount;
        $alteredAmount = $orderAmount - $discountamountVal;
        $couponCode = $empcode;
        $data = "";
        $data1 = "orderAmount=$orderAmount&alteredAmount=$alteredAmount&couponCode=$couponCode";
        $hmaccouponsingnature = Zend_Crypt_Hmac::compute($sigkey, "sha1", $data1);
    } else {
        $hmaccouponsingnature = '';
    }

		/******* ICICI  alliance  implementation  for self drive ****** */
	
	
		/** ***** AMX  alliance  implementation for self drive  ****** */
        if ( $empcode == 'MYLESAXP') {
            $sigkey = '01ead54113da1cb978b39c1af588cf83e16c519d';
			$discountamountVal = 600;
            $orderAmount = $_REQUEST["totFareGross"];
            $alteredAmount = $_POST["totFare"];
			$data = "$vanityUrl$orderAmount$merchantTxnId$currency";
			$secSignature = generateHmacKey($data, CitrusPay::getApiKey());
			$couponCode = $empcode;
            $data = "";
            $data2 = "orderAmount=$orderAmount&alteredAmount=$alteredAmount&couponCode=$couponCode";
            $hmaccouponsingnature = Zend_Crypt_Hmac::compute($sigkey, "sha1", $data2);
        } else {
            $hmaccouponsingnature = '';
        }

        /******* AMX  alliance  implementation  for self drive ****** */
		
		
	
    ?>
        <div class="main">
            <div class="innerpages">
                <div class="leftside">
                    <h1>Payment</h1>
                    <input type="hidden" name="cciid" id="cciid<?php echo $i; ?>" value="<?php echo $cciid; ?>" />
                    <form id="formPaymentOptions1" name="formPaymentOptions1" method="POST" action="<?php echo $action; ?>">
                        <!--CC Avenue Parameters starts # Aamir-->
                        <!--<input type="hidden" name="Order_Id" id="Order_Id" value="<?php echo $Order_Id; ?>">
                        <input type="hidden" name="merchant_id" value="320"/>
                        <input type="hidden" name="order_id" value="<?php echo $Order_Id; ?>"/>
                        <input type="hidden" name="amount" value="1.00"/>
                        <input type="hidden" name="currency" value="INR"/>
                        <input type="hidden" name="redirect_url" value="<?php echo corWebRoot . "/ccavResponseHandler.php"; ?>"/>
                        <input type="hidden" name="cancel_url" value="<?php echo corWebRoot . "/ccavResponseHandler.php"; ?>"/>
                        <input type="hidden" name="language" value="EN"/>
                        <input type="hidden" name="billing_name" value="<?php echo $_POST["name"] ?>"/>
                        <input type="hidden" name="billing_address" value="<?php echo $addressCity; ?>"/>
                        <input type="hidden" name="billing_city" value="<?php echo $addressCity; ?>"/>
                        <input type="hidden" name="billing_state" value="<?php echo $addressState; ?>"/>
                        <input type="hidden" name="billing_zip" value="<?php echo $addressZip; ?>"/>
                        <input type="hidden" name="billing_country" value="<?php echo $addressCountry; ?>"/>
                        <input type="hidden" name="billing_tel" value="<?php echo $phoneNumber; ?>"/>
                        <input type="hidden" name="billing_email" value="<?php echo $email; ?>"/>
                        <input type="hidden" name="delivery_name" value="<?php echo ($firstName . " " . $lastName); ?>"/>
                        <input type="hidden" name="delivery_address" value="<?php echo $addressCity; ?>"/>
                        <input type="hidden" name="delivery_city" value="<?php echo $addressCity; ?>"/>
                        <input type="hidden" name="delivery_state" value="<?php echo $addressState; ?>"/>
                        <input type="hidden" name="delivery_zip" value="<?php echo $addressZip; ?>"/>
                        <input type="hidden" name="delivery_country" value="<?php echo $addressCountry; ?>"/>
                        <input type="hidden" name="delivery_tel" value="<?php echo $phoneNumber; ?>"/>
                        <input type="hidden" name="merchant_param1" value=""/>
                        <input type="hidden" name="merchant_param2" value=""/>
                        <input type="hidden" name="merchant_param3" value=""/>
                        <input type="hidden" name="merchant_param4" value=""/>
                        <input type="hidden" name="merchant_param5" value=""/>
                        <input type="hidden" name="promo_code" value=""/
                        <input type="hidden" name="customer_identifier" value=""/>
                        <input type="hidden" name="integration_type" value="iframe_normal"/>-->
                        <!--CC Avenue Parameters ends-->

                        <input name="merchantTxnId" id="Order_Id" type="hidden" value="<?php echo $merchantTxnId; ?>" />
                        <input name="addressState" type="hidden" value="<?php echo $addressState; ?>" />
                        <input name="addressCity" type="hidden" value="<?php echo $addressCity; ?>" />
                        <input name="addressStreet1" type="hidden" value="<?php echo $addressStreet1; ?>" />
                        <input name="addressCountry" type="hidden" value="<?php echo $addressCountry; ?>" />
                        <input name="addressZip" type="hidden" value="<?php echo $addressZip; ?>" />
                        <input name="firstName" type="hidden" value="<?php echo $firstName; ?>" />
                        <input name="lastName" type="hidden" value="<?php echo $lastName; ?>" />
                        <input name="phoneNumber" type="hidden" value="<?php echo $phoneNumber; ?>" />
                        <input name="email" type="hidden" value="<?php echo $email; ?>" />
                        <input name="paymentMode" type="hidden" value="<?php echo $paymentMode; ?>" />
                        <input name="issuerCode" type="hidden" value="<?php echo $issuerCode; ?>" />
                        <input name="cardHolderName" type="hidden" value="<?php echo $cardHolderName; ?>" />
                        <input name="cardNumber" type="hidden" value="<?php echo $cardNumber; ?>" />
                        <input name="expiryMonth" type="hidden" value="<?php echo $expiryMonth; ?>" />
                        <input name="cardType" type="hidden" value="<?php echo $cardType; ?>" />
                        <input name="cvvNumber" type="hidden" value="<?php echo $cvvNumber; ?>" />
                        <input name="expiryYear" type="hidden" value="<?php echo $expiryYear; ?>" />
                        <input name="returnUrl" type="hidden" value="<?php echo $returnUrl; ?>" />
                        <input name="orderAmount" type="hidden" value="<?php echo $orderAmount; ?>" />
                        <input type="hidden" name="reqtime" value="<?php echo $time; ?>" />
                        <input type="hidden" name="secSignature" value="<?php echo $secSignature; ?>" />
                        <input type="hidden" name="currency" value="<?php echo $currency; ?>" />
						
						 <input type="hidden" id="dpSignature" name="dpSignature" value="<?php echo $hmaccouponsingnature; ?>" />
					    <input type="hidden" name="couponCode" value="<?php echo $empcode; ?>" />
                        <input type="hidden" name="alteredAmount" value="<?php echo $alteredAmount; ?>" />


                        <fieldset class="payment">
                            <div class="leftfieldset">
                               <!--<input type="radio" name="rdoPayment" id="rdo1" value="1" />-->
                                <label>Pay Online</label>
                                <ul class="listarrow">
                                    <li>Using VISA, MasterCard, AMEX and Debit Cards</li>
                                    <li>Using cash cards</li>
                                </ul>
                                <p style='margin-left:30px'><br /><input type="checkbox" name="tc" id="tc" />&nbsp;I agree to the Carzonrent (India) <a class="paytc" target="_blank" href="<?php echo corWebRoot; ?>/terms-of-use.php" style="text-decoration:underline;cursor:pointer;">Terms &amp; Conditions</a></p>
                            </div>
                            <div class="rightfieldset">
                               <!--<div class="payandbook"><a onclick="javascript: _gaq.push(['_trackPageview', '/payment-options/online/<?php echo $Order_Id; ?>']);_checkPaymentSubmit(document.getElementById('formPaymentOptions1'));" href="javascript: void(0);"></a></div>-->
                                <div class="payandbook"><a onclick="javascript: _checkPaymentSubmit(document.getElementById('formPaymentOptions1'));" href="javascript: void(0);"></a></div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="border_b"></div>
                </div><!--leftside-->
                <div class="rightside">
                    <h1>Booking Summary</h1>
                    <div class="tpdiv">
                        <img src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ", "-", trim($_POST["hdCarModel"])); ?>.jpg" alt="<?php echo $_POST["hdCarModel"]; ?>" />
                        <div class="clr"></div>
                        <p><span>Car:</span>&nbsp;<?php echo $_POST["hdCarModel"]; ?></p>
    <?php
    if ($_POST["hdOriginID"] == "69") {
        ?>
                            <p>Please note that the car in Goa will be provided through a partner.</p>
        <?php
    }
    ?>
                        <p><span>From:</span> <?php echo $_POST["hdOriginName"] ?></p>
                        <p><span>Service:</span> <?php echo $_POST["hdTourtype"] ?> </p>
                        <p><span>Destination:</span> <?php echo $_POST["hdDestinationName"] ?></p>
                        <?php
                        $pickDate = date_create($_POST["hdPickdate"]);
                        $dropDate = date_create($_POST["hdDropdate"]);
                        ?>
                        <p><span>Pickup Date:</span> <?php echo $pickDate->format('D d-M, Y'); ?></p>
                        <p><span>Drop Date:</span> <?php echo $dropDate->format('D d-M, Y'); ?></p>
                        <p><span>Pickup Time:</span> <?php if (isset($_POST["picktime"])) {
                        echo $_POST["picktime"];
                    } else {
                        echo $_POST["tHourP"] . $_POST["tMinP"];
                    } ?> Hrs</p>
                        <p><span>Drop Time:</span> <?php if (isset($_POST["droptime"])) {
                        echo $_POST["droptime"];
                    } else {
                        echo $_POST["tHourD"] . $_POST["tMinD"];
                    } ?> Hrs</p>
                        <p><span>Pickup Location:</span> <?php echo $_POST["subLocName"] ?></p>	
                        <h3>Total Fare<br />

                            <?php
                            if ($_REQUEST['empcode'] == 'PayBack') {
                                ?>
                                <span id="spPay">Rs <?php echo intval($orignalBilling); ?>/-<span style="font-size:11px;margin:10px 35px 0px 0px;display:block;width:auto;float:right;font-weight:normal;color:#666666;">(Including VAT)</span></span></h3>
                            <?php
                        } else {
                            ?>
                            <span id="spPay">Rs <?php echo intval($Amount); ?>/-<span style="font-size:11px;margin:10px 35px 0px 0px;display:block;width:auto;float:right;font-weight:normal;color:#666666;">(Including VAT)</span></span></h3>
                            <?php
                        }
                        ?>


                    </div><!--tpdiv-->
                    <div class="tpdiv">
    <?php
    if ($_POST["hdTourtype"] == "Selfdrive") {
        ?>
                            <ul>
                                <li>Minimum Billing: Rs <?php echo $basicAmt; ?>/-</li>
                                <li>Additional Services Cost: Rs <?php echo $addSrvAmt; ?>/-</li>
                                <li>Sub Location Cost: Rs <?php echo $subLocCost; ?>/-</li>
                                <li>Convenience Charge: Rs <?php echo $cnvCharge; ?>/-</li>



        <?php
        if ($_REQUEST['empcode'] == 'PayBack') {
            ?>
                                    <li>Payback Discount Amount: Rs <span id="disA"><?php echo $disAPayback; ?></span>/-</li>
                                    <li>Sub Total: Rs <?php echo (($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge)); ?>/-</li>
                                    <li>VAT (@<?php echo number_format($_POST["vat"], 3); ?>%): Rs <?php echo $vatAmt; ?>/-</li>
                                    <li>Total Fare: Rs <?php echo $totPayable; ?>/-</li>
                                    <?php
                                } else {
                                    ?>
                                    <li>Discount Amount: Rs <span id="disA"><?php echo $disA; ?></span>/-</li>
                                    <li>Sub Total: Rs <?php echo ($subtAmount); ?>/-</li>
                                    <li>VAT (@<?php echo number_format($_POST["vat"], 3); ?>%): Rs <?php echo $vatAmt; ?>/-</li>
                                    <li>Total Fare: Rs <?php echo $totPayable; ?>/-</li>
                                    <?php
                                }
                                ?>





        <!--<li>Discount Amount: Rs <span id="disA"><?php echo $disA; ?></span>/-</li>
        <li>Sub Total: Rs <?php echo (($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) - $disA); ?>/-</li>
        <li>VAT (@<?php echo number_format($_POST["vat"], 3); ?>%): Rs <?php echo $vatAmt; ?>/-</li>
        <li>Total Fare: Rs <?php echo $totPayable; ?>/-</li>-->
        <?php
        if (intval($_POST["OriginalAmt"]) > intval($_POST["BasicAmt"])) {
            ?>
                                    <li>Discount Amount (On base fare): Rs <?php echo (intval($_POST["OriginalAmt"]) - intval($_POST["BasicAmt"])); ?>/-</li>
            <?php
        }
        if (intval($_POST["hdOriginID"]) != "69") {
            ?>
                                    <li>Refundable Security Deposit  (Pre Auth from card): Rs <?php echo $_POST["secDeposit"]; ?> (Mastercard/Visa/Amex)</li>
                                    <?php
                                } else {
                                    ?>
                                    <li>The billing cycle starts from 8am each day</li>
                                    <li>Refundable Security Deposit: Rs <?php echo $_POST["secDeposit"]; ?> (To be paid in cash before the start of the journey)</li>
                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                    <?php
                                }
                                ?>
                            </ul>
							 <p><span>Eligibilty</span></p>
                            <ul>
                                <li>The minimum age required to rent a Myles-Nano is 18yrs. However, it is 23yrs for all other cars. </li>
                                <li>The identification documents needs to be in the name of the person who is booking the car.</li>
                            </ul>
        <?php
    }
    if (trim($_POST["chkPkgType"]) == "Hourly") {
        ?>
                            <p><span>Includes</span></p>
                            <ul>
                                <li>KMs Included: <?php echo $_POST["KMIncluded"]; ?></li>
                                <li>Extra KM Charge: Rs. <?php echo $_POST["ExtraKMRate"]; ?>/- per KM</li>
                            </ul>
                            <?php
                        }
                        if ($_POST["empcode"] == "PayBack") {
                            ?>
                            <p><span>PAYBACK Discount Details</span></p>
                            <ul>
                                <li>Original Billing: Rs <?php echo intval($orignalBilling); ?>/-</li>
                                <li>PAYBACK Discount Amount: Rs <?php echo intval($_POST["discountAmt"]); ?>/-</li>
                                <li>PAYBACK Points Deducted: <?php echo intval($_POST["discountAmt"] * 4); ?></li>
                                <li>Final Billing: Rs <?php echo (intval($orignalBilling) - intval($_POST["discountAmt"])); ?>/-</li>
                            </ul>
                            <?php
                        }
                        ?>
                        <p><span>Mandatory Documents(Original)</span></p>
    <?php
    if (intval($_POST["hdOriginID"]) != "69") {
        ?>
                            <ul>
                                <li>Passport / Voter ID card</li>
                                <li>Driving License</li>
                                <li>Credit card</li>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <ul>
                                <li>Passport / Voter ID card</li>
                                <li>Driving License</li>
                                <li>Any of the following has to be submitted in original as an identity proof.
                                    <ol type="1">
                                        <li>Adhaar card</li>
                                        <li>Pan card</li>
                                        <li>Voter ID card</li>
                                    </ol>
                                </li>
                            </ul>
        <?php
    }
    ?>
                    </div>
                    <div class="tpdiv">
                        <p><span>Rental Refund Policy</span></p>
                        <ul>
                            <li>If the booking is cancelled 24 Hrs prior to the pickup time - 100% refund will  be provided</li>
                            <li>If the booking is cancelled within 24 Hrs prior to the pickup time - 1 day rental will be charged and rest will be refunded</li>
                            <li>If you don't turn up to pick-up - NO refund will be made</li>
                        </ul>
                    </div>
                    <div class="tpdiv">
                        <div class="map">
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
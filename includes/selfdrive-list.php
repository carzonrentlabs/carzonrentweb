<?php date_default_timezone_set("Asia/Kolkata");
 
header("Location:www.carzonrent.com");
 die;
 
 $cor = new COR();
 $myXML = new CORXMLList();

 
 
 
 ?>
<script>


    $(function () {

// The height of the content block when it's not expanded
        var adjustheight = 31;
// The "more" link text
        var moreText = "+  More";
// The "less" link text
        var lessText = "- Less";

// Sets the .more-block div to the specified height and hides any content that overflows
        $(".more-less .more-block").css('height', adjustheight).css('overflow', 'hidden');

// The section added to the bottom of the "more-less" div


        $("div.adjust").text(moreText);

        $("div.adjust").toggle(function () {
            $(this).parents("div:first").find(".more-block").css('height', 'auto').css('overflow', 'visible');
            // Hide the [...] when expanded
            $(this).parents("div:first").find("p.continued").css('display', 'none');
            $(this).text(lessText);
        }, function () {
            $(this).parents("div:first").find(".more-block").css('height', adjustheight).css('overflow', 'hidden');
            $(this).parents("div:first").find("p.continued").css('display', 'block');
            $(this).text(moreText);
        });
    });
</script>
<div class="tbbinginr">
    <div id="country1" class="tabcontent">
        <div class="selectcartp">
            <div class="main">
                <div style="float: left;width:1130px;">
                    <h2>You Searched For</h2>
                    <div class="wd136">
                        <h3>City</h3>
                        <p><?php echo $orgName; ?></p>
                        <input type="hidden" name="cityid" id="cityid" value="<?php echo $_REQUEST['hdOriginID']; ?>">
                        <input type="hidden" name="cityname" id="cityname" value="<?php echo $orgName; ?>">
                        <input type="hidden" name="rentaltype" id="rentaltype" value="<?php echo $_REQUEST['chkPkgType']; ?>">
                        <input type="hidden" name="webroot" value="<?php echo corWebRoot; ?>" id="webroot">
                    </div>
                    <div class="wd136">
                        <h3>Rental Type</h3>
                        <p><?php echo $_REQUEST['chkPkgType']; ?></p>
                    </div>
                    <div class="wd136" style="width:200px !important;">
                        <h3>Pickup Date</h3>
                        <p><?php echo $pickDate->format('D d-M, Y'); ?><?php echo " " . $_REQUEST["tHourP"] . $_REQUEST["tMinP"] . "Hrs"; ?></p>
                    </div>
                    <?php
                    $pickDate = date_create(date('Y-m-d', strtotime($pDate)));
                    if ($_REQUEST["chkPkgType"] == "Daily" || $_REQUEST["chkPkgType"] == "Weekly" || $_REQUEST["chkPkgType"] == "Monthly") {
                        ?>
                        <div class="wd136">
                            <h3>Duration</h3>
                            <p><?php echo $interval; ?> Days</p>
                        </div>
                        <?php
                    } elseif ($_REQUEST["chkPkgType"] == "Hourly") {
                        ?>
                        <div class="wd136">
                            <h3>Duration</h3>
                            <p><?php
                                if (isset($_REQUEST["nHoursJS"]) && $_REQUEST["nHoursJS"] != "") {
                                    echo $_REQUEST["nHoursJS"] . " Hrs";
                                } else {
                                    echo $_REQUEST["nHours"] . " Hrs";
                                }
                                ?></p>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    $ladakhOffer = false;

                    $arrDelNCRID = array(2, 3, 11, 82);
                    $arrDelNCRName = array("Delhi", "Gurgaon", "Noida", "Ghaziabad");
                    $isDelNCR = false;
                    for ($i = 0; $i < count($arrDelNCRID); $i++) {
                        if ($arrDelNCRID[$i] == $_REQUEST["hdOriginID"]) {
                            $isDelNCR = true;
                            break;
                        }
                    }
                    if ($isDelNCR) {
                        $showDelNCR = "";
                        for ($i = 0; $i < count($arrDelNCRID); $i++) {
                            if ($arrDelNCRID[$i] != $_REQUEST["hdOriginID"])
                                $showDelNCR .= $showDelNCR == "" ? "<a href='javascript: void(0);' onclick=\"javascript: _resetSearch(" . $arrDelNCRID[$i] . ", '" . $arrDelNCRName[$i] . "');\">" . $arrDelNCRName[$i] . "</a>" : " | " . "<a href='javascript: void(0);' onclick=\"javascript: _resetSearch(" . $arrDelNCRID[$i] . ", '" . $arrDelNCRName[$i] . "');\">" . $arrDelNCRName[$i] . "</a>";
                        }
                        ?>
                        <script type="text/javascript">
                            function _resetSearch(id, nm) {
                                //alert(id);alert(nm);
                                document.getElementById('hdOriginIDSF').value = id;
                                document.getElementById('hdDestinationIDSF').value = id;
                                document.getElementById('hdOriginNameSF').value = nm;
                                document.getElementById('hdDestinationNameSF').value = nm;
                                document.getElementById('frmDelNCR').action = webRoot + "/search-result.php";
                                document.getElementById('frmDelNCR').submit();
                            }
                        </script>
                        <div class="wd136" style="width:325px !important;">
                            <form action="./" method="GET" name="frmDelNCR" id="frmDelNCR">
                                <input type="hidden" name="hdToday" id="hdToday" value="<?php echo $_REQUEST["hdToday"]; ?>" />
                                <input type="hidden" name="hdOriginName" id="hdOriginNameSF" value="<?php echo $_REQUEST["hdOriginName"]; ?>" />
                                <input type="hidden" name="hdOriginID" id="hdOriginIDSF" value="<?php echo $_REQUEST["hdOriginID"]; ?>" />
                                <input type="hidden" name="hdDestinationName" id="hdDestinationNameSF" value="<?php echo $_REQUEST["hdDestinationName"]; ?>" />
                                <input type="hidden" name="hdDestinationID" id="hdDestinationIDSF" value="<?php echo $_REQUEST["hdDestinationID"]; ?>" />
                                <input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" value="<?php echo $_REQUEST["hdTourtype"]; ?>" />
                                <input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo $_REQUEST["bookTime"]; ?>" />
                                <input type="hidden" name="userTime" id="userTimeSF" value="<?php echo $_REQUEST["userTime"]; ?>" />
                                <input type="hidden" name="onlyE2C" id="onlyE2C" value="0" />
                                <input type="hidden" name="chkPkgType" id="chkPkgType" value="<?php echo $_REQUEST["chkPkgType"]; ?>" />
                                <input type="hidden" name="tHourP" id="tHourP" value="<?php echo $_REQUEST["tHourP"]; ?>" />
                                <input type="hidden" name="tMinP" id="tMinP" value="<?php echo $_REQUEST["tMinP"]; ?>" />
                                <input type="hidden" name="tHourD" id="tHourD" value="<?php echo $_REQUEST["tHourD"]; ?>" />
                                <input type="hidden" name="tMinD" id="tMinD" value="<?php echo $_REQUEST["tMinD"]; ?>" />
                                <input type="hidden" name="pickdate" id="pickdate" value="<?php echo $_REQUEST["pickdate"]; ?>" />
                                <input type="hidden" name="dropdate" id="dropdate" value="<?php echo $_REQUEST["dropdate"]; ?>" />
                                <input type="hidden" name="customPkg" id="customPkg" value="<?php echo $_REQUEST["customPkg"]; ?>" />
                                <input type="hidden" name="nHours" id="nHours1" value="<?php echo $_REQUEST["nHours"] ?>" />
                                <h3>Looking for a car in</h3>
                                <?php echo $showDelNCR; ?>
                            </form>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="main">
            <div style="float: left;width:100%;display: none;" id="divMS">
                <div id="closeimg"><a href="javascript:void(0);" onclick="javascript: document.getElementById('divMS').style.display = 'none'">X</a></div>
                <div style='float: left;width: 100%;margin-bottom:20px;'>
				 <span id="goanote" class="goa_note" style="color:#DB4626;top:63px;"></span>
                    <span style="float:left;">
                        <!--Form for Modify Search by Shariq on 05 Jan, 2015-->
                        <form action="./" method="GET" name="frmModifySrch" id="frmModifySrch">
                            <input type="hidden" name="hdToday" id="hdTodayMS" value="<?php echo $_REQUEST["hdToday"]; ?>" />
                            <input type="hidden" name="hdOriginName" id="hdOriginNameSFMS" value="<?php echo $_REQUEST["hdOriginName"]; ?>" />
                            <input type="hidden" name="hdOriginID" id="hdOriginIDSFMS" value="<?php echo $_REQUEST["hdOriginID"]; ?>" />
                            <input type="hidden" name="hdDestinationName" id="hdDestinationNameSFMS" value="<?php echo $_REQUEST["hdDestinationName"]; ?>" />
                            <input type="hidden" name="hdDestinationID" id="hdDestinationIDSFMS" value="<?php echo $_REQUEST["hdDestinationID"]; ?>" />
                            <input type="hidden" name="hdTourtype" id="hdTourtypeSF" value="Selfdrive" value="<?php echo $_REQUEST["hdTourtype"]; ?>" />
                            <input type="hidden" name="bookTime" id="bookTimeSF" value="<?php echo $_REQUEST["bookTime"]; ?>" />
                            <input type="hidden" name="userTime" id="userTimeSF" value="<?php echo $_REQUEST["userTime"]; ?>" />
                            <input type="hidden" name="onlyE2C" id="onlyE2C" value="0" />
                            <input type="hidden" name="chkPkgType" id="chkPkgTypeMS" value="<?php echo $_REQUEST["chkPkgType"]; ?>" />
                            <input type="hidden" name="tHourP" id="tHourPMS" value="<?php echo $_REQUEST["tHourP"]; ?>" />
                            <input type="hidden" name="tMinP" id="tMinPMS" value="<?php echo $_REQUEST["tMinP"]; ?>" />
                            <input type="hidden" name="tHourD" id="tHourDMS" value="<?php echo $_REQUEST["tHourD"]; ?>" />
                            <input type="hidden" name="tMinD" id="tMinDMS" value="<?php echo $_REQUEST["tMinD"]; ?>" />
                            <input type="hidden" name="pickdate" id="pickdateMS" value="<?php echo $_REQUEST["pickdate"]; ?>" />
                            <input type="hidden" name="dropdate" id="dropdateMS" value="<?php echo $_REQUEST["dropdate"]; ?>" />
                            <input type="hidden" name="customPkg" id="customPkgMS" value="<?php echo $_REQUEST["customPkg"]; ?>" />
                            <input type="hidden" name="nHoursJS" id="nHoursJS" value="<?php echo $_REQUEST["nHours"] ?>" />

                            <?php
                             $resCities = $cor->_CORFetchCities();
                            $arrCities = $myXML->xml2ary($resCities->{'fetchCitiesResult'}->{'any'});
                            
//                            $db = new MySqlConnection(CONNSTRING);
//                            $db->open();
//                            $sql = "SELECT * FROM cor_location order by CityName asc";
//                            $r = $db->query("query", $sql);
//                            if (!array_key_exists("response", $r)) {
                                ?>
                                <select name="ddlOrigin" id="ddlOriginSF" style='padding: 7px;font-family: 13px;margin: 10px 7px 0 0;float: left;' onchange="javascript: _setOriginMSmodify(this.id, 's');">';
                                    <?php
                                    foreach($arrCities as $arrCity)
				{

                                        if ($arrCity['CityID'] == $_REQUEST["hdOriginID"]) {
                                            $selCityId = $arrCity['CityID'];
                                            $selCityName = $arrCity['CityName'];
                                            ?>
                                            <option value="<?php echo $arrCity['CityID']; ?>" selected="selected"><?php echo $arrCity['CityName']; ?></option>
                                            <script type="text/javascript" language="javascript">
                                                document.getElementById('hdOriginIDSF').value = '<?php echo $arrCity['CityID']; ?>';
                                                document.getElementById('hdOriginNameSF').value = '<?php echo str_ireplace(" (excluding NCR regions)", "", $arrCity['CityName']); ?>';
                                            </script>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $arrCity['CityID']; ?>"><?php echo $arrCity['CityName']; ?></option>
                                        <?php }
                                        ?>

                                    <?php } ?>

                                </select>
                                <?php
                            //}
                            ?>
                    </span>

                    <span style="float:left;border: 1px solid #a2a2a2;margin: 10px 7px 0 0">
                        <input id="inputFieldSF1" class="from picktime" type="text" value="<?php echo $pickDate->format('d M, Y'); ?><?php echo " " . $_REQUEST["tHourP"] . ":" . $_REQUEST["tMinP"]; ?>" readonly="readonly" placeholder="Pick Up Date and Time" onchange="javascript: _setDropDateMylesMS('inputFieldSF1', 'inputFieldSF2');">
                    </span>
                    <?php
                    ?>
                    <span style="float:left;border: 1px solid #a2a2a2;margin: 10px 7px 0 0;<?php
                    if ($_REQUEST["chkPkgType"] != 'Daily') {
                        echo " display:none;";
                    }
                    ?>" id="dailyBlock">
                        <input type="text" class="from picktime" id="inputFieldSF2" placeholder="Drop Off Date and Time" readonly="readonly" value="<?php echo $dropDate->format('d M, Y'); ?> <?php echo $_REQUEST["tHourD"]; ?>:<?php echo $_REQUEST["tMinD"]; ?>" onchange="javascript: _setDropOffDateMS(this.id);"/>
                    </span>
                    <?php ?>

                    <span style="float:left;border: 1px solid #a2a2a2;margin: 10px 7px 0 0;<?php
                    if ($_REQUEST["chkPkgType"] != 'Hourly') {
                        echo " display:none;";
                    }
                    ?> " id="hourlyBlock">

                        <select class="fromdd2" name="nHours" id="nHours" onchange="javascript: _setHoursJS(this.value);">
                            <?php
                            for ($i = 2; $i <= 23; $i++) {
                                //if (isset($_REQUEST["nHoursJS"]) && $_REQUEST["nHoursJS"] == $i) {
                                ?>
                                                                            <!--<option value="<?php //echo $i;          ?>"selected='selected'><?php //echo $i;          ?> HRS</option>-->
                                <?php
                                //} 
                                //else
                                if (isset($_REQUEST["nHours"]) && $_REQUEST["nHours"] == $i) {
                                    ?>
                                    <option value="<?php echo $i; ?>"selected='selected'><?php echo $i; ?> HRS</option>  

                                <?php } else {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?> HRS</option> 
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </span>
                    <span style="float:left;border: 1px solid #a2a2a2;margin:10px 7px 0 0;<?php
                    if ($_REQUEST["chkPkgType"] != 'Weekly') {
                        echo " display:none;";
                    }
                    ?> " id="weeklyBlock">
                        <select class="fromdd2" name="nWeeks" id="nWeeks" onchange="javascript: _setdropdatesmodify('week', 'inputFieldSF1', 'inputFieldSF2');">';

                            <?php
                            $db = new MySqlConnection(CONNSTRING);
                            $db->open();
                            $sql = "SELECT * FROM cor_week_search WHERE status = '1' && is_deleted='0'";
                            $r = $db->query("query", $sql);
                            if (!array_key_exists("response", $r)) {
                                ?>
                                <?php
                                for ($i = 0; $i < count($r); $i++) {
                                    $days = $interval / 7;
                                    if ($r[$i]["id"] == $days) {
                                        $sel = "selected";
                                    } else {
                                        $sel = '';
                                    }
                                    ?>
                                    <option value="<?php echo $r[$i]["id"] ?>" <?php echo $sel; ?>><?php echo $r[$i]["name"] ?></option>
                                <?php } ?>


                                <?php
                            }
                            ?>
                        </select></span>
                    <span style="float:left;border: 1px solid #a2a2a2;margin:10px 7px 0 0;<?php
                    if ($_REQUEST["chkPkgType"] != 'Monthly') {
                        echo " display:none;";
                    }
                    ?>" id="monthlyBlock">
                        <select class="fromdd2" name="nMonths" id="nMonths" onchange="javascript: _setdropdatesmodify('month', 'inputFieldSF1', 'inputFieldSF2');">';

                            <?php
                            $db = new MySqlConnection(CONNSTRING);
                            $db->open();
                            $sql = "SELECT * FROM cor_month_search WHERE status = '1' && is_deleted='0'";
                            $r = $db->query("query", $sql);
                            if (!array_key_exists("response", $r)) {
                                ?>

                                <?php
                                for ($i = 0; $i < count($r); $i++) {
                                    $days = $interval / 30;
                                    if ($r[$i]["id"] == $days) {
                                        $sel = "selected";
                                    } else {
                                        $sel = '';
                                    }
                                    ?>
                                    <option value="<?php echo $r[$i]["id"] ?>" <?php echo $sel; ?>><?php echo $r[$i]["name"] ?></option>
                                <?php } ?>

                                <?php
                            }
                            ?>
                        </select>
                    </span>




                    <span>
                        <span id="package"></span>
                    </span>
                    <div style="float:left;margin-top: 10px;">
                        <input type="hidden" name="curdatetime" id="curdatetime" value="" />
                        <input id="btnModifysrch" class="btn" type="button" onclick="javascript: return _validateSFMS();" value="Submit" name="btnModifysrch">
                    </div>
                    <input type="hidden" name="hdpickupdateforsearch" id="hdpickupdateforsearch" value='<?php
                    $picd = date("Y-m-d", strtotime($_REQUEST['pickdate'])) . " " . $_REQUEST['tHourP'] . ":" . $_REQUEST['tMinP'] . ":00";
                    echo strtotime($picd) * 1000;
                    ?>' />
                    <input type="hidden" name="hddropoffforsearch" id="hddropoffforsearch" value='<?php
                    $drp = date("Y-m-d", strtotime($_REQUEST['dropdate'])) . " " . $_REQUEST['tHourD'] . ":" . $_REQUEST['tMinD'] . ":00";
                    echo strtotime($drp) * 1000
                    ?>' />
                    </form>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var auto_refresh = setInterval(
                                function ()
                                {
                                    $(document).ready(function () {
                                        var d = new Date();
                                        var n = d.getTime();

                                        $("#curdatetime").val(d);
                                        if ($("#nextcurdatetime").length > 0)
                                            $("#nextcurdatetime").val(d);
                                    });
                                    $('#load').load('reload.php').fadeIn("slow");
                                }, 10000);

                    }); // refresh every 10000 milliseconds

                    function _modifySrch(id) {
//			 var cMode = document.getElementById('divMS').style.display;
//			 document.getElementById('divMS').style.display = cMode == 'none' ? 'block' : 'none';
                        //alert(id);
                        var cityName = document.getElementById('cityname').value;
                        var Cityid = document.getElementById('cityid').value;
                        var rntltype = document.getElementById('rentaltype').value;
                        var clientid = 2205;
                        var dataString = 'Cityid=' + Cityid + '&clientid=' + clientid + '&rentaltype=' + rntltype;
                        $.ajax({type: "POST", url: "<?php echo corWebRoot ?>/getavailablity.php",
                            data: dataString, success: function (response) {
                                var txt = response;
                                if (txt)
                                {
                                    $("#package").html(txt);
                                }
                            }});

                        var cMode = document.getElementById('divMS').style.display;
                        document.getElementById('divMS').style.display = cMode == 'none' ? 'block' : 'none';

                    }


                </script>
            </div>
            <?php
            $isOutStation = TRUE;
            $cor = new COR();
            $myXML = new CORXMLList();
            ?>
            <div class="SFilter">
                <div class="trtpSF">
                    <label class="frst">Pickup Location</label>
                </div>
                <div style="clear:both;"></div>
                <div class="repeatdiv">
                    <?php
                    //print_r($orgIDs);
                    for ($o = 0; $o < count($orgIDs); $o++) {
                        $res = $cor->_CORSelfDriveGetSubLocationsName(array("CityID" => $orgIDs[$o]));
                        $arrSL = $myXML->xml2ary($res);
                        if (count($arrSL) > 0) {
                            $isSLName = array();
                            foreach ($arrSL as $k => $v)
                                $isSLName[$k] = $v['SubLocationName'];
                            array_multisort($isSLName, SORT_ASC, $arrSL);
                            ?>
                            <div class="typefcr">
                                <table cellpadding="0" cellpadding="0" border="0">
                                    <tr>
                                        <td><input type="checkbox" value="<?php echo $orgIDs[$o]; ?>" onclick="javascript: _allSubLoc(this, <?php echo $orgIDs[$o]; ?>);
                                                        _getSRSFSubLoc(
                                            <?php echo $orgIDs[$o]; ?>,
                                                                '<?php echo $orgNames[0]; ?>',
                                                                '<?php echo $pickDate->format('Y-m-d'); ?>',
                                                                '<?php echo $dropDate->format('Y-m-d'); ?>',
                                                                '<?php echo $_REQUEST["tHourP"]; ?>',
                                                                '<?php echo $_REQUEST["tMinP"]; ?>',
                                                                '<?php echo $dropDate->format('H'); ?>',
                                                                '<?php echo $dropDate->format('i'); ?>',
                                                                this,
                                                                '<?php echo $_REQUEST["chkPkgType"]; ?>',
                                                                '<?php
                                            if ($_REQUEST["chkPkgType"] == "Hourly") {
                                                echo $_REQUEST["nHours"];
                                            } else {
                                                echo $interval;
                                            }
                                            ?>',
                                                                '<?php echo $_REQUEST["customPkg"]; ?>',
                                                                '<?php echo $_REQUEST["bookTime"]; ?>',
                                                                '<?php echo $_REQUEST["curdatetime"]; ?>');" 
                                                   id="chkAllSLoc<?php echo $orgIDs[$o]; ?>" name="chkAllSLoc" checked="checked" /></td>
                                            <?php
                                            if ($orgIDs[$o] == 2) {
                                                ?>
                                            <td>&nbsp;Delhi</td>
                                            <?php
                                        } elseif ($orgIDs[$o] == 3) {
                                            ?>
                                            <td>&nbsp;Gurgaon</td>
                                            <?php
                                        } elseif ($orgIDs[$o] == 11) {
                                            ?>
                                            <td>&nbsp;Noida</td>
                                            <?php
                                        } else {
                                            ?>
                                            <td>&nbsp;All Location</td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                </table>
                            </div>
                            <?php
                            for ($i = 0; $i < count($arrSL); $i++) {
                                ?>
                                <div class="typefcr">
                                    <table cellpadding="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="padding-left:10px;vertical-align: top;"><input type="checkbox" value="<?php echo $arrSL[$i]["SubLocationID"]; ?>" name="chkSLoc[]" id="chkSLoc-<?php echo $orgIDs[$o]; ?>-<?php echo $i; ?>" onclick="javascript: _getSRSFSubLoc(<?php echo $orgIDs[$o]; ?>, '<?php echo $orgNames[0]; ?>', '<?php echo $pickDate->format('Y-m-d'); ?>', '<?php echo $dropDate->format('Y-m-d'); ?>', '<?php echo $_REQUEST["tHourP"]; ?>', '<?php echo $_REQUEST["tMinP"]; ?>', '<?php echo $dropDate->format('H'); ?>', '<?php echo $dropDate->format('i'); ?>', this, '<?php echo $_REQUEST["chkPkgType"]; ?>', '<?php
                                                if ($_REQUEST["chkPkgType"] == "Hourly") {
                                                    echo $_REQUEST["nHours"];
                                                } else {
                                                    echo $interval;
                                                }
                                                ?>', <?php echo $_REQUEST["customPkg"]; ?>, '<?php echo $_REQUEST["curdatetime"]; ?>')" checked="checked" /></td>
                                            <td style="text-align:left;"><?php echo $arrSL[$i]["SubLocationName"]; ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <?php
                            }
                        }
                        unset($arrSL);
                    }
                    ?>	
                </div><!--repeatdiv-->
            </div><!--SFilter-->
            <div class="selectcarmidSF" id="srsf">
                <div class="trtp">
                    <label class="frst">Type of Car</label>
                    <label class="snd">Seating Capacity</label>
                    <label class="lst">Price</label>
                </div>
                <?php
				
				if($_REQUEST['hdOriginID']==69)
                {
                    echo "Due to technical problems, we are not accepting bookings for Goa right now.";
                    die;
                }
				
				
                $arr = array();

                for ($o = 0; $o < count($orgIDs); $o++) {
                    $pkgt = $_REQUEST["chkPkgType"];
                    if (trim($_REQUEST["chkPkgType"]) == "Daily" || trim($_REQUEST["chkPkgType"]) == "Weekly" || trim($_REQUEST["chkPkgType"]) == "Monthly") {
                        //if($interval < 30)
                        //$pkgt = "Daily";
                        //else
                        //$pkgt = "Monthly";
                        $res = $cor->_CORSelfDriveGetCabs(array("CityId" => $orgIDs[$o], "fromDate" => $pickDate->format('Y-m-d'), "DateIn" => $dropDate->format('Y-m-d'), "PickupTime" => $_REQUEST["tHourP"] . $_REQUEST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i'), "SubLocation" => "0", "Duration" => $interval, "PkgType" => $pkgt, "CustomPkgYN" => $_REQUEST["customPkg"]));
                    } elseif (trim($_REQUEST["chkPkgType"]) == "Hourly")
                        $res = $cor->_CORSelfDriveGetCabs(array("CityId" => $orgIDs[$o], "fromDate" => $pickDate->format('Y-m-d'), "DateIn" => $dropDate->format('Y-m-d'), "PickupTime" => $_REQUEST["tHourP"] . $_REQUEST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i'), "SubLocation" => "0", "Duration" => $_REQUEST["nHours"], "PkgType" => $_REQUEST["chkPkgType"], "CustomPkgYN" => $_REQUEST["customPkg"]));
                    $fleets = $myXML->xml2ary($res);

                    for ($f = 0; $f < count($fleets); $f++)
                        $fleets[$f]["CityID"] = $orgIDs[$o];
                    $arr = array_merge($arr, $fleets);
                }
                //echo "<!--<pre>";
                //print_r($arr);
                //echo "</pre>-->";
                $dayFare = array();
                $isAvail = array();
                foreach ($arr as $k => $v) {
                    $dayFare[$k] = $v['PkgRate'];
                    $isAvail[$k] = $v['IsAvailable'];
                }
                array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
                $arrSL = array();
                $checkerfornon = 0;
                for ($i = 0; $i < count($arr); $i++) {
                    $isSLF = 1;
                    $isShow = false;
                    if (!isset($_REQUEST["onlyE2C"]) || $_REQUEST["onlyE2C"] == 0)
                        $isShow = true;
                    else {
                        if ($arr[$i]["CarCatID"] == 21) {
                            $isShow = true;
                        }
                    }
                    $btnid = 1;
                    $passCount = $arr[$i]["SeatingCapacity"];
                    $totAmount = $arr[$i]["BasicAmt"];
                    $vat = $arr[$i]["VatRate"];
                    $carModel = "";
                    $carCatgName = "";
                    if ($arr[$i]["CarCatName"] == '4 Wheel Drive')
                        $carCatgName = "SUV";
                    else
                        $carCatgName = $arr[$i]["CarCatName"];
                    $carModel = $arr[$i]["model"];
                    $secDeposit = $arr[$i]["DepositeAmt"];
                    $CarDetail = "";
                    $CarMake = "";
                    if (intval($_REQUEST["hdOriginID"]) != "69") {
                        if (strtolower(trim($carModel)) == "toyota innova") {
                            $CarDetail = trim($carModel) . " - GX";
                            $CarMake = "<small>Make - 2012</small>";
                        } else if (strtolower(trim($carModel)) == "maruti swift") {
                            $CarDetail = trim($carModel) . " - VDI";
                            $CarMake = "<small>Make - 2012</small>";
                        } else if (strtolower(trim($carModel)) == "toyota etios") {
                            $CarDetail = trim($carModel) . " - GD";
                            $CarMake = "<small>Make - 2012</small>";
                        } else if (strtolower(trim($carModel)) == "mahindra xuv-500") {
                            $CarDetail = trim($carModel) . " - W6";
                            $CarMake = "<small>Make - 2012</small>";
                        } else if (strtolower(trim($carModel)) == "volkswagen polo" || strtolower(trim($carModel)) == "maruti ertiga" || strtolower(trim($carModel)) == "volkswagen vento" || strtolower(trim($carModel)) == "nissan sunny" || strtolower(trim($carModel)) == "ford endeavour" || strtolower(trim($carModel)) == "toyota fortuner") {
                            $CarDetail = trim($carModel);
                            $CarMake = "<small>Make - 2013</small>";
                        } else if (strtolower(trim($carModel)) == "mahindra e2o") {
                            $CarDetail = trim($carModel) . "<br />" . "<small><b>Electric Car</b></small>";
                            $CarMake = "<small>Make - 2013</small>";
                        } else
                            $CarDetail = trim($carModel);
                    } else {
                        $CarDetail = trim($carModel);
                    }
                    if ($isShow) {
                        if ($carCatgName == "Super")
                            $carCatgName = "Super-Luxury";
                    }
                    $ladakhOfferShow = false;
                    //    if($ladakhOffer){
                    //	if($arr[$i]["ModelID"] == 30 || $arr[$i]["ModelID"] == 45 || $arr[$i]["ModelID"] == 31 || $arr[$i]["ModelID"] == 91 || $arr[$i]["ModelID"] == 32 || $arr[$i]["ModelID"] == 92)
                    //	$ladakhOfferShow = true;
                    //	
                    //    }
                    $totalfare1 = intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));
                    $basicamount1 = ceil($arr[$i]["BasicAmt"]);
                    $res1 = $cor->_CORSelfDriveVariableFlatOffer(array('PromotionCode' => $arr[$i]['scheme'], "CityId" => $orgIDs[0]));
                    $fleets1 = $myXML->xml2ary($res1);


                    $schemestartdatearray = explode('T', $fleets1[0]['StartDate']);
                    $schemeexpirydatearray = explode('T', $fleets1[0]['ExpiryDate']);

                    $pickup_date = date("Y-m-d", strtotime($_REQUEST['pickdate'])) . ' ' . $_REQUEST['tHourP'] . ':' . $_REQUEST['tMinP'] . ':00';
                    $pickup_date = strtotime($pickup_date);
                    $dropoff_date = date("Y-m-d", strtotime($_REQUEST['dropdate'])) . ' ' . $_REQUEST['tHourD'] . ':' . $_REQUEST['tMinD'] . ':00';
                    $dropoff_date = strtotime($dropoff_date);
                    $timediff = $dropoff_date - $pickup_date;

                    $timediffhour = $timediff / (60 * 60);
                    $timediffday = intval($timediffhour / 24);
                    $remaininghour = $timediffhour % 24;
                    ///
                    $timeFrom = ($_REQUEST['tHourP'] * 60) + intval($_REQUEST['tMinP']);
                    $timeTo = ($_REQUEST['tHourD'] * 60) + intval($_REQUEST['tMinD']);

                    //    


                    $scheme_pickup_date = date_create($_REQUEST['pickdate'])->format("Y-m-d");
                    $scheme_pickup_date = new DateTime($scheme_pickup_date);

                    $scheme_dropoff_date = date_create($_REQUEST['dropdate'])->format("Y-m-d");
                    $scheme_dropoff_date = new DateTime($scheme_dropoff_date);

                    $scheme_start_date = date_create($schemestartdatearray[0])->format("Y-m-d");
                    $scheme_start_date = new DateTime($scheme_start_date);

                    $scheme_expiry_date = date_create($schemeexpirydatearray[0])->format("Y-m-d");
                    $scheme_expiry_date = new DateTime($scheme_expiry_date);

                    $regdayswithscheme = 0;
                    $premiumdayswithscheme = 0;
                    $regdayswithoutscheme = 0;
                    $premiumdayswithoutscheme = 0;

                    $reg_hours_with_scheme = 0;
                    $premium_hours_with_scheme = 0;
                    $reg_hours_without_scheme = 0;
                    $premium_hours_without_scheme = 0;

                    $premiumdays = array('Fri', 'Sat', 'Sun');
                    $premiumdaysforhours = array('Thu', 'Fri', 'Sat', 'Sun', 'Mon');
                    if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                        
                    } else {

                        $scheme_pickup_date->modify('+1 day');
                        ////
                        if (intval($_REQUEST["hdOriginID"]) == "69") {


                            if ($timeFrom >= 480 and $timeTo <= 480) {
                                // @Duration = @Duration
                            } else if ($timeFrom = 480 and $timeTo > 480) {
                                //@Duration = @Duration + 1
                                $scheme_dropoff_date->modify('+1 day');
                                //   echo '1 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                            } else if ($timeFrom > 480 and $timeTo >= 480) {
                                //@Duration = @Duration + 1
                                $scheme_dropoff_date->modify('+1 day');
                                //    echo '2 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                            } else if ($timeFrom < 480 and $timeTo = 480) {
                                //@Duration = @Duration + 1
                                $scheme_dropoff_date->modify('+1 day');
                                //   echo '3 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                            } else if (@TimeFrom < 480 and @ TimeTo > 480) {
                                // @Duration = @Duration + 2
                                $scheme_dropoff_date->modify('+2 day');
                                // echo '4 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                            } else if (@TimeFrom < 480 and @ TimeTo < 480) {
                                //@Duration = @Duration + 1
                                $scheme_dropoff_date->modify('+1 day');
                                //  echo '5 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                            }
                        } else {
//
//                                    if ($remaininghour > 1) {
//                                        $scheme_dropoff_date->modify('+1 day');
//                                       // echo '6 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
//                                    }
                        }
                        //   echo '$scheme_pickup_date = '.$scheme_pickup_date.'<br/>';
                        // // echo '$scheme_dropoff_date = '.$scheme_dropoff_date.'<br/>';
                        //  ///
                        //echo $scheme_dropoff_date->format("Y-m-d H:i:s");
//                                for (; $scheme_pickup_date <= $scheme_dropoff_date;) {
////echo "abcd";
//                                    $checkday = $scheme_pickup_date->format('D');
//                                    if (($scheme_pickup_date) < ($scheme_start_date)) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//
//                                            $regdayswithoutscheme++;
//                                        }
//                                    } if ($scheme_start_date->format('Y-m-d') <= $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if ($scheme_pickup_date->format('Y-m-d') <= $scheme_expiry_date->format('Y-m-d')) {
//
//                                            if (in_array($checkday, $premiumdays)) {
//
//                                                $premiumdayswithscheme++;
//                                            } else {
//
//                                                $regdayswithscheme++;
//                                            }
//                                        }
//                                    } if ($scheme_expiry_date->format('Y-m-d') < $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//                                            $regdayswithoutscheme++;
//                                        }
//                                    }
//                                    $scheme_pickup_date->modify('+1 day');
//                                }
                        $argval = array();

                        $argval['CityId'] = $orgIDs[0];
                        $argval['PkgType'] = $_REQUEST["chkPkgType"];
                        $argval['PromotionCode'] = $arr[$i]['scheme'];
                        $argval['FromDate'] = date("Y-m-d", strtotime($_REQUEST['pickdate']));
                        $argval['ToDate'] = date("Y-m-d", strtotime($_REQUEST['dropdate']));
                        $argval['TimeTo'] = $_REQUEST['tHourD'] . $_REQUEST['tMinD'];
                        $argval['TimeFrom'] = $_REQUEST['tHourP'] . $_REQUEST['tMinP'];


                        $resultVal = $cor->_CORGetFreedomDays($argval);

                        $fleets_arr = $myXML->xml2ary($resultVal);

                        $regdayswithscheme = intval("" . $fleets_arr[0]['WeekDayScheme']);

                        $premiumdayswithscheme = intval("" . $fleets_arr[0]['WeekEndScheme']);
                        $regdayswithoutscheme = intval("" . $fleets_arr[0]['WeekDayNoScheme']);
                        $premiumdayswithoutscheme = intval("" . $fleets_arr[0]['WeekEndNoScheme']);

                        $reg_days_with_scheme_totalprice = 0;
                        $premium_days_with_scheme_totalprice = 0;
                        $reg_days_without_scheme_totalprice = 0;
                        $premium_days_without_scheme_totalprice = 0;
                        $premium_days_with_scheme_totalprice1 = 0;
                        $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                        $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                        $reg_days_with_scheme_totalprice1 = $regdayswithscheme * $arr[$i]["PkgRate"];

                        if (intval($_REQUEST["hdOriginID"]) == "69") {
                            $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRate"];
                            $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekendDis']) / 100);
                            $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRate"];
                        } else {
                            $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                            $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                            $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRateWeekEnd"];
                        }
//                            echo '$reg_days_with_scheme_totalprice1 = '.$reg_days_with_scheme_totalprice1.'<br/>';
//                            echo '$premium_days_with_scheme_totalprice1 = '.$premium_days_with_scheme_totalprice1.'<br/>';
//                            echo '$reg_days_without_scheme_totalprice = '.$reg_days_without_scheme_totalprice.'<br/>';
//                            echo '$premium_days_without_scheme_totalprice = '.$premium_days_without_scheme_totalprice.'<br/>';
                        $totalschemeamount1 = $reg_days_with_scheme_totalprice1 + $premium_days_with_scheme_totalprice1 +
                                $reg_days_without_scheme_totalprice + $premium_days_without_scheme_totalprice;
                        $totalschemeamount1 = $totalschemeamount1 + ($totalschemeamount1 * $vat / 100);

                        $totalschemeamount = $reg_days_with_scheme_totalprice + $premium_days_with_scheme_totalprice +
                                $reg_days_without_scheme_totalprice +
                                $premium_days_without_scheme_totalprice;
                        $totAmount = $totalschemeamount;
                        $totalfare1 = intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100));
                        $basicamount1 = $totalschemeamount;
                    }
                    if (intval($arr[$i]["IsAvailable"]) == 1) {
                        ?>
                        <form id="formBook<?php echo $i; ?>" name="formBook<?php echo $i; ?>" method="post" action="">

                            <input type="hidden" name="urlcurdate" id="hdUrlcurdate<?php echo $i; ?>" value="<?php
                        if ($_REQUEST['curdatetime'] != '') {
                            echo $_REQUEST['curdatetime'];
                        } else {
                            if ($_REQUEST['urlcurdate'] != '') {
                                echo $_REQUEST['urlcurdate'];
                            }
                        };
                        ?>"/>
                            <?php
                            if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                                
                            } else {
                                ?>
                                <input type="hidden" id="hdCoupon<?php echo $i; ?>" name="hdCoupon" value="<?php echo $arr[$i]['scheme']; ?>" />
                                <input type="hidden" id="hdSchemeDiscount<?php echo $i; ?>" name="hdSchemeDiscount" value="<?php echo intval($arr[$i]["OriginalAmt"] - $totalschemeamount); ?>" />
                                       <?php
                                   }
                                   ?>

                            <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo trim($carModel); ?>" />
                            <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                            <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginName"] ?>" />
                            <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $arr[$i]["CityID"]; //echo $_REQUEST["hdOriginID"]         ?>"/>
                            <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationName"]; ?>"/>
                            <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $arr[$i]["CityID"]; ?>"/>
                            <input type="hidden" name="hdCarID" id="hdCarID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarID"]; ?>" />
                            <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                            <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                            <input type="hidden" name="hdCat" id="hdCat<?php echo $i; ?>" value="<?php echo $carCatgName; ?>" />
                            <input type="hidden" name="PkgRate" id="PkgRate<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                            <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekDayDuration"]; ?>"> 
                            <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekEndDuration"]; ?>">
                            <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["FreeDuration"]; ?>"> 
        <?php
        if ($_REQUEST["chkPkgType"] == "Daily" || $_REQUEST["chkPkgType"] == "Weekly" || $_REQUEST["chkPkgType"] == "Monthly") {
            ?>
                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />
            <?php
        } else {
            ?>
                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"]; ?>" />
                                <?php
                            }
                            ?>
                            <input type="hidden" name="totAmount" id="totAmount<?php echo $i; ?>" value="<?php echo $totAmount; ?>" />
                            <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $totalfare1; ?>" />
                            <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
                            <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y'); ?>" />
                            <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                            <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_REQUEST["hdTourtype"] ?>" />
                            <input type="hidden" name="tHourP" id="tHourP<?php echo $i; ?>" value="<?php
                            if (isset($_REQUEST["tHourP"])) {
                                echo $_REQUEST["tHourP"];
                            }
                            ?>" />
                            <input type="hidden" name="tMinP" id="tMinP<?php echo $i; ?>" value="<?php
                            if (isset($_REQUEST["tMinP"])) {
                                echo $_REQUEST["tMinP"];
                            }
                            ?>" />
                            <input type="hidden" name="tHourD" id="tHourD<?php echo $i; ?>" value="<?php echo $dropDate->format('H'); ?>" />
                            <input type="hidden" name="tMinD" id="tMinD<?php echo $i; ?>" value="<?php echo $dropDate->format('i'); ?>" />
                            <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $vat; ?>" />
                            <input type="hidden" name="secDeposit" id="secDeposit<?php echo $i; ?>" value="<?php echo $secDeposit; ?>" />
                            <input type="hidden" name="subLoc" id="subLoc<?php echo $i; ?>" value="" />
                            <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_REQUEST["chkPkgType"] ?>" />
                            <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"] ?>" />
                            <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["OriginalAmt"]); ?>" />
                            <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $basicamount1; ?>" />
                            <input type="hidden" name="allSubLoc" id="allSubLoc<?php echo $i; ?>" value="<?php echo $arr[$i]["DetailID"]; ?>" />
                            <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $arr[$i]["KMIncluded"]; ?>" />
                            <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $arr[$i]["ExtraKMRate"]; ?>" />
                            <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="<?php echo $_REQUEST["customPkg"]; ?>" />
                            <!----for lms --->
                            <input type="hidden" name="userremark" id="userremark" value="contact-information" /> <!---------xml code-----> 
                            <input type="hidden" name="searchId" id="searchId"  value="<?php echo $scid = isset($_SESSION['last_search_id']) ? $_SESSION['last_search_id'] : $last_search_id ?>"/>
                            <input type="hidden" name="usertraffic" id="usertraffic" value="contact-information" />
                            <input type="hidden" name="gross_amount_scheme" id="gross_amount_scheme" value="<?php echo $totalschemeamount1; ?>"/>
                            <!----for lms --->
        <?php
    }
    ?>
                        <div class="repeatdiv">
                            <div class="clr"> </div>
    <?php
    if ((intval($arr[$i]["IsAvailable"]) == 0) && $checkerfornon == 0) {

        $checkerfornon++;
        ?>
                                <div class="car_description">
                                    <b>The following cars are not available for the duration you searched for, however you have the following option(s):</b>
                                    <ul>
                                <?php
                                $resAdv = $cor->_CORGetAdvanceSearchAvailability(array("ModelID" => $arr[$i]["ModelID"], "CarCatId" => $arr[$i]["CarCatID"], "CityID" => $_REQUEST["hdOriginID"], "pickupDate" => $pickDate->format('Y-m-d'), "droffDate" => $dropDate->format('Y-m-d'), "SubLocations" => "0", "PickupTime" => $_REQUEST["tHourP"] . $_REQUEST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i')));
                                $AdvResults = $myXML->xml2ary($resAdv->{'GetAdvanceSearchAvailabilityResult'}->{'any'});
                                if (intval(count($AdvResults))) {
                                    ?>
                                            <!--<li>You can select the Flexible plans option, if you have flexible plans</li>-->
            <?php
        }
        ?>
                                        <li>You can select the Next available option, if you need the car for a fixed period</li>
                                    </ul>
                                </div>
                                    <?php } ?>
                            <div class="clr"> </div>

                            <div class="typefcr">
                                <div class="imgdv">
                                    <img src="<?php echo corWebRoot; ?>/images/<?php echo str_ireplace(" ", "-", trim($carModel)); ?>.jpg" alt="" />
                                </div>
                                <div class="infdv">
                                    <span><?php echo $CarDetail; ?></span><br /><?php
                            if (intval($arr[$i]["IsAvailable"]) == 1) {
                                ?>
                                        <small><div class="more-less">
                                                <div class="more-block">
        <?php
        echo "Available at: " . str_ireplace(':', ', ', (str_ireplace(',', ' ', $arr[$i]["Detail"])));
        ?>

                                                </div><div class="adjust" href="#"></div>
                                            </div></small>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="capcity">
                                <label class="noof"><?php echo $passCount; ?></label>
                            </div>
                            <div class="price billing">

                                    <?php
                                    if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                                        if ($_REQUEST['chkPkgType'] == 'Daily') {


                                            if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                ?>
                                            <span id="ffare<?php echo $i; ?>" class="new"> Starting Rs <?php echo intval($arr[$i]["PkgRate"]); ?><?php
                                                if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                    echo "/Day";
                                                }
                                                ?></span>
                                            <?php
                                        }
                                    }

                                    if ($_REQUEST['chkPkgType'] == 'Daily') {


                                        if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] == 0 && $arr[$i]["PkgRate"] > 0.00) {
                                            ?>
                                            <span id="ffare<?php echo $i; ?>" class="new">Rs <?php echo intval($arr[$i]["PkgRate"]); ?><?php
                                            if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                echo "/Day";
                                            }
                                            ?></span>
                                            <?php
                                        }
                                    }

                                    if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                        if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                            ?>
                                            <span id="ffare<?php echo $i; ?>" class="new">Rs <?php echo intval($arr[$i]["PkgRate"]); ?>/ Hour</span>
                                                <?php
                                        }
                                    }

                                    if ($_REQUEST['chkPkgType'] == 'Daily') {

                                        if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["WeekDayDuration"] == 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                            ?>
                                            <span id="ffare<?php echo $i; ?>" class="new">Rs<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?> <?php
                                            if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                echo "/Day";
                                            }
                                            ?></span>
                                            <?php
                                        }
                                    }


                                    if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                        if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                            ?>
                                            <span id="ffare<?php echo $i; ?>" class="new">Rs <?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?>/ Hour</span>
                                            <?php
                                        }
                                    }
                                    ?>


                                    <?php
                                    if ($pkgt == "Hourly" || $pkgt == "Daily") {
                                        if ($pkgt == "Hourly") {
                                            ?>
                                                                                                                                            <!-- <span id="ffare<?php echo $i; ?>" class="new">Rs <?php echo intval($arr[$i]["PkgRate"]); ?> / Hour</span>-->
                                            <?php
                                        } elseif ($pkgt == "Daily") {
                                            ?>
                                                                                                                                             <!--<span id="ffare<?php echo $i; ?>" class="new">Rs <?php echo intval($arr[$i]["PkgRate"]); ?> / Day</span>-->
                                            <?php
                                        }
                                        if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                            ?>
                                            <div style="clear: both;"></div><span class="newo" style="font-size: 13px;"><s style='color:#db4626;'>Rs <?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?>/-</s></span><span class="newp" style="font-size: 13px;">Rs <?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?>/-</span>
                                            <?php
                                        } else {
                                            ?>
                                            <div style="clear: both;"></div><span class="newo"><span class="newo" style="font-size: 13px;">Minimum Billing: Rs <?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?>/-</span></span>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <span>
                                        <?php
                                        if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                            ?>
                                                <span><span class="newo"><s style='color:#db4626;'>Rs 
                                            <?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?>/-</s></span>
                                                    <br />
                                                    <span class="newp">Rs <?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?>/-</span>
                                                </span>
                                                <span id="ffare<?php echo $i; ?>" class="new" style="position: relative;top:-10px;left:10px;">
                                                <?php
                                            } else {
                                                ?>
                                                    <div style="clear: both;"></div>
                                                    <span class="newo">
                                                        <span class="newo">Rs <?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?>/-</span>
                                                    </span>
                                                    <span id="ffare<?php echo $i; ?>" class="new" style="position: relative;left:10px;">
                                                    <?php
                                                }
                                                ?>

            <?php
            if ($pkgt == "Weekly") {
                ?>
                                                        / Week
                                                        <?php
                                                    } elseif ($pkgt == "Monthly") {
                                                        ?>
                                                        / Month
                                                        <?php
                                                    }
                                                }
                                            } else { // scheme 
                                                if ($_REQUEST['chkPkgType'] == 'Daily') {


                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                        ?>
                                                        <span id="ffare<?php echo $i; ?>" class="new block"> Rs <?php echo intval($arr[$i]["PkgRate"]); ?> /- Per Day</span>
                                                        <?php
                                                        if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                            ?>
                                                            <span class="d_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                if ($_REQUEST['chkPkgType'] == 'Daily') {


                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] == 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                        ?>
                                                        <span id="ffare<?php echo $i; ?>" class="new block">Rs<?php echo intval($arr[$i]["PkgRate"]); ?>/- Per Day</span>
                                                        <?php
                                                        if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                            ?>
                                                            <span class="d_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                            <?php
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }



                                                if ($_REQUEST['chkPkgType'] == 'Daily') {

                                                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["WeekDayDuration"] == 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                                        ?>
                                                        <span id="ffare<?php echo $i; ?>" class="new block">Rs. <?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?> /- Per Day </span>
                                                        <?php
                                                        if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                            ?>
                                                            <span class="d_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                            <?php
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }

                                                if ($pkgt == "Hourly" || $pkgt == "Daily") {

                                                    if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                        ?>
                                                        <div style="clear: both;"></div>
                                                        <span class="newp">
                                                            <span class="old_bill">
                                                                Regular Price: Rs.
                                                                <strike> <?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?></strike>
                                                            </span>
                                                            <span class="new_bill">
                                                                Discounted Price: Rs. 
                                                                <span><?php echo $totalfare1; ?>/-</span>
                                                            </span>
                                                        </span>
                <?php
            } else {
                ?>
                                                        <div style="clear: both;"></div>
                                                        <span class="newo">

                                                            <span class="newo">
                                                                <span class="old_bill">
                                                                    Regular Price: Rs
                                                                    <strike><?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?></strike>
                                                                </span>
                                                                <span class="new_bill">
                                                                    Discounted Price: Rs 
                                                                    <span><?php echo $totalfare1; ?>/-</span>
                                                                </span>
                                                            </span>
                                                        </span>
                <?php
            }
        } else {
            if ($pkgt == "Weekly") {
                ?>
                                                        <span class="d_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                <?php
            } elseif ($pkgt == "Monthly") {
                ?>
                                                        <span class="d_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                        <?php
                                                    }
                                                    ?>


                                                    <?php
                                                    if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                        ?>
                                                        <span><span class="newo">
                                                                <span class="newo">
                                                                    <span class="old_bill">
                                                                        Regular Price: Rs  
                                                                        <strike><?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?></strike>
                                                                    </span>
                                                                </span>
                                                                <span class="new_bill">
                                                                    Discounted Price: Rs 
                                                                    <span><?php echo intval($totalfare1); ?>/-</span>
                                                                </span>
                                                            </span>
                                                        </span>

                <?php
            } else {
                ?>
                                                        <div style="clear: both;"></div>
                                                        <span class="newo">
                                                            <span class="newo">
                                                                <span class="newo">
                                                                    <span class="old_bill">
                                                                        Regular Price: Rs
                                                                        <strike><?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?></strike>
                                                                    </span>
                                                                    <span class="new_bill">
                                                                        Discounted Price: Rs
                                                                        <span><?php echo intval($totalfare1); ?>/-</span>
                                                                    </span>
                                                                </span>
                                                            </span>

                <?php
            }
        }
    }
    ?>
                                            </span>
                                        </span>
                                        <div style="clear: both;"></div>
                                        <a class="faredetails" href="javascript:void(0)" onmouseover="javascript: _pass(this.id)" id="link_<?php echo $i; ?>">Fare details</a>

                                                <?php
                                                if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                                                    ?>
                                            <div class="details_wrapper" id="popupbox<?php echo $i; ?>">
                                                <div class="toolarrow"></div>
                                                <div class="heading"><!--Fare Details--><span class="closedpop">X</span></div>
                                                <br>
                                                <p><span>Fare Details</span></p>

                                                                            <!--<p><span>Rate</span></p>
        <?php
        if ($pkgt == "Hourly") {
            ?>
                                                                                                                        <p><big><span id="ffarepp<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["PkgRate"]); ?> / Hour</span></big></p>
                                                <?php
                                            } elseif ($pkgt == "Daily") {
                                                ?>
                                                                                                                        <p><big><span id="ffarepp<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["PkgRate"]); ?> / Day</span></big></p>
                                                <?php
                                            } elseif ($pkgt == "Weekly") {
                                                ?>
                                                                                                                        <p><big><span id="ffarepp<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["BasicAmt"]); ?> / Week</span></big></p>
            <?php
        } elseif ($pkgt == "Monthly") {
            ?>
                                                                                                                        <p><big><span id="ffarepp<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["BasicAmt"]); ?> / Month</span></big></p>
                                                    <?php
                                                }
                                                ?>
                                                                            <p><span>Fare Details</span></p>-->
                                                <ul>

                                                    <!--New code-->
                                                <?php
                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                        ?>
                                                            <li>Regular days:<?php echo $arr[$i]["WeekDayDuration"]; ?>@<?php echo $arr[$i]["PkgRate"]; ?></li>
                                                        <?php
                                                    }
                                                }
                                                if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                        ?>
                                                            <li>No. of hours:<?php echo $arr[$i]["WeekDayDuration"]; ?>@<?php echo $arr[$i]["PkgRate"]; ?></li>
                <?php
            }
        }


        if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

            if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                ?>
                                                            <li>Premium days:<?php echo $arr[$i]["WeekEndDuration"]; ?>@<?php echo $arr[$i]["PkgRateWeekEnd"]; ?></li>
                                                            <?php
                                                        }
                                                    }


                                                    if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                        if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                                            ?>
                                                            <li>No. of hours:<?php echo $arr[$i]["WeekEndDuration"]; ?>@<?php echo $arr[$i]["PkgRateWeekEnd"]; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!-- New Code-->


                                                    <li>Minimum billing: Rs <?php echo intval($arr[$i]["BasicAmt"]); ?>/-</li>
                                                    <li>VAT (@<?php echo number_format($vat, 2); ?>%): Rs <?php echo intval(ceil(($arr[$i]["BasicAmt"] * $vat) / 100)); ?>/-</li>
                                                    <li>Total fare: Rs <?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?>/-</li>
                                                    <?php
                                                    if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                        ?>
                                                        <li>Discount Amount (On base fare): Rs <?php echo intval($arr[$i]["OriginalAmt"] - $arr[$i]["BasicAmt"]); ?>/-</li>
                                                        <?php
                                                    }
                                                    if (intval($_REQUEST["hdOriginID"]) != "69") {
                                                        ?>
                                                        <li>Refundable security deposit (pre-auth from card): Rs <?php echo $secDeposit; ?> (Mastercard/Visa/Amex)</li>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <li>The billing cycle starts from 8am each day</li>
                                                        <li>Refundable Security Deposit: Rs <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                        <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                        <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                                    <?php
                                                    if (trim($_REQUEST["chkPkgType"]) == "Hourly") {
                                                        ?>
                                                    <p><span>Includes</span></p>
                                                    <ul>
                                                        <li>KMs Included: <?php echo $arr[$i]["KMIncluded"]; ?></li>
                                                        <li>Extra KM Charge: Rs. <?php echo $arr[$i]["ExtraKMRate"]; ?>/- per KM</li>
                                                    </ul>
            <?php
        }
        ?>
                                               
                                                  <p><span>Eligibilty</span></p>
													
													<ul>
                                                        <li>The minimum age required to rent a Myles-Nano is 18yrs. However,<br> it is 23yrs for all other cars. 
The identification documents needs to be in the name of the person who is booking the car</li>
                                                        
                                                    </ul>

											   <p><span>Mandatory Documents(Original)</span></p>
                                                    <?php
                                                    if (intval($_REQUEST["hdOriginID"]) != "69") {
                                                        ?>
                                                    <ul>
                                                        <li>Passport / Voter ID card</li>
                                                        <li>Driving license</li>
                                                        <li>Credit card</li>
														<li>Aadhar card</li>
                                                    </ul>
            <?php
        } else {
            ?>
                                                    <ul>
                                                        <li>Passport / Voter ID card</li>
                                                        <li>Driving license</li>
                                                        <li>Any of the following has to be submitted in original as an identity proof.
                                                            <ol type="1">
                                                                <li>Adhaar card</li>
                                                                <li>Pan card</li>
                                                                <li>Voter ID card</li>
                                                            </ol>
                                                        </li>
                                                        <li>Please note that the car in Goa will be provided through a partner.</li>
                                                    </ul>
                                                    <?php
                                                }
                                                ?>
                                            </div>
        <?php
    } else {
        // echo 'scheme code';
        //scheme code
        ?>
                                            <div class="details_wrapper" id="popupbox<?php echo $i; ?>">
                                                <div class="toolarrow"></div>
                                                <div class="heading"><!--Fare Details--><span class="closedpop">X</span></div>
                                                <br>
                                                <p><span>Fare Details</span></p>


                                                <ul>

                                                <?php
                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                    // echo '$regdayswithoutscheme = '.$regdayswithoutscheme.'<br/>';
                                                    if ($regdayswithoutscheme > 0) {
                                                        ?>
                                                            <li>Weekdays :<?php echo $regdayswithoutscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?></li>
                                                    <?php
                                                    $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                                                }
                                            }



                                            if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                //echo '$premiumdayswithoutscheme = '.$premiumdayswithoutscheme.'<br/>';
                                                if ($premiumdayswithoutscheme > 0) {
                                                    ?>
                                                            <li>Weekends :<?php echo $premiumdayswithoutscheme; ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?></li>
                                                            <?php
                                                            $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                                                        }
                                                    }


                                                    if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                        // echo '$regdayswithscheme = '.$regdayswithscheme.'<br/>';
                                                        if ($regdayswithscheme > 0) {
                                                            ?>
                                                            <li>Weekdays with offer <?php echo $fleets1[0]['WeekdayDis']; ?>% :<?php echo $regdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?>




                                                            </li>
                                                            <?php
                                                            $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                                                        }
                                                    }

                                                    if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                        //echo '$premiumdayswithscheme = '.$premiumdayswithscheme.'<br/>';
                                                        if ($premiumdayswithscheme > 0) {
                                                            if (intval($_REQUEST["hdOriginID"]) == "69") {
                                                                ?>
                                                                <li>Weekend with offer <?php echo $fleets1[0]['WeekendDis']; ?>% :<?php echo $premiumdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?>



                                                                </li>
                    <?php
                } else {
                    ?>
                                                                <li>Weekend with offer <?php echo $fleets1[0]['WeekendDis']; ?>% :<?php echo $premiumdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?>



                                                                </li>
                                                                <?php
                                                            }
                                                            $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                                                        }
                                                    }
                                                    ?>
                                                    <!-- New Code-->


                                                    <li>Regular Price: Rs. <strike><?php echo intval($arr[$i]["BasicAmt"]); ?></strike>
                                                    <?php
                                                    echo $totalschemeamount;
                                                    ?>
                                                    /-</li>
                                                    <li>VAT (@<?php echo number_format($vat, 2); ?>%): Rs. <?php echo intval(ceil(($totalschemeamount * $vat) / 100)); ?>/-</li>

        <?php
        if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
            ?>
                                                        <li>Total fare: Rs.
                                                        <?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?> /-</li>
                                                        <li><strong>Discounted Price:</strong> <?php echo intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100)); ?> /-</li>

                                                        <?php
                                                    } else {
                                                        ?>
                                                        <li>Total fare: Rs.
                                                        <?php
                                                        echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));
                                                        ?>
                                                            /-</li>
            <?php
        }
        ?>



                                                        <?php
                                                        if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                            ?>
                                                        <li>Discount Amount (On base fare): Rs. <?php echo intval($arr[$i]["OriginalAmt"] - $totalschemeamount); ?>/-</li>

                                                        <?php
                                                    }
                                                    if (intval($_REQUEST["hdOriginID"]) != "69") {
                                                        ?>
                                                        <li>Refundable security deposit (pre-auth from card): Rs. <?php echo $secDeposit; ?> (Mastercard/Visa/Amex)</li>
                                                            <?php
                                                        } else {
                                                            ?>
                                                        <li>The billing cycle starts from 8am each day</li>
                                                        <li>Refundable Security Deposit: Rs. <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                        <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                        <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                                        <?php
                                                    }
                                                    ?>
                                                    <li>Pre-authorization will be done at the time of car pickup.</li>
                                                </ul>

                                                <p><span>Mandatory Documents(Original)</span></p>
        <?php
        if (intval($_REQUEST["hdOriginID"]) != "69") {
            ?>
                                                    <ul>
                                                        <li>Passport / Voter ID card</li>
                                                        <li>Driving license</li>
                                                        <li>Credit card</li>
														<li>Aadhar card</li>
                                                    </ul>
                                                        <?php
                                                    } else {
                                                        ?>
                                                    <ul>
                                                        <li>Passport / Voter ID card</li>
                                                        <li>Driving license</li>
                                                        <li>Any of the following has to be submitted in original as an identity proof.
                                                            <ol type="1">
                                                                <li>Adhaar card</li>
                                                                <li>Pan card</li>
                                                                <li>Voter ID card</li>
                                                            </ol>
                                                        </li>
                                                        <li>Please note that the car in Goa will be provided through a partner.</li>
                                                    </ul>
                                                    <?php
                                                }
                                                ?>
                                            </div>
        <?php
    }
    ?>

                                        <!-- else condition  -->
                                        </div>

    <?php
    $nddate = "";
    if (intval($arr[$i]["IsAvailable"]) == 0) {
        $resNext = $cor->_COR_SelfDrive_NextAvailibility(array("ModelID" => $arr[$i]["ModelID"], "CarCatID" => $arr[$i]["CarCatID"], "CityID" => $_REQUEST["hdOriginID"], "PickUpDate" => $pickDate->format('Y-m-d'), "DropOffDate" => $dropDate->format('Y-m-d'), "subLocationID" => "0", "DropOffTime" => $dropDate->format('H') . $dropDate->format('i')));
        $NADate = $myXML->xml2ary($resNext);

        $nextAvailablity = date_create($NADate[0]['Pickupdate']);
        ?>					 

                                            <div class="boknw next_avai" style="padding-top: 0px;"><label>
                                                    <input type="hidden" name="nxtavlclicked" class="nxtavlclicked<?PHP echo $i; ?>"/> <!-------FOR TRACK  NXT AVL-->
                                                    <a class="next_available_flex" href="javascript: void(0);" 
                                                       onclick="booknowsubmit('<?php echo "formSrchINA" . $i ?>');" >Next Available <img border="0" src="images/flexi_arrow.png"/></a><br /><b><?php echo $nextAvailablity->format('d-M, Y H:i') ?></b></label></div>

                                            <?php
                                            $Date1 = $nextAvailablity->format('Y-m-d');
                                            $nddate = date('Y-m-d', strtotime($Date1 . " + " . $interval . " day"));
                                            $datHM = explode(':', $nextAvailablity->format('H:i'));
                                            $dateH = $datHM[0];
                                            $dateM = $datHM[1];
                                            $enddate = date('j M, Y', strtotime($nddate));
                                        } else {
                                            if (!$isOffer) {
                                                ?>
                                                <div class="boknw"><input type="button" id="btnBookNow<?php echo $i; ?>" name="btnBookNow" value="Book Now" onclick="subform(<?php echo $i; ?>);" /></div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="boknw"><img src="<?php echo corWebRoot; ?>/images/sold-out.png" border="0" title="Sold out" alt="Sold out" /></div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </div><!-- repeatdiv Self Drive-->
    <?php
    if (intval($arr[$i]["IsAvailable"]) == 1) {
        ?>				   
                                            </form>
                                            <?php
                                        }
                                        /*
                                          Priyanka code
                                          $carsDetail = $cor->_CORCarModelCount(array("CityID"=>$_REQUEST["hdOriginID"],"pickupDate"=>$pickDate->format('Y-m-d'),"droffDate"=>$dropDate->format('Y-m-d'),"PickupTime"=>$pickDate->format('H').$pickDate->format('m').$pickDate->format('i'), "DropOffTime"=>$dropDate->format('H').$dropDate->format('m').$dropDate->format('i')));
                                          $searchCar = $myXML->xml2ary($carsDetail);


                                          if($nextAvailablity){
                                          $nextavail = $nextAvailablity->format('Y-m-d');
                                          }else{
                                          $nextavail = '';
                                          }

                                          $db = new MySqlConnection(CONNSTRING);
                                          $db->open();
                                          if(isset($_SESSION['searchId']) && $_SESSION['searchId']!="" )
                                          {

                                          $dataToSave["customer_search_id"] =  $_SESSION['searchId'];
                                          $dataToSave["car_model"] = $CarDetail;
                                          $dataToSave["total_cars"] = $searchCar[0]['Total'];
                                          $dataToSave["available_cars"] = $searchCar[0]['Available'];
                                          $dataToSave["booked_cars"] = $searchCar[0]['NotAvailable'];
                                          $dataToSave['next_available_date'] = $nextavail;//$nextAvailablity->format('Y-m-d');
                                          $r =  $db->insert("customer_search_cars", $dataToSave);
                                          } */
                                        ?>				     
                                        <?php
                                        if (intval($arr[$i]["IsAvailable"]) == 0) {

                                            if ((count($AdvResults))) {
                                                //echo '<pre>';print_r($AdvResults);
                                                ?>	
                                                <input type="hidden"  class="flexibleplanclickid<?php echo $i; ?>"/><!-----track flexi CLICK----->				
                                                <div id="flaxi_option<?php echo $i; ?>" class="flaxi_option"></div>
                                                <div id="popup_flaxi<?php echo $i; ?>" class="pop_flexi"> 
                                                    <h1>Best Available Options For Your Selected Travel Period</h1>
                                                    <div class="close_popup"><a  onclick ="div_hide(<?php echo $i; ?>)" href="javascript:void(0);">X</a></div>
                                                <?php
                                                $f = 0;
                                                foreach ($AdvResults as $AdvResult) {

                                                    $pick = str_split($AdvResult["Pickuptime"], 2);
                                                    $pickH = $pick[0];
                                                    $pickM = $pick[1];

                                                    $drop = str_split($AdvResult["Dropofftime"], 2);
                                                    $dropH = $drop[0];
                                                    $dropM = $drop[1];

                                                    $pkdate = date_create($AdvResult["Pickupdate"]);
                                                    $dkdate = date_create($AdvResult["Dropoffdate"]);
                                                    ?>
                                                        <div class="p20">
                                                            <div class="row">
                                                                <div class="f_l w_24 mr5">
                                                                    <span>Pick Up Date and Time</span>
                                                                    <input type="text" value="<?php echo $pkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Pickuptime"])); ?>" readonly="readonly">
                                                                </div>
                                                                <div class="f_l w_24 mr5">
                                                                    <span>Drop Off Date and Time</span>
                                                                    <input type="text" value="<?php echo $dkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Dropofftime"])); ?>" readonly="readonly">
                                                                </div>
                                                                <div class="f_l w_24 mr3">
                                                                    <span>Available At</span>
                                                        <?php echo $AdvResult['SublocationName']; ?>
                                                                </div>
                                                                <div class="f_l w_12">
                                                                    <input type ="hidden" name="flexibookclicked" class="flexibookclicked"/><!-----for track flexi id-->
                                                                    <input type="button" value="Book Now" name="Book Now"  onclick="booknowsubmit('<?php echo "formSrchFlexi" . $f; ?>');">
                                                                </div>
                                                                <div class="clr"> </div>
                                                            </div>
                                                        </div>


                                                        <form id="formSrchFlexi<?php echo $f; ?>" name="formSrchFlexi<?php echo $f; ?>" method="GET" action="">
                                                            <input type="hidden" name="urlcurdate" id="hdUrlcurdate<?php echo $i; ?>" value="<?php echo $_REQUEST['curdatetime']; ?>"/>
                                                            <input type="hidden" id="hdCarModelINA<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
                                                            <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                                                            <input type="hidden" name="hdOriginName" id="hdOriginNameINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginName"] ?>" />
                                                            <input type="hidden" name="hdOriginID" id="hdOriginIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CityID"]; ?>"/>
                                                            <input type="hidden" name="hdDestinationName" id="hdDestinationNameINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationName"]; ?>"/>
                                                            <input type="hidden" name="hdDestinationID" id="hdDestinationIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CityID"]; ?>"/>
                                                            <input type="hidden" name="hdCarID" id="hdCarIDINA<?php echo $i; ?>" value="<?php echo $AdvResult["carID"]; ?>" />
                                                            <input type="hidden" name="hdCarCatID" id="hdCarCatIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                            <input type="hidden" name="hdPkgID" id="hdPkgIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                                                            <input type="hidden" name="hdCat" id="hdCatINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatName"]; ?>" />
                                                            <input type="hidden" name="PkgRate" id="PkgRateINA<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                                                            <input type="hidden" name="duration" id="durationINA<?php echo $i; ?>" value="<?php echo $interval; ?>" />
                                                            <input type="hidden" name="totAmount" id="totAmountINA<?php echo $i; ?>" value="<?php echo $totAmount; ?>" />
                                                            <input type="hidden" name="totFare" id="totFareINA<?php echo $i; ?>" value="<?php echo ($totAmount + $vaTax); ?>" />
                                                            <input type="hidden" name="tab" id="tabINA<?php echo $i; ?>" value="2" />				
                                                            <input type="hidden" name="hdPickdate" id="pickdateINA<?php echo $i; ?>" value="<?php echo $pkdate->format('Y-m-d'); ?>" />
                                                            <input type="hidden" name="hdDropdate" id="dropdateINA<?php echo $i; ?>" value="<?php echo $dkdate->format('Y-m-d'); ?>" />
                                                            <input type="hidden" name="hdTourtype" id="tourtypeINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdTourtype"] ?>" />
                                                            <input type="hidden" name="tHourP" id="tHourPINA<?php echo $i; ?>" value="<?php echo $pickH; ?>" />
                                                            <input type="hidden" name="tMinP" id="tMinPINA<?php echo $i; ?>" value="<?php echo $pickM; ?>" />
                                                            <input type="hidden" name="tHourD" id="tHourDINA<?php echo $i; ?>" value="<?php echo $dropH ?>" />
                                                            <input type="hidden" name="tMinD" id="tMinDINA<?php echo $i; ?>" value="<?php echo $dropM ?>" />
                                                            <input type="hidden" name="vat" id="vatINA<?php echo $i; ?>" value="<?php echo $vat; ?>" />
                                                            <input type="hidden" name="secDeposit" id="secDepositINA<?php echo $i; ?>" value="<?php echo $secDeposit; ?>" />
                                                            <input type="hidden" name="subLoc" id="subLoc<?php echo $i; ?>" value="<?php echo $AdvResult['SubLocationID']; ?>" />
                                                            <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_REQUEST["chkPkgType"] ?>" />
                                                            <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"] ?>" />
                                                            <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["OriginalAmt"]); ?>" />
                                                            <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["BasicAmt"]); ?>" />
                                                            <input type="hidden" name="allSubLoc" id="allSubLoc<?php echo $i; ?>" value="<?php echo $AdvResult['SubLocationID']; ?>" />
                                                            <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $arr[$i]["KMIncluded"]; ?>" />
                                                            <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $arr[$i]["ExtraKMRate"]; ?>" />
                                                            <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="<?php echo $_REQUEST["customPkg"]; ?>" />
                                                            <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekDayDuration"]; ?>"> 
                                                            <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekEndDuration"]; ?>">
                                                            <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["FreeDuration"]; ?>"> 
                                                            <input type="hidden" name="pkgR" id="pkgR<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgRate"]; ?>">
                                                            <input type="hidden" name="FlexiBooking"  value="FlexiBooking"> 	<!----flexi booking------>	 				  
                                                        </form>
                <?php
                $f++;
            }
            ?>	
                                                </div>
            <?php
        }
        ?>
                                            <form id="formSrchINA<?php echo $i; ?>" name="formSrchINA<?php echo $i; ?>" action="<?php echo corWebRoot; ?>/search-result.php" method="GET">				 

                                                <input type="hidden" name="urlcurdate" id="hdUrlcurdate<?php echo $i; ?>" value="<?php echo $_REQUEST['curdatetime']; ?>"/>
                                                <input type="hidden" name="hdToday" id="hdToday<?php echo $i; ?>" value="<?php echo $_REQUEST["hdToday"]; ?>" />
                                                <input type="hidden" name="hdOriginName" id="hdOriginNameSF<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginName"]; ?>" />
                                                <input type="hidden" name="hdOriginID" id="hdOriginIDSF<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginID"]; ?>" />
                                                <input type="hidden" name="ddlOrigin" id="ddlOrigin<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginID"]; ?>" />
                                                <input type="hidden" name="hdDestinationName" id="hdDestinationNameSF<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationName"]; ?>" />
                                                <input type="hidden" name="hdDestinationID" id="hdDestinationIDSF<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationID"]; ?>" />
                                                <input type="hidden" name="hdTourtype" id="hdTourtypeSF<?php echo $i; ?>" value="<?php echo $_REQUEST["hdTourtype"]; ?>" />
                                                <input type="hidden" name="bookTime" id="bookTimeSF<?php echo $i; ?>" value="<?php echo $_REQUEST["bookTime"]; ?>" />
                                                <input type="hidden" name="userTime" id="userTimeSF<?php echo $i; ?>" value="<?php echo $_REQUEST["userTime"]; ?>" />
                                                <input type="hidden" name="onlyE2C" id="onlyE2C<?php echo $i; ?>" value="0" />
                                                <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_REQUEST["chkPkgType"]; ?>" />
                                                <input type="hidden" name="tHourP" id="tHourP<?php echo $i; ?>" value="<?php echo $dateH; ?>" />
                                                <input type="hidden" name="tMinP" id="tMinP<?php echo $i; ?>" value="<?php echo $dateM; ?>" />
                                                <input type="hidden" name="tHourD" id="tHourD<?php echo $i; ?>" value="<?php echo $dateH; ?>" />
                                                <input type="hidden" name="tMinD" id="tMinD<?php echo $i; ?>" value="<?php echo $dateM; ?>" />
                                                <input type="hidden" name="pickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $nextAvailablity->format('j M, Y'); ?>" />
                                                <input type="hidden" name="dropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $enddate; ?>" />
                                                <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="<?php echo $_REQUEST["customPkg"]; ?>" />	
                                                <input type="hidden" id="hdCarModelINA<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
                                                <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                                                <input type="hidden" name="hdCarID" id="hdCarIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarID"]; ?>" />
                                                <input type="hidden" name="hdCarCatID" id="hdCarCatIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                <input type="hidden" name="hdPkgID" id="hdPkgIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                                                <input type="hidden" name="hdCat" id="hdCatINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatName"]; ?>" />
                                                <input type="hidden" name="PkgRate" id="PkgRateINA<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                                                <input type="hidden" name="duration" id="durationINA<?php echo $i; ?>" value="<?php echo $interval; ?>" />
                                                <input type="hidden" name="totAmount" id="totAmountINA<?php echo $i; ?>" value="<?php echo $totAmount; ?>" />
                                                <input type="hidden" name="totFare" id="totFareINA<?php echo $i; ?>" value="<?php echo ($totAmount + $vaTax); ?>" />
                                                <input type="hidden" name="tab" id="tabINA<?php echo $i; ?>" value="1" />				
                                                <input type="hidden" name="hdPickdate" id="pickdateINA<?php echo $i; ?>" value="<?php echo $nextAvailablity->format('Y-m-d'); ?>" />
                                                <input type="hidden" name="hdDropdate" id="dropdateINA<?php echo $i; ?>" value="<?php echo $nddate; ?>" />
                                                <input type="hidden" name="vat" id="vatINA<?php echo $i; ?>" value="<?php echo $vat; ?>" />
                                                <input type="hidden" name="secDeposit" id="secDepositINA<?php echo $i; ?>" value="<?php echo $secDeposit; ?>" />
                                                <input type="hidden" name="subLoc" id="subLoc<?php echo $i; ?>" value="" />
                                                <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"] ?>" />
                                                <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["OriginalAmt"]); ?>" />
                                                <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["BasicAmt"]); ?>" />
                                                <input type="hidden" name="allSubLoc" id="allSubLoc<?php echo $i; ?>" value="" />
                                                <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $arr[$i]["KMIncluded"]; ?>" />
                                                <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $arr[$i]["ExtraKMRate"]; ?>" />
                                                <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekDayDuration"]; ?>"> 
                                                <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekEndDuration"]; ?>">
                                                <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["FreeDuration"]; ?>"> 
        <?php
        if ($_REQUEST["chkPkgType"] == "Daily" || $_REQUEST["chkPkgType"] == "Weekly" || $_REQUEST["chkPkgType"] == "Monthly") {
            ?>
                                                    <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />
            <?php
        } else {
            ?>
                                                    <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"]; ?>" />
            <?php
        }
        ?>
                                                <!----for lms --->
                                                <input type="hidden" name="userremark" id="userremark" value="contact-information" /> 
                                                <input type="hidden" name="searchId" id="searchId"  value="<?php echo $scid = isset($_SESSION['last_search_id']) ? $_SESSION['last_search_id'] : $last_search_id ?>"/>
                                                <input type="hidden" name="usertraffic" id="usertraffic" value="contact-information" />
                                                <!----for lms --->
                                            </form>
        <?php
    }
}
?>
                                    </div>
                                    </div>
                                    </div><!--srsf-->
                                    </div><!--main-->
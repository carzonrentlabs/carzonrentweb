<!--Header Start Here-->
<?php
	include_once('./classes/cor.mysql.class.php');
?>
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/enfeedback.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/wz_tooltip.js"></script>
<script>
	$(document).ready(function(){
		$('.footer_appbg').css('display','none');
	});
	</script>
<?php
    //$corH = new COR();
    //$res = $corH->_CORGetCities();	
    //$myXML = new CORXMLList();
    //$orgH = $myXML->xml2ary($res);
    //$orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    //$orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    //unset($corH);
    
    $pageURL = 'http';
    //if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	if( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ) {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    $refURL = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
    $refURLParts = explode("/", $refURL);
    $domain = $refURLParts[2];
    if(strpos($refURLParts[3], "q=") != false)
	    $qstr = explode("q=", $refURLParts[3]);
    else if(strpos($refURLParts[3], "p=") != false)
	    $qstr = explode("p=", $refURLParts[3]);
    $qterm = explode("&", $qstr[1]);
    $qterm = urldecode($qterm[0]);
    if($qterm != ""){
	    $searchquery = $qterm;
	    $arrIllegalChars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','`','{','}','[',']',':',';','?',',','.','<','>','/','|','\\');
	    $searchquery = str_replace($arrIllegalChars, '', $searchquery);
	    $searchquery = str_replace("'", '', $searchquery);
	    $searchquery = str_replace('"', '', $searchquery);
	    $searchquery = str_replace('  ', ' ', $searchquery);
	    $searchquery = str_replace(' ', '-', $searchquery);
	    
		$searchData = array();
		$srchDB = new MySqlConnection(CONNSTRING);
		$srchDB->open();
	
		$searchData["search_string"] = $qterm;
		$searchData["entry_date"] = date('Y-m-d H:i:s');
		$searchData["search_url"] = $searchquery;
		$searchData["search_engine"] = $domain;
		$searchData["page_url"] = $pageURL;

		unset($searchData);
		$srchDB->close();
    }
?>
<!--Top Logo & Socail Icon-->    
<div class="top widthA" style="width: 1130px;">
    <div class="cor-logo"><a href="<?php echo corWebRoot; ?>/"><img src="<?php echo corWebRoot; ?>/images/COR-logo.gif" width="85%" /></a></div>
	
	
	<div class="download-icon" style="width:auto; float:left;text-align: right; margin-left:363px;">
	
	<a href="http://bit.ly/CorRideIOS"><img src="<?php echo corWebRoot; ?>/images/app_store.png"></a>
	<a href="http://bit.ly/CorRideAndroid"><img src="<?php echo corWebRoot; ?>/images/google_play.png"></a>
	
	</div>
	
	
    <div class="social-icon">
	<ul>
	 
	  <li><a href="https://www.facebook.com/carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/FB-icon.gif" border="0" alt="Facebook" title="Facebook"/></a></li> 
	   <li><a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="<?php echo corWebRoot; ?>/images/Twitter.gif" border="0" alt="Twitter" title="Twitter" /></a></li>
	  <li><a href="https://www.linkedin.com/company/carzonrent-india-pvt-ltd" target="_blank"><img src="<?php echo corWebRoot; ?>/images/linkdin.png" border="0" alt="Linkedin" title="Linkedin" /></a></li>
	 
	  <li><a href="https://plus.google.com/+carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/googleplus.gif"  alt="googleplus" title="googleplus" border="0" /></a></li>
	 
	  <li><a href="http://www.youtube.com/carzonrenttv" target="_blank"><img src="<?php echo corWebRoot; ?>/images/You-tube.gif" border="0" alt="Youtube" title="Youtube" /></a></li>
	  <li><a target="_blank" href="http://blog.carzonrent.com/"><img border="0" src="http://www.carzonrent.com/images/blog.gif" alt="Blog" title="Blog"></a></li>
	  
	  
	  
	</ul>
    <br><div class="login">
    <?php
        if(isset($_COOKIE["gname"])){
            if($_COOKIE["gname"] != ""){
		echo "Hi, ".$_COOKIE["gname"]."&nbsp;&nbsp;|&nbsp;&nbsp;<a onmouseover=\"Tip('Get your booking history and profile details.')\" onmouseout=\"UnTip()\" href='" . corWebRoot . "/myaccount.php'>My Account</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"" . corWebRoot . "/logout.php\">Logout</a>";
	    } else {
    ?>
	<a href="http://book.carzonrent.com" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a>&nbsp;<!--<a href="<?php echo corWebRoot; ?>/faq.php" class="btn-special" target="_blank">Myles FAQs</a>-->
    <?php
	    }
	} else {
    ?>
	<a href="http://book.carzonrent.com" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a>&nbsp;<!--<a href="<?php echo corWebRoot; ?>/faq.php" class="btn-special" target="_blank">Myles FAQs</a>-->
    <?php
	}
    ?>
    </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--End-->
<!--Header Start-->

</div> <!-- d100p ends -->
    <div class="top-header" style="width:100%">
    <div  class="top-mid widthA" style="width:1130px;">
	<ul>
	<?php
	    $pgName = corWebRoot;
	    $arrCityID = array(2, 4, 7, 6);
	    if(trim($cCityID) != "0"){
		if(in_array($cCityID, $arrCityID)){
		    $defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		    $defCityID = array(2, 4, 7, 6);
		} else {
		    $defCity = array("Delhi", "Mumbai", "Bangalore");
		    $defCityID = array(2, 4, 7);
		}
	    } else {
		$defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		$defCityID = array(2, 4, 7, 6);
	    }
	    //if(trim($cCity) != "" && $cCityID != "0"){
	    //}		
		
		if($_SERVER['SCRIPT_NAME'])
		{
			$vel=$_SERVER['SCRIPT_NAME'];
			$velData=explode("/",$vel);
			if($velData[1]=='index.php' || $velData[1]==''){
	?>
			<li id="outstationLI" class="currentTab"><a href="javascript: _setTab('outstation');">Outstation</a></li>
			<li id="localLI"><a href="javascript: _setTab('local');">Local</a></li>
			<li id="airportLI"><a href="javascript: _setActive('airport');">Airport Transfer</a></li>
			
			<li id="selfdriveLI"><a href="http://www.mylescars.com">Self-Drive</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
	<?php
			}
			elseif($velData[1]=='home.php'){
	?>
			<li id="outstationLI"><a href="javascript: _setActive('outstation');">Outstation</a></li>
			<li id="localLI"><a href="javascript: _setActive('local');">Local</a></li>
			<li id="airportLI"><a href="javascript: _setActive('airport');">Airport Transfer</a></li>
			
			<li id="selfdriveLI"><a href="http://www.mylescars.com">Self-Drive</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
	<?php			
			} else {
	?>
			<li id="outstationLI"><a href="<?php echo corWebRoot; ?>/#outstation">Outstation</a></li>
			<li id="localLI"><a href="<?php echo corWebRoot; ?>/#local">Local</a></li>
			<li id="airportLI"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
			
			<li id="selfdriveLI"><a href="http://www.mylescars.com">Self-Drive</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
	<?php
			}
	?>
	</ul>
    <div class="top-number"><span class="top-number2">Call 24 hours</span> 011-41841212</div>
   <div class="clearfix"></div>
   <?php
	}
   ?>
  </div>
</div>
<div class="clearfix"></div>

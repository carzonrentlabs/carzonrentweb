<!-- Google Tag Manager -COR-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSZV9B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSZV9B');</script>
<!-- End Google Tag Manager -->

<div class="footer fma">
    <div class="footer_box">
        <div id="footer_menu">
            <ul>
                <li><a href="javascript:void(0);">About Us</a></li>
                <li><a href="javascript:void(0);">Our Service</a></li>
                <li><a href="javascript:void(0);">Vehicle Guide</a></li>
                <li><a href="javascript:void(0);">Media</a></li>
                <li><a href="javascript:void(0);">Careers</a></li>
            </ul>
        </div> <!--close footer_menu div-->
        <div class="right">
            <div class="right_image">
                <img src="<?php echo corWebRoot; ?>/images-myles/gmailcount.png"/>
            </div>
            <div class="right_image mr10">
                <a href="javascript:void(0);"><img src="<?php echo corWebRoot; ?>/images-myles/gmail.png"/></a>
            </div>
            <div class="right_image mr10">
                <a href="javascript:void(0);"><img src="<?php echo corWebRoot; ?>/images-myles/follow.png"/></a>
            </div>
            <div class="right_image mr10">
                <img src="<?php echo corWebRoot; ?>/images-myles/count.png"/>
            </div>
            <div class="right_image mr10">
                <a href="javascript:void(0);"><img src="<?php echo corWebRoot; ?>/images-myles/like.png"/></a>
            </div>
            <span class="right_image mt10">Account Summary</span><div class="cb"></div>
            <span class="right_image">Contact Us</span>
            <span class="copyright_text">Copyright &copy Carzonrent. All Rights Reserved</span>
        </div> <!--close right div-->
    </div> <!--close footer_box div-->
</div> <!--close footer div-->
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/secure.js"></script>
<div class="tbbinginr outstation">
     <div id="country1" class="tabcontent">
     <div class="selectcartp">
          <div class="main">
               <h2>You Searched For</h2>
               <div class="wd136">
                    <h3>From</h3>
<?php
                         for($i = 0; $i < $points; $i++){
                         $dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
                         $dispOrgs = explode(",", $dispOrg);
?>
                         <p><?php echo $dispOrgs[0]; ?></p>
<?php
                         }
?>
               </div>
               <div class="wd136">
                    <h3>Destination</h3>
<?php
                         for($i = 0; $i < $points; $i++){
                         $dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
                         $dispDests = explode(",", $dispDest);
?>
                         <p><?php echo $dispDests[0]; ?></p>
<?php
                         }
?>
               </div>
               <div class="wd136">
                    <h3>Approx Kms</h3>
<?php
                         for($i = 0; $i < $points; $i++){
                         $dispKM = $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"text"};
                         $totalKM += $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"value"};
?>
                         <p><?php echo $dispKM; ?></p>
<?php
                         }
?>		
               </div>
               <div class="wd136" style="width:200px !important;">
                    <h3>Pickup Date</h3>
                    <p><?php echo $pickDate->format('D d-M, Y'); ?></p>
               </div>
<?php
                    $pickDate = date_create(date('Y-m-d', strtotime($pDate)));
?>
               <div class="wd136">
                    <h3>Duration</h3>
                    <p><?php echo $interval; ?> Days</p>
               </div>
          </div><!--main-->
     </div><!--selectcartp--><div class="main">
<?php
          $isOutStation = TRUE;
          $cor = new COR();
          $res = $cor->_CORGetCabs(array("source"=>$orgIDs[0],"dateout"=>$pickDate->format('Y-m-d'),"outStationYN"=>$isOutStation));
          $myXML = new CORXMLList();
          $arr = $myXML->xml2ary($res);
		  
?>
          <div class="selectcarmid">
               <div class="trtp">
                    <label class="frst" style="width:375px">Type of Car</label>
                    <label class="snd">Seating Capacity</label>
                    <label class="lst">Price</label>
                </div>
                <?php
                if (count($arr) > 0) {
                    $dayFare = array();
                    $isAvail = array();
                    foreach ($arr as $k => $v) {
                        $dayFare[$k] = $v['DayRate'];
                        $isAvail[$k] = $v['IsAvailable'];
                    }

                    array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
                    for ($i = 0; $i < count($arr); $i++) {

                        /*                         * * soldout implementation ** */

                        $soldout = $cor->_CORGetSoldOutWeb(array("cityid" => $orgIDs[0], "CategoryID" => $arr[$i]['CarCatID'], "PickUpDate" => $pickDate->format('Y-m-d'),
                            "PickUpTime" => 1000,
                            "DropOffDate" => $dropDate->format("Y-m-d"),
                            "DropOffTime" => 1800));
                        $soldout = $myXML->xml2ary($soldout);




                        /*                         * * soldout implementation ** */
                        $btnid = 1;
                        $kmPerDay = intval(($totalKM / 1000) / $interval);
                        $totDistance = 0;
                        if ($kmPerDay < 250) {
                            $totAmount = intval(250 * $interval * $arr[$i]["KMRate"] + ($interval * $arr[$i]["ChaufferAllowance"] + ($intervalnight * $arr[$i]["NightStayAllowance"])));
                            $totDistance = 250 * $interval;
                        } else {
                            $totDistance = intval($totalKM / 1000);
                            $totAmount = intval($totDistance * $arr[$i]["KMRate"] + ($interval * $arr[$i]["ChaufferAllowance"] + ($intervalnight * $arr[$i]["NightStayAllowance"])));
                        }
                        $totAmountforPopUp = $totAmount;
                        $totAmount = intval($totAmount + (($totAmount * $arr[$i]["Stax"]) / 100));
                        $serviceTax = $totAmount - $totAmountforPopUp;
                        $oldTotalAmount = $totAmount;
                        ?>
                        <form id="formBook" name="formBook" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>-to-<?php echo trim(str_ireplace(",", "-to-", str_ireplace(" ", "-", strtolower($_POST["hdDestinationName"])))); ?>/">
                        <?php
                        if ($arr[$i]["CarCatName"] == 'Intermediate')
                            $carCatgName = "Budget";
                        else if ($arr[$i]["CarCatName"] == 'Van')
                            $carCatgName = "Family";
                        else if ($arr[$i]["CarCatName"] == 'Superior')
                            $carCatgName = "Business";
                        else if ($arr[$i]["CarCatName"] == 'Premium')
                            $carCatgName = "Premium";
                        else if ($arr[$i]["CarCatName"] == 'Luxury')
                            $carCatgName = "Luxury";
                        else if ($arr[$i]["CarCatName"] == 'Special')
                            $carCatgName = "Super Luxury";
                        else
                            $carCatgName = $arr[$i]["CarCatName"];
                        ?>
                            <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $carCatgName; ?>" />
                            <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"] ?>" />
                            <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                            <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]; ?>"/>
                            <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                            <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgID"]; ?>" />
                            <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                            <input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo $cor->encrypt(intval($arr[$i]["DayRate"])); ?>" />
                            <input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo $cor->encrypt(number_format($arr[$i]["KMRate"], 1)); ?>" />
                            <input type="hidden" name="PkgHrs" id="PkgHrs<?php echo $i; ?>" value="<?php echo intval($arr[$i]["PkgHrs"]); ?>" />
                            <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />

                            <input type="hidden" name="nightduration" id="nightduration<?php echo $i; ?>" value="<?php echo $intervalnight; ?>" />

                            <input type="hidden" name="cabHour" id="cabHour<?php echo $i; ?>" value="<?php echo $_POST["cabHour"] ?>" />
                            <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $cor->encrypt($totAmount); ?>" />
                            <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
                            <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y'); ?>" />
                            <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                            <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                            <input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo $cor->encrypt($totDistance); ?>" />
                            <input type="hidden" name="tHour" id="tHour<?php echo $i; ?>" value="<?php if (isset($_POST["tHour"])) {
                        echo $_POST["tHour"];
                    } ?>" />
                            <input type="hidden" name="tMin" id="tMin<?php echo $i; ?>" value="<?php if (isset($_POST["tMin"])) {
                        echo $_POST["tMin"];
                    } ?>" />
                            <input type="hidden" name="ChauffeurCharges" id="ChauffeurCharges<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["ChaufferAllowance"]); ?>" />
                            <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $cor->encrypt($totAmountforPopUp); ?>" />
                            <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["Stax"]); ?>" />
                            <input type="hidden" name="NightStayAllowance" id="NightStayAllowance<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["NightStayAllowance"]); ?>"> 
                            <!----for lms --->
                            <input type="hidden" name="searchId" id="searchId"  value="<?php echo $scid = isset($_SESSION['last_search_id']) ? $_SESSION['last_search_id'] : $last_search_id ?>"/>
                            <input type="hidden" name="usertraffic" id="usertraffic" value="contact-information" />
                            <!----for lms --->
							<input type="hidden" name="refclient" id="refclient" value="<?php echo $_SESSION['refclient']; ?>" />
							<input type="hidden" name="CGSTPercent" id="CGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['CGSTPercent']);?>" />
							<input type="hidden" name="SGSTPercent" id="SGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['SGSTPercent']);?>" />
							<input type="hidden" name="IGSTPercent" id="IGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['IGSTPercent']);?>" />
							<input type="hidden" name="GSTEnabledYN" id="GSTEnabledYN" value="<?php echo $cor->encrypt($arr[$i]['GSTEnabledYN']); ?>"/>
							<input type="hidden" name="ClientGSTId" id="ClientGSTId" value="<?php echo $cor->encrypt($arr[$i]['ClientGSTId']); ?>"/>
                            <div class="repeatdiv">
                                <div class="typefcr">
                                    <div class="imgdv"><a href="javascript: void(0);">
                                            <img src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ", "-", $carCatgName); ?>.jpg" alt="" /></a>
                                    </div>
                                    <div class="infdv">
                                        <?php
                                        $carModel = "";
                                        $passCount = 4;
										
										if($arr[$i]["CarCatName"] == 'Economy')	
                                             $carModel = "Indica or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Compact')	
                                             $carModel = "Ritz or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Intermediate')	
                                             $carModel = "Swift Dzire or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Van'){	
                                             $carModel =  "Innova or Equivalent<br />";
                                             $passCount = 6;
                                   }
                                   else if($arr[$i]["CarCatName"] == 'Standard')	
                                             $carModel =  "City or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Superior')	
                                             $carModel =  "Corolla or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Premium')	
                                             $carModel =  "Accord or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Luxury')	
                                             $carModel =  "E Class or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Special')	
                                   $carModel =  "Mercedes-S Series" . "<br />";
                                               else if($arr[$i]["CarCatName"] == '4 Wheel Drive' || $arr[$i]["CarCatName"] == 'Full Size')	{
                                   $carModel =  "Qualis or Equivalent<br />";
								   
								   
                                        
                                            $passCount = 6;
                                        }
                                        ?>
                                        <span><?php echo $carCatgName; ?></span><br /><?php echo $carModel; ?>
                                        <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
			     </div>
                              </div>
                              <div class="capcity">
                                        <label class="noof"><?php echo $passCount; ?></label>
                                        <label class="tx">+ driver</label>
                              </div>
<?php
                         $oldDayRate = $arr[$i]["DayRate"];
                         $oldKMRate = $arr[$i]["KMRate"];
                         if(file_exists("./xml/rates/" . strtolower(str_ireplace(" ", "-", $arr[$i]["CarCatName"])) . ".xml")){
                              $rate = file_get_contents("./xml/rates/" . str_ireplace(" ", "-", $arr[$i]["CarCatName"]) . ".xml");
                              if($rate != ""){
                                   $xmlRate = new SimpleXmlElement($rate);
                                   $oldDayRate = $xmlRate->{'dayrate'};
                                   $oldKMRate = $xmlRate->{'kmrate'};
                              }
                         }
?>
                              <div class="price">
                                   <span id="ffare<?php echo $i; ?>">Rs <?php echo number_format($arr[$i]["KMRate"], 1); ?>/km</span><br />
                                   <span class="sep"></span>
<?php
                                   if($oldTotalAmount > $totAmount){
?>
                                        Minimum Billing: <s>Rs <?php echo $totAmountforPopUp; ?>
                                            <?php
                                        } else {
                                            ?>
                                            Minimum Billing: Rs <?php echo $totAmountforPopUp; ?>/-
                                            <?php
                                        }
                                        ?>
                                        <br /><a class="faredetails" href="javascript:void(0)" onmouseover="javascript: _pass(this.id)" id="link_<?php echo $i; ?>">Fare details</a>
                                        <div class="details_wrapper" id="popupbox<?php echo $i; ?>">
                                            <div class="toolarrow"></div>
                                            <div class="heading">Round Trip Fare Details <span class="closedpop">X</span></div>
                                            <p><span>Rs. <?php echo $totAmountforPopUp; //$totAmount;  ?>/-</span></p>
        <?php
        $kmPerDay = intval(($totalKM / 1000) / $interval);
        if ($kmPerDay < 250)
            $displayKMS = 250;
        else
            $displayKMS = intval($kmPerDay * $interval);
        ?>
                                            <p><span>Includes </span></p>
                                            <ul>
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                    <li><?php echo $displayKMS; ?> * <?php echo $interval; ?> = <?php echo intval($displayKMS * $interval); ?> Kms</li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li><?php echo intval($totDistance); ?> Kms</li>
                                                                                               <!-- <li><?php //echo intval(($totalKM/1000)); ?> Kms</li>-->
            <?php
        }
        ?>
                                                <li>Per Km charge = Rs. <?php echo number_format($arr[$i]["KMRate"], 1); ?></li>
                                                <li>No. of days = <?php echo $interval; ?> day(s)</li>
                                                <li>Chauffeur charge = Rs.<?php echo number_format($arr[$i]["ChaufferAllowance"]); ?> * <?php echo $interval; ?></li>
                                                <li>Night Stay Allowance Charge = Rs <?php echo number_format($arr[$i]["NightStayAllowance"]); ?> * <?php echo $intervalnight; ?></li>
                                                <li>Minimum billable kms per day = 250 kms</li>

                                            </ul>
                                            <p><span>Extra Charges</span></p>
                                            <ul>
                                                <li>GST</li>
                                                <li>Tolls, parking and state permits as per actuals</li>
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                    <li>Extra Km beyond <?php echo intval($displayKMS * $interval); ?> =  Rs.<?php echo intval($arr[$i]["KMRate"]); ?>/km</li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li>Extra Km beyond <?php echo intval(($totalKM / 1000)); ?> kms =  Rs.<?php echo intval($arr[$i]["KMRate"]); ?>/km</li>
                                                    <?php
                                                }
                                                if (stripos($_POST["hdDestinationName"], "Tirupati") > -1) {
                                                    ?>
                                                    <li>Approx Rs 1000 to be paid on entry to Tirupati as per actuals</li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                </div>

                                <div class="boknw">
        <?php
        if (count($soldout) == 0) {
            ?>
                                        <input type="button" id="btnBookNow<?php echo $i; ?>" name="btnBookNow" value="Book Now" onclick="javascript: this.form.submit();" />
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo corWebRoot; ?>/images/sold-out.png" border="0" title="Sold out" alt="Sold out" />
                                        <?php
                                    }
                                    ?>
                                </div>
                                   
                            </div>
                        </form>
                                <?php
                                $btnid++;
                            }
                        } else {
                            ?>
                    <div class="repeatdiv">
                        <div class="typefcr">
                            No car found.
                        </div>
                    </div>
                    <?php
                }
                unset($cor);
                ?>
            </div><!--selectcarmid-->
        </div><!--main-->
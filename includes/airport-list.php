<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/secure.js"></script>
<div class="tbbinginr">
<div id="country1" class="tabcontent">
     <div class="selectcartp">
          <div class="main">
               <h2>You Searched For</h2>
               <div class="wd136">
                    <h3>From</h3>
                    <p><?php echo $orgName; ?></p>
               </div>
               <div class="wd136">
                    <h3>Destination</h3>
                    <p><?php echo $destName; ?></p>
               </div>
               <div class="wd136" style="width:200px !important;">
                    <h3>Pickup Date</h3>
                    <p><?php echo $pickDate->format('D d-M, Y'); ?></p>
               </div>
<?php
                    $pickDate = date_create(date('Y-m-d', strtotime($pDate)));
?>
               <!--div class="wd136">
                  <h3>Duration</h3>
                    <p><?php echo $interval; ?> Hours</p>
               </div-->
          </div>
     </div>
     <div class="main">
<?php

				  $isOutStation = false;
				  $AirportYN=true;
                  if($_REQUEST['cabid'] == 1)
                  {
                      $argarray = array("source"=>$orgIDs[0],"dateout"=>$pickDate->format('Y-m-d'),
              "outStationYN"=>false,"AirportYN"=>true,"SubLocationName"=>$_REQUEST['hdDestinationName']);
            
				
                  }
                  else
                  { //do it later
                     $argarray = array("source"=>$orgIDs[0],"dateout"=>$pickDate->format('Y-m-d'),
              "outStationYN"=>false,"AirportYN"=>true,"SubLocationName"=>$_REQUEST['picuploactionName']);
            
                  }
				 
                  
          $cor = new COR();
          $res = $cor->_CORGetCabs_Service($argarray);
          $myXML = new CORXMLList();
          $arr = $myXML->xml4ary($res);

		 
?>
          <div class="selectcarmid">
               <div class="trtp">
                    <label class="frst" style="width:375px">Type of Car</label>
                    <label class="snd">Seating Capacity</label>
                    <label class="lst">Price</label>
               </div>
<?php
          if(count($arr) > 0)
          {
               $dayFare = array();
               $isAvail = array();
               foreach($arr as $k => $v){
                         $dayFare[$k] = $v['DayRate'];
                         $isAvail[$k] = $v['IsAvailable'];
               }
               array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
               for($i = 0; $i < count($arr); $i++){
			   if(trim($arr[$i]['DutyType']) == 'CustomPkg')
				  {
				 
				  $totBaseAmount = intval($arr[$i]["BaseRate"]);
				  $dutytype= $arr[$i]['DutyType'];
				  $totAmount=$arr[$i]['DayRate'];
	   
				  }
				  else
				  {
					$btnid = 1;
					$totAmount = intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval;
					$totDistance = $interval * 10;
					$picdatetime = $pickDate->format('Y-m-d')." ".$_POST["tHour"].":".$_POST["tMin"].":00";
					$date = DateTime::createFromFormat("Y-m-d H:i:s",$picdatetime);
					$date->modify("+".$_POST['cabHour']." hours");
					$droffdatetime =  $date->format("Y-m-d H:i:s");
					$droffdatetimeArray = explode(" ",$droffdatetime);
					$droffhourminutes = explode(":",$droffdatetimeArray[1]);
					$totBaseAmount = intval(($arr[$i]["BaseRate"] / $arr[$i]["PkgHrs"]) * $interval);
				  }
				
					
?>
                    <form id="formBook" name="formBook" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/">
<?php
                         if($arr[$i]["CarCatName"] == 'Intermediate')	
                              $carCatgName = "Budget";
                         else if($arr[$i]["CarCatName"] == 'Van')
                              $carCatgName =  "Family";
                         else if($arr[$i]["CarCatName"] == 'Superior')	
                              $carCatgName =  "Business";
                         else if($arr[$i]["CarCatName"] == 'Premium')	
                              $carCatgName =  "Premium";
                         else if($arr[$i]["CarCatName"] == 'Luxury')	
                              $carCatgName =  "Luxury";
                         else if($arr[$i]["CarCatName"] == 'Special')	
                              $carCatgName =  "Super Luxury";
                         else
                              $carCatgName = $arr[$i]["CarCatName"];
?>
                         <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $carCatgName; ?>" />
                         <input type="hidden" id="hdCabid<?php echo $i; ?>" name="cabid" value="<?php echo $_REQUEST['cabid']; ?>" />
                     
                         <input type="hidden" name="terminalname" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["picuploactionName"]; ?>"/>
                         <input type="hidden" name="terminalid" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["picuploactionid"]?>"/>
                         
                         <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"]?>" />
                         <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"]?>"/>
                         <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]; ?>"/>
                         <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"]?>"/>
                         
                              
                         <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgID"]; ?>" />
                         <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                         <input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["DayRate"]); ?>" />
                         <input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["KMRate"]); ?>" />
                         <input type="hidden" name="PkgHrs" id="PkgHrs<?php echo $i; ?>" value="<?php echo intval($arr[$i]["PkgHrs"]); ?>" />
                         <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />
                         <input type="hidden" name="cabHour" id="cabHour<?php echo $i; ?>" value="<?php echo $_POST["cabHour"]?>" />
                         <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $cor->encrypt($totAmount); ?>" />
                         <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
                         <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y');?>" />
                         <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                         <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"]?>" />
                         <input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo $cor->encrypt($totDistance);?>" />
                         <input type="hidden" name="tHour" id="tHour<?php echo $i; ?>" value="<?php if(isset($_POST["tHour"])){echo $_POST["tHour"];} ?>" />
                         <input type="hidden" name="tMin" id="tMin<?php echo $i; ?>" value="<?php if(isset($_POST["tMin"])){echo $_POST["tMin"];} ?>" />
                         <input type="hidden" name="ChauffeurCharges" id="ChauffeurCharges<?php echo $i; ?>" value="<?php echo $arr[$i]["ChaufferAllowance"]; ?>" />
                         <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $cor->encrypt($totBaseAmount); ?>" />
                         <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["Stax"]); ?>" />
						 <input type="hidden" name="dutytype" id="dutytype<?php echo $i; ?>" value="<?php echo $dutytype;?>" />
						 
						 <input type="hidden" name="searchId" id="searchId"  value="<?php echo $scid=isset($_SESSION['last_search_id'])?$_SESSION['last_search_id']:$last_search_id ?>"/>
                         <input type="hidden" name="usertraffic" id="usertraffic" value="contact-information" />
						 <input type="hidden" name="refclient" id="refclient" value="<?php echo $_SESSION['refclient']; ?>" />
						 <input type="hidden" name="CGSTPercent" id="CGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['CGSTPercent']);?>" />
							<input type="hidden" name="SGSTPercent" id="SGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['SGSTPercent']);?>" />
							<input type="hidden" name="IGSTPercent" id="IGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['IGSTPercent']);?>" />
							<input type="hidden" name="GSTEnabledYN" id="GSTEnabledYN" value="<?php echo $cor->encrypt($arr[$i]['GSTEnabledYN']); ?>"/>
							<input type="hidden" name="ClientGSTId" id="ClientGSTId" value="<?php echo $cor->encrypt($arr[$i]['ClientGSTId']); ?>"/>
                         <div class="repeatdiv">
                              <div class="typefcr">
                                   <div class="imgdv"><a href="javascript: void(0);">
                                        <img src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ","-",$carCatgName); ?>.jpg" alt="" /></a>
                                   </div>
			     <div class="infdv">
<?php
                                   $carModel = "";
                                   $passCount = 4;
                                   if($arr[$i]["CarCatName"] == 'Economy')	
                                             $carModel = "Indica or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Compact')	
                                             $carModel = "Ritz or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Intermediate')	
                                             $carModel = "Swift Dzire or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Van'){	
                                             $carModel =  "Innova or Equivalent<br />";
                                             $passCount = 6;
                                   }
                                   else if($arr[$i]["CarCatName"] == 'Standard')	
                                             $carModel =  "City or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Superior')	
                                             $carModel =  "Corolla or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Premium')	
                                             $carModel =  "Accord or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Luxury')	
                                             $carModel =  "E Class or Equivalent<br />";
                                   else if($arr[$i]["CarCatName"] == 'Special')	
                                   $carModel =  "Mercedes-S Series" . "<br />";
                                               else if($arr[$i]["CarCatName"] == '4 Wheel Drive' || $arr[$i]["CarCatName"] == 'Full Size')	{
                                   $carModel =  "Qualis or Equivalent<br />";
                                   $passCount = 6;
                                   }
?>
                                        <span><?php echo $carCatgName; ?></span><br /><?php echo $carModel; ?>
                                        <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
			     </div>
                              </div>
                              <div class="capcity">
                                   <label class="noof"><?php echo $passCount; ?></label>
                                   <label class="tx">+ driver</label>
                              </div>
<?php
                         $oldDayRate = $arr[$i]["DayRate"];
                         $oldKMRate = $arr[$i]["KMRate"];
                         if(file_exists("./xml/rates/" . strtolower(str_ireplace(" ", "-", $arr[$i]["CarCatName"])) . ".xml")){
                              $rate = file_get_contents("./xml/rates/" . str_ireplace(" ", "-", $arr[$i]["CarCatName"]) . ".xml");
                              if($rate != ""){
                                   $xmlRate = new SimpleXmlElement($rate);
                                   $oldDayRate = $xmlRate->{'dayrate'};
                                   $oldKMRate = $xmlRate->{'kmrate'};
                              }
                         }
						
						  if( $arr[$i]['DutyType'] == 'CustomPkg')
						  {
						  
						  
						  ?>
						  
						  <div class="price">
                                   <span id="ffare<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["DayRate"] ); ?> </span><br />
                                   Minimum Billing: Rs <?php echo  intval($arr[$i]["DayRate"] ) ?><br />
                                   <a class="faredetails" href="javascript:void(0)" onmouseover="javascript: _pass(this.id)" id="link_<?php echo $i; ?>">Fare details</a>
                                   <div class="details_wrapper" id="popupbox<?php echo $i; ?>">
                                        <div class="toolarrow"></div>
                                        <div class="heading">Fare Details <span class="closedpop">X</span></div>
                                        <p><span id="ffarepp<?php echo $i; ?>">Rs <?php echo  intval($arr[$i]["DayRate"] ) ?> </span></p>
                                        <p><span>Minimum Billing</span></p>
                                        <ul>
                                        <li>Rs <?php echo intval($arr[$i]["DayRate"]);?></li>
                                        <li>GST & chauffeur charge inclusive in the fixed rate</li>
                                        <li>Fixed charge is applicable only for one way point to point transfer</li>
                                        <li>No halt or deviation allowed in between the journey.</li>
                                        <li>Chauffeur will decide which route to take to destination.</li>
                                        <li>Toll, Parking, & State tax (If any) to be paid  extra as per actual.</li>
                                        </ul>
                                       
                                        
                                   </div>
                              </div>
						  <?php
						  }
						  else
						  {
						
?>
                              <div class="price">
                                   <span id="ffare<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]); ?> / hr</span><br />
                                   Minimum Billing: Rs <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval; ?><br />
                                   <a class="faredetails" href="javascript:void(0)" onmouseover="javascript: _pass(this.id)" id="link_<?php echo $i; ?>">Fare details</a>
                                   <div class="details_wrapper" id="popupbox<?php echo $i; ?>">
                                        <div class="toolarrow"></div>
                                        <div class="heading">Fare Details <span class="closedpop">X</span></div>
                                        <p><span>Rate</span></p>
                                        <p><big><span id="ffarepp<?php echo $i; ?>">Rs <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]); ?> / hr</span></big></p>
                                        <p><span>Minimum Billing</span></p>
                                        <p>Rs <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval; ?></p>
                                        <p><span>Fare Details</span></p>
                                        <ul>
                                             <li>10 km included for every hour of travel</li>
                                             <li>Rs <?php echo ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10)); ?> / km for additional km</li>
                                             <li>Rs <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval;; ?> minimum billing</li>
                                             <li>New hour billing starts when usage more than 30 mins</li>
<?php
                                             if($_POST["hdOriginID"] == 2) { 
                                                  $charge = 200;
                                                  if($passCount > 4){ $charge = 300;} ?>
                                                  <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
<?php
                                             } 
                                             if($_POST["hdOriginID"] == 7){
?>
                                                  <li>Additional Rs <?php echo (20 * ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
<?php
                                             } 
                                             if($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3){
?>
                                                  <li>Additional Rs <?php echo (20 * ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
<?php
                                             }
?>
                                        </ul>
                                   </div>
                              </div>
							  <?php
							  }
							  ?>
                              <div class="boknw">
<?php 
                                  if(count($soldout)  == 0)
									{
?>
                                   <input type="button" id="btnBookNow<?php echo $i; ?>" name="btnBookNow" value="Book Now" onclick="javascript: this.form.submit();" />
<?php
                                   } else {
?>
                                   <img src="<?php echo corWebRoot; ?>/images/sold-out.png" border="0" title="Sold out" alt="Sold out" />
<?php
                                   }
?>
			</div>

							
                         </div>
                         </form>
<?php
               $btnid++;
               }
          }
          else {
                    
?>
               <div class="repeatdiv">
                    <div class="typefcr">
                         No car found.
                    </div>
               </div>
<?php
          }
	unset($cor);
?>
          </div>
     </div>
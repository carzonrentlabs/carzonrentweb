<?php
    header('Content-type: text/html; charset=utf-8');
    $cCity = "Delhi";
    if(isset($_GET["c"])){
            $cCity = $_GET["c"];
            setcookie("ccity", $cCity);
    }
    else if(isset($_COOKIE["ccity"])){
        $cCity = $_COOKIE["ccity"];
    }
    $cCityID = "2";
    if(isset($_GET["ci"])){
            $cCityID = $_GET["ci"];
            setcookie("ccityID", $cCity);
    }
    else if(isset($_COOKIE["ccityID"])){
        $cCityID = $_COOKIE["ccityID"];
    }
    define('INDEXPERIOD', 86400);
    
    //define('corWebRoot','http://www.carzonrent.com');
	
	define('corWebRoot','http://localhost/carzonrentweb');
    define('ecWebRoot','http://www.easycabs.com');

    function createcache($content, $filename)
    {
        $iFile = fopen($filename, "w");
        fwrite($iFile, $content);
        fclose($iFile);
    }
    
    function getcache($filename, $expiretime)
    {
        if(file_exists($filename) && filemtime($filename) > (time() - $expiretime))
        {
            return file_get_contents($filename);
        }
        return false;
    }
	function encode_base64($sData)
	{
		$sBase64 = base64_encode($sData);
		return strtr($sBase64, '+/', '-_');
	}
	function decode_base64($sData)
	{
		$sBase64 = strtr($sData, '-_', '+/');
		return base64_decode($sBase64);
	}
?>
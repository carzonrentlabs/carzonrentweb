<?php

error_reporting(0);
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');

if (isset($_POST["oid"]))
    $oid = $_POST["oid"];
//$orgTIDs = explode("|", $oid);
$orgIDs = explode(",", $oid);

if (isset($_POST["oname"]))
    $oname = $_POST["oname"];
if (isset($_POST["pd"]))
    $pd = date_create($_POST["pd"]);

if (isset($_POST["dd"]))
    $dd = date_create($_POST["dd"]);
if (isset($_POST["ptH"]))
    $ptH = $_POST["ptH"];
if (isset($_POST["ptM"]))
    $ptM = $_POST["ptM"];
if (isset($_POST["dtH"]))
    $dtH = $_POST["dtH"];
if (isset($_POST["dtM"]))
    $dtM = $_POST["dtM"];
if (isset($_POST["slid"]))
    $slid = $_POST["slid"];
if (isset($_POST["pkgt"]))
    $pkgt = $_POST["pkgt"];
if (isset($_POST["nh"]))
    $nh = $_POST["nh"];
if (isset($_POST["cpkg"]))
    $cpkg = $_POST["cpkg"];
if ($slid != "" && $slid > 0) {
    if ($ptM == "")
        $ptM = "00";
    if ($dtM == "")
        $dtM = "00";
    $pt = $ptH . $ptM;
    $dt = $dtH . $dtM;

    $isInHrs = 0;
    $interval = strtotime($dd->format('Y') . "-" . $dd->format('m') . "-" . $dd->format('d') . " " . $dtH . ":" . $dtM . ":00") - strtotime($pd->format('Y') . "-" . $pd->format('m') . "-" . $pd->format('d') . " " . $ptH . ":" . $ptM . ":00");
    //echo $interval / (60 * 60 * 24);
    $intervalHrs = intval($interval / (60 * 60));
    $totHrs = $intervalHrs;
    $extraHrs = $intervalHrs % 24;
    if ($intervalHrs < 2)
        $intervalHrs = 2;
    $daysI = intval($interval / (60 * 60 * 24));
    if (($interval / (60 * 60)) % 24 > 1) {
        if ($daysI > 0)
            $interval = $daysI + 1;
        else
            $interval = 0;
    } else
        $interval = $daysI;

    if ($interval == 0 && $intervalHrs >= 2) {
        $interval = 1;
        $isInHrs = 1;
    }
    //echo $pd;
    $cor = new COR();
    $myXML = new CORXMLList();
    $arr = array();
    for ($o = 0; $o < count($orgIDs); $o++) {
        //if(count($orgTIDs) > )
        //if($pkgt == "Daily"){
        //    if($nh < 30)
        //    $pkgt = "Daily";
        //    else
        //    $pkgt = "Monthly";
        //}
        $res = $cor->_CORSelfDriveGetCabs(array("CityId" => $orgIDs[$o], "fromDate" => $pd->format('Y-m-d'), "DateIn" => $dd->format('Y-m-d'), "PickupTime" => $pt, "DropOffTime" => $dt, "SubLocation" => $slid, "Duration" => $nh, "PkgType" => $pkgt, "CustomPkgYN" => $cpkg));
        $fleets = $myXML->xml2ary($res);
        for ($f = 0; $f < count($fleets); $f++)
            $fleets[$f]["CityID"] = $orgIDs[$o];
        $arr = array_merge($arr, $fleets);
//            echo "<pre>";
//	    print_r($arr);
//	    echo "</pre>";
    }
    //for($o = 1; $o < count($orgTIDs); $o++){
    //    $res = $cor->_CORSelfDriveGetCabs(array("CityId"=>$orgTIDs[$o], "fromDate"=>$pd->format('Y-m-d'), "DateIn"=>$dd->format('Y-m-d'), "PickupTime"=>$pt, "DropOffTime"=>$dt, "SubLocation" => $slid, "Duration" => $nh, "PkgType" => $pkgt));
    //    $fleets = $myXML->xml2ary($res);
    //    for($f = 0; $f < count($fleets); $f++)
    //         $fleets[$f]["CityID"] = $orgTIDs[$o];
    //    $arr = array_merge($arr, $fleets);
    //}
    //print_r($arr);
    $html = '<div class="trtp">
            <label class="frst">Type of Car</label>
            <label class="snd">Seating Capacity</label>
            <label class="lst">Price</label>
        </div>';
    $dayFare = array();
    $isAvail = array();
    foreach ($arr as $k => $v) {
        $dayFare[$k] = $v['PkgRate'];
        $isAvail[$k] = $v['IsAvailable'];
    }
    array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
    $checkerfornon = 0;
    for ($i = 0; $i < count($arr); $i++) {
        $isShow = false;
        if (!isset($_POST["onlyE2C"]) || $_POST["onlyE2C"] == 0)
            $isShow = true;
        else {
            if ($arr[$i]["CarCatID"] == 21) {
                $isShow = true;
            }
        }
        $btnid = 1;
        $passCount = $arr[$i]["SeatingCapacity"];
        $totAmount = $arr[$i]["BasicAmt"];
        $vat = $arr[$i]["VatRate"];
        $carModel = "";
        $carCatgName = "";
        if ($arr[$i]["CarCatName"] == '4 Wheel Drive')
            $carCatgName = "SUV";
        else
            $carCatgName = $arr[$i]["CarCatName"];
        $carModel = $arr[$i]["model"];
        $secDeposit = $arr[$i]["DepositeAmt"];
        $CarDetail = "";
        $CarMake = "";
        if (intval($_POST["oid"]) != "69") {
            if (strtolower(trim($carModel)) == "toyota innova") {
                $CarDetail = trim($carModel) . " - GX";
                $CarMake = "<small>Make - 2012</small>";
            } else if (strtolower(trim($carModel)) == "maruti swift") {
                $CarDetail = trim($carModel) . " - VDI";
                $CarMake = "<small>Make - 2012</small>";
            } else if (strtolower(trim($carModel)) == "toyota etios") {
                $CarDetail = trim($carModel) . " - GD";
                $CarMake = "<small>Make - 2012</small>";
            } else if (strtolower(trim($carModel)) == "mahindra xuv-500") {
                $CarDetail = trim($carModel) . " - W6";
                $CarMake = "<small>Make - 2012</small>";
            } else if (strtolower(trim($carModel)) == "volkswagen polo" || strtolower(trim($carModel)) == "maruti ertiga" || strtolower(trim($carModel)) == "volkswagen vento" || strtolower(trim($carModel)) == "nissan sunny" || strtolower(trim($carModel)) == "ford endeavour" || strtolower(trim($carModel)) == "toyota fortuner") {
                $CarDetail = trim($carModel);
                $CarMake = "<small>Make - 2013</small>";
            } else if (strtolower(trim($carModel)) == "mahindra e2o") {
                $CarDetail = trim($carModel) . "<br />" . "<small><b>Electric Car</b></small>";
                $CarMake = "<small>Make - 2013</small>";
            } else
                $CarDetail = trim($carModel);
        } else {
            $CarDetail = trim($carModel);
        }
        if ($isShow) {
            if ($carCatgName == "Super")
                $carCatgName = "Super-Luxury";
        }
        
        $totalfare1 = intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));
                        $basicamount1 = ceil($arr[$i]["BasicAmt"]);
                        $res1 = $cor->_CORSelfDriveVariableFlatOffer(array('PromotionCode' => $arr[$i]['scheme'], "CityId" => $orgIDs[0]));
                        $fleets1 = $myXML->xml2ary($res1);


                        $schemestartdatearray = explode('T', $fleets1[0]['StartDate']);
                        $schemeexpirydatearray = explode('T', $fleets1[0]['ExpiryDate']);

                         $pickup_date = date("Y-m-d", strtotime($_REQUEST['pd'])) . ' ' . $_REQUEST['ptH'] . ':' . $_REQUEST['ptM'] . ':00';
                       
                        $pickup_date = strtotime($pickup_date);
                        $dropoff_date = date("Y-m-d", strtotime($_REQUEST['dd'])) . ' ' . $_REQUEST['dtH'] . ':' . $_REQUEST['dtM'] . ':00';
                        $dropoff_date = strtotime($dropoff_date);
                        $timediff = $dropoff_date - $pickup_date;
                         
                        $timediffhour = $timediff / (60 * 60);
                        $timediffday = intval($timediffhour / 24);
                        $remaininghour = $timediffhour % 24;
                        ///
                        $timeFrom = ($_REQUEST['ptH'] * 60) + intval($_REQUEST['ptM']);
                        $timeTo = ($_REQUEST['dtH'] * 60) + intval($_REQUEST['dtM']);

                        //    

                        $scheme_pickup_date = date_create($_REQUEST['pd'])->format("Y-m-d");
                        $scheme_pickup_date = new DateTime($scheme_pickup_date);

                        $scheme_dropoff_date = date_create($_REQUEST['dd'])->format("Y-m-d");
                        $scheme_dropoff_date = new DateTime($scheme_dropoff_date);

                        $scheme_start_date = date_create($schemestartdatearray[0])->format("Y-m-d");
                        $scheme_start_date = new DateTime($scheme_start_date);

                        $scheme_expiry_date = date_create($schemeexpirydatearray[0])->format("Y-m-d");
                        $scheme_expiry_date = new DateTime($scheme_expiry_date);

                        $regdayswithscheme = 0;
                        $premiumdayswithscheme = 0;
                        $regdayswithoutscheme = 0;
                        $premiumdayswithoutscheme = 0;

                        $reg_hours_with_scheme = 0;
                        $premium_hours_with_scheme = 0;
                        $reg_hours_without_scheme = 0;
                        $premium_hours_without_scheme = 0;
                       
                        $premiumdays = array('Fri', 'Sat', 'Sun');
                        $premiumdaysforhours = array('Thu', 'Fri', 'Sat', 'Sun', 'Mon');
                        if ($arr[$i]['scheme'] == '0' || $_REQUEST['pkgt'] == 'Hourly') {
                            
                        } else {
                         
                                $scheme_pickup_date->modify('+1 day');
                                ////
                                if (intval($_REQUEST["oid"]) == "69") {
                                    
                                   
                                    if ($timeFrom >= 480 and $timeTo <= 480) {
                                       // @Duration = @Duration
                                        
                                    } else if ($timeFrom = 480 and $timeTo > 480) {
                                        //@Duration = @Duration + 1
                                                 $scheme_dropoff_date->modify('+1 day');
                                              //   echo '1 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                    } else if ($timeFrom > 480 and $timeTo >= 480) {
                                        //@Duration = @Duration + 1
                                         $scheme_dropoff_date->modify('+1 day');
                                     //    echo '2 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                    } else if ($timeFrom < 480 and $timeTo = 480) {
                                        //@Duration = @Duration + 1
                                         $scheme_dropoff_date->modify('+1 day');
                                      //   echo '3 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                    } else if (@TimeFrom < 480 and @ TimeTo > 480) {
                                       // @Duration = @Duration + 2
                                         $scheme_dropoff_date->modify('+2 day');  
                                        // echo '4 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                    } else if (@TimeFrom < 480 and @ TimeTo < 480) {
                                        //@Duration = @Duration + 1
                                         $scheme_dropoff_date->modify('+1 day');
                                       //  echo '5 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                    }
                                } else {

//                                    if ($remaininghour > 1) {
//                                        $scheme_dropoff_date->modify('+1 day');
//                                       // echo '6 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
//                                    }
                                }
                               // echo '$scheme_pickup_date = '.$scheme_pickup_date.'<br/>';
                              // echo '$scheme_dropoff_date = '.$scheme_dropoff_date.'<br/>';
                              //  ///
                              // echo $scheme_dropoff_date->format("Y-m-d H:i:s");
                             //  die;
//                                for (; $scheme_pickup_date <= $scheme_dropoff_date;) {
////echo "abcd";
//                                    $checkday = $scheme_pickup_date->format('D');
//                                    if (($scheme_pickup_date) < ($scheme_start_date)) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//
//                                            $regdayswithoutscheme++;
//                                        }
//                                    } if ($scheme_start_date->format('Y-m-d') <= $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if ($scheme_pickup_date->format('Y-m-d') <= $scheme_expiry_date->format('Y-m-d')) {
//
//                                            if (in_array($checkday, $premiumdays)) {
//
//                                                $premiumdayswithscheme++;
//                                            } else {
//
//                                                $regdayswithscheme++;
//                                            }
//                                        }
//                                    } if ($scheme_expiry_date->format('Y-m-d') < $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//                                            $regdayswithoutscheme++;
//                                        }
//                                    }
//                                    $scheme_pickup_date->modify('+1 day');
//                                }
                                
                               $argval = array();
                        
                                $argval['CityId'] = $_REQUEST['oid'] ;
                                $argval['PkgType'] = $_REQUEST["pkgt"];
                                $argval['PromotionCode'] = $arr[$i]['scheme'];
                                $argval['FromDate']= date("Y-m-d",strtotime($_REQUEST['pd']));
                                $argval['ToDate']= date("Y-m-d",strtotime($_REQUEST['dd']));
                                $argval['TimeTo']= $_REQUEST['dtH'].$_REQUEST['dtM'];
                                $argval['TimeFrom'] = $_REQUEST['ptH'].$_REQUEST['ptM'];
                                

                                $resultVal = $cor->_CORGetFreedomDays($argval);

                                $fleets_arr = $myXML->xml2ary($resultVal);
                              
                                 $regdayswithscheme = intval("". $fleets_arr[0]['WeekDayScheme']);

                                 $premiumdayswithscheme = intval("". $fleets_arr[0]['WeekEndScheme']);
                                 $regdayswithoutscheme = intval("". $fleets_arr[0]['WeekDayNoScheme']);
                                 $premiumdayswithoutscheme = intval("". $fleets_arr[0]['WeekEndNoScheme']);
                              
                            $reg_days_with_scheme_totalprice = 0;
                            $premium_days_with_scheme_totalprice = 0;
                            $reg_days_without_scheme_totalprice = 0;
                            $premium_days_without_scheme_totalprice = 0;
                            $premium_days_with_scheme_totalprice1 = 0;
                            $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                            $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                            $reg_days_with_scheme_totalprice1 = $regdayswithscheme * $arr[$i]["PkgRate"];

                            if (intval($_REQUEST["oid"]) == "69") {
                                $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRate"];
                                $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekendDis']) / 100);
                                $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRate"];
                            } else {
                                $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                                $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                                $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRateWeekEnd"];
                            }
//                            echo '$reg_days_with_scheme_totalprice1 = '.$reg_days_with_scheme_totalprice1.'<br/>';
//                           echo '$premium_days_with_scheme_totalprice1 = '.$premium_days_with_scheme_totalprice1.'<br/>';
//                            echo '$reg_days_without_scheme_totalprice = '.$reg_days_without_scheme_totalprice.'<br/>';
//                           echo '$premium_days_without_scheme_totalprice = '.$premium_days_without_scheme_totalprice.'<br/>';
                            $totalschemeamount1 = $reg_days_with_scheme_totalprice1  + $premium_days_with_scheme_totalprice1+
                                    $reg_days_without_scheme_totalprice +  $premium_days_without_scheme_totalprice;
                            $totalschemeamount1 = $totalschemeamount1 + ($totalschemeamount1 * $vat/100);

                            $totalschemeamount = $reg_days_with_scheme_totalprice + $premium_days_with_scheme_totalprice +
                                    $reg_days_without_scheme_totalprice +
                                    $premium_days_without_scheme_totalprice;
                            $totAmount = $totalschemeamount;
                            $totalfare1 = intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100));
                            $basicamount1 = $totalschemeamount;
                        }
        if ($isShow) {

            ///////// scheme condition 

            





            ///////// scheme condition
            $html .= '<form id="formBook' . $i . '" name="formBook' . $i . '" method="post" action="">
                    <input type="hidden" id="hdCarModel' . $i . '" name="hdCarModel" value="' . trim($carModel) . '" />
                    
					<input type="hidden" name="urlcurdate" id="hdUrlcurdate' . $i . '" value="';
            if ($_REQUEST['curdatetime'] != '') {
                $html .= $_REQUEST['curdatetime'];
            } else {
                if ($_REQUEST['urlcurdate'] != '') {
                    $html .= $_REQUEST['urlcurdate'];
                }
            }
            $html .= '"/>
                    
					
					<input type="hidden" name="hdOriginName" id="hdOriginName' . $i . '" value="' . $oname . '" />
                    <input type="hidden" name="hdOriginID" id="hdOriginID' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                    <input type="hidden" name="hdDestinationName" id="hdDestinationName' . $i . '" value="' . $oname . '"/>
                    <input type="hidden" name="hdDestinationID" id="hdDestinationID' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                    <input type="hidden" name="hdCarID" id="hdCarID' . $i . '" value="' . $arr[$i]["CarID"] . '" />
                    <input type="hidden" name="hdCarCatID" id="hdCarCatID' . $i . '" value="' . $arr[$i]["CarCatID"] . '" />
                    <input type="hidden" name="hdPkgID" id="hdPkgID' . $i . '" value="' . $arr[$i]["pkgid"] . '" />
                    <input type="hidden" name="hdCat" id="hdCat' . $i . '" value="' . $carCatgName . '" />
                    <input type="hidden" name="PkgRate" id="PkgRate' . $i . '" value="' . intval($arr[$i]["DayRate"]) . '" />';
            if ($pkgt == "Daily" || $pkgt == "Weekly" || $pkgt == "Monthly") {
                $html .= '<input type="hidden" name="duration" id="duration' . $i . '" value="' . $interval . '" />';
            } else {
                $html .= '<input type="hidden" name="duration" id="duration' . $i . '" value="' . $nh . '" />';
            }

            $html .= '<input type="hidden" name="totAmount" id="totAmount' . $i . '" value="' . $totAmount . '" />
                    <input type="hidden" name="totFare" id="totFare' . $i . '" value="' . $totalfare1 . '" />
                    <input type="hidden" name="tab" id="tab' . $i . '" value="2" />				
                    <input type="hidden" name="hdPickdate" id="pickdate' . $i . '" value="' . $pd->format('m/d/Y') . '" />
                    <input type="hidden" name="hdDropdate" id="dropdate' . $i . '" value="' . $dd->format('m/d/Y') . '" />
                    <input type="hidden" name="hdTourtype" id="tourtype' . $i . '" value="Selfdrive" />
                    <input type="hidden" name="tHourP" id="tHourP' . $i . '" value="' . $ptH . '" />
                    <input type="hidden" name="tMinP" id="tMinP' . $i . '" value="' . $ptM . '" />
                    <input type="hidden" name="tHourD" id="tHourD' . $i . '" value="' . $dtH . '" />
                    <input type="hidden" name="tMinD" id="tMinD' . $i . '" value="' . $dtM . '" />
                    <input type="hidden" name="vat" id="vat' . $i . '" value="' . $vat . '" />
                    <input type="hidden" name="secDeposit" id="secDeposit' . $i . '" value="' . $secDeposit . '" />
                    <input type="hidden" name="subLoc" id="subLoc' . $i . '" value="' . $slid . '" />
                    <input type="hidden" name="chkPkgType" id="chkPkgType' . $i . '" value="' . $pkgt . '" />
					<input type="hidden" name="nHours" id="nHours' . $i . '" value="' . $nh . '" />
                    <input type="hidden" name="OriginalAmt" id="OriginalAmt' . $i . '" value="' . ceil($arr[$i]["OriginalAmt"]) . '" />
                    <input type="hidden" name="BasicAmt" id="BasicAmt' . $i . '" value="' . $basicamount1 . '" />
                    <input type="hidden" name="allSubLoc" id="allSubLoc' . $i . '" value="' . $arr[$i]["DetailID"] . '" />
					<input type="hidden" name="KMIncluded" id="KMIncluded' . $i . '" value="' . $arr[$i]["KMIncluded"] . '" />
					
					<input type="hidden" name="WeekDayDuration" id="WeekDayDuration' . $i . '" value="' . ceil($arr[$i]["WeekDayDuration"]) . '" />
					<input type="hidden" name="WeekEndDuration" id="WeekEndDuration' . $i . '" value="' . $arr[$i]["WeekEndDuration"] . '" />
					<input type="hidden" name="FreeDuration" id="FreeDuration' . $i . '" value="' . $arr[$i]["FreeDuration"] . '" />
					
					
					<input type="hidden" name="ExtraKMRate" id="ExtraKMRate' . $i . '" value="' . $arr[$i]["ExtraKMRate"] . '" />'
                    . '<input type="hidden" name="gross_amount_scheme" id="gross_amount_scheme" value="'.$totalschemeamount1.'"/>';

            if ($arr[$i]['scheme'] == '0' || $_REQUEST['pkgt'] == 'Hourly') {
                
            } else {

                $html .= '<input type="hidden" id="hdCoupon' . $i . '" name="hdCoupon" value="' . $arr[$i]['scheme'] . '" />
                            <input type="hidden" id="hdSchemeDiscount' . $i . '" name="hdSchemeDiscount" value="' . intval($arr[$i]["OriginalAmt"] - $totalschemeamount) . '" />';
            }



            $html .= '<div class="repeatdiv">';

            $html .= '<div class="clr"> </div>';
            if (intval($arr[$i]["IsAvailable"]) == 0 && $checkerfornon == 0) {
                $checkerfornon++;
                $html .= '<div class="car_description">
									<b>The following cars are not available for the duration you searched for, however you have the following option(s):</b>
									<ul>';
                $resAdv = $cor->_CORGetAdvanceSearchAvailability(array("ModelID" => $arr[$i]["ModelID"], "CarCatId" => $arr[$i]["CarCatID"], "CityID" => $_POST["oid"], "pickupDate" => $pd->format('Y-m-d'), "droffDate" => $dd->format('Y-m-d'), "SubLocations" => $slid, "PickupTime" => $ptH . $ptM, "DropOffTime" => $dtH . $dtM));
                $AdvResults = $myXML->xml2ary($resAdv->{'GetAdvanceSearchAvailabilityResult'}->{'any'});
                if (intval(count($AdvResults))) {
                   // $html .='<li>You can select the Flexible plans option, if you have flexible plans</li>';
                }
                $html .='<li>You can select the Next available option, if you need the car for a fixed period</li>
									</ul>
									</div>';
            }
            $html .= '<div class="clr"> </div>';


            $html .= '<div class="typefcr">
                                    <div class="imgdv"><a href="javascript: void(0);"><img src="' . corWebRoot . '/images/' . str_ireplace(" ", "-", trim($carModel)) . '.jpg" alt="" /></a>
                                        </div>
                                        <div class="infdv">
                                                <span>' . $CarDetail . '</span><br />';
            if (intval($arr[$i]["IsAvailable"]) == 1)
                $html .= '<small><div class="more-less">
    <div class="more-block">Available at: ' . str_ireplace(':', ', ', (str_ireplace(',', ' ', $arr[$i]["Detail"]))) . '</div><div class="adjust" href="#"></div>
</div></small>';
            $html .= '</div>
                                </div>
                                <div class="capcity">
                                        <label class="noof">' . $passCount . '</label>
                                </div>
                                <div class="price billing">';
            if ($arr[$i]['scheme'] == '0' || $_REQUEST['pkgt'] == 'Hourly') {
                if ($_REQUEST['pkgt'] == 'Daily') {
                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {

                        $html .= '<span id="ffarepp' . $i . '" class="new"> Starting Rs ' . intval($arr[$i]["PkgRate"]);
                        if ($_REQUEST['pkgt'] == 'Daily') {
                            $html .= '/Day</span>';
                        }
                    }
                }

                if ($_REQUEST['pkgt'] == 'Daily') {


                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] == 0 && $arr[$i]["PkgRate"] > 0.00) {

                        $html .= '<span id="ffare' . $i . '" class="new">Rs ' . intval($arr[$i]["PkgRate"]);
                        if ($_REQUEST['pkgt'] == 'Daily') {

                            $html .= '/Day</span>';
                        }
                    }
                }

                if ($_REQUEST['pkgt'] == 'Hourly') {
                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {

                        $html .= '<span id="ffare' . $i . '" class="new">Rs  ' . intval($arr[$i]["PkgRate"]) . '/ Hour</span>';
                    }
                }

                if ($_REQUEST['pkgt'] == 'Daily') {

                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["WeekDayDuration"] == 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {

                        $html .= '<span id="ffare' . $i . '" class="new">Rs  ' . intval($arr[$i]["PkgRateWeekEnd"]);
                        if ($_REQUEST['pkgt'] == 'Daily') {
                            $html .= '/Day</span>';
                        }
                    }
                }


                if ($_REQUEST['pkgt'] == 'Hourly') {
                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {

                        $html .= '<span id="ffare' . $i . '" class="new">Rs  ' . intval($arr[$i]["PkgRateWeekEnd"]) . '/ Hour</span>';
                    }
                }


                if ($pkgt == "Weekly") {
                    $html .= '<span id="ffare' . $i . '" class="new">Rs ' . intval($arr[$i]["BasicAmt"]) . ' / Week</span><br />';
                } elseif ($pkgt == "Monthly") {
                    $html .= '<span id="ffare' . $i . '" class="new">Rs ' . intval($arr[$i]["BasicAmt"]) . ' / Month</span><br />';
                }
                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                    $html .= '<span class="newo"><s style="color:#db4626;">Rs ' . intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))) . '/-</s></span> <span class="newp">Rs ' . intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))) . '/-</span><br />';
                } else {
                    $html .= '<span class="newo"><br>Minimum Billing: Rs ' . intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))) . '</span><br />';
                }
            } else { // scheme 
                if ($pkgt == 'Daily') {


                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {



                        $html .= '<span id="ffare' . $i . '>" class="new block"> Rs' . intval($arr[$i]["PkgRate"]) . ' /- Per Day</span>';

                       
                    }
                }

                if ($pkgt == 'Daily') {


                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] == 0 && $arr[$i]["PkgRate"] > 0.00) {
                        $html .='<span id="ffare' . $i . '" class="new block">Rs' . intval($arr[$i]["PkgRate"]) . '/- Per Day</span>';

                        
                    }
                }



                if ($pkgt == 'Daily') {

                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["WeekDayDuration"] == 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {

                        $html .='<span id="ffare' . $i . '" class="new block">Rs' . intval($arr[$i]["PkgRateWeekEnd"]) . ' /- Per Day </span>';


                       
                    }
                }

                 if ($pkgt == 'Daily') {


                            $html .='<span> <a href="' . corWebRoot . '/thegetdrivingsale.php" target="_blank">The Get Driving Sale</a></span>';
                        }
                        
                if ($pkgt == "Hourly" || $pkgt == "Daily") {

                    if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                        $html .='
                                        <div style="clear: both;"></div>
										
                                        <span class="newp">
										   
                                           <span class="old_bill">
                                             Regular Price: Rs
                                             <strike>' . intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))) . '</strike>
                                           </span>
                                            <span class="new_bill">
                                                Discounted Price: Rs 
                                                <span> ' . $totalfare1 . '/-</span>
                                            </span>
                                        </span>';
                    } else {
                        $html .='
                                        <div style="clear: both;"></div>
                                        <span class="newo">
                                             
                                            <span class="newo">
                                            <span class="old_bill">
                                             Regular Price: Rs
                                             <strike>' . intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))) . '</strike>
                                           </span>
                                            <span class="new_bill">
                                                Discounted Price: Rs 
                                                <span>' . $totalfare1 . ' /-</span>
                                            </span>
                                            </span>
                                        </span>';
                    }
                } else {
                    if ($pkgt == "Weekly") {
                        $html .= '<span> <a href="' . corWebRoot . '/thegetdrivingsale.php" target="_blank">The Get Driving Sale</a></span>';
                    } elseif ($pkgt == "Monthly") {

                        $html .= '<span> <a href="' . corWebRoot . '/thegetdrivingsale.php" target="_blank">The Get Driving Sale</a></span>';
                    }
                    ?>


                    <?php

                    if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                        $html .='
                                            <span><span class="newo">
                                                    <span class="newo">
                                                        <span class="old_bill">
                                                        Regular Price: Rs  
                                                        <strike>' . intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))) . '</strike>
                                                        </span>
                                                </span>
                                                 <span class="new_bill">
                                                     Discounted Price: Rs 
                                                     <span>' . intval($totalfare1) . ' /-</span>
                                                 </span>
                                                </span>
                                            </span>';
                    } else {
                        $html .='
                                                <div style="clear: both;"></div>
                                                <span class="newo">
                                                    <span class="newo">
                                                        <span class="newo">
                                                <span class="old_bill">
                                                      Regular Price: Rs
                                                      <strike>' . intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))) . '</strike>
                                                </span>
                                                <span class="new_bill">
                                                    Discounted Price: Rs
                                                    <span>' . intval($totalfare1) . '/-</span>
                                                </span>
                                                        </span>
                                                </span>';
                    }
                }
            }
            $html .= '<a class="faredetails" href="javascript:void(0)" onmouseover="javascript: var row = ' . $i . ';" id="link_' . $i . '">Fare details</a>';
            if ($arr[$i]['scheme'] == '0' || $_REQUEST['pkgt'] == 'Hourly') {
                $html .= '<div class="details_wrapper" id="popupbox' . $i . '">
                                                <div class="toolarrow"></div>
                                                <div class="heading">Fare Details<span class="closedpop">X</span></div>
                                                ';


                $html .= '<p><span></span></p>
                                            <ul>';

                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {

                        $html .= '<li>Regular days:' . $arr[$i]["WeekDayDuration"] . '@' . $arr[$i]["PkgRate"] . '</li>';
                    }
                }
                if ($_REQUEST['pkgt'] == 'Hourly') {
                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {

                        $html .= '<li>No. of hours:' . $arr[$i]["WeekDayDuration"] . '@' . $arr[$i]["PkgRate"] . '</li>';
                    }
                }


                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {

                        $html .= '<li>Premium days:' . $arr[$i]["WeekEndDuration"] . ' @' . $arr[$i]["PkgRateWeekEnd"] . '</li>';
                    }
                }


                if ($_REQUEST['pkgt'] == 'Hourly') {
                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {

                        $html .= '<li>No. of hours:' . $arr[$i]["WeekEndDuration"] . '@' . $arr[$i]["PkgRateWeekEnd"] . '</li>';
                    }
                }


                $html .= '<li>Minimum Billing: Rs ' . intval($arr[$i]["BasicAmt"]) . '</li>
                                            <li>VAT (@' . number_format($vat, 2) . '%): Rs ' . intval(ceil(($arr[$i]["BasicAmt"] * $vat) / 100)) . '</li>
                                            <li>Total Fare: Rs ' . intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)) . '</li>';
                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                    $html .= '<li>Discount Amount (On base fare): Rs ' . (intval($arr[$i]["OriginalAmt"]) - intval($arr[$i]["BasicAmt"])) . '</li>';
                }
                if (intval($oid) != "69") {
                    $html .= '<li>Refundable Security Deposit  (Pre Auth from card): Rs ' . $secDeposit . ' (Mastercard and Visa only)</li>';
                } else {
                    $html .= '<li>The billing cycle starts from 8am each day</li>
                                                        <li>Refundable Security Deposit: Rs ' . $secDeposit . ' (To be paid in cash before the start of the journey)</li>
                                                        <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                        <li>The vehicle is to be driven within the permissible limits of Goa.</li>';
                }
                $html .= '</ul>';
                if ($pkgt == "Hourly") {
                    $html .= '<p><span>Includes</span></p>';
                    $html .= '<ul>
                                                        <li>KMs Included: ' . $arr[$i]["KMIncluded"] . '</li>
                                                        <li>Extra KM Charge: Rs. ' . $arr[$i]["ExtraKMRate"] . '/- per KM</li>
                                                </ul>';
                }
                $html .= '<p><span>Mandatory Documents</span></p>';
                if ($oid != "69") {
                    $html .= '<ul>
                                                        <li>Passport</li>
                                                        <li>Driving License</li>
                                                        <li>Credit Card</li>
                                                </ul>';
												
					  $html .= ' <ul>
                                <li>The minimum age required to rent a Myles-Nano is 18yrs. However, it is 23yrs for all other cars. </li>
                                <li>The identification documents needs to be in the name of the person who is booking the car.</li>
                            </ul>';							
												
												
												
                } else {
                    $html .= '<ul>
                                                        <li>Passport</li>
                                                        <li>Driving License</li>
                                                        <li>Any of the following has to be submitted in original as an identity proof.
                                                                <ol type="1">
                                                                        <li>Adhaar card</li>
                                                                        <li>Pan card</li>
                                                                        <li>Voter ID card</li>
                                                                </ol>
                                                        </li>
                                                        <li>Please note that the car in Goa will be provided through a partner.</li>
                                                </ul>';
												
												
							$html .= '<p><span>Eligibilty</span></p>';					
						    $html .= ' <ul>
                                <li>The minimum age required to rent a Myles-Nano is 18yrs. However, it is 23yrs for all other cars. </li>
                                <li>The identification documents needs to be in the name of the person who is booking the car.</li>
                            </ul>';
												
                }
                $html .= '</div>';
                $html .= '</div>';
            } else {

                //scheme code

                $html .= '<div class="details_wrapper" id="popupbox' . $i . '">
                                            <div class="toolarrow"></div>
                                            <div class="heading"><!--Fare Details--><span class="closedpop">X</span></div>
                                            <br>
                                            <p><span>Fare Details</span></p>


                                            <ul>';
                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($regdayswithoutscheme > 0 ) {

                        $html .= '<li>Weekdays:' . $regdayswithoutscheme . '*' . intval($arr[$i]["PkgRate"]) . '</li>';

                        $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                    }
                }



                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($premiumdayswithoutscheme > 0 ) {

                        $html .= '<li>Weekends :' . $premiumdayswithoutscheme . '*' . intval($arr[$i]["PkgRateWeekEnd"]) . '</li>';

                        $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                    }
                }


                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($regdayswithscheme > 0) {

                        $html .= '<li>Weekdays with offer ' . $fleets1[0]['WeekdayDis'] . '% :' . $regdayswithscheme . '*' . intval($arr[$i]["PkgRate"]) . '</li>';

                        $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                    }
                }

                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    if ($premiumdayswithscheme > 0 ) {
                        if (intval($_REQUEST["oid"]) == "69") {
                              $html .='<li>Weekend with offer '.$fleets1[0]['WeekendDis'].' % : '. $premiumdayswithscheme.'*'. intval($arr[$i]["PkgRate"]).'</li>';
                        }
                        else
                        {
                            $html .= '<li>Weekends with offer ' . $fleets1[0]['WeekendDis'] . '% :' . $premiumdayswithscheme . '*' . intval($arr[$i]["PkgRateWeekEnd"]) . '</li>';
                        }
                        
                        

                        $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                    }
                }

                $html .= '<li>Regular Price: Rs. <strike>' . intval($arr[$i]["BasicAmt"]) . '</strike>' . $totalschemeamount;

                $html .= '/-</li>';
                $html .= '<li>VAT (@' . number_format($vat, 2) . '%): Rs ' . intval(ceil(($totalschemeamount * $vat) / 100)) . '/-</li>';

                if ($_REQUEST['pkgt'] == 'Daily' || $_REQUEST['pkgt'] == 'Monthly' || $_REQUEST['pkgt'] == 'Weekly') {

                    $html .= '<li>Total fare: Rs. ' . intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)) . ' /-</li>';

                    $html .= '<li><strong>Discounted Price:</strong>' . intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100)) . ' /-</li>';
                } else {
                    $html .= '
                                                     <li>Total fare: Rs.';

                    $html .= intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));

                    $html .= ' /-</li>';
                }

                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {

                    $html .= '<li>Discount Amount (On base fare): Rs. ' . intval($totalfare1) . '/-</li>';
                }
                if (intval($_REQUEST["oid"]) != "69") {

                    $html .= '<li>Refundable security deposit (pre-auth from card): Rs. ' . $secDeposit . ' (Mastercard/Visa/Amex)</li>';
                } else {

                    $html .= '<li>The billing cycle starts from 8am each day</li>
                                                    <li>Refundable Security Deposit: Rs. ' . $secDeposit . ' (To be paid in cash before the start of the journey)</li>
                                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>';
                }

                $html .= '</ul>

                                            <p><span>Mandatory Documents(Original)</span></p>';

                if (intval($_REQUEST["oid"]) != "69") {

                    $html .= '<ul>
                                                    <li>Passport / Voter ID card</li>
                                                    <li>Driving license</li>
                                                    <li>Credit card</li>
                                                </ul>';
                } else {
                    $html .= '
                                                <ul>
                                                    <li>Passport / Voter ID card</li>
                                                    <li>Driving license</li>
                                                    <li>Any of the following has to be submitted in original as an identity proof.
                                                        <ol type="1">
                                                            <li>Adhaar card</li>
                                                            <li>Pan card</li>
                                                            <li>Voter ID card</li>
                                                        </ol>
                                                    </li>
                                                    <li>Please note that the car in Goa will be provided through a partner.</li>
                                                </ul>';
                }

                $html .= '</div>';
            }
          
            $nddate = "";
            if (intval($arr[$i]["IsAvailable"]) == 0) {
                $resNext = $cor->_COR_SelfDrive_NextAvailibility(array("ModelID" => $arr[$i]["ModelID"], "CarCatID" => $arr[$i]["CarCatID"], "CityID" => $_POST["oid"], "PickUpDate" => $pd->format('Y-m-d'), "DropOffDate" => $dd->format('Y-m-d'), "subLocationID" => $slid, "DropOffTime" => $dtH . $dtM));
                $NADate = $myXML->xml2ary($resNext);
                $nextAvailablity = date_create($NADate[0]['Pickupdate'], timezone_open("Asia/Calcutta"));

                if (intval(count($AdvResults))) {

                    //$html .= '<a id = "popup' . $i . '" class="pflexi" onclick ="div_show(' . $i . ');">Flexible Plans? <img border="0" src="images/flexi_arrow.png"/></a>';
                }
                $html .= '<div class="boknw next_avai" style="padding-top:0px;"><label><a class="next_available_flex" href="javascript: void(0);" onclick="booknowsubmit(\'formSrchINA' . $i . '\');">Next Available <img border="0" src="images/flexi_arrow.png"/></a><b>' . $nextAvailablity->format('d-M, Y H:i') . '</b> </label></div>';


                $datHM = explode(':', $nextAvailablity->format('H:i'));
                $dateH = $datHM[0];
                $dateM = $datHM[1];

                $Date1 = $nextAvailablity->format('Y-m-d');
                $nddate = date('Y-m-d', strtotime($Date1 . " + " . $interval . " day"));
                $enddate = date('j M, Y', strtotime($nddate));
                //$curr_date = date('j M, Y',strtotime($pd));
                $curr_date = date('j M, Y');
            } else {
                if (!$isOffer) {
                    $html .= '<div class="boknw"><input type="button" id="btnBookNow' . $i . '" name="btnBookNow" value="Book Now" onclick="subform('.$i.');" 
					/>';
                } else {
                    $html .= '<div class="boknw"><img src="' . corWebRoot . '/images/sold-out.png" border="0" title="Sold out" alt="Sold out" /></div>';
                }
            }
			  $html .= '</div>';
            $html .= '</div></div></form>';
            if (intval($arr[$i]["IsAvailable"]) == 0) {

                //echo '<pre>';print_r($AdvResults);die;
                if ((count($AdvResults))) {
                    $html .= '<div id="flaxi_option' . $i . '" class="flaxi_option"></div>
								<div id="popup_flaxi' . $i . '" class="pop_flexi"> 
								<h1>Best Available Options For Your Selected Travel Period</h1>
								<div class="close_popup"><a  onclick ="div_hide(' . $i . ');" href="javascript:void(0)">X</a></div>';
                    $f = 0;
                    foreach ($AdvResults as $AdvResult) {
                        $pick = str_split($AdvResult["Pickuptime"], 2);
                        $pickH = $pick[0];
                        $pickM = $pick[1];

                        $drop = str_split($AdvResult["Dropofftime"], 2);
                        $dropH = $drop[0];
                        $dropM = $drop[1];

                        $pkdate = date_create($AdvResult["Pickupdate"]);
                        $dkdate = date_create($AdvResult["Dropoffdate"]);

                        $html .= '<div class="p20"><div class="row">
											<div class="f_l w_24 mr5">
												<span>Pick Up Date and Time</span>
												<input type="text" value="' . $pkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Pickuptime"])) . '" readonly="readonly">
											</div>
											<div class="f_l w_24 mr5">
												<span>Drop Off Date and Time</span>
												<input type="text" value="' . $dkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Dropofftime"])) . '" readonly="readonly">
											</div>
											<div class="f_l w_24 mr3">
											<span>Avilable At</span>
												' . $AdvResult['SublocationName'] . '
											</div>
											<div class="f_l w_12">
												<input type="button" value="Book Now" name="Book Now" onclick="booknowsubmit(\'formSrchFlexi' . $f . '\');">
											</div>
												<div class="clr"> </div>
										</div></div>';


                        $html .= '<form id="formSrchFlexi' . $f . '" name="formSrchFlexi' . $f . '" action="" method="POST">
					   <input type="hidden" name="urlcurdate" id="hdUrlcurdate' . $i . '" value="' . $_REQUEST['curdatetime'] . '" />
					   <input type="hidden" id="hdCarModelINA' . $i . '" name="hdCarModel" value="' . $carModel . '" />
                        <input type="hidden" name="hdOriginName" id="hdOriginNameINA' . $i . '" value="' . $oname . '" />
						<input type="hidden" id="hdCarModelID' . $i . '" name="hdCarModelID" value="' . $arr[$i]["ModelID"] . '" />
                        <input type="hidden" name="hdOriginID" id="hdOriginIDINA' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                        <input type="hidden" name="hdDestinationName" id="hdDestinationNameINA' . $i . '" value="' . $oname . '"/>
                        <input type="hidden" name="hdDestinationID" id="hdDestinationIDINA' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                        <input type="hidden" name="hdCarID" id="hdCarIDINA' . $i . '" value="' . $AdvResult["carID"] . '" />
                        <input type="hidden" name="hdCarCatID" id="hdCarCatIDINA' . $i . '" value="' . $arr[$i]["CarCatID"] . '" />
                        <input type="hidden" name="hdPkgID" id="hdPkgIDINA' . $i . '" value="' . $arr[$i]["pkgid"] . '" />
                        <input type="hidden" name="hdCat" id="hdCatINA' . $i . '" value="' . $arr[$i]["CarCatName"] . '" />
                        <input type="hidden" name="PkgRate" id="PkgRateINA' . $i . '" value="' . intval($arr[$i]["DayRate"]) . '" />
                        <input type="hidden" name="duration" id="durationINA' . $i . '" value="' . $interval . '" />
                        <input type="hidden" name="totAmount" id="totAmountINA' . $i . '" value="' . $totAmount . '" />
                        <input type="hidden" name="totFare" id="totFareINA' . $i . '" value="' . ($totAmount + $vaTax) . '" />
                        <input type="hidden" name="tab" id="tabINA' . $i . '" value="2" />				
                        <input type="hidden" name="hdPickdate" id="pickdateINA' . $i . '" value="' . $pkdate->format('j M, Y') . '" />
                        <input type="hidden" name="hdDropdate" id="dropdateINA' . $i . '" value="' . $dkdate->format('j M, Y') . '" />
                        <input type="hidden" name="hdTourtype" id="tourtype' . $i . '" value="Selfdrive" />
                        <input type="hidden" name="tHourP" id="tHourP' . $i . '" value="' . $pickH . '" />
                        <input type="hidden" name="tMinP" id="tMinP' . $i . '" value="' . $pickM . '" />
                        <input type="hidden" name="tHourD" id="tHourD' . $i . '" value="' . $dropH . '" />
                        <input type="hidden" name="tMinD" id="tMinD' . $i . '" value="' . $dropM . '" />
                        <input type="hidden" name="vat" id="vatINA' . $i . '" value="' . $vat . '" />
                        <input type="hidden" name="secDeposit" id="secDepositINA' . $i . '" value="' . $secDeposit . '" />
                        <input type="hidden" name="subLoc" id="subLoc' . $i . '" value="' . $AdvResult['SubLocationID'] . '" />
                        <input type="hidden" name="chkPkgType" id="chkPkgType' . $i . '" value="' . $pkgt . '" />
                        <input type="hidden" name="nHours" id="nHours' . $i . '" value="' . $nh . '" />
                        <input type="hidden" name="OriginalAmt" id="OriginalAmt' . $i . '" value="' . ceil($arr[$i]["OriginalAmt"]) . '" />
                        <input type="hidden" name="BasicAmt" id="BasicAmt' . $i . '" value="' . ceil($arr[$i]["BasicAmt"]) . '" />
                        <input type="hidden" name="allSubLoc" id="allSubLoc' . $i . '" value="' . $AdvResult['SubLocationID'] . '" />
                        <input type="hidden" name="KMIncluded" id="KMIncluded' . $i . '" value="' . $arr[$i]["KMIncluded"] . '" />
                        <input type="hidden" name="ExtraKMRate" id="ExtraKMRate' . $i . '" value="' . $arr[$i]["ExtraKMRate"] . '" />
						
						<input type="hidden" name="WeekDayDuration" id="WeekDayDuration' . $i . '" value="' . $arr[$i]["WeekDayDuration"] . '" />
                        <input type="hidden" name="WeekEndDuration" id="WeekEndDuration' . $i . '" value="' . $arr[$i]["WeekEndDuration"] . '" />
                        <input type="hidden" name="FreeDuration" id="FreeDuration' . $i . '" value="' . $arr[$i]["FreeDuration"] . '" />
						
						
                        </form>';

                        $f++;
                    }
                    $html .= '</div>';
                }


                $html .= '<form id="formSrchINA' . $i . '" name="formSrchINA' . $i . '" action="search-result.php" method="GET">
                        
						<input type="hidden" name="hdToday" id="hdToday' . $i . '" value="' . $curr_date . '" />
						<input type="hidden" id="hdCarModelINA' . $i . '" name="hdCarModel" value="' . $carModel . '" />
                        <input type="hidden" name="urlcurdate" id="hdUrlcurdate' . $i . '" value="' . $_REQUEST['curdatetime'] . '"/>
                        <input type="hidden" name="hdOriginName" id="hdOriginNameINA' . $i . '" value="' . $oname . '" />
						<input type="hidden" name="hdOriginID" id="ddlOriginIDINA' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                        <input type="hidden" name="ddlOrigin" id="ddlOriginIDINA' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                        <input type="hidden" name="hdDestinationName" id="hdDestinationNameINA' . $i . '" value="' . $oname . '"/>
                        <input type="hidden" name="hdDestinationID" id="hdDestinationIDINA' . $i . '" value="' . $arr[$i]["CityID"] . '"/>
                        <input type="hidden" name="hdCarID" id="hdCarIDINA' . $i . '" value="' . $arr[$i]["CarID"] . '" />
                        <input type="hidden" name="hdCarCatID" id="hdCarCatIDINA' . $i . '" value="' . $arr[$i]["CarCatID"] . '" />
                        <input type="hidden" name="hdPkgID" id="hdPkgIDINA' . $i . '" value="' . $arr[$i]["pkgid"] . '" />
                        <input type="hidden" name="hdCat" id="hdCatINA' . $i . '" value="' . $arr[$i]["CarCatName"] . '" />
                        <input type="hidden" name="PkgRate" id="PkgRateINA' . $i . '" value="' . intval($arr[$i]["DayRate"]) . '" />
                        <input type="hidden" name="duration" id="durationINA' . $i . '" value="' . $interval . '" />
						<input type="hidden" name="onlyE2C" id="onlyE2C' . $i . '" value="0" />
                        <input type="hidden" name="totAmount" id="totAmountINA' . $i . '" value="' . $totAmount . '" />
                        <input type="hidden" name="totFare" id="totFareINA' . $i . '" value="' . ($totAmount + $vaTax) . '" />
                        <input type="hidden" name="tab" id="tabINA' . $i . '" value="1" />				
                        <input type="hidden" name="pickdate" id="pickdateINA' . $i . '" value="' . $nextAvailablity->format('j M, Y') . '" />
                        <input type="hidden" name="dropdate" id="dropdateINA' . $i . '" value="' . $enddate . '" />
                        <input type="hidden" name="hdTourtype" id="tourtypeINA' . $i . '" value="Selfdrive" />
                        <input type="hidden" name="tHourP" id="tHourP' . $i . '" value="' . $dateH . '" />
                        <input type="hidden" name="tMinP" id="tMinP' . $i . '" value="' . $dateM . '" />
                        <input type="hidden" name="tHourD" id="tHourD' . $i . '" value="' . $dateH . '" />
                        <input type="hidden" name="tMinD" id="tMinD' . $i . '" value="' . $dateM . '" />
                        <input type="hidden" name="vat" id="vatINA' . $i . '" value="' . $vat . '" />
                        <input type="hidden" name="secDeposit" id="secDepositINA' . $i . '" value="' . $secDeposit . '" />
                        <input type="hidden" name="subLoc" id="subLoc' . $i . '" value="' . $slid . '" />
                        <input type="hidden" name="chkPkgType" id="chkPkgType' . $i . '" value="' . $pkgt . '" />
                        <input type="hidden" name="nHours" id="nHours' . $i . '" value="' . $nh . '" />
						<input type="hidden" name="customPkg" id="customPkg' . $i . '" value="' . $cpkg . '" />
						<input type="hidden" name="userTime" id="userTimeSF' . $i . '" value="" />
                        <input type="hidden" name="OriginalAmt" id="OriginalAmt' . $i . '" value="' . ceil($arr[$i]["OriginalAmt"]) . '" />
                        <input type="hidden" name="BasicAmt" id="BasicAmt' . $i . '" value="' . ceil($arr[$i]["BasicAmt"]) . '" />
                        <input type="hidden" name="allSubLoc" id="allSubLoc' . $i . '" value="' . $arr[$i]["DetailID"] . '" />
                        <input type="hidden" name="KMIncluded" id="KMIncluded' . $i . '" value="' . $arr[$i]["KMIncluded"] . '" />
                        <input type="hidden" name="ExtraKMRate" id="ExtraKMRate' . $i . '" value="' . $arr[$i]["ExtraKMRate"] . '" />
						<input type="hidden" name="bookTime" id="bookTimeSF' . $i . '" value="' . $_POST["bookTime"] . '" />
						<input type="hidden" name="WeekDayDuration" id="WeekDayDuration' . $i . '" value="' . $arr[$i]["WeekDayDuration"] . '" />
                        <input type="hidden" name="WeekEndDuration" id="WeekEndDuration' . $i . '" value="' . $arr[$i]["WeekEndDuration"] . '" />
                        <input type="hidden" name="FreeDuration" id="FreeDuration' . $i . '" value="' . $arr[$i]["FreeDuration"] . '" />

                        </form>';
            }
        }
    }
    //unset($myXML);
    //unset($cor);
} else {
    $html = '<div class="trtp">
            <label class="frst">Type of Car</label>
            <label class="snd">Capacity</label>
            <label class="lst">Price</label>
        </div>';
    $html .= '<div class="repeatdiv">
                   <div class="typefcr">
                           No car found.
                   </div>
           </div>';
}
echo $html;
?>
<!--Header Start Here-->
<?php
	include_once('../classes/cor.mysql.class.php');
?>
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/wz_tooltip.js"></script>
<?php
    $corH = new COR();
    $res = $corH->_CORGetCities();	
    $myXML = new CORXMLList();
    $orgH = $myXML->xml2ary($res);
    $orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    $orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    unset($corH);
    
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    $refURL = $_SERVER['HTTP_REFERER'];
    $refURLParts = split("/", $refURL);
    $domain = $refURLParts[2];
    if(strpos($refURLParts[3], "q=") != false)
	    $qstr = split("q=", $refURLParts[3]);
    else if(strpos($refURLParts[3], "p=") != false)
	    $qstr = split("p=", $refURLParts[3]);
    $qterm = split("&", $qstr[1]);
    $qterm = urldecode($qterm[0]);
    if($qterm != ""){
	    $searchquery = $qterm;
	    $arrIllegalChars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','`','{','}','[',']',':',';','?',',','.','<','>','/','|','\\');
	    $searchquery = str_replace($arrIllegalChars, '', $searchquery);
	    $searchquery = str_replace("'", '', $searchquery);
	    $searchquery = str_replace('"', '', $searchquery);
	    $searchquery = str_replace('  ', ' ', $searchquery);
	    $searchquery = str_replace(' ', '-', $searchquery);
	    
		$searchData = array();
		$srchDB = new MySqlConnection(CONNSTRING);
		$srchDB->open();
	
		$searchData["search_string"] = $qterm;
		$searchData["entry_date"] = "NOW()";
		$searchData["search_url"] = $searchquery;
		$searchData["search_engine"] = $domain;
		$searchData["page_url"] = $pageURL;

		unset($searchData);
		$srchDB->close();
    }
?>
<!--<div id="header">
	<div class="main">
	<div class="logo">
	  <?php
	      if(isset($_COOKIE["coragtoken"]) && $_COOKIE["coragtoken"] != "")
	      {
	  ?>
		<a href="<?php echo corWebRoot; ?>/home.php" id="brand" class="corbrand"></a>
	  <?php
	      }
	      else
	      {
	  ?>
		<a href="<?php echo corWebRoot; ?>/" id="brand" class="corbrand"></a>
	  <?php
	      }
	  ?>
	      
	</div>
        <div class="logo-right">
        	<div class="logo-righttop">
            	<ul class="lf">
                    <li><a href="<?php echo corWebRoot; ?>" target="_blank" class="active">COR-Car rentals</a></li>
                    <li><a href="<?php echo corWebRoot; ?>/rdurl.php" target="_blank">COR-Easycabs</a></li>
                    <li><a href="<?php echo corWebRoot; ?>/cor-lease/index.php" target="_blank">COR-leasing</a></li>
                </ul>
                <ul class="rt">
                <?php
                    if(isset($_COOKIE["gname"]))
		{
                        if($_COOKIE["gname"] != ""){
                ?>
                    <li class="no"><?php
		    echo "Hi <a onmouseover=\"Tip('Get your booking history and profile details.')\" onmouseout=\"UnTip()\" href='" . corWebRoot . "/myaccount.php'>" . $_COOKIE["gname"] . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"" . corWebRoot . "/logout.php\">Logout</a>";
		    ?></li>
                <?php
                        }
                        else {
                ?>
                        <li><a href="http://corporate.carzonrent.com/Corweb/CorporateSite/Corporatelogin.asp" target="_blank">Corporate User</a></li>
                        <li><a href="<?php echo corWebRoot; ?>/login.php">Login</a></li>
                        <li class="no"><a href="<?php echo corWebRoot; ?>/register.php">Register</a></li>
                <?php
                        }
                    }
                    else 
		    {
			    if((isset($_GET["src"]) && $_GET["src"] != "" && $_GET["src"] != "MastiIndia") && $_COOKIE["corsrc"] != "MastiIndia" || (isset($_COOKIE["corsrc"]) && isset($_COOKIE["corsrc"]) != "" && $_COOKIE["corsrc"] != "MastiIndia"))
			    {
				    if(isset($_GET["src"]))
					    $partnerlogo = $_GET["src"];
				    if(isset($_COOKIE["corsrc"]))
					    $partnerlogo = $_COOKIE["corsrc"];
	    ?>
			    <li class="indigo"><a href="javascript:void(0)"><img src="<?php echo corWebRoot; ?>/images/partner/<?php echo $partnerlogo ?>.png" alt="<?php echo $partnerlogo; ?>" title="<?php echo $partnerlogo; ?>" border="0" /></a></li>
	    <?php	
			    }
			    if(isset($_COOKIE["coragtoken"]) && $_COOKIE["coragtoken"] != "")
			    {
                ?>
                    <li><a href="javascript: void(0)">Welcome Agent </a></li>
                    <li style="border:none !important"><a href="<?php echo corWebRoot; ?>/logout.php">Logout</a></li>
                <?php
			  }
                    }
                ?>
                  
                </ul><div style="clear:both;"></div>
            </div>
            <div class="menu">
            	<ul>
		<?php
                    //if(strpos($_SERVER["REQUEST_URI"], "packages.php"))
                    //    $pgName = corWebRoot . "/packages.php";
		    $pgName = corWebRoot;
		    $defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
                    $defCityID = array(2, 4, 7, 6);
			//$maxI = 4;
			//if(!in_array($cCity, $defCity))
			//	$maxI = 3;
			//if($maxI == 3){
		?>
			<li><a href="<?php echo $pgName . "/car-rental-" . $cCity . "-" . $cCityID; ?>/" class="active"><?php echo $cCity; ?></a></li>
		<?php
			//}
			for($i = 0; $i < count($defCity); $i++){
                            if($cCity != $defCity[$i]){
		?>
				<li><a href="<?php echo $pgName . "/car-rental-" .  $defCity[$i] . "-" . $defCityID[$i]; ?>/"><?php echo $defCity[$i]; ?></a></li>
		<?php
			    }
			}
		?>
			<li class="more"><a href="#">More</a>
                    	<ul class="submenu">
		<?php
			for($i = 0; $i < count($orgH); $i++)
			{
                            if($cCity != $orgH[$i]["CityName"] && !in_array($orgH[$i]["CityName"], $defCity)){
		?>
			<li><a href="<?php echo $pgName. "/car-rental-" .  $orgH[$i]["CityName"] . "-" . $orgH[$i]["CityID"]; ?>/"><?php echo $orgH[$i]["CityName"]; ?></a></li>
		<?php
                            }
			}
		?>
                        </ul>
                    </li>
                    <li>&nbsp;</li>
		    <li class="tfn">Call 24 hours <strong class="ccn">0888 222 2222</strong></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_right"></div>
</div>-->

<!--Top Logo & Socail Icon-->    
<div class="top">
    <div class="cor-logo"><a href="<?php echo corWebRoot; ?>/"><img src="<?php echo corWebRoot; ?>/images/COR-logo.gif" /></a></div>
    <div class="social-icon">
	<ul>
	  <li><a href="https://www.facebook.com/carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/FB-icon.gif" border="0" /></a></li>
	  <li><a href="https://plus.google.com/+carzonrent" target="_blank"><img src="<?php echo corWebRoot; ?>/images/googleplus.gif" border="0" /></a></li>
	  <li><a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="<?php echo corWebRoot; ?>/images/Twitter.gif" border="0" /></a></li>
	  <li><a href="http://www.youtube.com/carzonrenttv" target="_blank"><img src="<?php echo corWebRoot; ?>/images/You-tube.gif" border="0" /></a></li>
	  <li><a target="_blank" href="http://blog.carzonrent.com/"><img border="0" src="http://www.carzonrent.com/images/blog.gif"></a></li>
	</ul>
    <br><div class="login">
    <?php
        if(isset($_COOKIE["gname"])){
            if($_COOKIE["gname"] != ""){
		echo "Hi <a onmouseover=\"Tip('Get your booking history and profile details.')\" onmouseout=\"UnTip()\" href='" . corWebRoot . "/myaccount.php'>" . $_COOKIE["gname"] . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"" . corWebRoot . "/logout.php\">Logout</a>";
	    } else {
    ?>
	<a href="http://book.carzonrent.com/" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a>
    <?php
	    }
	} else {
    ?>
	<a href="http://book.carzonrent.com" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a>
    <?php
	}
    ?>
    </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--End-->
<!--Header Start-->

</div> <!-- d100p ends -->
    <div class="top-header">
    <div  class="top-mid">
	<ul>
	<?php
	    $pgName = corWebRoot;
	    $arrCityID = array(2, 4, 7, 6);
	    if(trim($cCityID) != "0"){
		if(in_array($cCityID, $arrCityID)){
		    $defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		    $defCityID = array(2, 4, 7, 6);
		} else {
		    $defCity = array("Delhi", "Mumbai", "Bangalore");
		    $defCityID = array(2, 4, 7);
		}
	    } else {
		$defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		$defCityID = array(2, 4, 7, 6);
	    }
	    if(trim($cCity) != "" && $cCityID != "0"){
	?>
	    <li class="active"><a href="<?php echo $pgName . "/car-rental-" . $cCity . "-" . $cCityID; ?>/"><?php echo $cCity; ?></a></li>
	<?php
	    }
	    for($i = 0; $i < count($defCity); $i++){
		if($cCity != $defCity[$i]){
	?>
		<li><a href="<?php echo $pgName . "/car-rental-" .  $defCity[$i] . "-" . $defCityID[$i]; ?>/"><?php echo $defCity[$i]; ?></a></li>
	<?php
		}
	    }
	?>
	    <li id="tpmore">More<img src="<?php echo corWebRoot; ?>/images/down-aarow.gif" style="padding-left:10px;">
		<label>
		    <ul class="moremenu">
	<?php
			for($i = 0; $i < count($orgH); $i++){
			    if($cCity != $orgH[$i]["CityName"] && !in_array($orgH[$i]["CityName"], $defCity)){
	?>
			    <li><a href="<?php echo $pgName. "/car-rental-" .  $orgH[$i]["CityName"] . "-" . $orgH[$i]["CityID"]; ?>/"><?php echo $orgH[$i]["CityName"]; ?></a></li>
	<?php
			    }
			}
	?>
		    </ul>
		</label>
	    </li>
	</ul>
    <div class="top-number"><span class="top-number2">Call 24 hours</span>  0888 222 2222</div>
   <div class="clearfix"></div>
  </div>
</div>
<div class="clearfix"></div>
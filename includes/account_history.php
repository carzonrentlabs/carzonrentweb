<div id="country2" class="tabcontent">
	<div class="myprofile">
	<?php
		$arr = array();
		$userId = "";
		if(isset($_COOKIE["cciid"]) || isset($_COOKIE["emailid"])){
			if($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != ""){
				$userId = $_COOKIE["cciid"];
				$cor = new COR();
				//$res = $cor->_CORGetBookings(array("userId"=>"923482"));
				//$res = $cor->_CORGetSelfDriveBookings(array("userId"=>$userId));
				$res = $cor->_CORGetBookings(array("userId"=>$userId));
				$myXML = new CORXMLList();
				$arr = $myXML->xml2ary($res->{'RetrieveBookingResult'}->{'any'});
				//echo '<pre>'; print_r($arr); die;
				unset($cor);
				unset($myXML);
			}
		}
	?>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="booking_summary_data booking_new">
		<tr>
			<th align="left" valign="middle" width="11%">Booking ID</th>
			<th align="left" valign="middle" width="16%">Car Name</th>
			<th align="left" valign="middle" width="15%">Pickup Address</th>
			<th align="left" valign="middle" width="10%">Travel Date</th>
			<th align="left" valign="middle" width="10%">Status</th>
		</tr>
	<?php
		$flag = 1;
		if(count($arr)) {
			
			
			for($i = 0; $i < count($arr); $i++){
				$pDate = strtotime($arr[$i]["PickUpDate"]);
				
				$dropdate = date_create($arr[$i]['dropoffdate']);
				$DateVal = $dropdate->format('Y-m-d');
				
				$Current_Date = date('Y-m-d');
				if(strtotime($Current_Date) > strtotime($DateVal))
				{		
				
	?>
		<tr>
			<td align="left" valign="top"><span><?php echo $arr[$i]["BookingID"]; ?></span></td>
			<td align="left" valign="top"><?php echo $arr[$i]["CarModelName"]; ?></td>
			<td align="left" valign="top"><?php if($arr[$i]["PickUpAdd"] == array()){ echo '----';}else{ echo $arr[$i]["PickUpAdd"]; } ?></td>
			<td align="left" valign="top"><?php echo date('d M, Y', $pDate); ?> <?php echo $arr[$i]["PickUpTime"]; ?>Hrs</td>
			<td align="left" valign="top"><div class="status_can"><?php if($arr[$i]["Tripstatus"] == 'Cancel'){ echo 'Cancelled';}else{echo 'Closed';} ?></div></td>
		</tr>
	<?php
				}
			}
		}else{
			
	?>
	
			<tr>
			<td align="left" colspan="5" ><?php echo 'No account history available.'; ?></td>
			</tr>
	
		<?php 
		
		}
		
		?>
	</table>
	</div>
</div>	
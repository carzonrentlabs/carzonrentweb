<!--Header Start Here-->
<?php
$service = $_COOKIE['servicetype'];
?>

<?php
	 include_once('../classes/cor.mysql.class.php');
?>
<link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/enfeedback.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo corWebRoot; ?>/js/wz_tooltip.js"></script>
<?php
    $corH = new COR();
    $res = $corH->_CORGetCities();	
    $myXML = new CORXMLList();
    $orgH = $myXML->xml2ary($res);
    $orgH[] = array("CityID" => 11, "CityName" => "Ghaziabad");
    $orgH[] = array("CityID" => 3, "CityName" => "Faridabad");
    unset($corH);
    
    $pageURL = 'http';
	if(isset($_SERVER['HTTPS'])) {
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
    
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
	if(isset($_SERVER['HTTP_REFERER'])) {
    $refURL = $_SERVER['HTTP_REFERER'];
	$refURLParts = explode("/", $refURL);
	
    
    $domain = $refURLParts[2];
    if(strpos($refURLParts[3], "q=") != false)
	    $qstr = explode("q=", $refURLParts[3]);
    else if(strpos($refURLParts[3], "p=") != false)
	    $qstr = explode("p=", $refURLParts[3]);
    $qterm = explode("&", $qstr[1]);
    $qterm = urldecode($qterm[0]);
    if($qterm != ""){
	    $searchquery = $qterm;
	    $arrIllegalChars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','`','{','}','[',']',':',';','?',',','.','<','>','/','|','\\');
	    $searchquery = str_replace($arrIllegalChars, '', $searchquery);
	    $searchquery = str_replace("'", '', $searchquery);
	    $searchquery = str_replace('"', '', $searchquery);
	    $searchquery = str_replace('  ', ' ', $searchquery);
	    $searchquery = str_replace(' ', '-', $searchquery);
	    
		$searchData = array();
		$srchDB = new MySqlConnection(CONNSTRING);
		$srchDB->open();
	
		$searchData["search_string"] = $qterm;
		$searchData["entry_date"] = date('Y-m-d H:i:s');
		$searchData["search_url"] = $searchquery;
		$searchData["search_engine"] = $domain;
		$searchData["page_url"] = $pageURL;

		unset($searchData);
		$srchDB->close();
    }
	}

?>
<!--Top Logo & Socail Icon-->    
<div class="top">
    <div class="cor-logo"><a href="<?php echo corWebRoot; ?>/"><img src="<?php echo corWebRoot; ?>/images/COR-logo.gif" width="182" height="49" alt="Carzonrent India Pvt Ltd." title="Carzonrent India Pvt Ltd."/></a></div>
    <div class="download-icon" style="width:auto; float:left;text-align: right; margin-left:363px;">
	
	<a href="http://bit.ly/CorRideIOS"><img src="<?php echo corWebRoot; ?>/images/app_store.png"></a>
	<a href="http://bit.ly/CorRideAndroid"><img src="<?php echo corWebRoot; ?>/images/google_play.png"></a>
	
	</div>
	
	<div class="social-icon">
	<ul>
	
	  
	  <li><a href="https://www.facebook.com/carzonrent" target="_blank"><img width="32" height="33" src="<?php echo corWebRoot; ?>/images/FB-icon.gif"  border="0" alt="Facebook" title="Facebook"  /></a></li>
	    <li><a href="https://twitter.com/CarzonrentIN" target="_blank"><img width="32" height="33" src="<?php echo corWebRoot; ?>/images/Twitter.gif"  border="0" alt="Twitter" title="Twitter"  /></a></li>
	  <li><a href="https://www.linkedin.com/company/carzonrent-india-pvt-ltd" target="_blank"><img width="32" height="33" src="<?php echo corWebRoot; ?>/images/linkdin.png"  border="0" alt="Linkedin" title="Linkedin"/></a></li>
	  <li><a href="https://plus.google.com/+carzonrent" target="_blank"><img width="32" height="33" src="<?php echo corWebRoot; ?>/images/googleplus.gif" border="0" alt="Google Plus" title="Google Plus"/></a></li>
	  <li><a href="http://www.youtube.com/carzonrenttv" target="_blank"><img width="32" height="33" src="<?php echo corWebRoot; ?>/images/You-tube.gif" border="0" alt="YouTube" title="YouTube" /></a></li>
	  <li><a href="http://blog.carzonrent.com/" target="_blank"><img width="32" height="33" border="0" src="<?php echo corWebRoot; ?>/images/blog.gif" alt="Blog" title="blog" /></a></li>
	</ul>
    <br/><div class="login">
    <?php
        if(isset($_COOKIE["gname"])){
            if($_COOKIE["gname"] != ""){
		echo "Hi, ".$_COOKIE["gname"]."&nbsp;&nbsp;|&nbsp;&nbsp;<a onmouseover=\"Tip('Get your booking history and profile details.')\" onmouseout=\"UnTip()\" href='" . corWebRoot . "/myaccount.php'>My Account</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"" . corWebRoot . "/logout.php\">Logout</a>";
	    } else {
    ?>
	<a href="http://book.carzonrent.com" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a> <!--<a href="<?php // echo corWebRoot; ?>/faq.php" class="btn-special" target="_blank">Myles FAQs</a>-->
    <?php
	    }
	} else {
    ?>
	<a href="http://book.carzonrent.com" target="_blank">Corporate User</a> | <a href="<?php echo corWebRoot; ?>/login.php">Login</a> | <a href="<?php echo corWebRoot; ?>/register.php">Register</a> <!--<a href="<?php //echo corWebRoot; ?>/faq.php" class="btn-special" target="_blank">Myles FAQs</a>-->
    <?php
	}
    ?>
    </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--End-->
<!--Header Start-->

</div> <!-- d100p ends -->
    <div class="top-header">
    <div  class="top-mid">
	<ul>
	<?php
	    $pgName = corWebRoot;
	    $arrCityID = array(2, 4, 7, 6);
	    if(trim($cCityID) != "0"){
		if(in_array($cCityID, $arrCityID)){
		    $defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		    $defCityID = array(2, 4, 7, 6);
		} else {
		    $defCity = array("Delhi", "Mumbai", "Bangalore");
		    $defCityID = array(2, 4, 7);
		}
	    } else {
		$defCity = array("Delhi", "Mumbai", "Bangalore", "Hyderabad");
		$defCityID = array(2, 4, 7, 6);
	    }
	  
		
		$words = explode(' ', $_SERVER['REQUEST_URI']);
		$showword = trim($words[count($words) - 1], '/');
		$velData1=explode("/",$showword);
		$maintabName=$velData1[0];
		
		
		if($_SERVER['SCRIPT_NAME']  || $maintabName)
		{
			
            $vel=$_SERVER['SCRIPT_NAME'];
			$velData=explode("/",$vel);
			if($velData[1]=='index.php' || $velData[1]==''){
	        if($_COOKIE && $_COOKIE['servicetype']=='Selfdrive'){?>
								<li id="outstationLI" title=""><a href="javascript:window.location.href='<?php echo corWebRoot; ?>/logout.php?tourtype=outstation'">Outstation</a></li>
								<li id="localLI" title=""><a href="javascript:window.location.href='<?php echo corWebRoot; ?>/logout.php?tourtype=local'">Local</a></li>
								<?php }else{?>
								<li id="outstationLI" class="currentTab"><a href="javascript: _setTab('outstation');loadpopup('outstation')">Outstation </a></li>
								<li id="localLI"><a href="javascript: _setTab('local');loadpopup('local')">Local</a></li>
								<li id="airportLI"><a href="javascript: _setTab('airport');">Airport Transfer</a></li>
								
                             <?php }?>
                                
            
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
                        <?php if(isset($_COOKIE) && $_COOKIE['servicetype']=='outstation'){?>
                        <li id="selfdriveLI" title=""><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
                        <?php } else{?>
                                <li id="selfdriveLI" ><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
                        <?php }?>
						<li><a href="http://business.carzonrent.com">Business Travel</a></li>
			
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
	<?php
			}
			
			elseif($maintabName=='outstation')
			{
			?>
			<li id="outstationLI" class="currentTab"><a href="<?php echo corWebRoot; ?>/#outstation">Outstation</a>
			<li id="localLI"><a href="<?php echo corWebRoot; ?>/#local" onclick="loadpopup('local')">Local</a></li>
			<li id="airportLI"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
			<li id="selfdriveLI"><a href="http://www.mylescars.com">Self-Drive</a></li>
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			
			<?php
			
			}
			
			else if($maintabName=='airport')
			{
			?>
			<li id="outstationLI"><a href="<?php echo corWebRoot; ?>/#outstation" onclick="loadpopup('outstation')">Outstation</a>
			<li id="localLI" ><a href="<?php echo corWebRoot; ?>/#local">Local</a></li>
			<li id="airportLI" class="currentTab"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
			<li id="selfdriveLI"><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
			<?php
			}
			
			
			else if($maintabName=='local')
			{
			?>
			<li id="outstationLI"><a href="<?php echo corWebRoot; ?>/#outstation" onclick="loadpopup('outstation')">Outstation</a>
			<li id="localLI" class="currentTab"><a href="<?php echo corWebRoot; ?>/#local">Local</a></li>
			<li id="airportLI"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
			<li id="selfdriveLI"><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<?php
			}
			
			elseif($velData[1]=='home.php'){
	?>
			<li id="outstationLI"><a href="javascript: _setActive('outstation');loadpopup('outstation')">Outstation</a></li>
			<li id="localLI"><a href="javascript: _setActive('local');loadpopup('local')">Local</a></li>
			<li id="airportLI" class="currentTab"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
			<li id="selfdriveLI"><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
	<?php			
			} elseif($velData[1]=='page.php'){?>
			<li id="outstationLI" class="currentTab" ><a href="<?php echo corWebRoot; ?>/#outstation" onclick="loadpopup('outstation')">Outstation</a>
			<li id="localLI"><a href="<?php echo corWebRoot; ?>/#local" onclick="loadpopup('local')">Local</a></li>
			<li id="airportLI"><a href="javascript: _setActive('airport');">Airport Transfer</a></li>
			<!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
			<li id="selfdriveLI"><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
			<li><a href="http://business.carzonrent.com">Business Travel</a></li>
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
			
			<?php }else {//
                            
                             if($_COOKIE && $_COOKIE['servicetype']=='Selfdrive'){?>
                                <li id="outstationLI" title=""><a href="javascript:window.location.href='<?php echo corWebRoot; ?>/logout.php?tourtype=outstation'" onclick="loadpopup('outstation')">Outstation</a></li>
                                <li id="localLI" title=""><a href="javascript:window.location.href='<?php echo corWebRoot; ?>/logout.php?tourtype=local'">Local</a></li>
                             <?php }else{?>
                                <li id="outstationLI" ><a href="<?php echo corWebRoot; ?>/#outstation" onclick="loadpopup('outstation')">Outstation</a></li>
                                <li id="localLI"><a href="<?php echo corWebRoot; ?>/#local">Local</a></li>
								  <li id="airportLI"><a href="<?php echo corWebRoot; ?>/#airport">Airport Transfer</a></li>
								  
                             <?php }?>
                                
                             
			
			            <!--<li><a href="http://www.easycabs.com/">EasyCabs</a></li>-->
                        <?php if(isset($_COOKIE) && $_COOKIE['servicetype']=='outstation'){?>
                        <li id="selfdriveLI" title=""><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>
                        <?php } else{?>
                                <li id="selfdriveLI"><a href="http://www.mylescars.com" target="_blank">Self-Drive</a></li>  
                        <?php }?>
						<li><a href="http://business.carzonrent.com">Business Travel</a></li>
					     
			
			<!--<li id="internationalLI"><a href="http://international.carzonrent.com/booking/car.cfm?aff=CARZONRENT" target="_blank">International</a></li>-->
	<?php
			}
	?>
	</ul>
    <div class="top-number"><span class="top-number2">Call 24 hours</span> 011-41841212</div>
   <div class="clearfix"></div>
   <?php
	}
   ?>
  </div>
</div>
<div class="clearfix"></div>
<script>
    function logOut() {
       
        setcookie('userDetails[username]',"", time()-1200);
        unset($_COOKIE['userDetails']);
    }
    </script>
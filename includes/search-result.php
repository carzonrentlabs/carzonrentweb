<script>

    function div_show(val) {

        document.getElementById('flaxi_option' + val).style.display = "block";
        document.getElementById('popup_flaxi' + val).style.display = "block";
        $('.flexibleplanclickid' + val).attr('id', 'flexibleplanclickid');
    }
    function div_hide(val) {
        document.getElementById('flaxi_option' + val).style.display = "none";
        document.getElementById('popup_flaxi' + val).style.display = "none";
        $('.flexibleplanclickid' + val).attr('id', '');
    }
</script>
<?php
error_reporting(0);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
include_once('./classes/cor.ws.class.php');
include_once('./classes/cor.xmlparser.class.php');
include_once('./classes/cor.mysql.class.php');
include_once('./classes/cor.gp.class.php');
include_once("./includes/cache-func.php");

$cor = new COR();
$res = $cor->_CORGetCities();
$myXML = new CORXMLList();
$org = $myXML->xml2ary($res);
$res = $cor->_CORGetDestinations();
$des = $myXML->xml2ary($res);
unset($res);
unset($cor);
$tCCIID = 0;

if (isset($_POST["hdOriginName"])) {
    $orgName = $_POST["hdOriginName"];
    $orgNames = explode(",", $orgName);
}
if (isset($_POST["hdOriginID"])) {
    $orgID = $_POST["hdOriginID"];
    $orgIDs = explode(",", $orgID);
}
if (isset($_POST["hdDestinationName"])) {
    $destName = $_POST["hdDestinationName"];
    $destNames = explode(",", $destName);
}
if (isset($_POST["hdDestinationID"])) {
    $destID = $_POST["hdDestinationID"];
    $destIDs = explode(",", $destID);
}
if (isset($_POST["pickdate"])) {
    $pDate = str_ireplace(",", "", $_POST["pickdate"]);
    $pickDate = date_create($pDate);
}
if (isset($_POST["dropdate"])) {
    if (trim($_POST["hdTourtype"]) == "Selfdrive") {
        if (isset($_POST["chkPkgType"]) && trim($_POST["chkPkgType"]) == "Hourly") {
            $dDate = str_ireplace(",", "", $_POST["pickdate"]);
            $dropDate = date_create($dDate . " " . $_POST["tHourP"] . ":" . $_POST["tMinP"] . ":00");
            $dropDate->modify("+" . intval($_POST["nHours"]) . " hours");
        } else {

            $dDate = str_ireplace(",", "", $_POST["dropdate"]);
            $dropDate = date_create($dDate . " " . $_POST["tHourD"] . ":" . $_POST["tMinD"] . ":00");
        }
    } else {
        $dDate = str_ireplace(",", "", $_POST["dropdate"]);
        $dropDate = date_create($dDate);
    }
}

$tab = 1;
if (isset($_POST["tab"])) {
    $tab = $_POST["tab"];
}
if ($tab == 1) {
    $offerStartDate = date_create("2014-12-31");
    $offerEndDate = date_create("2014-12-31");
    if ($_POST["hdTourtype"] != "Selfdrive" && isset($_POST["hdOriginID"]) && $_POST["hdOriginID"] == 5) {
        if ((strtotime($pickDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($pickDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
            $isOffer = true;
        if ($_POST["hdTourtype"] != "Local") {
            if ((strtotime($dropDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($dropDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
                $isOffer = true;
        }
    }

    $ll = "";
    $dll = "";
    if ($_POST["hdTourtype"] == "Outstation") {
        if ($destName == 'Asola') {
            $destName = 'Asola-Delhi';
        }
        $travelData = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
//$travelData = json_decode(file_get_contents('http://www.bytesbrick.com/app-test/cor/t.php?origin=' . urlencode($orgName) . '&destination=' . urlencode($orgName) . '&waypoints=' . urlencode(str_ireplace(",", "|", $destName)) . '&sensor=false'));
        $points = count($travelData->{'routes'}[0]->{'legs'});
        $ll = $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} . "," . $travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'};
        $dll = ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lat'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lat'}) . "," . ($travelData->{'routes'}[0]->{'bounds'}->{'northeast'}->{'lng'} - $travelData->{'routes'}[0]->{'bounds'}->{'southwest'}->{'lng'});
        $totalKM = 0;
        for ($i = 0; $i < $points; $i++) {
            $dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
            if ($i == 0)
                $dwstate = $dispOrg;
            $dispDest = $travelData->{'routes'}[0]->{'legs'}[$i]->{'end_address'};
            if ($dwstate == "")
                $dwstate = $dispDest;
            else
                $dwstate .= " to:" . $dispDest;
            $totalKM += $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"value"};
        }
    }


    if ($_POST["hdTourtype"] == "Outstation") {
        $interval = strtotime($dDate) - strtotime($pDate);
        $interval = ($interval / (60 * 60 * 24)) + 1;

        $intervalnight = strtotime($dDate) - strtotime($pDate);
        $intervalnight = ($intervalnight / (60 * 60 * 24));
    } else if ($_POST["hdTourtype"] == "Selfdrive") {

        if ($_POST['chkPkgType'] == 'Hourly') {
            if ($_POST['nHoursJS']) {
                $interval = $_POST['nHoursJS'];
            } else {
                $interval = $_POST['nHours'];
            }
        } else {
            $isInHrs = 0;
            $cor = new COR();
            $pckDte = $pickDate->format('Y') . "-" . $pickDate->format('m') . "-" . $pickDate->format('d');
            $drpDte = $dropDate->format('Y') . "-" . $dropDate->format('m') . "-" . $dropDate->format('d');
            $drpTym = $_REQUEST["tHourD"] . $_REQUEST["tMinD"];
            $pckTym = $_REQUEST["tHourP"] . $_REQUEST["tMinP"];
            $res = $cor->_CORGetInterval(array("PickUpDate" => $pckDte, "DropOffDate" => $drpDte, "PickupTime" => $pckTym, "DropOffTime" => $drpTym, "CityId" => $orgID, "PkgType" => $_REQUEST["chkPkgType"]));
            $myXML = new CORXMLList();
            $arrUserId = $myXML->xml2ary($res->{'SelfDrive_GetTotalDurationResult'}->{'any'});
            $interval = $arrUserId[0]['Duration'];
        }
    } else {
        $dropDate = date_create(date('Y-m-d', strtotime($pDate)));
        $interval = 1;
        $interval = $_POST["cabHour"];
    }

    if ($interval <= 0 && $intervalHrs < 2)
        header("Location: " . corWebRoot);


    $db = new MySqlConnection(CONNSTRING);
    $db->open();

    $isDiscountGiven = 0;
    $ip = $_SERVER["REMOTE_ADDR"];
    $ua = $_SERVER["HTTP_USER_AGENT"];
    $tType = $_POST["hdTourtype"];
    $isSearched = 0;
    if (!isset($_COOKIE["CORCustID"])) {
        $CORCustID = strtoupper(uniqid("CMID-"));
        setcookie("CORCustID", $CORCustID, time() + 3600 * 24, "/");
    } else {
        $CORCustID = $_COOKIE["CORCustID"];
        $Rs = $db->query("stored procedure", "cor_check_search_data('" . $CORCustID . "', '" . $orgID . "', '" . $destID . "', '" . $pickDate->format('Y-m-d') . "', '" . $interval . "', '" . $tType . "', 'COR-MOBILE')");
        $isSearched = $Rs[0]["Total"];
    }

    if ($isSearched == 0) {

        $eTime = date("H:i:s");
        $searchData = array();
        $searchData["customer_id"] = $CORCustID;
        $searchData["origin_name"] = $orgName;
        $searchData["origin_code"] = $orgID;
        $searchData["destination_name"] = $destName;
        $searchData["destination_code"] = $destID;
        $searchData["pickup_date"] = $pickDate->format('Y-m-d');
        $searchData["duration"] = $interval;
        $searchData["distance"] = ceil(($totalKM / 1000));
        $searchData["ip"] = $ip;
        $searchData["user_agent"] = $ua;
        $searchData["entry_date"] = "CURDATE()";
        $searchData["entry_time"] = $eTime;
        $searchData["tour_type"] = $tType;
        $searchData["website"] = "COR-MOBILE";
        $searchData["rental_type"] = $_REQUEST["chkPkgType"];
        $Rs = $db->insert("customer_search", $searchData);
        unset($searchData);
    }
    $db->close();


    $isDiscountShow = 0;
    if ($_POST["hdTourtype"] == "Outstation") {
        if (ceil(($totalKM / 1000) / $interval) >= 150 && ceil(($totalKM / 1000) / $interval) <= 500) {
            $isDiscountShow = 1;
        }
    }
    mysql_close($con);
}
$totalKM = 0;


/* * ************ extra code ********** */


if ($tab == 4) {
    $fullname = explode(" ", trim($_POST["name"]));
    if (count($fullname) > 1) {
        $firstName = $fullname[0];
        $lastName = $fullname[1];
    } else {
        $firstName = $fullname[0];
        $lastName = "";
    }
    $cor = new COR();
    if (($_POST["hdTourtype"]) != "Selfdrive") {
        $res = $cor->_CORCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
    } else {
        $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
    }
    $myXML = new CORXMLList();
    $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
    if (intval($arrUserId[0]["ClientCoIndivID"]) > 0) {
        $mtime = round(microtime(true) * 1000);
        if ($_POST["hdTourtype"] == "Selfdrive") { } else {
            
            $pkgId = $_POST["hdPkgID"];
            $destination = $_POST["hdDestinationName"];
            $TimeOfPickup = $_POST["picktime"];
            $address = $_POST["address"];
            $phone = $_POST["monumber"];
            $emailId = $_POST["email"];
            $userId = $_POST["cciid"];
            $paymentAmount = $_POST["totFare"];

            $paymentType = "5";
            $paymentStatus = "0";
            $trackId = $_POST["hdTrackID"];
            $transactionId = $_POST["hdTransactionID"];
            $remarks = $_POST["hdRemarks"];
            $totalKM = $_POST["hdDistance"];
            $visitedCities = $_POST["hdDestinationName"];
            $pincode = $_POST["pincode"];
            $gpAddress = $_POST["gpaddress"];
            $dispc = $_POST["discount"];
            $discountAmt = $_POST["discountAmt"];
            $empcode = !empty($_POST["empcode"])?$_POST["empcode"] : 0;
            if (trim($empcode) == "Promotion code")
                $empcode = "0";
            $disccode = !empty($_POST["disccode"])?$_POST["disccode"] : 0;
            if (trim($disccode) == "Discount coupon number")
                $disccode = "0";
            $interval = $_POST["duration"];
            $outStationYN = "true";
            if (isset($_COOKIE["corsrc"]))
                $srcCookie = $_COOKIE["corsrc"]; //Aamir 1-Aug-2012
            if (isset($_COOKIE["gclid"]))
                $srcCookie = $_COOKIE["gclid"]; //Iqbal 10-Oct-2012
            $tourType = $_POST["hdTourtype"];
            if ($_POST["hdTourtype"] != "Local")
                $outStationYN = "true";
            else
                $outStationYN = "false";
            if (isset($_POST["hdPickdate"])) {
                $pDate = $_POST["hdPickdate"];
                $pickDate = date_create($pDate);
            }
            if (isset($_POST["hdDropdate"])) {
                $dDate = $_POST["hdDropdate"];
                $dropDate = date_create($dDate);
            }
            $dateOut = $pickDate->format('m/d/Y');
            $dateIn = $dropDate->format('m/d/Y');

            session_start();
            $_SESSION["pkgId"] = $_POST["hdPkgID"];
            $_SESSION["hdCat"] = $_POST["hdCat"];
            $_SESSION["hdCarModel"] = $_POST["hdCarModel"];
            $_SESSION["hdOriginName"] = $_POST["hdOriginName"];
            $_SESSION["hdTourtype"] = $_POST["hdTourtype"];
            $_SESSION["hdDestinationName"] = $_POST["hdDestinationName"];
            $_SESSION["hdPickdate"] = $_POST["hdPickdate"];
            if (isset($_POST["picktime"])) {
                $_SESSION["picktime"] = $_POST["picktime"];
            } else {
                $_SESSION["picktime"] = $_POST["tHour"] . $_POST["tMin"];
            }
            $_SESSION["hdDropdate"] = $_POST["hdDropdate"];
            $_SESSION["address"] = $_POST["address"];
            $_SESSION["remarks"] = $_POST["hdRemarks"];
            $_SESSION["totFare"] = $_POST["totFare"];
            $_SESSION["rdoPayment"] = $_POST["rdoPayment"];
            $_SESSION["dayRate"] = $_POST["dayRate"];
            $_SESSION["kmRate"] = $_POST["kmRate"];
            $_SESSION["duration"] = $_POST["duration"];
            $_SESSION["hdDistance"] = $_POST["hdDistance"];
            $_SESSION["name"] = $_POST["name"];
            $_SESSION["monumber"] = $_POST["monumber"];
            $_SESSION["email"] = $_POST["email"];
            $_SESSION["PkgHrs"] = $_POST["PkgHrs"];
            $_SESSION["cciid"] = $_POST["cciid"];
            $_SESSION["ChauffeurCharges"] = $_POST["ChauffeurCharges"];
            $_SESSION["dispc"] = $_POST["discount"];
            $_SESSION["discountAmt"] = $_POST["discountAmt"];
            $_SESSION["empcode"] = $_POST["empcode"];
            $_SESSION["disccode"] = $_POST["disccode"];
            $_SESSION["orderid"] = $transactionId;

            $corArrSTD = array();
            $corArrSTD["pkgId"] = $pkgId;
            $corArrSTD["destination"] = $destination;
            $corArrSTD["dateOut"] = $dateOut;
            $corArrSTD["dateIn"] = $dateIn;
            $corArrSTD["TimeOfPickup"] = $TimeOfPickup;
            $corArrSTD["address"] = $address;
            $corArrSTD["firstName"] = $firstName;
            $corArrSTD["lastName"] = $lastName;
            $corArrSTD["phone"] = $phone;
            $corArrSTD["emailId"] = $emailId;
            $corArrSTD["userId"] = $userId;
            $corArrSTD["paymentAmount"] = $cor->decrypt($paymentAmount);
            $corArrSTD["paymentType"] = $paymentType;
            $corArrSTD["paymentStatus"] = $paymentStatus;
            $corArrSTD["trackId"] = $trackId;
            $corArrSTD["transactionId"] = $transactionId;
            $corArrSTD["discountPc"] = $dispc;
            $corArrSTD["discountAmount"] = $cor->decrypt($discountAmt);
            $corArrSTD["remarks"] = $remarks;
            $corArrSTD["totalKM"] = intval($cor->decrypt($totalKM));
            $corArrSTD["visitedCities"] = $visitedCities;
            
            if ($_POST["hdTourtype"] == "Outstation")
                $outStationYN = "true";
            else
             $outStationYN = "false";
            $corArrSTD["outStationYN"] = $outStationYN;
            $corArrSTD["CustomPkgYN"] = "false";
            $corArrSTD["OriginCode"] = $srcCookie; //Aamir 1-Aug-2012
            $corArrSTD["chkSum"] = "0";
            $corArrSTD["DisCountCode"] = $disccode; //Iqbal 09-Jan-2013
            $corArrSTD["PromotionCode"] = $empcode; //Iqbal 09-Jan-2013
            $corArrSTD["IsPayBack"] = $_SESSION["IsPayBack"];
            $corArrSTD["discountTrancastionID"] = 0;
            $corArrSTD["latitude"] = $_REQUEST["city_location_lat"];
            $corArrSTD["longitude"] = $_REQUEST["city_location_long"];
            
             
            $res = $cor->_CORMakeBookingWithLatLong($corArrSTD);
           
            $myXML = new CORXMLList();
            if ($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'} != "") {
                 
                $arrUserId = $myXML->xml2ary($res->{'SetTravelDetails_WithPickupLatLonResult'}->{'any'});
           
                if ($arrUserId[0]["Column1"] > 0) {
                  
                    $query_string = "";
                    if ($_POST) {
                        $kv = array();
                        foreach ($_POST as $key => $value) {
                            if ($key != "corpassword")
                                $kv[] = "$key=$value";
                        }

                        $query_string = join("&", $kv);
                    }
                    else {
                        $query_string = $_SERVER['QUERY_STRING'];
                    }
                    $db = new MySqlConnection(CONNSTRING);
                    $db->open();
                    $whereData = array();
                    $dataToSave = array();
                    $whereData["coric"] = $transactionId;
                    $dataToSave["payment_mode"] = "2";
                    $dataToSave["payment_status"] = "1";
                    $dataToSave["booking_id"] = $arrUserId[0]["Column1"];
                    $r = $db->update("cor_booking_new", $dataToSave, $whereData);
                    unset($whereData);
                    unset($dataToSave);

                    if ($disccode != "") {
                        $whereData = array();
                        $whereData["voucher_number"] = $disccode;
                        $dataToSave = array();
                        $dataToSave["is_booked"] = "1";
                        $r = $db->update("dicount_vouchers_master", $dataToSave, $whereData);
                        unset($whereData);
                        unset($dataToSave);
                    }

                    $db->close();

                    if ($tourType != "Local")
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_POST["hdOriginName"])) . "-to-" . str_ireplace(" ", "-", strtolower($_POST["hdDestinationName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";
                    else
                        $url = corWebRoot . "/" . str_ireplace(" ", "-", strtolower($tourType)) . "/booking-confirm/" . str_ireplace(" ", "-", strtolower($_POST["hdOriginName"])) . "/" . $arrUserId[0]["Column1"] . "/?resp=booksucc";

         
                    $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                    $xmlToSave .= "<cortrans>";
                    $xmlToSave .= "<bookid>" . $arrUserId[0]["Column1"] . "</bookid>";
                    $xmlToSave .= "</cortrans>";
                    if (!is_dir("./xml/trans/" . date('Y')))
                        mkdir("./xml/trans/" . date('Y'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m'), 0775);
                    if (!is_dir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
                        mkdir("./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
                    if (isset($transactionId))
                        createcache($xmlToSave, "./xml/trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . $transactionId . "-B.xml");
                }
            }
            else {

                $tab = 4;
            }
            
           
            
        }
    } else {
        $tab = 4;
    }

    unset($cor);

    header('Location: ' . $url);
}

/* * ************ extra code ********** */


//************  TAB 1  **************/


if ($tab == 3) {

    if (isset($_COOKIE["cciid"]) && isset($_COOKIE["emailid"])) {
        if ($_COOKIE["cciid"] != "" && $_COOKIE["emailid"] != "") {
            $tab = 3;
        }
    } else {

        $cor = new COR();
        if ($_POST["corpassword"] == "") {
            if (($_POST["hdTourtype"]) != "Selfdrive") {
                $res = $cor->_CORCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
            } else {
                $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
            }

            $myXML = new CORXMLList();
            $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
            if (key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0) {

                $tab = 2;
            } else {

                setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                $tab = 3;
            }
            unset($myXML);
        } else {
            if (isset($_POST["isExistUser"])) {
                if ($_POST["isExistUser"] == "1") {
                    if (($_POST["hdTourtype"]) != "Selfdrive") {
                        $res = $cor->_CORCheckLogin(array("PhoneNo" => $_POST["monumber"], "password" => $_POST["corpassword"], "clientid" => 2205));
                    } else {
                        $res = $cor->_CORSelfDriveCheckLogin(array("PhoneNo" => $_POST["monumber"], "password" => $_POST["corpassword"], "clientid" => 2205));
                    }
                    $myXML = new CORXMLList();
                    if ($res->{'verifyLoginResult'}->{'any'} != "") {
                        $arrUserId = $myXML->xml2ary($res->{'verifyLoginResult'}->{'any'});
                        if ($arrUserId[0]["ClientCoIndivID"] > 0) {
                            setcookie("cciid", $arrUserId[0]["ClientCoIndivID"]);
                            setcookie("emailid", $arrUserId[0]["EmailID"]);
                            setcookie("phone1", $arrUserId[0]["Phone1"]);

                            $tab = 3;
                        }
                    } else {

                        $tab = 2;
                    }
                    unset($myXML);
                }
            } else {
                if (($_POST["hdTourtype"]) != "Selfdrive") {
                    $res = $cor->_CORCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
                } else {
                    $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
                }
                $myXML = new CORXMLList();
                $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});
                if (key_exists("ClientCoIndivID", $arrUserId[0]) && intval($arrUserId[0]["ClientCoIndivID"]) == 0) {

                    $tab = 2;
                } else {
                    setcookie("tcciid", trim($arrUserId[0]["ClientCoIndivID"]));
                    $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);

                    $tab = 3;
                }
                unset($myXML);
            }
        }
        session_start();
        $fullname = explode(" ", $_POST["name"]);
        if (count($fullname) > 1) {
            $fname = $fullname[0];
            $lname = $fullname[1];
        } else {
            $fname = $fullname[0];
            $lname = "";
        }

        if (($_POST["hdTourtype"]) != "Selfdrive") {
            $res = $cor->_CORCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
        } else {
            $res = $cor->_CORSelfDriveCheckPhone(array("PhoneNo" => $_POST["monumber"], "clientid" => 2205));
        }
        $myXML = new CORXMLList();
        $arrUserId = $myXML->xml2ary($res->{'VerifyPhoneResult'}->{'any'});

        if (key_exists("Column1", $arrUserId[0]) && intval($arrUserId[0]["Column1"]) == 0) {
            if (isset($_POST["corpassword"]))
                $pword = $_POST["corpassword"];
            else
                $pword = "retail@2012";
            if (($_POST["hdTourtype"]) != "Selfdrive") {
                $res = $cor->_CORRegister(array("fname" => $fname, "lname" => $lname, "PhoneNumber" => $_POST["monumber"], "emailId" => $_POST["email"], "password" => $pword));
            } else {
                $res = $cor->_CORSelfDriveRegister(array("fname" => $fname, "lname" => $lname, "PhoneNumber" => $_POST["monumber"], "emailId" => $_POST["email"], "password" => $pword));
            }

            $arrUserId = $myXML->xml2ary($res->{'RegistrationResult'}->{'any'});
            if (key_exists("Column1", $arrUserId[0]) && $arrUserId[0]["Column1"] > 0) {
                setcookie("tcciid", $arrUserId[0]["Column1"]);
                $tCCIID = trim($arrUserId[0]["Column1"]);

                $tab = 3;
            }
        } else {
            if (intval($arrUserId[0]["ClientCoIndivID"]) > 0) {
                setcookie("tcciid", $arrUserId[0]["ClientCoIndivID"]);
                $tCCIID = trim($arrUserId[0]["ClientCoIndivID"]);
                $tab = 3;
            }
        }
        unset($cor);
    }
}
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
    <!--<!DOCTYPE html>-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <title>Carzonrent</title>

        <!-- Extra Codiqa features -->
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/css/codiqa.ext.css">

        <!-- Extra Codiqa features -->
        <script src="<?php echo corWebRoot; ?>/js/codiqa.ext.js"></script>

        <?php
        if (trim($orgName) != "") {
            ?>
            <title>Car Rental <?php echo $orgName; ?>, Car Hire <?php echo $orgName; ?> at Carzonrent.com</title>
            <?php
        } else {
            ?>
            <title>Car Rental/Hire Company-Book a Cab or Rent a Car at Carzonrent.com</title>
            <?php
        }
        ?>
        <script type="text/javascript">
    var arrAddrs = new Array();
        </script>
        <?php include_once("./includes/header-css.php"); ?>
        <?php include_once("./includes/header-js.php"); ?>
        <style>
            .float {
                position:absolute;right:0px;bottom:0px;width:200px;height:200px;background-Color:red;
            }
        </style>
        <?php if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Local") { ?>
            <style>

            </style>
        <?php } elseif ($_POST["hdTourtype"] == "Selfdrive") {
            ?>
            <style>
                .genbtnlinksmall, .toprowinner, .genbtnlink{background-color: #db4626 !important;color:#fff !important;}

            </style>
            <?php
        }
        ?>
        <script type="text/javascript">
<?php
if ($_POST["hdTourtype"] == "Outstation" && $isDiscountShow == 1) {
    ?>
                var defaultCookieTime = 1440; //one day by default (set in terms of minutes)
                jQuery(document).ready(function () {
                    //js code to be executed on page load completed.
                    //if(jQuery.cookie('sreqshown')==null)    {
                    //if cookie is not set yet, means this is where we know that this is the first ever visit of this user and we will show him/her sliding box with some content
                    var ww = jQuery(window).width(); //get width of the web page window
                    var ppw = jQuery("#popupslider").width(); //get width of sliding box
                    var leftm = (ww - ppw) / 2; //do half of it, resulting "leftm" is the left margin needed to place the box in center of page horizontally

                    setTimeout("_showDiscountPopup()", 6000);

                    //var activeCookieTime = new Date(); //creating date object
                    //activeCookieTime.setTime(activeCookieTime.getTime() + (defaultCookieTime * 60 * 1000)); //setting cookie time using previously set variable "defaultCookieTime" which is measured in minutes. So for a day it is 1440.
                    //jQuery.cookie('sreqshown', true, { expires: activeCookieTime, path: '/' }); //setting the cookie now
                    //}

                    jQuery("#closepp").click(function () { //we usually include a "Close" link within the sliding box so user could close this box if he/she wanted to do so.
                        _gaq.push(['_trackPageview', '/discountpopup/close'])
                        jQuery("#popupslider").animate({opacity: "0", bottom: "-50"}, 1000).show(); //when user clicks "Close" it reverse the sliding animation to make the box disappear sliding from right to left.
                    });
                });

                function _showDiscountPopup() {
                    //_gaq.push(['_trackPageview', '/discountpopup/open'])
                    //jQuery("#popupslider").animate({opacity: "1", bottom: "-180" , bottom: 5}, 1000).show(); //using animate method of jQuery to load pop up on page load. The box starts sliding from the far left and slides until it reaches the middle position of page we just calculated above (see leftm)
                }
    <?php
}
?>
            arrDestination = new Array();
<?php
for ($i = 0; $i < count($des); $i++) {
    ?>
                arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
    <?php
}
?>
                mc = 1;
                if (typeof String.prototype.startsWith != 'function') {
                    String.prototype.startsWith = function (str) {
                        return this.indexOf(str) == 0;
                    };
                }
        </script>
        <script>
            $(function () {
                $('select.time').customSelect();
                $('select.area').customSelect();
            })

            function resgiter_cont() {
                document.getElementById("register").style.display = 'none';
                document.getElementById("travel_details").style.display = 'block';
            }
        </script>
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.css">
        <link rel="stylesheet" href="<?php echo corWebRoot; ?>/datecss/jquery.ui.theme.css">

        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.core.js"></script>
        <script src="<?php echo corWebRoot; ?>/datecss/jquery.ui.datepicker.js"></script>
        <script>
            $(function () {
                $(".datepicker").datepicker();
            });
        </script>
        <script>
            function subform(formid)
            {
                var d = new Date();

                //alert($("#hdUrlcurdate" + formid).val());
                urldatetime = new Date($("#hdUrlcurdate" + formid).val());

                difftime = ((d - urldatetime) / (1000));

                //alert(urldatetime)
                if (difftime > 600)
                {

                    // alert("Please wait, while we re-check the availability of our cars");
                    _showPopUp("html", '<br /><div style="text-align:center;"><img src="' + webRoot + '/images/loader.gif" border="0" /><br /><br />Please wait, while we re-check the availability of our cars.<br /><br />Please <b>DO NOT press REFRESH or BACK button</b> of your browser.</div>', "t", 400, 150, true, "");
                    location.href = "<?php echo corWebRoot; ?>";
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <script type="text/javascript">var _kiq = _kiq || [];</script>
        <script type="text/javascript" src="//s3.amazonaws.com/ki.js/43373/8qU.js" async="true"></script>
        <?php
        if (isset($_COOKIE["emailid"]) && $_COOKIE["emailid"] != "") {
            ?>
            <script type="text/javascript" charset="utf-8">
            _kiq.push(['identify', '<?php echo $_COOKIE["emailid"]; ?>']);
            </script>
            <?php
        }
        ?>
        <!--Header Start Here-->
        <?php include_once("./includes/header.php"); ?>
        <!--Banner Start Here-->
        <div data-role="content">
            <!--<div data-role="fieldcontain">
            <?php if ($tab == 1) { ?>
                        Select Car
            <?php } else if ($tab == 2) { ?>
                        Contact Information
            <?php } else if ($tab == 3) { ?>
                        Payment
            <?php } else { ?>
                        Other
            <?php } ?>
            </div>-->
            <!--Middle Start Here-->
            <!--Change by Iqbal on 09-May-2012 STARTS-->

            <!--Change by Iqbal on 09-May-2012 ENDS  http://maps.googleapis.com/maps/api/directions/json?origin=bangalore&destination=mysore&waypoints=mysore&sensor=false-->
            <?php if ($tab == 1) { ?>
                <div class="tbbinginr mobile_f">
                    <div id="country1" class="tabcontent">
                        <div class="main">
                            <div class="toprowinner">
                                <div data-role="fieldcontain" style="float:left;width:100%">
                                    <div class="d70">
                                        <p>
                                            <?php
                                            $offerStartDate = date_create("2015-03-05");
                                            $offerEndDate = date_create("2015-03-06");
                                            $isOffer = false;
                                            if ($_POST["hdTourtype"] != "Selfdrive" && $_POST["hdOriginID"] == 15) {
                                                if ((strtotime($pickDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($pickDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
                                                    $isOffer = true;
                                                if ($_POST["hdTourtype"] != "Local") {
                                                    if ((strtotime($dropDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($dropDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
                                                        $isOffer = true;
                                                }
                                            }

                                            $offerStartDate = date_create("2015-03-05");
                                            $offerEndDate = date_create("2015-03-07");
                                            $isOffer = false;
                                            if ($_POST["hdTourtype"] != "Selfdrive" && $_POST["hdOriginID"] == 2) {
                                                if ((strtotime($pickDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($pickDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
                                                    $isOffer = true;
                                                if ($_POST["hdTourtype"] != "Local") {
                                                    if ((strtotime($dropDate->format("Y-m-d")) - strtotime($offerStartDate->format("Y-m-d")) >= 0) && (strtotime($dropDate->format("Y-m-d")) - strtotime($offerEndDate->format("Y-m-d"))) <= 0)
                                                        $isOffer = true;
                                                }
                                            }


                                            if ($_POST["hdTourtype"] == "Outstation") {
                                                for ($i = 0; $i < $points; $i++) {
                                                    $dispOrg = $travelData->{'routes'}[0]->{'legs'}[$i]->{'start_address'};
                                                    $dispOrgs = explode(",", $dispOrg);
                                                    echo $dispOrgs[0] . "-";
                                                }
                                                $dispOrg = $travelData->{'routes'}[0]->{'legs'}[0]->{'start_address'};
                                                $dispOrgs = explode(",", $dispOrg);
                                                echo $dispOrgs[0];
                                            }

                                            if ($_POST["hdTourtype"] == "Selfdrive" || $_POST["hdTourtype"] == "Local") {
                                                echo $orgName;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <!--        </div>-->
                                    <?php
                                    ?>
                                    <!--<div data-role="fieldcontain">-->
                                    <div class="d30">
                                        <p>
                                            <?php
                                            if ($_POST["hdTourtype"] == "Outstation") {
                                                $totaldisplayKms = 0;
                                                for ($i = 0; $i < $points; $i++) {
                                                    $dispKM = $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"text"};
                                                    $totalKM += $travelData->{'routes'}[0]->{'legs'}[$i]->{'distance'}->{"value"};
                                                    $totaldisplayKms += $dispKM;
                                                }
                                                echo intval($totalKM / 1000) . " Kms";
                                            }
                                            if ($_POST["hdTourtype"] == "Local") {
                                                echo ($interval * 10) . " Kms";
                                            }
                                            if ($_POST["hdTourtype"] == "Selfdrive") {
                                                
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div data-role="fieldcontain" style="float:left;width:100%">
                                    <div class="d70 fwn">
                                        <?php
                                        if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Selfdrive") {
                                            if ($interval > 1)
                                                echo $pickDate->format('d M') . " - " . $dropDate->format('d M, Y');
                                            else
                                                echo $pickDate->format('d M, Y');
                                            if ($_POST["hdTourtype"] == "Selfdrive") {
                                                echo " " . $_POST["tHourP"] . $_POST["tMinP"] . " Hrs";
                                            }
                                        } else {
                                            echo $pickDate->format('d-M, Y');
                                        }
                                        ?>
                                    </div>
                                    <div class="d30">
                                        <?php
                                        $pickDate = date_create(date('Y-m-d', strtotime($pDate)));
                                        if ($_POST["hdTourtype"] == "Selfdrive") {
                                            if ($_POST["chkPkgType"] == "Daily" || $_POST["chkPkgType"] == "Weekly" || $_POST["chkPkgType"] == "Monthly") {
                                                if ($interval == 1)
                                                    echo $interval . " Day";
                                                else
                                                    echo $interval . " Days";
                                            } elseif ($_POST["chkPkgType"] == "Hourly") {
                                                echo " " . $_POST["nHours"] . " Hours";
                                            }
                                        } elseif ($_POST["hdTourtype"] == "Outstation") {
                                            ?>
                                            <?php echo $interval; ?> Days
                                            <?php
                                        } else {
                                            ?>
                                            <?php echo $interval; ?> Hours	
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div data-role="fieldcontain">
                                <?php
                                $isOutStation = TRUE;
                                if (isset($_POST["hdTourtype"])) {
                                    if ($_POST["hdTourtype"] == "Local")
                                        $isOutStation = FALSE;
                                }
                                $cor = new COR();
                                if ($_POST["hdTourtype"] == "Selfdrive") {
                                    $pkgt = $_POST["chkPkgType"];
                                    if (trim($_POST["chkPkgType"]) == "Daily" || trim($_POST["chkPkgType"]) == "Weekly" || trim($_POST["chkPkgType"]) == "Monthly")
                                        $res = $cor->_CORSelfDriveGetCabs(array("CityId" => $orgIDs[0], "fromDate" => $pickDate->format('Y-m-d'), "DateIn" => $dropDate->format('Y-m-d'), "PickupTime" => $_POST["tHourP"] . $_POST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i'), "SubLocation" => "0", "Duration" => $interval, "PkgType" => $pkgt, "CustomPkgYN" => $_POST["customPkg"]));
                                    elseif (trim($_POST["chkPkgType"]) == "Hourly")
                                        $res = $cor->_CORSelfDriveGetCabs(array("CityId" => $orgIDs[0], "fromDate" => $pickDate->format('Y-m-d'), "DateIn" => $dropDate->format('Y-m-d'), "PickupTime" => $_POST["tHourP"] . $_POST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i'), "SubLocation" => "0", "Duration" => $_POST["nHours"], "PkgType" => $_POST["chkPkgType"], "CustomPkgYN" => $_POST["customPkg"]));
                                } else
                                    $res = $cor->_CORGetCabs(array("source" => $orgIDs[0], "dateout" => $pickDate->format('Y-m-d'), "outStationYN" => $isOutStation));

                                $myXML = new CORXMLList();
                                $arr = $myXML->xml2ary($res);


                                if ($_POST["hdTourtype"] == "Selfdrive") {
                                    $res = $cor->_CORSelfDriveGetSubLocationsName(array("CityID" => $orgIDs[0]));
                                    $arrSL = $myXML->xml2ary($res);
                                    if (count($arrSL) > 0) {
                                        $isSLName = array();
                                        foreach ($arrSL as $k => $v)
                                            $isSLName[$k] = $v['SubLocationName'];
                                        array_multisort($isSLName, SORT_ASC, $arrSL);
                                        ?>
                                        <div style="float:left;width:100%;padding: 10px 0px;border-bottom:solid 1px #6e6e6e;">
                                            <select id="ddlSubLoc" name="ddlSubLoc" class="styled" style="width:96%;margin-left:2%" onchange="javascript: _getSRSFSubLoc(<?php echo $orgIDs[0]; ?>, '<?php echo $orgNames[0]; ?>', '<?php echo $pickDate->format('Y-m-d'); ?>', '<?php echo $dropDate->format('Y-m-d'); ?>', '<?php echo $_POST["tHourP"]; ?>', '<?php echo $_POST["tMinP"]; ?>', '<?php echo $dropDate->format('H'); ?>', '<?php echo $dropDate->format('i'); ?>', this, '<?php echo $_POST["chkPkgType"]; ?>', '<?php
                                            if ($_POST["chkPkgType"] == "Hourly") {
                                                echo $_POST["nHours"];
                                            } else {
                                                echo $interval;
                                            }
                                            ?>', <?php echo $_POST["customPkg"]; ?>);">
                                                <option value="0">All Location</option>
                                                <?php
                                                for ($i = 0; $i < count($arrSL); $i++) {
                                                    ?>
                                                    <option value="<?php echo $arrSL[$i]["SubLocationID"]; ?>"><?php echo $arrSL[$i]["SubLocationName"]; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <?php
                                    }
                                    unset($arrSL);
                                }
                                ?>
                                <div class="dhs_main" style="float:left;width:100%;margin-top: 20px;border-bottom:solid 1px #6e6e6e;">
                                    <div style="float:left;width:100%;">
                                        <div class="hdsection" style="width:23%;padding-left:2%">Cars</div>
                                        <div class="hdsection">Details</div>
                                        <div class="hdsection">Price</div>
                                        <div class="hdsection">Book</div>
                                    </div>
                                </div>
                                <div style="float:left;width:100%;" id="srsf">
                                    <!-- self drive Start here  all categories -->



                                    <?php
                                    if ($_POST["hdTourtype"] == "Selfdrive") {
                                        $dayFare = array();
                                        $isAvail = array();
                                        foreach ($arr as $k => $v) {
                                            $dayFare[$k] = $v['PkgRate'];
                                            $isAvail[$k] = $v['IsAvailable'];
                                        }
                                        array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
                                        $arrSL = array();
                                        $arrNA = array();

                                        for ($i = 0; $i < count($arr); $i++) {
                                            $isSLF = 1;
                                            $isShow = false;
                                            if (!isset($_POST["onlyE2C"]) || $_POST["onlyE2C"] == 0)
                                                $isShow = true;
                                            else {
                                                if ($arr[$i]["CarCatID"] == 21) {
                                                    $isShow = true;
                                                }
                                            }
                                            $btnid = 1;
                                            $passCount = $arr[$i]["SeatingCapacity"];
                                            $totAmount = $arr[$i]["BasicAmt"];
                                            $vat = $arr[$i]["VatRate"];
                                            $carModel = "";
                                            $carCatgName = "";
                                            if ($arr[$i]["CarCatName"] == '4 Wheel Drive')
                                                $carCatgName = "SUV";
                                            else
                                                $carCatgName = $arr[$i]["CarCatName"];
                                            $carModel = $arr[$i]["model"];
                                            $secDeposit = $arr[$i]["DepositeAmt"];
                                            $CarDetail = "";
                                            $CarMake = "";
                                            if (intval($_POST["hdOriginID"]) != "69") {
                                                if (strtolower(trim($carModel)) == "toyota innova") {
                                                    $CarDetail = trim($carModel) . " - GX";
                                                    $CarMake = "<small>Make - 2012</small>";
                                                } else if (strtolower(trim($carModel)) == "maruti swift") {
                                                    $CarDetail = trim($carModel) . " - VDI";
                                                    $CarMake = "<small>Make - 2012</small>";
                                                } else if (strtolower(trim($carModel)) == "toyota etios") {
                                                    $CarDetail = trim($carModel) . " - GD";
                                                    $CarMake = "<small>Make - 2012</small>";
                                                } else if (strtolower(trim($carModel)) == "mahindra xuv-500") {
                                                    $CarDetail = trim($carModel) . " - W6";
                                                    $CarMake = "<small>Make - 2012</small>";
                                                } else if (strtolower(trim($carModel)) == "volkswagen polo" || strtolower(trim($carModel)) == "maruti ertiga" || strtolower(trim($carModel)) == "volkswagen vento" || strtolower(trim($carModel)) == "nissan sunny" || strtolower(trim($carModel)) == "ford endeavour" || strtolower(trim($carModel)) == "toyota fortuner") {
                                                    $CarDetail = trim($carModel);
                                                    $CarMake = "<small>Make - 2013</small>";
                                                } else if (strtolower(trim($carModel)) == "mahindra e2o") {
                                                    $CarDetail = trim($carModel) . "<br />" . "<small><b>Electric Car</b></small>";
                                                    $CarMake = "<small>Make - 2013</small>";
                                                } else
                                                    $CarDetail = trim($carModel);
                                            } else {
                                                $CarDetail = trim($carModel);
                                            }
                                            if ($isShow) {
                                                if ($carCatgName == "Super")
                                                    $carCatgName = "Super-Luxury";
                                            }
                                            $_SESSION['PkgRate'] = $arr[$i]['PkgRate'];
                                            $totalfare1 = intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));
                                            $basicamount1 = ceil($arr[$i]["BasicAmt"]);
                                            $res1 = $cor->_CORSelfDriveVariableFlatOffer(array('PromotionCode' => $arr[$i]['scheme'], "CityId" => $orgIDs[0]));
                                            $fleets1 = $myXML->xml2ary($res1);


                                            $schemestartdatearray = explode('T', $fleets1[0]['StartDate']);
                                            $schemeexpirydatearray = explode('T', $fleets1[0]['ExpiryDate']);

                                            $pickup_date = date("Y-m-d", strtotime($_REQUEST['pickdate'])) . ' ' . $_REQUEST['tHourP'] . ':' . $_REQUEST['tMinP'] . ':00';
                                            $pickup_date = strtotime($pickup_date);
                                            $dropoff_date = date("Y-m-d", strtotime($_REQUEST['dropdate'])) . ' ' . $_REQUEST['tHourD'] . ':' . $_REQUEST['tMinD'] . ':00';
                                            $dropoff_date = strtotime($dropoff_date);
                                            $timediff = $dropoff_date - $pickup_date;

                                            $timediffhour = $timediff / (60 * 60);
                                            $timediffday = intval($timediffhour / 24);
                                            $remaininghour = $timediffhour % 24;
///
                                            $timeFrom = ($_REQUEST['tHourP'] * 60) + intval($_REQUEST['tMinP']);
                                            $timeTo = ($_REQUEST['tHourD'] * 60) + intval($_REQUEST['tMinD']);

//  changes   


                                            /* $scheme_pickup_date = date_create($_REQUEST['pickdate'])->format("Y-m-d");
                                              $scheme_pickup_date = new DateTime($scheme_pickup_date);

                                              $scheme_dropoff_date = date_create($_REQUEST['dropdate'])->format("Y-m-d");
                                              $scheme_dropoff_date = new DateTime($scheme_dropoff_date);

                                              $scheme_start_date = date_create($schemestartdatearray[0])->format("Y-m-d");
                                              $scheme_start_date = new DateTime($scheme_start_date);

                                              $scheme_expiry_date = date_create($schemeexpirydatearray[0])->format("Y-m-d");
                                              $scheme_expiry_date = new DateTime($scheme_expiry_date); */

                                            $regdayswithscheme = 0;
                                            $premiumdayswithscheme = 0;
                                            $regdayswithoutscheme = 0;
                                            $premiumdayswithoutscheme = 0;

                                            $reg_hours_with_scheme = 0;
                                            $premium_hours_with_scheme = 0;
                                            $reg_hours_without_scheme = 0;
                                            $premium_hours_without_scheme = 0;

                                            $premiumdays = array('Fri', 'Sat', 'Sun');
                                            $premiumdaysforhours = array('Thu', 'Fri', 'Sat', 'Sun', 'Mon');
                                            if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                                                
                                            } else {

                                                $scheme_pickup_date->modify('+1 day');
////
                                                if (intval($_REQUEST["hdOriginID"]) == "69") {


                                                    if ($timeFrom >= 480 and $timeTo <= 480) {
                                                        // @Duration = @Duration
                                                    } else if ($timeFrom = 480 and $timeTo > 480) {
                                                        //@Duration = @Duration + 1
                                                        $scheme_dropoff_date->modify('+1 day');
                                                        //   echo '1 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                                    } else if ($timeFrom > 480 and $timeTo >= 480) {
                                                        //@Duration = @Duration + 1
                                                        $scheme_dropoff_date->modify('+1 day');
                                                        //    echo '2 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                                    } else if ($timeFrom < 480 and $timeTo = 480) {
                                                        //@Duration = @Duration + 1
                                                        $scheme_dropoff_date->modify('+1 day');
                                                        //   echo '3 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                                    } else if (@TimeFrom < 480 and @ TimeTo > 480) {
                                                        // @Duration = @Duration + 2
                                                        $scheme_dropoff_date->modify('+2 day');
                                                        // echo '4 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                                    } else if (@TimeFrom < 480 and @ TimeTo < 480) {
                                                        //@Duration = @Duration + 1
                                                        $scheme_dropoff_date->modify('+1 day');
                                                        //  echo '5 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
                                                    }
                                                } else {

//                                    if ($remaininghour > 1) {
//                                        $scheme_dropoff_date->modify('+1 day');
//                                       // echo '6 '. $scheme_dropoff_date->format("Y-m-d H:i:s");
//                                    }
                                                }
//   echo '$scheme_pickup_date = '.$scheme_pickup_date.'<br/>';
// // echo '$scheme_dropoff_date = '.$scheme_dropoff_date.'<br/>';
//  ///
//echo $scheme_dropoff_date->format("Y-m-d H:i:s");
//                                for (; $scheme_pickup_date <= $scheme_dropoff_date;) {
////echo "abcd";
//                                    $checkday = $scheme_pickup_date->format('D');
//                                    if (($scheme_pickup_date) < ($scheme_start_date)) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//
//                                            $regdayswithoutscheme++;
//                                        }
//                                    } if ($scheme_start_date->format('Y-m-d') <= $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if ($scheme_pickup_date->format('Y-m-d') <= $scheme_expiry_date->format('Y-m-d')) {
//
//                                            if (in_array($checkday, $premiumdays)) {
//
//                                                $premiumdayswithscheme++;
//                                            } else {
//
//                                                $regdayswithscheme++;
//                                            }
//                                        }
//                                    } if ($scheme_expiry_date->format('Y-m-d') < $scheme_pickup_date->format('Y-m-d')) {
//
//                                        if (in_array($checkday, $premiumdays)) {
//                                            $premiumdayswithoutscheme++;
//                                        } else {
//                                            $regdayswithoutscheme++;
//                                        }
//                                    }
//                                    $scheme_pickup_date->modify('+1 day');
//                                }
                                                $argval = array();

                                                $argval['CityId'] = $orgIDs[0];
                                                $argval['PkgType'] = $_REQUEST["chkPkgType"];
                                                $argval['PromotionCode'] = $arr[$i]['scheme'];
                                                $argval['FromDate'] = date("Y-m-d", strtotime($_REQUEST['pickdate']));
                                                $argval['ToDate'] = date("Y-m-d", strtotime($_REQUEST['dropdate']));
                                                $argval['TimeTo'] = $_REQUEST['tHourD'] . $_REQUEST['tMinD'];
                                                $argval['TimeFrom'] = $_REQUEST['tHourP'] . $_REQUEST['tMinP'];


                                                $resultVal = $cor->_CORGetFreedomDays($argval);

                                                $fleets_arr = $myXML->xml2ary($resultVal);

                                                $regdayswithscheme = intval("" . $fleets_arr[0]['WeekDayScheme']);

                                                $premiumdayswithscheme = intval("" . $fleets_arr[0]['WeekEndScheme']);
                                                $regdayswithoutscheme = intval("" . $fleets_arr[0]['WeekDayNoScheme']);
                                                $premiumdayswithoutscheme = intval("" . $fleets_arr[0]['WeekEndNoScheme']);

                                                $reg_days_with_scheme_totalprice = 0;
                                                $premium_days_with_scheme_totalprice = 0;
                                                $reg_days_without_scheme_totalprice = 0;
                                                $premium_days_without_scheme_totalprice = 0;
                                                $premium_days_with_scheme_totalprice1 = 0;
                                                $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                                                $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                                                $reg_days_with_scheme_totalprice1 = $regdayswithscheme * $arr[$i]["PkgRate"];

                                                if (intval($_REQUEST["hdOriginID"]) == "69") {
                                                    $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRate"];
                                                    $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekendDis']) / 100);
                                                    $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRate"];
                                                } else {
                                                    $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                                                    $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                                                    $premium_days_with_scheme_totalprice1 = $premiumdayswithscheme * $arr[$i]["PkgRateWeekEnd"];
                                                }
//                            echo '$reg_days_with_scheme_totalprice1 = '.$reg_days_with_scheme_totalprice1.'<br/>';
//                            echo '$premium_days_with_scheme_totalprice1 = '.$premium_days_with_scheme_totalprice1.'<br/>';
//                            echo '$reg_days_without_scheme_totalprice = '.$reg_days_without_scheme_totalprice.'<br/>';
//                            echo '$premium_days_without_scheme_totalprice = '.$premium_days_without_scheme_totalprice.'<br/>';
                                                $totalschemeamount1 = $reg_days_with_scheme_totalprice1 + $premium_days_with_scheme_totalprice1 +
                                                        $reg_days_without_scheme_totalprice + $premium_days_without_scheme_totalprice;
                                                $totalschemeamount1 = $totalschemeamount1 + ($totalschemeamount1 * $vat / 100);

                                                $totalschemeamount = $reg_days_with_scheme_totalprice + $premium_days_with_scheme_totalprice +
                                                        $reg_days_without_scheme_totalprice +
                                                        $premium_days_without_scheme_totalprice;
                                                $totAmount = $totalschemeamount;
                                                $totalfare1 = intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100));
                                                $basicamount1 = $totalschemeamount;
                                            }
                                            if (intval($arr[$i]["IsAvailable"]) == 1) {

                                                /* freedom code start */



                                                /* freedom code end */
                                                ?>
                                                <form id="formBook<?php echo $i; ?>" name="formBook<?php echo $i; ?>" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/" onsubmit="return subform(<?php echo $i; ?>);">
                                                    <input type="hidden" name="urlcurdate" id="hdUrlcurdate<?php echo $i; ?>" value="<?php echo $_REQUEST['curdatetime']; ?>"/>
                                                    <?php
                                                    if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {
                                                        
                                                    } else {
                                                        ?>
                                                        <input type="hidden" id="hdCoupon<?php echo $i; ?>" name="hdCoupon" value="<?php echo $arr[$i]['scheme']; ?>" />
                                                        <input type="hidden" id="hdSchemeDiscount<?php echo $i; ?>" name="hdSchemeDiscount" value="<?php echo intval($arr[$i]["OriginalAmt"] - $totalschemeamount); ?>" />
                                                        <?php
                                                    }
                                                    ?>
                                                    <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo trim($carModel); ?>" />
                                                    <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                                                    <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"] ?>" />
                                                    <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                    <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]; ?>"/>
                                                    <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                    <input type="hidden" name="hdCarID" id="hdCarID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarID"]; ?>" />
                                                    <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                    <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                                                    <input type="hidden" name="hdCat" id="hdCat<?php echo $i; ?>" value="<?php echo $carCatgName; ?>" />
                                                    <input type="hidden" name="PkgRate" id="PkgRate<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                                                    <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekDayDuration"]; ?>"> 
                                                    <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekEndDuration"]; ?>">
                                                    <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["FreeDuration"]; ?>">
                                                    <input type="hidden" name="pkgR" id="pkgR<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgRate"]; ?>"> 

                                                    <?php
                                                    if ($_POST["chkPkgType"] == "Daily" || $_POST["chkPkgType"] == "Weekly" || $_POST["chkPkgType"] == "Monthly") {
                                                        ?>
                                                        <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />

                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["nHours"]; ?>" />

                                                        <?php
                                                    }
                                                    ?>
                                                    <input type="hidden" name="totAmount" id="totAmount<?php echo $i; ?>" value="<?php echo $totAmount; ?>" />
                                                    <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $totalfare1; ?>" />
                                                    <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
                                                    <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y'); ?>" />
                                                    <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                                                    <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                                                    <input type="hidden" name="tHourP" id="tHourP<?php echo $i; ?>" value="<?php
                                                    if (isset($_POST["tHourP"])) {
                                                        echo $_POST["tHourP"];
                                                    }
                                                    ?>" />
                                                    <input type="hidden" name="tMinP" id="tMinP<?php echo $i; ?>" value="<?php
                                                    if (isset($_POST["tMinP"])) {
                                                        echo $_POST["tMinP"];
                                                    }
                                                    ?>" />
                                                    <input type="hidden" name="tHourD" id="tHourD<?php echo $i; ?>" value="<?php echo $dropDate->format('H'); ?>" />
                                                    <input type="hidden" name="tMinD" id="tMinD<?php echo $i; ?>" value="<?php echo $dropDate->format('i'); ?>" />
                                                    <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $vat; ?>" />
                                                    <input type="hidden" name="secDeposit" id="secDeposit<?php echo $i; ?>" value="<?php echo $secDeposit; ?>" />
                                                    <input type="hidden" name="subLoc" id="subLoc<?php echo $i; ?>" value="" />
                                                    <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_POST["chkPkgType"] ?>" />
                                                    <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_POST["nHours"] ?>" />
                                                    <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["OriginalAmt"]); ?>" />
                                                    <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $basicamount1; ?>" />
                                                    <input type="hidden" name="allSubLoc" id="allSubLoc<?php echo $i; ?>" value="<?php echo $arr[$i]["DetailID"]; ?>" />
                                                    <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $arr[$i]["KMIncluded"]; ?>" />
                                                    <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $arr[$i]["ExtraKMRate"]; ?>" />
                                                    <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="0" />
                                                    <input type="hidden" name="gross_amount_scheme" id="gross_amount_scheme" value="<?php echo $totalschemeamount1; ?>" />        
                                                    <?php
                                                }
                                                ?>
                                                <div class="clr"> </div>
                                                <?php
                                                if ((intval($arr[$i]["IsAvailable"]) == 0) && $checkerfornon == 0) {
                                                    $checkerfornon++;
                                                    ?>
                                                    <div class="car_description">
                                                        <b>The following cars are not available for the duration you searched for, however you have the following option(s):</b>
                                                        <ul>
                                                            <?php
                                                            $resAdv = $cor->_CORGetAdvanceSearchAvailability(array("ModelID" => $arr[$i]["ModelID"], "CarCatId" => $arr[$i]["CarCatID"], "CityID" => $_REQUEST["hdOriginID"], "pickupDate" => $pickDate->format('Y-m-d'), "droffDate" => $dropDate->format('Y-m-d'), "SubLocations" => "0", "PickupTime" => $_REQUEST["tHourP"] . $_REQUEST["tMinP"], "DropOffTime" => $dropDate->format('H') . $dropDate->format('i')));
                                                            $AdvResults = $myXML->xml2ary($resAdv->{'GetAdvanceSearchAvailabilityResult'}->{'any'});
                                                            if (intval(count($AdvResults))) {
                                                                ?>
                                                                <!--li>You can select the Flexible plans option, if you have flexible plans</li-->
                                                                <?php
                                                            }
                                                            ?>
                                                            <li>You can select the Next available option, if you need the car for a fixed period</li>
                                                        </ul>
                                                        <div class="clr"> </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="clr"> </div>
                                                <div style="float:left;width:100%;">
                                                    <!-- kaushal start -->
                                                    <div class="row flex_slider">
                                                        <div class="flex_col1">
                                                            <?php
                                                            if ($carCatgName == "Super")
                                                                $carCatgName = "Super-Luxury";
                                                            ?>
                                                            <a href="javascript: void(0);">
                                                                <img src="<?php echo corWebRoot; ?>/images/<?php echo str_ireplace(" ", "-", trim($carModel)); ?>.jpg" alt=""/></a>
                                                            </a>
                                                        </div>
                                                        <div class="flex_col2">
                                                            <span><b><?php echo $CarDetail; ?><?php //echo $carCatgName;       ?></b></span><br />
                                                            <div style="float:left;width:100%;">
                                                                <label class="noof">Capacity: <?php echo $passCount; ?></label><br />
                                                                <label class="noof"><?php echo $SLNames; ?></label>

                                                                <!--<label class="tx">+ driver</label>-->
                                                            </div>
                                                        </div>
                                                        <div class="flex_col3">
                                                            <?php
                                                            if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') {

                                                                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                                    ?>
                                                                    <span class="newo"><s style='color:#db4626;'>Rs. <?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?>/-</s></span> 

                                                                    <span class="newp" style="font-size: 13px;">Rs. <?php echo $totalfare1; ?>/-</span>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <span class="newo"><span class="newp">Rs.  <?php echo $totalfare1; ?>/-</span><br />
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    // scheme 
                                                                    if ($_REQUEST['chkPkgType'] == 'Daily') {


                                                                        if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                                            ?>
                                                                            <span id="ffare<?php echo $i; ?>" class="new block"> Rs <?php echo intval($arr[$i]["PkgRate"]); ?> /- Per Day</span>
                                                                            <?php
                                                                            if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                                                ?>
                                                                                <span class="driving_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }

                                                                    if ($_REQUEST['chkPkgType'] == 'Daily') {


                                                                        if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["WeekEndDuration"] == 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                                            ?>
                                                                            <span id="ffare<?php echo $i; ?>" class="new block">Rs<?php echo intval($arr[$i]["PkgRate"]); ?>/- Per Day</span>
                                                                            <?php
                                                                            if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                                                ?>
                                                                                <span class="driving_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                        }
                                                                    }



                                                                    if ($_REQUEST['chkPkgType'] == 'Daily') {

                                                                        if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["WeekDayDuration"] == 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                                                            ?>
                                                                            <span id="ffare<?php echo $i; ?>" class="new block">Rs. <?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?> /- Per Day </span>
                                                                            <?php
                                                                            if ($_REQUEST['chkPkgType'] == 'Daily') {
                                                                                ?>
                                                                                <span class="driving_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <?php
                                                                        }
                                                                    }

                                                                    if ($pkgt == "Hourly" || $pkgt == "Daily") {

                                                                        if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                                            ?>
                                                                            <div style="clear: both;"></div>
                                                                            <span class="newp">
                                                                                <span class="old_bill">
                                                                                    Regular Price: Rs.
                                                                                    <strike> <?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?></strike>
                                                                                </span>
                                                                                <span class="new_bill">
                                                                                    Discounted Price: Rs. 
                                                                                    <span><?php echo $totalfare1; ?>/-</span>
                                                                                </span>
                                                                            </span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <div style="clear: both;"></div>
                                                                            <span class="newo">

                                                                                <span class="newo">
                                                                                    <span class="old_bill">
                                                                                        Regular Price: Rs
                                                                                        <strike><?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?></strike>
                                                                                    </span>
                                                                                    <span class="new_bill">
                                                                                        Discounted Price: Rs 
                                                                                        <span><?php echo $totalfare1; ?>/-</span>
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        if ($pkgt == "Weekly") {
                                                                            ?>
                                                                            <span class="driving_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                                            <?php
                                                                        } elseif ($pkgt == "Monthly") {
                                                                            ?>
                                                                            <span class="driving_sale"><a href="#"> <?php echo "The Get Driving Sale "; ?></a></span>
                                                                            <?php
                                                                        }
                                                                        ?>


                                                                        <?php
                                                                        if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                                            ?>
                                                                            <span><span class="newo">
                                                                                    <span class="newo">
                                                                                        <span class="old_bill">
                                                                                            Regular Price: Rs  
                                                                                            <strike><?php echo intval(ceil($arr[$i]["OriginalAmt"] + (($arr[$i]["OriginalAmt"] * $vat) / 100))); ?></strike>
                                                                                        </span>
                                                                                    </span>
                                                                                    <span class="new_bill">
                                                                                        Discounted Price: Rs 
                                                                                        <span><?php echo intval($totalfare1); ?>/-</span>
                                                                                    </span>
                                                                                </span>
                                                                            </span>

                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <div style="clear: both;"></div>
                                                                            <span class="newo">
                                                                                <span class="newo">
                                                                                    <span class="newo">
                                                                                        <span class="old_bill">
                                                                                            Regular Price: Rs
                                                                                            <strike><?php echo intval(ceil($arr[$i]["BasicAmt"] + (($arr[$i]["BasicAmt"] * $vat) / 100))); ?></strike>
                                                                                        </span>
                                                                                        <span class="new_bill">
                                                                                            Discounted Price: Rs
                                                                                            <span><?php echo intval($totalfare1); ?>/-</span>
                                                                                        </span>
                                                                                    </span>
                                                                                </span>

                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <a class="faredetails" href="javascript:void(0)" onclick="javascript: _pass('popupbox<?php echo $i; ?>');" id="link_<?php echo $i; ?>">Fare details</a>

                                                                    <?php if ($arr[$i]['scheme'] == '0' || $_REQUEST['chkPkgType'] == 'Hourly') { ?>
                                                                        <div class="details_wrapper" id="popupbox<?php echo $i; ?>" style="display:none">
                                                                            <div class="toolarrow"></div>
                                                                            <div class="heading"><!--Fare Details--> <span class="closedpop" onclick="javascript: _closeFD('popupbox<?php echo $i; ?>');">X</span></div>

                                                                            <p><span>Fare Details</span></p>
                                                                            <ul style="margin-left:15px;">

                                                                                <?php
                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                                                        ?>
                                                                                        <li>Weekdays:<?php echo intval($arr[$i]["WeekDayDuration"]); ?>*<?php echo intval($arr[$i]["PkgRate"]); ?></li>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                    if ($arr[$i]["WeekDayDuration"] > 0 && $arr[$i]["PkgRate"] > 0.00) {
                                                                                        ?>
                                                                                        <li>No. of hours:<?php echo intval($arr[$i]["WeekDayDuration"]); ?>*<?php echo intval($arr[$i]["PkgRate"]); ?></li>
                                                                                        <?php
                                                                                    }
                                                                                }


                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                                                                        ?>
                                                                                        <li>Weekends:<?php echo intval($arr[$i]["WeekEndDuration"]); ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?></li>
                                                                                        <?php
                                                                                    }
                                                                                }


                                                                                if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                    if ($arr[$i]["WeekEndDuration"] > 0 && $arr[$i]["PkgRateWeekEnd"] > 0.00) {
                                                                                        ?>
                                                                                        <li>No. of hours:<?php echo $arr[$i]["WeekEndDuration"]; ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?></li>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>


                                                                                <li>Minimum billing: Rs. <?php echo intval($arr[$i]["BasicAmt"]); ?>/-</li>
                                                                                <li>VAT (@<?php echo number_format($vat, 2); ?>%): Rs. <?php echo intval(ceil(($arr[$i]["BasicAmt"] * $vat) / 100)); ?>/-</li>
                                                                                <li>Total fare: Rs. <?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?>/-</li>
                                                                                <?php
                                                                                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                                                    ?>
                                                                                    <li>Discount Amount (On base fare): Rs. <?php echo intval($arr[$i]["OriginalAmt"] - $arr[$i]["BasicAmt"]); ?>/-</li>
                                                                                    <?php
                                                                                }
                                                                                if (intval($_POST["hdOriginID"]) != "69") {
                                                                                    ?>
                                                                                    <li>Refundable Security Deposit  (Pre Auth from card): Rs. <?php echo $secDeposit; ?> (Mastercard/Visa/Amex)</li>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <li>The billing cycle starts from 8am each day</li>
                                                                                    <li>Refundable Security Deposit: Rs. <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </ul>
                                                                            <?php
                                                                            if (trim($_POST["chkPkgType"]) == "Hourly") {
                                                                                ?>
                                                                                <p style="margin-top:10px;float:left;font-weight:bold"><span>Includes</span></p>
                                                                                <ul style="margin-left:15px;">
                                                                                    <li>KMs Included: <?php echo $arr[$i]["KMIncluded"]; ?></li>
                                                                                    <li>Extra KM Charge: Rs. <?php echo $arr[$i]["ExtraKMRate"]; ?>/- per KM</li>
                                                                                </ul>
                                                                                <?php
                                                                            }
                                                                            ?>

                                                                            <p style="margin-top:10px;float:left;font-weight:bold"><span>Eligibilty</span></p>

                                                                            <ul style="margin-left:15px;">
                                                                                <li>The minimum age required to rent a Myles-Nano is 18yrs.<br> However, it is 23yrs for all other cars. 
                                                                                    <br>The identification documents needs to be in the name of the person who is booking the car</li>

                                                                            </ul>

                                                                            <p style="margin-top:10px;float:left;font-weight:bold"><span>Mandatory Documents(Original)</span></p>
                                                                            <?php
                                                                            if (intval($_POST["hdOriginID"]) != "69") {
                                                                                ?>
                                                                                <ul style="margin-left:15px;">
                                                                                    <li>Passport / Voter ID card</li>
                                                                                    <li>Driving license</li>
                                                                                    <li>Credit card</li>
                                                                                    <li>Adhaar card</li>
                                                                                </ul>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <ul style="margin-left:15px;">
                                                                                    <li>Passport</li>
                                                                                    <li>Driving License</li>
                                                                                    <li>Any of the following has to be submitted in original as an identity proof.
                                                                                        <ol type="1">
                                                                                            <li>Adhaar card</li>
                                                                                            <li>Pan card</li>
                                                                                            <li>Voter ID card</li>
                                                                                        </ol>
                                                                                    </li>
                                                                                    <li>Please note that the car in Goa will be provided through a partner.</li>
                                                                                </ul>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                    } else {

                                                                        /// freedum
                                                                        ?>

                                                                        <div class="details_wrapper" id="popupbox<?php echo $i; ?>" style="display:none">
                                                                            <div class="toolarrow"></div>
                                                                            <div class="heading"><span class="closedpop" onclick="javascript: _closeFD('popupbox<?php echo $i; ?>');">X</span></div>

                                                                            <p><span>Fare Details</span></p>
                                                                            <ul style="margin-left:15px;">

                                                                                <?php
                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                                                    // echo '$regdayswithoutscheme = '.$regdayswithoutscheme.'<br/>';
                                                                                    if ($regdayswithoutscheme > 0) {
                                                                                        ?>
                                                                                        <li>Weekdays :<?php echo $regdayswithoutscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?></li>
                                                                                        <?php
                                                                                        $reg_days_without_scheme_totalprice = $regdayswithoutscheme * $arr[$i]["PkgRate"];
                                                                                    }
                                                                                }



                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                                                    //echo '$premiumdayswithoutscheme = '.$premiumdayswithoutscheme.'<br/>';
                                                                                    if ($premiumdayswithoutscheme > 0) {
                                                                                        ?>
                                                                                        <li>Weekends :<?php echo $premiumdayswithoutscheme; ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?></li>
                                                                                        <?php
                                                                                        $premium_days_without_scheme_totalprice = $premiumdayswithoutscheme * $arr[$i]["PkgRateWeekEnd"];
                                                                                    }
                                                                                }

                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                                                    // echo '$regdayswithscheme = '.$regdayswithscheme.'<br/>';
                                                                                    if ($regdayswithscheme > 0) {
                                                                                        ?>
                                                                                        <li>Weekdays with offer <?php echo $fleets1[0]['WeekdayDis']; ?>% :<?php echo $regdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?>




                                                                                        </li>
                                                                                        <?php
                                                                                        $reg_days_with_scheme_totalprice = $regdayswithscheme * ($arr[$i]["PkgRate"] - ($arr[$i]["PkgRate"] * $fleets1[0]['WeekdayDis']) / 100);
                                                                                    }
                                                                                }
                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                                                    //echo '$premiumdayswithscheme = '.$premiumdayswithscheme.'<br/>';
                                                                                    if ($premiumdayswithscheme > 0) {
                                                                                        if (intval($_REQUEST["hdOriginID"]) == "69") {
                                                                                            ?>
                                                                                            <li>Weekend with offer <?php echo $fleets1[0]['WeekendDis']; ?>% :<?php echo $premiumdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRate"]); ?>



                                                                                            </li>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <li>Weekend with offer <?php echo $fleets1[0]['WeekendDis']; ?>% :<?php echo $premiumdayswithscheme; ?>*<?php echo intval($arr[$i]["PkgRateWeekEnd"]); ?>



                                                                                            </li>
                                                                                            <?php
                                                                                        }
                                                                                        $premium_days_with_scheme_totalprice = $premiumdayswithscheme * ($arr[$i]["PkgRateWeekEnd"] - ($arr[$i]["PkgRateWeekEnd"] * $fleets1[0]['WeekendDis']) / 100);
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <!-- New Code-->


                                                                                <li>Minimum billing: Rs. <strike><?php echo intval($arr[$i]["BasicAmt"]); ?></strike>
                                                                                <?php
                                                                                echo $totalschemeamount;
                                                                                ?>
                                                                                /-</li>
                                                                                <li>VAT (@<?php echo number_format($vat, 2); ?>%): Rs. <?php echo intval(ceil(($totalschemeamount * $vat) / 100)); ?>/-</li>
                                                                                <li>Total fare: Rs.
                                                                                    <?php
                                                                                    if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {
                                                                                        ?>
                                                                                    <strike><?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?></strike>
                                                                                    <?php echo intval(ceil($totalschemeamount + ($totalschemeamount * $vat) / 100)); ?>
                                                                                    <?php
                                                                                } else {
                                                                                    echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100));
                                                                                }
                                                                                ?>


                                                                                /-</li>
                                                                                <?php
                                                                                if (intval($arr[$i]["OriginalAmt"]) > intval($arr[$i]["BasicAmt"])) {
                                                                                    ?>
                                                                                    <li>Discount Amount (On base fare): Rs. <?php echo intval($arr[$i]["OriginalAmt"] - $totalschemeamount); ?>/-</li>

                                                                                    <?php
                                                                                }
                                                                                if (intval($_REQUEST["hdOriginID"]) != "69") {
                                                                                    ?>
                                                                                    <li>Refundable security deposit (pre-auth from card): Rs. <?php echo $secDeposit; ?> (Mastercard/Visa/Amex)</li>
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <li>The billing cycle starts from 8am each day</li>
                                                                                    <li>Refundable Security Deposit: Rs. <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </ul>

                                                                            <p><span>Mandatory Documents(Original)</span></p>
                                                                            <?php
                                                                            if (intval($_REQUEST["hdOriginID"]) != "69") {
                                                                                ?>
                                                                                <ul style="margin-left:15px;">
                                                                                    <li>Passport / Voter ID card</li>
                                                                                    <li>Driving license</li>
                                                                                    <li>Credit card</li>
                                                                                    <li>Adhaar card</li>
                                                                                </ul>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <ul>
                                                                                    <li style="margin-left:15px;">Passport / Voter ID card</li>
                                                                                    <li>Driving license</li>
                                                                                    <li>Any of the following has to be submitted in original as an identity proof.
                                                                                        <ol type="1">
                                                                                            <li>Adhaar card</li>
                                                                                            <li>Pan card</li>
                                                                                            <li>Voter ID card</li>
                                                                                        </ol>
                                                                                    </li>
                                                                                    <li>Please note that the car in Goa will be provided through a partner.</li>
                                                                                </ul>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>







                                                                    <?php } //freedom end  ?>

                                                                    </div>
                                                                    <div class="col4">
                                                                        <div class="boknw next_avai">
                                                                            <?php
                                                                            $nddate = "";
                                                                            if (intval($arr[$i]["IsAvailable"]) == 0) {
                                                                                //$resNext = $cor->_COR_SelfDrive_NextAvailibility(array("ModelID"=>$arr[$i]["ModelID"],"CarCatID"=>$arr[$i]["CarCatID"],"CityID"=>$_POST["hdOriginID"],"PickUpDate"=>$pickDate->format('Y-m-d'),"DropOffDate"=>$dropDate->format('Y-m-d'),"subLocationID"=>"0"));
                                                                                $resNext = $cor->_COR_SelfDrive_NextAvailibility(array("ModelID" => $arr[$i]["ModelID"], "CarCatID" => $arr[$i]["CarCatID"], "CityID" => $_POST["hdOriginID"], "PickUpDate" => $pickDate->format('Y-m-d'), "DropOffDate" => $dropDate->format('Y-m-d'), "subLocationID" => "0", "DropOffTime" => $dropDate->format('H') . $dropDate->format('i')));
                                                                                $NADate = $myXML->xml2ary($resNext);
                                                                                $nextAvailablity = date_create($NADate[0]['Pickupdate']);
                                                                                if (intval(count($AdvResults))) {
                                                                                    ?>	

                                                                                                                                    <!--<a id = "popup<?php echo $i; ?>" class="pflexi" onclick ="div_show(<?php echo $i; ?>)">Flexible Plans? <img border="0" src="<?php echo corWebRoot; ?>/images/flexi_arrow.png"/></a>-->

                                                                                    <?php
                                                                                }
                                                                                ?>		
                                                                                <label style="font-size:14px;width: 120px;text-align: left;">

                                                                                    <a class="next_available_flex" href="javascript: void(0);" onclick="javascript: document.getElementById('formSrchINA<?php echo $i; ?>').submit();">Next Available <img border="0" src="<?php echo corWebRoot; ?>/images/flexi_arrow.png"/></a><div class="date_formate"><?php echo $nextAvailablity->format('d-M, Y H:i') ?></div></label>
                                                                                <?php
                                                                                $datHM = explode(':', $nextAvailablity->format('H:i'));
                                                                                $dateH = $datHM[0];
                                                                                $dateM = $datHM[1];

                                                                                $Date1 = $nextAvailablity->format('Y-m-d');
                                                                                $nddate = date('Y-m-d', strtotime($Date1 . " + " . $interval . " day"));


                                                                                $enddate = date('j M, Y', strtotime($nddate));
                                                                            } else {
                                                                                if (!$isOffer) {
                                                                                    ?>
                                                                                    <input type="submit" class="genbtnlinksmall" id="btnBookNow<?php echo $i; ?>" name="btnBookNow" value="Book Now"  />
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <img src="<?php echo corWebRoot; ?>/images/sold-out.png" border="0" title="Sold out" alt="Sold out" style="width:50px;" />
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                    <!-- kaushal end -->
                                                                    </div><!-- repeatdiv Self Drive-->
                                                                    <?php
                                                                    if (intval($arr[$i]["IsAvailable"]) == 1) {
                                                                        ?>				   
                                                                        </form>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    if (intval($arr[$i]["IsAvailable"]) == 0) {

                                                                        if ((count($AdvResults))) {
                                                                            //echo '<pre>';print_r($AdvResults);
                                                                            ?>				
                                                                            <div id="flaxi_option<?php echo $i; ?>" class="flaxi_option"></div>
                                                                            <div id="popup_flaxi<?php echo $i; ?>" class="pop_flexi"> 
                                                                                <h1>Best Available Options For Your Selected Travel Period</h1>
                                                                                <div class="close_popup"><a  onclick ="div_hide(<?php echo $i; ?>)" href="javascript:void(0);">X</a></div>
                                                                                <?php
                                                                                $f = 0;
                                                                                foreach ($AdvResults as $AdvResult) {

                                                                                    $pick = str_split($AdvResult["Pickuptime"], 2);
                                                                                    $pickH = $pick[0];
                                                                                    $pickM = $pick[1];

                                                                                    $drop = str_split($AdvResult["Dropofftime"], 2);
                                                                                    $dropH = $drop[0];
                                                                                    $dropM = $drop[1];

                                                                                    $pkdate = date_create($AdvResult["Pickupdate"]);
                                                                                    $dkdate = date_create($AdvResult["Dropoffdate"]);
                                                                                    ?>
                                                                                    <div class="p20">
                                                                                        <div class="row">
                                                                                            <div class="f_l w_24 mr5">
                                                                                                <span>Pick Up Date and Time</span>
                                                                                                <input type="text" value="<?php echo $pkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Pickuptime"])); ?>" readonly="readonly">
                                                                                            </div>
                                                                                            <div class="f_l w_24 mr5">
                                                                                                <span>Drop Off Date and Time</span>
                                                                                                <input type="text" value="<?php echo $dkdate->format('j M, Y') . " " . date('H:i', strtotime($AdvResult["Dropofftime"])); ?>" readonly="readonly">
                                                                                            </div>
                                                                                            <div class="f_l w_24 mr3">
                                                                                                <span>Available At</span>
                                                                                                <?php echo $AdvResult['SublocationName']; ?>
                                                                                            </div>
                                                                                            <div class="f_l w_12">
                                                                                                <input type="button" value="Book Now" name="Book Now"  onclick="javascript: document.getElementById('formSrchFlexi<?php echo $f; ?>').submit();">
                                                                                            </div>
                                                                                            <div class="clr"> </div>
                                                                                        </div>
                                                                                    </div>


                                                                                    <form id="formSrchFlexi<?php echo $f; ?>" name="formSrchFlexi<?php echo $f; ?>" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/">
                                                                                        <input type="hidden" name="urlcurdate" id="hdUrlcurdate<?php echo $i; ?>" value="<?php echo $_REQUEST['curdatetime']; ?>"/>
                                                                                        <input type="hidden" id="hdCarModelINA<?php echo $i; ?>" name="hdCarModel" value="<?php echo trim($carModel); ?>" />
                                                                                        <input type="hidden" name="hdCat" id="hdCat<?php echo $i; ?>" value="<?php echo $carCatgName; ?>" />
                                                                                        <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                                                                                        <input type="hidden" name="hdOriginName" id="hdOriginNameINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginName"] ?>" />
                                                                                        <input type="hidden" name="hdOriginID" id="hdOriginIDINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdOriginID"]; ?>"/>
                                                                                        <input type="hidden" name="hdDestinationName" id="hdDestinationNameINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationName"]; ?>"/>
                                                                                        <input type="hidden" name="hdDestinationID" id="hdDestinationIDINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdDestinationID"]; ?>"/>
                                                                                        <input type="hidden" name="hdCarID" id="hdCarIDINA<?php echo $i; ?>" value="<?php echo $AdvResult["carID"]; ?>" />
                                                                                        <input type="hidden" name="hdCarCatID" id="hdCarCatIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                                                        <input type="hidden" name="hdPkgID" id="hdPkgIDINA<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                                                                                        <input type="hidden" name="hdCat" id="hdCatINA<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatName"]; ?>" />
                                                                                        <input type="hidden" name="PkgRate" id="PkgRateINA<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                                                                                        <?php if ($_POST["chkPkgType"] == "Daily" || $_POST["chkPkgType"] == "Weekly" || $_POST["chkPkgType"] == "Monthly") { ?>
                                                                                            <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["duration"]; ?>" />
                                                                                        <?php } else { ?>
                                                                                            <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["nHours"]; ?>" />
                                                                                        <?php } ?>
                                                                                        <input type="hidden" name="totAmount" id="totAmountINA<?php echo $i; ?>" value="<?php echo $totAmount; ?>" />
                                                                                        <input type="hidden" name="totFare" id="totFareINA<?php echo $i; ?>" value="<?php echo intval(ceil($arr[$i]["BasicAmt"] + ($arr[$i]["BasicAmt"] * $vat) / 100)); ?>" />
                                                                                        <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />
                                                                                        <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y'); ?>" />
                                                                                        <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                                                                                        <input type="hidden" name="hdTourtype" id="tourtypeINA<?php echo $i; ?>" value="<?php echo $_REQUEST["hdTourtype"] ?>" />
                                                                                        <input type="hidden" name="tHourP" id="tHourPINA<?php echo $i; ?>" value="<?php echo $pickH; ?>" />
                                                                                        <input type="hidden" name="tMinP" id="tMinPINA<?php echo $i; ?>" value="<?php echo $pickM; ?>" />
                                                                                        <input type="hidden" name="tHourD" id="tHourDINA<?php echo $i; ?>" value="<?php echo $dropH ?>" />
                                                                                        <input type="hidden" name="tMinD" id="tMinDINA<?php echo $i; ?>" value="<?php echo $dropM ?>" />
                                                                                        <input type="hidden" name="vat" id="vatINA<?php echo $i; ?>" value="<?php echo $vat; ?>" />
                                                                                        <input type="hidden" name="secDeposit" id="secDepositINA<?php echo $i; ?>" value="<?php echo $secDeposit; ?>" />
                                                                                        <input type="hidden" name="subLoc" id="subLoc<?php echo $i; ?>" value="<?php echo $AdvResult['SubLocationID']; ?>" />

                                                                                        <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_REQUEST["chkPkgType"] ?>" />
                                                                                        <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_REQUEST["nHours"] ?>" />
                                                                                        <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["OriginalAmt"]); ?>" />
                                                                                        <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo ceil($arr[$i]["BasicAmt"]); ?>" />
                                                                                        <input type="hidden" name="allSubLoc" id="allSubLoc<?php echo $i; ?>" value="<?php echo $AdvResult['SubLocationID']; ?>" />
                                                                                        <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $arr[$i]["KMIncluded"]; ?>" />
                                                                                        <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $arr[$i]["ExtraKMRate"]; ?>" />
                                                                                        <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="<?php echo $_REQUEST["customPkg"]; ?>" />
                                                                                        <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekDayDuration"]; ?>"> 
                                                                                        <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["WeekEndDuration"]; ?>">
                                                                                        <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $arr[$i]["FreeDuration"]; ?>"> 
                                                                                        <input type="hidden" name="pkgR" id="pkgR<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgRate"]; ?>">
                                                                                        <input type="hidden" name="FlexiBooking"  value="FlexiBooking"> 	<!----flexi booking------>	 				  
                                                                                    </form>
                                                                                    <?php
                                                                                    $f++;
                                                                                }
                                                                                ?>	
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                        <form id="formSrchINA<?php echo $i; ?>" name="formSrchINA<?php echo $i; ?>" action="<?php echo corWebRoot; ?>/self-drive/search/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/" method="POST">				 
                                                                            <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo trim($carModel); ?>" />
                                                                            <input type="hidden" id="hdCarModelID<?php echo $i; ?>" name="hdCarModelID" value="<?php echo $arr[$i]["ModelID"]; ?>" />
                                                                            <input type="hidden" name="ddlOrigin" id="ddlOriginDSF<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"]; ?>" />
                                                                            <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"] ?>" />
                                                                            <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                                            <input type="hidden" name="hdTourtype" id="hdTourtypeSF<?php echo $i; ?>" value="Selfdrive" />
                                                                            <input type="hidden" name="bookTime" id="bookTimeSF<?php echo $i; ?>" value="<?php echo $_POST["bookTime"]; ?>" />
                                                                            <input type="hidden" name="userTime" id="userTimeSF<?php echo $i; ?>" value="<?php echo $_POST["userTime"]; ?>" />
                                                                            <input type="hidden" name="onlyE2C" id="onlyE2C<?php echo $i; ?>" value="0" />
                                                                            <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_POST["chkPkgType"]; ?>" />
                                                                            <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]; ?>"/>
                                                                            <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                                            <input type="hidden" name="hdCarID" id="hdCarID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarID"]; ?>" />
                                                                            <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                                            <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["pkgid"]; ?>" />
                                                                            <input type="hidden" name="hdCat" id="hdCat<?php echo $i; ?>" value="<?php echo $carCatgName; ?>" />
                                                                            <input type="hidden" name="PkgRate" id="PkgRate<?php echo $i; ?>" value="<?php echo intval($arr[$i]["DayRate"]); ?>" />
                                                                            <input type="hidden" name="tHourP" id="tHourP<?php echo $i; ?>" value="<?php echo $dateH; ?>" />
                                                                            <input type="hidden" name="tMinP" id="tMinP<?php echo $i; ?>" value="<?php echo $dateM; ?>" />
                                                                            <input type="hidden" name="tHourD" id="tHourD<?php echo $i; ?>" value="<?php echo $dateH; ?>" />
                                                                            <input type="hidden" name="tMinD" id="tMinD<?php echo $i; ?>" value="<?php echo $dateM; ?>" />
                                                                            <input type="hidden" name="nHours" id="nHours<?php echo $i; ?>" value="<?php echo $_POST["nHours"]; ?>" />
                                                                            <input type="hidden" name="pickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $nextAvailablity->format('j M, Y'); ?>" />
                                                                            <input type="hidden" name="dropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $enddate; ?>" />
                                                                            <input type="hidden" name="customPkg" id="customPkg<?php echo $i; ?>" value="<?php echo $_POST["customPkg"]; ?>" />
                                                                            <input type="hidden" name="pkgR"  id="pkgR<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgRate"]; ?>">



                                                                            <?php if ($_POST["chkPkgType"] == "Daily" || $_POST["chkPkgType"] == "Weekly" || $_POST["chkPkgType"] == "Monthly") { ?>
                                                                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["duration"]; ?>" />
                                                                            <?php } else { ?>
                                                                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["nHours"]; ?>" />
                                                                            <?php } ?>
                                                                        </form>
                                                                        <?php
                                                                    }
                                                                }
                                                            }                  // selfdrive ended
                                                            else {
                                                                if (count($arr) > 0) {



                                                                    $dayFare = array();
                                                                    $isAvail = array();
                                                                    foreach ($arr as $k => $v) {
                                                                        $dayFare[$k] = $v['DayRate'];
                                                                        $isAvail[$k] = $v['IsAvailable'];
                                                                    }
                                                                    array_multisort($isAvail, SORT_DESC, $dayFare, SORT_ASC, $arr);
                                                                    for ($i = 0; $i < count($arr); $i++) {
                                                                        $paramArray = array("cityid" => $orgIDs[0], "CategoryID" => $arr[$i]['CarCatID'], "PickUpDate" => $pickDate->format('Y-m-d'),
                                                                            "PickUpTime" => 1000,
                                                                            "DropOffDate" => $dropDate->format("Y-m-d"),
                                                                            "DropOffTime" => 1800);

                                                                        $soldout = @$cor->_CORGetSoldOutWeb($paramArray);
                                                                        $soldout = $myXML->xml2ary($soldout);

                                                                        $btnid = 1;
                                                                        if ($_POST["hdTourtype"] != "Local") {
                                                                            $kmPerDay = intval(($totalKM / 1000) / $interval);
                                                                            if ($kmPerDay < 250)
                                                                                $totAmount = intval(250 * $interval * $arr[$i]["KMRate"] + ($interval * $arr[$i]["ChaufferAllowance"] + ($arr[$i]["NightStayAllowance"] * $intervalnight)));
                                                                            else
                                                                                $totAmount = intval($kmPerDay * $interval * $arr[$i]["KMRate"] + ($interval * $arr[$i]["ChaufferAllowance"] + ($arr[$i]["NightStayAllowance"] * $intervalnight)));

                                                                            $totAmountforPopUp = $totAmount;
                                                                            $totAmount = intval($totAmount + (($totAmount * $arr[$i]["Stax"]) / 100));
                                                                            $serviceTax = $totAmount - $totAmountforPopUp;
                                                                            $oldTotalAmount = $totAmount;
                                                                        } else
                                                                            $totAmount = intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval;
                                                                        if ($_POST["hdTourtype"] == "Outstation") {
                                                                            ?>
                                                                            <form id="formBook<?php echo $i; ?>" name="formBook" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>-to-<?php echo trim(str_ireplace(",", "-to-", str_ireplace(" ", "-", strtolower($_POST["hdDestinationName"])))); ?>/">
                                                                                <?php
                                                                            } else if ($_POST["hdTourtype"] == "Local") {
                                                                                ?>
                                                                                <form id="formBook<?php echo $i; ?>" name="formBook" method="POST" action="../../contact-information/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/">
                                                                                    <?php
                                                                                }
                                                                                if ($_POST["hdTourtype"] == "Outstation") {


                                                                                    if ($arr[$i]["CarCatName"] == 'Intermediate')
                                                                                        $carCatgName = "Budget";
                                                                                    else if ($arr[$i]["CarCatName"] == 'Van')
                                                                                        $carCatgName = "Family";
                                                                                    else if ($arr[$i]["CarCatName"] == 'Superior')
                                                                                        $carCatgName = "Business";
                                                                                    else if ($arr[$i]["CarCatName"] == 'Premium')
                                                                                        $carCatgName = "Premium";
                                                                                    else if ($arr[$i]["CarCatName"] == 'Luxury')
                                                                                        $carCatgName = "Luxury";
                                                                                    else if ($arr[$i]["CarCatName"] == 'Special')
                                                                                        $carCatgName = "Super Luxury";
                                                                                    else
                                                                                        $carCatgName = $arr[$i]["CarCatName"];
                                                                                } else {
                                                                                    $carCatgName = $arr[$i]["CarCatName"];
                                                                                }
                                                                                ?>
                                                                                <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $carCatgName; ?>" />
                                                                                <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo $_POST["hdOriginName"] ?>" />
                                                                                <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                                                <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationName"]; ?>"/>
                                                                                <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                                                <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $arr[$i]["PkgID"]; ?>" />
                                                                                <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $arr[$i]["CarCatID"]; ?>" />
                                                                                <!--<input type="hidden" name="address1" id="address1<?php //echo $i;       ?>" value="<?php //echo $_POST["hdOriginName"];       ?>" />
                                                                                <input type="hidden" name="address2" id="address2<?php //echo $i;       ?>" value="<?php //echo $_POST["hdDestinationName"]      ?>" />-->
                                                                                <input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["DayRate"]); ?>" />
                                                                                <input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["KMRate"]); ?>" />
                                                                                <input type="hidden" name="PkgHrs" id="PkgHrs<?php echo $i; ?>" value="<?php echo intval($arr[$i]["PkgHrs"]); ?>" />
                                                                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $interval; ?>" />
                                                                                <!--Change by Iqbal on 31-Aug-2012 STARTS-->
                                                                                <input type="hidden" name="cabHour" id="cabHour<?php echo $i; ?>" value="<?php echo $_POST["cabHour"] ?>" />
                                                                                <!--Change by Iqbal on 31-Aug-2012 ENDS-->				
                                                                                <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $cor->encrypt($totAmount); ?>" />
                                                                                <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="2" />				
                                                                                <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $pickDate->format('m/d/Y'); ?>" />
                                                                                <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $dropDate->format('m/d/Y'); ?>" />
                                                                                <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                                                                                <!--Change by Iqbal on 09-May-2012 STARTS-->
                                                                                <input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo $cor->encrypt($totalKM / 1000); ?>" />
                                                                                <!--Change by Iqbal on 09-May-2012 ENDS-->
                                                                                <input type="hidden" name="tHour" id="tHour<?php echo $i; ?>" value="<?php
                                                                                if (isset($_POST["tHour"])) {
                                                                                    echo $_POST["tHour"];
                                                                                }
                                                                                ?>" />
                                                                                <input type="hidden" name="tMin" id="tMin<?php echo $i; ?>" value="<?php
                                                                                if (isset($_POST["tMin"])) {
                                                                                    echo $_POST["tMin"];
                                                                                }
                                                                                ?>" />
                                                                                <input type="hidden" name="ChauffeurCharges" id="ChauffeurCharges<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["ChaufferAllowance"]); ?>" />
                                                                                <input type="hidden" name="totAmount" id="totAmount<?php echo $i; ?>" value="<?php echo $cor->encrypt($totAmount); ?>" />
                                                                                <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["Stax"]); ?>" />
                                                                                <input type="hidden" name="NightStayAllowance" id="NightStayAllowance<?php echo $i; ?>" value="<?php echo $cor->encrypt($arr[$i]["NightStayAllowance"]); ?>"> 
                                                                                <input type="hidden" name="nightduration" id="nightduration<?php echo $i; ?>" value="<?php echo $intervalnight; ?>" />
<input type="hidden" name="CGSTPercent" id="CGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['CGSTPercent']);?>" />
							<input type="hidden" name="SGSTPercent" id="SGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['SGSTPercent']);?>" />
							<input type="hidden" name="IGSTPercent" id="IGSTPercent" value="<?php echo $cor->encrypt($arr[$i]['IGSTPercent']);?>" />
							<input type="hidden" name="GSTEnabledYN" id="GSTEnabledYN" value="<?php echo $cor->encrypt($arr[$i]['GSTEnabledYN']); ?>"/>
							<input type="hidden" name="ClientGSTId" id="ClientGSTId" value="<?php echo $cor->encrypt($arr[$i]['ClientGSTId']); ?>"/>


                                                                                <div style="float:left;width:100%;">
                                                                                    <div class="row">
                                                                                        <div style="float:left;width:25%;">
                                                                                            <a href="javascript: void(0);" style="padding-left:5%;">
                                                                                                <img width="50px" src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ", "-", $carCatgName); ?>.jpg" alt="" />
                                                                                            </a>
                                                                                        </div>
                                                                                        <div style="float:left;width:25%;">
                                                                                            <?php
                                                                                            $carModel = "";
                                                                                            $passCount = 4;
                                                                                            if ($arr[$i]["CarCatName"] == 'Economy')
                                                                                                $carModel = "Indica, Wagon R<br />Ecco, Santro" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Compact')
                                                                                                $carModel = "Indigo, Esteem<br />Swift" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Intermediate')
                                                                                                $carModel = "Dzire, Fusion<br />Ikon,  Accent" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Van') {
                                                                                                $carModel = "Innova, Xylo" . "<br />";
                                                                                                $passCount = 6;
                                                                                            } else if ($arr[$i]["CarCatName"] == 'Standard')
                                                                                                $carModel = "City, Fiesta<br />SX4, Verna" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Superior')
                                                                                                $carModel = "Corolla, Elantra<br />Cruze" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Premium')
                                                                                                $carModel = "Accord, Camry<br />Mercedes-C" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == 'Luxury')
                                                                                                $carModel = "Mercedes-E<br />BMW 5 Series" . "<br />";
                                                                                            else if ($arr[$i]["CarCatName"] == '4 Wheel Drive' || $arr[$i]["CarCatName"] == 'Full Size') {
                                                                                                $carModel = "Endeavour, Fortuner" . "<br />";
                                                                                                $passCount = 6;
                                                                                            }
                                                                                            //echo $carModel;
                                                                                            ?>
                                                                                            <span><b><?php echo $carCatgName; ?></b></span><br /><?php echo $carModel; ?>
                                                                                            <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $carModel; ?>" />
                                                                                            <div style="float:left;width:100%">
                                                                                                <label class="noof"><?php echo $passCount; ?></label>
                                                                                                <label class="tx">+ driver</label>
                                                                                            </div>	
                                                                                        </div>
                                                                                        <div style="float:left;width:25%;">
                                                                                            <?php
                                                                                            $oldDayRate = $arr[$i]["DayRate"];
                                                                                            $oldKMRate = $arr[$i]["KMRate"];
                                                                                            if (file_exists("./xml/rates/" . strtolower(str_ireplace(" ", "-", $arr[$i]["CarCatName"])) . ".xml")) {
                                                                                                $rate = file_get_contents("./xml/rates/" . str_ireplace(" ", "-", $arr[$i]["CarCatName"]) . ".xml");
                                                                                                if ($rate != "") {
                                                                                                    $xmlRate = new SimpleXmlElement($rate);
                                                                                                    $oldDayRate = $xmlRate->{'dayrate'};
                                                                                                    $oldKMRate = $xmlRate->{'kmrate'};
                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                            <?php if ($_POST["hdTourtype"] == "Outstation") { ?>

                                                                                                <span class="sep"></span>
                                                                                                <?php
                                                                                                if ($oldTotalAmount > $totAmount) {
                                                                                                    ?>
                                                                                                    Rs. <?php echo $totAmountforPopUp; ?>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    Rs. <?php echo $totAmountforPopUp; //$totAmount;      ?>/-
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <br /><a class="faredetails" href="javascript:void(0)" onclick="javascript: _pass('popupbox<?php echo $i; ?>');" id="link_<?php echo $i; ?>">Fare details</a>
                                                                                            </div>
                                                                                            <div class="details_wrapper" id="popupbox<?php echo $i; ?>" style="display:none">
                                                                                                <div class="toolarrow"></div>
                                                                                                <div class="heading">Fare Details <span class="closedpop" onclick="javascript: _closeFD('popupbox<?php echo $i; ?>');">X</span></div>
                                                                                                <div style="clear: both;"></div>
                                                                                                <p style="font-weight:bold;margin-top:10px;width:100%"><span>Rs. <?php echo $totAmountforPopUp; //$totAmount;      ?>/-</span></p>
                                                                                                <?php
                                                                                                $kmPerDay = intval(($totalKM / 1000) / $interval);
                                                                                                if ($kmPerDay < 250) {
                                                                                                    $displayKMS = 250;
                                                                                                    ?>
                                                                                                                                                                    <!--<p><span>Rs.. (<?php echo intval($arr[$i]["KMRate"]); ?> * <?php echo $displayKMS; ?> * <?php echo $interval; ?> + <?php echo $arr[$i]["ChaufferAllowance"]; ?> * <?php echo $interval; ?>) * 104.95% = Rs.. <?php echo $totAmount; ?>/-</span></p>-->
                                                                                                    <?php
                                                                                                } else {
                                                                                                    $displayKMS = intval($kmPerDay * $interval);
                                                                                                    ?>
                                                                                                                                                                    <!--<p><span>Rs.. (<?php echo intval($arr[$i]["KMRate"]); ?> * <?php echo $displayKMS; ?>  + <?php echo $arr[$i]["ChaufferAllowance"]; ?> * <?php echo $interval; ?>) * 104.95% = Rs.. <?php echo $totAmount; ?>/-</span></p>-->
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <p style="font-weight:bold;margin-top:10px;"><span>Includes </span></p>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <?php
                                                                                                    if ($kmPerDay < 250) {
                                                                                                        ?>
                                                                                                        <li><?php echo $displayKMS; ?> * <?php echo $interval; ?> = <?php echo intval($displayKMS * $interval); ?> Kms</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li><?php echo intval(($totalKM / 1000)); ?> Kms</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>Per Km charge = Rs..<?php echo intval($arr[$i]["KMRate"]); ?>/-</li>
                                                                                                    <li>No. of days = <?php echo $interval; ?> day(s)</li>
                                                                                                    <li>Chauffeur charge = Rs..<?php echo number_format($arr[$i]["ChaufferAllowance"]); ?> * <?php echo $interval; ?></li>
                                                                                                    <li>Night Stay Allowance Charge =Rs. <?php echo number_format($arr[$i]["NightStayAllowance"]); ?>* <?php echo $intervalnight; ?></li>
                                                                                                    <li>Minimum billable kms per day = 250 kms</li>
                                                                                                </ul>
                                                                                                <div style="clear: both;"></div>
                                                                                                <p style="font-weight:bold;margin-top:10px;"><span>Extra Charges</span></p>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>GST</li>
                                                                                                    <li>Tolls, parking and state permits as per actuals</li>
                                                                                                    <?php
                                                                                                    if ($kmPerDay < 250) {
                                                                                                        ?>
                                                                                                        <li>Extra Km beyond <?php echo intval($displayKMS * $interval); ?> =  Rs..<?php echo intval($arr[$i]["KMRate"]); ?>/km</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li>Extra Km beyond <?php echo intval(($totalKM / 1000)); ?> kms =  Rs..<?php echo intval($arr[$i]["KMRate"]); ?>/km</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>Extra Km =  Rs..<?php echo intval($arr[$i]["KMRate"]); ?>/km</li>
                                                                                                </ul>
                                                                                                <ul>
                                                                                                    <?php
                                                                                                    if (stripos($_POST["hdDestinationName"], "Tirupati") > -1) {
                                                                                                        ?>
                                                                                                        <li>Approx Rs. 1000 to be paid on entry to Tirupati as per actuals</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <?php
                                                                                        }
                                                                                        if ($_POST["hdTourtype"] == "Local") {
                                                                                            ?>

                                                                                            Rs. <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval; ?><br />

                                                                                            <a class="faredetails" href="javascript:void(0)" onclick="javascript: _pass('popupbox<?php echo $i; ?>');" id="link_<?php echo $i; ?>">Fare details</a>

                                                                                    <!--<span id="ffare<?php echo $i; ?>">Rs.. <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]); ?> / hr</span><br />-->
                                                                                        </div>
                                                                                        <div class="details_wrapper" id="popupbox<?php echo $i; ?>" style="display:none">
                                                                                            <div class="toolarrow"></div>
                                                                                            <div class="heading">Fare Details <span class="closedpop" onclick="javascript: _closeFD('popupbox<?php echo $i; ?>');">X</span></div>
                                                                                            <p style="margin-top:10px;float:left;font-weight:bold;width:100%"><span>Rate</span></p>
                                                                                            <p><big><span id="ffarepp<?php echo $i; ?>">Rs. <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]); ?> / hr</span></big></p>
                                                                                            <p><span>Minimum Billing</span></p>
                                                                                            <p>Rs. <?php echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval; ?></p>
                                                                                            <p><span>Fare Details</span></p>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <li>10 km included for every hour of travel</li>
                                                                                                <li>Rs. <?php echo ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10)); ?> / km for additional km</li>
                                                                                                <li>Rs. <?php
                                                                                                    echo intval($arr[$i]["DayRate"] / $arr[$i]["PkgHrs"]) * $interval;
                                                                                                    ;
                                                                                                    ?> minimum billing</li>
                                                                                                <li>New hour billing starts when usage more than 30 mins</li>
                                                                                                <?php if ($_POST["hdOriginID"] == 2) { ?>
                                                                                                    <?php
                                                                                                    $charge = 200;
                                                                                                    if ($passCount > 4) {
                                                                                                        $charge = 300;
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs. <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 7) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($arr[$i]["DayRate"] / ($arr[$i]["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                    <div style="width:25%;float:left">
                                                                                        <div class="boknw">


                                                                                            <?php
                                                                                            if (count($soldout) == 0) {
                                                                                                ?>
                                                                                                <a class="genbtnlinksmall" href="javascript:void(0);" id="btnBookNow<?php echo $i; ?>" name="btnBookNow"onclick="javascript: document.getElementById('formBook<?php echo $i; ?>').submit();">Book Now</a>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <img src="<?php echo corWebRoot; ?>/images/sold-out.png" border="0" title="Sold out" alt="Sold out" style="width:50px;" />
                                                                                                <?php
                                                                                            }
                                                                                            ?>



                                                                                        </div>
                                                                                    </div>
                                                                                </div><!--row-->
                                                                                </div><!-- repeatdiv -->

                                                                            </form>
                                                                            <?php
                                                                            //}
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <div class="repeatdiv">
                                                                            <div class="typefcr">
                                                                                No car found.
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                $btnid++;
                                                                unset($cor);
                                                                ?>
                                                                </div><!-- selectcarmid -->
                                                                </div><!-- main -->
                                                                </div><!-- country1 -->

                                                                </div><!--End warp all list-->
                                                                <?php
                                                            } if ($tab == 2) {
                                                                $emailid = "Email";
                                                                $gname = "Name";
                                                                if (isset($_COOKIE["tcciid"]))
                                                                    $cciid = $_COOKIE["tcciid"];
                                                                if (isset($_COOKIE["cciid"]))
                                                                    $cciid = $_COOKIE["cciid"];
                                                                if (isset($_COOKIE["emailid"]))
                                                                    $emailid = $_COOKIE["emailid"];
                                                                if (isset($_COOKIE["phone1"]))
                                                                    $phone1 = $_COOKIE["phone1"];
                                                                if (isset($_COOKIE["gname"]))
                                                                    $gname = $_COOKIE["gname"];
                                                                $bookdays = strtotime($_POST["hdPickdate"]) - strtotime(date('Y-m-d'));
                                                                $bookdays = ($bookdays / (60 * 60 * 24));
                                                                $xi = $i;
                                                                $pickDate = date_create($_POST["hdPickdate"]);
                                                                if (isset($_POST["hdDropdate"]))
                                                                    $dropDate = date_create($_POST["hdDropdate"]);
                                                                ?>
                                                                <div id="country2" class="tabcontent">
                                                                    <?php
                                                                    if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                        $subLocCost = 0;
                                                                        if (isset($_POST["subLoc"]) && $_POST["subLoc"] != "") {
                                                                            $data = array();
                                                                            $data["subLoctionID"] = $_POST["subLoc"];
                                                                            $data["basicAmount"] = $_POST["BasicAmt"];
                                                                            $cor = new COR();
                                                                            $res = $cor->_CORGetSubLocCost($data);
                                                                            $subLocCost = ceil($res->{"SublocationPercentCostResult"});
                                                                            unset($cor);
                                                                            unset($data);
                                                                        }
                                                                        $totalCost = $_POST["BasicAmt"] + $subLocCost;
                                                                        // added vinod 
                                                                        $Vatamount = intval(ceil(($totalCost * $_POST["vat"]) / 100));
                                                                        ?>

                                                                        <!-- added by gita /vinod-->
                                                                        <form id="formTravelDetail" name="formTravelDetail" method="POST" action="../../payment/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/">
                                                                            <input type="hidden" id="browser" name="browser" value=""/>
                                                                            <input type="hidden" id="ipaddress" name="ipaddress" value=""/>
                                                                            <input type="hidden" name="hdCarModel" id="hdCarModel<?php echo $i; ?>" value="<?php echo $_POST["hdCarModel"]; ?>" />
                                                                            <input type="hidden" name="hdCarModelID" id="hdCarModelID<?php echo $i; ?>" value="<?php echo $_POST["hdCarModelID"]; ?>" />
                                                                            <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo trim($_POST["hdOriginName"]); ?>" />
                                                                            <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                                            <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo trim($_POST["hdDestinationName"]); ?>"/>
                                                                            <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                                            <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $_POST["hdPkgID"]; ?>">
                                                                            <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $_POST["hdCat"]; ?>" />
                                                                            <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $_POST["hdCarCatID"]; ?>" />
                                                                            <input type="hidden" name="PkgRate" id="PkgRate<?php echo $i; ?>" value="<?php echo $_POST["PkgRate"]; ?>" />
                                                                            <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["duration"]; ?>" />
                                                                            <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $_POST["totFare"]; ?>" />
                                                                            <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="3" />

                                                                            <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $_POST["BasicAmt"]; ?>" />

                                                                            <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $_POST["hdPickdate"] ?>" />
                                                                            <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $_POST["hdDropdate"] ?>" />
                                                                            <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                                                                            <input type="hidden" name="monumber" id="mobile<?php echo $i; ?>" value="<?php echo $phone1; ?>" />
                                                                            <input type="hidden" name="name" id="name<?php echo $i; ?>" value="<?php echo $gname; ?>" />
                                                                            <input type="hidden" name="email" id="email<?php echo $i; ?>" value="<?php echo $emailid; ?>" />
                                                                            <input type="hidden" name="cciid" id="cciid<?php echo $i; ?>" value="<?php echo $cciid; ?>" />
                                                                            <input type="hidden" name="bookTime" id="bookTime" value="<?php echo date('h:i A'); ?>" />

                                                                            <!-- added vinod-->
                                                                            <input type="hidden" name="WeekDayDuration" id="WeekDayDuration<?php echo $i; ?>" value="<?php echo $_POST["WeekDayDuration"]; ?>"> 
                                                                            <input type="hidden" name="WeekEndDuration" id="WeekEndDuration<?php echo $i; ?>" value="<?php echo $_POST["WeekEndDuration"]; ?>">
                                                                            <input type="hidden" name="FreeDuration" id="FreeDuration<?php echo $i; ?>" value="<?php echo $_POST["FreeDuration"]; ?>"> 
                                                                            <input type="hidden" name="pkgR"  id="pkgR<?php echo $i; ?>" value="<?php echo $_REQUEST["pkgR"]; ?>">
                                                                            <?php
                                                                            if (($_REQUEST['hdCoupon'] == 'FLATDISCOUNT') && ($_REQUEST['hdCoupon'] != '')) {
                                                                                ?>
                                                                                <input type="hidden" name="totFareGross" id="totFareGrossflat" value="<?php echo $_REQUEST["gross_amount_scheme"]; ?>" />
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <input type="hidden" name="totFareGross" id="totFareGross<?php echo $i; ?>" value="<?php echo $_REQUEST["totFare"]; ?>" />
                                                                                <?php
                                                                            }
                                                                            ?>









                                                                            <!-- added vinod-->
                                                                            <?php
                                                                            if (isset($_POST["tHour"]) && isset($_POST["tMin"])) {
                                                                                ?>
                                                                                <input type="hidden" name="userTime" id="userTime" value="<?php echo $_POST["tHour"]; ?><?php echo $_POST["tMin"]; ?>" />
                                                                                <?php
                                                                            } elseif (isset($_POST["tHourP"]) && isset($_POST["tMinP"])) {
                                                                                ?>
                                                                                <input type="hidden" name="userTime" id="userTime" value="<?php echo $_POST["tHourP"]; ?><?php echo $_POST["tMinP"]; ?>" />
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            <input type="hidden" name="bookDays" id="bookDays" value="<?php echo intval($bookdays); ?>" />
                                                                            <input type="hidden" name="tHourP" id="tHourP<?php echo $i; ?>" value="<?php
                                                                            if (isset($_POST["tHourP"])) {
                                                                                echo $_POST["tHourP"];
                                                                            }
                                                                            ?>" />
                                                                            <input type="hidden" name="tMinP" id="tMinP<?php echo $i; ?>" value="<?php
                                                                            if (isset($_POST["tMinP"])) {
                                                                                echo $_POST["tMinP"];
                                                                            }
                                                                            ?>" />
                                                                            <input type="hidden" name="tHourD" id="tHourD<?php echo $i; ?>" value="<?php
                                                                            if (isset($_POST["tHourD"])) {
                                                                                echo $_POST["tHourD"];
                                                                            }
                                                                            ?>" />
                                                                            <input type="hidden" name="tMinD" id="tMinD<?php echo $i; ?>" value="<?php
                                                                            if (isset($_POST["tMinD"])) {
                                                                                echo $_POST["tMinD"];
                                                                            }
                                                                            ?>" />
                                                                            <input type="hidden" name="totAmount" id="totAmount<?php echo $i; ?>" value="<?php echo $_POST["totAmount"]; ?>" />
                                                                            <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $_POST["vat"]; ?>" />
                                                                            <input type="hidden" name="secDeposit" id="secDeposit<?php echo $i; ?>" value="<?php echo $_POST["secDeposit"]; ?>" />
                                                                            <input type="hidden" name="discount" id="discount<?php echo $i; ?>" value="0" />
                                                                            <input type="hidden" name="discountAmt" id="discountAmt<?php echo $i; ?>" value="0" />
                                                                            <input type="hidden" name="addServ" id="addServ<?php echo $i; ?>" value="" />
                                                                            <input type="hidden" name="addServAmt" id="addServAmt<?php echo $i; ?>" value="0" />
                                                                            <input type="hidden" name="address" id="address" value="Address not required for Self Drive" />
                                                                            <input type="hidden" name="subLocName" id="subLocName" value="" />
                                                                            <input type="hidden" name="totFarePB" id="totFarePB" value="<?php echo $_POST["totFare"]; ?>" />


                                                                            <input type="hidden" name="subLocName" id="subLocName<?php echo $i; ?>" value="" />
                                                                            <input type="hidden" name="subLocCost" id="subLocCost<?php echo $i; ?>" value="<?php echo $subLocCost; ?>" />
                                                                            <input type="hidden" name="OriginalAmt" id="OriginalAmt<?php echo $i; ?>" value="<?php echo $_POST["OriginalAmt"]; ?>" />
                                                                            <input type="hidden" name="BasicAmt" id="BasicAmt<?php echo $i; ?>" value="<?php echo $_POST["BasicAmt"]; ?>" />
                                                                            <input type="hidden" name="KMIncluded" id="KMIncluded<?php echo $i; ?>" value="<?php echo $_POST["KMIncluded"]; ?>" />
                                                                            <input type="hidden" name="ExtraKMRate" id="ExtraKMRate<?php echo $i; ?>" value="<?php echo $_POST["ExtraKMRate"]; ?>" />
                                                                            <input type="hidden" name="chkPkgType" id="chkPkgType<?php echo $i; ?>" value="<?php echo $_POST["chkPkgType"] ?>" />
                                                                            <input type="hidden" name="addServiceCostAll" id="addServiceCostAll<?php echo $i; ?>" value="" />
                                                                            <input type="hidden" name="airportCharges" id="airportCharges<?php echo $i; ?>" value="0" />
                                                                            <input type="hidden" name="vatAmt" id="vatAmt<?php echo $i; ?>" value="<?php echo $Vatamount; ?>" />
                                                                            <input type="hidden" name="subtAmount" id="subtAmount<?php echo $i; ?>" value="<?php echo $totalCost; ?>" />
                                                                            <?php
                                                                            if (isset($_REQUEST['hdCoupon']) && ($_REQUEST['hdCoupon'] != '')) {
                                                                                ?>
                                                                                <input type="hidden" name="hdSchemeCoupon"   id="hdSchemeCoupon" value="<?php echo $_REQUEST['hdCoupon']; ?>"/>
                                                                                <?php
                                                                            }
                                                                            if (isset($_REQUEST['hdSchemeDiscount']) && $_REQUEST['hdSchemeDiscount'] != '') {
                                                                                ?>
                                                                                <input type="hidden" name="hdSchemeDiscount"   id="hdSchemeDiscount" value="<?php echo $_REQUEST['hdSchemeDiscount']; ?>"/>
                                                                                <?php
                                                                            }
                                                                            ?>


                                                                            <!-- // by priyanka // -->
                                                                            <input type='hidden' name='intact_fare' id='intact_fare' />
                                                                            <input type='hidden' name='intact_spPay' id='intact_spPay' />
                                                                            <input type='hidden' name='intact_totFare' id='intact_totFare' />
                                                                            <input type='hidden' name='intact_totFarePB' id='intact_totFarePB' />
                                                                            <input type='hidden' name='intact_discountAmt' id='intact_discountAmt' />
                                                                            <input type='hidden' name='intact_disA' id='intact_disA' />
                                                                            <input type='hidden' name='intact_vatt' id='intact_vatt' />
                                                                            <input type='hidden' name='intact_vatAmt' id='intact_vatAmt' />
                                                                            <input type='hidden' name='intact_vatAmt' id='intact_vatAmt' />
                                                                            <input type='hidden' name='intact_subt' id='intact_subt' />

                                                                            <!-- // by priyanka // -->
                                                                            <?php
                                                                        } else {
                                                                            if ($_POST["hdTourtype"] == "Outstation") {
                                                                                ?>
                                                                                <form id="formTravelDetail" name="formTravelDetail" method="POST" action="../../payment/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>-to-<?php echo trim(str_ireplace(",", "-to-", str_ireplace(" ", "-", strtolower($_POST["hdDestinationName"])))); ?>/">
                                                                                    <?php
                                                                                } else if ($_POST["hdTourtype"] == "Local") {
                                                                                    ?>
                                                                                    <form id="formTravelDetail" name="formTravelDetail" method="POST" action="../../payment/<?php echo trim(str_ireplace(" ", "-", strtolower($_POST["hdOriginName"]))); ?>/">
                                                                                        <?php
                                                                                    }
                                                                                    ?>
																					<input type="hidden" name="CGSTPercent" id="CGSTPercent" value="<?php echo $_POST['CGSTPercent'];?>" />
							<input type="hidden" name="SGSTPercent" id="SGSTPercent" value="<?php echo $_POST['SGSTPercent']?>" />
							<input type="hidden" name="IGSTPercent" id="IGSTPercent" value="<?php echo $_POST['IGSTPercent'];?>" />
							<input type="hidden" name="GSTEnabledYN" id="GSTEnabledYN" value="<?php echo $_POST['GSTEnabledYN']; ?>"/>
							<input type="hidden" name="ClientGSTId" id="ClientGSTId" value="<?php echo $_POST['ClientGSTId']; ?>"/>
                                                                                    <input type="hidden" id="browser" name="browser" value=""/>
                                                                                    <input type="hidden" id="ipaddress" name="ipaddress" value=""/>
                                                                                    <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $_POST["hdCarModel"]; ?>" />
                                                                                    <input type="hidden" name="hdCarModelID" id="hdCarModelID<?php echo $i; ?>" value="<?php echo $_POST["hdCarModelID"]; ?>" />
                                                                                    <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $_POST["hdCat"]; ?>" />
                                                                                    <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo trim($_POST["hdOriginName"]); ?>" />
                                                                                    <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                                                    <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo trim($_POST["hdDestinationName"]); ?>"/>
                                                                                    <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                                                    <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $_POST["hdPkgID"]; ?>">
                                                                                    <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $_POST["hdCarCatID"]; ?>" />
                                                                                    <input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo $_POST["dayRate"]; ?>" />
                                                                                    <input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo $_POST["kmRate"]; ?>" />
                                                                                    <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["duration"]; ?>" />
                                                                                    <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $_POST["totFare"]; ?>" />

                                                                                    <input type="hidden" name="totFareGross" id="totFareGross<?php echo $i; ?>" value="<?php echo $_POST["totFare"]; ?>" />

                                                                                    <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="3" />
                                                                                    <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $_POST["hdPickdate"] ?>" />
                                                                                    <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $_POST["hdDropdate"] ?>" />
                                                                                    <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                                                                                    <input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo $_POST["hdDistance"] ?>" />
                                                                                    <input type="hidden" name="monumber" id="mobile<?php echo $i; ?>" value="<?php echo $phone1; ?>" />
                                                                                    <input type="hidden" name="name" id="name<?php echo $i; ?>" value="<?php echo $gname; ?>" />
                                                                                    <input type="hidden" name="email" id="email<?php echo $i; ?>" value="<?php echo $emailid; ?>" />
                                                                                    <input type="hidden" name="cciid" id="cciid<?php echo $i; ?>" value="<?php echo $cciid; ?>" />
                                                                                    <input type="hidden" name="bookDays" id="bookDays" value="<?php echo intval($bookdays); ?>" />
                                                                                    <input type="hidden" name="bookTime" id="bookTime" value="<?php echo date('h:i A'); ?>" />
                                                                                    <input type="hidden" name="userTime" id="userTime" value="" />
                                                                                    <input type="hidden" name="PkgHrs" id="PkgHrs<?php echo $i; ?>" value="<?php echo $_POST["PkgHrs"]; ?>" />
                                                                                    <input type="hidden" name="tHour" id="tHour<?php echo $i; ?>" value="<?php
                                                                                    if (isset($_POST["tHour"])) {
                                                                                        echo $_POST["tHour"];
                                                                                    }
                                                                                    ?>" />
                                                                                    <input type="hidden" name="tMin" id="tMin<?php echo $i; ?>" value="<?php
                                                                                    if (isset($_POST["tMin"])) {
                                                                                        echo $_POST["tMin"];
                                                                                    }
                                                                                    ?>" />
                                                                                    <input type="hidden" name="discount" id="discount<?php echo $i; ?>" value="0" />
                                                                                    <input type="hidden" name="discountAmt" id="discountAmt<?php echo $i; ?>" value="0" />
                                                                                    <input type="hidden" name="addServ" id="addServ<?php echo $i; ?>" value="" />
                                                                                    <input type="hidden" name="addServAmt" id="addServAmt<?php echo $i; ?>" value="0" />
                                                                                    <input type="hidden" name="ChauffeurCharges" id="ChauffeurCharges<?php echo $i; ?>" value="<?php echo $_POST["ChauffeurCharges"]; ?>" />
                                                                                    <input type="hidden" name="totFarePB" id="totFarePB" value="<?php echo $_POST["totFare"]; ?>" />
                                                                                    <input type="hidden" name="totAmount" id="totAmount<?php echo $i; ?>" value="<?php echo $_POST["totFare"]; ?>" />
                                                                                    <input type="hidden" name="vat" id="vat<?php echo $i; ?>" value="<?php echo $_POST["vat"]; ?>" />
                                                                                    <input type="hidden" name="NightStayAllowance" id="NightStayAllowance<?php echo $i; ?>" value="<?php echo $_POST["NightStayAllowance"]; ?>"> 
                                                                                    <input type="hidden" name="nightduration" id="nightduration<?php echo $i; ?>" value="<?php echo $_POST["nightduration"]; ?>" />
                                                                                    <!-- by priyanka -->
                                                                                    <input type="hidden" name="intact_distcount" id="intact_distcount" />
                                                                                    <input type="hidden" name="intact_discountAmt" id="intact_discountAmt" />
                                                                                    <input type="hidden" name="intact_disA" id="intact_disA" />
                                                                                    <input type="hidden" name="intact_spPay" id="intact_spPay" />
                                                                                    <input type="hidden" name="intact_totFarePB" id="intact_totFarePB" />
                                                                                    <input type="hidden" name="intact_spDiscount" id="intact_spDiscount" />


                                                                                    <!-- by priyanka -->


                                                                                <?php } ?>
                                                                                <div class="toprowinner">
                                                                                    <div data-role="fieldcontain" style="float:left;width:100%">
                                                                                        <div class="d70">
                                                                                            <p>
                                                                                                <?php
                                                                                                if ($_POST["hdTourtype"] != "Local" && $_POST["hdTourtype"] != "Selfdrive") {
                                                                                                    echo trim($_POST["hdOriginName"]) . "-" . trim($_POST["hdDestinationName"]) . "-";
                                                                                                }
                                                                                                if ($_POST["hdTourtype"] == "Local")
                                                                                                    echo trim($_POST["hdOriginName"]);
                                                                                                else
                                                                                                    echo trim($_POST["hdOriginName"]);
                                                                                                ?>

                                                                                                <?php $disp_PickDate = date_create($_POST["hdPickdate"]); ?>
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="d30">

                                                                                            <?php
                                                                                            $cor = new COR();
                                                                                            if ($_POST["hdTourtype"] != "Local" && $_POST["hdTourtype"] != "Selfdrive") {
                                                                                               
                                                                                                ?>

                                                                                                <p id="spPay">Rs. <?php echo $cor->decrypt($_POST["totFare"]); ?>/-<br /></p>
                                                                                                <?php
                                                                                            }
                                                                                            if ($_POST["hdTourtype"] == "Local") {
                                                                                                ?>
                                                                                                <p id="spPay">Rs. <?php echo $cor->decrypt($_POST["totFare"]) ?>/-</p>
                                                                                                <?php
                                                                                            }
                                                                                            if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                                ?>
                                                                                                <p>Rs.<span id="spPay"> <?php echo intval($totalCost) + intval(ceil(($totalCost * $_POST["vat"]) / 100)); ?>/-</span></p>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div data-role="fieldcontain" style="float:left;width:100%">
                                                                                        <div class="d70 fwn">
                                                                                            <?php //echo $disp_PickDate->format('D d-M, Y');  ?>
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Selfdrive") {
                                                                                                if (intval($_POST["duration"]) > 1)
                                                                                                    echo $pickDate->format('d M') . " - " . $dropDate->format('d M, Y');
                                                                                                else
                                                                                                    echo $pickDate->format('d M, Y');
                                                                                                if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                                    echo " " . $_POST["tHourP"] . $_POST["tMinP"] . " Hrs";
                                                                                                }
                                                                                            } else {
                                                                                                echo $pickDate->format('d-M, Y');
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                        <div class="d30">
                                                                                            <a style="color:#fff;" class="faredetails" href="javascript:void(0)" onclick="javascript: _pass('fdpopupmobile');" id="link_">Fare details</a>
                                                                                        </div>
                                                                                    </div>

                                                                                    <!--PopUpForFareDetial-->
                                                                                    <div id="fdpopupmobile" style="display:none;" class="details_wrapper">
                                                                                        <div class="heading">
                                                                                            Fare Details
                                                                                            <span class="closedpop" onclick="javascript: _closeFD('fdpopupmobile');">X</span>
                                                                                        </div>
                                                                                        <?php
                                                                                        if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                            ?>
                                                                                            <ul style="margin-left:15px;">


                                                                                                <?php
                                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                                    if ($_POST["WeekDayDuration"] > 0 && $cor->decrypt($_POST["pkgR"]) > 0.00) {
                                                                                                        ?>
                                                                                                        <li>Weekdays:<?php echo $_POST["WeekDayDuration"]; ?>@<?php echo $cor->decrypt($_POST["pkgR"]); ?></li>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                                if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                                    if ($_POST["WeekDayDuration"] > 0 && $cor->decrypt($_POST["pkgR"]) > 0.00) {
                                                                                                        ?>
                                                                                                        <li>No. of hours:<?php echo $_POST["WeekDayDuration"]; ?><?php echo $cor->decrypt($_POST["pkgR"]); ?></li>
                                                                                                        <?php
                                                                                                    }
                                                                                                }


                                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                                    if ($_POST["WeekEndDuration"] > 0 && $_POST["PkgRateWeekEnd"] > 0) {
                                                                                                        ?>
                                                                                                        <li>Weekends:<?php echo $_POST["WeekEndDuration"]; ?>@<?php echo $cor->decrypt($_POST["PkgRateWeekEnd"]); ?></li>
                                                                                                        <?php
                                                                                                    }
                                                                                                }


                                                                                                if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                                    if ($_POST["WeekEndDuration"] > 0 && $_POST["PkgRateWeekEnd"] > 0) {
                                                                                                        ?>
                                                                                                        <li>No. of hours:<?php echo $_POST["WeekEndDuration"]; ?>@<?php echo $cor->decrypt($_POST["PkgRateWeekEnd"]); ?></li>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                <li>Minimum billing: Rs. <span id="bsamt"><?php echo intval($cor->decrypt($_POST["BasicAmt"])); ?></span>/-</li>
                                                                                                <li>Additional Services Cost: Rs. <span id="asc">0</span>/-</li>
                                                                                                <li>Sub Location Cost: Rs. <span id="slc"><?php echo intval($subLocCost); ?></span>/-</li>
                                                                                                <li>Convenience Charge: Rs. <span id="cnvc">0</span>/-</li>
                                                                                                <li>Discount Amount: Rs. <span id="disA">0</span>/-</li>
                                                                                                <li>Sub Total: Rs. <span id="subt"><?php echo intval($totalCost); ?></span>/-</li>
                                                                                                <input type="hidden" name="vattttttttttt" id="vattttttt" value="<?php echo $cor->decrypt($_REQUEST["vat"]); ?>" />
                                                                                                <li>VAT (@<?php echo number_format($cor->decrypt($_POST["vat"]), 2); ?>%): Rs. <span id="vatt"><?php echo intval(ceil(($totalCost * $_POST["vat"]) / 100)); ?></span>/-</li>
                                                                                                <li>Total fare: <span id="tfare">Rs. <?php echo intval($totalCost) + intval(ceil(($totalCost * $_POST["vat"]) / 100)); ?>/-</span></li>
                                                                                                <?php
                                                                                                if (intval($_POST["OriginalAmt"]) > intval($_POST["BasicAmt"])) {
                                                                                                    ?>
                                                                                                    <li>Discount Amount (On base fare): Rs. <?php echo (intval($_POST["OriginalAmt"]) - intval($_POST["BasicAmt"])); ?>/-</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                    ?>
                                                                                                    <li>Refundable Security Deposit  (Pre Auth from card): Rs. <?php echo $_POST["secDeposit"]; ?> (Mastercard/Visa/Amex)</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li>The billing cycle starts from 8am each day</li>
                                                                                                    <li>Refundable Security Deposit: Rs. <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                                                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                                                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>

                                                                                            </ul>
                                                                                            <?php
                                                                                            if (trim($_POST["chkPkgType"]) == "Hourly") {
                                                                                                ?>
                                                                                                <p style="font-weight:bold;margin-top:10px;float:left"><span>Includes</span></p>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>KMs Included: <?php echo $_POST["KMIncluded"]; ?></li>
                                                                                                    <li>Extra KM Charge: Rs. <?php echo $_POST["ExtraKMRate"]; ?>/- per KM</li>
                                                                                                </ul>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                            <p style="font-weight:bold;margin-top:10px;float:left"><span>Mandatory Documents(Original)</span></p>
                                                                                            <?php
                                                                                            if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>Passport / Voter ID card</li>
                                                                                                    <li>Driving license</li>
                                                                                                    <li>Credit card</li>
                                                                                                    <li>Aadhar card </li>

                                                                                                </ul>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>Passport / Voter ID card<</li>
                                                                                                    <li>Driving license</li>
                                                                                                    <li>Any of the following has to be submitted in original as an identity proof.
                                                                                                        <ol type="1">
                                                                                                            <li>Adhaar card</li>
                                                                                                            <li>Pan card</li>
                                                                                                            <li>Voter ID card</li>
                                                                                                        </ol>
                                                                                                    </li>
                                                                                                </ul>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>





                                                                                        <?php
                                                                                        if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Custom Package") {
                                                                                            ?>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <?php if (intval($cor->decrypt($_POST["dayRate"])) && intval($cor->decrypt($_POST["kmRate"]))) { ?>
                                                                                                    <?php
                                                                                                    $kmPerDay = intval($cor->decrypt($_POST["hdDistance"]) / $_POST["duration"]);
                                                                                                    if ($kmPerDay < 250) {
                                                                                                        $displayKMS = 250;
                                                                                                       
                                                                                                    } else {
                                                                                                        $displayKMS = intval($kmPerDay * $_POST["duration"]);
                                                                                                       
                                                                                                    }
                                                                                                    ?>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                            <p style="font-weight:bold;margin-top:10px;"><span>Includes </span></p>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <?php
                                                                                                if ($kmPerDay < 250) {
                                                                                                    ?>
                                                                                                    <li><?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> = <?php echo intval($displayKMS * $_POST["duration"]); ?> Kms</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li><?php echo $cor->decrypt($_POST["hdDistance"]); ?> Kms</li>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <li>Per Km charge = Rs..<?php echo intval($cor->decrypt($_POST["kmRate"])); ?>/-</li>
                                                                                                <li>No. of days = <?php echo $_POST["duration"]; ?> day(s)</li>
                                                                                                <li>Chauffeur charge = Rs..<?php echo number_format($cor->decrypt($_POST["ChauffeurCharges"])); ?> * <?php echo $_POST["duration"]; ?></li>
                                                                                                <li>Night Stay Allowance Charge Rs. <?php echo number_format($cor->decrypt($_POST['NightStayAllowance'])); ?>* <?php echo $_POST["nightduration"]; ?></li>
                                                                                                <li>Minimum billable kms per day = 250 kms</li>
                                                                                                <li>Discount Amount: Rs. <span id="disA">0</span>/-</li>
                                                                                                <li>GST</li>
                                                                                            </ul>
                                                                                            <p style="font-weight:bold;margin-top:10px;"><span>Extra Charges</span></p>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <li>Tolls, parking and state permits as per actuals</li>
                                                                                                <?php
                                                                                                if ($kmPerDay < 250) {
                                                                                                    ?>
                                                                                                    <li>Extra Km beyond <?php echo intval($displayKMS * $_POST["duration"]); ?> =  Rs..<?php echo intval($cor->decrypt($_POST["kmRate"])); ?>/km</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li>Extra Km beyond <?php echo $cor->decrypt($_POST["hdDistance"]); ?> kms =  Rs..<?php echo intval($cor->decrypt($_POST["kmRate"])); ?>/km</li>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            </ul>
                                                                                            </ul>
                                                                                            <?php
                                                                                        }
                                                                                        if ($_POST["hdTourtype"] == "Local") {
                                                                                            ?>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <?php if (intval($cor->decrypt($_POST["dayRate"])) && intval($cor->decrypt($_POST["kmRate"]))) { ?>
                                                                                                    <li>Rate: Rs. <?php echo intval($cor->decrypt($_POST["dayRate"]) / $_POST["PkgHrs"]); ?> / hour</li>
                                                                                                <?php } ?>
                                                                                                <li>10 km included for every hour of travel</li>
                                                                                                <li>Rs. <?php echo ($cor->decrypt($_POST["dayRate"]) / ($_POST["PkgHrs"] * 10)); ?> / km for additional km</li>
                                                                                                <li>Rs. <?php echo $cor->decrypt($_POST["totFare"]); ?> minimum billing</li>
                                                                                                <li>New hour billing starts when usage more than 30 mins</li>
                                                                                                <li>Toll and parking extra</li>
                                                                                                <li>Chauffeur charges and gst included</li>
                                                                                                <?php if ($_POST["hdOriginID"] == 2) { ?>
                                                                                                    <?php
                                                                                                    $charge = 200;
                                                                                                    if ($_POST["hdCat"] == 'Van' || $_POST["hdCat"] == '4 Wheel Drive' || $_POST["hdCat"] == 'Full Size') {
                                                                                                        $charge = 300;
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs. <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 7) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($cor->decrypt($_POST["dayRate"]) / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($cor->decrypt($_POST["dayRate"]) / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        <div class="tpdiv">
                                                                                            <?php if (intval($cor->decrypt($_POST["dayRate"])) && intval($cor->decrypt($_POST["kmRate"]))) { ?>
                                                                                                <p style="font-weight:bold;margin-top:10px;">Final fare shall depend on actual kms traveled. </p>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--PopUpForFareDetailEnds-->
                                                                                </div>
                                                                                <div class="toprowinner yl">Travel Details</div>
                                                                                <div data-role="fieldcontain" style="float:left;width:100%">
                                                                                    <div class="leftside">	
                                                                                        <div id="travel_details">
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Selfdrive" && intval($_POST["hdOriginID"]) != "69") {
                                                                                                ?>


                                                                                                <?php
                                                                                                /*                                                                                                 * *****added by vinod ********* */
                                                                                                if ($_REQUEST['chkPkgType'] == 'Hourly')
                                                                                                    $durationVal = 1;
                                                                                                else
                                                                                                    $durationVal = $_REQUEST['duration'];

                                                                                                $carCatID = array();
                                                                                                $corAS = new COR();
                                                                                                $pick = explode('/', $_REQUEST["hdPickdate"]);
                                                                                                $drop = explode('/', $_REQUEST["hdDropdate"]);
                                                                                                $arrSer = array();
                                                                                                $arrSer["CarModelId"] = $_REQUEST["hdCarCatID"];
                                                                                                $arrSer["ClientID"] = 2205;
                                                                                                $arrSer["PickUpDate"] = $pick["2"] . '-' . $pick["0"] . '-' . $pick["1"];
                                                                                                $arrSer["DropOffDate"] = $drop["2"] . '-' . $drop["0"] . '-' . $drop["1"];
                                                                                                $arrSer["PickUpTime"] = $_REQUEST["tHourP"] . $_REQUEST["tMinP"];
                                                                                                $arrSer["DropOffTime"] = $_REQUEST["tHourD"] . $_REQUEST["tMinD"];
                                                                                                $arrSer["PickUpCityID"] = $_REQUEST["hdOriginID"];
                                                                                                ;
                                                                                                $resAS = $corAS->_CORSelfDriveAdditionalServiceDetails($arrSer);
                                                                                                $deses = $myXML->xml2ary($resAS->{'GetAdditionalService_NewResult'}->{'any'});
                                                                                                ?>
                                                                                                <div class="heading" style="padding-left:2%;width:98%;font-size:13px;padding-top: 1%">Additional Services</div>
                                                                                                <div class="middiv">
                                                                                                    <fieldset class="register" style="margin-bottom:10px !important;">
                                                                                                        <?php
                                                                                                        foreach ($deses as $des) {
                                                                                                            if ($des["Available"] > 0) {
                                                                                                                ?>
                                                                                                                <div class="fieldrow">
                                                                                                                    <div class="row_left plwl" style="text-align:left !important;width:38% !important"><input type="checkbox" id="<?php echo $des["ServiceID"]; ?>" name="chkAS" onclick="javascript: _manageAS(this.id, <?php echo $i; ?>, <?php echo intval($des["Amount"]); ?>);" value="<?php
                                                                                                                        if ($des["ServiceID"] == 3) {
                                                                                                                            echo $des["ServiceID"] . "|" . intval($des["Amount"]);
                                                                                                                        } else {
                                                                                                                            echo $des["ServiceID"] . "|" . intval($durationVal * $des["Amount"]);
                                                                                                                        }
                                                                                                                        ?>" /><?php echo $des["Description"]; ?></div>
                                                                                                                    <div class="row_right" style="width:60% !important;padding-top:5px;"><?php
                                                                                                                        if ($des["ispercent"] == "true") {
                                                                                                                            ?>

                                                                                                                            <?php echo intval($des["percentvalues"]); ?>%
                                                                                                                            <?php
                                                                                                                        } else {
                                                                                                                            ?>
                                                                                                                            <?php echo intval($des["Amount"]); ?>/-
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        &nbsp;<?php if ($des["Charges"] == "Daily") { ?>per day <?php } ?></div>
                                                                                                                </div>
                                                                                                                <?php
                                                                                                            }
                                                                                                        }
                                                                                                        unset($corAS);
                                                                                                        ?>
                                                                                                        <div class="fieldrow" id='addressData' style="display:none;" >

                                                                                                            <div class=""  style="text-align:left !important;padding-bottom:15px;margin-left:28px;">
                                                                                                                Special Offer – Home Delivery charge of Rs. 900 now slashed to Rs. 450 (limited time offer)<br>
                                                                                                                *Please ensure that the pick-up location is within 10 km of the drop location else the booking will be cancelled.

                                                                                                            </div>

                                                                                                            <div style="clear:both;"> </div>


                                                                                                            <div class="row_left plwl" style="text-align:left !important;width:38% !important;font-weight:bold;padding-top:12px;">
                                                                                                                Pick Up / Drop Off Address
                                                                                                            </div>
                                                                                                            <div class="row_right" style="width:60% !important;padding-top:5px;">
                                                                                                                <input placeholder="" type="text" id='pickdrop'  name="pickdrop" value="" >
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </fieldset>
                                                                                                </div>


                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Selfdrive") {

                                                                                                $slNames = array();
                                                                                                $cor = new COR();
                                                                                                $myXML = new CORXMLList();
                                                                                                if ($_POST["allSubLoc"] != "") {
                                                                                                    $allSubLoc = array();
                                                                                                    $allSubLoc = explode(":", $_POST["allSubLoc"]);
                                                                                                    $res = $cor->_CORSelfDriveGetSubLocationsName(array("CityID" => $_POST["hdOriginID"]));
                                                                                                    $arr = $myXML->xml2ary($res);
                                                                                                    if (count($arr) > 0) {
                                                                                                        $isSLName = array();
                                                                                                        foreach ($arr as $k => $v)
                                                                                                            $isSLName[$k] = $v['SubLocationName'];
                                                                                                        array_multisort($isSLName, SORT_ASC, $arr);
                                                                                                        $iC = 0;
                                                                                                        for ($s = 0; $s < count($arr); $s++) {
                                                                                                            $isFound = false;
                                                                                                            for ($as = 0; $as < count($allSubLoc); $as++) {
                                                                                                                if ($arr[$s]["SubLocationID"] == $allSubLoc[$as]) {
                                                                                                                    $slNames[$iC]["SubLocationID"] = $arr[$s]["SubLocationID"];
                                                                                                                    $slNames[$iC]["SubLocationName"] = $arr[$s]["SubLocationName"];
                                                                                                                    $slNames[$iC]["SubLocationAddress"] = $arr[$s]["SubLocationAddress"];
                                                                                                                    ?>
                                                                                                                    <script type="text/javascript">
                                                                                                                        arrAddrs.push('<?php echo str_ireplace("'", "&rsquo;", $arr[$s]["SubLocationAddress"]); ?>');
                                                                                                                    </script>
                                                                                                                    <?php
                                                                                                                    $isFound = true;
                                                                                                                    $iC++;
                                                                                                                    break;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                    $arrToPost = array("CityId" => $_POST["hdOriginID"], "fromDate" => $_POST["hdPickdate"], "DateIn" => $_POST["hdDropdate"], "PickupTime" => $_POST["tHourP"] . $_POST["tMinP"], "DropOffTime" => $_POST["tHourD"] . $_POST["tMinD"], "SubLocation" => "0", "Duration" => $_POST["duration"], "PkgType" => $_POST["chkPkgType"], "CustomPkgYN" => $_POST["customPkg"], "ModelID" => $_POST["hdCarModelID"]);
                                                                                                    $res = $cor->_CORSelfDriveGetSubLocationModelWise($arrToPost);
                                                                                                    $arr = $myXML->xml2ary($res);
                                                                                                    $isSLName = array();
                                                                                                    foreach ($arr as $k => $v)
                                                                                                        $isSLName[$k] = $v['sublocationname'];
                                                                                                    array_multisort($isSLName, SORT_ASC, $arr);
                                                                                                    $iC = 0;
                                                                                                    for ($s = 0; $s < count($arr); $s++) {
                                                                                                        $slNames[$iC]["SubLocationID"] = $arr[$s]["sublocationid"];
                                                                                                        $slNames[$iC]["SubLocationName"] = $arr[$s]["sublocationname"];
                                                                                                        $slNames[$iC]["SubLocationAddress"] = $arr[$s]["sublocationaddress"];
                                                                                                        ?>
                                                                                                        <script type="text/javascript">
                                                                                                            arrAddrs.push('<?php echo str_ireplace("'", "&rsquo;", $arr[$s]["SubLocationAddress"]); ?>');
                                                                                                        </script>
                                                                                                        <?php
                                                                                                        $iC++;
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                <div class="toprowinner yl">Pickup Location</div>
                                                                                                <div class="middiv">
                                                                                                    <fieldset class="register">
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Choose one</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="hidden" name="ival" id="ival" value="<?php echo $i; ?>"/>
                                                                                                                <input type="hidden" id="intact_fare" />
                                                                                                                <input type="hidden" id="intact_spPay" />
                                                                                                                <input type="hidden" id="intact_totFare" />
                                                                                                                <input type="hidden" id="intact_totFarePB" />
                                                                                                                <input type="hidden" id="intact_discountAmt" />
                                                                                                                <input type="hidden" id="intact_disA" />
                                                                                                                <input type="hidden" id="intact_vatt" />
                                                                                                                <input type="hidden" id="intact_vatAmt" />
                                                                                                                <input type="hidden" id="intact_subt" />
                                                                                                                <input type="hidden" id="intact_subtAmount" />
                                                                                                                <select name="ddlSubLoc" id="ddlSubLoc" class="styled" onchange="javascript:_getSubLocCost(<?php echo $i; ?>);
                                                                                                                                if (this.selectedIndex > 0) {
                                                                                                                                    document.getElementById('subLocAdd').innerHTML = arrAddrs[this.selectedIndex - 1];
                                                                                                                                } else {
                                                                                                                                    document.getElementById('subLocAdd').innerHTML = ''
                                                                                                                                }">
                                                                                                                            <?php
                                                                                                                            if (count($slNames) > 1) {
                                                                                                                                ?>						  
                                                                                                                        <option value="0">Select Pickup Location</option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                    for ($s = 0; $s < count($slNames); $s++) {
                                                                                                                        if ($_POST["subLoc"] == $slNames[$s]["SubLocationID"]) {
                                                                                                                            ?>
                                                                                                                            <option value="<?php echo $slNames[$s]["SubLocationID"]; ?>" selected="selected"><?php echo $slNames[$s]["SubLocationName"]; ?></option>
                                                                                                                            <script type="text/javascript">
                                                                                                                                document.getElementById('subLocName<?php echo $i; ?>').value = '<?php echo $slNames[$s]["SubLocationName"]; ?>';
                                                                                                                            </script>
                                                                                                                            <?php
                                                                                                                        } else {
                                                                                                                            ?>
                                                                                                                            <option value="<?php echo $slNames[$s]["SubLocationID"]; ?>"><?php echo $slNames[$s]["SubLocationName"]; ?></option>
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                                <br />
                                                                                                                <span id="subLocAdd" style="float:left;margin-top: 10px;"></span>
                                                                                                                <div style="clear:both"></div>
                                                                                                                <div id="spanAirportCharge" style="float:left;margin-top:10px;visibility:hidden;"><input type="checkbox" name="chkAirportCharge" id ="chkAirportCharge" /><span for="chkAirportCharge">I want cab at the Airport</span></div>
                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <?php
                                                                                                        $db = new MySqlConnection(CONNSTRING);
                                                                                                        $db->open();
                                                                                                        $Sqltrip = "SELECT id,trip_type,status FROM trip_type_list where status='1'";
                                                                                                        $resultTrip = $db->query("query", $Sqltrip);
                                                                                                        ?>

                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Type of Trip</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <select class="styled" name="triptype" id='triptype'>
                                                                                                                    <option value="">Select Trip Type</option>
                                                                                                                    <?php
                                                                                                                    foreach ($resultTrip as $valData) {
                                                                                                                        ?>
                                                                                                                        <option value="<?php echo $valData['trip_type']; ?>"><?php echo $valData['trip_type']; ?></option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </fieldset>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php
                                                                                            if (count($slNames) == 1) {
                                                                                                ?>
                                                                                                <script type="text/javascript">
                                                                                                    _getSubLocCost(<?php echo $i; ?>);
                                                                                                    document.getElementById('subLocAdd').innerHTML = '<?php echo $slNames[0]["SubLocationAddress"]; ?>';
                                                                                                </script>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                        }
                                                                                        if ($_POST["hdTourtype"] != "Selfdrive") {
                                                                                            ?>
                                                                                            <div class="heading">Pickup Details</div>
                                                                                            <div class="middiv">
                                                                                                <fieldset class="register">

                                                                                                    <div class="fieldrow">
                                                                                                        <div class="row_left">
                                                                                                            <label>Pickup Time</label>
                                                                                                        </div>
                                                                                                        <div class="row_right">
                                                                                                            <div class="w_61">
                                                                                                                <div class="selecttime">
                                                                                                                    <select class="time" name="tHour" id="tHourSL" onchange="javascript: _setTime('tHourSL', 'tMinSL', 'selT', 'seltime', 'userTime');">
                                                                                                                        <option value="tH">Hours</option> 
                                                                                                                        <?php
                                                                                                                        for ($hr = 0; $hr <= 23; $hr++) {
                                                                                                                            if ($hr < 10) {
                                                                                                                                $t = "0" . $hr;
                                                                                                                            } else {
                                                                                                                                $t = $hr;
                                                                                                                            }
                                                                                                                            $sel = "";
                                                                                                                            if (isset($_POST["tHour"])) {
                                                                                                                                if (intval($_POST["tHour"]) == $hr)
                                                                                                                                    $sel = " selected=\"selected\"";
                                                                                                                            }
                                                                                                                            if (isset($_POST["tHourP"])) {
                                                                                                                                if (intval($_POST["tHourP"]) == $hr)
                                                                                                                                    $sel = " selected=\"selected\"";
                                                                                                                            }
                                                                                                                            ?><option value="<?php echo $t; ?>"<?php echo $sel; ?>><?php echo $t; ?></option><?php } ?>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                                <div class="selecttime">
                                                                                                                    <select class="time" name="tMin" id="tMinSL" onchange="javascript: _setTime('tHourSL', 'tMinSL', 'selT', 'seltime', 'userTime');">
                                                                                                                        <option>Min</option>
                                                                                                                        <?php
                                                                                                                        $sel = "";
                                                                                                                        if (isset($_POST["tMin"])) {
                                                                                                                            if (intval($_POST["tMin"]) == 0) {
                                                                                                                                ?>
                                                                                                                                <option value="00" selected = "selected">00</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="00">00</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMin"]) == 15) {
                                                                                                                                ?>
                                                                                                                                <option value="15" selected = "selected">15</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="15">15</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMin"]) == 30) {
                                                                                                                                ?>
                                                                                                                                <option value="30" selected = "selected">30</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="30">30</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMin"]) == 45) {
                                                                                                                                ?>
                                                                                                                                <option value="45" selected = "selected">45</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="45">45</option>
                                                                                                                                <?php
                                                                                                                            }
                                                                                                                        }
                                                                                                                        if (isset($_POST["tMinP"])) {
                                                                                                                            if (intval($_POST["tMinP"]) == 0) {
                                                                                                                                ?>
                                                                                                                                <option value="00" selected = "selected">00</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="00">00</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMinP"]) == 15) {
                                                                                                                                ?>
                                                                                                                                <option value="15" selected = "selected">15</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="15">15</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMinP"]) == 30) {
                                                                                                                                ?>
                                                                                                                                <option value="30" selected = "selected">30</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="30">30</option>
                                                                                                                                <?php
                                                                                                                            }

                                                                                                                            if (intval($_POST["tMinP"]) == 45) {
                                                                                                                                ?>
                                                                                                                                <option value="45" selected = "selected">45</option>
                                                                                                                                <?php
                                                                                                                            } else {
                                                                                                                                ?>
                                                                                                                                <option value="45">45</option>
                                                                                                                                <?php
                                                                                                                            }
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                    </select> 
                                                                                                                </div>  
                                                                                                                <div id="selT" class="showtime"><span id="seltime"></span></div>
                                                                                                                <?php
                                                                                                                if (isset($_POST["tHour"]) && isset($_POST["tMin"])) {
                                                                                                                    ?>
                                                                                                                    <script type="text/javascript">
                                                                                                                        _setTime('tHourSL', 'tMinSL', 'selT', 'seltime', 'userTime');
                                                                                                                    </script>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                if (isset($_POST["tHourP"]) && isset($_POST["tMinP"])) {
                                                                                                                    ?>
                                                                                                                    <script type="text/javascript">
                                                                                                                        _setTime('tHourSL', 'tMinSL', 'selT', 'seltime', 'userTime');
                                                                                                                    </script>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="fieldrow">
                                                                                                        <div class="row_left">
                                                                                                            <label>Full Address</label>
                                                                                                        </div>
                                                                                                        <div class="row_right">
                                                                                                            <textarea name="address" id="address"></textarea>
                                                                                                            <br />
                                                                                                            <?php
                                                                                                            if ($_POST["hdTourtype"] == "Selfdrive" && $_POST["hdOriginID"] == '69') {
                                                                                                                ?>
                                                                                                                <a class="atcpopup" href="javascript:void(0);" style="clear:both;margin-bottom:10px;margin:0 auto;display:block;color:#666666" onclick="javascript:_pass('goaTC');">Additional Amount</a>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                            <small>Flight details required incase of Airport pickup</small>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!--///////////////////////////////////////////////////////////////////////////////////////////////////////////////-->


                                                                                                    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?sensor=true&client=gme-carzonrentindiapvt&v=3.20&libraries=places'></script>
                                                                                                    <script type='text/javascript' src='<?php echo corWebRoot; ?>/js/GoogleMapAPIWrapper.js'></script>
                                                                                                    <!--<script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>-->
                                                                                                    <script src="<?php echo corWebRoot; ?>/js/bootstrap.js" type="text/javascript"></script>
                                                                                                    <link href="<?php echo corWebRoot; ?>/css/bootstrap.min.css" rel="Stylesheet" type="text/css" />


                                                                                                    <div class="fieldrow">
                                                                                                        <div class="row_left">
                                                                                                            <label>Pickup Location</label>
                                                                                                        </div>
                                                                                                        <div class="row_right">

                                                                                                            <input class="input-large" type="text" id="city_location_text" name="city_location_text" placeholder = "Type Area...." />

                                                                                                            <input type='hidden' id="city_location_lat"  name="city_location_lat"/>
                                                                                                            <input type='hidden' id="city_location_long"  name="city_location_long"/>


                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <script type='text/javascript'>
                                                                                                                    var getUrlParameter = function getUrlParameter(sParam) {
                                                                                                                        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                                                                                                                                sURLVariables = sPageURL.split('&'),
                                                                                                                                sParameterName,
                                                                                                                                i;

                                                                                                                        for (i = 0; i < sURLVariables.length; i++) {
                                                                                                                            sParameterName = sURLVariables[i].split('=');

                                                                                                                            if (sParameterName[0] === sParam) {
                                                                                                                                return sParameterName[1] === undefined ? true : sParameterName[1];
                                                                                                                            }
                                                                                                                        }
                                                                                                                    };


                                                                                                                    var office_location_prediction_autocomplete = null;
                                                                                                                    function officeLocationAutoCompleteHandler() {
                                                                                                                        if (office_location_prediction_autocomplete != null) {
                                                                                                                            var place = office_location_prediction_autocomplete.getPlace();
                                                                                                                            // place.geometry.location.lat(), place.geometry.location.lng();
                                                                                                                            // map.setCenter(place.geometry.location);
                                                                                                                            // map.setZoom(12);
                                                                                                                            $("#city_location_lat").val(place.geometry.location.lat());
                                                                                                                            $("#city_location_long").val(place.geometry.location.lng());


                                                                                                                        }
                                                                                                                    }

                                                                                                                    function initializePlaceAutoComplete(element_id, callback_function, element_obj) {
                                                                                                                        var input = document.getElementById(element_id);
                                                                                                                        if (element_obj != null) {
                                                                                                                            input = element_obj;
                                                                                                                        }
                                                                                                                        if (input != null) {
                                                                                                                            var options = {};
                                                                                                                        }
                                                                                                                        options['componentRestrictions'] = {country: 'in'};
                                                                                                                        if (google == null || google.maps == null) {
                                                                                                                            loading();
                                                                                                                            alert("OOPS. There was a network error in loading this page. Please reload");
                                                                                                                            return;
                                                                                                                        }
                                                                                                                        office_location_prediction_autocomplete = new google.maps.places.Autocomplete(input, options);
                                                                                                                        google.maps.event.addListener(office_location_prediction_autocomplete, "place_changed", callback_function, false);

                                                                                                                    }

                                                                                                                    $(function () {
                                                                                                                        initializePlaceAutoComplete("city_location_text", officeLocationAutoCompleteHandler);
                                                                                                                        // getCities();
                                                                                                                    });

                                                                                                                    function parseJSON(obj) {
                                                                                                                        if (obj.constructor === "test".constructor) {
                                                                                                                            return JSON.parse(obj);
                                                                                                                        }
                                                                                                                        return obj;
                                                                                                                    }

                                                                                                                    function completeDriverOnboarding() {
                                                                                                                        var application_name = "CorMeterGoogleAPI";
                                                                                                                        if (window.location.hostname.indexOf("localhost") != -1) {
                                                                                                                            application_name = "RentMeterAPI";
                                                                                                                        }
                                                                                                                        if ($("#vehicle_reg_no").val() == null || $("#vehicle_reg_no").val() == "" || $("#vehicle_reg_no").val().length < 6) {
                                                                                                                            alert("Invalid registration number");
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                        if ($("#city_location_text").val() == null || $("#city_location_text").val() == "") {
                                                                                                                            alert("Invalid garage location");
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                        if ($("#city_location_lat").val() == null || $("#city_location_lat").val() == "") {
                                                                                                                            alert("Location Suggestions not loaded properly. Please refresh the page.");
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                        var jsonObj = {"is_vendor_car": $("#vendor_vehicle").is(":checked"),
                                                                                                                            "vehicle_reg_no": $("#vehicle_reg_no").val(),
                                                                                                                            "city_location_text": $("#city_location_text").val(),
                                                                                                                            "city_location_lat": $("#city_location_lat").val(),
                                                                                                                            "city_location_long": $("#city_location_long").val()
                                                                                                                        };
                                                                                                                        var jsonStr = JSON.stringify(jsonObj);
                                                                                                                        //            $.post("/" + application_name + "/MapWebMethods.asmx/SaveDriverDetails", jsonStr, function (data) {
                                                                                                                        //                var responseJSON = parseJSON(data);
                                                                                                                        //                if (responseJSON.status == "true") {
                                                                                                                        //                    alert("updated successsfully");
                                                                                                                        //                    window.location.reload();
                                                                                                                        //                } else {
                                                                                                                        //                    alert("error while updating");
                                                                                                                        //                }
                                                                                                                        //            });
                                                                                                                    }

                                                                                                                    $("#cor_drive_onboard_submit").click(function () {
                                                                                                                        completeDriverOnboarding();
                                                                                                                    });

                                                                                                                    $("#city_dropdown").change(function () {
                                                                                                                        var id = $(this).val();
                                                                                                                        if (id != "-1") {
                                                                                                                            $("#vehicle_details_container").show();
                                                                                                                        } else {
                                                                                                                            $("#vehicle_details_container").hide();
                                                                                                                        }

                                                                                                                    });

                                                                                                    </script>
                                                                                                    <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <div class="toprowinner yl">Contact Details</div>
                                                                                                <div class="middiv">
                                                                                                    <fieldset class="register">
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Mobile</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">


                                                                                                                <?php
                                                                                                                if ($_POST["hdTourtype"] != "Selfdrive") {
                                                                                                                    ?>
                                                                                                                    <input type="text" name="monumber" id="monumberX" value="<?php echo $phone1; ?>" value="10 digit mobile number" 
                                                                                                                           onblur="if (value == '')
                                                                                                                                           value = '10 digit mobile number';
                                                                                                                                       datapopulatelocaloutstation('<?PHP echo $_POST["hdTourtype"]; ?>')" 
                                                                                                                           maxlength="10" 
                                                                                                                           onkeypress="javascript: return _allowNumeric(event);" 
                                                                                                                           onfocus="if (value == '10 digit mobile number')
                                                                                                                                           value = ''" />
                                                                                                                           <?php
                                                                                                                       } else {
                                                                                                                           ?>
                                                                                                                    <input type="text" name="monumber" id="monumberX" value="<?php echo $phone1; ?>" value="10 digit mobile number" 
                                                                                                                           onblur="if (value == '')
                                                                                                                                           value = '10 digit mobile number';
                                                                                                                                       datapopulate('<?PHP echo $_POST["hdTourtype"]; ?>')" 
                                                                                                                           maxlength="10" 
                                                                                                                           onkeypress="javascript: return _allowNumeric(event);" 
                                                                                                                           onfocus="if (value == '10 digit mobile number')
                                                                                                                                           value = ''" />
                                                                                                                           <?php
                                                                                                                       }
                                                                                                                       ?>


                                                                                                                <span class="pls_wait" id="populate">Please Wait..</span>
                                                                                                                <br />
                                                                                                                <small>Your booking details will be Send to this number</small>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Name</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="text" name="name" id="txtname" value="<?php echo $gname; ?>" onblur="if (value == '')
                                                                                                                                value = 'Name'"  onfocus="if (value == 'Name')
                                                                                                                                            value = ''" onkeypress="javascript: return _allowAlpha(event)"; />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Email</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
    <?php
    if ($_POST["hdTourtype"] != "Selfdrive") {
        ?>
                                                                                                                    <input type="text" name="email" id="email" value="<?php echo $emailid; ?>" onblur="if (value == '')
                                                                                                                                    value = 'Email';
                                                                                                                                datapopulatelocaloutstation('<?PHP echo $_POST["hdTourtype"]; ?>')"  
                                                                                                                           onfocus="if (value == 'Email')
                                                                                                                                           value = '';" />
                                                                                                                           <?php
                                                                                                                       } else {
                                                                                                                           ?>
                                                                                                                    <input type="text" name="email" id="email" value="<?php echo $emailid; ?>" onblur="if (value == '')
                                                                                                                                    value = 'Email';
                                                                                                                                datapopulate('<?PHP echo $_POST["hdTourtype"]; ?>')"  
                                                                                                                           onfocus="if (value == 'Email')
                                                                                                                                           value = '';" />
                                                                                                                    <?php
                                                                                                                       }
                                                                                                                       ?>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                &nbsp;
                                                                                                            </div>
                                                                                                           <!-- <div class="row_right">
                                                                                                                <input type="checkbox" name="paybackearn" id="paybackearn" value="1" style="float:left;" />&nbsp;<span style="float:left;">I want to earn PAYBACK points.</span>&nbsp;<a class="qm" href="javascript: void(0);" onclick="javascript: alert('PAYBACK member?\n\n- Checkmark the box to earn 8 PAYBACK points for every Rs.100 spent.\n\n- The points shall be credited within 7 days if your mobile number is linked to your PAYBACK account.\n\n- Call PAYBACK at 1860-258-5000 to get your mobile number linked/checked or to enroll for a free membership.')">?</a>
                                                                                                            </div>-->
                                                                                                        </div>
                                                                                                    </fieldset>
                                                                                                </div>
                                                                                                <!--------------trip advisor----------------->
    <?php if ($_POST["hdTourtype"] == "Outstation") { ?>
                                                                                                    <div class="middiv">
                                                                                                        <fieldset class="register">
                                                                                                            <div class="fieldrow">                                                                                                            
                                                                                                                <div class="row_left"></div>
                                                                                                                <div class="row_right">
                                                                                                                    <!------trip Advisor hidden field------------>
                                                                                                                    <div class="f_l"><input type="checkbox" name="trip_advisor" id="trip_advisor"  onclick="tour_advisor();"></div>
                                                                                                                    <div class="f_l pt3">I want to receive the city guide for my destination powered by</div>
                                                                                                                    <img style="height:26px;" src="http://www.tripadvisor.com/img/cdsi/langs/en_IN/tripadvisor_logo_132x24-20823-1.gif">
                                                                                                                    <input type="hidden" name="tripadvisorname" id="tripadvisorid" value="false" />
                                                                                                                    <!------trip Advisor end------------>
                                                                                                                    <div class="conform_trip" id="tripadvisor_msg" style="color:#599544;display:none;"><small>A pdf link will soon be sent to your registered email address after confirmation of the booking.</small></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </fieldset>
                                                                                                    </div>
                                                                                                    <script>
                                                                                                        function tour_advisor()
                                                                                                        {
                                                                                                            if ($("#trip_advisor").is(":checked"))
                                                                                                            {
                                                                                                                $('#tripadvisorid').attr('value', 'true');
                                                                                                                $('#tripadvisor_msg').show();

                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                $('#tripadvisorid').attr('value', 'false');
                                                                                                                $('#tripadvisor_msg').hide();
                                                                                                            }
                                                                                                        }
                                                                                                    </script>				
        <?Php
    } else if ($_POST["hdTourtype"] == "Selfdrive") {
        $cor = new COR();
        $res = $cor->_CORGetCities();
        $myXML = new CORXMLList();
        $org = $myXML->xml2ary($res);
        $res = $cor->_CORGetDestinations();
        $des = $myXML->xml2ary($res);
        ?>
                                                                                                    <script>
                                                                                                        function tour_advisor()
                                                                                                        {
                                                                                                            if ($("#trip_advisor").is(":checked"))
                                                                                                            {
                                                                                                                $('#tripadvisorid').attr('value', 'true');
                                                                                                                $('#travelto').show();
                                                                                                                $('#txtDestination').focus().attr('Placeholder', 'Please Choose Your destination.');

                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                $('#tripadvisorid').attr('value', 'false');
                                                                                                                $('#travelto').hide();
                                                                                                                $('#tripadvisor_msg').hide();
                                                                                                            }
                                                                                                        }
                                                                                                    </script>		
                                                                                                    <script>
                                                                                                        arrDestination = new Array();
        <?php
        for ($i = 0; $i < count($des); $i++) {
            ?>
                                                                                                            arrDestination[<?php echo $i; ?>] = new Array("<?php echo $des[$i]['CityName'] ?>, <?php echo $des[$i]['state'] ?>", <?php echo $des[$i]['CityId'] ?>);
            <?php
        }
        ?>
                                                                                                    </script>
                                                                                                    <div class="middiv">
                                                                                                        <fieldset class="register">
                                                                                                            <div class="fieldrow">
                                                                                                                <div class="row_left"> </div>
                                                                                                                <div class="row_right">
                                                                                                                    <!------trip Advisor hidden field------------>
                                                                                                                    <div class="f_l"><input type="checkbox" name="trip_advisor" id="trip_advisor"  onclick="tour_advisor();"></div>
                                                                                                                    <div class="recive_trip">
                                                                                                                        <div class="f_l pt3">I want to receive the city guide for my destination powered by</div>
                                                                                                                        <img style="height:26px;" src="http://www.tripadvisor.com/img/cdsi/langs/en_IN/tripadvisor_logo_132x24-20823-1.gif">
                                                                                                                    </div>
                                                                                                                    <input type="hidden" name="tripadvisorname" id="tripadvisorid" value="false" />
                                                                                                                    <input type="hidden" name="tripadvisordestinationid" id="tripadvisordestinationid" />
                                                                                                                    <!------trip  hidden Advisor end------------>
                                                                                                                    <div class="clr"> </div>
                                                                                                                    <div class="conform_trip" id="tripadvisor_msg" style="color:#599544;display:none;"><small>A pdf link will soon be sent to your registered email address after compilation of the booking.</small></div>

                                                                                                                </div>
                                                                                                                <div class="clr"> </div>
                                                                                                                <div class="relative" id="travelto" style="display:none;">
                                                                                                                    <div class="row_left">
                                                                                                                        <label>Travelling to</label>
                                                                                                                    </div>
                                                                                                                    <div class="row_right">
                                                                                                                        <input type="text" name="txtDestination" id="txtDestination" value="" autocomplete="off" onKeyup="javascript: _getCities(this.value, event, this.id, 'hdDestinationID', 'hdDestinationName', 'autosuggest', arrDestination, 0, 0, 0);" onfocus="javascript: window.scrollBy(100, 200);" />
                                                                                                                        <div class="autos"><div id="autosuggest" class="autosuggest floatingDiv"></div></div>
                                                                                                                    </div>
                                                                                                                </div>	
                                                                                                            </div>
                                                                                                        </fieldset>
                                                                                                    </div>
    <?php } ?>
                                                                                                <!--------------trip advisor end--------------------->
                                                                                                
                                                                                                <div class="toprowinner yl" style="display:none" id="pblc">PAYBACK Loyalty Discount</div>
                                                                                                <div class="middiv" style="display:none" id="pbpd">
                                                                                                    <fieldset class="register">							
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left" style="width:181px;position:relative;right:10px;">
                                                                                                                <label>Card No.</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="text" name="txtcardnopb" id="txtcardnopb" maxlength="16" value="" onkeypress="javascript: return _allowNumeric(event);" onblur="javascript: if (this.value != '') {
                                                                                                                                _redeemPoints();
                                                                                                                            }" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Points In Card</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="text" name="txtpointsincard" id="txtpointsincard" maxlength="16" value="" readonly="readonly"/>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Points to Redeem</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="text" name="txtpbpoints" id="txtpbpoints" maxlength="10" value="" onkeypress="javascript: return _allowNumeric(event);" onblur="javascript: document.getElementById('txtamounttoredeem').readOnly = false;
                                                                                                                            document.getElementById('txtamounttoredeem').value = parseInt(this.value / 4);
                                                                                                                            document.getElementById('txtamounttoredeem').readOnly = true;" />&nbsp;<!-- <a href="javascript: void(0);" onclick="javascript: _getPBRedeem();">Redeem</a> --><br />4 PAYBACK Points = 1 INR
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left">
                                                                                                                <label>Amount to Redeem</label>
                                                                                                            </div>
                                                                                                            <div class="row_right">
                                                                                                                <input type="text" name="txtamounttoredeem" id="txtamounttoredeem" maxlength="6" value="" readonly="readonly"/>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </fieldset>
                                                                                                </div>
                                                                                                <div class="middiv" style="padding:0px 0px 0px 29px !important;">
                                                                                                    <fieldset class="register">
                                                                                                        <div class="fieldrow">
                                                                                                            <div class="row_left"></div>
                                                                                                            <div class="row_right">																										
                                                                                                                <div class="confirmbooking" id="cnfbooking">
                                                                                                                    <div class="loaderbooking" style="display:none;"><img src="<?php echo corWebRoot; ?>/images/loading2.gif"></div>	
                                                                                                                    <a onclick="javascript: _makeBooking();" class="genbtnlink" href="javascript: void(0);">Confirm Booking</a></div>
                                                                                                                <div class="confirmbookingredeem" id="cnfbookingredeem" style="display:none;"><a onclick="javascript: _getPBRedeem('<?php echo $xi; ?>');" class="genbtnlink" href="javascript: void(0);">Redeem &amp; Book</a></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </fieldset>
                                                                                                </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="rightside" style="display:none">
                                                                                    <h1>Booking Summary</h1>
                                                                                    <div class="tpdiv">
                                                                                        <img src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ", "-", $_POST["hdCat"]); ?>.jpg" alt="<?php echo $_POST["hdCat"]; ?>" />
                                                                                        <div class="clr"></div>
                                                                                        <p><span>Car Type:</span> <?php echo $_POST["hdCat"]; ?> - <?php echo $_POST["hdCarModel"]; ?></p>
                                                                                        <p><span>From:</span> <?php echo $_POST["hdOriginName"] ?></p>
                                                                                        <p><span>Service:</span> <?php echo $_POST["hdTourtype"] ?> </p>
    <?php
    if ($_POST["hdTourtype"] != "Local") {
        ?>
                                                                                            <p><span>Destination:</span> <?php echo $_POST["hdDestinationName"] ?></p>
                                                                                            <?php
                                                                                        }
                                                                                        $pickDate = date_create($_POST["hdPickdate"]);
                                                                                        $dropDate = date_create($_POST["hdDropdate"]);
                                                                                        ?>
                                                                                        <p><span>Pickup Date:</span> <?php echo $pickDate->format('D d-M, Y'); ?></p>
                                                                                        <p><span>Drop Date:</span> <?php echo $dropDate->format('D d-M, Y'); ?></p>
                                                                                        <?php
                                                                                        if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                            ?>
                                                                                            <p><span>Pickup Time:</span> <?php echo $_POST["tHourP"]; ?><?php echo $_POST["tMinP"]; ?> Hrs</p>
                                                                                            <p><span>Drop Time:</span> <?php echo $_POST["tHourD"]; ?><?php echo $_POST["tMinD"]; ?> Hrs</p>
                                                                                            <p><span>Pickup Address:</span> <?php echo $_POST["subLocName"]; ?></p>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <p><span>Pickup Time:</span> Not Set</p>
                                                                                            <p><span>Pickup Address:</span> Not Set</p>
                                                                                            <?php
                                                                                        }
                                                                                        ?>

                                                                                        <h3>Round Trip Fare<br />
                                                                                            <span id="spPay">Rs. <?php echo intval($_POST["totFare"]); ?>/- <span style="font-size:11px;margin:10px 30px 0px 0px;display:block;width:auto;float:right;font-weight:normal;color:#666666;">(Including GST)</span></span></h3>
                                                                                    </div>
                                                                                    <div class="tpdiv">
                                                                                        <p><span>Fare Details</span></p>
    <?php
    if ($_POST["hdTourtype"] == "Selfdrive") {
        ?>
                                                                                            <ul style="margin-left:15px;">
                                                                                                <li>Minimum Billing: Rs. <span id="bsamt"><?php echo intval($_POST["BasicAmt"]); ?></span></li>
                                                                                                <li>Additional Services Cost: Rs. <span id="asc">0</span>/-</li>
                                                                                                <li>Sub Location Cost: Rs. <span id="slc"><?php echo intval($subLocCost); ?></span>/-</li>
                                                                                                <li>Sub Total: Rs. <span id="subt"><?php echo intval($totalCost); ?></span>/-</li>
                                                                                                <li>VAT (@<?php echo number_format($_POST["vat"], 2); ?>%): Rs. <span id="vatt"><?php echo intval(ceil(($totalCost * $_POST["vat"]) / 100)); ?></span>/-</li>
                                                                                                <li>Total Fare: Rs. <span id="tfare"><?php echo intval($totalCost) + intval(ceil(($totalCost * $_POST["vat"]) / 100)); ?></span>/-</li>
        <?php
        if (intval($_POST["OriginalAmt"]) > intval($_POST["BasicAmt"])) {
            ?>
                                                                                                    <li>Discount Amount (On base fare): Rs. <?php echo (intval($_POST["OriginalAmt"]) - intval($_POST["BasicAmt"])); ?>/-</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                    ?>
                                                                                                    <li>Refundable Security Deposit  (Pre Auth from card): Rs. <?php echo $_POST["secDeposit"]; ?> (Mastercard/Visa/Amex)</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li>The billing cycle starts from 8am each day</li>
                                                                                                    <li>Refundable Security Deposit: Rs. <?php echo $secDeposit; ?> (To be paid in cash before the start of the journey)</li>
                                                                                                    <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                                                                    <li>The vehicle is to be driven within the permissible limits of Goa.</li>
            <?php
        }
        ?>

                                                                                            </ul>
                                                                                                <?php
                                                                                                if (trim($_POST["chkPkgType"]) == "Hourly") {
                                                                                                    ?>
                                                                                                <p style="font-weight:bold;margin-top:10px;float:left"><span>Includes</span></p>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>KMs Included: <?php echo $_POST["KMIncluded"]; ?></li>
                                                                                                    <li>Extra KM Charge: Rs. <?php echo $_POST["ExtraKMRate"]; ?>/- per KM</li>
                                                                                                </ul>
            <?php
        }
        ?>
                                                                                            <p style="font-weight:bold;margin-top:10px;float:left"><span>Mandatory Documents(Original)</span></p>
                                                                                            <?php
                                                                                            if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>Passport / Voter ID Card</li>
                                                                                                    <li>Driving License</li>
                                                                                                    <li>Credit Card</li>
                                                                                                </ul>
            <?php
        } else {
            ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>Passport / Voter ID Card<</li>
                                                                                                    <li>Driving License</li>
                                                                                                    <li>Any of the following has to be submitted in original as an identity proof.
                                                                                                        <ol type="1">
                                                                                                            <li>Adhaar card</li>
                                                                                                            <li>Pan card</li>
                                                                                                            <li>Voter ID card</li>
                                                                                                        </ol>
                                                                                                    </li>
                                                                                                </ul>
            <?php
        }
    }
    ?>
                                                                                        <?php
                                                                                        if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Custom Package") {
                                                                                            ?>
                                                                                            <ul>
                                                                                            <?php if (intval($_POST["dayRate"]) && intval($_POST["kmRate"])) { ?>
                                                                                                <?php
                                                                                                $kmPerDay = intval(($_POST["hdDistance"]) / $_POST["duration"]);
                                                                                                if ($kmPerDay < 250) {
                                                                                                    $displayKMS = 250;
                                                                                                    ?>
                                                                                                                                                                                    <!--<li>Rs.. (<?php echo intval($_POST["kmRate"]); ?> * <?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> + <?php echo $_POST["ChauffeurCharges"]; ?> * <?php echo $_POST["duration"]; ?>) * 104.95% = Rs.. <?php echo $_POST["totFare"]; ?>/-</li>-->
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        $displayKMS = intval($kmPerDay * $_POST["duration"]);
                                                                                                        ?>
                                                                                                                                                                                    <!--<li>Rs.. (<?php echo intval($_POST["kmRate"]); ?> * <?php echo $displayKMS; ?>  + <?php echo $_POST["ChauffeurCharges"]; ?> * <?php echo $_POST["duration"]; ?>) * 104.95% = Rs.. <?php echo $_POST["totFare"]; ?>/-</li>-->
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                            <p><span>Includes </span></p>
                                                                                            <ul>
                                                                                                <?php
                                                                                                if ($kmPerDay < 250) {
                                                                                                    ?>
                                                                                                    <li><?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> = <?php echo intval($displayKMS * $_POST["duration"]); ?> Kms</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li><?php echo $_POST["hdDistance"]; ?> Kms</li>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <li>Per Km charge = Rs..<?php echo intval($_POST["kmRate"]); ?>/-</li>
                                                                                                <li>No. of days = <?php echo $_POST["duration"]; ?> day(s)</li>
                                                                                                <li>Chauffeur charge = Rs..<?php echo number_format($_POST["ChauffeurCharges"]); ?> * <?php echo $_POST["duration"]; ?></li>
                                                                                                <li>Minimum billable kms per day = 250 kms</li>
                                                                                                <li>GST</li>
                                                                                            </ul>
                                                                                            <p><span>Extra Charges</span></p>
                                                                                            <ul>
                                                                                                <li>Tolls, parking and state permits as per actuals</li>
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                                                                    <li>Extra Km beyond <?php echo intval($displayKMS * $_POST["duration"]); ?> =  Rs..<?php echo intval($_POST["kmRate"]); ?>/km</li>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                    <li>Extra Km beyond <?php echo $_POST["hdDistance"]; ?> kms =  Rs..<?php echo intval($_POST["kmRate"]); ?>/km</li>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            </ul>
                                                                                            </ul>
                                                                                                <?php
                                                                                            }
                                                                                            if ($_POST["hdTourtype"] == "Local") {
                                                                                                ?>
                                                                                            <ul>
                                                                                            <?php if (intval($_POST["dayRate"]) && intval($_POST["kmRate"])) { ?>
                                                                                                    <li>Rate: Rs. <?php echo intval($_POST["dayRate"] / $_POST["PkgHrs"]); ?> / hour</li>
                                                                                            <?php } ?>
                                                                                                <li>10 km included for every hour of travel</li>
                                                                                                <li>Rs. <?php echo ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10)); ?> / km for additional km</li>
                                                                                                <li>Rs. <?php echo $_POST["totFare"]; ?> minimum billing</li>
                                                                                                <li>New hour billing starts when usage more than 30 mins</li>
                                                                                                <li>Toll and parking extra</li>
                                                                                                <li>Discount Amount: Rs. <span id="disA"><?php echo $disA; ?></span>/-</li>
                                                                                                <li>Chauffeur charges and gst included</li>
        <?php if ($_POST["hdOriginID"] == 2) { ?>
            <?php
            $charge = 200;
            if ($_POST["hdCat"] == 'Van' || $_POST["hdCat"] == '4 Wheel Drive' || $_POST["hdCat"] == 'Full Size') {
                $charge = 300;
            }
            ?>
                                                                                                    <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs. <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 7) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3) {
                                                                                                    ?>
                                                                                                    <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                    </div>
                                                                                    <div class="tpdiv">
                                                                                        <?php if (intval($_POST["dayRate"]) && intval($_POST["kmRate"])) { ?>
                                                                                            <p>Final fare shall depend on actual kms traveled. </p>
    <?php } ?>
                                                                                        <div class="map">
                                                                                        <?php
                                                                                        if ($_POST["hdTourtype"] != "Local" && $_POST["hdTourtype"] != "Selfdrive") {
                                                                                            ?>
                                                                                                <iframe width="257" height="179" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=<?php echo urlencode($orgName); ?>&amp;daddr=<?php echo urlencode($dwstate); ?>&amp;hl=en&amp;aq=1&amp;oq=<?php echo urlencode($orgName); ?>&amp;sll=<?php echo $ll; ?>&amp;mra=ls&amp;ie=UTF8&amp;ll=<?php echo $ll; ?>&amp;t=m&amp;z=4&amp;iwloc=addr&amp;output=embed"></iframe>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                            </div>
<?php } ?>
<?php
if ($tab == 3) {
    $cor = new COR();
    if ($tCCIID != 0)
        $cciid = $tCCIID;
    if (isset($_COOKIE["tcciid"]))
        $cciid = $_COOKIE["tcciid"];
    if (isset($_COOKIE["cciid"]))
        $cciid = $_COOKIE["cciid"];
    $totPayable = $basicAmt = $addSrvAmt = $subLocCost = $vatAmt = $disA = 0;
    
   
    if ($_POST["hdTourtype"] != "Selfdrive")  {
       
        if ($_POST['empcode'] == 'PayBack') {
            $basicAmt = $_POST['BasicAmt'];
            $totalfare = $_POST['totFare'];
            $disAPayBack = $_POST["discountAmt"];
            $orignalBilling = $totalfare;
            $totPayable = $totalfare - $disAPayBack;
        } else {
            $basicAmt = $cor->decrypt($_POST['BasicAmt']);
            $totalfare = $cor->decrypt($_POST['totFare']);
            $totPayable = $cor->decrypt($_POST["totFare"]);
        }
    }
    ?>
                                                                            <div id="country3" class="tabcontent">		
                                                                            <?php
                                                                            $_SESSION["rurl"] = urlencode("http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);

                                                                            $mtime = round(microtime(true) * 1000);

                                                                            $_SESSION["pkgId"] = $_POST["hdPkgID"];
                                                                            $_SESSION["hdCat"] = $_POST["hdCat"];
                                                                            $_SESSION["hdCarModel"] = $_POST["hdCarModel"];
                                                                            $_SESSION["hdOriginName"] = $_POST["hdOriginName"];
                                                                            $_SESSION["hdTourtype"] = $_POST["hdTourtype"];
                                                                            $_SESSION["hdDestinationName"] = $_POST["hdDestinationName"];
                                                                            $_SESSION["hdPickdate"] = $_POST["hdPickdate"];
                                                                            $_SESSION["hdDropdate"] = $_POST["hdDropdate"];
                                                                            if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                if (isset($_POST["picktime"])) {
                                                                                    $_SESSION["picktime"] = $_POST["picktime"];
                                                                                } else {
                                                                                    $_SESSION["picktime"] = $_POST["tHourP"] . $_POST["tMinP"];
                                                                                }
                                                                                if (isset($_POST["droptime"])) {
                                                                                    $_SESSION["droptime"] = $_POST["droptime"];
                                                                                } else {
                                                                                    $_SESSION["droptime"] = $_POST["tHourD"] . $_POST["tMinD"];
                                                                                }
                                                                            } else {
                                                                                if (isset($_POST["picktime"])) {
                                                                                    $_SESSION["picktime"] = $_POST["picktime"];
                                                                                } else {
                                                                                    $_SESSION["picktime"] = $_POST["tHour"] . $_POST["tMin"];
                                                                                }
                                                                            }
                                                                            $_SESSION["address"] = $_POST["address"];
                                                                            $_SESSION["remarks"] = $_POST["remarks"];
                                                                            $_SESSION["totFare"] = $_POST["totFare"];
                                                                            $_SESSION["rdoPayment"] = $_POST["rdoPayment"];
                                                                            $_SESSION["dayRate"] = $_POST["dayRate"];
                                                                            $_SESSION["kmRate"] = $_POST["kmRate"];
                                                                            $_SESSION["duration"] = $_POST["duration"];
                                                                            $_SESSION["hdDistance"] = $_POST["hdDistance"];
                                                                            $_SESSION["name"] = $_POST["name"];
                                                                            $_SESSION["monumber"] = $_POST["monumber"];
                                                                            $_SESSION["email"] = $_POST["email"];
                                                                            $_SESSION["PkgHrs"] = $_POST["PkgHrs"];
                                                                            if (isset($_POST["totAmount"]))
                                                                                $_SESSION["totAmount"] = $_POST["totAmount"];
                                                                            if (isset($_POST["vat"]))
                                                                                $_SESSION["vat"] = $_POST["vat"];
                                                                            $_SESSION["secDeposit"] = $_POST["secDeposit"];
                                                                            $_SESSION["dispc"] = $_POST["discount"];
                                                                            $_SESSION["discountAmt"] = $_POST["discountAmt"];
                                                                            $_SESSION["empcode"] = $_POST["empcode"];
                                                                            $_SESSION["disccode"] = $_POST["disccode"];
                                                                            $_SESSION["cciid"] = $cciid;
                                                                            $_SESSION["ChauffeurCharges"] = $_POST["ChauffeurCharges"];
                                                                            $_SESSION["orderid"] = "CORIC-" . $mtime;
                                                                            $_SESSION["addServ"] = $_POST["addServ"];
                                                                            $_SESSION["addServAmt"] = $_POST["addServAmt"];
                                                                            $isPB = '0';
                                                                            if (isset($_POST["paybackearn"]) && $_POST["paybackearn"] != "")
                                                                                $isPB = $_POST["paybackearn"];
                                                                            $_SESSION["IsPayBack"] = $isPB;
                                                                            if (isset($_POST["pagetransactionId"]))
                                                                                $_SESSION["disc_txn_id"] = $_POST["pagetransactionId"];
                                                                            if (isset($_POST["redeemedPts"]))
                                                                                $_SESSION["disc_value"] = $_POST["redeemedPts"];
                                                                            if (isset($_POST["transactionStatus"]))
                                                                                $_SESSION["disc_txn_status"] = $_POST["transactionStatus"];
                                                                            if (isset($_POST["orderid"]))
                                                                                $_SESSION["disc_orderid"] = $_POST["orderid"];
                                                                            if (isset($_POST["ddlSubLoc"]))
                                                                                $_SESSION["subLoc"] = $_POST["ddlSubLoc"];
                                                                            else
                                                                                $_SESSION["subLoc"] = "0";
                                                                            if (isset($_POST["subLocName"]))
                                                                                $_SESSION["subLocName"] = $_POST["subLocName"];
                                                                            else
                                                                                $_SESSION["subLocName"] = "NA";

                                                                            $xmlToSave = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
                                                                            $xmlToSave .= "<ccvtrans>";
                                                                            $query_string = "";
                                                                            if ($_POST) {
                                                                                $kv = array();
                                                                                $dataToSave = array();
                                                                                foreach ($_POST as $key => $value) {
                                                                                    if ($key != "corpassword") {
                                                                                        $kv[] = "$key=$value";
                                                                                        $xmlToSave .= "<$key>$value</$key>";
                                                                                        if ($key == "hdPickdate") {
                                                                                            $picDate = date_create($value);
                                                                                            $dataToSave[$key] = $picDate->format('Y-m-d');
                                                                                        } elseif ($key == "hdDropdate") {
                                                                                            $drpDate = date_create($value);
                                                                                            $dataToSave[$key] = $drpDate->format('Y-m-d');
                                                                                        } else
                                                                                            $dataToSave[$key] = $value;
                                                                                    }
                                                                                }
                                                                                $query_string = join("&", $kv);
                                                                            }
                                                                            else {
                                                                                $query_string = $_SERVER['QUERY_STRING'];
                                                                            }
                                                                            $xmlToSave .= "</ccvtrans>";
                                                                            if (!is_dir("./xml/pre-trans/" . date('Y')))
                                                                                mkdir("./xml/pre-trans/" . date('Y'), 0775);
                                                                            if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m')))
                                                                                mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m'), 0775);
                                                                            if (!is_dir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d')))
                                                                                mkdir("./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d'), 0775);
                                                                            createcache($xmlToSave, "./xml/pre-trans/" . date('Y') . "/" . date('m') . "/" . date('d') . "/CORIC-" . $mtime . ".xml");

                                                                            $db = new MySqlConnection(CONNSTRING);
                                                                            $db->open();
                                                                            $dataToSave = array();
                                                                            $dataToSave["source"] = "COR-MOBILE";

                                                                            $dataToSave["tour_type"] = $_POST["hdTourtype"];
                                                                            $dataToSave["package_id"] = $_POST["hdPkgID"];
                                                                            $dataToSave["car_cat"] = $_POST["hdCat"];
                                                                            if (isset($_POST["hdCarCatID"]) && $_POST["hdCarCatID"] != "")
                                                                                $dataToSave["car_cat_id"] = $_POST["hdCarCatID"];

                                                                            $dataToSave["origin_name"] = $_POST["hdOriginName"];
                                                                            $dataToSave["origin_id"] = $_POST["hdOriginID"];
                                                                            $dataToSave["destinations_name"] = $_POST["hdDestinationName"];
                                                                            $dataToSave["destinations_id"] = $_POST["hdDestinationID"];

                                                                            $picDate = date_create($_POST["hdPickdate"]);
                                                                            $dataToSave["pickup_date"] = $picDate->format('Y-m-d');
                                                                            if (isset($_POST["tHour"]) && $_POST["tHour"] != "" && isset($_POST["tMin"]) && $_POST["tMin"] != "")
                                                                                $dataToSave["pickup_time"] = $_POST["tHour"] . $_POST["tMin"];
                                                                            elseif (isset($_POST["tHourP"]) && isset($_POST["tMinP"]))
                                                                                $dataToSave["pickup_time"] = $_POST["tHourP"] . $_POST["tMinP"];
                                                                            $drpDate = date_create($_POST["hdDropdate"]);
                                                                            $dataToSave["drop_date"] = $drpDate->format('Y-m-d');
                                                                            if ($_POST["hdTourtype"] == "Selfdrive")
																				if($_POST['chkPkgType']=='Weekly' || $_POST['chkPkgType']=='Monthly'){
																					$dataToSave["drop_time"] = $_POST["tHourP"] . $_POST["tMinP"];
																				}else{
                                                                                $dataToSave["drop_time"] = $_POST["tHourD"] . $_POST["tMinD"];
																				}
                                                                                //$dataToSave["drop_time"] = $_POST["tHourD"] . $_POST["tMinD"];

                                                                            $dataToSave["duration"] = $_POST["duration"];
                                                                            if (isset($_POST["hdDistance"]) && $_POST["hdDistance"] != "")
                                                                                $dataToSave["distance"] = $cor->decrypt($_POST["hdDistance"]);
                                                                            else
                                                                                $dataToSave["distance"] = "0";
                                                                            $dataToSave["tot_fare"] = $cor->decrypt($_POST["totFare"]);

                                                                            $dataToSave["address"] = $_POST["address"];
                                                                            $dataToSave["remarks"] = $_POST["remarks"];

                                                                            $dataToSave["full_name"] = $_POST["name"];
                                                                            $dataToSave["email_id"] = $_POST["email"];
                                                                            $dataToSave["mobile"] = $_POST["monumber"];

                                                                            $dataToSave["discount"] = $_POST["discount"];
                                                                            //$dataToSave["discount_amt"] = $_POST["discountAmt"];

                                                                            $disccode = $_POST["disccode"];

                                                                            
                                                                                $dataToSave["discount_amt"] = $cor->decrypt($_POST["discountAmt"]);
                                                                            

                                                                            $empcode = $_POST["empcode"];

                                                                            $dataToSave["promotion_code"] = $disccode;
                                                                            $dataToSave["discount_coupon"] = $empcode;


                                                                            $dataToSave["additional_srv"] = $_POST["addServ"];
                                                                            $dataToSave["additional_srv_amt"] = $_POST["addServAmt"];

                                                                            $dataToSave["chauffeur_charges"] = $cor->decrypt($_POST["ChauffeurCharges"]);

                                                                            $dataToSave["cciid"] = $cciid;
                                                                            $dataToSave["coric"] = "CORIC-" . $mtime;
                                                                            $dataToSave["ip"] = $_POST['ipaddress']; //$_SERVER["REMOTE_ADDR"];
                                                                            $dataToSave["ua"] = $_POST['browser']; //$_SERVER["HTTP_USER_AGENT"];
                                                                            $dataToSave["entry_date"] = date('Y-m-d H:i:s');
                                                                            if (isset($_POST["totAmount"]) && $_POST["totAmount"] != "")
                                                                                $dataToSave["base_fare"] = $cor->decrypt($_POST["totAmount"]);
                                                                            else
                                                                                $dataToSave["base_fare"] = $cor->decrypt($_POST["totFare"]);
                                                                            if (isset($_POST["vat"]) && $_POST["vat"] != "")
                                                                                $dataToSave["vat"] = $cor->decrypt($_POST["vat"]);
                                                                            else
                                                                                $dataToSave["vat"] = 0.00;
                                                                            if (isset($_POST["secDeposit"]) && $_POST["secDeposit"] != "")
                                                                                $dataToSave["sec_deposit"] = $cor->decrypt($_POST["secDeposit"]);
                                                                            else
                                                                                $dataToSave["sec_deposit"] = 0;
                                                                            if (isset($_POST["kmRate"]) && $_POST["kmRate"] != "")
                                                                                $dataToSave["km_rate"] = $cor->decrypt($_POST["kmRate"]);
                                                                            else
                                                                                $dataToSave["km_rate"] = 0;
                                                                            $dataToSave["isPayBack"] = $isPB;
                                                                            if (isset($_POST["pagetransactionId"]))
                                                                                $dataToSave["disc_txn_id"] = $_POST["pagetransactionId"];
                                                                            if (isset($_POST["redeemedPts"]))
                                                                                $dataToSave["disc_value"] = $_POST["redeemedPts"];
                                                                            if (isset($_POST["transactionStatus"]))
                                                                                $dataToSave["disc_txn_status"] = $_POST["transactionStatus"];
                                                                            if (isset($_POST["orderid"]))
                                                                                $dataToSave["disc_orderid"] = $_POST["orderid"];
                                                                            if (isset($_POST["ddlSubLoc"]))
                                                                                $dataToSave["subLoc"] = $_POST["ddlSubLoc"];
                                                                            if (isset($_COOKIE["kword"]) && $_COOKIE["kword"] != "")
                                                                                $dataToSave["kword"] = $_COOKIE["kword"];
                                                                            if (isset($_COOKIE["gclid"]) && $_COOKIE["gclid"] != "")
                                                                                $dataToSave["gclid"] = $_COOKIE["gclid"];
                                                                            $dataToSave["pkg_type"] = $_POST["chkPkgType"];
                                                                            $dataToSave["add_service_cost_all"] = $_POST["addServiceCostAll"];
                                                                            $dataToSave["model_name"] = str_ireplace(array("<br>", "<br />"), "", nl2br($_POST["hdCarModel"]));
                                                                            if (isset($_POST["hdCarModelID"]) && $_POST["hdCarModelID"] != "")
                                                                                $dataToSave["model_id"] = $_POST["hdCarModelID"];

                                                                            if (isset($_POST["subLocName"]) && $_POST["subLocName"] != "")
                                                                                $dataToSave["subLocName"] = $_POST["subLocName"];
                                                                            if (isset($_POST["subLocCost"]) && $_POST["subLocCost"] != "")
                                                                                $dataToSave["subLocCost"] = $cor->decrypt($_POST["subLocCost"]);
                                                                            if (isset($_POST["OriginalAmt"]) && $_POST["OriginalAmt"] != "")
                                                                                $dataToSave["OriginalAmt"] = $cor->decrypt($_POST["OriginalAmt"]);
                                                                            if (isset($_POST["BasicAmt"]) && $_POST["BasicAmt"] != "")
                                                                                $dataToSave["BasicAmt"] = $cor->decrypt($_POST["BasicAmt"]);
                                                                            if (isset($_POST["KMIncluded"]) && $_POST["KMIncluded"] != "")
                                                                                $dataToSave["KMIncluded"] = $_POST["KMIncluded"];
                                                                            if (isset($_POST["ExtraKMRate"]) && $_POST["ExtraKMRate"] != "")
                                                                                $dataToSave["ExtraKMRate"] = $cor->decrypt($_POST["ExtraKMRate"]);
                                                                            if (isset($_POST["airportCharges"]) && $_POST["airportCharges"] != "")
                                                                                $dataToSave["airportCharges"] = $cor->decrypt($_POST["airportCharges"]);

                                                                            if (isset($_POST['nightduration']) && $_POST['nightduration'] != "")
                                                                                $nightcharge = $_POST['nightduration'];

                                                                            if (isset($_POST["NightStayAllowance"]) && $_POST["NightStayAllowance"] != "")
                                                                                $dataToSave["nightstayCharge"] = intval($cor->decrypt($_POST["NightStayAllowance"])) * $nightcharge;


                                                                            if (isset($_POST["WeekDayDuration"]) && $_POST["WeekDayDuration"] != "")
                                                                                $dataToSave["WeekDayDuration"] = $_POST["WeekDayDuration"];

                                                                            if (isset($_POST["WeekEndDuration"]) && $_POST["WeekEndDuration"] != "")
                                                                                $dataToSave["WeekEndDuration"] = $_POST["WeekEndDuration"];

                                                                            if (isset($_POST["FreeDuration"]) && $_POST["FreeDuration"] != "")
                                                                                $dataToSave["FreeDuration"] = $_POST["FreeDuration"];


                                                                            if (isset($_POST["totFare"]) && $_POST["totFare"] != "")
                                                                                $dataToSave["User_billable_amount"] = $cor->decrypt($_POST["totFare"]);

                                                                            if (isset($_POST["triptype"]) && $_POST["triptype"] != "")
                                                                                $dataToSave["trip_type"] = $_POST["triptype"];

                                                                            if (isset($_POST["totFareGross"]) && $_POST["totFareGross"] != "") {
                                                                                $dataToSave["tot_gross_amount"] = $cor->decrypt($_POST["totFareGross"]); //Gross total without discount
                                                                            }

                                                                            if (isset($_REQUEST["pickdrop"]) && $_REQUEST["pickdrop"] != "") {
                                                                                $dataToSave["pickupdrop_address"] = $_REQUEST["pickdrop"]; //pick drop address
                                                                            }

                                                                            $dataToSave["city_location"] = $_POST["city_location_text"];
                                                                            $dataToSave["latitude"] = $_POST["city_location_lat"];
                                                                            $dataToSave["longitude"] = $_POST["city_location_long"];
																			$dataToSave["CGSTPercent"] = $cor->decrypt($_POST['CGSTPercent']);
		$dataToSave["SGSTPercent"] = $cor->decrypt($_POST['SGSTPercent']);
		$dataToSave["IGSTPercent"] = $cor->decrypt($_POST['IGSTPercent']);
		$dataToSave["GSTEnabledYN"] = $cor->decrypt($_POST['GSTEnabledYN']);
		$dataToSave["ClientGSTId"] = $cor->decrypt($_POST['ClientGSTId']);
                                                                            if ($_POST['empcode'] != 'PayBack') {
                                                                                $r = $db->insert("cor_booking_new", $dataToSave);
                                                                            }
                                                                            unset($dataToSave);

                                                                            /* For Tracking Multiple Booking Attempts without being booked 24th August starts */
                                                                            $sql = "Select full_name,email_id,mobile,tour_type,origin_name,destinations_name,ip,ua,pickup_date FROM cor_booking_new WHERE ip = '" . $_SERVER["REMOTE_ADDR"] . "' AND DATE(entry_date) = DATE(NOW()) AND booking_id is null Order By uid desc limit 0, 1";
                                                                            $r = $db->query("query", $sql);

                                                                            if (!array_key_exists("response", $r)) {
                                                                                if (count($r) % 3 == 0) {
                                                                                    //Fire Mail
                                                                                    $cor = new COR();
                                                                                    $html = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Name</td>";
                                                                                    $html .= "<td>" . $r[0]["full_name"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Email</td>";
                                                                                    $html .= "<td>" . $r[0]["email_id"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Mobile</td>";
                                                                                    $html .= "<td>" . $r[0]["mobile"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Tour Type</td>";
                                                                                    $html .= "<td>" . $r[0]["tour_type"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Origin</td>";
                                                                                    $html .= "<td>" . $r[0]["origin_name"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Destination</td>";
                                                                                    $html .= "<td>" . $r[0]["destinations_name"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>IP</td>";
                                                                                    $html .= "<td>" . $r[0]["ip"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Pickup Date</td>";
                                                                                    $html .= "<td>" . $r[0]["pickup_date"] . "</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "<tr>";
                                                                                    $html .= "<td>Source Website</td>";
                                                                                    $html .= "<td>m.carzonrent.com</td>";
                                                                                    $html .= "</tr>";
                                                                                    $html .= "</table>";
                                                                                    $dataToMail = array();
                                                                                    $sql = "select email from EmailManagement as em join email_page_master as epm on em.p_id= epm.p_id where epm.page_name='multiple attempt booking m.carzonrent' and em.status='1'";
                                                                                    $db = new MySqlConnection(CONNSTRING);
                                                                                    $db->open();
                                                                                    $resemail = $db->query("query", $sql);
                                                                                    $a = '';
                                                                                    for ($i = 0; $i <= count($resemail) - 1; $i++) {
                                                                                        $a.= $resemail[$i]['email'] . ';';
                                                                                        $email = trim($a, ';');
                                                                                    }
                                                                                    $db->close();
                                                                                    $dataToMail["To"] = $email;
                                                                                    $dataToMail["Subject"] = "ALERT: Multiple Booking Attempts (" . count($r) . ") on m.carzonrent.com website";
                                                                                    $dataToMail["MailBody"] = $html;

                                                                                    $res = $cor->_CORSendMail($dataToMail);
                                                                                    unset($cor);
                                                                                    unset($dataToMail);
                                                                                }
                                                                            }

                                                                            $db->close();
                                                                            set_include_path('./lib' . PATH_SEPARATOR . get_include_path());
                                                                            require_once('./lib/CitrusPay.php');
                                                                            require_once 'Zend/Crypt/Hmac.php';

                                                                            function generateHmacKey($data, $apiKey = null) {
                                                                                $hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
                                                                                return $hmackey;
                                                                            }

                                                                            $action = "";
                                                                            $flag = "";
                                                                            // CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d", 'production');
                                                                            //CitrusPay::setApiKey("a06521adb14788d10713f08b6885e62d409d507f", 'sandbox');
                                                                            CitrusPay::setApiKey("01ead54113da1cb978b39c1af588cf83e16c519d", 'production');

                                                                           /* if (($cciid == "1400323" || $cciid == "1386289") && $_POST["email"] == "iqbal.shamsi@gmail.com" && $_POST["name"] == "Iqbal Shamsi")
                                                                                $Amount = "1";
                                                                            else {*/
                                                                                $Amount = $totPayable;
                                                                            //}

                                                                            if (isset($_POST["hdCORIC"]) && $_POST["hdCORIC"] != "")
                                                                                $Order_Id = $_POST["hdCORIC"];
                                                                            else
                                                                                $Order_Id = "CORIC-" . $mtime;
                                                                            $fullname = $_POST['name'];
                                                                            $fNames = explode(" ", $fullname);
                                                                            if (count($fNames) > 1) {
                                                                                $fname = $fNames[0];
                                                                                $lname = $fNames[1];
                                                                            } else {
                                                                                $fname = $fullname;
                                                                                $lname = "";
                                                                            }
                                                                            //$vanityUrl = "carzonrent";
                                                                            $vanityUrl = 'carzonrent';
                                                                            $currency = "INR";
                                                                            $merchantTxnId = $Order_Id;
                                                                            $addressState = "";
                                                                            $addressCity = "";
                                                                            $addressStreet1 = "";
                                                                            $addressCountry = "";
                                                                            $addressZip = "";
                                                                            $firstName = $fname;
                                                                            $lastName = $lname;
                                                                            $phoneNumber = $_POST['monumber'];
                                                                            $email = $_POST['email'];
                                                                            $paymentMode = "";
                                                                            $issuerCode = "";
                                                                            $cardHolderName = "";
                                                                            $cardNumber = "";
                                                                            $expiryMonth = "";
                                                                            $cardType = "";
                                                                            $cvvNumber = "";
                                                                            $expiryYear = "";
                                                                            $returnUrl = corWebRoot . "/cp-thanks.php";
                                                                            $orderAmount = $Amount;
                                                                            $flag = "post";
                                                                            $data = "$vanityUrl$orderAmount$merchantTxnId$currency";
                                                                            $secSignature = generateHmacKey($data, CitrusPay::getApiKey());
                                                                            $action = CitrusPay::getCPBase() . "$vanityUrl";
                                                                            $time = time() * 1000;
                                                                            $time = number_format($time, 0, '.', '');

                                                                            $disccode = trim($disccode);
                                                                            if ($disccode == 'MYICI500' || $disccode == 'OTICI500') {
                                                                                $sigkey = '01ead54113da1cb978b39c1af588cf83e16c519d';
                                                                                $discountamountVal = 500;
                                                                                $orderAmount = $orderAmount;
                                                                                $alteredAmount = $orderAmount - $discountamountVal;
                                                                                $couponCode = $disccode;
                                                                                $data = "";
                                                                                $data .= "orderAmount=$orderAmount&alteredAmount=$alteredAmount&couponCode=$couponCode";
                                                                                $hmaccouponsingnature = Zend_Crypt_Hmac::compute($sigkey, "sha1", $data);
                                                                            } else {
                                                                                $hmaccouponsingnature = '';
                                                                            }


                                                                            /**                                                                             * **** AMX  alliance  implementation for self drive  ****** */
                                                                            if ($disccode == 'MYLESAXP') {
                                                                                $sigkey = '01ead54113da1cb978b39c1af588cf83e16c519d';
                                                                                $discountamountVal = 600;
                                                                                $vatval = $_POST["vat"];
                                                                                $mainDiscountData = ($discountamountVal * $vatval / 100);
                                                                                $orderAmount = intval($_REQUEST["totFareGross"] - $mainDiscountData);
                                                                                $alteredAmount = $_POST["totFare"];
                                                                                $data = "$vanityUrl$orderAmount$merchantTxnId$currency";
                                                                                $secSignature = generateHmacKey($data, CitrusPay::getApiKey());
                                                                                $couponCode = $disccode;
                                                                                $data = "";
                                                                                $data2 = "orderAmount=$orderAmount&alteredAmount=$alteredAmount&couponCode=$couponCode";
                                                                                $hmaccouponsingnature = Zend_Crypt_Hmac::compute($sigkey, "sha1", $data2);
                                                                            } else {
                                                                                $hmaccouponsingnature = '';
                                                                            }
                                                                            ?>

                                                                                <div class="toprowinner">
                                                                                    <div data-role="fieldcontain" style="float:left;width:100%">
                                                                                        <div class="d70">
                                                                                            <p>
    <?php
    if ($_POST["hdTourtype"] != "Local" && $_POST["hdTourtype"] != "Selfdrive") {
        echo trim($_POST["hdOriginName"]) . "-" . trim($_POST["hdDestinationName"]) . "-";
    }
    if ($_POST["hdTourtype"] == "Local")
        echo trim($_POST["hdOriginName"]);
    else
        echo trim($_POST["hdOriginName"]);
    ?>

    <?php $disp_PickDate = date_create($_POST["hdPickdate"]); ?>
    <?php
    if (isset($_POST["hdDropdate"]))
        $dropDate = date_create($_POST["hdDropdate"]);
    ?>
</p>
</div>
<div class="d30">


    <?php
    if ($_REQUEST['empcode'] == 'PayBack') {
        ?>

        <p>Rs. <?php echo intval($orignalBilling); ?>/-<br /></p>
        <?php
    } else {
        ?>
        <p>Rs. <?php echo $Amount; ?>/-<br /></p>

        <?php
    }
    ?>


</div>
</div>
<div data-role="fieldcontain" style="float:left;width:100%">
<div class="d70 fwn">
    <?php //echo $disp_PickDate->format('D d-M, Y');   ?>

    <?php
    if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Selfdrive") {
        if (intval($_POST["duration"]) > 1)
            echo $disp_PickDate->format('d M') . " - " . $dropDate->format('d M, Y');
        else
            echo $disp_PickDate->format('d M, Y');
        if ($_POST["hdTourtype"] == "Selfdrive") {
            echo " " . $_POST["tHourP"] . $_POST["tMinP"] . " Hrs";
        }
    } else {
        echo $disp_PickDate->format('d-M, Y');
    }
    ?>
                                                                                        </div>
                                                                                        <div class="d30">
                                                                                            <a style="color:#fff;" class="faredetails" href="javascript:void(0)" onclick="javascript: _pass('fdpopupmobile');" id="link_">Fare details</a>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--PopUpForFareDetial-->
                                                                                    <div id="fdpopupmobile" style="display:none;" class="details_wrapper">
                                                                                        <div class="heading">
                                                                                            Fare Details
                                                                                            <span class="closedpop" onclick="javascript: _closeFD('fdpopupmobile');">X</span>
                                                                                        </div>
                                                                                        <div class="tpdiv">
                                                                                                                        <!--<span id="spPay"><br /><b>Rs.. <?php //echo (intval($_POST["totFare"]) + intval($_POST["addServAmt"])) - intval($_POST["discountAmt"]);    ?>/-</b><span style="font-size:11px;margin:10px 35px 0px 0px;display:block;width:auto;float:right;font-weight:normal;color:#666666;"><?php //if($_POST["hdTourtype"] == "Selfdrive"){     ?>(Including VAT)<?php //} else {     ?>(Including Service Tax)<?php //}    ?></span></span></h3>-->
                                                                                        </div>
                                                                                        <div class="tpdiv">
    <?php
    if ($_POST["hdTourtype"] == "Selfdrive") {
        ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                <?php
                                                                                                if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                                    if ($_POST["WeekDayDuration"] > 0 && $_POST["pkgR"] > 0) {
                                                                                                        ?>
                                                                                                            <li>Weekdays:<?php echo $_POST["WeekDayDuration"]; ?>@<?php echo $_POST["pkgR"]; ?></li>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }
                                                                                                    if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                                        if ($_POST["WeekDayDuration"] > 0 && $_POST["pkgR"] > 0.00) {
                                                                                                            ?>
                                                                                                            <li>No. of hours:<?php echo $_POST["WeekDayDuration"]; ?>@<?php echo $_POST["pkgR"]; ?></li>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }


                                                                                                    if ($_REQUEST['chkPkgType'] == 'Daily' || $_REQUEST['chkPkgType'] == 'Monthly' || $_REQUEST['chkPkgType'] == 'Weekly') {

                                                                                                        if ($_POST["WeekEndDuration"] > 0 && $_POST["PkgRateWeekEnd"] > 0) {
                                                                                                            ?>
                                                                                                            <li>Weekends:<?php echo $_POST["WeekEndDuration"]; ?>@<?php echo $_POST["PkgRateWeekEnd"]; ?></li>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }


                                                                                                    if ($_REQUEST['chkPkgType'] == 'Hourly') {
                                                                                                        if ($_POST["WeekEndDuration"] > 0 && $_POST["PkgRateWeekEnd"] > 0) {
                                                                                                            ?>
                                                                                                            <li>No. of hours:<?php echo $_POST["WeekEndDuration"]; ?>@<?php echo $_POST["PkgRateWeekEnd"]; ?></li>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>Minimum billing: Rs. <?php echo $basicAmt; ?>/-</li>
                                                                                                    <li>Additional Services Cost: Rs. <?php echo $addSrvAmt; ?>/-</li>
                                                                                                    <li>Convenience Charge: Rs. <?php echo $cnvCharge; ?>/-</li>


        <?php
        if ($_REQUEST['empcode'] == 'PayBack') {
            ?>
                                                                                                        <li>Payback Discount Amount: Rs. <span id="disA"><?php echo $disAPayback; ?></span>/-</li>
                                                                                                        <li>Sub Total: Rs. <?php echo (($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge)); ?>/-</li>
                                                                                                        <li>VAT (@<?php echo number_format($_POST["vat"], 3); ?>%): Rs. <?php echo $vatAmt; ?>/-</li>
                                                                                                        <li>Total Fare: Rs. <?php echo $totPayable; ?>/-</li>
            <?php
        } else {
            ?>
                                                                                                        <li>Discount Amount: Rs. <span id="disA"><?php echo $disA; ?></span>/-</li>
                                                                                                        <li>Sub Total: Rs. <?php echo ($subtAmount); ?>/-</li>
                                                                                                        <li>VAT (@<?php echo number_format($_POST["vat"], 3); ?>%): Rs. <?php echo $vatAmt; ?>/-</li>
                                                                                                        <li>Total Fare: Rs. <?php echo $totPayable; ?>/-</li>
            <?php
        }
        ?>





                        <!--<li>Discount Amount: Rs. <span id="disA"><?php echo $disA; ?></span>/-</li>
                        <li>Sub Total: Rs. <?php echo (($basicAmt + $addSrvAmt + $subLocCost + $cnvCharge) - $disA); ?>/-</li>
                        <li>VAT (@<?php echo number_format($_POST["vat"], 2); ?>%): Rs. <?php echo $vatAmt; ?>/-</li>
                        <li>Total fare: Rs. <?php echo $totPayable; ?>/-</li>-->
        <?php
        if (intval($_POST["OriginalAmt"]) > intval($_POST["BasicAmt"])) {
            ?>
                                                                                                        <li>Discount Amount (On base fare): Rs. <?php echo (intval($_POST["OriginalAmt"]) - intval($_POST["BasicAmt"])); ?>/-</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                        ?>
                                                                                                        <li>Refundable Security Deposit  (Pre Auth from card): Rs. <?php echo $_POST["secDeposit"]; ?> (Mastercard/Visa/Amex)</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li>The billing cycle starts from 8am each day</li>
                                                                                                        <li>Refundable Security Deposit: Rs. <?php echo $_POST["secDeposit"]; ?> (To be paid in cash before the start of the journey)</li>
                                                                                                        <li>Additional charges will be applicable for delivery of the vehicle at a special location.</li>
                                                                                                        <li>The vehicle is to be driven within the permissible limits of Goa.</li>
            <?php
        }
        ?>
                                                                                                </ul>
                                                                                                    <?php
                                                                                                    if (trim($_POST["chkPkgType"]) == "Hourly") {
                                                                                                        ?>
                                                                                                    <p style="font-weight:bold;margin-top:10px;float:left"><span>Includes</span></p>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>KMs Included: <?php echo $_POST["KMIncluded"]; ?></li>
                                                                                                        <li>Extra KM Charge: Rs. <?php echo $_POST["ExtraKMRate"]; ?>/- per KM</li>
                                                                                                    </ul>
            <?php
        }
        if ($_POST["empcode"] == "PayBack") {
            ?>
                                                                                                    <p style="font-weight:bold;margin-top:10px;float:left"><span>PAYBACK Discount Details</span></p>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>Original Billing: Rs. <?php echo intval($orignalBilling); ?>/-</li>
                                                                                                        <li>PAYBACK Discount: Rs. <?php echo intval($_POST["discountAmt"]); ?>/-</li>
                                                                                                        <li>PAYBACK Points Deducted: <?php echo intval($_POST["discountAmt"] * 4); ?></li>
                                                                                                        <li>Final Billing: Rs. <?php echo (intval($orignalBilling) - intval($_POST["discountAmt"])); ?>/-</li>
                                                                                                    </ul>
            <?php
        }
        ?>
                                                                                                <p style="font-weight:bold;margin-top:10px;float:left"><span>Mandatory Documents(Original)</span></p>
                                                                                                <?php
                                                                                                if (intval($_POST["hdOriginID"]) != "69") {
                                                                                                    ?>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>Passport / Voter ID card</li>
                                                                                                        <li>Driving license</li>
                                                                                                        <li>Credit card</li>
                                                                                                        <li>Adhaar card</li>
                                                                                                    </ul>
            <?php
        } else {
            ?>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>Passport / Voter ID card</li>
                                                                                                        <li>Driving License</li>
                                                                                                        <li>Any of the following has to be submitted in original as an identity proof.
                                                                                                            <ol type="1">
                                                                                                                <li>Adhaar card</li>
                                                                                                                <li>Pan card</li>
                                                                                                                <li>Voter ID card</li>
                                                                                                            </ol>
                                                                                                        </li>
                                                                                                    </ul>
            <?php
        }
    }
    ?>
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Custom Package") {
                                                                                                ?>
                                                                                                <ul style="margin-left:15px;">					
                                                                                                <?php
                                                                                                if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                    $kmPerDay = intval($cor->decrypt($_POST["hdDistance"]) / $_POST["duration"]);
                                                                                                else 
                                                                                                    $kmPerDay = intval(($_POST["hdDistance"]) / $_POST["duration"]); 
                                                                                                
                                                                                                if ($kmPerDay < 250) {
                                                            
                                                                                                    $displayKMS = 250;
                                                                                                    
                                                                                                    } else {
                                                                                                        
                                                                                                        $displayKMS = intval($kmPerDay * $_POST["duration"]);
                                                                                                        
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                                </ul>
                                                                                                <p style="font-weight:bold;margin-top:10px;float:left"><span>Includes </span></p>
                                                                                                <ul style="margin-left:15px;">
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                                                                        <li><?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> = <?php echo intval($displayKMS * $_POST["duration"]); ?> Kms</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li><?php 
                                                                                                        if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                            echo $cor->decrypt($_POST["hdDistance"]);
                                                                                                        else
                                                                                                            echo $_POST["hdDistance"]; ?> Kms</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>Per Km charge = Rs..<?php 
                                                                                                     if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                        echo $cor->decrypt($_POST["kmRate"]); 
                                                                                                    else
                                                                                                        echo intval($_POST["kmRate"])
                                                                                                    ?>/-</li>
                                                                                                    <li>No. of days = <?php echo $_POST["duration"]; ?> day(s)</li>
                                                                                                    <li>Chauffeur charge = Rs..<?php  
                                                                                                    if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                        echo number_format($cor->decrypt($_POST["ChauffeurCharges"])).'*'.$_POST["duration"]; 
                                                                                                    else
                                                                                                        echo number_format($_POST["ChauffeurCharges"]).'*'.$_POST["duration"];
                                                                                                    ?>
                                                                                                    </li>
                                                                                                    <li>Night Stay Allowance Charge =Rs..<?php 
                                                                                                     if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                        echo number_format($cor->decrypt($_POST['NightStayAllowance'])).'*'.$_POST["nightduration"]; 
                                                                                                    else
                                                                                                        echo number_format($_POST['NightStayAllowance']).'*'.$_POST["nightduration"]; 
                                                                                                    
                                                                                                    ?></li>
                                                                                                    <li>Minimum billable kms per day = 250 kms</li>
                                                                                                    <li>GST</li>
                                                                                                </ul>
                                                                                                <p style="font-weight:bold;margin-top:10px;float:left"><span>Extra Charges</span></p>
                                                                                                <ul style="margin-left:15px;">
                                                                                                    <li>Tolls, parking and state permits as per actuals</li>
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                                                                        <li>Extra Km beyond <?php 
                                                                                                         if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                            echo intval($displayKMS * $_POST["duration"]).' =  Rs..'.intval($cor->decrypt($_POST["kmRate"])); 
                                                                                                         else
                                                                                                             echo intval($displayKMS * $_POST["duration"]).' =  Rs..'.intval($_POST["kmRate"]); 
                                                                                                         ?>/km</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li>Extra Km beyond <?php
                                                                                                        if ($_REQUEST['empcode'] != 'PayBack')
                                                                                                            echo intval($displayKMS * $_POST["duration"]).' =  Rs..'.intval($cor->decrypt($_POST["kmRate"])); 
                                                                                                         else
                                                                                                             echo intval($displayKMS * $_POST["duration"]).' =  Rs..'.intval($_POST["kmRate"]); 
                                                                                                         ?>/km</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                                    <?php
                                                                                                    if ($_POST["empcode"] == "PayBack") {
                                                                                                        ?>
                                                                                                    <p style="font-weight:bold;margin-top:10px;float:left"><span>PAYBACK Discount Details</span></p>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>Original Billing: Rs. <?php echo intval($totPayable); ?>/-</li>
                                                                                                        <li>PAYBACK Discount: Rs. <?php echo intval($_POST["discountAmt"]); ?>/-</li>
                                                                                                        <li>PAYBACK Points Deducted: <?php echo intval($_POST["discountAmt"] * 4); ?></li>
                                                                                                        <li>Final Billing: Rs. <?php echo (intval($totPayable) - intval($_POST["discountAmt"])); ?>/-</li>
                                                                                                    </ul>
            <?php
        }
    }
    if ($_POST["hdTourtype"] == "Local") {
        ?>
                                                                                                <ul style="margin-left:15px;">
                                                                                                <?php if (intval($_POST["dayRate"]) && intval($_POST["kmRate"])) { ?>
                                                                                                        <li>Rate: Rs. <?php echo intval($_POST["dayRate"] / $_POST["PkgHrs"]); ?> / hour</li>
                                                                                                <?php } ?>
                                                                                                    <li>10 km included for every hour of travel</li>
                                                                                                    <li>Rs. <?php echo ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10)); ?> / km for additional km</li>
                                                                                                    <li>Rs. <?php echo $_POST["totFare"]; ?> minimum billing</li>
                                                                                                    <li>New hour billing starts when usage more than 30 mins</li>
                                                                                                    <li>Toll and parking extra</li>
                                                                                                    <li>Discount Amount: Rs. <span id="disA"><?php echo $disA; ?></span>/-</li>
                                                                                                    <li>Chauffeur charges and gst included</li>
        <?php if ($_POST["hdOriginID"] == 2) { ?>
            <?php
            $charge = 200;
            if ($_POST["hdCat"] == 'Van' || $_POST["hdCat"] == '4 Wheel Drive' || $_POST["hdCat"] == 'Full Size') {
                $charge = 300;
            }
            ?>
                                                                                                        <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs. <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    if ($_POST["hdOriginID"] == 7) {
                                                                                                        ?>
                                                                                                        <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    if ($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3) {
                                                                                                        ?>
                                                                                                        <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
                                                                                                    <?php } ?>
                                                                                                </ul>
                                                                                                    <?php
                                                                                                    if ($_POST["disccode"] == "PayBack") {
                                                                                                        ?>
                                                                                                    <p style="font-weight:bold;margin-top:10px;float:left"><span>PAYBACK Discount Details</span></p>
                                                                                                    <ul style="margin-left:15px;">
                                                                                                        <li>Original Billing: Rs. <?php echo intval($totPayable); ?>/-</li>
                                                                                                        <li>PAYBACK Discount: Rs. <?php echo intval($_POST["discountAmt"]); ?>/-</li>
                                                                                                        <li>PAYBACK Points Deducted: <?php echo intval($_POST["discountAmt"] * 4); ?></li>
                                                                                                        <li>Final Billing: Rs. <?php echo (intval($totPayable) - intval($_POST["discountAmt"])); ?>/-</li>
                                                                                                    </ul>
            <?php
        }
    }
    ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--PopUpForFareDetialEnds-->
                                                                                </div>

                                                                                <div class="toprowinner yl">Payment</div>
                                                                                <div data-role="fieldcontain" style="float:left;width:100%">
                                                                                    <div class="leftside">
                                                                                        <h1>&nbsp;</h1>
                                                                                        <form id="formPaymentOptions1" name="formPaymentOptions1" method="POST" action="<?php echo $action; ?>" <?php if ($_POST['hdTourtype'] == 'Selfdrive') { ?> onsubmit="return _checkPaymentSubmit(document.getElementById('formPaymentOptions1'), '<?PHP echo $_REQUEST["hdTourtype"]; ?>');" <?PHP } ?>>
                                                                                            <input name="merchantTxnId" id="Order_Id" type="hidden" value="<?php echo $merchantTxnId; ?>" />
                                                                                            <input name="addressState" type="hidden" value="<?php echo $addressState; ?>" />
                                                                                            <input name="addressCity" type="hidden" value="<?php echo $addressCity; ?>" />
                                                                                            <input name="addressStreet1" type="hidden" value="<?php echo $addressStreet1; ?>" />
                                                                                            <input name="addressCountry" type="hidden" value="<?php echo $addressCountry; ?>" />
                                                                                            <input name="addressZip" type="hidden" value="<?php echo $addressZip; ?>" />
                                                                                            <input name="firstName" type="hidden" value="<?php echo $firstName; ?>" />
                                                                                            <input name="lastName" type="hidden" value="<?php echo $lastName; ?>" />
                                                                                            <input name="phoneNumber" type="hidden" value="<?php echo $phoneNumber; ?>" />
                                                                                            <input name="email" type="hidden" value="<?php echo $email; ?>" />
                                                                                            <input name="paymentMode" type="hidden" value="<?php echo $paymentMode; ?>" />
                                                                                            <input name="issuerCode" type="hidden" value="<?php echo $issuerCode; ?>" />
                                                                                            <input name="cardHolderName" type="hidden" value="<?php echo $cardHolderName; ?>" />
                                                                                            <input name="cardNumber" type="hidden" value="<?php echo $cardNumber; ?>" />
                                                                                            <input name="expiryMonth" type="hidden" value="<?php echo $expiryMonth; ?>" />
                                                                                            <input name="cardType" type="hidden" value="<?php echo $cardType; ?>" />
                                                                                            <input name="cvvNumber" type="hidden" value="<?php echo $cvvNumber; ?>" />
                                                                                            <input name="expiryYear" type="hidden" value="<?php echo $expiryYear; ?>" />
                                                                                            <input name="returnUrl" type="hidden" value="<?php echo $returnUrl; ?>" />
                                                                                            <input name="orderAmount" type="hidden" value="<?php echo $orderAmount; ?>" />
                                                                                            <input type="hidden" name="reqtime" value="<?php echo $time; ?>" />
                                                                                            <input type="hidden" name="secSignature" value="<?php echo $secSignature; ?>" />
                                                                                            <input type="hidden" name="currency" value="<?php echo $currency; ?>" />

                                                                                            <!-- icici alliance implementation -->
                                                                                            <input type="hidden" id="dpSignature" name="dpSignature" value="<?php echo $hmaccouponsingnature; ?>" />
                                                                                            <input type="hidden" name="couponCode" value="<?php echo $disccode; ?>" />
                                                                                            <input type="hidden" name="alteredAmount" value="<?php echo $alteredAmount; ?>" />
                                                                                          
                                                                                            
                                                                                             <input type="hidden" id="cityId" value="<?php echo $_REQUEST['hdOriginID']; ?>"/>
                    <input type="hidden" id="ClientId" value="2205"/>
                    <input type="hidden" id="pickupDate" value="<?php echo $_REQUEST['hdPickdate']; ?>"/>
                    <input type="hidden" id="droffDate" value="<?php echo $_REQUEST['hdDropdate']; ?>"/>
                    <input type="hidden" id="PickUpTime"  value="<?php echo $_REQUEST['tHourP'] . '' . $_REQUEST['tMinP']; ?>"/>
                    <input type="hidden" id="DropOffTime"  value="<?php echo $_REQUEST['tHourD'] . '' . $_REQUEST['tMinD']; ?>"/>
                    <input type="hidden" id="SubLocationId" value="<?php echo $_REQUEST['ddlSubLoc']; ?>"/>
                    <input type="hidden" id="PkgType"  value="<?php echo $_REQUEST['chkPkgType']; ?>"/> 
                    <input type="hidden" id="CarModelId" value="<?php echo $_REQUEST['hdCarModelID']; ?>"/>


                                                                                            
                                                                                            <!-- icici alliance implementation -->
                                                                                            <!----------trip advisor-------------->
    <?PHP
    if ($_REQUEST['tripadvisorname'] == 'true' && $_REQUEST["hdTourtype"] == "Outstation") {
        $_SESSION['tripadvisor'] = $_REQUEST['tripadvisorname'];
    } else if ($_REQUEST['tripadvisorname'] == 'true' && $_REQUEST["hdTourtype"] == "Selfdrive") {
        $_SESSION['tripadvisor'] = $_REQUEST['tripadvisorname'];
        $_SESSION['tripadvisordestinationid'] = $_REQUEST['tripadvisordestinationid'];
    }
    ?>
                                                                                            <!----------trip advisor-------------->


                                                                                            <fieldset class="payment" style="width:98%;margin-left: 2%;padding-left:0 !important">
                                                                                                <div class="leftfieldset">
                                                                                                    <label>Pay Online</label>
                                                                                                    <ul class="listarrow">
                                                                                                        <li>Using VISA, MasterCard, AMEX and Debit Cards</li>
                                                                                                        <li>Using cash cards</li>
                                                                                                    </ul>
                                                                                                    <div class="clr"> </div>
                                                                                                    <!---change----->
    <?PHP
    if ($_POST["hdDestinationID"] == '69') {
        $css_checkbox = 'margin-left:24px;margin-top: 8px;display:none;';
        $css_button = 'display:block;';
    } else {
        $css_checkbox = 'margin-left:24px;margin-top: 8px;';
        $css_button = 'display:none;';
    }
    ?>
                                                                                                    <?php if ($_POST['hdTourtype'] == 'Selfdrive') {
                                                                                                        ?>
                                                                                                        <p style='<?php echo $css_checkbox; ?>'><!---change----->
                                                                                                            To make the pick-up process quicker and smoother, simply tick on the Terms & Conditions below and proceed to payment.</p>
                                                                                                        <p style='<?php echo $css_checkbox; ?>'><!---change----->
                                                                                                            Please note a pre-authorisation of only Rs. 5000 will be charged at the time of pick-up from you and will be released to you at the time of drop-off subject to deductions due to mis-use or damage to the car.
                                                                                                        </p>
                                                                                                        <p style='<?php echo $css_checkbox; ?>'><!---change----->

                                                                                                        <p style='margin-left:30px'><br /><input type="checkbox" name="tc" id="tc" onclick="checkbox_action();" />&nbsp;I agree to the Carzonrent (India) <a class="paytc"  onclick="tnclink_action();" title="Click to show Terms And Conditions" href="javascript:void(0);" style="text-decoration:underline;cursor:pointer;">Terms &amp; Conditions</a></p>	
                                                                                                        <!------tnc agreement start---->
        <?php include("./selfdrive-tnc/termandcondition.php"); ?>  
                                                                                                        <!------tnc agreement end---->
    <?PHP } ?>	                                                  </div>

                                                                                                    <?php if ($_POST['hdTourtype'] == 'Selfdrive') {
                                                                                                        ?>	
                                                                                                    <div class="rightfieldset">
                                                                                                        <div class="payandbook">				
                                                                                                            <input type="submit" name="" value="Book And Pay" class="styled-button-1" style='<?php echo $css_button; ?>'/>
                                                                                                        </div>
                                                                                                    </div>
    <?PHP } else { ?>
                                                                                                    <div class="rightfieldset">
                                                                                                        <div class="payandbook"><a onclick="javascript: _checkPaymentSubmit(document.getElementById('formPaymentOptions1'), '<?PHP echo $_REQUEST["hdTourtype"]; ?>');" href="javascript: void(0);" class="genbtnlink bt_pay">Pay and Book</a>
                                                                                                        </div>
                                                                                                    </div>
    <?PHP } ?>
                                                                                            </fieldset>
                                                                                        </form>

                                                                                                <?php
																								$cor = new COR();
                                                                                                if ($cor->decrypt($_POST["totFare"]) < 5000 && $_POST["hdTourtype"] != "Selfdrive") {
                                                                                                    ?>
                                                                                            <form id="formPaymentOptions2" name="formPaymentOptions2" method="POST" action="./">
                                                                                                <input type="hidden" id="hdCarModel<?php echo $i; ?>" name="hdCarModel" value="<?php echo $_POST["hdCarModel"]; ?>" />
                                                                                                <input type="hidden" id="hdCat<?php echo $i; ?>" name="hdCat" value="<?php echo $_POST["hdCat"]; ?>" />
                                                                                                <input type="hidden" name="hdOriginName" id="hdOriginName<?php echo $i; ?>" value="<?php echo trim($_POST["hdOriginName"]); ?>" />
                                                                                                <input type="hidden" name="hdOriginID" id="hdOriginID<?php echo $i; ?>" value="<?php echo $_POST["hdOriginID"] ?>"/>
                                                                                                <input type="hidden" name="hdDestinationName" id="hdDestinationName<?php echo $i; ?>" value="<?php echo trim($_POST["hdDestinationName"]); ?>"/>
                                                                                                <input type="hidden" name="hdDestinationID" id="hdDestinationID<?php echo $i; ?>" value="<?php echo $_POST["hdDestinationID"] ?>"/>
                                                                                                <input type="hidden" name="hdPkgID" id="hdPkgID<?php echo $i; ?>" value="<?php echo $_POST["hdPkgID"]; ?>">
                                                                                                <input type="hidden" name="hdCarCatID" id="hdCarCatID<?php echo $i; ?>" value="<?php echo $_POST["hdCarCatID"]; ?>" />
                                                                                                <input type="hidden" name="dayRate" id="dayRate<?php echo $i; ?>" value="<?php echo $_POST["dayRate"]; ?>" />
                                                                                                <input type="hidden" name="kmRate" id="kmRate<?php echo $i; ?>" value="<?php echo $_POST["kmRate"]; ?>" />
                                                                                                <input type="hidden" name="duration" id="duration<?php echo $i; ?>" value="<?php echo $_POST["duration"]; ?>" />
                                                                                                <input type="hidden" name="totFare" id="totFare<?php echo $i; ?>" value="<?php echo $_POST["totFare"]; ?>" />
                                                                                                <input type="hidden" name="tab" id="tab<?php echo $i; ?>" value="4" />
                                                                                                <input type="hidden" name="hdPickdate" id="pickdate<?php echo $i; ?>" value="<?php echo $_POST["hdPickdate"] ?>" />
                                                                                                <input type="hidden" name="hdDropdate" id="dropdate<?php echo $i; ?>" value="<?php echo $_POST["hdDropdate"] ?>" />
                                                                                                <input type="hidden" name="hdTourtype" id="tourtype<?php echo $i; ?>" value="<?php echo $_POST["hdTourtype"] ?>" />
                                                                                                <input type="hidden" name="hdDistance" id="distance<?php echo $i; ?>" value="<?php echo $_POST["hdDistance"] ?>" />
                                                                                                <input type="hidden" name="monumber" id="mobile<?php echo $i; ?>" value="<?php echo $_POST["monumber"] ?>" />
                                                                                                <input type="hidden" name="picktime" id="picktime<?php echo $i; ?>" value="<?php
                                                                                    if (isset($_POST["picktime"])) {
                                                                                        echo $_POST["picktime"];
                                                                                    } else {
                                                                                        echo $_POST["tHour"] . $_POST["tMin"];
                                                                                    }
                                                                                                    ?>" />
                                                                                                <input type="hidden" name="pincode" id="pincode<?php echo $i; ?>" value="<?php echo $_POST["pincode"] ?>" />
                                                                                                <input type="hidden" name="address" id="address<?php echo $i; ?>" value="<?php echo $_POST["address"] ?>" />
                                                                                                <input type="hidden" name="name" id="name<?php echo $i; ?>" value="<?php echo $_POST["name"] ?>" />
                                                                                                <input type="hidden" name="email" id="email<?php echo $i; ?>" value="<?php echo $_POST["email"] ?>" />
                                                                                                <input type="hidden" name="cciid" id="cciid<?php echo $i; ?>" value="<?php echo $cciid; ?>" />
                                                                                                <input type="hidden" name="hdTransactionID" id="hdTransactionID<?php echo $i; ?>" value="<?php echo $Order_Id; ?>" />
                                                                                                <input type="hidden" name="hdTrackID" id="hdTrackID<?php echo $i; ?>" value="<?php echo $Order_Id; ?>" />
                                                                                                <input type="hidden" name="hdRemarks" id="hdRemarks<?php echo $i; ?>" value="<?php echo $_POST["remarks"]; ?>" />
                                                                                                <input type="hidden" name="PkgHrs" id="PkgHrs<?php echo $i; ?>" value="<?php echo $_POST["PkgHrs"]; ?>" />
                                                                                                <input type="hidden" name="discount" id="discount<?php echo $i; ?>" value="<?php echo $_POST["discount"]; ?>" />
                                                                                                <input type="hidden" name="discountAmt" id="discountAmt<?php echo $i; ?>" value="<?php echo $_POST["discountAmt"]; ?>" />
                                                                                                <input type="hidden" name="empcode" id="empcode<?php echo $i; ?>" value="<?php echo $_POST["empcode"]; ?>" />
                                                                                                <input type="hidden" name="disccode" id="disccode<?php echo $i; ?>" value="<?php echo $_POST["disccode"]; ?>" />
                                                                                                <input type="hidden" name="ChauffeurCharges" id="ChauffeurCharges<?php echo $i; ?>" value="<?php echo $_POST["ChauffeurCharges"]; ?>" />
                                                                                                <input type="hidden" name="NightStayAllowance" id="NightStayAllowance<?php echo $i; ?>" value="<?php echo $_POST["NightStayAllowance"]; ?>" />
                                                                                                <input type="hidden" name="nightduration" id="nightduration<?php echo $i; ?>" value="<?php echo $_POST["nightduration"]; ?>" />
                                                                                                <input type='hidden' id="city_location_lat"  name="city_location_lat" value = "<?php echo $_POST["city_location_lat"]; ?>"/>
                                                                                                <input type='hidden' id="city_location_long"  name="city_location_long" value="<?php echo $_POST["city_location_long"]; ?>"/>

                                                                                                <fieldset class="payment" style="width:98%;margin-left: 2%;padding-left:0 !important">
                                                                                                    <div class="leftfieldset">
                                                                                                        <label>Pay cash to the driver at start of journey</label>
                                                                                                        <ul class="listarrow">
                                                                                                            <li>You shall receive a confirmation call 2-4 hours prior to start of journey</li>
                                                                                                            <li>Cab will be dispatched to pick up location after receiving confirmation on call</li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="rightfieldset">
                                                                                                        <div class="confirmbooking"><br /><a onclick="javascript: _checkPaymentSubmit(document.getElementById('formPaymentOptions2'));" href="javascript: void(0);" class="genbtnlink">Pay and Book</a></div>
                                                                                                    </div>
                                                                                                </fieldset>
                                                                                            </form>
    <?php } ?>
    <?php if ($_POST["hdTourtype"] != "Selfdrive") { ?>
                                                                                            <div class="border_b"></div>
                                                                                            <div class="middiv"> <strong>NOTE</strong>
                                                                                                <ul style="margin-left:15px">
                                                                                                    <li>Final amount will be as per actual</li>
        <?php
        if ($_POST["hdTourtype"] != "Selfdrive") {
            ?>
                                                                                                        <li>Pending amount after advance adjustment is to be paid to driver as cash at the end of journey</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>0% cancellation charges on amount paid as advance</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                                <?PHP } ?>
                                                                                    </div>
                                                                                    <div class="rightside" style="display:none">
                                                                                        <h1>Booking Summary</h1>
                                                                                        <div class="tpdiv">						
                                                                                            <img src="<?php echo corWebRoot; ?>/images/<?php echo str_replace(" ", "-", $_POST["hdCat"]); ?>.jpg" alt="<?php echo $_POST["hdCat"]; ?>" />
                                                                                            <div class="clr"></div>
                                                                                            <p><span>Car Type:</span> <?php echo $_POST["hdCat"]; ?> - <?php echo $_POST["hdCarModel"]; ?></p>
                                                                                            <p><span>From:</span> <?php echo $_POST["hdOriginName"] ?></p>
                                                                                            <p><span>Service:</span> <?php echo $_POST["hdTourtype"] ?> </p>
    <?php
    if ($_POST["hdTourtype"] != "Local") {
        ?>
                                                                                                <p><span>Destination:</span> <?php echo $_POST["hdDestinationName"] ?></p>
                                                                                                <?php
                                                                                            }
                                                                                            $pickDate = date_create($_POST["hdPickdate"]);
                                                                                            $dropDate = date_create($_POST["hdDropdate"]);
                                                                                            ?>

                                                                                            <p><span>Pickup Date:</span> <?php echo $pickDate->format('D d-M, Y'); ?></p>
                                                                                            <p><span>Drop Date:</span> <?php echo $dropDate->format('D d-M, Y'); ?></p>
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Selfdrive") {
                                                                                                ?>
                                                                                                <p><span>Pickup Time:</span> <?php
                                                                                                if (isset($_POST["picktime"])) {
                                                                                                    echo $_POST["picktime"];
                                                                                                } else {
                                                                                                    echo $_POST["tHourP"] . $_POST["tMinP"];
                                                                                                }
                                                                                                ?> Hrs</p>
                                                                                                <p><span>Drop Time:</span> <?php
                                                                                                    if (isset($_POST["droptime"])) {
                                                                                                        echo $_POST["droptime"];
                                                                                                    } else {
                                                                                                        echo $_POST["tHourD"] . $_POST["tMinD"];
                                                                                                    }
                                                                                                    ?> Hrs</p>
                                                                                                    <?php
                                                                                                } else {
                                                                                                    ?>
                                                                                                <p><span>Pickup Time:</span> <?php
                                                                                                if (isset($_POST["picktime"])) {
                                                                                                    echo $_POST["picktime"];
                                                                                                } else {
                                                                                                    echo $_POST["tHour"] . $_POST["tMin"];
                                                                                                }
                                                                                                ?> hrs</p>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            <p><span>Pickup Address:</span> <?php echo $_POST["address"] ?></p>
                                                                                            <h3>Round Trip Fare<br />
                                                                                                <span id="spPay">Rs. <?php echo (intval($_POST["totFare"]) + intval($_POST["addServAmt"])) - intval($_POST["discountAmt"]); ?>/-<span style="font-size:11px;margin:10px 35px 0px 0px;display:block;width:auto;float:right;font-weight:normal;color:#666666;">(Including GST)</span></span></h3>
                                                                                        </div>
                                                                                        <div class="tpdiv">
    <?php
    if ($_POST["hdTourtype"] == "Selfdrive") {
        ?>
                                                                                                <ul>
                                                                                                    <li>Minimum Billing: Rs. <?php echo $_POST["totAmount"]; ?></li>
                                                                                                    <li>VAT (@<?php echo $_POST["vat"]; ?>%): Rs. <?php echo intval(ceil(($_POST["totAmount"] * $_POST["vat"]) / 100)); ?></li>
                                                                                                    <li>Total Fare: Rs. <?php echo intval($_POST["totAmount"]) + intval(ceil(($_POST["totAmount"] * $_POST["vat"]) / 100)); ?></li>
                                                                                                    <li>Refundable Security Deposit  (Pre Auth from card): Rs. <?php echo $_POST["secDeposit"]; ?> (Mastercard/Visa/Amex)</li>
                                                                                                </ul>
                                                                                                <p style="margin-top:10px;float:left;font-weight:bold"><span>Mandatory Documents(Original)</span></p>
                                                                                                <ul>
                                                                                                    <li>Passport</li>
                                                                                                    <li>Driving License</li>
                                                                                                    <li>Credit Card</li>
                                                                                                    <li>Adhaar card</li>
                                                                                                </ul>
        <?php
    }
    ?>
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] == "Outstation" || $_POST["hdTourtype"] == "Custom Package") {
                                                                                                ?>
                                                                                                <ul>					
                                                                                                <?php
                                                                                                $kmPerDay = intval(($_POST["hdDistance"]) / $_POST["duration"]);
                                                                                                if ($kmPerDay < 250) {
                                                                                                    //	$totAmount = intval(250 * $_POST["duration"] * $_POST["kmRate"]);
                                                                                                    $displayKMS = 250;
                                                                                                    ?>
                                                                                                                                                    <!--<li>Rs.. (<?php echo intval($_POST["kmRate"]); ?> * <?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> + <?php echo $_POST["ChauffeurCharges"]; ?> * <?php echo $_POST["duration"]; ?>) * 104.95% = Rs.. <?php echo $_POST["totFare"]; ?>/-</li>-->
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        //	$totAmount = intval($kmPerDay * $_POST["duration"] * $_POST["kmRate"]);
                                                                                                        $displayKMS = intval($kmPerDay * $_POST["duration"]);
                                                                                                        ?>
                                                                                                                                                    <!--<li>Rs.. (<?php echo intval($_POST["kmRate"]); ?> * <?php echo $displayKMS; ?>  + <?php echo $_POST["ChauffeurCharges"]; ?> * <?php echo $_POST["duration"]; ?>) * 104.95% = Rs.. <?php echo $_POST["totFare"]; ?>/-</li>-->
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                                </ul><p><span>Includes </span></p>
                                                                                                <ul>
                                                                                                    <?php
                                                                                                    if ($kmPerDay < 250) {
                                                                                                        ?>
                                                                                                        <li><?php echo $displayKMS; ?> * <?php echo $_POST["duration"]; ?> = <?php echo intval($displayKMS * $_POST["duration"]); ?> Kms</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li><?php echo $_POST["hdDistance"]; ?> Kms</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <li>Per Km charge = Rs..<?php echo intval($_POST["kmRate"]); ?>/-</li>
                                                                                                    <li>No. of days = <?php echo $_POST["duration"]; ?> day(s)</li>
                                                                                                    <li>Chauffeur charge = Rs..<?php echo number_format($_POST["ChauffeurCharges"]); ?> * <?php echo $_POST["duration"]; ?></li>
                                                                                                    <li>Minimum billable kms per day = 250 kms</li>
                                                                                                    <li>GST</li>
                                                                                                </ul>
                                                                                                <p><span>Extra Charges</span></p>
                                                                                                <ul>
                                                                                                    <li>Tolls, parking and state permits as per actuals</li>
        <?php
        if ($kmPerDay < 250) {
            ?>
                                                                                                        <li>Extra Km beyond <?php echo intval($displayKMS * $_POST["duration"]); ?> =  Rs..<?php echo intval($_POST["kmRate"]); ?>/km</li>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <li>Extra Km beyond <?php echo $_POST["hdDistance"]; ?> kms =  Rs..<?php echo intval($_POST["kmRate"]); ?>/km</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                                    <?php
                                                                                                }
                                                                                                if ($_POST["hdTourtype"] == "Local") {
                                                                                                    ?>
                                                                                                <ul>
                                                                                                <?php if (intval($_POST["dayRate"]) && intval($_POST["kmRate"])) { ?>
                                                                                                        <li>Rate: Rs. <?php echo intval($_POST["dayRate"] / $_POST["PkgHrs"]); ?> / hour</li>
                                                                                                <?php } ?>
                                                                                                    <li>10 km included for every hour of travel</li>
                                                                                                    <li>Rs. <?php echo ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10)); ?> / km for additional km</li>
                                                                                                    <li>Rs. <?php echo $_POST["totFare"]; ?> minimum billing</li>
                                                                                                    <li>New hour billing starts when usage more than 30 mins</li>
                                                                                                    <li>Toll and parking extra</li>
                                                                                                    <li>Chauffeur charges and gst included</li>
        <?php if ($_POST["hdOriginID"] == 2) { ?>
            <?php
            $charge = 200;
            if ($_POST["hdCat"] == 'Van' || $_POST["hdCat"] == '4 Wheel Drive' || $_POST["hdCat"] == 'Full Size') {
                $charge = 300;
            }
            ?>
                                                                                                        <li>In case of travel to Noida or Ghaziabad, UP state permit of Rs. <?php echo $charge; ?> to be paid by guest as per tax receipt</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    if ($_POST["hdOriginID"] == 7) {
                                                                                                        ?>
                                                                                                        <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from airport.</li>
                                                                                                        <?php
                                                                                                    }
                                                                                                    if ($_POST["hdOriginID"] == 2 || $_POST["hdOriginID"] == 11 || $_POST["hdOriginID"] == 3) {
                                                                                                        ?>
                                                                                                        <li>Additional Rs. <?php echo (20 * ($_POST["dayRate"] / ($_POST["PkgHrs"] * 10))); ?> to be paid in case of pick up or drop from Faridabad and Ghaziabad</li>
                                                                                                    <?php } ?>
                                                                                                </ul>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                        </div>
                                                                                        <div class="tpdiv">
                                                                                            <div class="map">
                                                                                            <?php
                                                                                            if ($_POST["hdTourtype"] != "Local" && $_POST["hdTourtype"] != "Selfdrive") {
                                                                                                ?>
                                                                                                    <iframe width="257" height="179" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=<?php echo urlencode($orgName); ?>&amp;daddr=<?php echo urlencode($dwstate); ?>&amp;hl=en&amp;aq=1&amp;oq=<?php echo urlencode($orgName); ?>&amp;sll=<?php echo $ll; ?>&amp;mra=ls&amp;ie=UTF8&amp;ll=<?php echo $ll; ?>&amp;t=m&amp;z=4&amp;iwloc=addr&amp;output=embed"></iframe>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            </div>	
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
<?php } ?>
                                                                        </div>
<?php include_once("./includes/footer.php"); ?>
                                                                        </div><!--Content for Mobile DIV-->
                                                                        <?php
                                                                        if (isset($_POST["resp"])) {
                                                                            if ($_POST["resp"] == 'country2') {
                                                                                ?>
                                                                                <script>
                                                                                    <!--
                                                                                                document.getElementById('country1').style.display = 'none';
                                                                                    document.getElementById('country2').style.display = 'block';
                                                                                    document.getElementById('menuTab1').style.className = '';
                                                                                    document.getElementById('menuTab2').style.className = 'selected';
                                                                                    //-->
                                                                                </script>
        <?php
    }
}
?>


                                                                        <?php
                                                                        if ($_POST["hdTourtype"] == "Selfdrive" && $_POST["hdOriginID"] == '69') {
                                                                            ?>
                                                                            <!-- Goa Pop Up Starts-->
                                                                            <div id="goaTC">
                                                                                <div class="heading" style="font-size:20px;color:#666"><b>Additional Amount</b><a style="float:right;text-decoration:none;color:#666" href="javascript: void(0);" onclick="_hideFloatingObjectWithID('goaTC');
                                                                                            _enableThisPage();">X</a></div>
                                                                                <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;margin-bottom:10px;">
                                                                                    <tr>
                                                                                        <td style="width:25%;"><b>Destination</b></td>
                                                                                        <td style="width:25%;"><b>Rates one way </b></td>
                                                                                        <td style="width:25%;"><b>Early morning charge</b></td>
                                                                                        <td style="width:25%;"><b>Night charges 6 - 9pm to 9 to 12 am & above</b></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;"><b>&nbsp;</b></td>
                                                                                        <td style="width:25%;"><b>&nbsp;</b></td>
                                                                                        <td style="width:25%;"><b>5 am - 8am</b></td>
                                                                                        <td style="width:25%;"><b>Every 3 hours Rs.200/- after 6 pm</b></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Margao Railway station</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Airport</td>
                                                                                        <td style="width:25%;">300</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Mapusa</td>
                                                                                        <td style="width:25%;">400</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Panaji</td>
                                                                                        <td style="width:25%;">300</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Tivim station</td>
                                                                                        <td style="width:25%;">700</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Calangute Baga</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Anjuna Vagator</td>
                                                                                        <td style="width:25%;">800</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Colva, Varca, Benaulim</td>
                                                                                        <td style="width:25%;">300</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Carmona</td>
                                                                                        <td style="width:25%;">400</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width:25%;">Mobor Cavelossim</td>
                                                                                        <td style="width:25%;">400</td>
                                                                                        <td style="width:25%;">200</td>
                                                                                        <td style="width:25%;">600</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>

    <?php
}
?>
                                                                        <img src="<?php echo corWebRoot; ?>/images/loading.gif" border="0" width="1px" height="1px"/>
                                                                        </body>
                                                                        </html>
                                                                        <script>
                                                                            function activeinactivepayback() {

                                                                                var v = document.getElementById("disccode").value;
                                                                                document.getElementById("chkCopyFromTop").disabled = true;
                                                                                if (v.length < 1) {
                                                                                    document.getElementById("chkCopyFromTop").disabled = false;
                                                                                }
                                                                            }
                                                                        </script>
                                                                        <script type="text/javascript" src="<?php echo corWebRoot; ?>/js/securebrowser.js"></script>
                                                                        <script type="text/javascript" src="http://l2.io/ip.js?var=myip"></script>
                                                                        <script>document.getElementById('ipaddress').value = myip;</script>
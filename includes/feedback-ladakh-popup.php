<div id="registration" style="padding-left:25px !important;">
<div id="regclose">
	<a onclick="_hideFloatingObjectWithID('registration'); _enableThisPage();document.getElementById('finishmess').innerHTML = '';"><img alt="Close" title="Close" style="float:right;padding-right:15px; cursor: pointer;" src ="<?php echo corWebRoot; ?>/images/close.png"/></a>
</div>
<div class="divtabtxt" style="width:560px;">
				<div class="middiv" style="float:left;height:500px;	overflow-y:scroll;overflow-x:hidden;">
					<div class="register" style="margin-bottom:25px !important;width:528px !important;">
						
						<form method="post" name="frmFeedback" id="frmFeedback" action="../save-booking-feedback-myles.php" onsubmit="javascript: return _validateFeedback();">
						<input type="hidden" name="hdAllowFormSubmit" id="hdAllowFormSubmit" value="no" />	
						<input type="hidden" name="hdCheckfeedback" id="hdCheckfeedback" value="true" />					
						<input type="hidden" name="hdIsSelfDrive" id="hdIsSelfDrive" value="0" />
						<!-- Added By Aamir on 3May -->
						<input type="hidden" name="hdmonumber" id="hdmonumber" value="" />
						<input type="hidden" name="hdtxtname" id="hdtxtname" value="" />
						<input type="hidden" name="hdemail" id="hdemail" value="" />

						<div class="fieldrow2">
							Thank you for considering this. <br />This won't take more than a minute of yours & will help us serve you better.<br /><br /><br />
							<label>Please let us know if you have a valid booking ID?</label>
						</div>
						<div class="fieldrow">
							<table width="30%" cellspacing="2" cellpadding="2" border="0">
								<tbody>
									<tr>
										<td width="15px"><input type="radio" checked="checked" onclick="javascript: document.getElementById('feedback1').style.display = 'block';document.getElementById('divNOblock').style.display = 'none'" value="1" id="opt1" name="opt" /></td>
										<td>Yes</td>
										<td width="15px"><input type="radio" onclick="javascript: document.getElementById('feedback1').style.display = 'none';document.getElementById('divNOblock').style.display = 'block';" value="2" id="opt2" name="opt" /></td>
										<td>No</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div style="display:block;">
							<div class="fl">
								<div id="feedback1">
									<div class="fieldrow2" style="margin-bottom:14px;">
										  <label>Please provide us with your booking id</label>&nbsp;
									</div>
									<div class="fieldrow" style="margin-bottom:0px;">
										<input type="text" onkeypress="javascript: return _allowNumeric(event);" onblur="javascript: _getBooking(document.getElementById('bookingid').value);" maxlength="10" value="" id="bookingid" name="bookingid" class="tbgen" />&nbsp;<span id="spDiscount"></span>
									</div>
								</div>
							</div>
							<div id="divNOblock" style="display:none">
								<div class="fl">
									<div class="fieldrow2">
										  <label>Mobile number</label>&nbsp;
									</div>
									<div class="fieldrow">
										<input type="text" maxlength="10" value="" class="tbgen" id="monumber" name="monumber" />
									</div>
								</div>
								<div class="fl">
									<div class="fieldrow2">
										  <label>Name</label>&nbsp;
									</div>
									<div class="fieldrow">
										<input type="text" onkeypress="javascript: return _allowAlpha(event);" class="tbgen" value="" id="txtname" name="txtname" />
									</div>
								</div>
								<div style="clear:both"></div>
								<div class="fl">
									<div class="fieldrow2">
										<label>Email</label>
									</div>
									<div class="fieldrow">
										<input type="text" value="" id="email" class="tbgen" name="email" />
									</div>
								</div>
							</div>
							<div style="clear:both"></div>
							<div class="row">
									What was the purpose of your trip?
								</div>
								<div style="clear:both"></div>
								<div class="resp">
									<select id="ddlpurpose" name="ddlpurpose" class="ddl" onchange="_showOtherPurpose(this.value)">
										<option value="0">Select One</option>
										<option value="Holiday">Holiday</option>
										<option value="Business">Business</option>
										<option value="Alternate car">Alternate car</option>
										<option value="Bigger car">Bigger car</option>
										<option value="Others">Others</option>
									</select>
									<textarea id="txtOtherPurpose" rows="2" cols="40" name="txtOtherPurpose" style="margin-left:15px;display: none">Please specify</textarea>
								</div>
								<div style="clear:both"></div>
								<div class="row">
									Reason you chose us?
								</div>
								<div style="clear:both"></div>
								<div class="resp">
								<div style="float:left;">
									<div class="col">
										<input type="checkbox" id="ckReason1" name="satisfied1" value="Multiple/nearby location (s)" class="mybox" /><label for="ckReason1">Multiple/nearby location (s)</label>
									</div>
									<div class="col">
										<input type="checkbox" id="ckReason2" name="satisfied2" value="Variety of cars" class="mybox" /><label for="ckReason2">Variety of cars</label>
									</div>
								</div>
								<div>
									<div class="col">
										<input type="checkbox" id="ckReason3" name="satisfied3" value="I like the concept" class="mybox" /><label for="ckReason3">I like the concept</label>
									</div>
									<div class="col">
										<input type="checkbox" id="ckReason4" name="satisfied4" value="Cost effective" class="mybox" /><label for="ckReason4">Cost effective</label>
									</div>
								</div>
								<div>
									<div class="col">
										<input type="checkbox" id="ckReason5" name="satisfied5" value="Privacy" class="mybox" /><label for="ckReason5">Privacy</label>
									</div>
									<div class="col" style="width:300px">
										<input type="checkbox" id="ckReason6" name="satisfied6" value="Was referred by a friend/colleague/relative" class="mybox" /><label for="ckReason6">Was referred by a friend/colleague/relative</label>
									</div>
								</div>
								<div>
									<div class="col" style="width:500px;">
										<input type="checkbox" id="ckReason7" name="satisfied7" id="chkOtherReason" value="Yes" class="mybox" onclick="javascript: _showOtherReason(this.id)" /><label for="ckReason7">Others</label>
										<textarea  id="txtOtherReason" rows="2" cols="40" name="txtOtherReason" style="display: none;margin-left:20px;">Please specify</textarea>
									</div>
								</div>
								</div>
								<div style="clear:both"></div>
								<div class="experience">
									Rate your overall experience with us?<br />
									<div class="resp">
									<input type="radio" id="rd1" name="rd" value="1" class="mybox" onclick="javascript: _showWWR(this.id)"/><label for="rd1">1</label>
									<input type="radio" id="rd2" name="rd" value="2" class="mybox" onclick="javascript: _showWWR(this.id)"/><label for="rd2">2</label>
									<input type="radio" id="rd3" name="rd" value="3" class="mybox" onclick="javascript: _showWWR(this.id)"/><label for="rd3">3</label>
									<input type="radio" id="rd4" name="rd" value="4" class="mybox" onclick="javascript: _showWWR(this.id)"/><label for="rd4">4</label>
									<input type="radio" id="rd5" name="rd" value="5" class="mybox" onclick="javascript: _showWWR(this.id)"/><label for="rd5">5</label>
									</div>
									<div style="clear:both"></div>
									<div id="wwr">
									What went wrong?<br />
									<div class="resp">
										<div class="col">
											<input type="checkbox" id="chkwwr1" name="chkwwr1" value="Call center executive" class="mybox"/><label for="chkwwr1">Call center executive</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr2" name="chkwwr2" value="Ground Staff" class="mybox"/><label for="chkwwr2">Ground Staff</label>
										</div>
										<div style="clear:both"></div>
										<div class="col">
											<input type="checkbox" id="chkwwr3" name="chkwwr3" value="Issues in car pick up & drop" class="mybox"/><label for="chkwwr3">Issues in car pick up & drop</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr4" name="chkwwr4" value="Website/booking engine" class="mybox"/><label for="chkwwr4">Website/booking engine</label>
										</div>
										<div style="clear:both"></div>
										<div class="col">
											<input type="checkbox" id="chkwwr5" name="chkwwr5" value="Condition of the car" class="mybox"/><label for="chkwwr5">Condition of the car</label>
										</div>
										<div class="col">
											<input type="checkbox" id="chkwwr6" name="chkwwr6" value="Invoicing" class="mybox"/><label for="chkwwr6">Invoicing</label>
										</div>
										<div style="clear:both"></div>
										<div class="col" style="width:500px;">
											<input type="checkbox" id="chkwwr7" name="chkwwr7" value="1" class="mybox" onclick="javascript: _showWWROthers(this.id)"/><label for="chkwwr7">Other</label>
											<textarea  id="txtWrong" name="txtWrong" rows="2" cols="40" style="display: none;margin-left:20px;">Please specify</textarea>
										</div>
									</div>
									</div>
								</div>
							</div>							
							<div id="divIsNotForSelfDrive" style="display:none;">
								<div class="fieldrow2 row">
								<label>Was the booking procedure convenient?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo1" name="rdo1" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo2" name="rdo1" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Did the cab report on time?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo3" name="rdo2" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo4" name="rdo2" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Were you happy with your experience with the chauffeur?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo5" name="rdo3" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo6" name="rdo3" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
									<label>Did the car meet your expectations?</label>
								</div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo7" name="rdo4" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo8" name="rdo4" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow2 row">
								<label>Were you satisfied with your overall experience with us?</label>
								</div>
								<div style="clear:both"></div>
								<div class="fieldrow resp">
									<table width="20%" cellspacing="2" cellpadding="2" border="0" style="margin-left:20px;">
										<tbody><tr>
											<td width="15px"><input type="radio" value="1" id="rdo9" name="rdo5" /></td>
											<td>Yes</td>
											<td width="15px"><input type="radio" value="2" id="rdo10" name="rdo5" /></td>
											<td>No</td>
										</tr>
									</tbody></table>
								</div>
								</div>
								<div style="clear:both"></div>
								<div class="row">
									What can we do to keep you coming back for more?
									<div style="clear:both"></div>
									<div class="resp resp">
									<textarea id="txtfeedback" name="txtfeedback" rows="3" cols="60"></textarea>
									</div>
								</div>
								<div class="row">
									Are you human? Please type the digits<br /><br />
									<div style="float:left;width:80px;"><img alt="Image Capture" title="Image Capture" src="../captcha.php" id="imgcaptcha" /></div> <div style="float:left;width:260px;"><input class="captcha ddl" type="text" name="capcha" id="capcha" /></div>
									<span id="err"></span>
								</div>
								<div class="fieldrow">
									<!-- <div class="submit"><br /><a onclick="javascript: _validateFeedback();" id="feedbacksubmit" href="javascript:void(0)" style="float:left !important;"></a></div> -->
									<div class="submit"><br /><input type="submit" id="feedbacksubmit" name="feedbacksubmit" href="javascript:void(0)" style="float:left !important;" /></div>
									<span id="finishmess"></span>
								</div>
						</form>
								</div>
					</div>
				</div>
			</div>
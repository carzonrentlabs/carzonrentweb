<?php
class api
{
	static function apicall()
	{
		$service_url = 'http://api.mylescars.com/Services/MylesService.svc/GetListOfCarModels';
		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");                                                                                                                            
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
		   'Content-Type: application/json')                                                                       
		);   
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
		   $info = curl_getinfo($curl);
		   curl_close($curl);
		   die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
	    curl_close($curl);
	    $decoded = json_decode($curl_response);
	   if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
	   die('error occured: ' . $decoded->response->errormessage);
	   }
	   return $decoded;
	}
}
?>

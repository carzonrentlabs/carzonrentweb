<?php
include('../dbconnect.php');
include('apirestcall.php');
$result = api::apicall();
//print_R($result);exit;
$content = array();
$i = 0;
if(isset($_REQUEST['send']) && $_REQUEST['send']=="Submit")
{
  $modalname = $_REQUEST['modalname'];
  $url_for_file_name = str_replace('[Car Model]',str_replace(' ','-',$modalname),str_replace('[city name]','cityspecific',$_REQUEST['url']));  
 //echo $url_for_file_name;exit;
  $limit = $_REQUEST['thousand'];
  $lim="";
  $filename="";
  if($limit=='1000')
  {
	  $lim = '1,1000';
	  $filename = $url_for_file_name .'1'.'.xml';
  }
  else
  {
	  $lim = '1000,1600';
	  $filename = $url_for_file_name .'2'.'.xml';
  }
  $sql ="select * from myles_seolocality limit $lim";
  $res = mysql_query($sql);
  while($city = mysql_fetch_array($res))
  {
	    $content[$i]['modalname'] = $modalname;
		$content[$i]['permalink'] = 'locality/'.str_replace('[Car Model]',str_replace(' ','-',$modalname),str_replace('[city name]',str_replace(' ','-',$city['localityname']),trim($_REQUEST['url'])));
		$content[$i]['updated'] = date('Y-m-d');
		$content[$i]['priority'] = "0.51";
		$i++;
  }
	createXml($content,$filename);
 }
function createXml($content,$filename){

	$xml = new DomDocument('1.0', 'utf-8'); 
	$xml->formatOutput = true; 

	// creating base node
	$urlset = $xml->createElement('urlset'); 
	$urlset -> appendChild(
		new DomAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9')
	);

		// appending it to document
	$xml -> appendChild($urlset);
	
	$site_url = "http://www.mylescars.com/";
	// building the xml document with your website content
	foreach($content as $entry)
	{

		//Creating single url node
		$url = $xml->createElement('url'); 

		//Filling node with entry info
		$url -> appendChild( $xml->createElement('loc', $site_url.$entry['permalink']) ); 
		$url -> appendChild( $lastmod = $xml->createElement('lastmod', $entry['updated']) ); 
		$url -> appendChild( $changefreq = $xml->createElement('changefreq', 'weekly') ); 
		$url -> appendChild( $priority = $xml->createElement('priority', $entry['priority']) ); 
		$modalname = $entry['modalname'];

		// append url to urlset node
		$urlset -> appendChild($url);

	}
    //$mpdf->Output($_SERVER["DOCUMENT_ROOT"].'/myles/webroot/Agreements/'.$res['coric'].'.pdf','F');
    //$xml->save($_SERVER["DOCUMENT_ROOT"].'/myles_web/webroot/localitywithmodel/'.$filename); //local
	$xml->save('/var/www/html/mylescars/webroot/localitywithmodel/'.$filename); //live
	if($xml)
	{
		echo $_REQUEST['modalname'].' SiteMap Executed Successfully with limit '.$_REQUEST['thousand'];
	}
}
?>
<html>
<title>Sitemap For City With Modal</title>
<style type="text/css">
.form-style-3{
    max-width: 450px;
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
.form-style-3 label{
    display:block;
    margin-bottom: 10px;
}
.form-style-3 label > span{
    float: left;
    width: 100px;
    color: #F072A9;
    font-weight: bold;
    font-size: 13px;
    text-shadow: 1px 1px 1px #fff;
}
.form-style-3 fieldset{
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    margin: 0px 0px 10px 0px;
    border: 1px solid #FFD2D2;
    padding: 20px;
    background: #FFF4F4;
    box-shadow: inset 0px 0px 15px #FFE5E5;
    -moz-box-shadow: inset 0px 0px 15px #FFE5E5;
    -webkit-box-shadow: inset 0px 0px 15px #FFE5E5;
}
.form-style-3 fieldset legend{
    color: #FFA0C9;
    border-top: 1px solid #FFD2D2;
    border-left: 1px solid #FFD2D2;
    border-right: 1px solid #FFD2D2;
    border-radius: 5px 5px 0px 0px;
    -webkit-border-radius: 5px 5px 0px 0px;
    -moz-border-radius: 5px 5px 0px 0px;
    background: #FFF4F4;
    padding: 0px 8px 3px 8px;
    box-shadow: -0px -1px 2px #F1F1F1;
    -moz-box-shadow:-0px -1px 2px #F1F1F1;
    -webkit-box-shadow:-0px -1px 2px #F1F1F1;
    font-weight: normal;
    font-size: 12px;
}
.form-style-3 textarea{
    width:250px;
    height:100px;
}
.form-style-3 input[type=text],
.form-style-3 input[type=date],
.form-style-3 input[type=datetime],
.form-style-3 input[type=number],
.form-style-3 input[type=search],
.form-style-3 input[type=time],
.form-style-3 input[type=url],
.form-style-3 input[type=email],
.form-style-3 select, 
.form-style-3 textarea{
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border: 1px solid #FFC2DC;
    outline: none;
    color: #F072A9;
    padding: 5px 8px 5px 8px;
    box-shadow: inset 1px 1px 4px #FFD5E7;
    -moz-box-shadow: inset 1px 1px 4px #FFD5E7;
    -webkit-box-shadow: inset 1px 1px 4px #FFD5E7;
    background: #FFEFF6;
    width:50%;
}
.form-style-3  input[type=submit],
.form-style-3  input[type=button]{
    background: #EB3B88;
    border: 1px solid #C94A81;
    padding: 5px 15px 5px 15px;
    color: #FFCBE2;
    box-shadow: inset -1px -1px 3px #FF62A7;
    -moz-box-shadow: inset -1px -1px 3px #FF62A7;
    -webkit-box-shadow: inset -1px -1px 3px #FF62A7;
    border-radius: 3px;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;    
    font-weight: bold;
}
.required{
    color:red;
    font-weight:normal;
}
</style>
<div class="form-style-3">
<form method="POST">
<label for="field4"><span>Modal Name</span>
<select name="modalname" class="select-field" required style="width: 270px;">>
<option value="" selected>Please Select Modal Name</option>
<?php foreach($result as $val)
{?>
<option  <?php if(isset($_REQUEST['modalname']) && $val->CarCategoryName.' '.$val->CarModelName == $_REQUEST['modalname']){ echo 'selected';}?>  value="<?php echo $val->CarCategoryName.' '.$val->CarModelName;?>"><?php echo $val->CarCategoryName.' '.$val->CarModelName;?></option>
<?php } ?>
</select></label>
<label for="field4"><span>URL</span>
<select name="url" class="select-field" required style="width: 270px;">
<option value="" selected>Please Select Url</option>

<option <?php if(isset($_REQUEST['url']) && $_REQUEST['url']=="[Car Model]-Car-for-Self-Drive-in-[city name]"){ echo "selected";}?> value="[Car Model]-Car-for-Self-Drive-in-[city name]">[Car Model]-Car-for-Self-Drive-in-[city name]</option>

<option  <?php if(isset($_REQUEST['url']) && $_REQUEST['url']=="Rent-[Car Model]-Car-in-[city name]"){ echo "selected";}?> value="Rent-[Car Model]-Car-in-[city name]">Rent-[Car Model]-Car-in-[city name]</option>

<option  <?php if(isset($_REQUEST['url']) && $_REQUEST['url']=="Book-[Car Model]-Car-in-[city name]"){ echo "selected";}?> value="Book-[Car Model]-Car-in-[city name]">Book-[Car Model]-Car-in-[city name]</option>

<option <?php if(isset($_REQUEST['url']) && $_REQUEST['url']=="Hire-[Car Model]-Car-in-[city name]"){ echo "selected";}?> value="Hire-[Car Model]-Car-in-[city name]">Hire-[Car Model]-Car-in-[city name]</option>
</select></label>
<label for="field4"><span>Limit</span>
 <input type="radio" <?php if(isset($_REQUEST['thousand']) && $_REQUEST['thousand']=='1000') { echo 'checked';}?>  name="thousand" value="1000"  required/> 1-1000  &nbsp;||&nbsp;
<input type="radio" <?php if(isset($_REQUEST['thousand']) && $_REQUEST['thousand']=='1600') { echo 'checked';}?>  name="thousand" value="1600" required/> 1000-1600 
</label>

<label><span>&nbsp;</span><input type="submit" value="Submit" name="send"/></label>
</form>
</div>
</html>


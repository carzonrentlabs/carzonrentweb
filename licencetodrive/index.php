<?php
	error_reporting(0);
	include_once("../includes/cache-func.php");
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Car Rental | Hire Company | Book a Cab in India -  Carzonrent</title>	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/default-new.css" />
  <link rel="stylesheet" type="text/css" href="css/ladakh.css" />
    <link rel="stylesheet" type="text/css" href="css/jcarousel.css" /> 
<body class="body">
	<?php include_once("../includes/header.php"); ?>
<div class="d100pmain">
<div id="main-container">
	<div class="wrappern-main">
                <div class="wrapper-main">
			<span class="headtxt">INTRODUCING MYLES CITY-DRIVE AT ONLY<span class="rs"><img src="./images/rsb.png" /></span>99/HR*</span>
			<span class="ht23">YOUR LICENCE TO DRIVE YOURSELF</span>
			<span class="mimg"><img src="./images/main.gif" /></span>
		</div>
	        <div style="float: left;width:100%">
			<span class="myledv"><img src="./images/myles.png" /></span>
		</div>
		<div style="float: left;width:98%">
			<span style="float:left;">
				<a href="javascript:void(0);" class="btn bkneno" >BOOK YOUR MYLES NANO HERE</a>
			</span>
		</div>
		<div id='tab1' style="display:block; margin-top: 30px;">
			<div class="sl-main m-0 wd100p">
				<div class="tab1_bg">
					<div class="sl-content">
					      <div class="sl-slide visible">
						   <div class="w40 f_l">
							<div class="img_design"><img src="./images/nano.png" id="carbrandimage" alt="" title=""/></div>
						   </div>
						   <div class="w40 pr3 f_l pt30 greyTxt">
							<div class="xuv"><span id="pkgname">Nano</span><br>
							     <img class="rssp" src="images/rsn.png"/> <span id="carbrandcost">99</span>/hr
							     <div class="hdtdv"><span class="hourtxt">HOURLY</span><span class="dailytxt">DAILY</span></div>
							</div>
							
							<div class="h10"> </div>
							<div class="w48 f_l pr2">
							     <input type="text" class="from picktime fbff" id="inputFieldSF1" onchange="javascript: _setDropDateMyles('inputFieldSF1', 'inputFieldSF2');" placeholder="Pick Up Date and Time" readonly="readonly" />
							</div>
							<div class="w50 f_l">
							     <input type="text" class="from picktime fbff" id="inputFieldSF2" placeholder="Drop Off Date and Time" readonly="readonly" onchange="javascript: _setDropOffDate(this.id);" />
							</div>
						   </div>
						   <div class="w15 f_l pt95 pr2 greyTxt">
							<a href="javascript: void(0);" class="fareD button">Fare details</a>
							</div>
							<button type="button" class="select button">Select</button>				
						</div>
					</div>
				</div>
			</div>
			<div style="float:left;width:100%;margin:25px 0px 20px 0px">
			    <ul id="mycarnano-1">
				    <li><img src="./images/nsv.gif"/></li>
				    <li><img src="./images/nsb.gif"/></li>
				    <li><img src="./images/nsw.gif"/></li>
				    <li><img src="./images/nsy.gif"/></li>	       
			    </ul>
		       </div>
	       </div>
	  </div>
		<div style="float: left;">
			<div class="citydrive">
				
					<div class="mylesdv">
					<span class="ctydrvtxt">MYLES CITY-DRIVE</span>
					</div>
					
					<div class="ctydv">
						<div class="ctyinnerdv">
						<span class="hcty">Now drive a car when you need one,<br />without the cost and hassle of owning one.</span>
						<span class="hiretxt">HIRE AT</span>
						<div class="chrgdv">
							<li class="chargestxt"><span class="rssp"><img src="./images/rs.png" /></span>99/HR</li><li class="chargestxt"><span class="rssp"><img src="./images/rs.png" /></span>399/DAY</li><li class="chargestxt" style="float: right !important;padding-right: 0 !important"><span class="rssp"><img src="./images/rs.png" /></span>6999/MONTH</li>
						</div>
						<span class="incfuel">Inclusive of Fuel &nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="incfuel">47 Pick Up Locations</span>
						</div>
					</div>
				
			</div>
		</div>
		<div class="dvchkout">
			<div class="bbfb"></div><span style="float:right;width:210px"><a href="javascript:void(0);" class="btn wd210 tal" >CHECK OUT OTHER<br /> MYLES CARS</a>
			</span>
		</div>
	</div>
</div>
<div class="clr"> </div>
<footer>
	  <div class="main">
	  <div class="f_l w50">
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/aboutus.php">About Us</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/services.php">Our Service</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/vehicle-guide.php">Vehicle Guide</a></li>
		    <!--<li><a href="#">Media</a></li>-->
		    <li><a href="http://careers.carzonrent.com/">Careers</a></li>
	       </ul>
	  </div>
	  <div class="f_r w50 t_a_r">
	       <a href="https://www.facebook.com/carzonrent" target="_blank"><img src="images/facebook.png" border="0" class="mr2"/></a>
	       <a href="https://plus.google.com/+carzonrent" target="_blank"><img src="images/google+.png" border="0" class="mr2"/></a>
	       <a href="https://twitter.com/CarzonrentIN" target="_blank"><img src="images/twitter.png" border="0"/></a>
	       <ul>
		    <li><a href="<?php echo corWebRoot; ?>/login.php">Account Summary</a></li>
		    <li><a href="<?php echo corWebRoot; ?>/contact-us.php">Contact Us</a></li>
	       </ul>
	       <div class="copyright">
		    Copyright � 2014 Carzonrent India Pvt Ltd. All Rights Reserved.
	       </div>
	  </div>
	  <div class="clr"> </div>
     </div>
     </footer>
</body>
</html>